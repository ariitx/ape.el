﻿Namespace Misc
    Public Class PrintHelper
        ''' <summary>
        ''' Printing output method.
        ''' </summary>
        Public Enum OutputType
            ''' <summary> Show report in modal dialog window using FastReport. </summary>
            Preview
            ''' <summary> Show report in design mode using FastReport. </summary>
            Designer
            ''' <summary> Print out report to selected printer using FastReport. </summary>
            Print
            ''' <summary> Show printer selector and print using FastReport. </summary>
            SelectAndPrint
            ''' <summary> Experimental mode. Save report as TXT in ExportPath using FastReport. </summary>
            ExportTxt
            ''' <summary> Save report as PDF in ExportPath using FastReport. </summary>
            ExportPdf
        End Enum
    End Class
End Namespace
