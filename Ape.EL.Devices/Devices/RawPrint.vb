﻿Imports System
Imports System.IO
Imports System.Runtime.InteropServices
Imports System.Text

Namespace Devices
    ''' <summary>
    ''' Provide generic raw printing method. Use PrintText for better dot-matrix printers compatibility.
    ''' </summary>
    Public Class RawPrint
        Inherits Devices.Printer

#Region "Nested types"
        Public Structure PRINTER_DEFAULTS
            Public pDocName As String
            Public pOutputFile As String
            Public pDataType As String
        End Structure

        Public Class NativeMethods
            <DllImport("winspool.drv")> _
            Friend Shared Function ClosePrinter(ByVal hPrinter As IntPtr) As <MarshalAs(UnmanagedType.Bool)> Boolean
            End Function

            <DllImport("winspool.drv")> _
            Friend Shared Function EndDocPrinter(ByVal hPrinter As IntPtr) As <MarshalAs(UnmanagedType.Bool)> Boolean
            End Function

            <DllImport("winspool.drv")> _
            Friend Shared Function EndPagePrinter(ByVal hPrinter As IntPtr) As <MarshalAs(UnmanagedType.Bool)> Boolean
            End Function

            <DllImport("winspool.drv")> _
            Friend Shared Function OpenPrinter(ByVal pPrinterName As String, <Out> ByRef phPrinter As IntPtr, ByVal pDefault As IntPtr) As <MarshalAs(UnmanagedType.Bool)> Boolean
            End Function

            <DllImport("winspool.drv")> _
            Friend Shared Function StartDocPrinter(ByVal hPrinter As IntPtr, ByVal level As UInt32, ByRef pDocInfo As PRINTER_DEFAULTS) As <MarshalAs(UnmanagedType.Bool)> Boolean
            End Function

            <DllImport("winspool.drv")> _
            Friend Shared Function StartPagePrinter(ByVal hPrinter As IntPtr) As <MarshalAs(UnmanagedType.Bool)> Boolean
            End Function

            <DllImport("winspool.drv")> _
            Friend Shared Function WritePrinter(ByVal hPrinter As IntPtr, ByVal pBuf As Byte(), ByVal cbBuf As UInt32, <Out> ByRef pcWritten As UInt32) As <MarshalAs(UnmanagedType.Bool)> Boolean
            End Function
        End Class
#End Region

#Region "Fields"
        Protected myStream As MemoryStream = New MemoryStream()
#End Region

#Region "Properties"
        Public Property PrinterName() As String
            Get
                Return Me._PrinterName
            End Get
            Set(value As String)
                If value Is Nothing Then
                    Throw New ArgumentNullException("value")
                End If
                Me._PrinterName = value
            End Set
        End Property
        Private _PrinterName As String

        Public Property DocumentName() As String
            Get
                Return Me._DocumentName
            End Get
            Set(value As String)
                If value Is Nothing Then
                    Throw New ArgumentNullException("value")
                End If
                Me._DocumentName = value
            End Set
        End Property
        Private _DocumentName As String = "Raw Print"

        Public Property Encoding() As Encoding
            Get
                Return Me._Encoding
            End Get
            Set(value As Encoding)
                If value Is Nothing Then
                    Throw New ArgumentNullException("value")
                End If
                Me._Encoding = value
            End Set
        End Property
        Private _Encoding As Encoding = System.Text.Encoding.Default
#End Region

#Region "Methods"
        ''' <summary>
        ''' Create a new raw print object.
        ''' </summary>
        ''' <param name="printerName">Printer to use.</param>
        ''' <param name="documentName">Set documentName to "-1" to use original DocumentName.</param>
        Public Sub New(printerName As String, documentName As String)
            Me.PrinterName = printerName

            If documentName <> "-1" Then
                Me.DocumentName = documentName
            End If
        End Sub

        ''' <summary>
        ''' Create a new raw print object.
        ''' </summary>
        ''' <param name="printerName">Printer to use.</param>
        Public Sub New(printerName As String)
            Me.New(printerName, "-1")
        End Sub

        Public Sub Clear()
            Me.myStream = New MemoryStream()
        End Sub

        Public Sub Write(buffer As Byte())
            If buffer Is Nothing Then
                Throw New ArgumentNullException("buffer")
            End If
            Me.myStream.Write(buffer, 0, buffer.Length)
        End Sub

        Public Sub Write(str As String)
            If str Is Nothing Then
                Throw New ArgumentNullException("str")
            End If
            Me.Write(Me.Encoding.GetBytes(str))
        End Sub

        Public Sub WriteLine()
            Me.myStream.WriteByte(10)
        End Sub

        Public Sub WriteLine(str As String)
            Me.Write(str)
            Me.WriteLine()
        End Sub

        ''' <summary>
        ''' Execute printing based on data set from Write methods.
        ''' </summary>
        Public Overridable Sub Print()
            If Me.myStream.ToArray.Count > 0 AndAlso Me.myStream.ToArray.Last <> 10 Then
                WriteLine()
            End If
            Print(myStream)
        End Sub

        ''' <summary>
        ''' Send stream directly to printer.
        ''' </summary>
        Public Sub Print(nStream As MemoryStream)
            If Me.PrinterName.IsEmpty Then
                Me.PrinterName = GetDefaultPrinter()
            End If

            Dim dOC_INFO_ As RawPrint.PRINTER_DEFAULTS = Nothing
            dOC_INFO_.pDocName = Me.DocumentName
            dOC_INFO_.pOutputFile = Nothing
            dOC_INFO_.pDataType = "RAW"

            Dim hPrinter As IntPtr
            If Not RawPrint.NativeMethods.OpenPrinter(Me.PrinterName, hPrinter, IntPtr.Zero) Then
                Throw New IOException("Failed to open printer: " + Me.PrinterName)
            End If
            Dim flag As Boolean = False

            If NativeMethods.StartDocPrinter(hPrinter, 1, dOC_INFO_) Then
                If RawPrint.NativeMethods.StartPagePrinter(hPrinter) Then
                    ' Get stream and convert it to byte array.
                    Dim array As Byte() = nStream.ToArray()

                    '   begin printing using the byte array.
                    Dim num As UInteger
                    flag = RawPrint.NativeMethods.WritePrinter(hPrinter, array, CUInt(array.Length), num)
                    RawPrint.NativeMethods.EndPagePrinter(hPrinter)
                End If
                RawPrint.NativeMethods.EndDocPrinter(hPrinter)
            End If
            RawPrint.NativeMethods.ClosePrinter(hPrinter)
            If Not flag Then
                Throw New IOException("Failed to print data.")
            End If
        End Sub
#End Region

#Region "Shared methods"
        ' SendBytesToPrinter()
        ' When the function is given a printer name and an unmanaged array of  
        ' bytes, the function sends those bytes to the print queue.
        ' Returns True on success or False on failure.
        Public Shared Function SendBytesToPrinter(ByVal szPrinterName As String, ByVal pBytes() As Byte) As Boolean
            Try
                Dim rp As New RawPrint(szPrinterName)
                rp.Write(pBytes)
                rp.Print()
                Return True
            Catch ex As Exception
                Ape.EL.WinForm.General.WarningMsg(ex.Message)
            End Try
            Return False
        End Function

        ' SendFileToPrinter()
        ' When the function is given a file name and a printer name, 
        ' the function reads the contents of the file and sends the
        ' contents to the printer.
        ' Presumes that the file contains printer-ready data.
        ' Shows how to use the SendBytesToPrinter function.
        ' Returns True on success or False on failure.
        Public Shared Function SendFileToPrinter(ByVal szPrinterName As String, ByVal szFileName As String) As Boolean
            ' Open the file.
            Using fs As New FileStream(szFileName, FileMode.Open)
                ' Create a BinaryReader on the file.
                Using br As New BinaryReader(fs)
                    ' Dim an array of bytes large enough to hold the file's contents.
                    Dim bytes(fs.Length) As Byte
                    ' Read the contents of the file into the array.
                    bytes = br.ReadBytes(fs.Length)
                    Return SendBytesToPrinter(szPrinterName, bytes)
                End Using
            End Using

            Return False
        End Function

        ' When the function is given a string and a printer name,
        ' the function sends the string to the printer as raw bytes.
        Public Shared Function SendStringToPrinter(ByVal szPrinterName As String, ByVal szString As String) As Boolean
            Try
                Dim rp As New RawPrint(szPrinterName)
                rp.Write(szString)
                rp.Print()
                Return True
            Catch ex As Exception
                Ape.EL.WinForm.General.WarningMsg(ex.Message)
            End Try
            Return False
        End Function

        ''' <summary>
        ''' Send partial cut command to Bixolon printers.
        ''' </summary>
        Public Shared Function SendCutCommand(ByVal szPrinterName As String, Optional ByVal lfCount As Integer = 3) As Boolean
            Dim lfList As New List(Of Byte)
            For i As Integer = 0 To lfCount - 1
                lfList.Add(10)
            Next
            lfList.AddRange({29, 86, 1}) 'GS, V, 1
            Return SendCommandToPrinter(szPrinterName, lfList.ToArray)
        End Function

        ''' <summary>
        ''' Send alignment command to Bixolon printers.
        ''' </summary>
        ''' <param name="n">Left = 0; Center = 1; Right = 2; Default is 0.</param>
        Public Shared Function SendAlignmentCommand(ByVal szPrinterName As String, ByVal n As Byte) As Boolean
            If n < 0 Or n > 2 Then n = 0
            Return SendCommandToPrinter(szPrinterName, New Byte() {27, 64, 27, 97, n}) 'ESC, @ 'ESC, a, n
        End Function

        ''' <summary>
        ''' Send printer font command to Bixolon printers.
        ''' </summary>
        ''' <param name="n">Font A = 0; Font B = 1; Default is 0.</param>
        Public Shared Function SendFontCommand(ByVal szPrinterName As String, ByVal n As Byte) As Boolean
            If n < 0 Or n > 1 Then n = 0
            Return SendCommandToPrinter(szPrinterName, New Byte() {27, 77, n}) 'ESC, M, n
        End Function

        ''' <summary>
        ''' Send command to printer with leading ESC.
        ''' </summary>
        Public Shared Function SendEscapeCommandToPrinter(ByVal szPrinterName As String, ByVal szString As String) As Boolean
            szString = Chr(27) & szString
            Return SendCommandToPrinter(szPrinterName, szString)
        End Function

        ''' <summary>
        ''' Use ASCII to get byte array from string and send to printer.
        ''' </summary>
        Public Shared Function SendCommandToPrinter(ByVal szPrinterName As String, ByVal szString As String) As Boolean
            Dim pBytes As Byte() = Encoding.ASCII.GetBytes(szString)
            Return SendCommandToPrinter(szPrinterName, pBytes)
        End Function

        ''' <summary>
        ''' Send the raw byte array to printer.
        ''' </summary>
        Public Shared Function SendCommandToPrinter(ByVal szPrinterName As String, ByVal pBytes As Byte()) As Boolean
            Try
                Using rp As New RawPrint(szPrinterName)
                    'Convert byte array to memory stream to print
                    rp.Print(New MemoryStream(pBytes))
                End Using

                Return True
            Catch ex As Exception
                Ape.EL.WinForm.General.WarningMsg(ex.Message)
            End Try
            Return False
        End Function
#End Region
    End Class
End Namespace
