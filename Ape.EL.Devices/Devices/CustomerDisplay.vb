Imports Ape.EL.Localization

Namespace Devices
    Public Class CustomerDisplay
        '        Private ReadOnly lic As Boolean = Internal.CheckMyLicense

        <LocalizableString()>
        Public Enum CdType
            <DefaultString("Generic")>
            Generic
            <DefaultString("Timer")>
            Timer
        End Enum

        Private WithEvents SerialPort_CustomerDisplay As System.IO.Ports.SerialPort

        Public Sub SetCustDisplay()
            Try
                'SerialPort_CustomerDisplay.PortName = "Com" & p_strCD_Port
                'SerialPort_CustomerDisplay.BaudRate = "9600"
                'SerialPort_CustomerDisplay.DataBits = 8
                'SerialPort_CustomerDisplay.StopBits = Ports.StopBits.One
                'SerialPort_CustomerDisplay.Parity = IO.Ports.Parity.None

                'Select Case p_intCD_Handshake
                '    Case 0
                '        SerialPort_CustomerDisplay.Handshake = IO.Ports.Handshake.None
                '    Case 1
                '        SerialPort_CustomerDisplay.Handshake = IO.Ports.Handshake.RequestToSend
                '    Case 2
                '        SerialPort_CustomerDisplay.Handshake = IO.Ports.Handshake.RequestToSendXOnXOff
                '    Case 3
                '        SerialPort_CustomerDisplay.Handshake = IO.Ports.Handshake.XOnXOff
                '    Case Else
                '        SerialPort_CustomerDisplay.Handshake = IO.Ports.Handshake.RequestToSend
                'End Select

                'SerialPort_CustomerDisplay.WriteTimeout = 1000
                'SerialPort_CustomerDisplay.ReadTimeout = 1000

                'If SerialPort_CustomerDisplay.IsOpen Then
                '    SerialPort_CustomerDisplay.Close()
                'End If

                'SerialPort_CustomerDisplay.Open()

                'If SerialPort_CustomerDisplay.IsOpen Then
                '    SerialPort_CustomerDisplay.Write(Chr(31) & Chr(67) & Chr(0)) 'Hide cursor
                'End If

                'If Not String.IsNullOrEmpty(p_strCDInitCmd) Then ' comma (,) is the separator
                '    If SerialPort_CustomerDisplay.IsOpen Then
                '        Dim aryInitCmd As String() = p_strCDInitCmd.Split(New String() {","}, StringSplitOptions.None)
                '        For i As Integer = 0 To aryInitCmd.Count - 1
                '            SerialPort_CustomerDisplay.Write(Chr(aryInitCmd(i)))
                '        Next
                '    End If
                'End If

                'CustomerDisplay("WELCOME TO " & p_strCompany & IIf(p_strIdleMessage = "", "", ", " & p_strIdleMessage) & " ", "", True)

            Catch ex As Exception

            End Try
        End Sub

        Public Sub CustomerDisplay(ByVal strLine1 As String, ByVal strLine2 As String, Optional ByVal bScroll As Boolean = False)
            Try
                'timerCustomerDisplay.Stop() 'force stop writing to customer display
                'strCustomerDisplayLine1 = ""
                'strCustomerDisplayLine2 = ""
                'intCustomerDisplayCount = 0
                'bCustomerDisplayNew = False

                'If p_strCD_EXIST = "Y" Then
                '    If p_bCDDisableScrolling = True Then bScroll = False

                '    If Not bScroll Then 'Center the Text if not Scroll
                '        strLine1 = CenterStr(strLine1, p_intCD_LEN)
                '        strLine2 = CenterStr(strLine2, p_intCD_LEN)

                '        If Len(strLine1) > p_intCD_LEN Then
                '            strLine1 = Mid(strLine1, 1, p_intCD_LEN)
                '        End If

                '        If Len(strLine2) > p_intCD_LEN Then
                '            strLine2 = Mid(strLine2, 1, p_intCD_LEN)
                '        End If
                '    End If

                '    If SerialPort_CustomerDisplay.IsOpen Then
                '        SerialPort_CustomerDisplay.Write(Chr(12)) ' Clear Screen
                '        SerialPort_CustomerDisplay.Write(Chr(11)) ' Home
                '        If bScroll Then
                '            Select Case p_intCDScrollMode
                '                Case 1 'if using method 2
                '                    'only scroll for first line
                '                    strCustomerDisplayLine1 = strLine1
                '                    strCustomerDisplayLine2 = strLine2
                '                    bCustomerDisplayNew = True
                '                    timerCustomerDisplay.Start()
                '                Case Else 'if using original method or other selection is selected
                '                    SerialPort_CustomerDisplay.Write(Chr(27) & Chr(81) & Chr(68) & strLine1 & Chr(13))
                '            End Select
                '        Else
                '            SerialPort_CustomerDisplay.Write(strLine1)
                '            SerialPort_CustomerDisplay.Write(Chr(31) & Chr(66)) 'Move Cursor to bottom position
                '            SerialPort_CustomerDisplay.Write(Chr(13)) 'Move Cursor to left hand position
                '            SerialPort_CustomerDisplay.Write(strLine2)
                '        End If
                '    End If
                'End If

            Catch ex As Exception
                Throw New Exception("Write Customer Display failure.", ex)
            End Try
        End Sub

#Region "CustomerDisplay Setting Property"
        Private CD_Port As String
        Private CD_Handshake As Integer
        Private CDInitCmd As String
#End Region
    End Class
End Namespace