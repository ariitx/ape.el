﻿Imports FastReport.Utils
Imports System.Windows.Forms
Imports Ape.EL.Extensions
Imports FastReport.Design
Imports FastReport.Design.StandardDesigner

Namespace Devices
    ''' <summary>
    ''' Printing with FastReport.
    ''' </summary>
    ''' <remarks>
    ''' Notes from Ari: Sorry guys if this is too complex for you.
    ''' This is to make sure the Save event in FastReport can be forwarded to PrintReport object and thus allowing you to save the MemoryStream object.
    ''' </remarks>
    Public Class PrintReport
        Inherits Devices.Printer

#Region "Nested types"
        ''' <summary>
        ''' Internal use only.
        ''' </summary>
        <System.ComponentModel.DesignerCategory(" ")> _
        Private Class FReport
            Inherits FastReport.Report

            Property Form As Form
                Get
                    Return _Form
                End Get
                Set(value As Form)
                    _Form = value
                End Set
            End Property
            Private _Form As Form = Nothing

            ''' <summary>
            ''' Get report stream.
            ''' </summary>
            ReadOnly Property ReportStream As System.IO.MemoryStream
                Get
                    Return PrintReport.GetFastReportStream(Me)
                End Get
            End Property

            Property OwnerObject As PrintReport
                Get
                    Return _OwnerObject
                End Get
                Set(value As PrintReport)
                    _OwnerObject = value
                End Set
            End Property
            Private _OwnerObject As PrintReport

            Public Sub New(ByRef oo As PrintReport)
                MyBase.New()
                OwnerObject = oo
            End Sub

            Public Sub New()
                MyBase.New()
            End Sub

            Function GetReportData() As ReportData
                Return New ReportData(Me)
            End Function
        End Class

        ''' <summary>
        ''' Internal use only.
        ''' </summary>
        <System.ComponentModel.DesignerCategory(" ")> _
        Public Class ReportData
            Implements IDisposable

            ''' <summary>
            ''' Get report stream.
            ''' </summary>
            ReadOnly Property ReportStream As System.IO.MemoryStream
                Get
                    Return _ReportStream
                End Get
            End Property
            Private _ReportStream As System.IO.MemoryStream

            ''' <summary>
            ''' Get report file path.
            ''' </summary>
            ReadOnly Property ReportPath As String
                Get
                    Return _ReportPath
                End Get
            End Property
            Private _ReportPath As String

            ''' <summary>
            ''' Get report designer form.
            ''' </summary>
            ReadOnly Property DesignerForm As Form
                Get
                    Return _DesignerForm
                End Get
            End Property
            Private _DesignerForm As Form

            ''' <summary>
            ''' Get FastReport.Report object.
            ''' </summary>
            ReadOnly Property FastReport As FastReport.Report
                Get
                    Return _FastReport
                End Get
            End Property
            Private _FastReport As FastReport.Report

            Public Sub New(ByVal _report As FastReport.Report)
                _FastReport = _report
                _ReportStream = PrintReport.GetFastReportStream(_report)
                _ReportPath = _report.Report.FileName()
                _DesignerForm = _report.Designer.FindForm()
            End Sub

#Region "IDisposable Support"
            Private disposedValue As Boolean ' To detect redundant calls

            ' IDisposable
            Protected Overridable Sub Dispose(disposing As Boolean)
                If Not Me.disposedValue Then
                    If disposing Then
                        ' TODO: dispose managed state (managed objects).
                        If _ReportStream IsNot Nothing Then
                            _ReportStream.Dispose()
                        End If
                    End If

                    ' TODO: free unmanaged resources (unmanaged objects) and override Finalize() below.
                    ' TODO: set large fields to null.
                End If
                Me.disposedValue = True
            End Sub

            ' TODO: override Finalize() only if Dispose(ByVal disposing As Boolean) above has code to free unmanaged resources.
            'Protected Overrides Sub Finalize()
            '    ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            '    Dispose(False)
            '    MyBase.Finalize()
            'End Sub

            ' This code added by Visual Basic to correctly implement the disposable pattern.
            Public Sub Dispose() Implements IDisposable.Dispose
                ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
                Dispose(True)
                GC.SuppressFinalize(Me)
            End Sub
#End Region
        End Class
#End Region

#Region "Properties"
        ''' <summary>
        ''' Dataset to be used in LoadReport function.
        ''' </summary>
        Public Property ReportDataSet() As DataSet
            Get
                Return _ReportDataSet
            End Get
            Set(ByVal value As DataSet)
                _ReportDataSet = value
            End Set
        End Property
        Private _ReportDataSet As DataSet

        ''' <summary>
        ''' paramHeader.
        ''' </summary>
        Public Property RptHeaderName() As String
            Get
                Return _RptHeaderName
            End Get
            Set(ByVal value As String)
                _RptHeaderName = value
            End Set
        End Property
        Private _RptHeaderName As String

        ''' <summary>
        ''' paramFilter.
        ''' </summary>
        Public Property RptFilterDate() As String
            Get
                Return _RptFilterDate
            End Get
            Set(ByVal value As String)
                _RptFilterDate = value
            End Set
        End Property
        Private _RptFilterDate As String

        ''' <summary>
        ''' Number of copy.
        ''' </summary>
        Public Property NoOfCopy() As Integer
            Get
                Return _NoOfCopy
            End Get
            Set(ByVal value As Integer)
                _NoOfCopy = value
            End Set
        End Property
        Private _NoOfCopy As Integer = 1

        ''' <summary>
        ''' Modal dialog to contain the report preview or design form.
        ''' </summary>
        Property Owner As Form
            Get
                Return _Owner
            End Get
            Set(value As Form)
                _Owner = value
                If _Owner IsNot Nothing Then
                    Config.PreviewSettings.Icon = Owner.Icon
                    Config.DesignerSettings.Icon = Owner.Icon
                Else
                    Config.PreviewSettings.Icon = Nothing
                    Config.DesignerSettings.Icon = Nothing
                End If
            End Set
        End Property
        Private _Owner As Form

        ''' <summary>
        ''' Force use modal dialog when previewing or designing.
        ''' </summary>
        Property UseModal As Boolean
            Get
                Return _UseModal
            End Get
            Set(value As Boolean)
                _UseModal = value
            End Set
        End Property
        Private _UseModal As Boolean

        ''' <summary>
        ''' Determine whether showing Preview in owner as mdi-container (Only applicable when Owner is MdiContainer). Default value is true.
        ''' </summary>
        Property UseMdi As Boolean
            Get
                Return _UseMdi
            End Get
            Set(value As Boolean)
                _UseMdi = value
            End Set
        End Property
        Private _UseMdi As Boolean = True

        ''' <summary>
        ''' Determine whether when preview or design's form is closed, the owner form will invoke BringToFront. Default value is true.
        ''' </summary>
        Property UseBringFront As Boolean
            Get
                Return _UseBringFront
            End Get
            Set(value As Boolean)
                _UseBringFront = value
            End Set
        End Property
        Private _UseBringFront As Boolean = True

        ''' <summary>
        ''' Path to export the resulted report.
        ''' </summary>
        Property ExportPath As String
            Get
                Return _ExportPath
            End Get
            Set(value As String)
                _ExportPath = value
            End Set
        End Property
        Private _ExportPath As String = Environment.GetFolderPath(Environment.SpecialFolder.Desktop)
#End Region

#Region "Shared properties"
        ''' <summary>
        ''' Get or set indicator for saving designer to stream instead of file. Use CustomSaveReport event to retrieve the report stream.
        ''' </summary>
        Shared Property DesignerSaveToStream As Boolean
            Get
                Return _DesignerSaveToStream
            End Get
            Set(value As Boolean)
                _DesignerSaveToStream = value

                RemoveHandlerDesignerStreamSave()
                If value Then
                    AddHandlerDesignerStreamSave()
                End If
            End Set
        End Property
        Private Shared _DesignerSaveToStream As Boolean

        ''' <summary>
        ''' Get or set indicator for saving designer to stream instead of file. Use CustomOpenReport event to set the report stream.
        ''' </summary>
        Shared Property DesignerLoadFromStream As Boolean
            Get
                Return _DesignerLoadFromStream
            End Get
            Set(value As Boolean)
                _DesignerLoadFromStream = value

                RemoveHandlerDesignerStreamLoad()
                If value Then
                    AddHandlerDesignerStreamLoad()
                End If
            End Set
        End Property
        Private Shared _DesignerLoadFromStream As Boolean

        ''' <summary>
        ''' Get or set common tables to be added into report.
        ''' </summary>
        ''' <remarks>Future implementation</remarks>
        Private Shared Property CommonReportTableList As List(Of DataTable)
            Get
                If _CommonReportTableList Is Nothing Then
                    Return New List(Of DataTable)
                Else
                    Return _CommonReportTableList
                End If
            End Get
            Set(value As List(Of DataTable))
                _CommonReportTableList = value
            End Set
        End Property
        Private Shared _CommonReportTableList As New List(Of DataTable)
#End Region

#Region "Events"
        ''' <summary>
        ''' Occurs when the report designer is about to show the "Open" dialog. Must set DesignerSaveToStream to true.
        ''' </summary>
        ''' <remarks>
        ''' Use this event to attach own "Open" dialog to the designer. In the event handler, you must display a dialog window to allow user to choose a report file.  If dialog was executed successfully, you must return e.Cancel = false and set the e.FileName to the selected file name.
        ''' You also need to use FastReport.Design.DesignerSettings.CustomOpenReport event to provide code that will open the report.
        '''</remarks>
        Private Event CustomOpenDialog(sender As ReportData, e As Object) 'temporary not used

        ''' <summary>
        ''' Occurs when the report designer is about to load the report. Must set DesignerSaveToStream to true.
        ''' </summary>
        ''' <remarks>
        ''' This event is used together with the FastReport.Design.DesignerSettings.CustomOpenDialog event.
        ''' Use this event to attach own "Open" dialog to the designer. In the event handler, you must load the e.Report from the location specified in the e.FileName property.  For example, if you work with files: e.Report.Load(e.FileName);
        '''</remarks>
        Private Event CustomOpenReport(sender As ReportData, e As Object) 'temporary not used

        ''' <summary>
        ''' Occurs when the report designer is about to show the "Save" dialog. Must set DesignerSaveToStream to true.
        ''' </summary>
        ''' <remarks>
        ''' Use this event to attach own "Save" dialog to the designer. In the event handler, you must display a dialog window to allow user to choose a report file.  If dialog was executed successfully, you must return e.Cancel = false and set the e.FileName to the selected file name.
        ''' You also need to use FastReport.Design.DesignerSettings.CustomSaveReport event to provide code that will save the report.
        '''</remarks>
        Public Event CustomSaveDialog(sender As ReportData, e As Object)

        ''' <summary>
        ''' Occurs when the report designer is about to save the report. Must set DesignerSaveToStream to true.
        ''' </summary>
        ''' <remarks>
        ''' This event is used together with the FastReport.Design.DesignerSettings.CustomSaveDialog event.
        ''' Use this event to attach own "Save" dialog to the designer. In the event handler, you must save the e.Report to the location specified in the e.FileName property.  For example, if you work with files: e.Report.Save(e.FileName);
        '''</remarks>
        Public Event CustomSaveReport(sender As ReportData, e As Object)

        Private Sub RaiseCustomOpenDialog(sender As ReportData, e As OpenSaveDialogEventArgs) 'temporary not used
            RaiseEvent CustomOpenDialog(sender, e)
        End Sub

        Private Sub RaiseCustomOpenReport(sender As ReportData, e As OpenSaveReportEventArgs) 'temporary not used
            RaiseEvent CustomOpenReport(sender, e)
        End Sub

        Public Sub RaiseCustomSaveDialog(sender As ReportData, e As OpenSaveDialogEventArgs)
            RaiseEvent CustomSaveDialog(sender, e)
        End Sub

        Public Sub RaiseCustomSaveReport(sender As ReportData, e As OpenSaveReportEventArgs)
            RaiseEvent CustomSaveReport(sender, e)
        End Sub
#End Region

#Region "Methods"
        ''' <summary>
        ''' Show FastReport in designer mode.
        ''' </summary>
        ''' <param name="args">args(0) must be file path. Use /preview for preview.</param>
        Public Sub LoadDesigner(ParamArray args() As String)
            Try
                Dim report As FReport = New FReport(Me)
                Dim path As String = ""
                If args.Length > 0 Then
                    path = args(0)
                End If
                If Array.IndexOf(Of String)(args, "/preview") <> -1 Then
                    report.Load(path)
                    report.Prepare()
                    report.ShowPrepared()
                Else
                    If ReportDataSet IsNot Nothing Then _
                        report.RegisterData(ReportDataSet)
                    report.FileName = path
                    If Owner IsNot Nothing AndAlso Owner.IsMdiContainer AndAlso UseMdi Then
                        report.Design(Owner)
                    Else
                        report.Design(UseModal) 'True
                    End If
                    report.Form = If(report.Designer IsNot Nothing, report.Designer.FindForm, Nothing)
                End If

                FinalizeReport(report)
            Catch ex As Exception
                Ape.EL.WinForm.General.ExceptionErrorMsg(ex)
            End Try
        End Sub

        ''' <summary>
        ''' Generate report using FastReport.
        ''' </summary>
        ''' <param name="reportPath">Frx report file path.</param>
        ''' <param name="output">Output method.</param>
        ''' <param name="printer">Leave empty to use default printer in operating system.</param>
        Public Overridable Sub LoadReport(ByVal reportPath As String, _
                                          ByVal output As Ape.EL.Misc.PrintHelper.OutputType, _
                                          Optional ByVal printer As String = "")
            Using stream As New System.IO.MemoryStream(System.IO.File.ReadAllBytes(reportPath))
                LoadReport(stream, output, printer, reportPath)
            End Using
        End Sub

        ''' <summary>
        ''' Generate report using FastReport.
        ''' </summary>
        ''' <param name="reportStream">Frx report file stream.</param>
        ''' <param name="output">Output method.</param>
        ''' <param name="printer">Leave empty to use default printer in operating system.</param>
        Public Overridable Sub LoadReport(ByRef reportStream As IO.Stream, _
                                          ByVal output As Ape.EL.Misc.PrintHelper.OutputType, _
                                          Optional ByVal printer As String = "", _
                                          Optional ByVal reportPath As String = "")
            If ReportDataSet Is Nothing Then
                Throw New ApplicationException("Please assign ReportDataSet property.")
                Exit Sub
            End If

            'Set file name for export modes
            Dim exportFilePath As String = String.Format("{0}{1}{2}", ExportPath, System.IO.Path.DirectorySeparatorChar, System.IO.Path.GetFileNameWithoutExtension(reportPath))
            Select Case output
                Case Misc.PrintHelper.OutputType.ExportTxt
                    exportFilePath = exportFilePath & ".txt"
                Case Misc.PrintHelper.OutputType.ExportPdf
                    exportFilePath = exportFilePath & ".pdf"
            End Select

            'Create print object and process the report according to the output type
            Try
                Dim rptReport As New FReport(Me)

                rptReport.Load(reportStream)
                rptReport.RegisterData(ReportDataSet)
                rptReport.FileName = reportPath

                For introw = 0 To rptReport.AllObjects.Count - 1
                    If rptReport.AllObjects.Item(introw).Name = "txtReportName" Then
                        Dim toRptName As FastReport.TextObject = rptReport.FindObject("txtReportName")
                        FastReport.Utils.Config.PreviewSettings.Text = toRptName.Text
                    End If
                Next

                If Not printer.IsEmpty Then
                    rptReport.PrintSettings.Printer = printer
                End If

                If rptReport.Parameters.Count > 0 Then
                    For introw = 0 To rptReport.Parameters.Count - 1
                        Select Case rptReport.Parameters(introw).Name
                            Case "paramHeader"
                                If RptHeaderName <> "" Then
                                    rptReport.Parameters(introw).AsString = RptHeaderName
                                End If
                            Case "paramFilter"
                                If RptFilterDate <> "" Then
                                    rptReport.Parameters(introw).AsString = RptFilterDate
                                End If
                        End Select
                    Next
                End If

                Config.ReportSettings.ShowPerformance = True
                Config.ReportSettings.ShowProgress = True

                Select Case output
                    Case Misc.PrintHelper.OutputType.Preview
                        rptReport.PrintSettings.ShowDialog = True
                        If Owner IsNot Nothing AndAlso Owner.IsMdiContainer AndAlso UseMdi Then
                            rptReport.Show(Owner)
                        Else
                            rptReport.Show(UseModal, Owner)
                        End If
                        rptReport.Form = If(rptReport.Preview IsNot Nothing, rptReport.Preview.FindForm, Nothing)

                    Case Misc.PrintHelper.OutputType.Designer
                        rptReport.PrintSettings.ShowDialog = True
                        If Owner IsNot Nothing AndAlso Owner.IsMdiContainer AndAlso UseMdi Then
                            rptReport.Design(Owner)
                        Else
                            rptReport.Design(UseModal)
                        End If
                        rptReport.Form = If(rptReport.Designer IsNot Nothing, rptReport.Designer.FindForm, Nothing)

                    Case Misc.PrintHelper.OutputType.Print
                        'Use original printing mechanism
                        rptReport.PrintSettings.ShowDialog = False
                        Config.ReportSettings.ShowPerformance = False
                        Config.ReportSettings.ShowProgress = False
                        rptReport.PrintSettings.Copies = NoOfCopy
                        rptReport.Print()

                    Case Misc.PrintHelper.OutputType.SelectAndPrint
                        rptReport.PrintSettings.ShowDialog = True
                        Config.ReportSettings.ShowPerformance = False
                        Config.ReportSettings.ShowProgress = False
                        rptReport.PrintSettings.Copies = NoOfCopy
                        rptReport.Print()

                    Case Misc.PrintHelper.OutputType.ExportTxt
                        'Use save Text File / Matrix Printer method to export to text file first then direct print
                        '''''Format to print out in plain text for dot-matrix printer usage
                        Dim DefaultPrinterName As String = GetDefaultPrinter()
                        Dim rptTextExport As New FastReport.Export.Text.TextExport
                        rptTextExport.PageRange = FastReport.PageRange.All
                        rptTextExport.PageBreaks = False
                        rptTextExport.Frames = True
                        rptTextExport.TextFrames = True
                        rptTextExport.Encoding = System.Text.Encoding.Unicode
                        rptTextExport.ScaleX = Ape.EL.Setting.MySetting.GetSettingEx("PRINTER", "FR_DOTMATRIX_SCALEX", "1.31") 'Report file width must be 7.8cm
                        rptTextExport.ScaleY = 100
                        rptTextExport.AvoidDataLoss = False
                        rptTextExport.PrinterName = IIf(String.IsNullOrEmpty(printer), DefaultPrinterName, printer)
                        rptTextExport.PrintAfterExport = True

                        'help bPrintDotMatrix to print out line feed after finish printing data, otherwise the printer will need to feed manually
                        ''Dim oPrintClass As Object = Nothing
                        ''If bPrintDotMatrix = True Then
                        ''    oPrintClass = New PrinterClassDll.Win32Print
                        ''    oPrintClass.SetPrinterName(rptTextExport.PrinterName)
                        ''    oPrintClass.SetDeviceFont("9", "FontB1x1", False, False)
                        ''End If

                        rptTextExport.PrinterName = ""
                        rptTextExport.PrintAfterExport = False
                        Config.ReportSettings.ShowPerformance = False
                        Config.ReportSettings.ShowProgress = False
                        rptReport.Prepare(True)
                        rptReport.Export(rptTextExport, exportFilePath)

                    Case Misc.PrintHelper.OutputType.ExportPdf
                        Dim rptExport As FastReport.Export.Pdf.PDFExport = New FastReport.Export.Pdf.PDFExport()
                        Config.ReportSettings.ShowPerformance = False
                        Config.ReportSettings.ShowProgress = False
                        rptReport.Prepare(True)
                        rptReport.Export(rptExport, exportFilePath)

                End Select

                FinalizeReport(rptReport)
            Catch exFile As FileFormatException
                'Let the caller handle this exception.
                Throw exFile
            Catch ex As Exception
                Ape.EL.WinForm.General.WarningMsg(ex.Message)
            Finally
                'only delete if it's a file.
                If System.IO.File.Exists(exportFilePath) Then System.IO.File.Delete(exportFilePath)
            End Try
        End Sub

        ''' <summary>
        ''' Add new table to ReportDataSet.
        ''' </summary>
        ''' <param name="dt">The new datatable to be added into the dataset.</param>
        ''' <param name="tableName">Leave empty to use dt.TableName or rename it with tableName param.</param>
        Public Overridable Sub AddDataTable(ByVal dt As DataTable, Optional ByVal tableName As String = "")
            If ReportDataSet Is Nothing Then
                ReportDataSet = New DataSet
            End If

            If dt Is Nothing Then
                Exit Sub
            End If

            If tableName.IsEmpty Then
                If dt.TableName.IsEmpty Then
                    Throw New ApplicationException("dt.TableName must not be empty!")
                End If
            Else
                dt.TableName = tableName
            End If

            If ReportDataSet.Tables(dt.TableName) IsNot Nothing Then
                Throw New ApplicationException(String.Format("Table name '{0}' exists. Please use other name.", tableName))
            End If

            ReportDataSet.Tables.Add(dt)
        End Sub

        ''' <summary>
        ''' Add cleaning up code for created FReport object.
        ''' </summary>
        Private Sub FinalizeReport(ByVal report As FReport)
            If report Is Nothing Then Throw New ArgumentNullException("report")

            'Bring the preview forward, add FormClosed handler to clear the report objects, set icon to follow owner
            If report.Form IsNot Nothing Then
                Try
                    report.Form.Location = New Drawing.Point(0, 0)
                    report.Form.WindowState = FormWindowState.Maximized
                    report.Form.BringToFront()
                    AddHandler report.Form.FormClosing, AddressOf ReportForm_FormClosing
                    AddHandler report.Form.FormClosed, AddressOf ReportForm_FormClosed
                Catch ex As Exception
                    Ape.EL.WinForm.General.Trace(ex.Message)
                End Try
            End If

            'Add in disposal list.
            _FastReportObjects.Add(report)
        End Sub
#End Region

#Region "Shared methods"
        ''' <summary>
        ''' Get the current FastReport's report stream.
        ''' </summary>
        Public Shared Function GetFastReportStream(ByVal report As FastReport.Report) As System.IO.MemoryStream
            Dim ms As New System.IO.MemoryStream

            If report IsNot Nothing Then
                report.Save(ms)
            End If

            Return ms
        End Function

        Private Shared Sub AddHandlerDesignerStreamSave()
            AddHandler Config.DesignerSettings.CustomSaveDialog, AddressOf DesignerSettings_CustomSaveDialog
            AddHandler Config.DesignerSettings.CustomSaveReport, AddressOf DesignerSettings_CustomSaveReport
        End Sub

        Private Shared Sub RemoveHandlerDesignerStreamSave()
            RemoveHandler Config.DesignerSettings.CustomSaveDialog, AddressOf DesignerSettings_CustomSaveDialog
            RemoveHandler Config.DesignerSettings.CustomSaveReport, AddressOf DesignerSettings_CustomSaveReport
        End Sub

        Private Shared Sub AddHandlerDesignerStreamLoad()
            AddHandler Config.DesignerSettings.CustomOpenDialog, AddressOf DesignerSettings_CustomOpenDialog
            AddHandler Config.DesignerSettings.CustomOpenReport, AddressOf DesignerSettings_CustomOpenReport
        End Sub

        Private Shared Sub RemoveHandlerDesignerStreamLoad()
            RemoveHandler Config.DesignerSettings.CustomOpenDialog, AddressOf DesignerSettings_CustomOpenDialog
            RemoveHandler Config.DesignerSettings.CustomOpenReport, AddressOf DesignerSettings_CustomOpenReport
        End Sub
#End Region

#Region "Misc initializer"
        Private Shared _InitFastReport As Boolean = InitFastReport()

        ''' <summary>
        ''' This wil force the FastReport.Util.Config.UIStyle changes to take place. I am still wondering what causes it not to apply if the form not shown first (on FastReport 2015.4.7).
        ''' </summary>
        Private Shared Function InitFastReport() As Boolean
            Try
                Using p As New FReport(Nothing)
                    'Create a fastreport object so the environment settings below can be applied.
                    'Dispose the class immediately as it has no further purpose.
                End Using

                'Begin FR environment changes.
                Config.UIStyle = UIStyle.Office2013
                Config.UseRibbon = False
                Config.DesignerSettings.ShowInTaskbar = True
                Config.PreviewSettings.ShowInTaskbar = True
                Config.WebMode = False
            Catch ex As Exception
                'do nothing
            End Try

            Return True
        End Function
#End Region

#Region "Constructor and destructor"
        ''' <summary>
        ''' Keep track of all created fast report object so that we can dispose it safely when it is no longer needed.
        ''' </summary>
        Private Shared _FastReportObjects As New List(Of FReport)

        Public Sub New()
            DisposeRptObject()
        End Sub

        Protected Overrides Sub Dispose(disposing As Boolean)
            DisposeRptObject()

            MyBase.Dispose(disposing)
        End Sub

        ''' <summary>
        ''' Dispose old rptReport objects.
        ''' </summary>
        Private Shared Sub DisposeRptObject()
            If _FastReportObjects IsNot Nothing AndAlso _FastReportObjects.Count = 0 Then Exit Sub

            Dim found As Boolean = False
            For Each f As FReport In _FastReportObjects
                If f.Form IsNot Nothing AndAlso Not f.Form.IsDisposed Then
                    found = True
                    Exit For
                End If
            Next

            If Not found Then
                _FastReportObjects.ClearList(True)
            End If
        End Sub
#End Region

#Region "Events"
        ''' <summary>
        ''' Add handler to FastReport's preview or designer form's FormClosing event.
        ''' </summary>
        Private Sub ReportForm_FormClosing(sender As Object, e As FormClosingEventArgs)
            Dim f As Form = DirectCast(sender, Form)
            If f IsNot Nothing AndAlso f.Owner IsNot Nothing Then
                f.Hide()
                If UseBringFront Then f.Owner.BringToFront()
            Else
                If Owner IsNot Nothing Then
                    If UseBringFront Then Owner.BringToFront()
                End If
            End If
        End Sub

        ''' <summary>
        ''' Add handler to FastReport's preview or designer form's FormClosed event.
        ''' </summary>
        Private Sub ReportForm_FormClosed(sender As Object, e As FormClosedEventArgs)
            DisposeRptObject()
        End Sub
#End Region

#Region "Shared events"
        ''' <summary>
        ''' This event is fired when the user press the "Open file" button.
        ''' </summary>
        Private Shared Sub DesignerSettings_CustomOpenDialog(sender As Object, e As OpenSaveDialogEventArgs)
            If e.Designer.Report.GetType Is GetType(FReport) Then
                Dim rpt As FReport = DirectCast(e.Designer.Report, FReport)
                If rpt.OwnerObject IsNot Nothing Then
                    rpt.OwnerObject.RaiseCustomOpenDialog(rpt.GetReportData, e)
                End If
            End If
        End Sub

        ''' <summary>
        ''' This event is fired when report needs to be loaded.
        ''' </summary>
        Private Shared Sub DesignerSettings_CustomOpenReport(sender As Object, e As OpenSaveReportEventArgs)
            If e.Report.GetType Is GetType(FReport) Then
                Dim rpt As FReport = DirectCast(e.Report, FReport)
                If rpt.OwnerObject IsNot Nothing Then
                    rpt.OwnerObject.RaiseCustomOpenReport(rpt.GetReportData, e)
                End If
            End If
        End Sub

        ''' <summary>
        ''' This event is fired when the user press the "Save file" button to save untitled report, or "Save file as" button.
        ''' </summary>
        Private Shared Sub DesignerSettings_CustomSaveDialog(sender As Object, e As OpenSaveDialogEventArgs)
            If e.Designer.Report.GetType Is GetType(FReport) Then
                Dim rpt As FReport = DirectCast(e.Designer.Report, FReport)
                If rpt.OwnerObject IsNot Nothing Then
                    rpt.OwnerObject.RaiseCustomSaveDialog(rpt.GetReportData, e)
                End If
            End If
        End Sub

        ''' <summary>
        ''' This event is fired when report needs to be saved.
        ''' </summary>
        Private Shared Sub DesignerSettings_CustomSaveReport(sender As Object, e As OpenSaveReportEventArgs)
            If e.Report.GetType Is GetType(FReport) Then
                Dim rpt As FReport = DirectCast(e.Report, FReport)
                If rpt.OwnerObject IsNot Nothing Then
                    rpt.OwnerObject.RaiseCustomSaveReport(rpt.GetReportData, e)
                End If
            End If
        End Sub
#End Region
    End Class
End Namespace
