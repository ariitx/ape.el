Imports Ape.EL.Localization

Namespace Devices
    Public Class WeighMachine
        '        Private ReadOnly lic As Boolean = Internal.CheckMyLicense

        <LocalizableString()>
        Public Enum WmType
            <DefaultString("CAS SW-1S (S-SEND)")>
            CAS_SW_1S
            <DefaultString("CAS SW-1C (TYPE-6)")>
            CAS_SW_1C
        End Enum
    End Class
End Namespace
