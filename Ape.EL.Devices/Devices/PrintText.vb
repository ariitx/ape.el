﻿Imports Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6
Imports System.Drawing.Printing
Imports System.Drawing
Imports System.Windows.Forms

Namespace Devices
    ''' <summary>
    ''' Wraps Printer6.dll functions into .net class.
    ''' Provide basic text printing methods.
    ''' Printer6.dll is based on Bixolon SRP-350 VB6 PrinterClassDll.Win32Print source code, thus you can use FontControl and so on.
    ''' To print out unicode character, you have to change your OS's system locale that uses the specific font. Eg. to print chinese, use Chinese (PRC).
    ''' </summary>
    Public Class PrintText
        Inherits Devices.Printer

#Region "Nested types"
        Public Enum ControlCode
            LF 'new line
            Left 'move to left
            Right 'move to right
            Center 'move to center
            Cut 'cut the paper
        End Enum

        Public Class LineString
            Private _Properties As Repository.RepositoryPrintTextCmd

            Public Sub New()
                _Properties = New Repository.RepositoryPrintTextCmd
                _Font = _Properties.DefaultFontName
                _Size = _Properties.DefaultFontSize
            End Sub

            Public Sub New(ByVal properties As Repository.RepositoryPrintTextCmd)
                Me.New()
                If properties IsNot Nothing Then
                    _Properties = properties
                End If
            End Sub

            Public Sub New(ByVal properties As Repository.RepositoryPrintTextCmd, ByVal text As String)
                Me.New(properties)
                _Text = text
            End Sub

            Public Sub New(ByVal text As String)
                Me.New(Nothing, text)
            End Sub

            Public Sub New(ByVal control As ControlCode)
                Me.New()
                Select Case control
                    Case ControlCode.LF
                        _Text = _Properties.LF
                    Case Else
                        _Font = "FontControl"
                        Select Case control
                            Case ControlCode.Left
                                _Text = _Properties.Left
                            Case ControlCode.Right
                                _Text = _Properties.Right
                            Case ControlCode.Center
                                _Text = _Properties.Center
                            Case ControlCode.Cut
                                _Text = _Properties.Cut
                        End Select
                End Select
            End Sub

            ''' <summary>
            ''' Print out order when left in a list.
            ''' </summary>
            Property Order As Integer
                Get
                    Return _Order
                End Get
                Set(value As Integer)
                    _Order = value
                End Set
            End Property
            Private _Order As Integer

            ''' <summary>
            ''' Text to print. Empty text will return linefeed instead.
            ''' </summary>
            Property Text As String
                Get
                    Return If(_Text.IsEmpty, _Properties.LF, _Text)
                End Get
                Set(value As String)
                    _Text = value
                End Set
            End Property
            Private _Text As String

            ''' <summary>
            ''' Font name.
            ''' </summary>
            Property Font As String
                Get
                    Return If(_Font.IsEmpty, _Properties.DefaultFontName, _Font)
                End Get
                Set(value As String)
                    _Font = value
                End Set
            End Property
            Private _Font As String

            ''' <summary>
            ''' Font print out size.
            ''' </summary>
            Property Size As Single
                Get
                    Return _Size
                End Get
                Set(value As Single)
                    _Size = value
                End Set
            End Property
            Private _Size As Single

            Property UseBold As Boolean
                Get
                    Return _UseBold
                End Get
                Set(value As Boolean)
                    _UseBold = value
                End Set
            End Property
            Private _UseBold As Boolean

            ''' <summary>
            ''' Use distinct colour other than black.
            ''' </summary>
            Property UseColour As Boolean
                Get
                    Return _UseColour
                End Get
                Set(value As Boolean)
                    _UseColour = value
                End Set
            End Property
            Private _UseColour As Boolean

            ''' <summary>
            ''' A comment for this object.
            ''' </summary>
            Property Remark As String
                Get
                    Return _Remark
                End Get
                Set(value As String)
                    _Remark = value
                End Set
            End Property
            Private _Remark As String
        End Class
#End Region

#Region "Properties"
        ''' <summary>
        ''' Set default raw printing commands.
        ''' </summary>
        Public ReadOnly Property Properties As Repository.RepositoryPrintTextCmd
            Get
                Return _Properties
            End Get
        End Property
        Private _Properties As New Repository.RepositoryPrintTextCmd

        ''' <summary>
        ''' Get or set option to print out alignment control code instead of manually align it.
        ''' </summary>
        Property PrintAlignControlCode As Boolean
            Get
                Return Printer6.PrintAlignControlCode
            End Get
            Set(value As Boolean)
                Printer6.PrintAlignControlCode = value
            End Set
        End Property
#End Region

#Region "Methods"
        Public Sub New()
            If Printer6 Is Nothing Then
                Try
                    Printer6 = New Printer6.Win32Print
                Catch exCom As Runtime.InteropServices.COMException
                    'Handle interop exception.
                    Throw exCom
                Catch ex As Exception
                    Throw ex
                End Try
            End If
        End Sub

        ''' <summary>
        ''' Set the printer name to print on.
        ''' </summary>
        Public Sub SetPrinterName(ByVal PrinterName As String)
            Printer6.SetPrinterName(PrinterName)
        End Sub

        ''' <summary>
        ''' Sets default font setting to the printer. 
        ''' Set command to false.
        ''' </summary>
        Public Sub SetDeviceFont(ByVal FontSize As Single, ByVal FontName As String, ByVal BoldType As Boolean, ByVal FontColor As Boolean)
            Printer6.SetDeviceFont(FontSize, FontName, BoldType, FontColor)

            If Printer6.GetFontName <> FontName Then
                Ape.EL.WinForm.General.Trace("Font not same, current font : " & Printer6.GetFontName)
            End If
        End Sub

        ''' <summary>
        ''' Stops the printing on the current page and resumes printing on a new page.
        ''' </summary>
        Public Sub NewPage()
            Printer6.NewPage()
        End Sub

        ''' <summary>
        ''' Ends a print operation sent to the Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer object, releasing the document to the print device or spooler.
        ''' </summary>
        Public Sub EndDoc()
            Printer6.EndDoc()
        End Sub

        ''' <summary>
        ''' Prints text to a page.
        ''' </summary>
        Public Sub PrintText(ByVal Text As String)
            Printer6.PrintText(Text)
        End Sub

        ''' <summary>
        ''' Prints image from file to a page.
        ''' </summary>
        Public Sub PrintImage(ByVal FileName As String)
            Printer6.PrintImage(FileName)
        End Sub

        ''' <summary>
        ''' Prints text to a page based on a LineString.
        ''' </summary>
        Public Sub PrintText(ByVal ls As LineString)
            SetDeviceFont(ls.Size, ls.Font, ls.UseBold, ls.UseColour)
            PrintText(ls.Text)
        End Sub
#End Region

#Region "Shared fields"
        Protected Shared Printer6 As Printer6.Win32Print
#End Region

#Region "Shared methods"
        Public Shared Sub Print(ByVal strPrinter As String, ByVal strText As String)
            Try
                Using oPrint As New PrintText
                    If strPrinter.IsEmpty Then
                        strPrinter = oPrint.GetDefaultPrinter()
                    End If

                    oPrint.SetPrinterName(strPrinter)
                    oPrint.SetDeviceFont(oPrint.Properties.DefaultFontSize, oPrint.Properties.DefaultFontName, False, False)
                    oPrint.PrintText(strText)
                    oPrint.EndDoc()
                End Using
            Catch ex As Exception
                Ape.EL.WinForm.General.WarningMsg(ex.Message)
            End Try
        End Sub

        ''' <summary>
        ''' Print out list of LineString and translate them to printing method.
        ''' </summary>
        ''' <param name="strPrinter"></param>
        ''' <param name="listLine"></param>
        ''' <param name="path"></param>
        ''' <remarks></remarks>
        Public Shared Sub Print(ByVal strPrinter As String, ByVal listLine As List(Of LineString), Optional ByVal path As String = "")
            If listLine Is Nothing Then
                Throw New ArgumentNullException("listLine")
            End If

            Try
                Using oPrint As New PrintText
                    If strPrinter.IsEmpty Then
                        strPrinter = oPrint.GetDefaultPrinter
                    End If

                    oPrint.PrintAlignControlCode = False
                    oPrint.SetPrinterName(strPrinter)

                    For Each ls As LineString In listLine
                        oPrint.PrintText(ls)
                    Next

                    oPrint.EndDoc()
                End Using
            Catch ex As Exception
                Ape.EL.WinForm.General.WarningMsg(ex.Message)
            End Try
        End Sub
#End Region
    End Class
End Namespace
