﻿Imports Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6
Imports System.Drawing.Printing
Imports System.Drawing
Imports System.Windows.Forms
Imports Ape.EL.Devices.PrintText

Namespace Devices
    ''' <summary>
    ''' Provide basic text printing methods with alignment.
    ''' Based on Bixolon SRP-350 VB6 PrinterClassDll.WIn32Print source code.
    ''' Does not support FontControl.
    ''' Requires Microsoft.VisualBasic.PowerPacks.Vs 9.0 in reference.
    ''' </summary>
    Friend Class PrintText2
        Inherits Devices.Printer

#Region "Nested types"
        Public Class LineString2
            Inherits Devices.PrintText.LineString

            ''' <summary>
            ''' Indicate that the line string contains a command.
            ''' Set command value in Text property and each command is in decimal separated by comma.
            ''' </summary>
            Property IsCommand As Boolean
                Get
                    Return _IsCommand
                End Get
                Set(value As Boolean)
                    _IsCommand = value
                End Set
            End Property
            Private _IsCommand As Boolean
        End Class
#End Region

#Region "Fields"
        ''' <summary> Set value in SetDeviceFont. </summary>
        Private myFontName As String
        ''' <summary> Set value in SetDeviceFont. </summary>
        Private myFontSize As Single
        ''' <summary> Set value in SetUseCommand. </summary>
        Private myIsCommand As Boolean
#End Region

#Region "Properties"
        ''' <summary>
        ''' Set default raw printing commands.
        ''' </summary>
        Public ReadOnly Property Properties As Repository.RepositoryPrintTextCmd
            Get
                Return _Properties
            End Get
        End Property
        Private _Properties As New Repository.RepositoryPrintTextCmd

        ''' <summary>
        ''' Print out alignment control code instead of manually align it.
        ''' </summary>
        Property PrintAlignControlCode As Boolean
            Get
                Return _PrintAlignControlCode
            End Get
            Set(value As Boolean)
                _PrintAlignControlCode = value
            End Set
        End Property
        Private _PrintAlignControlCode As Boolean

        ''' <summary>
        ''' Set value to make PrintText method output on a file instead to printer.
        ''' </summary>
        Property PrintFilePath As String
            Get
                Return _PrintFilePath
            End Get
            Set(value As String)
                _PrintFilePath = value
            End Set
        End Property
        Private _PrintFilePath As String
#End Region

#Region "Methods"
        ''' <summary>
        ''' Set the printer name to print on.
        ''' </summary>
        Public Sub SetPrinterName(ByVal PrinterName As String)
            Dim prnPrinter As Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer

            For Each prnPrinter In Printers
                If prnPrinter.DeviceName = PrinterName Then
                    Printer = prnPrinter
                    Exit For
                End If
            Next

            If Not PrintFilePath.IsEmpty Then
                Printer.PrintFileName = String.Format("{0}{1}{2}.eps", PrintFilePath, System.IO.Path.DirectorySeparatorChar, Printer.DocumentName)
                Printer.PrintAction = PrintAction.PrintToFile
            End If
        End Sub

        ''' <summary>
        ''' Sets default font setting to the printer. 
        ''' Set command to false.
        ''' </summary>
        Public Sub SetDeviceFont(ByVal FontSize As Single, ByVal FontName As String, ByVal BoldType As Boolean, ByVal FontColor As Boolean)
            Printer.FontSize = FontSize
            Printer.FontName = FontName
            Printer.FontBold = BoldType

            '[0] to be used in PrintText
            myFontSize = FontSize
            myFontName = FontName

            If FontColor Then
                Printer.ForeColor = RGB(255, 0, 0)
            Else
                Printer.ForeColor = RGB(0, 0, 0)
            End If

            myIsCommand = False

            If Printer.FontName <> FontName Then
                Ape.EL.WinForm.General.Trace("Font not same, current font : " & Printer.FontName)
            End If
        End Sub

        ''' <summary>
        ''' Make all printable text assumed as printer commands when print. 
        ''' It will reset to false after NewPage or EndDoc or SetDeviceFont is executed.
        ''' </summary>
        Public Sub SetUseCommand()
            myIsCommand = True
        End Sub

        ''' <summary>
        ''' Stops the printing on the current page and resumes printing on a new page.
        ''' Set command to false.
        ''' </summary>
        Public Sub NewPage()
            Printer.NewPage()
            myIsCommand = False
        End Sub

        ''' <summary>
        ''' Ends a print operation sent to the Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer object, releasing the document to the print device or spooler.
        ''' Set command to false.
        ''' </summary>
        Public Sub EndDoc()
            Printer.EndDoc()

            myIsCommand = False

            Threading.Thread.Sleep(100)
            Devices.RawPrint.SendCutCommand(Printer.DeviceName)
        End Sub

        ''' <summary>
        ''' Prints text to a page.
        ''' </summary>
        Public Sub PrintText(ByVal Text As String)
            Static alignment As String

            Dim canPrint As Boolean = False
            If myIsCommand AndAlso Not PrintAlignControlCode Then
                'Change the alignment through setting the Printer.CurrentX
                Select Case UCase(Text)
                    Case UCase(Properties.Left), UCase(Properties.Center), UCase(Properties.Right)
                        alignment = UCase(Text)
                    Case UCase(Properties.Cut)
                    Case Else
                        'Print out text other than alignment code
                        canPrint = True
                End Select
            Else
                canPrint = True
            End If

            Select Case UCase(alignment)
                Case UCase(Properties.Left)
                    Printer.CurrentX = 0
                Case UCase(Properties.Center)
                    Printer.CurrentX = (Printer.ScaleWidth - Printer.TextWidth(Text)) \ 2
                Case UCase(Properties.Right)
                    Printer.CurrentX = Printer.ScaleWidth - Printer.TextWidth(Text)
                Case Else
                    Printer.CurrentX = 0
            End Select

            If canPrint Then
                Printer.Print(Text) 'print normally when it's truetype font
            End If
        End Sub

        ''' <summary>
        ''' Prints image from file to a page.
        ''' </summary>
        Public Sub PrintImage(ByVal FileName As String)
            Dim pic As System.Drawing.Bitmap

            pic = New System.Drawing.Bitmap(FileName)

            Printer.PaintPicture(pic, Printer.CurrentX, Printer.CurrentY)
            Printer.CurrentY = Printer.CurrentY + pic.Height
        End Sub
#End Region

#Region "Shared fields"
        Public Shared Printer As Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer
#End Region

#Region "Shared methods"
        Public Shared Sub Print(ByVal strPrinter As String, ByVal strText As String)
            Try
                Using oPrint As New PrintText2
                    If strPrinter.IsEmpty Then
                        strPrinter = oPrint.GetDefaultPrinter()
                    End If

                    oPrint.SetPrinterName(strPrinter)
                    oPrint.SetDeviceFont(oPrint.Properties.DefaultFontSize, oPrint.Properties.DefaultFontName, False, False)
                    oPrint.PrintText(strText)
                    oPrint.EndDoc()
                End Using
            Catch ex As Exception
                Ape.EL.WinForm.General.WarningMsg(ex.Message)
            End Try
        End Sub

        Public Shared Sub Print(ByVal strPrinter As String, ByVal listLine As List(Of LineString2), Optional ByVal path As String = "")
            If listLine Is Nothing Then
                Throw New ArgumentNullException("listLine")
            End If

            Try
                Using oPrint As New PrintText2
                    If strPrinter.IsEmpty Then
                        strPrinter = oPrint.GetDefaultPrinter
                    End If

                    If System.IO.Directory.Exists(path) Then oPrint.PrintFilePath = path

                    oPrint.SetPrinterName(strPrinter)

                    For Each ls As LineString2 In listLine
                        oPrint.SetDeviceFont(ls.Size, ls.Font, ls.UseBold, ls.UseColour)
                        If ls.IsCommand Then oPrint.SetUseCommand()
                        oPrint.PrintText(ls.Text)
                    Next

                    oPrint.EndDoc()
                End Using
            Catch ex As Exception
                Ape.EL.WinForm.General.WarningMsg(ex.Message)
            End Try
        End Sub
#End Region
    End Class
End Namespace
