Imports Ape.EL.Localization

Namespace Devices
    Public Class FingerPrint
        '        Private ReadOnly lic As Boolean = Internal.CheckMyLicense

        <LocalizableString()>
        Public Enum FpType
            <DefaultString("IRIS BCR200DTP")>
            IRIS_BCR200DTP
            <DefaultString("ZKFinger")>
            ZKFinger
        End Enum
    End Class
End Namespace
