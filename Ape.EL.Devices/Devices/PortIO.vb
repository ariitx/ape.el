﻿Imports System
Imports System.Globalization
Imports System.IO
Imports System.Runtime.InteropServices
Imports System.Text

Namespace Devices
    Public Class PortIO
        Implements IDisposable


        Private Structure DCB
            Public dcbLength As Integer

            Public baudRate As Integer

            Public bitfield As Integer

            Public wReserved As Short

            Public xonLim As Short

            Public xoffLim As Short

            Public byteSize As Byte

            Public parity As Byte

            Public stopBits As Byte

            Public xonChar As Byte

            Public xoffChar As Byte

            Public errorChar As Byte

            Public eofChar As Byte

            Public evtChar As Byte

            Public wReserved1 As Short
        End Structure

        Private Class NativeMethods
            Private Sub New()
            End Sub

            Friend Declare Function CreateFile Lib "kernel32.dll" (lpFileName As String, dwDesiredAccess As UInteger, dwShareMode As UInteger, lpSecurityAttributes As IntPtr, dwCreationDisposition As UInteger, dwFlagsAndAttributes As UInteger, hTemplateFile As IntPtr) As IntPtr

            Friend Declare Function CloseHandle Lib "kernel32.dll" (hObject As IntPtr) As <MarshalAs(UnmanagedType.Bool)>
            Boolean

            Friend Declare Function ReadFile Lib "kernel32.dll" (hFile As IntPtr, lpBuffer As Byte(), nNumberOfBytesToRead As UInteger, <Out()> ByRef lpNumberOfBytesRead As UInteger, lpOverlapped As IntPtr) As <MarshalAs(UnmanagedType.Bool)>
            Boolean

            Friend Declare Function WriteFile Lib "kernel32.dll" (hFile As IntPtr, lpBuffer As Byte(), nNumberOfBytesToWrite As UInteger, <Out()> ByRef lpNumberOfBytesWritten As UInteger, lpOverlapped As IntPtr) As <MarshalAs(UnmanagedType.Bool)>
            Boolean

            Friend Declare Function GetCommState Lib "kernel32.dll" (hFile As IntPtr, ByRef lpDCB As PortIO.DCB) As <MarshalAs(UnmanagedType.Bool)>
            Boolean

            Friend Declare Function SetCommState Lib "kernel32.dll" (hFile As IntPtr, ByRef lpDCB As PortIO.DCB) As <MarshalAs(UnmanagedType.Bool)>
            Boolean

            Friend Declare Function GetCommModemStatus Lib "kernel32.dll" (hFile As IntPtr, ByRef lpModemStat As Integer) As <MarshalAs(UnmanagedType.Bool)>
            Boolean
        End Class

        Private Const GENERIC_READ As UInteger = 2147483648

        Private Const GENERIC_WRITE As UInteger = 1073741824

        Private Const OPEN_EXISTING As UInteger = 3

        Private Const INVALID_HANDLE_VALUE As Integer = -1

        Private myHandle As IntPtr

        Private myEncoding As Encoding = Encoding.ASCII

        Private myPortName As String

        Private mySerialSettings As Devices.SerialSettings

        Public ReadOnly Property IsOpen() As Boolean
            Get
                Return Me.myHandle <> IntPtr.Zero
            End Get
        End Property

        Public Property Encoding() As Encoding
            Get
                Return Me.myEncoding
            End Get
            Set(value As Encoding)
                If value Is Nothing Then
                    Throw New ArgumentNullException("value")
                End If
                Me.myEncoding = value
            End Set
        End Property

        Public Property PortName() As String
            Get
                Return Me.myPortName
            End Get
            Set(value As String)
                Me.myPortName = value
            End Set
        End Property

        Public Property SerialSettings() As Devices.SerialSettings
            Get
                Return Me.mySerialSettings
            End Get
            Set(value As Devices.SerialSettings)
                Me.mySerialSettings = value
            End Set
        End Property

        Public ReadOnly Property CDHolding() As Boolean
            Get
                Return (128 And Me.GetStatus()) <> 0
            End Get
        End Property

        Public ReadOnly Property CtsHolding() As Boolean
            Get
                Return (16 And Me.GetStatus()) <> 0
            End Get
        End Property

        Public ReadOnly Property DsrHolding() As Boolean
            Get
                Return (32 And Me.GetStatus()) <> 0
            End Get
        End Property

        Public Sub New(portName As String)
            If portName Is Nothing Then
                Throw New ArgumentNullException("portName")
            End If
            Me.PortName = portName
            Me.Open()
        End Sub

        Public Sub New(setting As Devices.SerialSettings)
            If setting Is Nothing Then
                Throw New ArgumentNullException("setting")
            End If
            Me.SerialSettings = setting
            Me.Open()
        End Sub

        Shadows Sub Finalize()
            Me.Dispose(False)
        End Sub

        Public Sub Dispose() Implements IDisposable.Dispose
            Me.Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub

        Private Sub Dispose(disposing As Boolean)
            Try
                Me.Close2(False)
            Catch ex_09 As Exception
            End Try
        End Sub

        Private Function GetStatus() As Integer
            If Not Me.IsOpen Then
                Throw New InvalidOperationException("Port is not open.")
            End If
            Dim result As Integer = 0
            If Not PortIO.NativeMethods.GetCommModemStatus(Me.myHandle, result) Then
                Throw New IOException("GetCommModemStatus failed.")
            End If
            Return result
        End Function

        Private Sub Close2(throwExceptionIfFails As Boolean)
            If Me.IsOpen Then
                Dim flag As Boolean = PortIO.NativeMethods.CloseHandle(Me.myHandle)
                Me.myHandle = IntPtr.Zero
                If Not flag AndAlso throwExceptionIfFails Then
                    Throw New IOException("Failed to close port.")
                End If
            End If
        End Sub

        Public Sub Open()
            If Me.IsOpen Then
                Throw New InvalidOperationException("Port is open.")
            End If
            Dim text As String = If((Me.SerialSettings IsNot Nothing), Me.SerialSettings.PortName, Me.PortName)
            If Not text.EndsWith(":") Then
                text += ":"
            End If
            Me.myHandle = PortIO.NativeMethods.CreateFile(text, 3221225472, 0, IntPtr.Zero, 3, 0, IntPtr.Zero)
            If Me.myHandle = CType((-1), IntPtr) Then
                Me.myHandle = IntPtr.Zero
                Throw New IOException("Failed to open port: " + Me.PortName)
            End If
            If Me.SerialSettings IsNot Nothing Then
                Dim dCB As PortIO.DCB = Nothing
                If Not PortIO.NativeMethods.GetCommState(Me.myHandle, dCB) Then
                    Throw New IOException("GetCommState failed.")
                End If
                dCB.baudRate = Me.SerialSettings.BaudRate
                dCB.byteSize = CByte(Me.SerialSettings.DataBits)
                dCB.parity = CByte(Me.SerialSettings.Parity)
                dCB.stopBits = CByte(Me.SerialSettings.StopBits)
                If Not PortIO.NativeMethods.SetCommState(Me.myHandle, dCB) Then
                    Throw New IOException("SetCommState failed.")
                End If
            End If
        End Sub

        Public Sub Close()
            Me.Close2(True)
        End Sub

        Public Function Read(buffer As Byte()) As Integer
            If buffer Is Nothing Then
                Throw New ArgumentNullException("buffer")
            End If
            If Not Me.IsOpen Then
                Throw New InvalidOperationException("Port is not open.")
            End If
            Dim result As UInteger
            If Not PortIO.NativeMethods.ReadFile(Me.myHandle, buffer, CUInt(buffer.Length), result, IntPtr.Zero) Then
                Throw New IOException("Failed to read data from port.")
            End If
            Return CInt(result)
        End Function

        Public Function Read() As Byte
            Dim array As Byte() = New Byte(1 - 1) {}
            Me.Read(array)
            Return array(0)
        End Function

        Public Sub Write(buffer As Byte())
            If buffer Is Nothing Then
                Throw New ArgumentNullException("buffer")
            End If
            If Not Me.IsOpen Then
                Throw New InvalidOperationException("Port is not open.")
            End If
            Dim num As UInteger
            If Not PortIO.NativeMethods.WriteFile(Me.myHandle, buffer, CUInt(buffer.Length), num, IntPtr.Zero) Then
                Throw New IOException("Failed to write data to port.")
            End If
        End Sub

        Public Sub Write(str As String)
            If str Is Nothing Then
                Throw New ArgumentNullException("str")
            End If
            Me.Write(Me.Encoding.GetBytes(str))
        End Sub

        Public Shared Function IsLptPort(portName As String) As Boolean
            If portName Is Nothing Then
                Throw New ArgumentNullException("portName")
            End If
            Dim result As Boolean = False
            Dim text As String = portName.ToLower(CultureInfo.InvariantCulture)
            If text.StartsWith("lpt") Then
                Dim num As Integer = If(text.EndsWith(":"), (text.Length - 1), text.Length)
                If num - 3 > 0 Then
                    Dim flag As Boolean = True
                    For i As Integer = 3 To num - 1
                        If text(i) < "0" OrElse text(i) > "9" Then
                            flag = False
                            Exit For
                        End If
                    Next
                    If flag Then
                        result = True
                    End If
                End If
            End If
            Return result
        End Function

    End Class
End Namespace
