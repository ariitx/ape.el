﻿Imports System.Runtime.InteropServices
Imports System.IO
Imports System.Drawing.Printing

Namespace Devices
    Public Class Drawer
        Implements IDisposable
        '        Private ReadOnly lic As Boolean = Internal.CheckMyLicense

        Private WithEvents SerialPort_CashDrawer As System.IO.Ports.SerialPort

        Public Sub Dispose() Implements IDisposable.Dispose
            If SerialPort_CashDrawer IsNot Nothing Then
                SerialPort_CashDrawer.Dispose()
                SerialPort_CashDrawer = Nothing
            End If
        End Sub
        ''' <summary>
        ''' Open connected drawer using this routine. Must set ConnectionType property first.
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub OpenDrawer()
            Try
                If ConnectionType = DrawerType.None Then
                    Throw New Exception("Please set ConnectionType property.")
                End If

                Select Case _ConnectionType
                    Case DrawerType.SerialPort 'Eject From Serial Port
                        SerialPort_CashDrawer = New System.IO.Ports.SerialPort() 'initialize serial port
                        SerialPort_CashDrawer.PortName = "Com" & DW_Port
                        SerialPort_CashDrawer.BaudRate = DW_BAUDRATE
                        SerialPort_CashDrawer.DataBits = DW_START_BIT
                        SerialPort_CashDrawer.StopBits = DW_STOP_BIT

                        Select Case DW_PARITY
                            Case "N"
                                SerialPort_CashDrawer.Parity = Ports.Parity.None
                            Case "E"
                                SerialPort_CashDrawer.Parity = Ports.Parity.Even
                            Case "M"
                                SerialPort_CashDrawer.Parity = Ports.Parity.Mark
                            Case "O"
                                SerialPort_CashDrawer.Parity = Ports.Parity.Odd
                            Case "S"
                                SerialPort_CashDrawer.Parity = Ports.Parity.Space
                            Case Else
                                SerialPort_CashDrawer.Parity = Ports.Parity.None
                        End Select

                        Select Case DW_Handshake
                            Case 0
                                SerialPort_CashDrawer.Handshake = IO.Ports.Handshake.None
                            Case 1
                                SerialPort_CashDrawer.Handshake = IO.Ports.Handshake.RequestToSend
                            Case 2
                                SerialPort_CashDrawer.Handshake = IO.Ports.Handshake.RequestToSendXOnXOff
                            Case 3
                                SerialPort_CashDrawer.Handshake = IO.Ports.Handshake.XOnXOff
                            Case Else
                                SerialPort_CashDrawer.Handshake = IO.Ports.Handshake.RequestToSend
                        End Select

                        If SerialPort_CashDrawer.IsOpen Then
                            SerialPort_CashDrawer.Close()
                        End If
                        SerialPort_CashDrawer.WriteTimeout = 1000
                        SerialPort_CashDrawer.ReadTimeout = 1000
                        SerialPort_CashDrawer.Open()
                        SerialPort_CashDrawer.Write(Chr(DW_EJECT_ASCII))
                        SerialPort_CashDrawer.Close()

                    Case DrawerType.USB
                        'USB CONNECTION
                        InitFRIUSBLibrary()
                        OpenFRIDoor()
                        CloseFRIUSBLibrary()

                    Case DrawerType.PrinterKickOut
                        If String.IsNullOrEmpty(PrinterName) Then
                            Throw New Exception("Please set PrinterName property.")
                        End If
                        'RJ 11 Connection
                        'Dim oPrintClass As New PrinterClassDll.Win32Print

                        ' oPrintClass.SetPrinterName(p_strReceiptPrinter)
                        ' oPrintClass.SetDeviceFont(9, p_strDW_FONT, False, False)
                        'oPrintClass.PrintText(p_strDW_CHR)
                        ' oPrintClass.EndDoc()

                        If UsePrinterCommand = True Then
                            'RJ 11 Connection - For Star Printer

                            Dim oPrintClass As New Devices.PrintText

                            oPrintClass.SetPrinterName(PrinterName)
                            oPrintClass.SetDeviceFont(9, DW_FONT, False, False)
                            oPrintClass.PrintText(DW_CHR)
                            oPrintClass.EndDoc()

                        Else
                            Dim strPrintString As String

                            strPrintString = Chr(&H1B) + "p" + Chr(&H0) + Chr(&H64) + Chr(&H64)

                            Devices.RawPrint.SendCommandToPrinter(PrinterName, strPrintString)
                        End If
                End Select
            Catch ex As Exception
                Throw New Exception("Open drawer failure.", ex)
            End Try
        End Sub

        Enum DrawerType
            None = -1
            USB = 0
            SerialPort = 1
            PrinterKickOut = 2
        End Enum

        Private _ConnectionType As DrawerType = DrawerType.None
        Public Property ConnectionType() As DrawerType
            Get
                Return _ConnectionType
            End Get
            Set(ByVal value As DrawerType)
                _ConnectionType = value
            End Set
        End Property

#Region "SerialPort Setting Property"
        Private _strDW_Port As String = "1"
        ''' <summary>
        ''' SerialPort setting.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property DW_Port() As String
            Get
                Return _strDW_Port
            End Get
            Set(ByVal value As String)
                _strDW_Port = value
            End Set
        End Property

        Private _strDW_BAUDRATE As String = "9600"
        ''' <summary>
        ''' SerialPort setting.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property DW_BAUDRATE() As String
            Get
                Return _strDW_BAUDRATE
            End Get
            Set(ByVal value As String)
                _strDW_BAUDRATE = value
            End Set
        End Property

        Private _strDW_START_BIT As String = "8"
        ''' <summary>
        ''' SerialPort setting.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property DW_START_BIT() As String
            Get
                Return _strDW_START_BIT
            End Get
            Set(ByVal value As String)
                _strDW_START_BIT = value
            End Set
        End Property

        Private _strDW_STOP_BIT As String = "1"
        ''' <summary>
        ''' SerialPort setting.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property DW_STOP_BIT() As String
            Get
                Return _strDW_STOP_BIT
            End Get
            Set(ByVal value As String)
                _strDW_STOP_BIT = value
            End Set
        End Property

        Private _strDW_PARITY As String = "N"
        ''' <summary>
        ''' SerialPort setting.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property DW_PARITY() As String
            Get
                Return _strDW_PARITY
            End Get
            Set(ByVal value As String)
                _strDW_PARITY = value
            End Set
        End Property

        Private _intDW_Handshake As Integer = -1
        ''' <summary>
        ''' SerialPort setting.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property DW_Handshake() As Integer
            Get
                Return _intDW_Handshake
            End Get
            Set(ByVal value As Integer)
                _intDW_Handshake = value
            End Set
        End Property

        Private _strDW_EJECT_ASCII As String = "13"
        ''' <summary>
        ''' SerialPort setting. Set eject ASCII code, default is 13.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property DW_EJECT_ASCII() As String
            Get
                Return _strDW_EJECT_ASCII
            End Get
            Set(ByVal value As String)
                _strDW_EJECT_ASCII = value
            End Set
        End Property
#End Region

#Region "USB Setting Property"
        <DllImport("FIRIDLLU.dll")> _
        Private Shared Sub InitFRIUSBLibrary()
        End Sub

        <DllImport("FIRIDLLU.dll")> _
        Private Shared Sub CloseFRIUSBLibrary()
        End Sub

        <DllImport("FIRIDLLU.dll")> _
        Private Shared Sub OpenFRIDoor()
        End Sub
#End Region

#Region "PrinterKickOut Setting Property"
        Private _bUsePrinterCommand As Boolean
        ''' <summary>
        ''' PrinterKickOut setting.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property UsePrinterCommand() As Boolean
            Get
                Return _bUsePrinterCommand
            End Get
            Set(ByVal value As Boolean)
                _bUsePrinterCommand = value
            End Set
        End Property

        Private _strPrinterName As String
        ''' <summary>
        ''' PrinterKickOut setting.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property PrinterName() As String
            Get
                Return _strPrinterName
            End Get
            Set(ByVal value As String)
                _strPrinterName = value
            End Set
        End Property

        Public _strDW_FONT As String = "FontControl"
        ''' <summary>
        ''' PrinterKickOut setting. Default value is "FontControl"
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property DW_FONT() As String
            Get
                Return _strDW_FONT
            End Get
            Set(ByVal value As String)
                _strDW_FONT = value
            End Set
        End Property

        Private _strDW_CHR As String = "A"
        ''' <summary>
        ''' PrinterKickOut setting. Default value is "A"
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property DW_CHR() As String
            Get
                Return _strDW_CHR
            End Get
            Set(ByVal value As String)
                _strDW_CHR = value
            End Set
        End Property
#End Region
    End Class
End Namespace