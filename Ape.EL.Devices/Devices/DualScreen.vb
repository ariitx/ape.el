Namespace Devices
    Public Class DualScreen
        '        Private ReadOnly lic As Boolean = Internal.CheckMyLicense

        ''' <summary>
        ''' Default delimeter for dual screen file paths setting.
        ''' </summary>
        Public Const STR_Dm As String = Chr(9) 'or ControlChars.Tab

        ''' <summary>
        ''' Get a single string of file paths, concatenated by a delimiter.
        ''' </summary>
        Public Shared Function BuildFilePath(ByVal strPaths() As String) As String
            Dim sb As New System.Text.StringBuilder

            For Each path As String In strPaths
                If Not String.IsNullOrEmpty(sb.ToString) Then sb.Append(STR_Dm)
                sb.Append(path)
            Next

            Return sb.ToString
        End Function

        ''' <summary>
        ''' Get file paths from concatenated string with a delimeter.
        ''' </summary>
        Public Shared Function GetFilePaths(ByVal strPath As String) As String()
            Dim tmpDSPs() As String = strPath.Split(STR_Dm)
            If tmpDSPs.Count = 1 Then
                If String.IsNullOrEmpty(tmpDSPs(0)) Then
                    tmpDSPs = New String() {}
                End If
            End If
            Return tmpDSPs
        End Function

    End Class
End Namespace