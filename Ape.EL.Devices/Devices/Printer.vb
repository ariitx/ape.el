﻿Imports System.Drawing.Printing

Namespace Devices
    Public Class Printer
        Implements IDisposable

        ''' <summary>
        ''' Get list of printer names.
        ''' </summary>
        Public Function GetPrinterList() As List(Of String)
            Dim list As New List(Of String)
            For Each strInstalledPrinters In PrinterSettings.InstalledPrinters
                list.Add(strInstalledPrinters)
            Next
            Return list
        End Function

        ''' <summary>
        ''' Get default printer name registered in the system.
        ''' </summary>
        Public Function GetDefaultPrinter() As String
            Dim oPS As New System.Drawing.Printing.PrinterSettings
            Dim DefaultPrinterName As String
            Try
                DefaultPrinterName = oPS.PrinterName
            Catch ex As System.Exception
                DefaultPrinterName = ""
            Finally
                oPS = Nothing
            End Try
            Return DefaultPrinterName
        End Function

        Protected Overridable Function GetAlternatePrinter() As String
            Return String.Empty
        End Function

        Protected Overridable Function LoadAlternatePrinter() As Boolean
            Return False
        End Function

#Region "IDisposable Support"
        Private disposedValue As Boolean ' To detect redundant calls

        ' IDisposable
        Protected Overridable Sub Dispose(disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                    ' TODO: dispose managed state (managed objects).
                End If

                ' TODO: free unmanaged resources (unmanaged objects) and override Finalize() below.
                ' TODO: set large fields to null.
            End If
            Me.disposedValue = True
        End Sub

        ' TODO: override Finalize() only if Dispose(ByVal disposing As Boolean) above has code to free unmanaged resources.
        'Protected Overrides Sub Finalize()
        '    ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        '    Dispose(False)
        '    MyBase.Finalize()
        'End Sub

        ' This code added by Visual Basic to correctly implement the disposable pattern.
        Public Sub Dispose() Implements IDisposable.Dispose
            ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub
#End Region
    End Class
End Namespace
