Imports Ape.EL.Localization

Namespace Devices
    Public Class PaymentTerminal
        '        Private ReadOnly lic As Boolean = Internal.CheckMyLicense

        <LocalizableString()>
        Public Enum TrmDeviceType
            <DefaultString("Attended")>
            Attended
            <DefaultString("Unattended")>
            Unattended
        End Enum

        <LocalizableString()>
        Public Enum TrmLogFlag
            <DefaultString("No")>
            No
            <DefaultString("Yes")>
            Yes
        End Enum
    End Class
End Namespace
