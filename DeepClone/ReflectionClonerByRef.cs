﻿#region CPL License
/*
Nuclex Framework
Copyright (C) 2002-2012 Nuclex Development Labs

This library is free software; you can redistribute it and/or
modify it under the terms of the IBM Common Public License as
published by the IBM Corporation; either version 1.0 of the
License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
IBM Common Public License for more details.

You should have received a copy of the IBM Common Public
License along with this library
*/
#endregion

using System;
using System.Reflection;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace DeepClone {

  /// <summary>Clones objects using reflection</summary>
  /// <remarks>
  ///   <para>
  ///     This type of cloning is a lot faster than cloning by serialization and
  ///     incurs no set-up cost, but requires cloned types to provide a default
  ///     constructor in order to work.
  ///   </para>
  /// </remarks>
  public class ReflectionClonerByRef {

    /// <summary>
    ///   Creates a deep clone of the specified object, also creating clones of all
    ///   child objects being referenced By Reference. It will create the object if it's null.
    /// </summary>
    /// <param name="target"></param>
    /// <typeparam name="TCloned">Type of the object that will be cloned</typeparam>
    /// <param name="objectToClone">Object that will be cloned</param>
    /// <returns>A deep clone of the provided object</returns>
    public static void DeepFieldClone<TCloned>(ref object target, object objectToClone) {
      object objectToCloneAsObject = objectToClone;
      if(objectToClone == null) {
        target = default(TCloned);
        return;
      } else {
        deepCloneSingleFieldBased(ref target, objectToCloneAsObject);
        return;
      }
    }

    /// <summary>Copies a single object using field-based value transfer</summary>
    /// <param name="target"></param>
    /// <param name="original">Original object that will be cloned</param>
    /// <returns>A clone of the original object</returns>
    private static void deepCloneSingleFieldBased(ref object target, object original)
    {
        Type originalType = original.GetType();
        if (originalType.IsPrimitive || (originalType == typeof(string)))
        {
            target = original; // Creates another box, does not reference boxed primitive
            return;
        }
        else if (originalType.IsArray)
        {
            deepCloneArrayFieldBased(ref target, (Array)original, originalType.GetElementType());
            return;
        }
        else
        {
            deepCloneComplexFieldBased(ref target, original);
            return;
        }
    }

    /// <summary>Clones a complex type using field-based value transfer</summary>
    /// <param name="target"></param>
    /// <param name="original">Original instance that will be cloned</param>
    /// <returns>A clone of the original instance</returns>
    private static void deepCloneComplexFieldBased(ref object target, object original)
    {
        Type originalType = original.GetType();

        if (target == null)
        {
            target = FormatterServices.GetUninitializedObject(originalType);
        }

        FieldInfo[] fieldInfosArray = ClonerHelpers.GetFieldInfosIncludingBaseClasses(
          originalType, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance
        );
        FieldInfo[] fieldInfosArrayTarget = ClonerHelpers.GetFieldInfosIncludingBaseClasses(
          target.GetType(), BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance
        );

        List<FieldInfo> fieldInfos = new List<FieldInfo>();
        fieldInfos.AddRange(fieldInfosArray);

        List<FieldInfo> fieldInfosTarget = new List<FieldInfo>();
        fieldInfosTarget.AddRange(fieldInfosArrayTarget);

        for (int index = 0; index < fieldInfos.Count; ++index)
        {
            FieldInfo fieldInfo = fieldInfos[index];
            Type fieldType = fieldInfo.FieldType;
            object originalValue = fieldInfo.GetValue(original);
            if (originalValue != null && fieldInfosTarget.Contains(fieldInfo))
            {
                // Primitive types can be assigned directly
                if (fieldType.IsPrimitive || (fieldType == typeof(string)))
                {
                    fieldInfo.SetValue(target, originalValue);
                }
                else if (fieldType.IsArray)
                { // Arrays need to be cloned element-by-element
                    object t = null;
                    deepCloneArrayFieldBased(ref t, (Array)originalValue, fieldType.GetElementType());
                    fieldInfo.SetValue(target, t);
                }
                else
                { // Complex types need to be cloned member-by-member
                    object t = null;
                    deepCloneSingleFieldBased(ref t, originalValue);
                    fieldInfo.SetValue(target, t);
                }
            }
        }

        return;
    }

    /// <summary>Clones an array using field-based value transfer</summary>
    /// <param name="target"></param>
    /// <param name="original">Original array that will be cloned</param>
    /// <param name="elementType">Type of elements the original array contains</param>
    /// <returns>A clone of the original array</returns>
    private static void deepCloneArrayFieldBased(ref object target, Array original, Type elementType)
    {
        if (elementType.IsPrimitive || (elementType == typeof(string)))
        {
            target = original.Clone();
            return;
        }

        int dimensionCount = original.Rank;

        // Find out the length of each of the array's dimensions, also calculate how
        // many elements there are in the array in total.
        var lengths = new int[dimensionCount];
        int totalElementCount = 0;
        for (int index = 0; index < dimensionCount; ++index)
        {
            lengths[index] = original.GetLength(index);
            if (index == 0)
            {
                totalElementCount = lengths[index];
            }
            else
            {
                totalElementCount *= lengths[index];
            }
        }

        // Knowing the number of dimensions and the length of each dimension, we can
        // create another array of the exact same sizes.
        Array clone = Array.CreateInstance(elementType, lengths);

        // If this is a one-dimensional array (most common type), do an optimized copy
        // directly specifying the indices
        if (dimensionCount == 1)
        {

            // Clone each element of the array directly
            for (int index = 0; index < totalElementCount; ++index)
            {
                object originalElement = original.GetValue(index);
                if (originalElement != null)
                {
                    object t = null;
                    deepCloneSingleFieldBased(ref t, originalElement);
                    clone.SetValue(t, index);
                }
            }

        }
        else
        { // Otherwise use the generic code for multi-dimensional arrays

            var indices = new int[dimensionCount];
            for (int index = 0; index < totalElementCount; ++index)
            {

                // Determine the index for each of the array's dimensions
                int elementIndex = index;
                for (int dimensionIndex = dimensionCount - 1; dimensionIndex >= 0; --dimensionIndex)
                {
                    indices[dimensionIndex] = elementIndex % lengths[dimensionIndex];
                    elementIndex /= lengths[dimensionIndex];
                }

                // Clone the current array element
                object originalElement = original.GetValue(indices);
                if (originalElement != null)
                {
                    object t = null;
                    deepCloneSingleFieldBased(ref t, originalElement);
                    clone.SetValue(t, indices);
                }

            }

        }

        target = clone;
        return;
    }

  }

} // namespace Nuclex.Support.Cloning
