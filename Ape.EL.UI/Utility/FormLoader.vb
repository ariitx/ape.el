﻿Imports System.Windows.Forms
Imports System.Reflection

Namespace Utility
    ''' <summary>
    ''' This class provides a utility to load forms from loaded assemblies.
    ''' </summary>
    ''' <remarks></remarks>
    Public Class FormLoader
#Region "Properties"
        ''' <summary>
        ''' Get the last loaded module object.
        ''' </summary>
        Shared ReadOnly Property LastLoadedModule As Form
            Get
                Return _LastLoadedModule
            End Get
        End Property
        Private Shared _LastLoadedModule As Form = Nothing

        ''' <summary>
        ''' Contains list of assembly public key tokens to skip, so performance is increased when loading GlobalClassesFullName.
        ''' </summary>
        Shared Property SkippedPublicKeyToken As New List(Of String)

        ''' <summary>
        ''' Contains list of assemblies to be loaded into GlobalClassesFullName. Leave empty to use all loaded assemblies.
        ''' </summary>
        Shared ReadOnly Property MyAssemblies As List(Of Assembly)
            Get
                Static lastResult As List(Of Assembly)
                Static lastResultHash As String
                Static lastHash As String
                Dim tmpHash As String = lastHash
                Dim assemblies As List(Of Assembly) = AppDomain.CurrentDomain.GetAssemblies.ToList
                lastHash = Ape.EL.Utility.Data.MD5HashGenerator.GenerateKey(String.Join("-", (From x As Assembly In assemblies Select x.FullName).ToArray))

                If lastResult IsNot Nothing Then
                    Dim tmpResultHash As String = Ape.EL.Utility.Data.MD5HashGenerator.GenerateKey(String.Join("-", (From x As Assembly In lastResult Select x.FullName).ToArray))
                    If lastResultHash = tmpResultHash AndAlso lastHash = tmpHash Then
                        'Use existing result if list of currently loaded assemblies haven't changed.
                        Return lastResult
                    End If
                End If

                'Remove unnecessary assemblies by skippable tokens.
                Dim skippedToken As List(Of String) = SkippedPublicKeyToken 'If assembly contain following token, then skip them.
                If skippedToken Is Nothing Then skippedToken = New List(Of String)
                skippedToken.Add("B77A5C561934E089") '.Net
                skippedToken.Add("B03F5F7F11D50A3A") '.Net
                skippedToken.Add("31BF3856AD364E35") '.Net
                assemblies.RemoveAll(Function(x) skippedToken.Contains(x.GetPublicKeyToken, StringComparer.OrdinalIgnoreCase) OrElse x.GlobalAssemblyCache)

                'Remove unnecessary assemblies by finding this project's referenced DLL.
                Dim asm As Assembly = Assembly.GetExecutingAssembly
                Dim curAsm As List(Of Assembly) = Ape.EL.Misc.AssemblyHelper.GetReferencedAssemblies(asm.GetName)
                assemblies.RemoveAll(Function(x) curAsm.Contains(x))

                lastResult = assemblies
                lastResultHash = Ape.EL.Utility.Data.MD5HashGenerator.GenerateKey(String.Join("-", (From x As Assembly In lastResult Select x.FullName).ToArray))

                Return lastResult
            End Get
        End Property

        ''' <summary>
        ''' Get or set address of a routine to be called when loading form started.
        ''' </summary>
        Shared Property MyOnStatusBusyShow As Action(Of String)

        ''' <summary>
        ''' Get or set address of a routine to be called when loading form finished.
        ''' </summary>
        Shared Property MyOnStatusBusyHide As Action

        ''' <summary>
        ''' Get or set address of a routine to be called before form Show or ShowDialog is called.
        ''' </summary>
        Shared Property MyOnFormShowing As Action(Of Object)

        ''' <summary>
        ''' Get or set address of a routine to be called after form Show event is raised.
        ''' </summary>
        Shared Property MyOnFormShown As Action(Of Object)

        Shared Property MyBeforeFormShowingFullScreen As Action(Of Object)
#End Region

#Region "Fields"
        Private m_Owner As Form
#End Region

#Region "Methods"
        Public Sub New(ByVal owner As Form)
            m_Owner = owner
        End Sub

        ''' <summary>
        ''' Try to load module from reflection.
        ''' </summary>
        ''' <param name="strFormName">FullName</param>
        ''' <remarks></remarks>
        <System.Diagnostics.DebuggerStepThrough()> _
        Sub LoadModule(ByVal strFormName As String, _
                       Optional ByVal bFullScreen As Boolean = False, _
                       Optional ByVal bNew As Boolean = False, _
                       Optional ByVal bDialog As Boolean = False, _
                       Optional ByVal bMdi As Boolean = True)
            'If only provided the form name without namespace, we try to find it's namespace form GlobalClassesFullName.
            If Not strFormName.Contains(".") Then
                Dim formFullName As String = GlobalClassesFullName.Where(Function(x) x.EndsWith(String.Format(".{0}", strFormName), StringComparison.OrdinalIgnoreCase)).FirstOrDefault
                If Not formFullName.IsEmpty Then
                    strFormName = formFullName
                End If
            End If

            Dim frm As Form = Nothing
            Dim strNewFormName As String = strFormName

            Try
                If Not String.IsNullOrEmpty(strNewFormName) Then
                    'Get existing form from MdiChildren collection.
                    If m_Owner IsNot Nothing Then frm = (From x As Form In m_Owner.MdiChildren Select x Where Ape.EL.Misc.AssemblyHelper.FindType(strNewFormName).IsAssignableFrom(x.GetType)).FirstOrDefault

                    'If not opened in MdiChildren, create a new instance.
                    If frm Is Nothing AndAlso Ape.EL.Misc.AssemblyHelper.FindType(strNewFormName) IsNot Nothing Then frm = DirectCast(Ape.EL.Misc.AssemblyHelper.FindType(strNewFormName).Assembly.CreateInstance(strNewFormName), Form)

                    'Load the frm.
                    LoadModule(frm, bFullScreen, bNew, bDialog, bMdi)
                End If
            Catch ex As Exception
                Ape.EL.WinForm.General.ExceptionErrorMsg(ex)
            End Try
        End Sub

        ''' <summary>
        ''' Load module from main screen.
        ''' </summary>
        ''' <param name="frm">The form to be shown.</param>
        ''' <param name="bFullScreen">False by default. Show the form in full screen mode.</param>
        ''' <param name="bNew">False by default. Show the form in a new mdi tab. If bFullScreen is true, this parameter will not take effect.</param>
        ''' <remarks></remarks>
        <System.Diagnostics.DebuggerStepThrough()> _
        Sub LoadModule(ByVal frm As Form, _
                       Optional ByVal bFullScreen As Boolean = False, _
                       Optional ByVal bNew As Boolean = False, _
                       Optional ByVal bDialog As Boolean = False, _
                       Optional ByVal bMdi As Boolean = True)
            'The two conditions when a form is not disposed on Close is when 
            '(1) it is part of a multiple-document interface (MDI) application, 
            '    and the form is not visible; and 
            '(2) you have displayed the form using ShowDialog.
            'In these cases, you will need to call Dispose manually to mark all 
            '    of the form's controls for garbage collection.

            Dim strBusyMessage As String = If(frm IsNot Nothing, "Loading " & frm.Text & " to screen...", "")
            Dim frmNew As Form = Nothing

            Try
                If frm Is Nothing Then
                    Throw New ApplicationException(String.Format("Form doesn't exist. Try to reinstall the {0}.", Ape.EL.Utility.General.GetProgramName))
                End If

                Debug.Assert(Not frm.Name.IsEmpty, "LoadModule(frm as Form,...) frm.Name is empty.")
                If String.IsNullOrEmpty(frm.Name) Then frm.Name = frm.Text

                Dim strNewFormName As String
                strNewFormName = frm.GetType.FullName

                If bDialog Then
                    StatusBusyShow(strBusyMessage)
                    frmNew = DirectCast(frm.GetType.Assembly.CreateInstance(strNewFormName), Form)
                    frmNew.StartPosition = FormStartPosition.CenterParent
                    StatusBusyHide()
                    frmNew.Icon = m_Owner.Icon
                    FormShowing(frmNew)
                    frmNew.ShowDialog(m_Owner)
                    frmNew.Dispose()
                Else
                    If bFullScreen Then
                        'Full screen will not load into MDI
                        StatusBusyShow(strBusyMessage)
                        frmNew = frm
                        'frmNew = DirectCast(System.Reflection.Assembly.GetEntryAssembly.CreateInstance(strNewFormName), Form)
                        frmNew.Icon = m_Owner.Icon
                        BeforeFormShowingFullScreen(frmNew)
                        AddHandler frmNew.Shown, Sub() m_Owner.Hide()
                        AddHandler frmNew.FormClosed, Sub()
                                                          m_Owner.Show()
                                                          If frmNew IsNot Nothing AndAlso Not frmNew.IsDisposed Then frmNew.Dispose()
                                                      End Sub
                        StatusBusyHide()
                        FormShowing(frmNew)
                        frmNew.Show()
                        'it will be disposed when Close is called.
                    Else
                        StatusBusyShow(strBusyMessage)
                        'MDI screen, show main form when it is not visible
                        If Not m_Owner.Visible Then
                            'control for when the main screen is not Program.AppMainForm.
                            m_Owner.Visible = True
                            m_Owner.Focus()
                        End If

                        'MDI screens, if bNew param is set then make it as new form
                        If bNew Then
                            frm = DirectCast(frm.GetType.Assembly.CreateInstance(strNewFormName), Form)
                        End If

                        AddHandler frm.FormClosed, Sub() If frm IsNot Nothing AndAlso Not frm.IsDisposed Then frm.Dispose()
                        If bMdi Then frm.MdiParent = m_Owner
                        frm.Icon = m_Owner.Icon
                        If bMdi Then frm.AutoScroll = True
                        FormShowing(frm)
                        frm.Show()
                        frm.Focus()
                        StatusBusyHide()
                    End If
                End If

                'Set the last loaded form into _LastLoadedModule.
                If frmNew IsNot Nothing AndAlso Not frmNew.IsDisposed Then
                    _LastLoadedModule = frmNew
                ElseIf frm IsNot Nothing AndAlso Not frm.IsDisposed Then
                    _LastLoadedModule = frm
                Else
                    _LastLoadedModule = Nothing
                End If
            Catch ex As Exception
                Ape.EL.WinForm.General.ExceptionErrorMsg(ex)
                Try 'Make sure the form is closed if failed to initialize the form
                    If frmNew IsNot Nothing AndAlso Not frmNew.IsDisposed Then
                        frmNew.Close()
                        frmNew.Dispose()
                    End If
                    If frm IsNot Nothing AndAlso Not frm.IsDisposed Then
                        frm.Close()
                        frm.Dispose()
                    End If
                Catch ex1 As Exception
                End Try
            End Try
        End Sub

        ''' <summary>
        ''' Contains list of names of classes from loaded assemblies.
        ''' </summary>
        Private Function GlobalClassesFullName() As List(Of String)
            Static str As List(Of String) = Nothing

            If str Is Nothing Then
                str = New List(Of String)
                Dim assemblies As List(Of Assembly) = MyAssemblies
                If assemblies Is Nothing Then assemblies = AppDomain.CurrentDomain.GetAssemblies.ToList
                For i As Integer = 0 To assemblies.Count - 1
                    Dim assembly As Assembly = assemblies(i)
                    str.AddRange(Ape.EL.Utility.General.GetClasses(assembly))
                Next
            End If

            Return str
        End Function

        Private Sub StatusBusyShow(ByVal str As String)
            If MyOnStatusBusyShow IsNot Nothing Then
                MyOnStatusBusyShow.Invoke(str)
            End If
        End Sub

        Private Sub StatusBusyHide()
            If MyOnStatusBusyHide IsNot Nothing Then
                MyOnStatusBusyHide.Invoke()
            End If
        End Sub

        Private Sub FormShowing(sender As Object)
            If MyOnFormShowing IsNot Nothing Then
                MyOnFormShowing.Invoke(sender)
            End If
        End Sub

        Private Sub BeforeFormShowingFullScreen(sender As Object)
            If MyBeforeFormShowingFullScreen IsNot Nothing Then
                MyBeforeFormShowingFullScreen.Invoke(sender)
            End If
        End Sub
#End Region
    End Class
End Namespace
