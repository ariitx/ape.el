﻿Imports Ape.EL.WinForm.General
Imports System.ComponentModel
Imports System.Windows.Forms
Imports System.Drawing
Imports Ape.EL.Data.DbManager

Namespace Component.Misc
    Public Class DatabaseControl
        Inherits Ape.EL.UI.Component.BaseUserControl

#Region "Windows Form Designer Generated Code"
        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.sbDbTestConnection = New System.Windows.Forms.Button()
            Me.pnlDbSecurity = New System.Windows.Forms.Panel()
            Me.lbDbUser = New System.Windows.Forms.Label()
            Me.txtDbPassword = New System.Windows.Forms.TextBox()
            Me.lbDbPass = New System.Windows.Forms.Label()
            Me.txtDbUsername = New System.Windows.Forms.TextBox()
            Me.txtDbServerName = New System.Windows.Forms.TextBox()
            Me.ceDbIntegratedSecurity = New System.Windows.Forms.CheckBox()
            Me.lbDbServerName = New System.Windows.Forms.Label()
            Me.txtDescription = New System.Windows.Forms.TextBox()
            Me.txtDbCatalogue = New System.Windows.Forms.TextBox()
            Me.lbDescription = New System.Windows.Forms.Label()
            Me.lbDbCatalogue = New System.Windows.Forms.Label()
            Me.pnlDbSecurity.SuspendLayout()
            Me.SuspendLayout()
            '
            'sbDbTestConnection
            '
            Me.sbDbTestConnection.Location = New System.Drawing.Point(3, 196)
            Me.sbDbTestConnection.Name = "sbDbTestConnection"
            Me.sbDbTestConnection.Size = New System.Drawing.Size(145, 26)
            Me.sbDbTestConnection.TabIndex = 13
            Me.sbDbTestConnection.Text = "Test Connection"
            '
            'pnlDbSecurity
            '
            Me.pnlDbSecurity.AutoSize = True
            Me.pnlDbSecurity.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
            Me.pnlDbSecurity.Controls.Add(Me.lbDbUser)
            Me.pnlDbSecurity.Controls.Add(Me.txtDbPassword)
            Me.pnlDbSecurity.Controls.Add(Me.lbDbPass)
            Me.pnlDbSecurity.Controls.Add(Me.txtDbUsername)
            Me.pnlDbSecurity.Location = New System.Drawing.Point(18, 127)
            Me.pnlDbSecurity.Name = "pnlDbSecurity"
            Me.pnlDbSecurity.Size = New System.Drawing.Size(413, 57)
            Me.pnlDbSecurity.TabIndex = 24
            '
            'lbDbUser
            '
            Me.lbDbUser.Location = New System.Drawing.Point(3, 3)
            Me.lbDbUser.Name = "lbDbUser"
            Me.lbDbUser.Size = New System.Drawing.Size(71, 19)
            Me.lbDbUser.TabIndex = 0
            Me.lbDbUser.Text = "Username"
            '
            'txtDbPassword
            '
            Me.txtDbPassword.Location = New System.Drawing.Point(160, 34)
            Me.txtDbPassword.Name = "txtDbPassword"
            Me.txtDbPassword.Size = New System.Drawing.Size(250, 20)
            Me.txtDbPassword.TabIndex = 3
            '
            'lbDbPass
            '
            Me.lbDbPass.Location = New System.Drawing.Point(3, 35)
            Me.lbDbPass.Name = "lbDbPass"
            Me.lbDbPass.Size = New System.Drawing.Size(67, 19)
            Me.lbDbPass.TabIndex = 2
            Me.lbDbPass.Text = "Password"
            '
            'txtDbUsername
            '
            Me.txtDbUsername.Location = New System.Drawing.Point(160, 2)
            Me.txtDbUsername.Name = "txtDbUsername"
            Me.txtDbUsername.Size = New System.Drawing.Size(250, 20)
            Me.txtDbUsername.TabIndex = 1
            '
            'txtDbServerName
            '
            Me.txtDbServerName.Location = New System.Drawing.Point(178, 66)
            Me.txtDbServerName.Name = "txtDbServerName"
            Me.txtDbServerName.Size = New System.Drawing.Size(250, 20)
            Me.txtDbServerName.TabIndex = 22
            '
            'ceDbIntegratedSecurity
            '
            Me.ceDbIntegratedSecurity.Location = New System.Drawing.Point(3, 98)
            Me.ceDbIntegratedSecurity.Name = "ceDbIntegratedSecurity"
            Me.ceDbIntegratedSecurity.Size = New System.Drawing.Size(425, 23)
            Me.ceDbIntegratedSecurity.TabIndex = 23
            Me.ceDbIntegratedSecurity.Text = "Enable Integrated Security"
            '
            'lbDbServerName
            '
            Me.lbDbServerName.Location = New System.Drawing.Point(3, 67)
            Me.lbDbServerName.Name = "lbDbServerName"
            Me.lbDbServerName.Size = New System.Drawing.Size(91, 19)
            Me.lbDbServerName.TabIndex = 21
            Me.lbDbServerName.Text = "Server Name"
            '
            'txtDescription
            '
            Me.txtDescription.Location = New System.Drawing.Point(178, 2)
            Me.txtDescription.Name = "txtDescription"
            Me.txtDescription.Size = New System.Drawing.Size(250, 20)
            Me.txtDescription.TabIndex = 29
            '
            'txtDbCatalogue
            '
            Me.txtDbCatalogue.Location = New System.Drawing.Point(178, 34)
            Me.txtDbCatalogue.Name = "txtDbCatalogue"
            Me.txtDbCatalogue.Size = New System.Drawing.Size(250, 20)
            Me.txtDbCatalogue.TabIndex = 26
            '
            'lbDescription
            '
            Me.lbDescription.Location = New System.Drawing.Point(3, 3)
            Me.lbDescription.Name = "lbDescription"
            Me.lbDescription.Size = New System.Drawing.Size(79, 19)
            Me.lbDescription.TabIndex = 27
            Me.lbDescription.Text = "Description"
            '
            'lbDbCatalogue
            '
            Me.lbDbCatalogue.Location = New System.Drawing.Point(3, 37)
            Me.lbDbCatalogue.Name = "lbDbCatalogue"
            Me.lbDbCatalogue.Size = New System.Drawing.Size(70, 19)
            Me.lbDbCatalogue.TabIndex = 25
            Me.lbDbCatalogue.Text = "Catalogue"
            '
            'DatabaseControl
            '
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
            Me.Controls.Add(Me.txtDescription)
            Me.Controls.Add(Me.txtDbCatalogue)
            Me.Controls.Add(Me.lbDescription)
            Me.Controls.Add(Me.lbDbCatalogue)
            Me.Controls.Add(Me.pnlDbSecurity)
            Me.Controls.Add(Me.txtDbServerName)
            Me.Controls.Add(Me.ceDbIntegratedSecurity)
            Me.Controls.Add(Me.lbDbServerName)
            Me.Controls.Add(Me.sbDbTestConnection)
            Me.Name = "DatabaseControl"
            Me.Size = New System.Drawing.Size(441, 225)
            Me.pnlDbSecurity.ResumeLayout(False)
            Me.pnlDbSecurity.PerformLayout()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Private WithEvents lbDbUser As System.Windows.Forms.Label
        Private WithEvents txtDbPassword As System.Windows.Forms.TextBox
        Private WithEvents lbDbPass As System.Windows.Forms.Label
        Private WithEvents txtDbUsername As System.Windows.Forms.TextBox
        Private WithEvents txtDescription As System.Windows.Forms.TextBox
        Private WithEvents txtDbCatalogue As System.Windows.Forms.TextBox
        Private WithEvents lbDescription As System.Windows.Forms.Label
        Private WithEvents lbDbCatalogue As System.Windows.Forms.Label
        Private WithEvents sbDbTestConnection As System.Windows.Forms.Button
        Private WithEvents pnlDbSecurity As System.Windows.Forms.Panel
        Private WithEvents txtDbServerName As System.Windows.Forms.TextBox
        Private WithEvents ceDbIntegratedSecurity As System.Windows.Forms.CheckBox
        Private WithEvents lbDbServerName As System.Windows.Forms.Label
#End Region

#Region "Codes behind"
        ''' <summary>
        ''' A class to remember the initial controls' sizes.
        ''' </summary>
        Private Class ctlSize
            Public mySize As New Size(0, 0)
            Public myLocation As New Point(0, 0)
            Public myCtl As Control = Nothing
        End Class

        Private aryOriCtlSize As New List(Of ctlSize)

        ''' <summary>
        ''' Set connect button, which can be triggered when enter key is pressed on available textboxes.
        ''' </summary>
        <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
        WriteOnly Property ConnectButton As Button
            Set(value As Button)
                _ConnectButton = value
            End Set
        End Property
        Private _ConnectButton As Button

        ''' <summary>
        ''' Extract ConnectionInfo from control.
        ''' </summary>
        <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
        Property MyConnectionInfo As ConnectionInfo
            Get
                Dim ci As New ConnectionInfo
                ci.ServerName = txtDbServerName.Text
                ci.Description = txtDescription.Text
                ci.Catalogue = txtDbCatalogue.Text
                ci.IntegratedSecurity = ceDbIntegratedSecurity.Checked
                ci.Username = IIf(ceDbIntegratedSecurity.Checked, "", txtDbUsername.Text)
                ci.Password = IIf(ceDbIntegratedSecurity.Checked, "", txtDbPassword.Text)
                Return ci
            End Get
            Set(value As ConnectionInfo)
                ApplyConnectionInfo(value)
            End Set
        End Property

        ''' <summary>
        ''' Set default catalogue using this property.
        ''' </summary>
        <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
        WriteOnly Property Catalogue As String
            Set(value As String)
                txtDbCatalogue.Text = value
            End Set
        End Property

        <DefaultValue(True), Browsable(True), Category("DatabaseControl"), Description("Show or hide Description field.")>
        Property ShowDescription As Boolean
            Get
                Return lbDescription.Enabled
            End Get
            Set(value As Boolean)
                lbDescription.Visible = value
                lbDescription.Enabled = value
                txtDescription.Visible = value
                txtDescription.Enabled = value
                'ShowHistory = value
                ArrangeControlPosition()
            End Set
        End Property

        <DefaultValue(True), Browsable(True), Category("DatabaseControl"), Description("Show or hide catalogue.")>
        Property ShowCatalogue As Boolean
            Get
                Return lbDbCatalogue.Enabled
            End Get
            Set(value As Boolean)
                lbDbCatalogue.Visible = value
                lbDbCatalogue.Enabled = value
                txtDbCatalogue.Visible = value
                txtDbCatalogue.Enabled = value
                ArrangeControlPosition()
            End Set
        End Property

        <DefaultValue(True), Browsable(True), Category("DatabaseControl"), Description("Show or hide Test Connection button.")>
        Property ShowTestConnection As Boolean
            Get
                Return sbDbTestConnection.Enabled
            End Get
            Set(value As Boolean)
                sbDbTestConnection.Visible = value
                sbDbTestConnection.Enabled = value
                ArrangeControlPosition()
            End Set
        End Property

        ''' <summary>
        ''' Set value to enable changing the selected ConnectionInfo's details.
        ''' </summary>
        WriteOnly Property OptionsEditable As Boolean
            Set(value As Boolean)
                lbDbCatalogue.Enabled = value
                txtDbCatalogue.Enabled = value
                lbDbServerName.Enabled = value
                txtDbServerName.Enabled = value
                ceDbIntegratedSecurity.Enabled = value
                pnlDbSecurity.Enabled = value
            End Set
        End Property

        Private Sub ArrangeControlPosition()
            Static UpdatingControl As Boolean
            If UpdatingControl Then Exit Sub
            UpdatingControl = True

            If aryOriCtlSize.Count > 0 Then
                Ape.EL.UI.Extensions.SuspendDrawing(Me)

                'reset position to original
                Dim h As Integer = 0
                For Each m As ctlSize In aryOriCtlSize
                    If m.myCtl IsNot Nothing AndAlso Me.Controls.Contains(m.myCtl) Then
                        m.myCtl.Location = m.myLocation
                        If m.myLocation.Y + m.mySize.Height > h Then h = m.myLocation.Y + m.mySize.Height
                    End If
                Next
                If Not Ape.EL.Utility.General.IsRuntime Then Me.Height = h

                If Not ShowDescription Then 'And Not ShowHistory Then
                    For Each ctl As Control In Me.Controls
                        Select Case ctl.Name
                            Case lbDescription.Name, txtDescription.Name ', lueHistory.Name
                            Case Else
                                ctl.Top -= (txtDescription.Height + txtDescription.Margin.Top + txtDescription.Margin.Bottom)
                        End Select
                    Next
                    If Not Ape.EL.Utility.General.IsRuntime Then Me.Height -= (txtDescription.Height + txtDescription.Margin.Top + txtDescription.Margin.Bottom)
                End If

                If Not ShowCatalogue Then
                    For Each ctl As Control In Me.Controls
                        Select Case ctl.Name ', lueHistory.Name
                            Case lbDescription.Name, txtDescription.Name, _
                                    lbDbCatalogue.Name, txtDbCatalogue.Name
                            Case Else
                                ctl.Top -= (txtDbCatalogue.Height + txtDbCatalogue.Margin.Top + txtDbCatalogue.Margin.Bottom)
                        End Select
                    Next
                    If Not Ape.EL.Utility.General.IsRuntime Then Me.Height -= (txtDbCatalogue.Height + txtDbCatalogue.Margin.Top + txtDbCatalogue.Margin.Bottom)
                End If

                If Not ShowTestConnection Then
                    If Not Ape.EL.Utility.General.IsRuntime Then Me.Height -= (sbDbTestConnection.Height + sbDbTestConnection.Margin.Top + sbDbTestConnection.Margin.Bottom)
                End If

                Ape.EL.UI.Extensions.ResumeDrawing(Me, True)
            End If

            UpdatingControl = False
        End Sub

        Private Sub ApplyConnectionInfo(ci As ConnectionInfo)
            If ci IsNot Nothing Then
                txtDescription.Text = ci.Description
                txtDbCatalogue.Text = ci.Catalogue
                txtDbServerName.Text = ci.ServerName
                ceDbIntegratedSecurity.Checked = ci.IntegratedSecurity
                txtDbUsername.Text = ci.Username
                txtDbPassword.Text = ci.Password
            End If
        End Sub

        ''' <summary>
        ''' Clear connection info from UI.
        ''' </summary>
        Sub ClearConnectionInfo()
            If IsHandleCreated Then
                txtDescription.Text = Nothing
                txtDbCatalogue.Text = Nothing
                txtDbServerName.Text = Nothing
                ceDbIntegratedSecurity.Checked = Nothing
                txtDbUsername.Text = Nothing
                txtDbPassword.Text = Nothing
            End If
        End Sub

        Function TestConnection(Optional ByVal WithMessage As Boolean = False) As Boolean
            Dim result As Boolean

            result = Ape.EL.Data.DbUtility.TestConnection(txtDbServerName.Text, ceDbIntegratedSecurity.Checked, txtDbUsername.Text, txtDbPassword.Text)

            If WithMessage Then
                If result = False Then
                    WarningMsg("Connection failure.")
                Else
                    InformationMsg("Connection succeed.")
                End If
            End If

            Return result
        End Function

        Public Sub New()

            ' This call is required by the designer.
            InitializeComponent()

            ' Add any initialization after the InitializeComponent() call.
            For Each c As Control In Me.Controls
                Dim m As New ctlSize
                m.mySize = c.Size
                m.myLocation = c.Location
                m.myCtl = c
                aryOriCtlSize.Add(m)
            Next
        End Sub

        Private Sub DatabaseControl_Load(sender As Object, e As EventArgs) Handles Me.Load
            MyBase.RefreshTabScheme(TabOrder.TabOrderManager.TabScheme.AcrossFirst)
        End Sub

#Region "Control show/hide password for txtDbPass"
        Private Sub ceDbIntegratedSecurity_CheckedChanged(sender As Object, e As EventArgs) Handles ceDbIntegratedSecurity.CheckedChanged
            pnlDbSecurity.Enabled = Not ceDbIntegratedSecurity.Checked
        End Sub
#End Region

#Region "Test Connection"
        Private Sub sbDbTestConnection_Click(sender As Object, e As EventArgs) Handles sbDbTestConnection.Click
            Dim result As Boolean

            result = Ape.EL.Data.DbUtility.TestConnection(txtDbServerName.Text, ceDbIntegratedSecurity.Checked, txtDbUsername.Text, txtDbPassword.Text)

            If result = False Then
                WarningMsg("Connection failure.")
            Else
                InformationMsg("Connection succeed.")
            End If
        End Sub
#End Region
#End Region
    End Class

End Namespace
