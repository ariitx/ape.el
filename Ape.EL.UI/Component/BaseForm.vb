﻿Imports System
Imports System.ComponentModel
Imports System.Drawing
Imports System.Windows.Forms
Imports System.Reflection

Namespace Component
    ''' <summary>
    ''' A customized base form with standardized functions.
    ''' Don't confuse with UI.DX's BaseForm.
    ''' They share some functions, but this one is lightweight.
    ''' Last updated on 12.11.2016.
    ''' </summary>
    <System.Diagnostics.DebuggerStepThrough()> _
    Public Class BaseForm
        Inherits System.Windows.Forms.Form

#Region "Windows Form Designer Generated Code"
        Private components As IContainer

        Private tabSchemeProvider As Ape.EL.UI.Component.TabOrder.TabSchemeProvider


        Public Sub New()
            Me.InitializeComponent()

            Me.KeyPreview = True
            If Ape.EL.Utility.General.IsRuntime Then
                Initializer()
                UIUpdateStart()
            End If
        End Sub

        Protected Overrides Sub Dispose(disposing As Boolean)
            If disposing AndAlso Me.components IsNot Nothing Then
                Me.components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        Private Sub InitializeComponent()
            Me.tabSchemeProvider = New Ape.EL.UI.Component.TabOrder.TabSchemeProvider()
            Me.SuspendLayout()
            '
            'BaseForm
            '
            Me.ClientSize = New System.Drawing.Size(576, 382)
            Me.Name = "BaseForm"
            Me.tabSchemeProvider.SetTabScheme(Me, Ape.EL.UI.Component.TabOrder.TabOrderManager.TabScheme.DownFirst)
            Me.Text = "Form"
            Me.ResumeLayout(False)

        End Sub

        ''' <summary>
        ''' Overridable routine which is called before performing other routines that is affected by BaseForm's properties (eg. UIUpdateStart).
        ''' </summary>
        Protected Overridable Sub Initializer()
        End Sub

        ''' <summary>
        ''' Update tab scheme to default.
        ''' </summary>
        Protected Sub RefreshTabScheme()
            RefreshTabScheme(Me.tabSchemeProvider.GetTabScheme(Me))
        End Sub

        Protected Sub RefreshTabScheme(tabschm As Ape.EL.UI.Component.TabOrder.TabOrderManager.TabScheme)
            Me.tabSchemeProvider.SetTabScheme(Me, tabschm)
        End Sub
#End Region

#Region "Pre Initiate"
        ''' <summary>
        ''' Put any code that you think is very crucial before running any other code.
        ''' This will be execute once BaseForm is called out before other variable initiated or constructor.
        ''' </summary>
        Private m_PreInit As Object = PreInit()
        Private Function PreInit() As Object
            If Ape.EL.Utility.General.IsRuntime Then
                Ape.EL.Setting.MySetting.ConsumeException = True
            End If
            Return Nothing
        End Function
#End Region

#Region "Parent Visibility"
        Private m_OriginalOwnerVisibility As Boolean
        Private m_HasHiddenOwner As Boolean

        <Category("Misc")>
        <Description("Hide Owner when loading as borderless maximized modal dialog.")>
        <DefaultValue(True)>
        <Browsable(False)>
        Property HideOwnerWhenMaximized As Boolean
            Get
                Return _HideOwnerWhenMaximized
            End Get
            Set(value As Boolean)
                _HideOwnerWhenMaximized = value
            End Set
        End Property
        Private _HideOwnerWhenMaximized As Boolean = True
#End Region

#Region "UIUpdate"
        Private m_OriginalOpacity As Integer

        ''' <summary>
        ''' Enable or disable UI Update feature which hide the form when loading in constructor and show it in Load event. Overrides Initializer to set the property.
        ''' </summary>
        <DefaultValue(True)>
        <Description("Enable or disable UI Update feature which hide the form when loading in constructor and show it in Load event.")>
        <Category("Misc")>
        <Browsable(False)>
        Property UseUIUpdate As Boolean
            Get
                Return _UseUIUpdate
            End Get
            Set(value As Boolean)
                _UseUIUpdate = value
            End Set
        End Property
        Private _UseUIUpdate As Boolean = True

        ''' <summary>
        ''' Must be called after initialize component in New constructor.
        ''' </summary>
        Private Sub UIUpdateStart()
            If _UseUIUpdate Then
                m_OriginalOpacity = Me.Opacity
                Me.Opacity = 0
            End If
        End Sub

        ''' <summary>
        ''' Must be called if UIUpdateStart was used. Otherwise screen will be hidden.
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub UIUpdateEnd()
            If _UseUIUpdate Then
                Application.DoEvents()
                Me.Opacity = m_OriginalOpacity
            End If
        End Sub
#End Region

#Region "AutoScale"
        ''' <summary> Force form and any userccontrol's AutoScaleMode to None. </summary>
        <DefaultValue(True), Category("Misc"), Browsable(False)>
        Property DisableAutoScale As Boolean
            Get
                Return _DisableAutoScale
            End Get
            Set(value As Boolean)
                _DisableAutoScale = value
            End Set
        End Property
        Private _DisableAutoScale As Boolean = True

        ''' <summary> Force form and any userccontrol's AutoScaleMode to None. </summary>
        Private Sub PerformDisableAutoScale()
            If _DisableAutoScale Then
                Me.AutoScaleMode = Windows.Forms.AutoScaleMode.None
                Dim ctllist As List(Of Control) = Ape.EL.WinForm.General.FindAllControls(Me)
                For Each ctl As Control In ctllist
                    If GetType(UserControl).IsAssignableFrom(ctl.GetType) Then
                        DirectCast(ctl, UserControl).AutoScaleMode = Windows.Forms.AutoScaleMode.None
                    End If
                    ctl.Font = ctl.Font
                Next
            End If
        End Sub
#End Region

#Region "Prevent Form Flickering"
        Private originalExStyle As Integer = -1
        Private enableFormLevelDoubleBuffering As Boolean = True

        Protected Overrides ReadOnly Property CreateParams() As CreateParams
            Get
                If Not Ape.EL.Utility.General.IsRuntime Then enableFormLevelDoubleBuffering = False

                If originalExStyle = -1 Then
                    originalExStyle = MyBase.CreateParams.ExStyle
                End If

                Dim cp As CreateParams = MyBase.CreateParams
                If enableFormLevelDoubleBuffering Then
                    cp.ExStyle = cp.ExStyle Or Ape.EL.Win32.WindowStylesExtended.WS_EX_COMPOSITED '33554432 or &H2000000
                Else
                    ' WS_EX_COMPOSITED
                    cp.ExStyle = originalExStyle
                End If

                Return cp
            End Get
        End Property

        ''' <summary>Use this on form shown.</summary>
        Protected Sub TurnOffFormLevelDoubleBuffering()
            If Not Ape.EL.Utility.General.IsRuntime Then Exit Sub
            Me.Refresh()
            enableFormLevelDoubleBuffering = False
            Me.MaximizeBox = Not Me.MaximizeBox
            Me.MaximizeBox = Not Me.MaximizeBox
            Me.BringToFront()
        End Sub
#End Region

#Region "Active Control Changed Custom Event"
        'This region contains code that able to raise a customized event (FocusChanged)
        <System.Diagnostics.DebuggerStepThrough()> _
        Public Class FocusChangedArg
            Inherits EventArgs
            Public LastControlWithFocus As Control
            Public Sub New(ByVal Last As Control)
                LastControlWithFocus = Last
            End Sub
        End Class

        Private LastActiveControl As Control = Nothing
        Protected Event FocusChanged As EventHandler(Of FocusChangedArg)

        Protected Overrides Sub UpdateDefaultButton()
            RaiseEvent FocusChanged(Me, New FocusChangedArg(LastActiveControl))
            LastActiveControl = Me.ActiveControl 'Store this for the next time Focus changes
            MyBase.UpdateDefaultButton() 'send back the original button
        End Sub
#End Region

#Region "User Access"
        Protected Property UserAccess As Ape.EL.App.UserAccessControl
            Get
                Return _UserAccess
            End Get
            Set(value As Ape.EL.App.UserAccessControl)
                _UserAccess = value
            End Set
        End Property
        Private _UserAccess As Ape.EL.App.UserAccessControl = Nothing

        Protected Overridable Sub ApplyUserAccess()
        End Sub
#End Region

#Region "Disable close when still loading.."
        Overloads Sub Close()
            If _IsFormLoaded = True Then
                MyBase.Close()
            End If
        End Sub
#End Region

        ''' <summary>
        ''' Indicate that Load event has been executed. This property is updated in Shown event.
        ''' </summary>
        Protected ReadOnly Property IsFormLoaded As Boolean
            Get
                Return _IsFormLoaded
            End Get
        End Property
        Private _IsFormLoaded As Boolean = False

        Private Sub BaseForm_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
            'Hide owner when maximized
            If _HideOwnerWhenMaximized AndAlso m_HasHiddenOwner Then
                If Me.Owner IsNot Nothing AndAlso _
                        Me.FormBorderStyle = Windows.Forms.FormBorderStyle.None AndAlso _
                        Me.WindowState = FormWindowState.Maximized Then
                    Me.Owner.Visible = True
                End If
            End If
        End Sub

        Private Sub BaseForm_Load(sender As Object, e As EventArgs) Handles Me.Load
            If Ape.EL.Utility.General.IsRuntime Then
                'set default icon
                Ape.EL.Internal.SetIcon(Me.Icon)

                'Apply user access
                ApplyUserAccess()

                'I am not sure this will prompt for keyboard input in windows 8+ or not when user focus on any textbox.
                Dim aeForm As Windows.Automation.AutomationElement = System.Windows.Automation.AutomationElement.FromHandle(Me.Handle)
            End If
        End Sub

        Private Sub BaseForm_Shown(sender As Object, e As EventArgs) Handles Me.Shown
            TurnOffFormLevelDoubleBuffering()

            PerformDisableAutoScale()

            If Ape.EL.Utility.General.IsRuntime Then
                UIUpdateEnd()
                Ape.EL.Setting.MySetting.ConsumeException = False
            End If

            If _HideOwnerWhenMaximized Then
                If Me.Owner IsNot Nothing AndAlso _
                         Me.FormBorderStyle = Windows.Forms.FormBorderStyle.None AndAlso _
                         Me.WindowState = FormWindowState.Maximized Then
                    m_OriginalOwnerVisibility = Me.Owner.Visible
                    Me.Owner.Visible = False
                    m_HasHiddenOwner = True
                End If
            End If

            If Ape.EL.Utility.General.IsRuntime Then
                RaiseEvent FocusChanged(Me, New FocusChangedArg(LastActiveControl))
            End If

            _IsFormLoaded = True
        End Sub

        Private Sub BaseForm_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed

        End Sub
    End Class
End Namespace