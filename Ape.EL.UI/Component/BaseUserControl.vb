﻿Imports System.Runtime.InteropServices
Imports System.Windows.Forms
Imports System.ComponentModel
Imports System.Drawing
Imports Ape.EL.UI.Component

Namespace Component
    ''' <summary>
    ''' A customized base user control with standardized functions.
    ''' Don't confuse with UI.DX's BaseUserControl.
    ''' They share some functions, but this one is lightweight.
    ''' Last updated on 12.11.2016.
    ''' </summary>
    <System.Diagnostics.DebuggerStepThrough()> _
    Public Class BaseUserControl
        Inherits System.Windows.Forms.UserControl

#Region "Windows Form Designer Generated Code"
        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub
        Private WithEvents tabSchemeProvider As Ape.EL.UI.Component.TabOrder.TabSchemeProvider

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.tabSchemeProvider = New Ape.EL.UI.Component.TabOrder.TabSchemeProvider()
            Me.SuspendLayout()
            '
            'BaseUserControl
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
            Me.Name = "BaseUserControl"
            Me.tabSchemeProvider.SetTabScheme(Me, Ape.EL.UI.Component.TabOrder.TabOrderManager.TabScheme.DownFirst)
            Me.ResumeLayout(False)

        End Sub
#End Region

        ''' <summary> Occurs before ShowDialog is showing the dialog form.</summary>
        Protected Event DialogShowing()

        ''' <summary> Occurs after ShowDialog is closed and before returning result. </summary>
        Protected Event DialogClosed()

        ''' <summary> Occurs when ShowDialog Shown event is raised. </summary>
        Protected Event Shown()

        ''' <summary>
        ''' Indicate that Load event has been executed. This property is updated in Load event.
        ''' </summary>
        <Browsable(False)> _
        Protected ReadOnly Property IsControlLoaded As Boolean
            Get
                Return _IsControlLoaded
            End Get
        End Property
        Private _IsControlLoaded As Boolean = False

        ''' <summary>
        ''' Determine whether the control is used on test container.
        ''' </summary>
        <Browsable(False)> _
        Protected ReadOnly Property IsOnTestContainer As Boolean
            Get
                Return FindForm() IsNot Nothing AndAlso FindForm.GetType.Name = "UserControlTestContainer"
            End Get
        End Property

        ''' <summary>
        ''' Get or set the title text for DialogShow's form.
        ''' </summary>
        <Category("Misc")> _
        <Description("Get or set the title text for DialogShow's form.")> _
        <DefaultValue("")> _
        <Browsable(True)> _
        Public Property Caption As String
            Get
                Return _Caption
            End Get
            Set(value As String)
                _Caption = value
            End Set
        End Property
        Private _Caption As String = ""

#Region "KeyPreview" 'This will make our user control has function like normal form for KeyPreview, which will raise KeyDown events when it is true. Originally, UserControl does not have KeyPreview capability.

        Private _KeyPreview As Boolean = True
        ''' <summary> Gets or sets a value indicating whether the form will receive key events before the event is passed to the control that has focus. </summary>
        <Category("Misc")> _
        <Description("Determines whether keyboard events for controls on the form are registered with the form.")> _
        <DefaultValue(True)> _
        <Browsable(True)> _
        Public Property KeyPreview As Boolean
            Get
                Return _KeyPreview
            End Get
            Set(value As Boolean)
                _KeyPreview = value
            End Set
        End Property

        '----------------------------------------------
        ' Define the PeekMessage API call
        '----------------------------------------------

        Private Structure MSG
            Public hwnd As IntPtr
            Public message As Integer
            Public wParam As IntPtr
            Public lParam As IntPtr
            Public time As Integer
            Public pt_x As Integer
            Public pt_y As Integer
        End Structure

        <DllImport("user32.dll", CharSet:=CharSet.Auto)> _
        Private Shared Function PeekMessage(<[In], Out> ByRef msg As MSG, hwnd As HandleRef, msgMin As Integer, msgMax As Integer, remove As Integer) As Boolean
        End Function

        '----------------------------------------------

        ''' <summary> 
        ''' Trap any keypress before child controls get hold of them.
        ''' </summary>
        ''' <param name="m">Windows message</param>
        ''' <returns>True if the keypress is handled</returns>
        Protected Overrides Function ProcessKeyPreview(ByRef m As Message) As Boolean
            Const WM_KEYDOWN As Integer = &H100
            'Const WM_KEYUP As Integer = &H101
            Const WM_CHAR As Integer = &H102
            Const WM_SYSCHAR As Integer = &H106
            Const WM_SYSKEYDOWN As Integer = &H104
            'Const WM_SYSKEYUP As Integer = &H105
            Const WM_IME_CHAR As Integer = &H286

            Dim e As KeyEventArgs = Nothing

            If (m.Msg <> WM_CHAR) AndAlso (m.Msg <> WM_SYSCHAR) AndAlso (m.Msg <> WM_IME_CHAR) Then
                e = New KeyEventArgs(DirectCast(CInt(CLng(m.WParam)), Keys) Or ModifierKeys)
                If (m.Msg = WM_KEYDOWN) OrElse (m.Msg = WM_SYSKEYDOWN) Then
                    If KeyPreview Then
                        'TrappedKeyDown(e)
                        OnKeyDown(e)
                    End If
                End If
                'else
                '{
                '    TrappedKeyUp(e);
                '}

                ' Remove any WM_CHAR type messages if supresskeypress is true.
                If e.SuppressKeyPress Then
                    Me.RemovePendingMessages(WM_CHAR, WM_CHAR)
                    Me.RemovePendingMessages(WM_SYSCHAR, WM_SYSCHAR)
                    Me.RemovePendingMessages(WM_IME_CHAR, WM_IME_CHAR)
                End If

                If e.Handled Then
                    Return e.Handled
                End If
            End If
            Return MyBase.ProcessKeyPreview(m)
        End Function

        Private Sub RemovePendingMessages(msgMin As Integer, msgMax As Integer)
            If Not Me.IsDisposed Then
                Dim msg As New MSG()
                Dim handle As IntPtr = Me.Handle
                While PeekMessage(msg, New HandleRef(Me, handle), msgMin, msgMax, 1)
                End While
            End If
        End Sub

        ' ''' <summary>
        ' ''' This routine gets called if a keydown has been trapped 
        ' ''' before a child control can get it.
        ' ''' </summary>
        ' ''' <param name="e"></param>
        'Protected Overridable Sub TrappedKeyDown(e As KeyEventArgs)
        '    'If e.KeyCode = Keys.A Then
        '    '    e.Handled = True
        '    '    e.SuppressKeyPress = True
        '    'End If
        'End Sub
#End Region

#Region "Disable control resizing, set DisableResizing to true."
        Public Overrides Property MaximumSize() As Size
            Get
                Return If(Me.IsHandleCreated And _DisableResizing, Me.Size, MyBase.MaximumSize)
            End Get
            Set(ByVal value As Size)
                MyBase.MaximumSize = value
            End Set
        End Property

        Public Overrides Property MinimumSize() As Size
            Get
                Return If(Me.IsHandleCreated And _DisableResizing, Me.Size, MyBase.MinimumSize)
            End Get
            Set(ByVal value As Size)
                MyBase.MinimumSize = value
            End Set
        End Property

        ''' <summary>
        ''' Disable resizing of user control by setting its MaximumSize and MinimumSize property to its base Size.
        ''' </summary>
        Protected Property DisableResizing As Boolean
            Get
                Return _DisableResizing
            End Get
            Set(value As Boolean)
                _DisableResizing = value
                If Me.IsHandleCreated And Not value Then
                    MyBase.MaximumSize = DefaultMaximumSize
                    MyBase.MinimumSize = DefaultMinimumSize
                End If
            End Set
        End Property
        Private _DisableResizing As Boolean
#End Region

#Region "Show single control in form"
        ''' <summary>
        ''' Prompt a dialog form with the control docked to return DialogResult.
        ''' </summary>
        ''' <param name="ContainerForm"></param>
        ''' <param name="owner">Set container form's owner.</param>
        ''' <param name="ContainerTitle">Set container form's Text if ContainerForm is nothing. Caption property will be used if ContainerTitle is empty.</param>
        ''' <param name="IsResizableForm">Set whether the container form should resizable if ContainerForm is nothing.</param>
        Function ShowDialog(ContainerForm As Form,
                            DefaultResult As DialogResult,
                            Optional owner As IWin32Window = Nothing,
                            Optional ContainerTitle As String = "",
                            Optional IsResizableForm As Boolean = False) As DialogResult
            RaiseEvent DialogShowing()

            Dim res As DialogResult = DefaultResult
            Dim isnewF As Boolean 'indicate that containerform isnot nothing and no need to dispose from here
            Dim f As Form = ContainerForm
            If f IsNot Nothing Then
                isnewF = False
            Else 'create a new form.
                f = New BaseForm
                f.Text = If(ContainerTitle.IsEmpty, _Caption, ContainerTitle)
                If Not IsResizableForm Then
                    f.FormBorderStyle = FormBorderStyle.FixedSingle
                    f.MaximizeBox = False
                    f.MinimizeBox = False
                End If
            End If

            If f IsNot Nothing AndAlso Not f.IsDisposed Then
                AddHandler f.Shown, Sub() RaiseEvent Shown() 'forward form's Shown to this user control's Shown
                f.StartPosition = If(owner Is Nothing, FormStartPosition.CenterScreen, FormStartPosition.CenterParent)
                f.ClientSize = Me.Size
                f.MinimumSize = f.Size
                f.Controls.Add(Me)
                f.CancelButton = _CancelButton
                Me.Dock = DockStyle.Fill
                res = f.ShowDialog(owner)
                f.Controls.Remove(Me)
            End If

            'dispose f if it is created from here.
            If isnewF AndAlso f IsNot Nothing AndAlso Not f.IsDisposed Then f.Dispose()

            RaiseEvent DialogClosed()

            Return res
        End Function

        ''' <summary>
        ''' Prompt a dialog form with the control docked to return DialogResult.
        ''' </summary>
        ''' <param name="owner">Set container form's owner.</param>
        ''' <param name="ContainerTitle">Set container form's Text if ContainerForm is nothing. Caption property will be used if ContainerTitle is empty.</param>
        ''' <param name="IsResizableForm">Set whether the container form should resizable if ContainerForm is nothing.</param>
        Function ShowDialog(Optional owner As IWin32Window = Nothing,
                              Optional ContainerTitle As String = "",
                              Optional IsResizableForm As Boolean = False) As DialogResult
            Return ShowDialog(Nothing, DialogResult.None, owner, ContainerTitle, IsResizableForm)
        End Function

        ''' <summary>
        ''' Cancel button will be used in ShowDialog's CancelButton property.
        ''' </summary>
        Protected Property CancelButton As System.Windows.Forms.IButtonControl
            Get
                Return _CancelButton
            End Get
            Set(value As System.Windows.Forms.IButtonControl)
                _CancelButton = value
            End Set
        End Property
        Private _CancelButton As System.Windows.Forms.IButtonControl
#End Region

        Public Sub New()
            InitializeComponent()

            RefreshTabScheme()
        End Sub

        ''' <summary>
        ''' Update tab scheme to default.
        ''' </summary>
        Protected Sub RefreshTabScheme()
            RefreshTabScheme(Me.tabSchemeProvider.GetTabScheme(Me))
        End Sub

        Protected Sub RefreshTabScheme(tabschm As Ape.EL.UI.Component.TabOrder.TabOrderManager.TabScheme)
            Me.tabSchemeProvider.SetTabScheme(Me, tabschm)
        End Sub

        Private Sub BaseUserControl_Load(sender As Object, e As EventArgs) Handles Me.Load
            _IsControlLoaded = True
        End Sub
    End Class
End Namespace
