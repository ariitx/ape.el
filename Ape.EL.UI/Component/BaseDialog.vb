﻿'FlexibleMessageBox – A flexible replacement for the .NET MessageBox
'https://www.codeproject.com/Articles/601900/FlexibleMessageBox-A-flexible-replacement-for-the

Imports System.Diagnostics
Imports System.Drawing
Imports System.Globalization
Imports System.Linq
Imports System.Windows.Forms
Imports Ape.EL.UI.Extensions

Namespace Component
    <System.Diagnostics.DebuggerStepThrough()>
    Public Class BaseDialog
        Inherits System.Windows.Forms.Form

#Region "Windows Form Designer Generated Code"
        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.button1 = New System.Windows.Forms.Button()
            Me.richTextBoxMessage = New System.Windows.Forms.Label()
            Me.BaseDialogFormBindingSource = New System.Windows.Forms.BindingSource()
            Me.panel1 = New System.Windows.Forms.Panel()
            Me.pictureBoxForIcon = New System.Windows.Forms.PictureBox()
            Me.button2 = New System.Windows.Forms.Button()
            Me.button3 = New System.Windows.Forms.Button()
            CType(Me.BaseDialogFormBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.panel1.SuspendLayout()
            CType(Me.pictureBoxForIcon, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'button1
            '
            Me.button1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.button1.AutoSize = True
            Me.button1.DialogResult = System.Windows.Forms.DialogResult.OK
            Me.button1.Location = New System.Drawing.Point(17, 81)
            Me.button1.MinimumSize = New System.Drawing.Size(0, 24)
            Me.button1.Name = "button1"
            Me.button1.Size = New System.Drawing.Size(75, 24)
            Me.button1.TabIndex = 0
            Me.button1.Text = "OK"
            Me.button1.UseVisualStyleBackColor = True
            Me.button1.Visible = False
            '
            'richTextBoxMessage
            '
            Me.richTextBoxMessage.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.richTextBoxMessage.BackColor = System.Drawing.Color.White
            Me.richTextBoxMessage.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BaseDialogFormBindingSource, "MessageText", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
            Me.richTextBoxMessage.Font = New System.Drawing.Font("Tahoma", 9.0!)
            Me.richTextBoxMessage.Location = New System.Drawing.Point(63, 23)
            Me.richTextBoxMessage.Margin = New System.Windows.Forms.Padding(0)
            Me.richTextBoxMessage.Name = "richTextBoxMessage"
            Me.richTextBoxMessage.Size = New System.Drawing.Size(193, 31)
            Me.richTextBoxMessage.TabIndex = 1
            Me.richTextBoxMessage.Text = "<Message>"
            '
            'panel1
            '
            Me.panel1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.panel1.BackColor = System.Drawing.Color.White
            Me.panel1.Controls.Add(Me.pictureBoxForIcon)
            Me.panel1.Controls.Add(Me.richTextBoxMessage)
            Me.panel1.Location = New System.Drawing.Point(0, 0)
            Me.panel1.Name = "panel1"
            Me.panel1.Size = New System.Drawing.Size(270, 70)
            Me.panel1.TabIndex = 2
            '
            'pictureBoxForIcon
            '
            Me.pictureBoxForIcon.BackColor = System.Drawing.Color.Transparent
            Me.pictureBoxForIcon.Location = New System.Drawing.Point(21, 23)
            Me.pictureBoxForIcon.Name = "pictureBoxForIcon"
            Me.pictureBoxForIcon.Size = New System.Drawing.Size(32, 32)
            Me.pictureBoxForIcon.TabIndex = 0
            Me.pictureBoxForIcon.TabStop = False
            '
            'button2
            '
            Me.button2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.button2.DialogResult = System.Windows.Forms.DialogResult.OK
            Me.button2.Location = New System.Drawing.Point(98, 81)
            Me.button2.MinimumSize = New System.Drawing.Size(0, 24)
            Me.button2.Name = "button2"
            Me.button2.Size = New System.Drawing.Size(75, 24)
            Me.button2.TabIndex = 0
            Me.button2.Text = "OK"
            Me.button2.UseVisualStyleBackColor = True
            Me.button2.Visible = False
            '
            'button3
            '
            Me.button3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.button3.AutoSize = True
            Me.button3.DialogResult = System.Windows.Forms.DialogResult.OK
            Me.button3.Location = New System.Drawing.Point(179, 81)
            Me.button3.MinimumSize = New System.Drawing.Size(0, 24)
            Me.button3.Name = "button3"
            Me.button3.Size = New System.Drawing.Size(75, 24)
            Me.button3.TabIndex = 0
            Me.button3.Text = "OK"
            Me.button3.UseVisualStyleBackColor = True
            Me.button3.Visible = False
            '
            'BaseDialog
            '
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
            Me.ClientSize = New System.Drawing.Size(270, 115)
            Me.Controls.Add(Me.panel1)
            Me.Controls.Add(Me.button3)
            Me.Controls.Add(Me.button2)
            Me.Controls.Add(Me.button1)
            Me.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BaseDialogFormBindingSource, "CaptionText", True))
            Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
            Me.MaximizeBox = False
            Me.MinimizeBox = False
            Me.MinimumSize = New System.Drawing.Size(6, 140)
            Me.Name = "BaseDialog"
            Me.ShowIcon = False
            Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
            Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
            Me.Text = "<Caption>"
            CType(Me.BaseDialogFormBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
            Me.panel1.ResumeLayout(False)
            CType(Me.pictureBoxForIcon, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub

        Private WithEvents button1 As System.Windows.Forms.Button
        Private WithEvents BaseDialogFormBindingSource As System.Windows.Forms.BindingSource
        Private WithEvents richTextBoxMessage As System.Windows.Forms.Label 'As System.Windows.Forms.RichTextBox '
        Private WithEvents panel1 As System.Windows.Forms.Panel
        Private WithEvents pictureBoxForIcon As System.Windows.Forms.PictureBox
        Private WithEvents button2 As System.Windows.Forms.Button
        Private WithEvents button3 As System.Windows.Forms.Button
#End Region

        Private Declare Function Beep Lib "kernel32" (ByVal soundFrequency As Int32, ByVal soundDuration As Int32) As Int32
        Private Shared m_keysSuppressed() As Keys
        Private Shared m_intFrequency As Integer
        Private Shared m_intDuration As Integer
        Private Shared m_intBeep As Integer

#Region "Public statics"

        ''' <summary>
        ''' Defines the maximum width for all BaseDialog instances in percent of the working area.
        ''' 
        ''' Allowed values are 0.2 - 1.0 where: 
        ''' 0.2 means:  The BaseDialog can be at most half as wide as the working area.
        ''' 1.0 means:  The BaseDialog can be as wide as the working area.
        ''' 
        ''' Default is: 70% of the working area width.
        ''' </summary>
        Public Shared MAX_WIDTH_FACTOR As Double = 0.7

        ''' <summary>
        ''' Defines the maximum height for all BaseDialog instances in percent of the working area.
        ''' 
        ''' Allowed values are 0.2 - 1.0 where: 
        ''' 0.2 means:  The BaseDialog can be at most half as high as the working area.
        ''' 1.0 means:  The BaseDialog can be as high as the working area.
        ''' 
        ''' Default is: 90% of the working area height.
        ''' </summary>
        Public Shared MAX_HEIGHT_FACTOR As Double = 0.9

        ''' <summary>
        ''' Defines the font for all BaseDialog instances.
        ''' 
        ''' Default is: SystemFonts.MessageBoxFont
        ''' </summary>
        Public Shared FONT_ As Font = SystemFonts.MessageBoxFont

#End Region

#Region "Public show functions"

        ''' <summary>
        ''' Shows the specified message box.
        ''' </summary>
        ''' <param name="text">The text.</param>
        ''' <returns>The dialog result.</returns>
        Public Overloads Shared Function Show(ByVal text As String) As DialogResult
            Return Show(Nothing, text, String.Empty, MessageBoxButtons.OK, MessageBoxIcon.None, MessageBoxDefaultButton.Button1, Nothing, Nothing, Nothing, Nothing)
        End Function

        ''' <summary>
        ''' Shows the specified message box.
        ''' </summary>
        ''' <param name="owner">The owner.</param>
        ''' <param name="text">The text.</param>
        ''' <returns>The dialog result.</returns>
        Public Overloads Shared Function Show(ByVal owner As IWin32Window, ByVal text As String) As DialogResult
            Return Show(owner, text, String.Empty, MessageBoxButtons.OK, MessageBoxIcon.None, MessageBoxDefaultButton.Button1, Nothing, Nothing, Nothing, Nothing)
        End Function

        ''' <summary>
        ''' Shows the specified message box.
        ''' </summary>
        ''' <param name="text">The text.</param>
        ''' <param name="caption">The caption.</param>
        ''' <returns>The dialog result.</returns>
        Public Overloads Shared Function Show(ByVal text As String, ByVal caption As String) As DialogResult
            Return Show(Nothing, text, caption, MessageBoxButtons.OK, MessageBoxIcon.None, MessageBoxDefaultButton.Button1, Nothing, Nothing, Nothing, Nothing)
        End Function

        ''' <summary>
        ''' Shows the specified message box.
        ''' </summary>
        ''' <param name="owner">The owner.</param>
        ''' <param name="text">The text.</param>
        ''' <param name="caption">The caption.</param>
        ''' <returns>The dialog result.</returns>
        Public Overloads Shared Function Show(ByVal owner As IWin32Window, ByVal text As String, ByVal caption As String) As DialogResult
            Return Show(owner, text, caption, MessageBoxButtons.OK, MessageBoxIcon.None, MessageBoxDefaultButton.Button1, Nothing, Nothing, Nothing, Nothing)
        End Function

        ''' <summary>
        ''' Shows the specified message box.
        ''' </summary>
        ''' <param name="text">The text.</param>
        ''' <param name="caption">The caption.</param>
        ''' <param name="buttons">The buttons.</param>
        ''' <returns>The dialog result.</returns>
        Public Overloads Shared Function Show(ByVal text As String, ByVal caption As String, ByVal buttons As MessageBoxButtons) As DialogResult
            Return Show(Nothing, text, caption, buttons, MessageBoxIcon.None, MessageBoxDefaultButton.Button1, Nothing, Nothing, Nothing, Nothing)
        End Function

        ''' <summary>
        ''' Shows the specified message box.
        ''' </summary>
        ''' <param name="owner">The owner.</param>
        ''' <param name="text">The text.</param>
        ''' <param name="caption">The caption.</param>
        ''' <param name="buttons">The buttons.</param>
        ''' <returns>The dialog result.</returns>
        Public Overloads Shared Function Show(ByVal owner As IWin32Window, ByVal text As String, ByVal caption As String, ByVal buttons As MessageBoxButtons) As DialogResult
            Return Show(owner, text, caption, buttons, MessageBoxIcon.None, MessageBoxDefaultButton.Button1, Nothing, Nothing, Nothing, Nothing)
        End Function

        ''' <summary>
        ''' Shows the specified message box.
        ''' </summary>
        ''' <param name="text">The text.</param>
        ''' <param name="caption">The caption.</param>
        ''' <param name="buttons">The buttons.</param>
        ''' <param name="icon">The icon.</param>
        ''' <returns></returns>
        Public Overloads Shared Function Show(ByVal text As String, ByVal caption As String, ByVal buttons As MessageBoxButtons, ByVal icon As MessageBoxIcon) As DialogResult
            Return Show(Nothing, text, caption, buttons, icon, MessageBoxDefaultButton.Button1, Nothing, Nothing, Nothing, Nothing)
        End Function

        ''' <summary>
        ''' Shows the specified message box.
        ''' </summary>
        ''' <param name="owner">The owner.</param>
        ''' <param name="text">The text.</param>
        ''' <param name="caption">The caption.</param>
        ''' <param name="buttons">The buttons.</param>
        ''' <param name="icon">The icon.</param>
        ''' <returns>The dialog result.</returns>
        Public Overloads Shared Function Show(ByVal owner As IWin32Window, ByVal text As String, ByVal caption As String, ByVal buttons As MessageBoxButtons, ByVal icon As MessageBoxIcon) As DialogResult
            Return Show(owner, text, caption, buttons, icon, MessageBoxDefaultButton.Button1, Nothing, Nothing, Nothing, Nothing)
        End Function

        ''' <summary>
        ''' Shows the specified message box.
        ''' </summary>
        ''' <param name="text">The text.</param>
        ''' <param name="caption">The caption.</param>
        ''' <param name="buttons">The buttons.</param>
        ''' <param name="icon">The icon.</param>
        ''' <param name="defaultButton">The default button.</param>
        ''' <returns>The dialog result.</returns>
        Public Overloads Shared Function Show(ByVal text As String, ByVal caption As String, ByVal buttons As MessageBoxButtons, ByVal icon As MessageBoxIcon, ByVal defaultButton As MessageBoxDefaultButton) As DialogResult
            Return Show(Nothing, text, caption, buttons, icon, defaultButton, Nothing, Nothing, Nothing, Nothing)
        End Function

        ''' <summary>
        ''' Shows the specified message box.
        ''' </summary>
        ''' <param name="owner">The owner.</param>
        ''' <param name="text">The text.</param>
        ''' <param name="caption">The caption.</param>
        ''' <param name="buttons">The buttons.</param>
        ''' <param name="icon">The icon.</param>
        ''' <param name="defaultButton">The default button.</param>
        ''' <returns>The dialog result.</returns>
        Public Overloads Shared Function Show(ByVal owner As IWin32Window, ByVal text As String, ByVal caption As String, ByVal buttons As MessageBoxButtons, ByVal icon As MessageBoxIcon, ByVal defaultButton As MessageBoxDefaultButton) As DialogResult
            Return Show(owner, text, caption, buttons, icon, defaultButton, Nothing, Nothing, Nothing, Nothing)
        End Function

        ''' <summary>
        ''' Shows the specified message box.
        ''' </summary>
        ''' <param name="text">The text.</param>
        ''' <param name="caption">The caption.</param>
        ''' <param name="buttons">The buttons.</param>
        ''' <param name="icon">The icon.</param>
        ''' <param name="keys">Suppressed keys.</param>
        ''' <returns>The dialog result.</returns>
        Public Overloads Shared Function Show(ByVal text As String, ByVal caption As String, ByVal buttons As MessageBoxButtons, ByVal icon As MessageBoxIcon, ByVal keys As Keys()) As DialogResult
            Return Show(Nothing, text, caption, buttons, icon, MessageBoxDefaultButton.Button1, keys, Nothing, Nothing, Nothing)
        End Function

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="text">The text.</param>
        ''' <param name="caption">The caption.</param>
        ''' <param name="buttons">The buttons.</param>
        ''' <param name="icon">The icon.</param>
        ''' <param name="keys">Suppressed keys.</param>
        ''' <param name="SoundFrequency">Sound frequency.</param>
        ''' <returns>The dialog result.</returns>
        Public Overloads Shared Function Show(ByVal text As String, ByVal caption As String, ByVal buttons As MessageBoxButtons, ByVal icon As MessageBoxIcon, ByVal keys As Keys(), ByVal SoundFrequency As Integer) As DialogResult
            Return Show(Nothing, text, caption, buttons, icon, MessageBoxDefaultButton.Button1, keys, SoundFrequency, 300, 1)
        End Function

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="text">The text.</param>
        ''' <param name="caption">The caption.</param>
        ''' <param name="buttons">The buttons.</param>
        ''' <param name="icon">The icon.</param>
        ''' <param name="keys">Suppressed keys.</param>
        ''' <param name="SoundFrequency">Sound frequency.</param>
        ''' <param name="SoundDuration">Sound duration.</param>
        ''' <returns>The dialog result.</returns>
        Public Overloads Shared Function Show(ByVal text As String, ByVal caption As String, ByVal buttons As MessageBoxButtons, ByVal icon As MessageBoxIcon, ByVal keys As Keys(), ByVal SoundFrequency As Integer, ByVal SoundDuration As Integer) As DialogResult
            Return Show(Nothing, text, caption, buttons, icon, MessageBoxDefaultButton.Button1, keys, SoundFrequency, SoundDuration, 1)
        End Function

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="text">The text.</param>
        ''' <param name="caption">The caption.</param>
        ''' <param name="buttons">The buttons.</param>
        ''' <param name="icon">The icon.</param>
        ''' <param name="keys">Suppressed keys.</param>
        ''' <param name="SoundFrequency">Sound frequency.</param>
        ''' <param name="SoundDuration">Sound duration.</param>
        ''' <param name="intBeep">Number of beep.</param>
        ''' <returns>The dialog result.</returns>
        Public Overloads Shared Function Show(ByVal text As String, ByVal caption As String, ByVal buttons As MessageBoxButtons, ByVal icon As MessageBoxIcon, ByVal keys As Keys(), ByVal SoundFrequency As Integer, ByVal SoundDuration As Integer, ByVal intBeep As Integer) As DialogResult
            Return Show(Nothing, text, caption, buttons, icon, MessageBoxDefaultButton.Button1, keys, SoundFrequency, SoundDuration, intBeep)
        End Function
#End Region

#Region "Private constants"

        Private Enum BUTTON_TEXT
            OK = 0
            CANCEL
            YES
            NO
            ABORT
            RETRY
            IGNORE
        End Enum
        Private Shared ReadOnly BUTTON_TEXTS_ENGLISH As [String]() = {"&OK", "&Cancel", "&Yes", "&No", "&Abort", "&Retry", _
         "&Ignore"}
        Private Shared ReadOnly BUTTON_TEXTS_GERMAN As [String]() = {"OK", "Abbrechen", "Ja", "Nein", "Abbrechen", "Wiederholen", _
         "Ignorieren"}

#End Region

#Region "Private members"

        Private _defaultButton As MessageBoxDefaultButton
        Private _visibleButtonsCount As Integer
        Private _isCultureGerman As Boolean

#End Region

#Region "Private constructor"

        ''' <summary>
        ''' Initializes a new instance of the <see cref="BaseDialog"/> class.
        ''' </summary>
        Private Sub New()
            InitializeComponent()

            'Me._isCultureGerman = CultureInfo.InstalledUICulture.TwoLetterISOLanguageName.Contains("de")
            Me.KeyPreview = True
        End Sub

#End Region

#Region "Private helper functions"

        ''' <summary>
        ''' Gets the string rows.
        ''' </summary>
        ''' <param name="message">The message.</param>
        ''' <returns>The string rows as 1-dimensional array</returns>
        Private Shared Function GetStringRows(ByVal message As String) As String()
            If String.IsNullOrEmpty(message) Then
                Return Nothing
            End If

            Dim messageRows = message.Split(New Char() {ControlChars.Lf}, StringSplitOptions.None)
            Return messageRows
        End Function

        ''' <summary>
        ''' Gets the button text for the current language (if not german, english is used as default always)
        ''' </summary>
        ''' <param name="buttonTextIndex">Index of the button text.</param>
        ''' <returns>The button text</returns>
        Private Function GetButtonText(ByVal buttonTextIndex As BUTTON_TEXT) As String
            Return If(Me._isCultureGerman, BUTTON_TEXTS_GERMAN(Convert.ToInt32(buttonTextIndex)), BUTTON_TEXTS_ENGLISH(Convert.ToInt32(buttonTextIndex)))
        End Function

        ''' <summary>
        ''' Ensure the given working area factor in the range of  0.2 - 1.0 where: 
        ''' 
        ''' 0.2 means:  Half as large as the working area.
        ''' 1.0 means:  As large as the working area.
        ''' </summary>
        ''' <param name="workingAreaFactor">The given working area factor.</param>
        ''' <returns>The corrected given working area factor.</returns>
        Private Shared Function GetCorrectedWorkingAreaFactor(ByVal workingAreaFactor As Double) As Double
            Const MIN_FACTOR As Double = 0.2
            Const MAX_FACTOR As Double = 1.0

            If workingAreaFactor < MIN_FACTOR Then
                Return MIN_FACTOR
            End If
            If workingAreaFactor > MAX_FACTOR Then
                Return MAX_FACTOR
            End If

            Return workingAreaFactor
        End Function

        ''' <summary>
        ''' Set the dialogs start position when given. 
        ''' Otherwise center the dialog on the current screen.
        ''' </summary>
        ''' <param name="BaseDialogForm">The BaseDialog dialog.</param>
        ''' <param name="owner">The owner.</param>
        Private Shared Sub SetDialogStartPosition(ByVal BaseDialogForm As BaseDialog, ByVal owner As IWin32Window)
            'If no owner given: Center on current screen
            If owner Is Nothing Then
                Dim screen__1 = Screen.FromPoint(Cursor.Position)
                BaseDialogForm.StartPosition = FormStartPosition.Manual
                BaseDialogForm.Left = screen__1.Bounds.Left + screen__1.Bounds.Width \ 2 - BaseDialogForm.Width \ 2
                BaseDialogForm.Top = screen__1.Bounds.Top + screen__1.Bounds.Height \ 2 - BaseDialogForm.Height \ 2
            End If
        End Sub

        ''' <summary>
        ''' Calculate the dialogs start size (Try to auto-size width to show longest text row).
        ''' Also set the maximum dialog size. 
        ''' </summary>
        ''' <param name="BaseDialogForm__1">The BaseDialog dialog.</param>
        ''' <param name="text">The text (the longest text row is used to calculate the dialog width).</param>
        Private Shared Sub SetDialogSizes(ByVal BaseDialogForm__1 As BaseDialog, ByVal text As String, ByVal caption As String)
            'Set maximum dialog size
            BaseDialogForm__1.MaximumSize = New Size(Convert.ToInt32(SystemInformation.WorkingArea.Width * BaseDialog.GetCorrectedWorkingAreaFactor(MAX_WIDTH_FACTOR)), Convert.ToInt32(SystemInformation.WorkingArea.Height * BaseDialog.GetCorrectedWorkingAreaFactor(MAX_HEIGHT_FACTOR)))

            'Calculate dialog start size: Try to auto-size width to show longest text row
            Dim rowSize = System.Drawing.Size.Empty
            Dim maxTextRowWidth = 0
            Dim maxTextRowHeight = 0.0F
            Dim stringRows = GetStringRows(text)
            Dim intTextHeight As Integer = 0
            Using graphics = BaseDialogForm__1.CreateGraphics()
                maxTextRowHeight = graphics.MeasureString(" ", FONT_).Height

                maxTextRowWidth = graphics.MeasureString(caption, SystemFonts.CaptionFont, BaseDialogForm__1.MaximumSize.Width).ToSize.Width

                '1. Calculate optimal width
                For Each textForRow As System.String In stringRows
                    textForRow = textForRow.Replace(" ", Chr(160)) 'this is to make sure space get replaced with measurable character (invisible char alt+0160)
                    rowSize = graphics.MeasureString(textForRow, FONT_, BaseDialogForm__1.MaximumSize.Width).ToSize()
                    If rowSize.Width > maxTextRowWidth Then
                        maxTextRowWidth = rowSize.Width
                    End If
                Next

                'Set dialog start width
                BaseDialogForm__1.Width = maxTextRowWidth + BaseDialogForm__1.Width - BaseDialogForm__1.richTextBoxMessage.Width

                '2. Hackish way to calculate optimal height
                'use a temporary label which has same font as messagebox's richTextBoxMessage,
                'set the label's maximum size with the richTextBoxMessage's width and unlimited height.
                'this will ensure that the temporary label autosize the height.
                'after that, we put the label into BaseDialogForm and set the text.
                'remove from BaseDialogForm as we have generated the optimal height from temporary label.
                Static tmpLabel As New Label
                tmpLabel.AutoSize = True
                tmpLabel.MaximumSize = New Size(BaseDialogForm__1.richTextBoxMessage.Width, Int16.MaxValue)
                tmpLabel.Size = BaseDialogForm__1.richTextBoxMessage.Size
                tmpLabel.Font = BaseDialogForm__1.Font
                BaseDialogForm__1.Controls.Add(tmpLabel)
                tmpLabel.Text = text
                BaseDialogForm__1.Controls.Remove(tmpLabel)
                intTextHeight = tmpLabel.Height

                'Set dialog start height
                BaseDialogForm__1.Height = Convert.ToInt32(intTextHeight) + BaseDialogForm__1.Height - BaseDialogForm__1.richTextBoxMessage.Height
            End Using
        End Sub

        ''' <summary>
        ''' Set the dialogs icon. 
        ''' When no icon is used: Correct placement and width of rich text box.
        ''' </summary>
        ''' <param name="BaseDialogForm">The BaseDialog dialog.</param>
        ''' <param name="icon">The MessageBoxIcon.</param>
        Private Shared Sub SetDialogIcon(ByVal BaseDialogForm As BaseDialog, ByVal icon As MessageBoxIcon)
            Select Case icon
                Case MessageBoxIcon.Information
                    BaseDialogForm.pictureBoxForIcon.Image = SystemIcons.Information.ToBitmap()
                    Exit Select
                Case MessageBoxIcon.Warning
                    BaseDialogForm.pictureBoxForIcon.Image = SystemIcons.Warning.ToBitmap()
                    Exit Select
                Case MessageBoxIcon.[Error]
                    BaseDialogForm.pictureBoxForIcon.Image = SystemIcons.[Error].ToBitmap()
                    Exit Select
                Case MessageBoxIcon.Question
                    BaseDialogForm.pictureBoxForIcon.Image = SystemIcons.Question.ToBitmap()
                    Exit Select
                Case Else
                    'When no icon is used: Correct placement and width of rich text box.
                    BaseDialogForm.pictureBoxForIcon.Visible = False
                    BaseDialogForm.richTextBoxMessage.Left -= BaseDialogForm.pictureBoxForIcon.Width
                    BaseDialogForm.richTextBoxMessage.Width += BaseDialogForm.pictureBoxForIcon.Width
                    Exit Select
            End Select
        End Sub

        ''' <summary>
        ''' Set dialog buttons visibilities and texts. 
        ''' Also set a default button.
        ''' </summary>
        ''' <param name="BaseDialogForm">The BaseDialog dialog.</param>
        ''' <param name="buttons">The buttons.</param>
        ''' <param name="defaultButton">The default button.</param>
        Private Shared Sub SetDialogButtons(ByVal BaseDialogForm As BaseDialog, ByVal buttons As MessageBoxButtons, ByVal defaultButton As MessageBoxDefaultButton)
            'Set the buttons visibilities and texts
            Select Case buttons
                Case MessageBoxButtons.AbortRetryIgnore
                    BaseDialogForm._visibleButtonsCount = 3

                    BaseDialogForm.button1.Visible = True
                    BaseDialogForm.button1.Text = BaseDialogForm.GetButtonText(BUTTON_TEXT.ABORT)
                    BaseDialogForm.button1.DialogResult = DialogResult.Abort

                    BaseDialogForm.button2.Visible = True
                    BaseDialogForm.button2.Text = BaseDialogForm.GetButtonText(BUTTON_TEXT.RETRY)
                    BaseDialogForm.button2.DialogResult = DialogResult.Retry

                    BaseDialogForm.button3.Visible = True
                    BaseDialogForm.button3.Text = BaseDialogForm.GetButtonText(BUTTON_TEXT.IGNORE)
                    BaseDialogForm.button3.DialogResult = DialogResult.Ignore

                    BaseDialogForm.ControlBox = False
                    Exit Select

                Case MessageBoxButtons.OKCancel
                    BaseDialogForm._visibleButtonsCount = 2

                    BaseDialogForm.button2.Visible = True
                    BaseDialogForm.button2.Text = BaseDialogForm.GetButtonText(BUTTON_TEXT.OK)
                    BaseDialogForm.button2.DialogResult = DialogResult.OK

                    BaseDialogForm.button3.Visible = True
                    BaseDialogForm.button3.Text = BaseDialogForm.GetButtonText(BUTTON_TEXT.CANCEL)
                    BaseDialogForm.button3.DialogResult = DialogResult.Cancel

                    BaseDialogForm.CancelButton = BaseDialogForm.button3
                    Exit Select

                Case MessageBoxButtons.RetryCancel
                    BaseDialogForm._visibleButtonsCount = 2

                    BaseDialogForm.button2.Visible = True
                    BaseDialogForm.button2.Text = BaseDialogForm.GetButtonText(BUTTON_TEXT.RETRY)
                    BaseDialogForm.button2.DialogResult = DialogResult.Retry

                    BaseDialogForm.button3.Visible = True
                    BaseDialogForm.button3.Text = BaseDialogForm.GetButtonText(BUTTON_TEXT.CANCEL)
                    BaseDialogForm.button3.DialogResult = DialogResult.Cancel

                    BaseDialogForm.CancelButton = BaseDialogForm.button3
                    Exit Select

                Case MessageBoxButtons.YesNo
                    BaseDialogForm._visibleButtonsCount = 2

                    BaseDialogForm.button2.Visible = True
                    BaseDialogForm.button2.Text = BaseDialogForm.GetButtonText(BUTTON_TEXT.YES)
                    BaseDialogForm.button2.DialogResult = DialogResult.Yes

                    BaseDialogForm.button3.Visible = True
                    BaseDialogForm.button3.Text = BaseDialogForm.GetButtonText(BUTTON_TEXT.NO)
                    BaseDialogForm.button3.DialogResult = DialogResult.No

                    BaseDialogForm.ControlBox = False
                    Exit Select

                Case MessageBoxButtons.YesNoCancel
                    BaseDialogForm._visibleButtonsCount = 3

                    BaseDialogForm.button1.Visible = True
                    BaseDialogForm.button1.Text = BaseDialogForm.GetButtonText(BUTTON_TEXT.YES)
                    BaseDialogForm.button1.DialogResult = DialogResult.Yes

                    BaseDialogForm.button2.Visible = True
                    BaseDialogForm.button2.Text = BaseDialogForm.GetButtonText(BUTTON_TEXT.NO)
                    BaseDialogForm.button2.DialogResult = DialogResult.No

                    BaseDialogForm.button3.Visible = True
                    BaseDialogForm.button3.Text = BaseDialogForm.GetButtonText(BUTTON_TEXT.CANCEL)
                    BaseDialogForm.button3.DialogResult = DialogResult.Cancel

                    BaseDialogForm.CancelButton = BaseDialogForm.button3
                    Exit Select

                Case MessageBoxButtons.OK
                    BaseDialogForm._visibleButtonsCount = 1
                    BaseDialogForm.button3.Visible = True
                    BaseDialogForm.button3.Text = BaseDialogForm.GetButtonText(BUTTON_TEXT.OK)
                    BaseDialogForm.button3.DialogResult = DialogResult.OK

                    BaseDialogForm.CancelButton = BaseDialogForm.button3
                    Exit Select
            End Select

            'Set default button (used in BaseDialogForm_Shown)
            BaseDialogForm._defaultButton = defaultButton
        End Sub

#End Region

#Region "Private event handlers"

        Protected Overrides Function ProcessCmdKey(ByRef msg As Message, ByVal keyData As Keys) As Boolean
            'Return keyData = Keys.Enter OrElse MyBase.ProcessCmdKey(msg, keyData)
            If m_keysSuppressed IsNot Nothing Then
                If m_keysSuppressed.Count > 0 Then
                    For Each k As Keys In m_keysSuppressed
                        If keyData = k Then
                            For i As Integer = 1 To m_intBeep
                                Beep(m_intFrequency, m_intDuration)
                            Next
                            Return True
                        End If
                    Next
                End If
            End If
            Return MyBase.ProcessCmdKey(msg, keyData)
        End Function

        Private Sub BaseDialog_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
            If e.Control And e.KeyCode = Keys.C Then
                Dim sb As New System.Text.StringBuilder
                sb.AppendLine("---------------------------")
                sb.AppendLine(Me.Text)
                sb.AppendLine("---------------------------")
                sb.AppendLine(richTextBoxMessage.Text)
                sb.AppendLine("---------------------------")
                sb.AppendLine(If(button1.Visible, "[" & button1.Text.Replace("&", "") & "]", "") & If(button2.Visible, "[" & button2.Text.Replace("&", "") & "]", "") & If(button3.Visible, "[" & button3.Text.Replace("&", "") & "]", ""))
                sb.AppendLine("---------------------------")
                Clipboard.SetText(sb.ToString)
            End If
        End Sub

        Private Sub BaseDialog_Shown(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Shown
            Dim buttonIndexToFocus As Integer = 1
            Dim buttonToFocus As Button

            'Set the default button...
            Select Case Me._defaultButton
                Case MessageBoxDefaultButton.Button1
                    buttonIndexToFocus = 1
                    Exit Select
                Case MessageBoxDefaultButton.Button2
                    buttonIndexToFocus = 2
                    Exit Select
                Case MessageBoxDefaultButton.Button3
                    buttonIndexToFocus = 3
                    Exit Select
                Case Else
                    buttonIndexToFocus = 1
                    Exit Select
            End Select

            buttonIndexToFocus = 3 - Me._visibleButtonsCount + buttonIndexToFocus

            If buttonIndexToFocus = 3 Then
                buttonToFocus = Me.button3
            ElseIf buttonIndexToFocus = 2 Then
                buttonToFocus = Me.button2
            Else
                buttonToFocus = Me.button1
            End If

            buttonToFocus.Focus()
        End Sub

        '''' <summary>
        '''' Handles the LinkClicked event of the richTextBoxMessage control.
        '''' </summary>
        '''' <param name="sender">The source of the event.</param>
        '''' <param name="e">The <see cref="System.Windows.Forms.LinkClickedEventArgs"/> instance containing the event data.</param>
        'Private Sub richTextBoxMessage_LinkClicked(ByVal sender As Object, ByVal e As LinkClickedEventArgs)
        '    Try
        '        Cursor.Current = Cursors.WaitCursor
        '        Process.Start(e.LinkText)
        '    Catch generatedExceptionName As Exception
        '        'Let the caller of BaseDialogForm decide what to do with this exception...
        '        Throw
        '    Finally
        '        Cursor.Current = Cursors.[Default]
        '    End Try
        'End Sub

#End Region

#Region "Properties (only used for binding)"
        ''' <summary>
        ''' The text that is been used for the heading.
        ''' </summary>
        Public Property CaptionText() As String
            Get
                Return m_CaptionText
            End Get
            Set(ByVal value As String)
                m_CaptionText = value
            End Set
        End Property
        Private m_CaptionText As String

        ''' <summary>
        ''' The text that is been used in the BaseDialogForm.
        ''' </summary>
        Public Property MessageText() As String
            Get
                Return m_MessageText
            End Get
            Set(ByVal value As String)
                m_MessageText = value
            End Set
        End Property
        Private m_MessageText As String

        ''' <summary>
        ''' Gets or sets a value indicating whether the form should be displayed as a topmost form.
        ''' Static replacement for TopMost property.
        ''' </summary>
        Shared Property UseTopMost As Boolean
#End Region

#Region "Public show function"

        ''' <summary>
        ''' Shows the specified message box.
        ''' </summary>
        ''' <param name="owner">The owner.</param>
        ''' <param name="text">The text.</param>
        ''' <param name="caption">The caption.</param>
        ''' <param name="buttons">The buttons.</param>
        ''' <param name="icon">The icon.</param>
        ''' <param name="defaultButton">The default button.</param>
        ''' <param name="keySuppressed">Supressed Keys</param>
        ''' <returns>The dialog result.</returns>
        Public Overloads Shared Function Show(ByVal owner As IWin32Window, ByVal text As String, ByVal caption As String, ByVal buttons As MessageBoxButtons, ByVal icon As MessageBoxIcon, ByVal defaultButton As MessageBoxDefaultButton, ByVal keySuppressed As Keys(), ByVal intFrequency As Integer, ByVal intDuration As Integer, ByVal intBeep As Integer) As DialogResult
            'Create a new instance of the BaseDialog form
            Dim BaseDialogForm = New BaseDialog()
            BaseDialogForm.ShowInTaskbar = False
            BaseDialogForm.TopMost = UseTopMost

            'Bind the caption and the message text
            BaseDialogForm.CaptionText = caption
            BaseDialogForm.MessageText = text
            BaseDialogForm.BaseDialogFormBindingSource.DataSource = BaseDialogForm

            'Set the buttons visibilities and texts. Also set a default button.
            SetDialogButtons(BaseDialogForm, buttons, defaultButton)

            'Set the dialogs icon. When no icon is used: Correct placement and width of rich text box.
            SetDialogIcon(BaseDialogForm, icon)

            'Set the font for all controls
            BaseDialogForm.Font = FONT_
            BaseDialogForm.richTextBoxMessage.Font = FONT_

            'Calculate the dialogs start size (Try to auto-size width to show longest text row). Also set the maximum dialog size. 
            SetDialogSizes(BaseDialogForm, text, caption)

            'Set the dialogs start position when given. Otherwise center the dialog on the current screen.
            SetDialogStartPosition(BaseDialogForm, owner)

            'Set Suppressed Keys
            m_keysSuppressed = keySuppressed

            'Set the sound
            m_intFrequency = intFrequency
            m_intDuration = intDuration
            m_intBeep = intBeep

            Beep(m_intFrequency, m_intDuration)

            'Make sure sure the form can show nicely
            If owner Is Nothing OrElse CType(owner, Form).Visible = False Then
                owner = Ape.EL.WinForm.General.GetCallingForm
            End If

            'Show the dialog
            Return BaseDialogForm.ShowDialog(owner)
        End Function
#End Region

    End Class

    ''' <summary>
    ''' Use this class's function SetMessageBoxForm to change Ape.EL.WinForm.General.MessageBoxForm to BaseDialog.
    ''' </summary>
    <System.Diagnostics.DebuggerStepThrough()> _
    Public Class WinFormDialog
        Implements Ape.EL.Interfaces.IDialogBox

        ''' <summary>
        ''' Get or set the message box to be always on top of everything else. This is useful when the messagebox is shown by a different thread. Value directly affect BaseDialog.UseTopMost property.
        ''' </summary>
        Shared Property TopMost As Boolean
            Get
                Return BaseDialog.UseTopMost
            End Get
            Set(value As Boolean)
                BaseDialog.UseTopMost = value
            End Set
        End Property

        Public Function Show(owner As IWin32Window, text As String, caption As String, buttons As MessageBoxButtons, icon As MessageBoxIcon, defaultButton As MessageBoxDefaultButton) As DialogResult Implements Ape.EL.Interfaces.IDialogBox.Show
            Return BaseDialog.Show(owner, text, caption, buttons, icon, defaultButton)
        End Function

        ''' <summary>
        ''' Set default messagebox form to be used in ExceptionErrorMsg, QuestionMsg, InformationMsg, and WarningMsg.
        ''' </summary>
        Public Shared Sub SetMessageBoxForm()
            Ape.EL.WinForm.General.MessageBoxForm = GetType(WinFormDialog).GetInstance
        End Sub
    End Class
End Namespace
