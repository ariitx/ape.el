﻿
Imports System
Imports System.CodeDom
Imports System.CodeDom.Compiler
Imports System.Collections.Generic
Imports System.IO
Imports System.Reflection
Imports System.Reflection.Emit
Imports System.Security.Permissions

Namespace Misc
    Public Class CodeDomHelper
        Private Shared myReferencedAssemblies As String() = CodeDomHelper.GetAssemblies()

        Private Sub New()
        End Sub

        Private Shared Sub AddToArray(ary As List(Of String), nameOnlyArray As List(Of String), ParamArray filePaths As String())
            For i As Integer = 0 To filePaths.Length - 1
                Dim text As String = filePaths(i)
                Dim item As String = Path.GetFileName(text).ToUpperInvariant()
                If nameOnlyArray.IndexOf(item) < 0 Then
                    ary.Add(text)
                    nameOnlyArray.Add(item)
                End If
            Next
        End Sub

        Private Shared Function GetAssemblies() As String()
            Dim list As List(Of String) = New List(Of String)()
            Dim nameOnlyArray As List(Of String) = New List(Of String)()
            CodeDomHelper.AddToArray(list, nameOnlyArray, New String() {"System.dll", "System.Data.dll", "System.Xml.dll", "System.Drawing.dll", "System.Windows.Forms.dll"})
            CodeDomHelper.AddAssembliesFromDevExpress(list, nameOnlyArray)
            CodeDomHelper.AddAssembliesFromScriptReferences(list, nameOnlyArray)
            CodeDomHelper.AddAssembliesFromFolder(list, nameOnlyArray, AppDomain.CurrentDomain.BaseDirectory)
            Return list.ToArray()
        End Function

        Private Shared Sub AddAssembliesFromScriptReferences(ary As List(Of String), nameOnlyArray As List(Of String))
            Dim myPath As String = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "ScriptReferences.txt")
            If File.Exists(myPath) Then
                Try
                    Dim streamReader As StreamReader = File.OpenText(myPath)
                    While streamReader.Peek() >= 0
                        Dim text As String = streamReader.ReadLine().Trim()
                        Dim flag As Boolean = False
                        Dim assemblies As Assembly() = AppDomain.CurrentDomain.GetAssemblies()
                        For i As Integer = 0 To assemblies.Length - 1
                            Dim assembly As Assembly = assemblies(i)
                            If String.Compare(assembly.GetName().Name, Path.GetFileNameWithoutExtension(text), True) = 0 AndAlso Not (TypeOf assembly Is AssemblyBuilder) Then
                                flag = True
                                CodeDomHelper.AddToArray(ary, nameOnlyArray, New String() {assembly.Location})
                                Exit For
                            End If
                        Next
                        If Not flag Then
                            CodeDomHelper.AddToArray(ary, nameOnlyArray, New String() {text})
                        End If
                    End While
                Catch ex_CA As Exception
                End Try
            End If
        End Sub

        Private Shared Sub AddAssembliesFromDevExpress(ary As List(Of String), nameOnlyArray As List(Of String))
            Dim assemblies As Assembly() = AppDomain.CurrentDomain.GetAssemblies()
            For i As Integer = 0 To assemblies.Length - 1
                Dim assembly As Assembly = assemblies(i)
                If assembly.GetName().Name.Contains("DevExpress.") AndAlso Not (TypeOf assembly Is AssemblyBuilder) Then
                    CodeDomHelper.AddToArray(ary, nameOnlyArray, New String() {assembly.Location})
                End If
            Next
        End Sub

        Private Shared Sub AddAssembliesFromFolder(ary As List(Of String), nameOnlyArray As List(Of String), path As String)
            Try
                Dim files As String() = Directory.GetFiles(path)
                For i As Integer = 0 To files.Length - 1
                    Dim text As String = files(i).ToLowerInvariant()
                    If Not text.EndsWith(".resources.dll") AndAlso text.EndsWith(".dll") Then
                        Try
                            If CodeDomHelper.IsClrImage(files(i)) Then
                                CodeDomHelper.AddToArray(ary, nameOnlyArray, New String() {files(i)})
                            End If
                        Catch ex_52 As Exception
                        End Try
                    End If
                Next
                Dim directories As String() = Directory.GetDirectories(path)
                For j As Integer = 0 To directories.Length - 1
                    CodeDomHelper.AddAssembliesFromFolder(ary, nameOnlyArray, directories(j))
                Next
            Catch ex_85 As Exception
            End Try
        End Sub

        Private Shared Function IsClrImage(fileName As String) As Boolean
            Dim fileStream As FileStream = Nothing
            Dim result As Boolean
            Try
                fileStream = New FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read)
                Dim array As Byte() = New Byte(300 - 1) {}
                fileStream.Read(array, 0, 128)
                If array(0) <> 77 OrElse array(1) <> 90 Then
                    result = False
                Else
                    Dim num As Integer = BitConverter.ToInt32(array, 60)
                    fileStream.Seek(CLng(num), SeekOrigin.Begin)
                    fileStream.Read(array, 0, 24)
                    If array(0) <> 80 OrElse array(1) <> 69 Then
                        result = False
                    Else
                        fileStream.Read(array, 0, 224)
                        If array(0) <> 11 OrElse array(1) <> 1 Then
                            result = False
                        Else
                            Dim num2 As Integer = BitConverter.ToInt32(array, 208)
                            result = (num2 <> 0)
                        End If
                    End If
                End If
            Catch ex_A0 As IOException
                result = False
            Catch ex_A6 As UnauthorizedAccessException
                result = False
            Finally
                If fileStream IsNot Nothing Then
                    fileStream.Close()
                End If
            End Try
            Return result
        End Function

        '<PermissionSet(SecurityAction.Demand, Unrestricted = True)>
        <PermissionSet(SecurityAction.Demand)>
        Public Shared Sub GenerateCode(compileUnit As CodeCompileUnit, provider As CodeDomProvider, writer As TextWriter)
            If compileUnit Is Nothing Then
                Throw New ArgumentNullException("compileUnit")
            End If
            If provider Is Nothing Then
                Throw New ArgumentNullException("provider")
            End If
            If writer Is Nothing Then
                Throw New ArgumentNullException("writer")
            End If
            provider.GenerateCodeFromCompileUnit(compileUnit, New IndentedTextWriter(writer), New CodeGeneratorOptions())
        End Sub

        '<PermissionSet(SecurityAction.Demand, Unrestricted = True)>
        <PermissionSet(SecurityAction.Demand)>
        Public Shared Function CompileCode(compileUnit As CodeCompileUnit, provider As CodeDomProvider, referencedAssemblies As String()) As CompilerResults
            If compileUnit Is Nothing Then
                Throw New ArgumentNullException("compileUnit")
            End If
            If provider Is Nothing Then
                Throw New ArgumentNullException("provider")
            End If
            Dim compilerParameters As CompilerParameters = New CompilerParameters()
            compilerParameters.CompilerOptions = "/target:library /optimize"
            compilerParameters.GenerateExecutable = False
            compilerParameters.GenerateInMemory = True
            compilerParameters.IncludeDebugInformation = False
            compilerParameters.ReferencedAssemblies.AddRange(If((referencedAssemblies Is Nothing), CodeDomHelper.myReferencedAssemblies, referencedAssemblies))
            Return provider.CompileAssemblyFromDom(compilerParameters, New CodeCompileUnit() {compileUnit})
        End Function

    End Class
End Namespace
