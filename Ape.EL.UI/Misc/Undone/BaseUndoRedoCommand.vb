﻿Imports System.Windows.Forms

'******************************************************************************************************************
' Undo/Redo framework (c) Copyright 2009 Etienne Nijboer
'******************************************************************************************************************

Namespace Misc.Undone
    Public MustInherit Class BaseUndoRedoCommand

        Private _UndoRedoMonitor As BaseUndoRedoMonitor
        Private _UndoRedoControl As Control
        Private _UndoRedoData As Object

        Public ReadOnly Property UndoRedoMonitor() As BaseUndoRedoMonitor
            Get
                Return _UndoRedoMonitor
            End Get
        End Property

        Public ReadOnly Property UndoRedoControl() As Control
            Get
                Return _UndoRedoControl
            End Get
        End Property

        Protected Property UndoRedoData() As Object
            Get
                Return _UndoRedoData
            End Get
            Set(ByVal value As Object)
                _UndoRedoData = value
            End Set
        End Property

        Protected Sub New()
            Throw New Exception("Cannot create instance with the default constructor.")
        End Sub

        Public Sub New(ByVal AUndoRedoMonitor As BaseUndoRedoMonitor, ByVal AMonitorControl As Control)
            Me.New(AUndoRedoMonitor, AMonitorControl, Nothing)
        End Sub

        Public Sub New(ByVal AUndoRedoMonitor As BaseUndoRedoMonitor, ByVal AMonitorControl As Control, ByVal AUndoRedoData As Object)
            _UndoRedoMonitor = AUndoRedoMonitor
            _UndoRedoControl = AMonitorControl
            _UndoRedoData = AUndoRedoData
        End Sub

        Protected Sub AddCommand(ByVal UndoRedoCommandType As UndoRedoCommandType, ByVal UndoRedoCommand As BaseUndoRedoCommand)
            UndoRedoMonitor.AddCommand(UndoRedoCommandType, UndoRedoCommand)
        End Sub

        Public Overridable Sub Undo()
            AddCommand(UndoRedoCommandType.ctRedo, Activator.CreateInstance(Me.GetType, UndoRedoMonitor, UndoRedoControl))
        End Sub

        Public Overridable Sub Redo()
            AddCommand(UndoRedoCommandType.ctUndo, Activator.CreateInstance(Me.GetType, UndoRedoMonitor, UndoRedoControl))
        End Sub

        Public Overridable Sub Undo(ByVal RedoData As Object)
            AddCommand(UndoRedoCommandType.ctRedo, Activator.CreateInstance(Me.GetType, UndoRedoMonitor, UndoRedoControl, RedoData))
        End Sub

        Public Overridable Sub Redo(ByVal UndoData As Object)
            AddCommand(UndoRedoCommandType.ctUndo, Activator.CreateInstance(Me.GetType, UndoRedoMonitor, UndoRedoControl, UndoData))
        End Sub

        Public MustOverride Function CommandAsText() As String

        Public Overrides Function ToString() As String
            Return CommandAsText()
        End Function

    End Class

    '****************************************************************************************************************
    ' SimpleControl 
    ' Controls: TextBox, ComboBox, DateTimePicker, NumericUpDown, MaskedTextBox
    '****************************************************************************************************************
    Public Class SimpleControlUndoRedoCommand : Inherits BaseUndoRedoCommand

        Protected ReadOnly Property UndoRedoText() As String
            Get
                Return CStr(UndoRedoData)
            End Get
        End Property

        Public Sub New(ByVal AUndoMonitor As BaseUndoRedoMonitor, ByVal AMonitorControl As Control)
            MyBase.New(AUndoMonitor, AMonitorControl)
            UndoRedoData = UndoRedoControl.Text
        End Sub

        Public Sub New(ByVal AUndoMonitor As BaseUndoRedoMonitor, ByVal AMonitorControl As Control, ByVal AUndoRedoData As String)
            MyBase.New(AUndoMonitor, AMonitorControl, AUndoRedoData)
        End Sub

        Public Overrides Sub Undo()
            MyBase.Undo()
            UndoRedoControl.Text = UndoRedoText
        End Sub

        Public Overrides Sub Redo()
            MyBase.Redo()
            UndoRedoControl.Text = UndoRedoText
        End Sub

        Public Overrides Function CommandAsText() As String
            Return String.Format("Change to '{0}'", UndoRedoText)
        End Function

    End Class

    '****************************************************************************************************************
    ' ListBox
    '****************************************************************************************************************
    Public Class ListBoxUndoRedoCommand : Inherits BaseUndoRedoCommand

        Public Sub New(ByVal AUndoMonitor As BaseUndoRedoMonitor, ByVal AMonitorControl As Control)
            MyBase.New(AUndoMonitor, AMonitorControl)
            UndoRedoData = GetSelection()
        End Sub

        Public Sub New(ByVal AUndoMonitor As BaseUndoRedoMonitor, ByVal AMonitorControl As Control, ByVal AUndoRedoData As Object)
            MyBase.New(AUndoMonitor, AMonitorControl, AUndoRedoData)
        End Sub

        Public ReadOnly Property Control() As ListBox
            Get
                Return CType(UndoRedoControl, ListBox)
            End Get
        End Property

        Private Sub RestoreSelection()
            CType(UndoRedoMonitor, ListBoxMonitor).RestoreSelected(UndoRedoControl, CStr(UndoRedoData))
        End Sub

        Private Function GetSelection() As Object
            Return CType(UndoRedoMonitor, ListBoxMonitor).GetSelected(UndoRedoControl)
        End Function

        Public Overrides Sub Undo()
            MyBase.Undo()
            RestoreSelection()
        End Sub

        Public Overrides Sub Redo()
            MyBase.Redo()
            RestoreSelection()
        End Sub

        Public Overrides Function CommandAsText() As String
            Return String.Format("Select {0}", CStr(UndoRedoData))
        End Function
    End Class


    '****************************************************************************************************************
    ' CheckBox
    '****************************************************************************************************************
    Public Class CheckBoxUndoRedoCommand : Inherits BaseUndoRedoCommand

        Protected ReadOnly Property UndoRedoCheckState() As CheckState
            Get
                Return CType(UndoRedoData, CheckState)
            End Get
        End Property

        Public Sub New(ByVal AUndoMonitor As BaseUndoRedoMonitor, ByVal AMonitorControl As Control)
            MyBase.New(AUndoMonitor, AMonitorControl)
            UndoRedoData = Control.CheckState
        End Sub

        Public Sub New(ByVal AUndoMonitor As BaseUndoRedoMonitor, ByVal AMonitorControl As Control, ByVal AUndoRedoData As String)
            MyBase.New(AUndoMonitor, AMonitorControl, AUndoRedoData)
        End Sub

        Public ReadOnly Property Control() As CheckBox
            Get
                Return CType(UndoRedoControl, CheckBox)
            End Get
        End Property

        Public Overrides Sub Undo()
            MyBase.Undo()
            Control.CheckState = UndoRedoCheckState
        End Sub

        Public Overrides Sub Redo()
            MyBase.Redo()
            Control.CheckState = UndoRedoCheckState
        End Sub

        Public Overrides Function CommandAsText() As String
            Return String.Format("Change to '{0}'", UndoRedoCheckState.ToString)
        End Function

    End Class

    '****************************************************************************************************************
    ' RadioButton
    '****************************************************************************************************************
    Public Class RadioButtonUndoRedoCommand : Inherits BaseUndoRedoCommand

        Protected ReadOnly Property UndoRedoRadioButton() As RadioButton
            Get
                Return CType(UndoRedoData, RadioButton)
            End Get
        End Property

        Public Sub New(ByVal AUndoMonitor As BaseUndoRedoMonitor, ByVal AMonitorControl As Control)
            MyBase.New(AUndoMonitor, AMonitorControl)
            UndoRedoData = Control.Checked
        End Sub

        Public Sub New(ByVal AUndoMonitor As BaseUndoRedoMonitor, ByVal AMonitorControl As Control, ByVal AUndoRedoData As Control)
            MyBase.New(AUndoMonitor, AMonitorControl, AUndoRedoData)
        End Sub

        Public ReadOnly Property Control() As RadioButton
            Get
                Return CType(UndoRedoControl, RadioButton)
            End Get
        End Property

        Public Overrides Sub Undo()
            MyBase.Undo(UndoRedoRadioButton)
            Control.Checked = False
            If UndoRedoRadioButton IsNot Nothing Then
                UndoRedoRadioButton.Checked = True
            End If
        End Sub

        Public Overrides Sub Redo()
            MyBase.Redo(UndoRedoRadioButton)
            If UndoRedoRadioButton IsNot Nothing Then
                UndoRedoRadioButton.Checked = False
            End If
            Control.Checked = True
        End Sub

        Public Overrides Function CommandAsText() As String
            If UndoRedoRadioButton IsNot Nothing Then
                Return String.Format("Invert '{0}'/'{1}'", Control.Text, UndoRedoRadioButton.Text)
            Else
                Return String.Format("Change '{0}'", Control.Text)
            End If
        End Function

    End Class


    '****************************************************************************************************************
    ' MonthCalendar
    '****************************************************************************************************************
    Public Class MonthCalendarUndoRedoCommand : Inherits BaseUndoRedoCommand

        Protected ReadOnly Property UndoRedoSelectionRange() As SelectionRange
            Get
                Return CType(UndoRedoData, SelectionRange)
            End Get
        End Property

        Public Sub New(ByVal AUndoMonitor As BaseUndoRedoMonitor, ByVal AMonitorControl As Control)
            MyBase.New(AUndoMonitor, AMonitorControl)
            UndoRedoData = Control.SelectionRange
        End Sub

        Public Sub New(ByVal AUndoMonitor As BaseUndoRedoMonitor, ByVal AMonitorControl As Control, ByVal AUndoRedoData As SelectionRange)
            MyBase.New(AUndoMonitor, AMonitorControl, AUndoRedoData)
        End Sub

        Public ReadOnly Property Control() As MonthCalendar
            Get
                Return CType(UndoRedoControl, MonthCalendar)
            End Get
        End Property

        Public Overrides Sub Undo()
            MyBase.Undo()
            Control.SelectionRange = UndoRedoSelectionRange
        End Sub

        Public Overrides Sub Redo()
            MyBase.Redo()
            Control.SelectionRange = UndoRedoSelectionRange
        End Sub

        Public Overrides Function CommandAsText() As String
            If Date.Equals(UndoRedoSelectionRange.Start, UndoRedoSelectionRange.End) Then
                Return String.Format("Select date {0}", FormatDateTime(UndoRedoSelectionRange.Start, DateFormat.ShortDate))
            Else
            End If
            Return String.Format("Change to '{0}'", String.Format("{0} until {1}", FormatDateTime(UndoRedoSelectionRange.Start, DateFormat.ShortDate), _
                                                                                   FormatDateTime(UndoRedoSelectionRange.End, DateFormat.ShortDate)))
        End Function

    End Class
End Namespace