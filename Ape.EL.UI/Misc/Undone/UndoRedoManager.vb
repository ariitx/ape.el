﻿Imports System.Windows.Forms

'******************************************************************************************************************
' Undo/Redo framework (c) Copyright 2009 Etienne Nijboer
'******************************************************************************************************************

Namespace Misc.Undone
    Public Enum UndoRedoCommandType
        ctNone
        ctUndo
        ctRedo
    End Enum

    Public Class UndoRedoManager

#Region "UndoRedoMonitor auto register types"

        Private Shared RegisteredUndoRedoMonitorTypes As List(Of Type) = Nothing

        ' ScanAssembly
        ' The first created UndoRedoMonitor will scan the assembly for BaseUndoRedoMonitors and
        ' store these types in the monitor type list. 
        ' 
        Private Shared Sub ScanAssembly()
            If RegisteredUndoRedoMonitorTypes Is Nothing Then
                RegisteredUndoRedoMonitorTypes = New List(Of Type)
                Dim AssemblyTypes() As Type = Reflection.Assembly.GetExecutingAssembly().GetTypes()
                Dim BaseUndoRedoMonitorType As Type = GetType(BaseUndoRedoMonitor)
                For Each typeItem As Type In AssemblyTypes
                    If typeItem.BaseType Is BaseUndoRedoMonitorType Then
                        RegisteredUndoRedoMonitorTypes.Add(typeItem)
                    End If
                Next
            End If
        End Sub

#End Region

        Private Control As Control = Nothing
        Private UndoRedoMonitors As List(Of BaseUndoRedoMonitor)
        Private ExcludeControls As List(Of Control)

        ' InitializeUndoRedoMonitors
        ' When a new UndoRedoManager instance is created, a new instance of each registered monitor
        ' is created and used only within the scope of this UndoRedoManager, preventing temporary data 
        ' moved to another UndoRedoManager. This is because Each form, or group control like a panel 
        ' to make seperate undo/redo groups on a single form, can have it's own UndoRedoManager. It is
        ' of course also possible to use one global UndoRedoManager for multiple forms. This lets you
        ' control how data is seperated or combined, depending on the relation between te undo/redo commands.
        Private Sub InitializeUndoRedoMonitors()
            ScanAssembly()
            UndoRedoMonitors = New List(Of BaseUndoRedoMonitor)
            For Each typeItem In RegisteredUndoRedoMonitorTypes
                UndoRedoMonitors.Add(Activator.CreateInstance(typeItem, Me))
            Next
        End Sub

        Public Sub New()
            InitializeUndoRedoMonitors()
        End Sub

        Public Sub New(ByVal AControl As Control)
            Me.New(AControl, New List(Of Control))
        End Sub

        Public Sub New(ByVal AControl As Control, ByVal AExcludeControls As List(Of Control))
            Me.New()
            ExcludeControls = AExcludeControls
            MonitorControl(AControl)
        End Sub

        Public Sub New(ByVal AControl As Control, ByVal ParamArray AExcludeControls() As Control)
            Me.New(AControl, AExcludeControls.ToList)
        End Sub

        ' MonitorControl
        ' If a given control is not in the list of controls to exclude from undo/redo actions,
        ' an attempt is made to attach it to a matching UndoRedoMonitor. If no direct match is
        ' found, a same attempt is made for each control contained within the control recursively.
        Private Sub MonitorControl(ByVal AControl As Control)
            If Not ExcludeControls.Contains(AControl) Then
                If Not BindMonitor(AControl) Then
                    For Each ctl As Control In AControl.Controls
                        MonitorControl(ctl)
                    Next
                End If
            End If
        End Sub

        ' BindMonitor
        ' An attempt is made to bind the control to a each registered monitor. When a match is  
        ' found the search ends and the function will return true, false otherwise meaning there
        ' is no specific UndoRedoMonitor for this control.
        Private Function BindMonitor(ByVal AControl As Control) As Boolean
            Dim index As Integer = UndoRedoMonitors.Count - 1, result As Boolean = False
            While index >= 0 And Not result
                result = UndoRedoMonitors(index).Monitor(AControl)
                index -= 1
            End While
            Return result
        End Function

        Public Sub Monitor(ByVal AControl As Control)
            MonitorControl(AControl)
        End Sub

        Private undoStack As Stack(Of BaseUndoRedoCommand) = New Stack(Of BaseUndoRedoCommand)
        Private redoStack As Stack(Of BaseUndoRedoCommand) = New Stack(Of BaseUndoRedoCommand)
        Private _undoRedoCommand As UndoRedoCommandType = UndoRedoCommandType.ctNone
        Private _canUndo As Boolean = False
        Private _canRedo As Boolean = False

        Public Event CanUndoChanged(ByVal Sender As Object, ByVal CanUndo As Boolean)
        Public Event CanRedoChanged(ByVal Sender As Object, ByVal CanRedo As Boolean)
        Public Event UndoRedoStacksChanged(ByVal Sender As Object)

        Private Sub UpdateCanUndoRedo()
            Dim isCanUndoChanged As Boolean = Not (undoStack.Count > 0) = _canUndo, _
                isCanRedoChanged As Boolean = Not (redoStack.Count > 0) = _canRedo
            _canUndo = undoStack.Count > 0
            _canRedo = redoStack.Count > 0
            If isCanUndoChanged Then
                RaiseEvent CanUndoChanged(Me, _canUndo)
            End If
            If isCanRedoChanged Then
                RaiseEvent CanRedoChanged(Me, _canRedo)
            End If
            RaiseEvent UndoRedoStacksChanged(Me)
        End Sub

        Public ReadOnly Property isUndoing() As Boolean
            Get
                Return _undoRedoCommand = UndoRedoCommandType.ctUndo
            End Get
        End Property
        Public ReadOnly Property isRedoing() As Boolean
            Get
                Return _undoRedoCommand = UndoRedoCommandType.ctRedo
            End Get
        End Property
        Public ReadOnly Property isPerformingUndoRedo() As Boolean
            Get
                Return _undoRedoCommand <> UndoRedoCommandType.ctNone
            End Get
        End Property

        Public ReadOnly Property CanUndo() As Boolean
            Get
                Return _canUndo
            End Get
        End Property

        Public ReadOnly Property CanRedo() As Boolean
            Get
                Return _canRedo
            End Get
        End Property

        Public Sub AddUndoCommand(ByVal UndoRedoCommand As BaseUndoRedoCommand)
            If Not isUndoing Then
                undoStack.Push(UndoRedoCommand)
                If Not isRedoing Then
                    redoStack.Clear()
                    UpdateCanUndoRedo()
                End If
            End If
        End Sub

        Public Sub AddRedoCommand(ByVal UndoRedoCommand As BaseUndoRedoCommand)
            If Not isRedoing Then
                redoStack.Push(UndoRedoCommand)
                If Not isUndoing Then
                    UpdateCanUndoRedo()
                End If
            End If
        End Sub

        Public Sub AddCommand(ByVal UndoRedoCommandType As UndoRedoCommandType, ByVal UndoRedoCommand As BaseUndoRedoCommand)
            Select Case UndoRedoCommandType
                Case UndoRedoCommandType.ctUndo
                    AddUndoCommand(UndoRedoCommand)
                Case UndoRedoCommandType.ctRedo
                    AddRedoCommand(UndoRedoCommand)
                Case Else
                    Throw New Exception("An undo or redo command could not be accepted.")
            End Select
        End Sub

        Public Sub Undo()
            If CanUndo Then
                'Try                
                _undoRedoCommand = UndoRedoCommandType.ctUndo
                undoStack.Pop.Undo()
                'Catch e As Exception
                'Finally
                UpdateCanUndoRedo()
                _undoRedoCommand = UndoRedoCommandType.ctNone
                'End Try
            End If
        End Sub

        Public Sub Redo()
            If CanRedo Then
                _undoRedoCommand = UndoRedoCommandType.ctRedo
                redoStack.Pop.Redo()
                UpdateCanUndoRedo()
                _undoRedoCommand = UndoRedoCommandType.ctNone
            End If
        End Sub

        Protected Overrides Sub Finalize()
            MyBase.Finalize()
        End Sub


#Region "debug info"

        Public Shared Function ArrayToString(ByVal ObjectArray() As Object) As String
            Dim sb As New System.Text.StringBuilder
            For Each item As Object In ObjectArray
                sb.AppendLine(item.ToString)
            Next
            Return sb.ToString
        End Function


        Public Function GetUndoStack() As String
            Return ArrayToString(undoStack.ToArray)
        End Function

        Public Function GetRedoStack() As String
            Return ArrayToString(redoStack.ToArray)
        End Function

        Public Function GetRegisteredUndoRedoMonitorTypes() As String
            Return ArrayToString(RegisteredUndoRedoMonitorTypes.ToArray)
        End Function

#End Region

    End Class
End Namespace