﻿Imports Ape.EL.Win32.WndProcHelper.Msg
Imports System.Windows.Forms

Namespace Utils
    Public Class DrawingControl
        Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hWnd As Integer, _
                                                                    ByVal wMsg As Integer, _
                                                                    ByVal wParam As Integer,
                                                                    ByVal lParam As Integer) As Integer
        Public Shared Function MakeLong(ByVal wLow As Integer, ByVal wHigh As Integer) As Long
            MakeLong = wHigh * &H10000 + wLow
        End Function

        ' Extension methods for Control
        '<Runtime.CompilerServices.Extension()>
        Public Shared Sub ResumeDrawing(ByVal Target As Windows.Forms.Control, ByVal Redraw As Boolean)
            If Target Is Nothing OrElse Target.IsDisposed OrElse Not Target.IsHandleCreated Then
                Return
            End If

            SendMessage(Target.Handle, WM_SETREDRAW, 1, 0)
            If Redraw Then
                Target.Refresh()
            End If
        End Sub

        '<Runtime.CompilerServices.Extension()>
        Public Shared Sub SuspendDrawing(ByVal Target As Windows.Forms.Control)
            If Target Is Nothing OrElse Target.IsDisposed OrElse Not Target.IsHandleCreated Then
                Return
            End If

            SendMessage(Target.Handle, WM_SETREDRAW, 0, 0)
        End Sub

        '<Runtime.CompilerServices.Extension()>
        Public Shared Sub ResumeDrawing(ByVal Target As Windows.Forms.Control)
            ResumeDrawing(Target, True)
        End Sub

        ''' <summary>
        ''' Apply SuspendDrawing to Target and its children.
        ''' </summary>
        Public Shared Sub SuspendChildren(ByVal Target As Control)
            If Target IsNot Nothing Then
                SuspendDrawing(Target)
                For Each ctl As Control In Target.Controls
                    SuspendDrawing(ctl)
                Next
            End If
        End Sub

        ''' <summary>
        ''' Apply ResumeDrawing to Target and its children.
        ''' </summary>
        Public Shared Sub ResumeChildren(ByVal Target As Control, ByVal Redraw As Boolean)
            If Target IsNot Nothing Then
                ResumeDrawing(Target, Redraw)
                For Each ctl As Control In Target.Controls
                    ResumeDrawing(ctl, Redraw)
                Next

                If Redraw AndAlso Target.FindForm IsNot Nothing AndAlso Target.FindForm.Owner IsNot Nothing Then
                    Target.FindForm.Owner.Refresh()
                End If
            End If
        End Sub

        ''' <summary>
        ''' Apply ResumeDrawing to Target and its children.
        ''' </summary>
        Public Shared Sub ResumeChildren(ByVal Target As Control)
            ResumeChildren(Target, True)
        End Sub

        ''' <summary>
        ''' Hide focused rectangle when user focus on the target control, only on XP Style.
        ''' </summary>
        Public Shared Sub HideFocusRectangle(ByVal Target As Windows.Forms.Control)
            SendMessage(Target.Handle, WM_CHANGEUISTATE, MakeLong(UIS_HideRectangle, UISF_FocusRectangle), 0)
        End Sub

        ''' <summary>
        ''' Show focused rectangle when user focus on the target control, only on XP Style.
        ''' </summary>
        Public Shared Sub ShowFocusRectangle(ByVal Target As Windows.Forms.Control)
            SendMessage(Target.Handle, WM_CHANGEUISTATE, MakeLong(UIS_ShowRectangle, UISF_FocusRectangle), 0)
        End Sub


        Public Shared Sub SetDoubleBuffering(control As System.Windows.Forms.Control, value As Boolean)
            Dim controlProperty As System.Reflection.PropertyInfo = GetType(System.Windows.Forms.Control).GetProperty("DoubleBuffered", System.Reflection.BindingFlags.NonPublic Or System.Reflection.BindingFlags.Instance)
            controlProperty.SetValue(control, value, Nothing)
        End Sub
    End Class
End Namespace

'I found that making a Windows API call to disable painting during the layout completely solved this problem and also speeded up the layout as a bonus.
'Normal layout time: 9.4 seconds; with BeginUpdate/EndUpdate: 4.8 seconds; with BeginUpdate/EndUpdate and Suspend Paint: 1.2 seconds.
'https://www.devexpress.com/Support/Center/Question/Details/Q423017

'code usage

'ControlHelper.SuspendRedraw(layoutControl)
'layoutControl.BeginUpdate()

'' Do layout creation here
'Try
'Finally
'	layoutControl.EndUpdate()
'	ControlHelper.ResumeRedraw(layoutControl)
'End Try