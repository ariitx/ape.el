﻿Imports System
Imports System.Drawing
Imports System.Drawing.Drawing2D
Imports System.Windows.Forms

Namespace Utils
    Public Class PanelUtils
        Friend Sub New()
        End Sub

        Public Shared Sub FillPanelGradient(panel As Panel, color1 As Color, color2 As Color, gradientMode As LinearGradientMode)
            Dim linearGradientBrush As LinearGradientBrush = New LinearGradientBrush(New RectangleF(0.0F, 0.0F, CSng(panel.Width), CSng(panel.Height)), color1, color2, gradientMode)
            Dim graphics As Graphics = panel.CreateGraphics()
            graphics.FillRectangle(linearGradientBrush, New RectangleF(0.0F, 0.0F, CSng(panel.Width), CSng(panel.Height)))
            graphics.Dispose()
            linearGradientBrush.Dispose()
        End Sub

        Public Shared Sub DrawHeaderPanel(caption As String, panel As Panel, color1 As Color, color2 As Color, gradientMode As LinearGradientMode)
            FillPanelGradient(panel, color1, color2, gradientMode)
            Dim graphics As Graphics = panel.CreateGraphics()
            Dim font As Font = New Font("Arial", 16.0F, FontStyle.Bold)
            graphics.DrawString(caption, font, Brushes.White, New Point(8, 8))
            font.Dispose()
            graphics.Dispose()
        End Sub

        Public Shared Sub DrawHintPanel(hint As String, panel As Panel, color1 As Color, color2 As Color, gradientMode As LinearGradientMode)
            FillPanelGradient(panel, color1, color2, gradientMode)
            Dim graphics As Graphics = panel.CreateGraphics()
            Dim font As Font = New Font("Verdana", 8.0F, FontStyle.Bold)
            graphics.DrawString("Hint", font, Brushes.GreenYellow, New Point(8, 4))
            font.Dispose()
            font = New Font("Verdana", 8.0F)
            graphics.DrawString(hint, font, Brushes.Honeydew, New RectangleF(8.0F, 18.0F, CSng((panel.Width - 16)), CSng((panel.Height - 18))))
            font.Dispose()
            graphics.Dispose()
        End Sub

        Public Shared Sub Draw3DHorizontalBar(panel As Panel, borderColor As Color, color1 As Color, color2 As Color)
            Dim pen As Pen = New Pen(borderColor)
            Dim linearGradientBrush As LinearGradientBrush = New LinearGradientBrush(New RectangleF(0.0F, 0.0F, CSng(panel.Width), CSng(panel.Height)), color1, color2, LinearGradientMode.Vertical)
            Dim graphics As Graphics = panel.CreateGraphics()
            graphics.DrawLine(pen, 0, 0, panel.Width - 1, 0)
            graphics.DrawLine(pen, 0, panel.Height - 1, panel.Width - 1, panel.Height - 1)
            graphics.FillRectangle(linearGradientBrush, New RectangleF(0.0F, 1.0F, CSng(panel.Width), CSng((panel.Height - 2))))
            graphics.Dispose()
            linearGradientBrush.Dispose()
            pen.Dispose()
        End Sub

        Public Shared Sub Draw3DVerticalBar(panel As Panel, borderColor As Color, color1 As Color, color2 As Color)
            Dim pen As Pen = New Pen(borderColor)
            Dim linearGradientBrush As LinearGradientBrush = New LinearGradientBrush(New RectangleF(0.0F, 0.0F, CSng(panel.Width), CSng(panel.Height)), color1, color2, LinearGradientMode.Horizontal)
            Dim graphics As Graphics = panel.CreateGraphics()
            graphics.DrawLine(pen, 0, 0, 0, panel.Height - 1)
            graphics.DrawLine(pen, panel.Width - 1, 0, panel.Width - 1, panel.Height - 1)
            graphics.FillRectangle(linearGradientBrush, New RectangleF(1.0F, 0.0F, CSng((panel.Width - 2)), CSng(panel.Height)))
            graphics.Dispose()
            linearGradientBrush.Dispose()
            pen.Dispose()
        End Sub
    End Class
End Namespace
