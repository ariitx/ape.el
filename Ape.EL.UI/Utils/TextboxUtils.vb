﻿Imports System.Windows.Forms

Namespace Utils
    Public Class TextBoxUtils
        ''' <summary>
        ''' Hide caret when focused to textbox. Set to readonly.
        ''' </summary>
        Public Shared Sub ReadOnlyTextBox(medit As TextBox, Optional ByVal apply As Boolean = True)
            medit.ReadOnly = apply

            RemoveHandler medit.GotFocus, AddressOf medit_GotFocus
            RemoveHandler medit.MouseMove, AddressOf medit_MouseMove

            If apply Then
                AddHandler medit.GotFocus, AddressOf medit_GotFocus
                AddHandler medit.MouseMove, AddressOf medit_MouseMove
            End If
        End Sub

        Private Shared Sub medit_GotFocus(sender As Object, e As EventArgs)
            Ape.EL.Win32.User32.HideCaret(DirectCast(sender, TextBox).Handle)
        End Sub

        Private Shared Sub medit_MouseMove(sender As Object, e As MouseEventArgs)
            Ape.EL.Win32.User32.HideCaret(DirectCast(sender, TextBox).Handle)
        End Sub
    End Class
End Namespace
