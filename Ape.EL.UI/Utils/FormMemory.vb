﻿Imports System.Drawing
Imports System.Windows.Forms

Namespace Utils
    ''' <summary>
    ''' A class that able to remember your form's last position and size.
    ''' </summary>
    ''' <remarks></remarks>
    Public Class FormMemory
        Implements IDisposable

        Private Const STR_SIZE_WIDTH As String = "SIZE_WIDTH"
        Private Const STR_SIZE_HEIGHT As String = "SIZE_HEIGHT"
        Private Const STR_LOCATION_X As String = "LOCATION_X"
        Private Const STR_LOCATION_Y As String = "LOCATION_Y"
        Private Const STR_WINDOWSTATE As String = "WINDOWSTATE"
        Private Const STR_FORMMEMORY As String = "FORM_MEMORY"

        Private m_Form As Windows.Forms.Form
        Private m_ID As String
        Private m_OriginalState As FormWindowState

#Region "Size and Location"
        ''' <summary>
        ''' Save keyboard size.
        ''' </summary>
        Private Sub SaveSize()
            If m_Form Is Nothing Then
                Exit Sub
            End If

            Ape.EL.Setting.MySetting.SaveSettingEx(STR_FORMMEMORY, m_ID & STR_SIZE_WIDTH, m_Form.Size.Width)
            Ape.EL.Setting.MySetting.SaveSettingEx(STR_FORMMEMORY, m_ID & STR_SIZE_HEIGHT, m_Form.Size.Height)
        End Sub

        ''' <summary>
        ''' Save keyboard position.
        ''' </summary>
        Private Sub SaveLocation()
            If m_Form Is Nothing Then
                Exit Sub
            End If

            Ape.EL.Setting.MySetting.SaveSettingEx(STR_FORMMEMORY, m_ID & STR_LOCATION_X, m_Form.Location.X)
            Ape.EL.Setting.MySetting.SaveSettingEx(STR_FORMMEMORY, m_ID & STR_LOCATION_Y, m_Form.Location.Y)
        End Sub

        ''' <summary>
        ''' Put in shown or load.
        ''' </summary>
        Sub LoadSizeAndLocation()
            If m_Form Is Nothing Then
                Exit Sub
            End If

            Dim mySize As Size
            Dim myLocation As Point

            mySize.Width = Ape.EL.Setting.MySetting.GetSettingEx(STR_FORMMEMORY, m_ID & STR_SIZE_WIDTH, 0)
            mySize.Height = Ape.EL.Setting.MySetting.GetSettingEx(STR_FORMMEMORY, m_ID & STR_SIZE_HEIGHT, 0)

            If mySize.Width > 0 And mySize.Height > 0 Then
                m_Form.Size = mySize
            Else
                SaveSize()
            End If

            myLocation.X = Ape.EL.Setting.MySetting.GetSettingEx(STR_FORMMEMORY, m_ID & STR_LOCATION_X, (Integer.MaxValue - 9999) * -1)
            myLocation.Y = Ape.EL.Setting.MySetting.GetSettingEx(STR_FORMMEMORY, m_ID & STR_LOCATION_Y, (Integer.MaxValue - 9999) * -1)

            'Check if form is maximized first and change it to normal to be able to move location
            If m_Form.WindowState = FormWindowState.Maximized Then
                m_OriginalState = m_Form.WindowState
                m_Form.WindowState = FormWindowState.Normal
            End If

            'Use saved setting
            If myLocation.X <> (Integer.MaxValue - 9999) * -1 And myLocation.Y <> (Integer.MaxValue - 9999) * -1 Then
                m_Form.Location = myLocation
            End If

            'Restore back WindowsState if it was originally maximized
            If m_OriginalState = FormWindowState.Maximized Then
                m_Form.WindowState = m_OriginalState
            End If

            'Check if form is in boundary
            If Not WinForm.General.IsOnScreen(m_Form) Then
                m_Form.Location = WinForm.General.GetCenterPosition(m_Form, m_Form.Owner)
                SaveLocation()
            End If
        End Sub
#End Region

#Region "Windowstate"
        ''' <summary>
        ''' Save last windowstate.
        ''' </summary>
        Private Sub SaveState()
            If m_Form Is Nothing Then
                Exit Sub
            End If

            Ape.EL.Setting.MySetting.SaveSettingEx(STR_FORMMEMORY, m_ID & STR_WINDOWSTATE, m_Form.WindowState)
        End Sub

        ''' <summary>
        ''' Put in shown or load.
        ''' </summary>
        Sub LoadWindowState()
            If m_Form Is Nothing Then
                Exit Sub
            End If

            Dim myState As Integer = -1

            myState = Ape.EL.Setting.MySetting.GetSettingEx(STR_FORMMEMORY, m_ID & STR_WINDOWSTATE, -1)

            If myState >= 0 Then
                m_Form.WindowState = myState
            Else
                SaveState()
            End If
        End Sub
#End Region

        ''' <summary>
        ''' Remember the last form location and position based on RememberSizePos, saving / restoring the windowstate based on RememberState.
        ''' </summary>
        Public Sub New(ByRef theForm As Windows.Forms.Form, ByVal ID As String, ByVal RememberSizePos As Boolean, ByVal RememberState As Boolean)
            m_Form = theForm
            m_ID = ID

            If m_Form IsNot Nothing Then
                If String.IsNullOrEmpty(m_ID) Then
                    m_ID = Right(Ape.EL.Utility.Data.Crypt.GetHash(m_Form.ToString), 15)
                End If

                If RememberSizePos Then LoadSizeAndLocation()
                If RememberState Then LoadWindowState()

                AddHandler m_Form.Disposed, Sub() Dispose()
            Else
                Debug.Assert(False, "theForm parameter is nothing or object has been disposed!")
            End If
        End Sub

        ''' <summary>
        ''' Remember the last form location and position only, not saving / restoring the windowstate.
        ''' </summary>
        Public Sub New(ByRef theForm As Windows.Forms.Form, ByVal ID As String)
            Me.New(theForm, ID, True, False)
        End Sub

        ''' <summary>
        ''' Remember the last form location and position only, not saving / restoring the windowstate. ID is automatically generated.
        ''' </summary>
        Public Sub New(ByRef theForm As Windows.Forms.Form)
            Me.New(theForm, "")
        End Sub

#Region "IDisposable Support"
        Private disposedValue As Boolean ' To detect redundant calls

        ' IDisposable
        Protected Overridable Sub Dispose(disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                    ' TODO: dispose managed state (managed objects).
                    SaveSize()
                    SaveLocation()
                    SaveState()
                End If

                ' TODO: free unmanaged resources (unmanaged objects) and override Finalize() below.
                ' TODO: set large fields to null.
            End If
            Me.disposedValue = True
        End Sub

        ' TODO: override Finalize() only if Dispose(ByVal disposing As Boolean) above has code to free unmanaged resources.
        'Protected Overrides Sub Finalize()
        '    ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        '    Dispose(False)
        '    MyBase.Finalize()
        'End Sub

        ' This code added by Visual Basic to correctly implement the disposable pattern.
        Public Sub Dispose() Implements IDisposable.Dispose
            ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub
#End Region
    End Class
End Namespace
