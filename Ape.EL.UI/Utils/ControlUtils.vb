﻿Imports System.Windows.Forms
Imports System.Drawing
Imports System.Globalization
Imports Ape.EL.UI.Extensions

Namespace Utils
    Public Class ControlUtils
        <Obsolete("Use Controls.Clear(True) extension from Lib.UI.Extensions")>
        Public Shared Sub ClearDispose(cnt As Control)
            cnt.Controls.Clear(True)
        End Sub

        ''' <summary>
        ''' Draw string in middle of control (eg. put on Paint event in form or panel).
        ''' </summary>
        Public Shared Sub DefaultDrawEmptyForeground(ByVal customDrawEventArgs_0 As PaintEventArgs, ByVal [text] As String)
            Dim font As Font
            If (CultureInfo.CurrentUICulture.TwoLetterISOLanguageName = "zh") Then
                font = New Font("SimSun", 36.0!, FontStyle.Bold)
            Else
                font = New Font("Tahoma", 36.0!, FontStyle.Bold)
            End If
            Dim ef As SizeF = customDrawEventArgs_0.Graphics.MeasureString([text], font)
            Dim x As Single = ((customDrawEventArgs_0.ClipRectangle.Width - ef.Width) / 2.0!)
            Dim y As Single = (((customDrawEventArgs_0.ClipRectangle.Height - ef.Height) / 2.0!) + 14.0!)
            If (y < 14.0!) Then
                y = 14.0!
            End If
            Dim point As New PointF(x, y)
            customDrawEventArgs_0.Graphics.DrawString([text], font, Brushes.DarkGray, point)
            font.Dispose()
        End Sub

        <Obsolete("testonly", True)>
        Public Shared Sub AllowMove(sender As Control)
            Static sta As Boolean
            Static ptX, ptY As Integer

            AddHandler sender.MouseDown, Sub(x As Control, e As MouseEventArgs)
                                             If e.Button = Windows.Forms.MouseButtons.Left Then
                                                 Dim f As Form = DirectCast(x, Control).FindForm
                                                 If f IsNot Nothing Then
                                                     sta = True
                                                     ptX = e.X : ptY = e.Y

                                                 End If
                                             End If
                                         End Sub
            AddHandler sender.MouseMove, Sub(x As Control, e As MouseEventArgs)
                                             If sta Then
                                                 Dim c As Control = DirectCast(x, Control)
                                                 Dim xxx As Long = sender.Location.X + e.X - ptX
                                                 Dim yyy As Long = sender.Location.Y + e.Y - ptY

                                                 x.Parent.Location = New Point(xxx, yyy)
                                                 x.Parent.Update()
                                             End If
                                         End Sub
            AddHandler sender.MouseUp, Sub(x As Control, e As MouseEventArgs)
                                           sta = False
                                       End Sub
        End Sub
    End Class
End Namespace
