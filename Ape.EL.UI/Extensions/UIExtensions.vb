﻿Imports System.Runtime.CompilerServices
Imports System.Windows.Forms
Imports System.Drawing
Imports System.Reflection

Namespace Extensions
    ''' <summary>
    ''' This is an extensions module. Contains extensions for UI related instance. Eg. Control type.
    ''' Last modify on 19 May 2016 - Ari
    ''' </summary>
    <HideModuleName()> _
    Public Module UIExtensions
        ''' <summary>Colour to be used as background colour. </summary>
        Public ReadOnly DefaultRequiredBackColour As Color = Color.FromArgb(255, 224, 192)

        ''' <summary>
        ''' Change BackColor to a DefaultRequiredBackColor / mandatory field colour.
        ''' </summary>
        <Extension>
        Public Sub SetBackColorRequired(sender As Control)
            sender.BackColor = DefaultRequiredBackColour
        End Sub

        ''' <summary>
        ''' Dispose the collection items when clearing.
        ''' </summary>
        <Extension>
        Public Sub Clear(controls As Control.ControlCollection, dispose As Boolean)
            If controls.Count > 0 Then
                For i As Integer = controls.Count - 1 To 0 Step -1
                    If dispose Then
                        controls(i).Dispose()
                    Else
                        controls.RemoveAt(i)
                    End If
                Next
            End If
        End Sub

        ''' <summary>
        ''' Dispose the collection items when clearing.
        ''' </summary>
        <Extension>
        Public Sub Clear(Of T)(collection As List(Of T), dispose As Boolean)
            ClearList(collection, dispose)
        End Sub

#Region "Drawing Control extensions functions"
        ''' <summary>
        ''' Force suspend layout.
        ''' </summary>
        <Extension>
        Public Sub SuspendDrawing(sender As Control)
            Ape.EL.UI.Utils.DrawingControl.SuspendDrawing(sender)
        End Sub

        ''' <summary>
        ''' Force resume layout.
        ''' </summary>
        <Extension>
        Public Sub ResumeDrawing(sender As Control)
            Ape.EL.UI.Utils.DrawingControl.ResumeDrawing(sender)
        End Sub

        ''' <summary>
        ''' Force resume layout.
        ''' </summary>
        <Extension>
        Public Sub ResumeDrawing(sender As Control, ByVal Redraw As Boolean)
            Ape.EL.UI.Utils.DrawingControl.ResumeDrawing(sender, Redraw)
        End Sub

        ''' <summary>
        ''' Force resume layout on Target and children.
        ''' </summary>
        <Extension>
        Public Sub ResumeChildren(ByVal Target As Control, ByVal Redraw As Boolean)
            Ape.EL.UI.Utils.DrawingControl.ResumeChildren(Target, Redraw)
        End Sub

        ''' <summary>
        ''' Force resume layout on Target and children.
        ''' </summary>
        <Extension>
        Public Sub ResumeChildren(ByVal Target As Control)
            Ape.EL.UI.Utils.DrawingControl.ResumeChildren(Target)
        End Sub

        ''' <summary>
        ''' Force suspend layout on Target and children.
        ''' </summary>
        <Extension>
        Public Sub SuspendChildren(ByVal Target As Control)
            Ape.EL.UI.Utils.DrawingControl.SuspendChildren(Target)
        End Sub

        ''' <summary>
        ''' Hide marquee rectangle on control when focused, only on XP Style.
        ''' </summary>
        <Extension>
        Public Sub HideFocusRectangle(sender As Control)
            Ape.EL.UI.Utils.DrawingControl.HideFocusRectangle(sender)
        End Sub

        ''' <summary>
        ''' Show marquee rectangle on control when focused, only on XP Style.
        ''' </summary>
        <Extension>
        Public Sub ShowFocusRectangle(sender As Control)
            Ape.EL.UI.Utils.DrawingControl.ShowFocusRectangle(sender)
        End Sub

        ''' <summary>
        ''' Set double buffering.
        ''' </summary>
        <Extension>
        Public Sub SetDoubleBuffering(sender As Control, value As Boolean)
            Ape.EL.UI.Utils.DrawingControl.SetDoubleBuffering(sender, value)
        End Sub
#End Region

#Region "Controls"
        ''' <summary>
        ''' Check for control's true visibility (through reflection) without the manipulation from parent or owner.
        ''' </summary>
        <Extension>
        Public Function VisibleSelf(ctl As Control) As Boolean
            Return Reflex.PropertyHelper.Invoke(Of Boolean)(ctl, "GetState", New Object() {2})
        End Function

        ''' <summary>
        ''' Set for control's true visibility (through reflection) without the manipulation from parent or owner.
        ''' </summary>
        <Extension>
        Public Sub VisibleSelf(ByRef ctl As Control, vis As Boolean)
            Reflex.PropertyHelper.Invoke(Of Object)(ctl, "SetState", New Object() {2, vis})
        End Sub

        ''' <summary>
        ''' Extension method to return if the control is in design mode.
        ''' </summary>
        ''' <param name="control">Control to examine</param>
        ''' <returns>True if in design mode, otherwise false</returns>
        ''' <remarks>https://www.codeproject.com/tips/447319/resolve-designmode-for-a-user-control</remarks>
        <Extension> _
        Public Function IsInDesignMode(control As System.Windows.Forms.Control) As Boolean
            Return ResolveDesignMode(control)
        End Function

        ''' <summary>
        ''' Method to test if the control or it's parent is in design mode.
        ''' </summary>
        ''' <param name="control">Control to examine</param>
        ''' <returns>True if in design mode, otherwise false</returns>
        Private Function ResolveDesignMode(control As System.Windows.Forms.Control) As Boolean
            Dim designModeProperty As System.Reflection.PropertyInfo
            Dim designMode As Boolean

            ' Get the protected property
            designModeProperty = control.[GetType]().GetProperty("DesignMode", System.Reflection.BindingFlags.Instance Or System.Reflection.BindingFlags.NonPublic)

            ' Get the controls DesignMode value
            designMode = CBool(designModeProperty.GetValue(control, Nothing))

            ' Test the parent if it exists
            If control.Parent IsNot Nothing Then
                designMode = designMode Or ResolveDesignMode(control.Parent)
            End If

            Return designMode
        End Function
#End Region

#Region "Control Finder"
        ''' <summary>
        ''' Get all controls and components out of a control container through reflection.
        ''' This is enhanced function of WinForm.General.FindAllControls.
        ''' Be careful of performance slow down!
        ''' </summary>
        <Extension>
        Public Function FindCtl(Of T)(container As T) As List(Of Object)
            Dim objList As New List(Of Object)

            'Populate the object to the list.
            TrueFindCtl(container, objList)

            Return objList
        End Function

        ''' <summary>
        ''' Populate controls and components to a list of object.
        ''' Internal use by FindCtl(Of T)
        ''' Forms within container form will be ignored.
        ''' </summary>
        Private Sub TrueFindCtl(container As Object, objList As IList(Of Object))
            Static ReflectionBindingAll As BindingFlags = BindingFlags.Instance Or BindingFlags.Static Or BindingFlags.Public Or BindingFlags.NonPublic
            Static MustSkipProperty As List(Of String) = New String() {"Owner", "Parent", "MdiParent"}.ToList

            'To make sure we dont cause stack overflow / infinite loop, we keep track of looped objects.
            Static lastContainer As New List(Of Object)

            If lastContainer.Contains(container) Then
                Exit Sub
            End If

            'Ignore any type of Form if objList already contains an item or more.
            If objList.Count > 0 AndAlso GetType(System.Windows.Forms.Form).IsAssignableFrom(container.GetType) Then
                Exit Sub
            End If

            'Keeping track...
            lastContainer.Add(container)

            'Add into the list if not yet added. Return to previous stack if previously added.
            If objList Is Nothing Then objList = New List(Of Object)
            If Not objList.Contains(container) Then
                If IsControl(container.GetType) Then
                    objList.Add(container)
                End If
            Else
                lastContainer.Remove(container)
                Exit Sub
            End If

            'Find current container's properties.
            Dim prp As PropertyInfo() = container.GetType.GetProperties
            'Only get property value without any parameter and make sure it's only CollectionBase type or Control type (except Form type).
            prp = (From x As PropertyInfo In prp
                   Where x.GetIndexParameters.Count = 0 AndAlso
                         Not GetType(System.Windows.Forms.Form).IsAssignableFrom(x.PropertyType) AndAlso
                         Not MustSkipProperty.Contains(x.Name) AndAlso
                        ((x.CanRead And GetType(CollectionBase).IsAssignableFrom(x.PropertyType)) OrElse
                         (x.CanRead And GetType(System.Windows.Forms.Control.ControlCollection).IsAssignableFrom(x.PropertyType)) OrElse
                         (x.CanWrite And x.CanRead And IsControl(x.PropertyType)))
                   Select x).ToArray
            'Try to get the property value from container.
            For Each pi As PropertyInfo In prp
                Try
                    Dim oItem As Object = pi.GetValue(container, Nothing)
                    If oItem IsNot Nothing Then
                        'If property is CollectionBase, we try to determine the member type and loop through it and recurse if it IsControl.
                        'If property is Control.ControlCollection, we loop through it and recurse.
                        'Otherwise if it IsControl, we recurse.
                        If GetType(CollectionBase).IsAssignableFrom(pi.PropertyType) Then
                            Dim oCollection As CollectionBase = DirectCast(oItem, CollectionBase)
                            Dim oCollectionType As Type = oCollection.GetMemberType
                            'Skip if it's Form type.
                            If Not GetType(System.Windows.Forms.Form).IsAssignableFrom(oCollectionType) AndAlso IsControl(oCollectionType) Then
                                For i As Integer = 0 To oCollection.Count - 1
                                    TrueFindCtl(oCollection(i), objList)
                                Next
                            End If
                        ElseIf GetType(System.Windows.Forms.Control.ControlCollection).IsAssignableFrom(pi.PropertyType) Then
                            Dim oCollection As System.Windows.Forms.Control.ControlCollection = DirectCast(oItem, System.Windows.Forms.Control.ControlCollection)
                            For i As Integer = 0 To oCollection.Count - 1
                                'Skip if it's Form type.
                                If Not GetType(System.Windows.Forms.Form).IsAssignableFrom(oCollection(i).GetType) Then
                                    TrueFindCtl(oCollection(i), objList)
                                End If
                            Next
                        ElseIf IsControl(pi.PropertyType) Then
                            TrueFindCtl(oItem, objList)
                        End If
                    End If
                Catch ex As Exception
                    Debug.Assert(False, ex.Message)
                End Try
            Next

            'Get all FieldInfo (variable info) that exist in this obj. But filtered by its type being Component and Control type only.
            Dim fieldList As List(Of FieldInfo)
            fieldList = (From x As FieldInfo In container.GetType.GetFields(ReflectionBindingAll).ToList
                         Where IsControl(x.FieldType)
                         Select x).ToList

            'Loop through each FieldInfo and get the real variable object from obj.
            For Each field As FieldInfo In fieldList
                Dim oItem As Object = field.GetValue(container)               'Get the real variable object.
                If oItem IsNot Nothing Then
                    TrueFindCtl(oItem, objList)
                End If
            Next

            objList = objList.Distinct.ToList

            lastContainer.Remove(container)
        End Sub
#End Region

#Region "Visibility Checker"
        ''' <summary>
        ''' Plot a grid with total number of points. Points are pointed on screen.
        ''' </summary>
        Public Function GridPointCollector(control As Control, totalPoints As Integer) As List(Of Point)
            Dim lPointsToCheck As New List(Of Point)
            Dim useGap As Boolean = True
            Dim pointPerRow As Integer = Math.Ceiling(Math.Sqrt(totalPoints))
            Dim minPointGap As Integer = If(useGap, 3, Int16.MaxValue)
            Dim r As Integer = control.Top + 1
            Dim c As Integer = control.Left + 1
            Dim dWidth As Integer = (control.Right - 1 - control.Left + 1) \ pointPerRow
            Dim dHeight As Integer = (control.Bottom - 1 - control.Top + 1) \ pointPerRow

            Dim lastRow As Boolean
            Dim lastCol As Boolean
            Dim cGap As Integer = control.Right - 1 - (c + (pointPerRow * dWidth))
            Dim rGap As Integer = control.Bottom - 1 - (r + (pointPerRow * dHeight))
            Dim cExit As Boolean = False And Math.Abs(cGap) > minPointGap
            Dim rExit As Boolean = False And Math.Abs(rGap) > minPointGap

            While True
                If c > control.Right - 1 Then
                    If cExit Then Exit While
                    lastRow = True
                    c = control.Right - 1
                End If

                r = control.Top + 1
                lastCol = False
                While True
                    If r > control.Bottom - 1 Then
                        If rExit Then Exit While
                        lastCol = True
                        r = control.Bottom - 1
                    End If

                    lPointsToCheck.Add(New Point(c, r))

                    r += dHeight
                    If lastCol Then Exit While
                End While

                c += dWidth
                If lastRow Then Exit While
            End While

            Return lPointsToCheck
        End Function

        ''' <summary>
        ''' Checks if a control is actually visible to the user completely.
        ''' http://stackoverflow.com/a/16760756
        ''' Check more on https://stackoverflow.com/questions/34025833/given-a-hwnd-determine-if-that-window-is-not-hidden-by-other-windows-z-orderin
        ''' </summary>
        ''' <param name="control">The control to check.</param>
        ''' <returns>True, if the control is completely visible, false else.</returns>
        ''' <remarks>This is not 100% accurate.</remarks>
        <Extension>
        Public Function IsControlVisibleToUser(ByVal control As Windows.Forms.Control) As Boolean
            If Not control.Visible Then Return False

            Dim bAllPointsVisible As Boolean = True
            Dim lPointsToCheck As New List(Of Point)

            lPointsToCheck = GridPointCollector(control, 300)

            'Check each point. If all points return the handle of the control,
            'the control should be visible to the user.
            For Each oPoint In lPointsToCheck
                Dim sPoint As New Ape.EL.Win32.User32.PointStruct() With {
                    .x = oPoint.X, _
                    .y = oPoint.Y _
                }
                bAllPointsVisible = bAllPointsVisible And ( _
                    (Ape.EL.Win32.User32.WindowFromPoint(sPoint) = control.Handle) _
                )
            Next

            Return bAllPointsVisible
        End Function

        ''' <summary>
        ''' Same as IsControlVisibleToUser, but only to forms that are created from main program.
        ''' </summary>
        ''' <returns>True, if the control is completely visible, false else.</returns>
        <Extension> <Obsolete("Is not reliable anymore since ActiveFormStack is removed.")>
        Public Function IsControlVisibleToProgram(ByVal control As Windows.Forms.Control) As Boolean
            If Not control.Visible Then Return False

            Dim bAllPointsVisible As Boolean = True
            Dim lPointsToCheck As New List(Of Point)

            lPointsToCheck = GridPointCollector(control, 300)

            Dim programOwnedForm As List(Of IntPtr) = Nothing
            'programOwnedForm = (From n As Form In Ape.EL.WinForm.General.ActiveFormStack Where n IsNot Nothing AndAlso n.IsDisposed = False Select n.Handle).ToList
            If programOwnedForm Is Nothing Then programOwnedForm = New List(Of IntPtr)

            'Check each point. If all points return the handle of the control,
            'the control should be visible to the user.
            For Each oPoint In lPointsToCheck
                Dim sPoint As New Ape.EL.Win32.User32.PointStruct() With {
                    .x = oPoint.X, _
                    .y = oPoint.Y _
                }
                Dim hPoint As IntPtr = Ape.EL.Win32.User32.WindowFromPoint(sPoint) 'the overlapping window handle (may or may not created by main program)
                Dim isMyForm As Boolean = programOwnedForm.Contains(hPoint) 'determine if the window handle is from main program
                If isMyForm Or programOwnedForm.Count = 0 Then
                    bAllPointsVisible = bAllPointsVisible And (hPoint = control.Handle)
                Else
                    bAllPointsVisible = bAllPointsVisible And True
                End If
                If Not bAllPointsVisible Then Exit For 'if false, directly exit for and return false.
            Next

            Return bAllPointsVisible
        End Function
#End Region

#Region "Front Most"
        ''' <summary>
        ''' Allow form to stay in front of other main program's visible forms.
        ''' Like TopMost, but only on top of main program's forms.
        ''' Experimental function, use with caution.
        ''' </summary>
        <Extension> <Obsolete("Is not reliable anymore since ActiveFormStack is removed.")>
        Public Sub FrontMost(sender As Form, enable As Boolean)
            Ape.EL.App.General.RunMethodOnNewThread(Sub()
                                                        sender.SetExProperty("FrontMost", enable)
                                                        sender.SetExProperty("WaitFrontMost", False)

                                                        Dim isEnabled As Boolean = enable
                                                        If isEnabled Then
                                                            '1. Set other FrontMost form to wait
                                                            Dim tmp1 As List(Of Form) = New List(Of Form) '= Ape.EL.WinForm.General.ActiveFormStack.ToArray().ToList()
                                                            tmp1 = (From x As Form In tmp1 Where Not (x Is Nothing OrElse x.IsDisposed) AndAlso x.GetExPropertyStr("FrontMost").ToBoolean Select x).ToList
                                                            For Each f1 As Form In tmp1
                                                                If f1 IsNot sender Then f1.SetExProperty("WaitFrontMost", True)
                                                            Next

                                                            '2. Begin timer
                                                            Dim wait As Boolean
                                                            Dim isDisposed As Boolean
                                                            Dim isVisible As Boolean
                                                            Dim isVisibleToProgram As Boolean

                                                            While enable
                                                                Threading.Thread.Sleep(100)

                                                                If App.General.GetIdle > 5000 Then
                                                                    Threading.Thread.Sleep(20)
                                                                    Continue While
                                                                End If

                                                                wait = False
                                                                isDisposed = False
                                                                isVisible = True
                                                                isVisibleToProgram = True

                                                                Try
                                                                    sender.InvokeIfRequired(Sub(x As Form)
                                                                                                enable = x.GetExPropertyStr("FrontMost").ToBoolean
                                                                                                wait = x.GetExPropertyStr("WaitFrontMost").ToBoolean
                                                                                                isDisposed = x Is Nothing OrElse x.IsDisposed
                                                                                                isVisible = x.Visible
                                                                                            End Sub)

                                                                    If isDisposed OrElse Not enable Then
                                                                        Exit While
                                                                    End If

                                                                    If wait Then
                                                                        Continue While
                                                                    End If

                                                                    sender.InvokeIfRequired(Sub(x As Form)
                                                                                                isVisibleToProgram = x.IsControlVisibleToProgram
                                                                                            End Sub)

                                                                    If isVisible Then
                                                                        If Not isVisibleToProgram Then
                                                                            sender.InvokeIfRequired(Sub(x) sender.BringToFront())
                                                                        End If
                                                                    End If
                                                                Catch ex As Exception
                                                                End Try
                                                            End While

                                                            '3. Disable WaitFrontMost for the last activated FrontMost
                                                            Dim tmp2 As List(Of Form) = New List(Of Form) '= Ape.EL.WinForm.General.ActiveFormStack.ToArray().ToList()
                                                            tmp2 = (From x As Form In tmp2 Where Not (x Is Nothing OrElse x.IsDisposed) AndAlso x.GetExPropertyStr("FrontMost").ToBoolean Select x).ToList
                                                            Dim f2 As Form = tmp2.LastOrDefault
                                                            If f2 IsNot Nothing Then
                                                                f2.SetExProperty("WaitFrontMost", False)
                                                            End If
                                                        End If
                                                    End Sub)
        End Sub
#End Region
    End Module
End Namespace
