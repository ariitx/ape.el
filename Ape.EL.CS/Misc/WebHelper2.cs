﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web;

namespace Ape.EL.Misc
{
    public class WebHelper2
    {
        public class RequestHeader
        {
            public RequestHeader() { }
            public RequestHeader(string _name, string _value) { this.name = _name; this.value = _value; }

            public string name { get; set; }
            public string value { get; set; }
        }

        public class OwinToken
        {
            public string AccessToken { get { return _AccessToken; } }
            private string _AccessToken;
            public string TokenType { get { return _TokenType; } }
            private string _TokenType;
            public string RefreshToken { get { return _RefreshToken; } }
            private string _RefreshToken;
            public DateTime TokenExpiry { get { return _TokenExpiry; } }
            private DateTime _TokenExpiry;
            
            private OwinToken() { }
            public OwinToken(string paccessToken, string ptokenType, string prefreshToken, DateTime ploginTokenExpiry)
            {
                this._AccessToken = paccessToken;
                this._TokenType = ptokenType;
                this._RefreshToken = prefreshToken;
                this._TokenExpiry = ploginTokenExpiry;
            }
            public OwinToken(string JsonObj)
            {
                var dictResult = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, object>>(JsonObj);
                this._AccessToken = dictResult["access_token"].ToString();
                this._TokenType = dictResult["token_type"].ToString();
                this._RefreshToken = dictResult["refresh_token"].ToString();
                this._TokenExpiry = DateTime.Now.AddSeconds(System.Convert.ToInt32(dictResult["expires_in"].ToString()));
            }
        }
        
        public static string DeleteAsync(string Uri, string ApiLink, params RequestHeader[] requestHeaders)
        {
            try
            {
                var baseAddress = new Uri(Uri);

                if (baseAddress.ToString().EndsWith("/") && ApiLink.StartsWith("/"))
                {
                    ApiLink = ApiLink.Remove(0, 1);
                }
                else if (!baseAddress.ToString().EndsWith("/") && !ApiLink.StartsWith("/"))
                {
                    ApiLink = "/" + ApiLink;
                }

                using (var httpClient = new HttpClient { BaseAddress = baseAddress })
                {
                    foreach (var reqHdr in requestHeaders)
                    {
                        httpClient.DefaultRequestHeaders.TryAddWithoutValidation(reqHdr.name, reqHdr.value);
                    }

                    using (var response = httpClient.DeleteAsync(ApiLink).Result)
                    {
                        string responseData = response.Content.ReadAsStringAsync().Result;
                        return responseData;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetAsync(string Uri, string ApiLink, params RequestHeader[] requestHeaders)
        {
            try
            {
                var baseAddress = new Uri(Uri);

                if (baseAddress.ToString().EndsWith("/") && ApiLink.StartsWith("/"))
                {
                    ApiLink = ApiLink.Remove(0, 1);
                }
                else if (!baseAddress.ToString().EndsWith("/") && !ApiLink.StartsWith("/"))
                {
                    ApiLink = "/" + ApiLink;
                }

                using (var httpClient = new HttpClient { BaseAddress = baseAddress })
                {
                    foreach (var reqHdr in requestHeaders)
                    {
                        httpClient.DefaultRequestHeaders.TryAddWithoutValidation(reqHdr.name, reqHdr.value);
                    }

                    using (var response = httpClient.GetAsync(ApiLink).Result)
                    {
                        string responseData = response.Content.ReadAsStringAsync().Result;
                        return responseData;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string PostAsync(string Uri, string ApiLink, string JsonObj, params RequestHeader[] requestHeaders)
        {
            try
            {
                var baseAddress = new Uri(Uri);

                if (baseAddress.ToString().EndsWith("/") && ApiLink.StartsWith("/"))
                {
                    ApiLink = ApiLink.Remove(0, 1);
                }
                else if (!baseAddress.ToString().EndsWith("/") && !ApiLink.StartsWith("/"))
                {
                    ApiLink = "/" + ApiLink;
                }

                using (var httpClient = new HttpClient { BaseAddress = baseAddress })
                {
                    foreach (var reqHdr in requestHeaders)
                    {
                        httpClient.DefaultRequestHeaders.TryAddWithoutValidation(reqHdr.name, reqHdr.value);
                    }

                    using (var content = new StringContent(JsonObj, System.Text.Encoding.Default, "application/json"))
                    {
                        using (var response = httpClient.PostAsync(ApiLink, content).Result)
                        {
                            string responseData = response.Content.ReadAsStringAsync().Result;
                            return responseData;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string PostAsync(string Uri, string ApiLink, Dictionary<string, string> FormUrlEncodedObj, params RequestHeader[] requestHeaders)
        {
            try
            {
                var baseAddress = new Uri(Uri);

                if (baseAddress.ToString().EndsWith("/") && ApiLink.StartsWith("/"))
                {
                    ApiLink = ApiLink.Remove(0, 1);
                }
                else if (!baseAddress.ToString().EndsWith("/") && !ApiLink.StartsWith("/"))
                {
                    ApiLink = "/" + ApiLink;
                }

                using (var httpClient = new HttpClient { BaseAddress = baseAddress })
                {
                    foreach (var reqHdr in requestHeaders)
                    {
                        httpClient.DefaultRequestHeaders.TryAddWithoutValidation(reqHdr.name, reqHdr.value);
                    }

                    if (FormUrlEncodedObj == null) FormUrlEncodedObj = new Dictionary<string, string>();

                    using (var response = httpClient.PostAsync(ApiLink, new FormUrlEncodedContent(FormUrlEncodedObj)).Result)
                    {
                        string responseData = response.Content.ReadAsStringAsync().Result;
                        return responseData;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string PutAsync(string Uri, string ApiLink, string JsonObj, params RequestHeader[] requestHeaders)
        {
            try
            {
                var baseAddress = new Uri(Uri);

                if (baseAddress.ToString().EndsWith("/") && ApiLink.StartsWith("/"))
                {
                    ApiLink = ApiLink.Remove(0, 1);
                }
                else if (!baseAddress.ToString().EndsWith("/") && !ApiLink.StartsWith("/"))
                {
                    ApiLink = "/" + ApiLink;
                }

                using (var httpClient = new HttpClient { BaseAddress = baseAddress })
                {
                    foreach (var reqHdr in requestHeaders)
                    {
                        httpClient.DefaultRequestHeaders.TryAddWithoutValidation(reqHdr.name, reqHdr.value);
                    }

                    using (var content = new StringContent(JsonObj, System.Text.Encoding.Default, "application/json"))
                    {
                        using (var response = httpClient.PutAsync(ApiLink, content).Result)
                        {
                            string responseData = response.Content.ReadAsStringAsync().Result;
                            return responseData;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string PutAsync(string Uri, string ApiLink, Dictionary<string, string> FormUrlEncodedObj, params RequestHeader[] requestHeaders)
        {
            try
            {
                var baseAddress = new Uri(Uri);

                if (baseAddress.ToString().EndsWith("/") && ApiLink.StartsWith("/"))
                {
                    ApiLink = ApiLink.Remove(0, 1);
                }
                else if (!baseAddress.ToString().EndsWith("/") && !ApiLink.StartsWith("/"))
                {
                    ApiLink = "/" + ApiLink;
                }

                using (var httpClient = new HttpClient { BaseAddress = baseAddress })
                {
                    foreach (var reqHdr in requestHeaders)
                    {
                        httpClient.DefaultRequestHeaders.TryAddWithoutValidation(reqHdr.name, reqHdr.value);
                    }

                    if (FormUrlEncodedObj == null) FormUrlEncodedObj = new Dictionary<string, string>();

                    using (var response = httpClient.PutAsync(ApiLink, new FormUrlEncodedContent(FormUrlEncodedObj)).Result)
                    {
                        string responseData = response.Content.ReadAsStringAsync().Result;
                        return responseData;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
