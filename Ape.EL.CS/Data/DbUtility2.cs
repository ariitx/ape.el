﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Ape.EL.Data
{
    public static class DbUtility2
    {
        /// <summary>
        /// Insert data in bulk. Faster than DataAdapter and normal insert.
        /// </summary>
        public class BulkUploadToSql
        {
            /// <summary>
            /// Name of the database table that you want to bulk upload.
            /// </summary>
            public string TableName { get; set; }
            public string ConnectionString { get; set; }
            /// <summary>
            /// If you set this transaction, you dont need to set ConnectionString.
            /// </summary>
            public SqlTransaction Transaction { get; set; }
            
            public void Commit<T>(IList<T> InternalStore, int CommitBatchSize = 1000)
            {
                if (InternalStore.Count > 0)
                {
                    DataTable dt;
                    int numberOfPages = (InternalStore.Count / CommitBatchSize) + (InternalStore.Count % CommitBatchSize == 0 ? 0 : 1);
                    for (int pageIndex = 0; pageIndex < numberOfPages; pageIndex++)
                    {
                        dt = InternalStore.Skip(pageIndex * CommitBatchSize).Take(CommitBatchSize).ToDataTable();
                        BulkInsert(dt);
                    }
                }
            }

            /// <param name="option">Default value is TableLock | FireTriggers | UseInternalTransaction</param>
            public void BulkInsert(DataTable dt, SqlBulkCopyOptions? option = null)
            {
                SqlBulkCopy bulkCopy;
                SqlConnection connection = null;
                bool createConnection = false;

                try
                {
                    if (Transaction != null)
                    {
                        connection = Transaction.Connection;
                    }
                    else
                    {
                        connection = new SqlConnection(ConnectionString);
                        connection.Open();
                        createConnection = true;
                    }

                    if (!option.HasValue)
                    {
                        // make sure to enable triggers
                        // more on triggers in next post
                        bulkCopy = new SqlBulkCopy
                                    (
                                    connection,
                                    SqlBulkCopyOptions.TableLock |
                                    SqlBulkCopyOptions.FireTriggers |
                                    SqlBulkCopyOptions.UseInternalTransaction,
                                    null
                                    );
                    }
                    else
                    {
                        if (Transaction != null)
                            bulkCopy = new SqlBulkCopy(connection, option.Value, Transaction);
                        else
                            bulkCopy = new SqlBulkCopy(connection, option.Value, null);
                    }

                    // set the destination table name
                    bulkCopy.DestinationTableName = TableName;
                    
                    // map columns
                    foreach (DataColumn column in dt.Columns)
                    {
                        bulkCopy.ColumnMappings.Add(column.ColumnName, column.ColumnName);
                    }

                    // write the data in the "dataTable"
                    bulkCopy.WriteToServer(dt);

                    // reset
                    //this.dataTable.Clear();
                }
                finally
                {
                    if (createConnection && connection != null)
                    {
                        connection.Dispose();
                    }
                }
            }
        }

        public static DataTable ToDataTable<T>(this IEnumerable<T> data)
        {
            PropertyDescriptorCollection properties =
                TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;
        }
        
        public static List<T> FromDataTable<T>(this DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }

        private static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (System.Reflection.PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                        pro.SetValue(obj, dr[column.ColumnName], null);
                    else
                        continue;
                }
            }
            return obj;
        } 
    }
}
