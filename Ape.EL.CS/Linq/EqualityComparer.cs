﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ape.EL.Linq
{
    public class EqualityComparer<T> : IEqualityComparer<T>
    {
        Func<T, T, bool> _equalsFunction;
        Func<T, int> _hashCodeFunction;

        public EqualityComparer(Func<T, T, bool> equalsFunction, Func<T, int> hashCodeFunction)
        {
            if (equalsFunction == null) throw new ArgumentNullException();
            if (hashCodeFunction == null) throw new ArgumentNullException();

            _equalsFunction = equalsFunction;
            _hashCodeFunction = hashCodeFunction;
        }

        public EqualityComparer(Func<T, T, bool> equalsFunction)
        {
            if (equalsFunction == null) throw new ArgumentNullException();

            _equalsFunction = equalsFunction;
            _hashCodeFunction = (x => x.GetHashCode());
        }

        public bool Equals(T x, T y)
        {
            return _equalsFunction(x, y);
        }

        public int GetHashCode(T obj)
        {
            return _hashCodeFunction(obj);
        }
    }

}
