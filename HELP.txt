* How to deploy Ape.EL.Device.PrintText class?
- You have to register Printer6.dll in Ape.EL.Device\COM\Printer6 folder.
- If you are using 64 bit Windows, open OLE/COM Object Viewer.
- Under following nodes : Object Classes\All objects\Printer6.Win32Print, go to the Implementation tab.
- Check "Use Surrogate Process" and go to Registry tab to apply the changes. Go back to Implementation tab to confirm the changes.
- You have to do above steps when you are developing too.

* Where is the interop files for COM objects?
- Most of the COM references created interop files by default, but I have set it to embed to the main binary.