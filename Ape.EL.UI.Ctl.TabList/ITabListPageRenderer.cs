﻿using System.Drawing;

namespace Ape.EL.UI.Ctl
{
  public interface ITabListPageRenderer
  {
    #region  Private Methods

    void RenderHeader(Graphics g, TabListPage page, TabListPageState state);

    #endregion  Private Methods
  }
}
