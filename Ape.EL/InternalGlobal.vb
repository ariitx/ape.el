Imports System.Reflection
Imports System.Runtime.InteropServices
Imports Ape.EL.WinForm.General

Public Class InternalGlobal
    Implements IDisposable

    Public Shared ReadOnly ScreenWidth As Integer = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width
    Public Shared ReadOnly ScreenHeight As Integer = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height

    Private Sub Dispose() Implements IDisposable.Dispose
        If _GlobalConnection IsNot Nothing Then
            _GlobalConnection.Dispose()
            _GlobalConnection = Nothing
        End If
    End Sub

    ''' <summary>
    ''' Provide default value for SqlClient related query execution timeout in seconds. Default value is 7200 seconds.
    ''' </summary>
    Public Shared Property DefaultQueryTimeout As Integer
        Get
            Return _DefaultQueryTimeout
        End Get
        Set(value As Integer)
            _DefaultQueryTimeout = value
        End Set
    End Property
    Private Shared _DefaultQueryTimeout As Integer = 7200

    ' This is default encryption code to be used in Business.Setting
    ' basically it is a result of Rot13 of the root namespace of entry assembly
    ' for example your program exe's assembly is Ape.EL.ProgramName.XYZ
    '  i will take only the root (Ape) and use ROT13 become NCR as the IntegralVal result
    Friend Shared ReadOnly Property IntegralVal As String
        Get
            Dim [mod] As String = If(Not String.IsNullOrEmpty(IntegralValMod), IntegralValMod, Utility.General.GetAsmNamespace(Misc.AssemblyHelper.GetTrueEntryAssembly))
            Return NewIntegralVal([mod])
        End Get
    End Property

    ' Process value to become integral value by using Rot13
    Friend Shared Function NewIntegralVal(ByVal val As String) As String
        Return Utility.Data.Crypt.Rot13(val)
    End Function

    ' You can use this IntegralValMod to change the IntegralVal to other result 
    ' without restriction to the program exe's root assembly name
    ' Use Ape.EL.Reflex.PropertyHelper.SetPrivateStaticField() to change it
    Private Shared IntegralValMod As String = String.Empty

    ' This will affect the DbGeneral connection lifecycle.
    Public Shared RenewConnection As Boolean = True
    Friend Shared Property MyConnection() As SqlClient.SqlConnection
        Get
            'It is safer to renew the connection instead of using old one! (avoid thread being in use issue)
            'Tested for performance, doesnt have much difference, unless being repeated for 1000 times.
            If RenewConnection Then
                Return Nothing
            Else
                Return _GlobalConnection
            End If
        End Get
        Set(ByVal value As SqlClient.SqlConnection)
            _GlobalConnection = value
        End Set
    End Property
    Private Shared _GlobalConnection As SqlClient.SqlConnection

    ''' <summary>
    ''' This property will have effect on WinForm.General functions for exception handling. It will suppress the message and throw as an ApplicationException. Default value is true.
    ''' </summary>
    Public Shared Property SuppressExceptionMessage As Boolean = True

    ''' <summary>
    ''' This property will have effect on WinForm.General functions. It will suppress the message and throw as an ApplicationException. Default value is false.
    ''' </summary>
    Public Shared Property SuppressMessage As Boolean = False

    ''' <summary>
    ''' This property will have effect on WinForm.General.ExceptionErrorMsg(ByVal _ex As Exception). Instead of treating it as exception, we treat it as warning.
    ''' </summary>
    Public Shared Property CatchApplicationExceptionAsWarning As Boolean = True

    ''' <summary>
    ''' This property will have effect on WinForm.General.
    ''' InitOnceFormMonitor will be paused.
    ''' </summary>
    Public Shared Property PauseFormMonitor As Boolean = False

    ''' <summary>
    ''' This property will have effect on WinForm.General.
    ''' InitOnceFormMonitor will exit thread, some functions will not work properly.
    ''' </summary>
    Public Shared Property DisableFormMonitor As Boolean = False

    Friend Shared Function GetNJsonSerializerSetting(IgnoreNullValue As Boolean, IgnoreDefaultValue As Boolean) As Newtonsoft.Json.JsonSerializerSettings
        Dim _setting As New Newtonsoft.Json.JsonSerializerSettings

        If IgnoreNullValue Then
            _setting.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore
        Else
            _setting.NullValueHandling = Newtonsoft.Json.NullValueHandling.Include
        End If

        If IgnoreDefaultValue Then
            _setting.DefaultValueHandling = Newtonsoft.Json.DefaultValueHandling.Ignore
        Else
            _setting.DefaultValueHandling = Newtonsoft.Json.DefaultValueHandling.Include
        End If

        Return _setting
    End Function

    ''' <summary>
    ''' Get NJsonSerialize's setting.
    ''' </summary>
    Friend Shared ReadOnly Property NJsonSerializerSetting As Newtonsoft.Json.JsonSerializerSettings
        Get
            Static _setting As New Newtonsoft.Json.JsonSerializerSettings

            _setting = GetNJsonSerializerSetting(NJsonSerializer_IgnoreNullValue, NJsonSerializer_IgnoreDefaultValue)

            Return _setting
        End Get
    End Property

    ''' <summary>
    ''' Set NJsonSerialize to ignore null value properties.
    ''' </summary>
    Public Shared Property NJsonSerializer_IgnoreNullValue As Boolean = False

    ''' <summary>
    ''' Set NJsonSerialize to ignore default value properties.
    ''' </summary>
    Public Shared Property NJsonSerializer_IgnoreDefaultValue As Boolean = False
End Class
