﻿Imports ICSharpCode.SharpZipLib.Zip
Imports System
Imports System.IO
Imports System.Runtime.Serialization.Formatters.Binary
Imports System.Runtime.Serialization.Formatters.Soap

Namespace Misc
    Public Class PersistenceUtil
        Public Shared ReadOnly Property UserSettingDataPath As String
            Get
                Dim path As String = (App.PathHelper.ApplicationDataPath & System.IO.Path.DirectorySeparatorChar & "User Settings")
                If Not Directory.Exists(path) Then
                    Directory.CreateDirectory(path)
                End If
                Return path
            End Get
        End Property

        Public Shared Function LoadSetting(ByVal fileName As String) As Object
            Dim obj2 As Object
            If Not File.Exists(fileName) Then
                Return Nothing
            End If
            Try
                Using stream As Stream = File.Open(fileName, FileMode.Open)
                    Try
                        Dim formatter As New BinaryFormatter
                        Return formatter.Deserialize(stream)
                    Catch obj1 As Exception
                        stream.Position = 0
                        Dim formatter2 As New SoapFormatter
                        Return formatter2.Deserialize(stream)
                    End Try
                End Using
            Catch obj3 As Exception
                obj2 = Nothing
            End Try
            Return obj2
        End Function

        Public Shared Function LoadUserSetting(ByVal fileName As String) As Object
            Return PersistenceUtil.LoadSetting((PersistenceUtil.UserSettingDataPath & Path.DirectorySeparatorChar & fileName))
        End Function

        Public Shared Sub SaveSetting(ByVal object_0 As Object, ByVal fileName As String)
            Try
                Using stream As Stream = File.Open(fileName, FileMode.Create)
                    Dim bf As New BinaryFormatter
                    bf.Serialize(stream, object_0)
                End Using
            Catch obj1 As Exception
            End Try
        End Sub

        Public Shared Sub SaveUserSetting(ByVal object_0 As Object, ByVal fileName As String)
            Dim str As String = (PersistenceUtil.UserSettingDataPath & Path.DirectorySeparatorChar & fileName)
            PersistenceUtil.SaveSetting(object_0, str)
        End Sub
    End Class
End Namespace

