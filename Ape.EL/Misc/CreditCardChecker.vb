﻿
Imports System
Imports System.Globalization
Imports System.Text

Namespace Misc
    Public Class CreditCardChecker
        Public Enum CreditCardType
            Unknown
            AmericanExpress
            DinersClub
            CarteBlanche
            Discover
            EnRoute
            JCB
            MasterCard
            Visa
        End Enum

        Public Shared Function IsValidCreditCardNo(cardNo As String) As Boolean
            cardNo = cardNo.Replace(" ", "").Replace("-", "")
            If cardNo.Length < 13 OrElse cardNo.Length > 16 Then
                Return False
            End If
            Dim text As String = cardNo
            For i As Integer = 0 To text.Length - 1
                Dim c As Char = text(i)
                If Not Char.IsDigit(c) Then
                    Return False
                End If
            Next
            Dim stringBuilder As StringBuilder = New StringBuilder()
            For j As Integer = 0 To cardNo.Length - 1
                Dim num As Integer = Integer.Parse(cardNo(j).ToString(), CultureInfo.InvariantCulture)
                If j Mod 2 <> 0 Then
                    num *= 2
                End If
                stringBuilder.Append(num)
            Next
            Dim text2 As String = stringBuilder.ToString()
            Dim num2 As Integer = 0
            For k As Integer = 0 To text2.Length - 1
                num2 += Integer.Parse(text2(k).ToString(), CultureInfo.InvariantCulture)
            Next
            Return num2 <> 0 AndAlso num2 Mod 10 = 0
        End Function

        Public Shared Function GetCreditCardType(cardNo As String) As CreditCardChecker.CreditCardType
            Dim result As CreditCardChecker.CreditCardType = CreditCardChecker.CreditCardType.Unknown
            cardNo = cardNo.Replace(" ", "").Replace("-", "")
            If cardNo.Length < 13 Then
                Return result
            End If
            Dim text As String = cardNo
            For i As Integer = 0 To text.Length - 1
                Dim c As Char = text(i)
                If Not Char.IsDigit(c) Then
                    Return result
                End If
            Next
            Dim num As Integer = Integer.Parse(cardNo.Substring(0, 2), CultureInfo.InvariantCulture)
            If num = 34 OrElse num = 37 Then
                result = CreditCardChecker.CreditCardType.AmericanExpress
            Else
                If num = 36 Then
                    result = CreditCardChecker.CreditCardType.DinersClub
                Else
                    If num = 38 Then
                        result = CreditCardChecker.CreditCardType.CarteBlanche
                    Else
                        If num >= 51 AndAlso num <= 55 Then
                            result = CreditCardChecker.CreditCardType.MasterCard
                        Else
                            num = Integer.Parse(cardNo.Substring(0, 4))
                            If num = 2014 OrElse num = 2149 Then
                                result = CreditCardChecker.CreditCardType.EnRoute
                            Else
                                If num = 2131 OrElse num = 1800 Then
                                    result = CreditCardChecker.CreditCardType.JCB
                                Else
                                    If num = 6011 Then
                                        result = CreditCardChecker.CreditCardType.Discover
                                    Else
                                        num = Integer.Parse(cardNo.Substring(0, 3))
                                        If num >= 300 AndAlso num <= 305 Then
                                            result = CreditCardChecker.CreditCardType.DinersClub
                                        Else
                                            If cardNo(0) = "3" Then
                                                result = CreditCardChecker.CreditCardType.JCB
                                            Else
                                                If cardNo(0) = "4" Then
                                                    result = CreditCardChecker.CreditCardType.Visa
                                                End If
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            End If
            Return result
        End Function

    End Class
End Namespace
