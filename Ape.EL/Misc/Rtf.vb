﻿
Imports System
Imports System.Text
Imports System.Windows.Forms

Namespace Misc
    Public Class Rtf
        Friend Sub New()
        End Sub

        Public Shared Function IsRichText(text As String) As Boolean
            Return text.Length >= 7 AndAlso text.Substring(0, 7) = "{\rtf1\"
        End Function

        Public Shared Function ToArialRichText(text As String) As String
            Return Rtf.ToRichText("Arial", 10, text)
        End Function

        Public Shared Function ToRichText(fontType As String, fontSize As Integer, text As String) As String
            If Rtf.IsRichText(text) Then
                Return text
            End If
            text = text.Replace(vbCrLf, vbLf)
            Dim array As String() = text.Split(New Char() {vbLf})
            fontSize <<= 1
            Dim value As String = String.Concat(New String() {"{\rtf1\ansi\ansicpg1252\deff0\deflang1033{\fonttbl{\f0\fswiss\fcharset0 ", fontType, ";}}\viewkind4\uc1\pard\f0\fs", fontSize.ToString(), " "})
            Dim stringBuilder As StringBuilder = New StringBuilder()
            stringBuilder.Append(value)
            Dim array2 As String() = array
            For i As Integer = 0 To array2.Length - 1
                Dim text2 As String = array2(i)
                stringBuilder.Append(text2.Replace("\", "\\"))
                stringBuilder.Append("\par" & vbCrLf)
            Next
            stringBuilder.Append("}")
            Return stringBuilder.ToString()
        End Function

        Public Shared Function RichTextToPlainText(richText As String) As String
            If Not Rtf.IsRichText(richText) Then
                Return richText
            End If
            Dim text As String
            Using richTextBox As RichTextBox = New RichTextBox()
                richTextBox.Rtf = richText
                text = richTextBox.Text
            End Using
            Return text
        End Function
    End Class
End Namespace
