﻿Imports System.IO
Imports System.Reflection

Namespace Misc
    Public Class AssemblyHelper
        ''' <summary>
        ''' Determine whether the provided file is valid .Net assembly.
        ''' </summary>
        Public Shared Function IsValid(ByVal filepath As String) As Boolean
            If File.Exists(filepath) Then
                Dim sr As New StreamReader(filepath)
                sr.Read()

                Dim reader As New IO.BinaryReader(New IO.StreamReader(filepath).BaseStream)
                Dim bytes() As Byte = reader.ReadBytes(2)
                Dim text As String = System.Text.ASCIIEncoding.ASCII.GetString(bytes)

                Return text = System.Text.Encoding.ASCII.GetString(Misc.MagicNumber.NetAssembly)
            End If

            Return False
        End Function

        Public Shared Function GetPublicKeyTokenFromAssembly(assembly As Assembly) As String
            Dim bytes = assembly.GetName().GetPublicKeyToken()
            If bytes Is Nothing OrElse bytes.Length = 0 Then
                Return "NULL"
            End If

            Dim publicKeyToken = String.Empty
            For i As Integer = 0 To bytes.GetLength(0) - 1
                publicKeyToken += String.Format("{0:x2}", bytes(i))
            Next

            Return publicKeyToken
        End Function

        ''' <summary>
        ''' Get entry assembly based on program runtime (asp or winform).
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetTrueEntryAssembly() As Assembly
            If Utility.General.IsAspNet Then
                Return GetAspAssembly()
            Else
                Dim entAsm As Assembly = System.Reflection.Assembly.GetEntryAssembly
                If entAsm Is Nothing Then
                    entAsm = System.Reflection.Assembly.GetCallingAssembly
                End If
                If entAsm Is Nothing Then
                    entAsm = System.Reflection.Assembly.GetExecutingAssembly
                End If
                Return entAsm
            End If
        End Function

        ''' <summary>
        ''' Find an assembly through stack trace which is before System.Web assembly.
        ''' Only used in GetTrueEntryAssembly!
        ''' </summary>
        Private Shared Function GetAspAssembly() As Assembly
            Static result As Assembly = Nothing
            If result IsNot Nothing Then Return result

            'This checking is to make sure there is no overlap calling of this assembly on multi-threading.
            Static checking As Boolean
            While checking
                Threading.Thread.Sleep(1)
            End While

            checking = True
            Try
                'create the stack trace and get the methods
                Dim st As New System.Diagnostics.StackTrace()
                Dim methodX As Reflection.MethodBase = Nothing
                If st.FrameCount > 0 Then
                    For methodFrame As Integer = 0 To st.FrameCount - 1
                        methodX = st.GetFrame(methodFrame).GetMethod

                        If result IsNot Nothing Then
                            If System.IO.Path.GetFileNameWithoutExtension(methodX.DeclaringType.Assembly.BinaryPath) = "System.Web" Then
                                Exit For
                            End If
                        End If

                        result = methodX.DeclaringType.Assembly
                    Next

                    If result IsNot Nothing Then
                        Dim fileName As String = System.IO.Path.GetFileName(result.BinaryPath)
                        Dim realPath As String = System.Web.HttpRuntime.AppDomainAppPath
                        Dim allFiles As List(Of String) = Misc.FileHelper.GetFilesRecursive(realPath)
                        Dim keepFiles As New List(Of String)
                        For Each f As String In allFiles
                            If f.EndsWith(fileName, StringComparison.OrdinalIgnoreCase) Then
                                keepFiles.Add(f)
                            End If
                        Next
                        If keepFiles.Count > 0 Then
                            realPath = ""
                            For Each f As String In keepFiles
                                'find shortest subdirectory-travelling file.
                                If realPath.IsEmpty OrElse f.Split(System.IO.Path.DirectorySeparatorChar).Count < realPath.Split(System.IO.Path.DirectorySeparatorChar).Count Then
                                    realPath = f
                                End If
                            Next
                        Else
                            realPath = ""
                        End If

                        If System.IO.File.Exists(realPath) Then
                            Dim realResult As Reflection.Assembly = Reflection.Assembly.ReflectionOnlyLoadFrom(realPath)
                            If realResult IsNot Nothing Then
                                result = realResult
                            End If
                        End If
                    End If
                End If

                'clean up the mess
                methodX = Nothing
                st = Nothing
                Return result
            Catch ex As Exception
            Finally
                checking = False
            End Try
            Throw New ApplicationException("Failed to locate asp assembly.")
        End Function

#Region "Assembly From String"
        ''' <summary>
        ''' Get original Type of a Type's FullName.
        ''' https://stackoverflow.com/questions/1825147/type-gettypenamespace-a-b-classname-returns-null
        ''' </summary>
        ''' <param name="typeName">The Type's FullName.</param>
        ''' <returns>The type.</returns>
        ''' <remarks>Make sure you have loaded the assembly of the origin TypeName.</remarks>
        Public Shared Function FindType(typeName As String) As Type
            Dim type__1 = Type.GetType(typeName)
            If type__1 IsNot Nothing Then
                Return type__1
            End If
            For Each a As Assembly In AppDomain.CurrentDomain.GetAssemblies()
                type__1 = a.GetType(typeName)
                If type__1 IsNot Nothing Then
                    Return type__1
                End If
            Next
            Return Nothing
        End Function
#End Region

#Region "AssemblyLoader"
        ''' <summary>
        ''' Load plugins libraries dynamically to memory. Only .Net Framework same or less than this assembly will be loaded.
        ''' </summary>
        ''' <param name="asmPath">Path or file is accepted.</param>
        ''' <remarks></remarks>
        Public Shared Sub LoadAssembly(asmPath As String)
            If System.IO.File.Exists(asmPath) Then
                If Ape.EL.Misc.AssemblyHelper.IsValid(asmPath) Then
                    Try
                        Dim asm As Assembly = Reflection.Assembly.LoadFrom(asmPath)
                    Catch ex As Exception
                        'ignore if cannot load.
                        Ape.EL.App.ErrorLog.Debug(ex.Message)
                    End Try
                End If
            ElseIf System.IO.Directory.Exists(asmPath) Then
                Dim paths As List(Of String) = System.IO.Directory.GetFiles(asmPath).ToList
                For Each o As String In paths
                    LoadAssembly(o)
                Next
            End If
        End Sub
#End Region

#Region "Referenced Assemblies"
        ''' <summary>
        ''' Find all assemblies which have referenced from provided AssemblyName.
        ''' </summary>
        Public Shared Function GetReferencedAssemblies(asmName As AssemblyName) As List(Of Assembly)
            Dim asmList As New List(Of Assembly)

            GetReferencedAssemblies(asmName, asmList)

            Return asmList
        End Function

        ''' <summary>
        ''' Used by GetReferencedAssemblies.
        ''' </summary>
        Private Shared Sub GetReferencedAssemblies(asmName As AssemblyName, asmList As List(Of Assembly))
            Static loadedAssemblies As List(Of Assembly)

            'To make sure we dont cause stack overflow / infinite loop, we keep track of looped objects.
            Static lastContainer As New List(Of Object)

            If lastContainer.Contains(asmName) Then
                Exit Sub
            End If
            'Keeping track...
            lastContainer.Add(asmName)

            'Add into the list if not yet added. Return to previous stack if previously added.
            If asmList Is Nothing Then asmList = New List(Of Assembly)
            If loadedAssemblies Is Nothing Then loadedAssemblies = AppDomain.CurrentDomain.GetAssemblies.ToList()
            Dim asm As Assembly = (From x As Assembly In loadedAssemblies Where x.FullName = asmName.FullName Select x).FirstOrDefault 'find by name in the loaded assemblies
            If asm IsNot Nothing AndAlso Not asmList.Contains(asm) Then
                asmList.Add(asm)
            Else
                lastContainer.Remove(asmName)
                Exit Sub
            End If

            If asm IsNot Nothing Then
                'Find asm's references AssemblyNames and loop.
                Dim refAsmName As List(Of AssemblyName) = asm.GetReferencedAssemblies.ToList
                If refAsmName IsNot Nothing Then
                    For Each asmn As AssemblyName In refAsmName
                        GetReferencedAssemblies(asmn, asmList)
                    Next
                End If
            End If

            lastContainer.Remove(asmName)
            If lastContainer.Count = 0 Then loadedAssemblies = Nothing
        End Sub
#End Region
    End Class
End Namespace
