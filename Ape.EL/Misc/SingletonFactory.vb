﻿Namespace Misc
    ''' <summary>
    ''' Wraps method for singleton object of class.
    ''' </summary>
    <Obsolete("Use the TypeExtensions's GetInstance method instead.")> _
    Public Class SingletonFactory(Of T)
        Public Shared ReadOnly Property Instance() As T
            Get
                If Not _instanceDict.ContainsKey(GetType(T).FullName) OrElse _instanceDict(GetType(T).FullName) Is Nothing And GetType(T) IsNot Nothing Then
                    _instanceDict.Add(GetType(T).FullName, GetType(T).Assembly.CreateInstance(GetType(T).FullName, True))
                End If
                Return _instanceDict(GetType(T).FullName)
            End Get
        End Property
        Private Shared _instanceDict As New Dictionary(Of String, Object)
    End Class
End Namespace
