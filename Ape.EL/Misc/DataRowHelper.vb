﻿
Imports System
Imports System.Data

Namespace Misc
    Public Class DataRowHelper
        Public Shared Sub CopyDataRow(fromRow As DataRow, toRow As DataRow, fromFieldList As String, toFieldList As String)
            Dim array As String() = fromFieldList.Split(New Char() {";"})
            Dim array2 As String() = toFieldList.Split(New Char() {";"})
            If array.Length <> array2.Length Then
                Throw New ArgumentException("Number of fields in fromFieldList and toFieldList is not same.")
            End If
            For i As Integer = 0 To array.Length - 1
                Dim columnName As String = array(i)
                Dim columnName2 As String = array2(i)
                If Not toRow(columnName2).Equals(fromRow(columnName)) Then
                    toRow(columnName2) = fromRow(columnName)
                End If
            Next
        End Sub

        Public Shared Sub CopyDataRow(fromRow As DataRow, toRow As DataRow, fieldList As String)
            Dim array As String() = fieldList.Split(New Char() {";"})
            Dim array2 As String() = array
            For i As Integer = 0 To array2.Length - 1
                Dim columnName As String = array2(i)
                If Not toRow(columnName).Equals(fromRow(columnName)) Then
                    toRow(columnName) = fromRow(columnName)
                End If
            Next
        End Sub
    End Class
End Namespace
