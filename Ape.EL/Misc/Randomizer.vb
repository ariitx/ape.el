﻿Namespace Misc
    Public Class Randomizer
        Private Shared random As System.Random = New System.Random

        Public Shared Function [Next]() As Integer
            Return random.Next()
        End Function

        Public Shared Function [Next](maxValue As Integer) As Integer
            Return random.Next(maxValue)
        End Function

        Public Shared Function [Next](minValue As Integer, maxValue As Integer) As Integer
            Return random.Next(minValue, maxValue)
        End Function

        Public Shared Function NextDouble() As Double
            Return random.NextDouble()
        End Function
    End Class
End Namespace
