﻿
Imports System
Imports System.Data

Namespace Misc
    Public Class DataSetHelper
        Public Shared Sub TransferAllTables(dsSource As DataSet, dsTarget As DataSet)
            While dsSource.Tables.Count > 0
                Dim table As DataTable = dsSource.Tables(0)
                dsSource.Tables.Remove(table)
                dsTarget.Tables.Add(table)
            End While
        End Sub
    End Class
End Namespace
