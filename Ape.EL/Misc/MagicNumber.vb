﻿Imports System.IO

Namespace Misc
    Public Class MagicNumber
        Public Shared ReadOnly NetAssembly() As Byte = New Byte() {&H4D, &H5A} 'MZ
        'Public Shared ReadOnly ICO() As Byte = New Byte() {&H0, &H0, &H1, &H0}
        'Public Shared ReadOnly RAR150() As Byte = New Byte() {&H52, &H61, &H72, &H21, &H1A, &H7, &H0}
        'Public Shared ReadOnly RAR500() As Byte = New Byte() {&H52, &H61, &H72, &H21, &H1A, &H7, &H1, &H0}

        ''' <summary>
        ''' Get first 2 bytes to determine binary type.
        ''' </summary>
        Public Shared Function GetFirstBytes(ByVal filepath As String, Optional firstX As Integer = 2) As Byte()
            If firstX = 0 Then firstX = 2

            If File.Exists(filepath) Then
                Dim reader As New IO.BinaryReader(New IO.StreamReader(filepath).BaseStream)
                Dim bytes() As Byte = reader.ReadBytes(firstX)
                reader.Close()

                Return bytes
            End If
            Return New Byte() {}
        End Function
    End Class
End Namespace
