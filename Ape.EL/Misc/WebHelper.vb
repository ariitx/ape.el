﻿Imports System.Net
Imports System.Text

Namespace Misc
    Public Class WebHelper
        ''' <summary>
        ''' Use this class to return as JSON string template. Append your specific object into Obj.
        ''' </summary>
        ''' <typeparam name="T">Obj's object type</typeparam>
        ''' <remarks></remarks>
        Public Class ReturnMsg(Of T)
            ''' <summary>
            ''' Define result as succeed
            ''' </summary>
            ''' <value></value>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Property Result As Boolean
            ''' <summary>
            ''' Message for success or failure
            ''' </summary>
            ''' <value></value>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Property Message As String
            ''' <summary>
            ''' Returned object as T
            ''' </summary>
            ''' <value></value>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Property Obj As T

            Public Sub New()
            End Sub

            ''' <summary>
            ''' Create a new ReturnMsg of T
            ''' </summary>
            ''' <param name="res">Define result as succeed</param>
            ''' <param name="msg">Message for success or failure</param>
            ''' <param name="o">Returned object as T</param>
            ''' <remarks></remarks>
            Public Sub New(res As Boolean, msg As String, o As T)
                Result = res
                Message = msg
                Obj = o
            End Sub

            ''' <summary>
            ''' Throw Message property as an exception if it is not empty.
            ''' </summary>
            ''' <param name="asApplicationException">By default, it will throw ServerExecutionException which message contains info that the error comes from server execution.</param>
            ''' <returns>Result property.</returns>
            <DebuggerStepThrough>
            Public Function CheckError(Optional ByVal asApplicationException As Boolean = False) As Boolean
                If Not Message.IsEmpty Then
                    If asApplicationException Then
                        Throw New ApplicationException(Message)
                    Else
                        Throw New ServerExecutionException(Message)
                    End If
                End If
                Return Result
            End Function
        End Class

        ''' <summary>
        ''' Application exception type for using GetWebAPI or PostWebAPI.
        ''' </summary>
        Public Class ServerExecutionException
            Inherits System.ApplicationException

            Public Sub New(message As String)
                MyBase.New(String.Format("Server has returned an error: {0}", message))
            End Sub
        End Class

        ''' <summary>
        ''' Default encoding used in GetWebAPI and PostWebAPI. Default value is UTF8Encoding.
        ''' </summary>
        Public Shared Property WebAPIEncoding As Text.Encoding
            Get
                Return _WebAPIEncoding
            End Get
            Set(value As Text.Encoding)
                _WebAPIEncoding = value
            End Set
        End Property
        Private Shared _WebAPIEncoding As Text.Encoding = New UTF8Encoding

        ''' <summary>
        ''' Make Uri starts with Http.
        ''' </summary>
        Private Shared Sub FormatUri(ByRef Uri As String)
            Uri = Uri.Replace("\", "/")

            If Not Uri.ToLower.StartsWith("http://", StringComparison.OrdinalIgnoreCase) AndAlso Not Uri.ToLower.StartsWith("https://", StringComparison.OrdinalIgnoreCase) Then
                While Uri.Length > 0 AndAlso Uri.StartsWith("/")
                    Uri = Uri.Remove(0, 1)
                End While

                Uri = String.Format("http://{0}", Uri)
            End If
            If Not Uri.EndsWith("/") Then
                Uri.Append("/")
            End If
        End Sub

        ''' <summary>
        ''' Execute get method for web api and return result as string.
        ''' </summary>
        Public Shared Function GetWebAPI(ByVal Uri As String, ByVal ApiLink As String) As String
            FormatUri(Uri)

            Dim result As String = ""

            Try
                Dim client As New System.Net.WebClient
                client.Headers(HttpRequestHeader.ContentType) = "application/json"
                client.Encoding = WebAPIEncoding
                result = client.DownloadString(String.Format("{0}{1}", Uri, ApiLink))
            Catch ex As Exception
                Throw ex
            End Try

            Return result
        End Function

        ''' <summary>
        ''' Execute post method for web api and return result as string.
        ''' </summary>
        Public Shared Function PostWebAPI(ByVal Uri As String, ByVal ApiLink As String, ByVal JsonObj As String) As String
            FormatUri(Uri)

            Dim result As String = ""

            Try
                Dim client As New System.Net.WebClient
                client.Headers(HttpRequestHeader.ContentType) = "application/json"
                result = WebAPIEncoding.GetString(client.UploadData(String.Format("{0}{1}", Uri, ApiLink), "POST", WebAPIEncoding.GetBytes(JsonObj)))
            Catch ex As Exception
                Throw ex
            End Try

            Return result
        End Function

        ''' <summary>
        ''' Determine if provided uri is still alive.
        ''' </summary>
        ''' <param name="Uri"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function IsWebAlive(ByVal Uri As String) As Boolean
            FormatUri(Uri)

            Dim client As System.Net.WebClient = Nothing

            'To make sure there isnt any cross-thread issue.
            Static lastThreadIdCount As Integer = 0
            Dim myThreadID As String = Threading.Thread.CurrentThread.GetExPropertyStr("IsWebAliveID")
            If myThreadID.IsEmpty Then
                lastThreadIdCount += 1
                Threading.Thread.CurrentThread.SetExProperty("IsWebAliveID", lastThreadIdCount)
                myThreadID = lastThreadIdCount
            End If

            'Create new webclient for unique threads & uri.
            Static dict As New Dictionary(Of String, System.Net.WebClient)
            Dim Uri_thread As String = Uri & "|" & myThreadID
            If dict.ContainsKey(Uri_thread) Then
                dict.TryGetValue(Uri_thread, client)
            End If
            If client Is Nothing Then
                client = New System.Net.WebClient
                dict.Add(Uri_thread, client)
            End If

            Try
                If client IsNot Nothing Then
                    While client.IsBusy
                        Threading.Thread.Sleep(1000)
                    End While
                    client.DownloadString(Uri)
                    Return True
                End If
            Catch ex As Exception
            End Try
            Return False
        End Function

        ''' <summary>
        ''' Check if there is internet connection.
        ''' </summary>
        Public Shared Function HasInternetConnection() As Boolean
            Try
                If My.Computer.Network.Ping("8.8.8.8", 100) Then
                    Return True
                End If
                If My.Computer.Network.Ping("8.8.4.4", 100) Then
                    Return True
                End If
                If My.Computer.Network.Ping("www.google.com", 100) Then
                    Return True
                End If
                If My.Computer.Network.Ping("www.microsoft.com", 100) Then
                    Return True
                End If
            Catch ex As Exception
            End Try
            Return False
        End Function
    End Class
End Namespace
