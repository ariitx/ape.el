﻿
Imports System

Namespace Misc
    Public Class DateTimeUtils
        Public Enum DateFormat
            DDMMYY
            MMDDYY
            YYMMDD
            DDYYMM
            MMYYDD
            YYDDMM
        End Enum

        Public Shared Function ParseDate(str As String, dateFormat As DateTimeUtils.DateFormat) As DateTime
            Dim today As DateTime = DateTime.Today
            Dim year As Integer = today.Year
            Dim month As Integer = today.Month
            Dim day As Integer = today.Day
            Dim num As Integer = 0
            Dim num2 As Integer = 0
            Dim length As Integer = str.Length
            Dim array As Integer() = New Integer(3 - 1) {}
            Dim array2 As Integer() = array
            Dim flag As Boolean = True
            For i As Integer = 0 To str.Length - 1
                Dim c As Char = str(i)
                If Not Char.IsDigit(c) Then
                    flag = False
                    Exit For
                End If
            Next
            If flag Then
                If length = 8 Then
                    If dateFormat = DateTimeUtils.DateFormat.DDMMYY OrElse dateFormat = DateTimeUtils.DateFormat.MMDDYY Then
                        array2(0) = Convert.ToInt32(str.Substring(0, 2))
                        array2(1) = Convert.ToInt32(str.Substring(2, 2))
                        array2(2) = Convert.ToInt32(str.Substring(4, 4))
                    Else
                        If dateFormat = DateTimeUtils.DateFormat.DDYYMM OrElse dateFormat = DateTimeUtils.DateFormat.MMYYDD Then
                            array2(0) = Convert.ToInt32(str.Substring(0, 2))
                            array2(1) = Convert.ToInt32(str.Substring(2, 4))
                            array2(2) = Convert.ToInt32(str.Substring(6, 2))
                        Else
                            array2(0) = Convert.ToInt32(str.Substring(0, 4))
                            array2(1) = Convert.ToInt32(str.Substring(4, 2))
                            array2(2) = Convert.ToInt32(str.Substring(6, 2))
                        End If
                    End If
                Else
                    If length = 6 Then
                        array2(0) = Convert.ToInt32(str.Substring(0, 2))
                        array2(1) = Convert.ToInt32(str.Substring(2, 2))
                        array2(2) = Convert.ToInt32(str.Substring(4, 2))
                    End If
                End If
            Else
                While num < length AndAlso num2 < 3
                    While num < length AndAlso Not Char.IsDigit(str(num))
                        num += 1
                    End While
                    Dim text As String = ""
                    While num < length AndAlso Char.IsDigit(str(num))
                        Dim arg_1A8_0 As Object = text
                        Dim expr_199 As Integer = num
                        num = expr_199 + 1
                        text = arg_1A8_0 + str(expr_199)
                    End While
                    If text.Length > 0 Then
                        Dim arg_1DE_0 As Integer() = array2
                        Dim expr_1D2 As Integer = num2
                        num2 = expr_1D2 + 1
                        arg_1DE_0(expr_1D2) = Convert.ToInt32(text)
                    End If
                End While
            End If
            If array2(0) > 0 Then
                If dateFormat = DateTimeUtils.DateFormat.DDMMYY OrElse dateFormat = DateTimeUtils.DateFormat.DDYYMM Then
                    day = array2(0)
                Else
                    If dateFormat = DateTimeUtils.DateFormat.MMDDYY OrElse dateFormat = DateTimeUtils.DateFormat.MMYYDD Then
                        month = array2(0)
                    Else
                        If array2(0) < 50 Then
                            year = 2000 + array2(0)
                        Else
                            If array2(0) < 100 Then
                                year = 1900 + array2(0)
                            Else
                                year = array2(0)
                            End If
                        End If
                    End If
                End If
            End If
            If array2(1) > 0 Then
                If dateFormat = DateTimeUtils.DateFormat.MMDDYY OrElse dateFormat = DateTimeUtils.DateFormat.YYDDMM Then
                    day = array2(1)
                Else
                    If dateFormat = DateTimeUtils.DateFormat.DDMMYY OrElse dateFormat = DateTimeUtils.DateFormat.YYMMDD Then
                        month = array2(1)
                    Else
                        If array2(1) < 50 Then
                            year = 2000 + array2(1)
                        Else
                            If array2(1) < 100 Then
                                year = 1900 + array2(1)
                            Else
                                year = array2(1)
                            End If
                        End If
                    End If
                End If
            End If
            If array2(2) > 0 Then
                If dateFormat = DateTimeUtils.DateFormat.MMYYDD OrElse dateFormat = DateTimeUtils.DateFormat.YYMMDD Then
                    day = array2(2)
                Else
                    If dateFormat = DateTimeUtils.DateFormat.DDYYMM OrElse dateFormat = DateTimeUtils.DateFormat.YYDDMM Then
                        month = array2(2)
                    Else
                        If array2(2) < 50 Then
                            year = 2000 + array2(2)
                        Else
                            If array2(2) < 100 Then
                                year = 1900 + array2(2)
                            Else
                                year = array2(2)
                            End If
                        End If
                    End If
                End If
            End If
            Return New DateTime(year, month, day)
        End Function
    End Class
End Namespace
