﻿Imports System.IO
Imports Newtonsoft.Json

Namespace Misc
    Class JsonUtil
        Public Shared Function JsonPrettify(json As String) As String
            Using stringReader = New StringReader(json)
                Using stringWriter = New StringWriter()
                    Dim jsonReader = New JsonTextReader(stringReader)
                    Dim jsonWriter = New JsonTextWriter(stringWriter) With {.Formatting = Formatting.Indented}
                    JsonWriter.WriteToken(jsonReader)
                    Return stringWriter.ToString()
                End Using
            End Using
        End Function
    End Class
End Namespace
