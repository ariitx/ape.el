﻿Imports System.Security.Permissions
Imports System.ComponentModel

Namespace Misc.VB
    Public Module Conversion
        Public Function ErrorToString() As String
            Return Microsoft.VisualBasic.Conversion.ErrorToString
        End Function

        ''' <summary>
        '''     Returns the error message that corresponds to a given error number.
        ''' </summary>
        ''' <returns>
        '''     Returns the error message that corresponds to a given error number.
        ''' </returns>
        ''' <param name="ErrorNumber">
        '''     Optional. Any valid error number.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function ErrorToString(ErrorNumber As Integer) As String
            Return Microsoft.VisualBasic.Conversion.ErrorToString(ErrorNumber)
        End Function

        ''' <summary>
        '''     Return the integer portion of a number.
        ''' </summary>
        ''' <returns>
        '''     Return the integer portion of a number.
        ''' </returns>
        ''' <param name="Number">
        '''     Required. A number of type Double or any valid numeric expression. If <paramref name="Number" /> contains Nothing, Nothing is returned.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function Fix(Number As Short) As Short
            Return Microsoft.VisualBasic.Conversion.Fix(Number)
        End Function

        ''' <summary>
        '''     Return the integer portion of a number.
        ''' </summary>
        ''' <returns>
        '''     Return the integer portion of a number.
        ''' </returns>
        ''' <param name="Number">
        '''     Required. A number of type Double or any valid numeric expression. If <paramref name="Number" /> contains Nothing, Nothing is returned.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function Fix(Number As Integer) As Integer
            Return Microsoft.VisualBasic.Conversion.Fix(Number)
        End Function

        ''' <summary>
        '''     Return the integer portion of a number.
        ''' </summary>
        ''' <returns>
        '''     Return the integer portion of a number.
        ''' </returns>
        ''' <param name="Number">
        '''     Required. A number of type Double or any valid numeric expression. If <paramref name="Number" /> contains Nothing, Nothing is returned.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function Fix(Number As Long) As Long
            Return Microsoft.VisualBasic.Conversion.Fix(Number)
        End Function

        ''' <summary>
        '''     Return the integer portion of a number.
        ''' </summary>
        ''' <returns>
        '''     Return the integer portion of a number.
        ''' </returns>
        ''' <param name="Number">
        '''     Required. A number of type Double or any valid numeric expression. If <paramref name="Number" /> contains Nothing, Nothing is returned.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function Fix(Number As Double) As Double
            Return Microsoft.VisualBasic.Conversion.Fix(Number)
        End Function

        ''' <summary>
        '''     Return the integer portion of a number.
        ''' </summary>
        ''' <returns>
        '''     Return the integer portion of a number.
        ''' </returns>
        ''' <param name="Number">
        '''     Required. A number of type Double or any valid numeric expression. If <paramref name="Number" /> contains Nothing, Nothing is returned.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function Fix(Number As Single) As Single
            Return Microsoft.VisualBasic.Conversion.Fix(Number)
        End Function

        ''' <summary>
        '''     Return the integer portion of a number.
        ''' </summary>
        ''' <returns>
        '''     Return the integer portion of a number.
        ''' </returns>
        ''' <param name="Number">
        '''     Required. A number of type Double or any valid numeric expression. If <paramref name="Number" /> contains Nothing, Nothing is returned.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function Fix(Number As Decimal) As Decimal
            Return Microsoft.VisualBasic.Conversion.Fix(Number)
        End Function

        ''' <summary>
        '''     Return the integer portion of a number.
        ''' </summary>
        ''' <returns>
        '''     Return the integer portion of a number.
        ''' </returns>
        ''' <param name="Number">
        '''     Required. A number of type Double or any valid numeric expression. If <paramref name="Number" /> contains Nothing, Nothing is returned.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function Fix(Number As Object) As Object
            Return Microsoft.VisualBasic.Conversion.Fix(Number)
        End Function

        ''' <summary>
        '''     Return the integer portion of a number.
        ''' </summary>
        ''' <returns>
        '''     Return the integer portion of a number.
        ''' </returns>
        ''' <param name="Number">
        '''     Required. A number of type Double or any valid numeric expression. If <paramref name="Number" /> contains Nothing, Nothing is returned.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function Int(Number As Short) As Short
            Return Microsoft.VisualBasic.Conversion.Int(Number)
        End Function

        ''' <summary>
        '''     Return the integer portion of a number.
        ''' </summary>
        ''' <returns>
        '''     Return the integer portion of a number.
        ''' </returns>
        ''' <param name="Number">
        '''     Required. A number of type Double or any valid numeric expression. If <paramref name="Number" /> contains Nothing, Nothing is returned.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function Int(Number As Integer) As Integer
            Return Microsoft.VisualBasic.Conversion.Int(Number)
        End Function

        ''' <summary>
        '''     Return the integer portion of a number.
        ''' </summary>
        ''' <returns>
        '''     Return the integer portion of a number.
        ''' </returns>
        ''' <param name="Number">
        '''     Required. A number of type Double or any valid numeric expression. If <paramref name="Number" /> contains Nothing, Nothing is returned.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function Int(Number As Long) As Long
            Return Microsoft.VisualBasic.Conversion.Int(Number)
        End Function

        ''' <summary>
        '''     Return the integer portion of a number.
        ''' </summary>
        ''' <returns>
        '''     Return the integer portion of a number.
        ''' </returns>
        ''' <param name="Number">
        '''     Required. A number of type Double or any valid numeric expression. If <paramref name="Number" /> contains Nothing, Nothing is returned.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function Int(Number As Double) As Double
            Return Microsoft.VisualBasic.Conversion.Int(Number)
        End Function

        ''' <summary>
        '''     Return the integer portion of a number.
        ''' </summary>
        ''' <returns>
        '''     Return the integer portion of a number.
        ''' </returns>
        ''' <param name="Number">
        '''     Required. A number of type Double or any valid numeric expression. If <paramref name="Number" /> contains Nothing, Nothing is returned.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function Int(Number As Single) As Single
            Return Microsoft.VisualBasic.Conversion.Int(Number)
        End Function

        ''' <summary>
        '''     Return the integer portion of a number.
        ''' </summary>
        ''' <returns>
        '''     Return the integer portion of a number.
        ''' </returns>
        ''' <param name="Number">
        '''     Required. A number of type Double or any valid numeric expression. If <paramref name="Number" /> contains Nothing, Nothing is returned.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function Int(Number As Decimal) As Decimal
            Return Microsoft.VisualBasic.Conversion.Int(Number)
        End Function

        ''' <summary>
        '''     Return the integer portion of a number.
        ''' </summary>
        ''' <returns>
        '''     Return the integer portion of a number.
        ''' </returns>
        ''' <param name="Number">
        '''     Required. A number of type Double or any valid numeric expression. If <paramref name="Number" /> contains Nothing, Nothing is returned.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function Int(Number As Object) As Object
            Return Microsoft.VisualBasic.Conversion.Int(Number)
        End Function

        ''' <summary>
        '''     Returns a string representing the hexadecimal value of a number.
        ''' </summary>
        ''' <returns>
        '''     Returns a string representing the hexadecimal value of a number.
        ''' </returns>
        ''' <param name="Number">
        '''     Required. Any valid numeric expression or String expression.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        '''   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
        ''' </PermissionSet>
        <CLSCompliant(False)>
        Public Function Hex(Number As SByte) As String
            Return Microsoft.VisualBasic.Conversion.Hex(Number)
        End Function

        ''' <summary>
        '''     Returns a string representing the hexadecimal value of a number.
        ''' </summary>
        ''' <returns>
        '''     Returns a string representing the hexadecimal value of a number.
        ''' </returns>
        ''' <param name="Number">
        '''     Required. Any valid numeric expression or String expression.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        '''   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
        ''' </PermissionSet>
        Public Function Hex(Number As Byte) As String
            Return Microsoft.VisualBasic.Conversion.Hex(Number)
        End Function

        ''' <summary>
        '''     Returns a string representing the hexadecimal value of a number.
        ''' </summary>
        ''' <returns>
        '''     Returns a string representing the hexadecimal value of a number.
        ''' </returns>
        ''' <param name="Number">
        '''     Required. Any valid numeric expression or String expression.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        '''   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
        ''' </PermissionSet>
        Public Function Hex(Number As Short) As String
            Return Microsoft.VisualBasic.Conversion.Hex(Number)
        End Function

        ''' <summary>
        '''     Returns a string representing the hexadecimal value of a number.
        ''' </summary>
        ''' <returns>
        '''     Returns a string representing the hexadecimal value of a number.
        ''' </returns>
        ''' <param name="Number">
        '''     Required. Any valid numeric expression or String expression.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        '''   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
        ''' </PermissionSet>
        <CLSCompliant(False)>
        Public Function Hex(Number As UShort) As String
            Return Microsoft.VisualBasic.Conversion.Hex(Number)
        End Function

        ''' <summary>
        '''     Returns a string representing the hexadecimal value of a number.
        ''' </summary>
        ''' <returns>
        '''     Returns a string representing the hexadecimal value of a number.
        ''' </returns>
        ''' <param name="Number">
        '''     Required. Any valid numeric expression or String expression.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        '''   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
        ''' </PermissionSet>
        Public Function Hex(Number As Integer) As String
            Return Microsoft.VisualBasic.Conversion.Hex(Number)
        End Function

        ''' <summary>
        '''     Returns a string representing the hexadecimal value of a number.
        ''' </summary>
        ''' <returns>
        '''     Returns a string representing the hexadecimal value of a number.
        ''' </returns>
        ''' <param name="Number">
        '''     Required. Any valid numeric expression or String expression.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        '''   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
        ''' </PermissionSet>
        <CLSCompliant(False)>
        Public Function Hex(Number As UInteger) As String
            Return Microsoft.VisualBasic.Conversion.Hex(Number)
        End Function

        ''' <summary>
        '''     Returns a string representing the hexadecimal value of a number.
        ''' </summary>
        ''' <returns>
        '''     Returns a string representing the hexadecimal value of a number.
        ''' </returns>
        ''' <param name="Number">
        '''     Required. Any valid numeric expression or String expression.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        '''   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
        ''' </PermissionSet>
        Public Function Hex(Number As Long) As String
            Return Microsoft.VisualBasic.Conversion.Hex(Number)
        End Function

        ''' <summary>
        '''     Returns a string representing the hexadecimal value of a number.
        ''' </summary>
        ''' <returns>
        '''     Returns a string representing the hexadecimal value of a number.
        ''' </returns>
        ''' <param name="Number">
        '''     Required. Any valid numeric expression or String expression.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        '''   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
        ''' </PermissionSet>
        <CLSCompliant(False)>
        Public Function Hex(Number As ULong) As String
            Return Microsoft.VisualBasic.Conversion.Hex(Number)
        End Function

        ''' <summary>
        '''     Returns a string representing the hexadecimal value of a number.
        ''' </summary>
        ''' <returns>
        '''     Returns a string representing the hexadecimal value of a number.
        ''' </returns>
        ''' <param name="Number">
        '''     Required. Any valid numeric expression or String expression.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        '''   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
        ''' </PermissionSet>
        Public Function Hex(Number As Object) As String
            Return Microsoft.VisualBasic.Conversion.Hex(Number)
        End Function

        ''' <summary>
        '''     Returns a string representing the octal value of a number.
        ''' </summary>
        ''' <returns>
        '''     Returns a string representing the octal value of a number.
        ''' </returns>
        ''' <param name="Number">
        '''     Required. Any valid numeric expression or String expression.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        '''   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
        ''' </PermissionSet>
        <CLSCompliant(False)>
        Public Function Oct(Number As SByte) As String
            Return Microsoft.VisualBasic.Conversion.Oct(Number)
        End Function

        ''' <summary>
        '''     Returns a string representing the octal value of a number.
        ''' </summary>
        ''' <returns>
        '''     Returns a string representing the octal value of a number.
        ''' </returns>
        ''' <param name="Number">
        '''     Required. Any valid numeric expression or String expression.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        '''   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
        ''' </PermissionSet>
        Public Function Oct(Number As Byte) As String
            Return Microsoft.VisualBasic.Conversion.Oct(Number)
        End Function

        ''' <summary>
        '''     Returns a string representing the octal value of a number.
        ''' </summary>
        ''' <returns>
        '''     Returns a string representing the octal value of a number.
        ''' </returns>
        ''' <param name="Number">
        '''     Required. Any valid numeric expression or String expression.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        '''   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
        ''' </PermissionSet>
        Public Function Oct(Number As Short) As String
            Return Microsoft.VisualBasic.Conversion.Oct(Number)
        End Function

        ''' <summary>
        '''     Returns a string representing the octal value of a number.
        ''' </summary>
        ''' <returns>
        '''     Returns a string representing the octal value of a number.
        ''' </returns>
        ''' <param name="Number">
        '''     Required. Any valid numeric expression or String expression.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        '''   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
        ''' </PermissionSet>
        <CLSCompliant(False)>
        Public Function Oct(Number As UShort) As String
            Return Microsoft.VisualBasic.Conversion.Oct(Number)
        End Function

        ''' <summary>
        '''     Returns a string representing the octal value of a number.
        ''' </summary>
        ''' <returns>
        '''     Returns a string representing the octal value of a number.
        ''' </returns>
        ''' <param name="Number">
        '''     Required. Any valid numeric expression or String expression.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        '''   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
        ''' </PermissionSet>
        Public Function Oct(Number As Integer) As String
            Return Microsoft.VisualBasic.Conversion.Oct(Number)
        End Function

        ''' <summary>
        '''     Returns a string representing the octal value of a number.
        ''' </summary>
        ''' <returns>
        '''     Returns a string representing the octal value of a number.
        ''' </returns>
        ''' <param name="Number">
        '''     Required. Any valid numeric expression or String expression.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        '''   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
        ''' </PermissionSet>
        <CLSCompliant(False)>
        Public Function Oct(Number As UInteger) As String
            Return Microsoft.VisualBasic.Conversion.Oct(Number)
        End Function

        ''' <summary>
        '''     Returns a string representing the octal value of a number.
        ''' </summary>
        ''' <returns>
        '''     Returns a string representing the octal value of a number.
        ''' </returns>
        ''' <param name="Number">
        '''     Required. Any valid numeric expression or String expression.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        '''   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
        ''' </PermissionSet>
        Public Function Oct(Number As Long) As String
            Return Microsoft.VisualBasic.Conversion.Oct(Number)
        End Function

        ''' <summary>
        '''     Returns a string representing the octal value of a number.
        ''' </summary>
        ''' <returns>
        '''     Returns a string representing the octal value of a number.
        ''' </returns>
        ''' <param name="Number">
        '''     Required. Any valid numeric expression or String expression.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        '''   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
        ''' </PermissionSet>
        <CLSCompliant(False)>
        Public Function Oct(Number As ULong) As String
            Return Microsoft.VisualBasic.Conversion.Oct(Number)
        End Function

        ''' <summary>
        '''     Returns a string representing the octal value of a number.
        ''' </summary>
        ''' <returns>
        '''     Returns a string representing the octal value of a number.
        ''' </returns>
        ''' <param name="Number">
        '''     Required. Any valid numeric expression or String expression.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        '''   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
        ''' </PermissionSet>
        Public Function Oct(Number As Object) As String
            Return Microsoft.VisualBasic.Conversion.Oct(Number)
        End Function

        ''' <summary>
        '''     Returns a String representation of a number.
        ''' </summary>
        ''' <returns>
        '''     Returns a String representation of a number.
        ''' </returns>
        ''' <param name="Number">
        '''     Required. An Object containing any valid numeric expression.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        '''   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
        ''' </PermissionSet>
        Public Function Str(Number As Object) As String
            Return Microsoft.VisualBasic.Conversion.Str(Number)
        End Function


        ''' <summary>
        '''     Returns the numbers contained in a string as a numeric value of appropriate type.
        ''' </summary>
        ''' <returns>
        '''     Returns the numbers contained in a string as a numeric value of appropriate type.
        ''' </returns>
        ''' <param name="InputStr">
        '''     Required. Any valid String expression, Object variable, or Char value. If <paramref name="Expression" /> is of type Object, its value must be convertible to String or an <see cref="T:System.ArgumentException" /> error occurs.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        '''   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
        ''' </PermissionSet>
        Public Function Val(InputStr As String) As Double
            Return Microsoft.VisualBasic.Conversion.Val(InputStr)
        End Function

        ''' <summary>
        '''     Returns the numbers contained in a string as a numeric value of appropriate type.
        ''' </summary>
        ''' <returns>
        '''     Returns the numbers contained in a string as a numeric value of appropriate type.
        ''' </returns>
        ''' <param name="Expression">
        '''     Required. Any valid String expression, Object variable, or Char value. If <paramref name="Expression" /> is of type Object, its value must be convertible to String or an <see cref="T:System.ArgumentException" /> error occurs.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        '''   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
        ''' </PermissionSet>
        Public Function Val(Expression As Char) As Integer
            Return Microsoft.VisualBasic.Conversion.Val(Expression)
        End Function

        ''' <summary>
        '''     Returns the numbers contained in a string as a numeric value of appropriate type.
        ''' </summary>
        ''' <returns>
        '''     Returns the numbers contained in a string as a numeric value of appropriate type.
        ''' </returns>
        ''' <param name="Expression">
        '''     Required. Any valid String expression, Object variable, or Char value. If <paramref name="Expression" /> is of type Object, its value must be convertible to String or an <see cref="T:System.ArgumentException" /> error occurs.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        '''   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
        ''' </PermissionSet>
        Public Function Val(Expression As Object) As Double
            Return Microsoft.VisualBasic.Conversion.Val(Expression)
        End Function
    End Module

    Public Module DateAndTime
        ''' <summary>
        ''' Returns a Date value containing a date and time value to which a specified time interval has been added.
        ''' </summary>
        ''' <returns>
        ''' Returns a Date value containing a date and time value to which a specified time interval has been added.
        ''' </returns>
        ''' <param name="Interval">
        ''' Required. DateInterval enumeration value or String expression representing the time interval you want to add.
        ''' </param>
        ''' <param name="Number">
        ''' Required. Double. Floating-point expression representing the number of intervals you want to add. <paramref name="Number" /> can be positive (to get date/time values in the future) or negative (to get date/time values in the past). It can contain a fractional part when <paramref name="Interval" /> specifies hours, minutes, or seconds. For other values of <paramref name="Interval" />, any fractional part of <paramref name="Number" /> is ignored.
        ''' </param>
        ''' <param name="DateValue">
        ''' Required. Date. An expression representing the date and time to which the interval is to be added. <paramref name="DateValue" /> itself is not changed in the calling program.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        '''   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
        ''' </PermissionSet>
        Public Function DateAdd(Interval As DateInterval, Number As Double, DateValue As DateTime) As DateTime
            Return Microsoft.VisualBasic.DateAndTime.DateAdd(Interval, Number, DateValue)
        End Function

        ''' <summary>
        ''' Returns a Long value specifying the number of time intervals between two Date values.
        ''' </summary>
        ''' <returns>
        ''' Returns a Long value specifying the number of time intervals between two Date values.
        ''' </returns>
        ''' <param name="Interval">
        ''' Required. DateInterval enumeration value or String expression representing the time interval you want to use as the unit of difference between <paramref name="Date1" /> and <paramref name="Date2" />.
        ''' </param>
        ''' <param name="Date1">
        ''' Required. Date. The first date/time value you want to use in the calculation. 
        ''' </param>
        ''' <param name="Date2">
        ''' Required. Date. The second date/time value you want to use in the calculation.
        ''' </param>
        ''' <param name="DayOfWeek">
        ''' Optional. A value chosen from the FirstDayOfWeek enumeration that specifies the first day of the week. If not specified, FirstDayOfWeek.Sunday is used.
        ''' </param>
        ''' <param name="WeekOfYear">
        ''' Optional. A value chosen from the FirstWeekOfYear enumeration that specifies the first week of the year. If not specified, FirstWeekOfYear.Jan1 is used.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        '''   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
        ''' </PermissionSet>
        Public Function DateDiff(Interval As DateInterval, Date1 As DateTime, Date2 As DateTime, Optional DayOfWeek As FirstDayOfWeek = FirstDayOfWeek.Sunday, Optional WeekOfYear As FirstWeekOfYear = FirstWeekOfYear.Jan1) As Long
            Return Microsoft.VisualBasic.DateAndTime.DateDiff(Interval, Date1, Date2, DayOfWeek, WeekOfYear)
        End Function

        ''' <summary>
        ''' Returns an Integer value containing the specified component of a given Date value.
        ''' </summary>
        ''' <returns>
        ''' Returns an Integer value containing the specified component of a given Date value.
        ''' </returns>
        ''' <param name="Interval">
        ''' Required. DateInterval enumeration value or String expression representing the part of the date/time value you want to return.
        ''' </param>
        ''' <param name="DateValue">
        ''' Required. Date value that you want to evaluate.
        ''' </param>
        ''' <param name="FirstDayOfWeekValue">
        ''' Optional. A value chosen from the FirstDayOfWeek enumeration that specifies the first day of the week. If not specified, FirstDayOfWeek.Sunday is used.
        ''' </param>
        ''' <param name="FirstWeekOfYearValue">
        ''' Optional. A value chosen from the FirstWeekOfYear enumeration that specifies the first week of the year. If not specified, FirstWeekOfYear.Jan1 is used.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        '''   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
        ''' </PermissionSet>
        Public Function DatePart(Interval As DateInterval, DateValue As DateTime, Optional FirstDayOfWeekValue As FirstDayOfWeek = FirstDayOfWeek.Sunday, Optional FirstWeekOfYearValue As FirstWeekOfYear = FirstWeekOfYear.Jan1) As Integer
            Return Microsoft.VisualBasic.DateAndTime.DatePart(Interval, DateValue, FirstDayOfWeekValue, FirstWeekOfYearValue)
        End Function

        ''' <summary>
        ''' Returns a Date value containing a date and time value to which a specified time interval has been added.
        ''' </summary>
        ''' <returns>
        ''' Returns a Date value containing a date and time value to which a specified time interval has been added.
        ''' </returns>
        ''' <param name="Interval">
        ''' Required. DateInterval enumeration value or String expression representing the time interval you want to add.
        ''' </param>
        ''' <param name="Number">
        ''' Required. Double. Floating-point expression representing the number of intervals you want to add. <paramref name="Number" /> can be positive (to get date/time values in the future) or negative (to get date/time values in the past). It can contain a fractional part when <paramref name="Interval" /> specifies hours, minutes, or seconds. For other values of <paramref name="Interval" />, any fractional part of <paramref name="Number" /> is ignored.
        ''' </param>
        ''' <param name="DateValue">
        ''' Required. Date. An expression representing the date and time to which the interval is to be added. <paramref name="DateValue" /> itself is not changed in the calling program.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        '''   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
        ''' </PermissionSet>
        Public Function DateAdd(Interval As String, Number As Double, DateValue As Object) As DateTime
            Return Microsoft.VisualBasic.DateAndTime.DateAdd(Interval, Number, DateValue)
        End Function

        ''' <summary>
        ''' Returns a Long value specifying the number of time intervals between two Date values.
        ''' </summary>
        ''' <returns>
        ''' Returns a Long value specifying the number of time intervals between two Date values.
        ''' </returns>
        ''' <param name="Interval">
        ''' Required. DateInterval enumeration value or String expression representing the time interval you want to use as the unit of difference between <paramref name="Date1" /> and <paramref name="Date2" />.
        ''' </param>
        ''' <param name="Date1">
        ''' Required. Date. The first date/time value you want to use in the calculation. 
        ''' </param>
        ''' <param name="Date2">
        ''' Required. Date. The second date/time value you want to use in the calculation.
        ''' </param>
        ''' <param name="DayOfWeek">
        ''' Optional. A value chosen from the FirstDayOfWeek enumeration that specifies the first day of the week. If not specified, FirstDayOfWeek.Sunday is used.
        ''' </param>
        ''' <param name="WeekOfYear">
        ''' Optional. A value chosen from the FirstWeekOfYear enumeration that specifies the first week of the year. If not specified, FirstWeekOfYear.Jan1 is used.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        '''   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
        ''' </PermissionSet>
        Public Function DateDiff(Interval As String, Date1 As Object, Date2 As Object, Optional DayOfWeek As FirstDayOfWeek = FirstDayOfWeek.Sunday, Optional WeekOfYear As FirstWeekOfYear = FirstWeekOfYear.Jan1) As Long
            Return Microsoft.VisualBasic.DateAndTime.DateDiff(Interval, Date1, Date2, DayOfWeek, WeekOfYear)
        End Function

        ''' <summary>
        ''' Returns an Integer value containing the specified component of a given Date value.
        ''' </summary>
        ''' <returns>
        ''' Returns an Integer value containing the specified component of a given Date value.
        ''' </returns>
        ''' <param name="Interval">
        ''' Required. DateInterval enumeration value or String expression representing the part of the date/time value you want to return.
        ''' </param>
        ''' <param name="DateValue">
        ''' Required. Date value that you want to evaluate.
        ''' </param>
        ''' <param name="DayOfWeek">
        ''' Optional. A value chosen from the FirstDayOfWeek enumeration that specifies the first day of the week. If not specified, FirstDayOfWeek.Sunday is used.
        ''' </param>
        ''' <param name="WeekOfYear">
        ''' Optional. A value chosen from the FirstWeekOfYear enumeration that specifies the first week of the year. If not specified, FirstWeekOfYear.Jan1 is used.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        '''   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
        ''' </PermissionSet>
        Public Function DatePart(Interval As String, DateValue As Object, Optional DayOfWeek As FirstDayOfWeek = FirstDayOfWeek.Sunday, Optional WeekOfYear As FirstWeekOfYear = FirstWeekOfYear.Jan1) As Integer
            Return Microsoft.VisualBasic.DateAndTime.DatePart(Interval, DateValue, DayOfWeek, WeekOfYear)
        End Function

        ''' <summary>
        ''' Returns a Date value representing a specified year, month, and day, with the time information set to midnight (00:00:00).
        ''' </summary>
        ''' <returns>
        ''' Returns a Date value representing a specified year, month, and day, with the time information set to midnight (00:00:00).
        ''' </returns>
        ''' <param name="Year">
        ''' Required. Integer expression from 1 through 9999. However, values below this range are also accepted. If <paramref name="Year" /> is 0 through 99, it is interpreted as being between 1930 and 2029, as explained in the "Remarks" section below. If <paramref name="Year" /> is less than 1, it is subtracted from the current year.
        ''' </param>
        ''' <param name="Month">
        ''' Required. Integer expression from 1 through 12. However, values outside this range are also accepted. The value of <paramref name="Month" /> is offset by 1 and applied to January of the calculated year. In other words, (<paramref name="Month" /> - 1) is added to January. The year is recalculated if necessary. The following results illustrate this effect:
        '''
        ''' If <paramref name="Month" /> is 1, the result is January of the calculated year.
        '''
        ''' If <paramref name="Month" /> is 0, the result is December of the previous year.
        '''
        ''' If <paramref name="Month" /> is -1, the result is November of the previous year.
        '''
        ''' If <paramref name="Month" /> is 13, the result is January of the following year.
        ''' </param>
        ''' <param name="Day">
        ''' Required. Integer expression from 1 through 31. However, values outside this range are also accepted. The value of <paramref name="Day" /> is offset by 1 and applied to the first day of the calculated month. In other words, (<paramref name="Day" /> - 1) is added to the first of the month. The month and year are recalculated if necessary. The following results illustrate this effect:
        '''
        ''' If <paramref name="Day" /> is 1, the result is the first day of the calculated month.
        '''
        ''' If <paramref name="Day" /> is 0, the result is the last day of the previous month.
        '''
        ''' If <paramref name="Day" /> is -1, the result is the penultimate day of the previous month.
        '''
        ''' If <paramref name="Day" /> is past the end of the current month, the result is the appropriate day of the following month. For example, if <paramref name="Month" /> is 4 and <paramref name="Day" /> is 31, the result is May 1.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function DateSerial(Year As Integer, Month As Integer, Day As Integer) As DateTime
            Return Microsoft.VisualBasic.DateAndTime.DateSerial(Year, Month, Day)
        End Function

        ''' <summary>
        ''' Returns a Date value representing a specified hour, minute, and second, with the date information set relative to January 1 of the year 1.
        ''' </summary>
        ''' <returns>
        ''' Returns a Date value representing a specified hour, minute, and second, with the date information set relative to January 1 of the year 1.
        ''' </returns>
        ''' <param name="Hour">
        ''' Required. Integer expression from 0 through 23. However, values outside this range are also accepted.
        ''' </param>
        ''' <param name="Minute">
        ''' Required. Integer expression from 0 through 59. However, values outside this range are also accepted. The value of <paramref name="Minute" /> is added to the calculated hour, so a negative value specifies minutes before that hour.
        ''' </param>
        ''' <param name="Second">
        ''' Required. Integer expression from 0 through 59. However, values outside this range are also accepted. The value of <paramref name="Second" /> is added to the calculated minute, so a negative value specifies seconds before that minute.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function TimeSerial(Hour As Integer, Minute As Integer, Second As Integer) As DateTime
            Return Microsoft.VisualBasic.DateAndTime.TimeSerial(Hour, Minute, Second)
        End Function

        ''' <summary>
        ''' Returns a Date value containing the date information represented by a string, with the time information set to midnight (00:00:00).
        ''' </summary>
        ''' <returns>
        ''' Returns a Date value containing the date information represented by a string, with the time information set to midnight (00:00:00).
        ''' </returns>
        ''' <param name="StringDate">
        ''' Required. String expression representing a date/time value from 00:00:00 on January 1 of the year 1 through 23:59:59 on December 31, 9999.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function DateValue(StringDate As String) As DateTime
            Return Microsoft.VisualBasic.DateAndTime.DateValue(StringDate)
        End Function

        ''' <summary>
        ''' Returns a Date value containing the time information represented by a string, with the date information set to January 1 of the year 1.
        ''' </summary>
        ''' <returns>
        ''' Returns a Date value containing the time information represented by a string, with the date information set to January 1 of the year 1.
        ''' </returns>
        ''' <param name="StringTime">
        ''' Required. String expression representing a date/time value from 00:00:00 on January 1 of the year 1 through 23:59:59 on December 31, 9999.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function TimeValue(StringTime As String) As DateTime
            Return Microsoft.VisualBasic.DateAndTime.TimeValue(StringTime)
        End Function

        ''' <summary>
        ''' Returns an Integer value from 1 through 9999 representing the year.
        ''' </summary>
        ''' <returns>
        ''' Returns an Integer value from 1 through 9999 representing the year.
        ''' </returns>
        ''' <param name="DateValue">
        ''' Required. Date value from which you want to extract the year.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function Year(DateValue As DateTime) As Integer
            Return Microsoft.VisualBasic.DateAndTime.Year(DateValue)
        End Function

        ''' <summary>
        ''' Returns an Integer value from 1 through 12 representing the month of the year.
        ''' </summary>
        ''' <returns>
        ''' Returns an Integer value from 1 through 12 representing the month of the year.
        ''' </returns>
        ''' <param name="DateValue">
        ''' Required. Date value from which you want to extract the month.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function Month(DateValue As DateTime) As Integer
            Return Microsoft.VisualBasic.DateAndTime.Month(DateValue)
        End Function

        ''' <summary>
        ''' Returns an Integer value from 1 through 31 representing the day of the month.
        ''' </summary>
        ''' <returns>
        ''' Returns an Integer value from 1 through 31 representing the day of the month.
        ''' </returns>
        ''' <param name="DateValue">
        ''' Required. Date value from which you want to extract the day.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function Day(DateValue As DateTime) As Integer
            Return Microsoft.VisualBasic.DateAndTime.Day(DateValue)
        End Function

        ''' <summary>
        ''' Returns an Integer value from 0 through 23 representing the hour of the day.
        ''' </summary>
        ''' <returns>
        ''' Returns an Integer value from 0 through 23 representing the hour of the day.
        ''' </returns>
        ''' <param name="TimeValue">
        ''' Required. Date value from which you want to extract the hour.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function Hour(TimeValue As DateTime) As Integer
            Return Microsoft.VisualBasic.DateAndTime.Hour(TimeValue)
        End Function

        ''' <summary>
        ''' Returns an Integer value from 0 through 59 representing the minute of the hour.
        ''' </summary>
        ''' <returns>
        ''' Returns an Integer value from 0 through 59 representing the minute of the hour.
        ''' </returns>
        ''' <param name="TimeValue">
        ''' Required. Date value from which you want to extract the minute.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function Minute(TimeValue As DateTime) As Integer
            Return Microsoft.VisualBasic.DateAndTime.Minute(TimeValue)
        End Function

        ''' <summary>
        ''' Returns an Integer value from 0 through 59 representing the second of the minute.
        ''' </summary>
        ''' <returns>
        ''' Returns an Integer value from 0 through 59 representing the second of the minute.
        ''' </returns>
        ''' <param name="TimeValue">
        ''' Required. Date value from which you want to extract the second.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function Second(TimeValue As DateTime) As Integer
            Return Microsoft.VisualBasic.DateAndTime.Second(TimeValue)
        End Function

        ''' <summary>
        ''' Returns an Integer value containing a number representing the day of the week.
        ''' </summary>
        ''' <returns>
        ''' Returns an Integer value containing a number representing the day of the week.
        ''' </returns>
        ''' <param name="DateValue">
        ''' Required. Date value for which you want to determine the day of the week.
        ''' </param>
        ''' <param name="DayOfWeek">
        ''' Optional. A value chosen from the FirstDayOfWeek enumeration that specifies the first day of the week. If not specified, FirstDayOfWeek.Sunday is used.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function Weekday(DateValue As DateTime, Optional DayOfWeek As FirstDayOfWeek = FirstDayOfWeek.Sunday) As Integer
            Return Microsoft.VisualBasic.DateAndTime.Weekday(DateValue, DayOfWeek)
        End Function

        ''' <summary>
        ''' Returns a String value containing the name of the specified month.
        ''' </summary>
        ''' <returns>
        ''' Returns a String value containing the name of the specified month.
        ''' </returns>
        ''' <param name="Month">
        ''' Required. Integer. The numeric designation of the month, from 1 through 13; 1 indicates January and 12 indicates December. You can use the value 13 with a 13-month calendar. If your system is using a 12-month calendar and <paramref name="Month" /> is 13, MonthName returns an empty string.
        ''' </param>
        ''' <param name="Abbreviate">
        ''' Optional. Boolean value that indicates if the month name is to be abbreviated. If omitted, the default is False, which means the month name is not abbreviated.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function MonthName(Month As Integer, Optional Abbreviate As Boolean = False) As String
            Return Microsoft.VisualBasic.DateAndTime.MonthName(Month, Abbreviate)
        End Function

        ''' <summary>
        ''' Returns a String value containing the name of the specified weekday.
        ''' </summary>
        ''' <returns>
        ''' Returns a String value containing the name of the specified weekday.
        ''' </returns>
        ''' <param name="Weekday">
        ''' Required. Integer. The numeric designation for the weekday, from 1 through 7; 1 indicates the first day of the week and 7 indicates the last day of the week. The identities of the first and last days depend on the setting of <paramref name="FirstDayOfWeekValue" />.
        ''' </param>
        ''' <param name="Abbreviate">
        ''' Optional. Boolean value that indicates if the weekday name is to be abbreviated. If omitted, the default is False, which means the weekday name is not abbreviated.
        ''' </param>
        ''' <param name="FirstDayOfWeekValue">
        ''' Optional. A value chosen from the FirstDayOfWeek enumeration that specifies the first day of the week. If not specified, FirstDayOfWeek.System is used.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function WeekdayName(Weekday As Integer, Optional Abbreviate As Boolean = False, Optional FirstDayOfWeekValue As FirstDayOfWeek = FirstDayOfWeek.System) As String
            Return Microsoft.VisualBasic.DateAndTime.WeekdayName(Weekday, Abbreviate, FirstDayOfWeekValue)
        End Function
    End Module

    Public Module FileSystem
        ''' <summary>
        ''' Changes the current directory or folder. The My feature gives you better productivity and performance in file I/O operations than the ChDir function. For more information, see My.Computer.FileSystem.CurrentDirectory Property.
        ''' </summary>
        ''' <param name="Path">
        ''' Required. A String expression that identifies which directory or folder becomes the new default directory or folder. <paramref name="Path" /> may include the drive. If no drive is specified, ChDir changes the default directory or folder on the current drive. 
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub ChDir(Path As String)
            Microsoft.VisualBasic.FileSystem.ChDir(Path)
        End Sub

        ''' <summary>
        ''' Changes the current drive.
        ''' </summary>
        ''' <param name="Drive">
        ''' Required. String expression that specifies an existing drive. If you supply a zero-length string (""), the current drive does not change. If the <paramref name="Drive" /> argument is a multiple-character string, ChDrive uses only the first letter.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub ChDrive(Drive As Char)
            Microsoft.VisualBasic.FileSystem.ChDrive(Drive)
        End Sub

        ''' <summary>
        ''' Changes the current drive.
        ''' </summary>
        ''' <param name="Drive">
        ''' Required. String expression that specifies an existing drive. If you supply a zero-length string (""), the current drive does not change. If the <paramref name="Drive" /> argument is a multiple-character string, ChDrive uses only the first letter.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub ChDrive(Drive As String)
            Microsoft.VisualBasic.FileSystem.ChDrive(Drive)
        End Sub

        ''' <summary>
        ''' Returns a string representing the current path. The My.Computer.FileSystem Object gives you better productivity and performance in file I/O operations than CurDir. For more information, see My.Computer.FileSystem.CurrentDirectory Property.
        ''' </summary>
        ''' <returns>
        ''' Returns a string representing the current path. The My.Computer.FileSystem Object gives you better productivity and performance in file I/O operations than CurDir. For more information, see My.Computer.FileSystem.CurrentDirectory Property.
        ''' </returns>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Function CurDir() As String
            Return Microsoft.VisualBasic.FileSystem.CurDir()
        End Function

        ''' <summary>
        ''' Returns a string representing the current path. The My.Computer.FileSystem Object gives you better productivity and performance in file I/O operations than CurDir. For more information, see My.Computer.FileSystem.CurrentDirectory Property.
        ''' </summary>
        ''' <returns>
        ''' Returns a string representing the current path. The My.Computer.FileSystem Object gives you better productivity and performance in file I/O operations than CurDir. For more information, see My.Computer.FileSystem.CurrentDirectory Property.
        ''' </returns>
        ''' <param name="Drive">
        ''' Optional. Char expression that specifies an existing drive. If no drive is specified, or if <paramref name="Drive" /> is a zero-length string (""), CurDir returns the path for the current drive.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Function CurDir(Drive As Char) As String
            Return Microsoft.VisualBasic.FileSystem.CurDir(Drive)
        End Function

        ''' <summary>
        ''' Returns a string representing the name of a file, directory, or folder that matches a specified pattern or file attribute, or the volume label of a drive. The My.Computer.FileSystem Object gives you better productivity and performance in file I/O operations than the Dir function. See My.Computer.FileSystem.GetDirectoryInfo Method for more information.
        ''' </summary>
        ''' <returns>
        ''' Returns a string representing the name of a file, directory, or folder that matches a specified pattern or file attribute, or the volume label of a drive. The My.Computer.FileSystem Object gives you better productivity and performance in file I/O operations than the Dir function. See My.Computer.FileSystem.GetDirectoryInfo Method for more information.
        ''' </returns>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Function Dir() As String
            Return Microsoft.VisualBasic.FileSystem.Dir()
        End Function

        ''' <summary>
        ''' Returns a string representing the name of a file, directory, or folder that matches a specified pattern or file attribute, or the volume label of a drive. The My.Computer.FileSystem Object gives you better productivity and performance in file I/O operations than the Dir function. See My.Computer.FileSystem.GetDirectoryInfo Method for more information.
        ''' </summary>
        ''' <returns>
        ''' Returns a string representing the name of a file, directory, or folder that matches a specified pattern or file attribute, or the volume label of a drive. The My.Computer.FileSystem Object gives you better productivity and performance in file I/O operations than the Dir function. See My.Computer.FileSystem.GetDirectoryInfo Method for more information.
        ''' </returns>
        ''' <param name="PathName">
        ''' Optional. String expression that specifies a file name, directory or folder name, or drive volume label. A zero-length string ("") is returned if <paramref name="PathName" /> is not found. 
        ''' </param>
        ''' <param name="Attributes">
        ''' Optional. Enumeration or numeric expression whose value specifies file attributes. If omitted, Dir returns files that match <paramref name="PathName" /> but have no attributes.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Function Dir(PathName As String, Optional Attributes As FileAttribute = FileAttribute.Normal) As String
            Return Microsoft.VisualBasic.FileSystem.Dir(PathName, Attributes)
        End Function

        ''' <summary>
        ''' Creates a new directory. The My feature gives you better productivity and performance in file I/O operations than MkDir. For more information, see My.Computer.FileSystem.CreateDirectory Method.
        ''' </summary>
        ''' <param name="Path">
        ''' Required. String expression that identifies the directory to be created. The <paramref name="Path" /> may include the drive. If no drive is specified, MkDir creates the new directory on the current drive.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub MkDir(Path As String)
            Microsoft.VisualBasic.FileSystem.MkDir(Path)
        End Sub

        ''' <summary>
        ''' Removes an existing directory. The My feature gives you better productivity and performance in file I/O operations than RmDir. For more information, see My.Computer.FileSystem.DeleteDirectory Method.
        ''' </summary>
        ''' <param name="Path">
        ''' Required. String expression that identifies the directory or folder to be removed. <paramref name="Path" /> can include the drive. If no drive is specified, RmDir removes the directory on the current drive.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub RmDir(Path As String)
            Microsoft.VisualBasic.FileSystem.RmDir(Path)
        End Sub

        ''' <summary>
        ''' Copies a file. The My.Computer.FileSystem Object gives you better productivity and performance in file I/O operations than FileCopy. See My.Computer.FileSystem.CopyFile Method for more information.
        ''' </summary>
        ''' <param name="Source">
        ''' Required. String expression that specifies the name of the file to be copied. <paramref name="Source" /> may include the directory or folder, and drive, of the source file.
        ''' </param>
        ''' <param name="Destination">
        ''' Required. String expression that specifies the destination file name. <paramref name="Destination" /> may include the directory or folder, and drive, of the destination file.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub FileCopy(Source As String, Destination As String)
            Microsoft.VisualBasic.FileSystem.FileCopy(Source, Destination)
        End Sub

        ''' <summary>
        ''' Returns a Date value that indicates the date and time a file was created or last modified. The My feature gives you better productivity and performance in file I/O operations than FileDateTime. For more information, see My.Computer.FileSystem.GetFileInfo Method.
        ''' </summary>
        ''' <returns>
        ''' Returns a Date value that indicates the date and time a file was created or last modified. The My feature gives you better productivity and performance in file I/O operations than FileDateTime. For more information, see My.Computer.FileSystem.GetFileInfo Method.
        ''' </returns>
        ''' <param name="PathName">
        ''' Required. String expression that specifies a file name. <paramref name="PathName" /> may include the directory or folder, and the drive.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Function FileDateTime(PathName As String) As DateTime
            Return Microsoft.VisualBasic.FileSystem.FileDateTime(PathName)
        End Function

        ''' <summary>
        ''' Returns a Long value that specifies the length of a file in bytes. The My feature gives you better productivity and performance in file I/O operations than FileLen. For more information, see My.Computer.FileSystem.GetFileInfo Method.
        ''' </summary>
        ''' <returns>
        ''' Returns a Long value that specifies the length of a file in bytes. The My feature gives you better productivity and performance in file I/O operations than FileLen. For more information, see My.Computer.FileSystem.GetFileInfo Method.
        ''' </returns>
        ''' <param name="PathName">
        ''' Required. String expression that specifies a file. <paramref name="PathName" /> may include the directory or folder, and the drive.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Function FileLen(PathName As String) As Long
            Return Microsoft.VisualBasic.FileSystem.FileLen(PathName)
        End Function

        ''' <summary>
        ''' Returns a FileAttribute value that represents the attributes of a file, directory, or folder. The My feature gives you better productivity and performance in file I/O operations than FileAttribute. For more information, see My.Computer.FileSystem Object.
        ''' </summary>
        ''' <returns>
        ''' Value Normal, Constant vbNormal, Description normal. Value ReadOnly, Constant vbReadOnly, Description read-only. Value Hidden, Constant vbHidden, Description hidden. Value System, Constant vbSystem, Description system file. Value Directory, Constant vbDirectory, Description directory or folder. Value Archive, Constant vbArchive, Description file has changed since last backup. Value Alias, Constant vbAlias, Description file has a different name.
        ''' </returns>
        ''' <param name="PathName">
        ''' Required. String expression that specifies a file, directory, or folder name. <paramref name="PathName" /> can include the directory or folder, and the drive.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Function GetAttr(PathName As String) As FileAttribute
            Return Microsoft.VisualBasic.FileSystem.GetAttr(PathName)
        End Function

        ''' <summary>
        ''' Deletes files from a disk. The My feature gives you better productivity and performance in file I/O operations than Kill. For more information, see My.Computer.FileSystem Object.
        ''' </summary>
        ''' <param name="PathName">
        ''' Required. String expression that specifies one or more file names to be deleted. <paramref name="PathName" /> can include the directory or folder, and the drive.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub Kill(PathName As String)
            Microsoft.VisualBasic.FileSystem.Kill(PathName)
        End Sub

        ''' <summary>
        ''' Sets attribute information for a file. The My feature gives you better productivity and performance in file I/O operations than SetAttr. For more information, see My.Computer.FileSystem Object.
        ''' </summary>
        ''' <param name="PathName">
        ''' Required. String expression that specifies a file name. <paramref name="PathName" /> can include directory or folder, and drive.
        ''' </param>
        ''' <param name="Attributes">
        ''' Required. Constant or numeric expression, whose sum specifies file attributes.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub SetAttr(PathName As String, Attributes As FileAttribute)
            Microsoft.VisualBasic.FileSystem.SetAttr(PathName, Attributes)
        End Sub

        ''' <summary>
        ''' Opens a file for input or output. The My feature gives you better productivity and performance in file I/O operations than FileOpen. For more information, see My.Computer.FileSystem Object. 
        ''' </summary>
        ''' <param name="FileNumber">
        ''' Required. Any valid file number. Use the FreeFile function to obtain the next available file number.
        ''' </param>
        ''' <param name="FileName">
        ''' Required. String expression that specifies a file name—may include directory or folder, and drive.
        ''' </param>
        ''' <param name="Mode">
        ''' Required. Enumeration specifying the file mode: Append, Binary, Input, Output, or Random. (For more information, see OpenMode Enumeration.)
        ''' </param>
        ''' <param name="Access">
        ''' Optional. Enumeration specifying the operations permitted on the open file: Read, Write, or ReadWrite. Defaults to ReadWrite. (For more information, see OpenAccess Enumeration.)
        ''' </param>
        ''' <param name="Share">
        ''' Optional. Enumeration specifying the operations not permitted on the open file by other processes: Shared, Lock Read, Lock Write, and Lock Read Write. Defaults to Lock Read Write. (For more information, see OpenShare Enumeration.)
        ''' </param>
        ''' <param name="RecordLength">
        ''' Optional. Number less than or equal to 32,767 (bytes). For files opened for random access, this value is the record length. For sequential files, this value is the number of characters buffered.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub FileOpen(FileNumber As Integer, FileName As String, Mode As OpenMode, Optional Access As OpenAccess = OpenAccess.[Default], Optional Share As OpenShare = OpenShare.[Default], Optional RecordLength As Integer = -1)
            Microsoft.VisualBasic.FileSystem.FileOpen(FileNumber, FileName, Mode, Access, Share, RecordLength)
        End Sub

        ''' <summary>
        ''' Concludes input/output (I/O) to a file opened using the FileOpen function. My gives you better productivity and performance in file I/O operations. See My.Computer.FileSystem Object for more information.
        ''' </summary>
        ''' <param name="FileNumbers">
        ''' Optional. Parameter array of 0 or more channels to be closed. 
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" PathDiscovery="*AllFiles*" />
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub FileClose(<System.Runtime.InteropServices.OutAttribute()> ParamArray FileNumbers As Integer())
            Microsoft.VisualBasic.FileSystem.FileClose(FileNumbers)
        End Sub

        ''' <summary>
        ''' Reads data from an open disk file into a variable.  The My feature gives you better productivity and performance in file I/O operations than FileGetObject. For more information, see My.Computer.FileSystem Object.
        ''' </summary>
        ''' <param name="FileNumber">
        ''' Required. Any valid file number.
        ''' </param>
        ''' <param name="Value">
        ''' Required. Valid variable name into which data is read.
        ''' </param>
        ''' <param name="RecordNumber">
        ''' Optional. Record number (Random mode files) or byte number (Binary mode files) at which reading starts.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub FileGetObject(FileNumber As Integer, ByRef Value As Object, Optional RecordNumber As Long = -1L)
            Microsoft.VisualBasic.FileSystem.FileGetObject(FileNumber, Value, RecordNumber)
        End Sub

        ''' <summary>
        ''' Reads data from an open disk file into a variable. The My feature gives you better productivity and performance in file I/O operations than FileGet. For more information, see My.Computer.FileSystem Object.
        ''' </summary>
        ''' <param name="FileNumber">
        ''' Required. Any valid file number.
        ''' </param>
        ''' <param name="Value">
        ''' Required. Valid variable name into which data is read.
        ''' </param>
        ''' <param name="RecordNumber">
        ''' Optional. Record number (Random mode files) or byte number (Binary mode files) at which reading starts.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub FileGet(FileNumber As Integer, ByRef Value As ValueType, Optional RecordNumber As Long = -1L)
            Microsoft.VisualBasic.FileSystem.FileGet(FileNumber, Value, RecordNumber)
        End Sub

        ''' <summary>
        ''' Reads data from an open disk file into a variable. The My feature gives you better productivity and performance in file I/O operations than FileGet. For more information, see My.Computer.FileSystem Object.
        ''' </summary>
        ''' <param name="FileNumber">
        ''' Required. Any valid file number.
        ''' </param>
        ''' <param name="Value">
        ''' Required. Valid variable name into which data is read.
        ''' </param>
        ''' <param name="RecordNumber">
        ''' Optional. Record number (Random mode files) or byte number (Binary mode files) at which reading starts.
        ''' </param>
        ''' <param name="ArrayIsDynamic">
        ''' Optional. Applies only when writing an array. Specifies whether the array is to be treated as dynamic and whether an array descriptor describing the size and bounds of the array is necessary.
        ''' </param>
        ''' <param name="StringIsFixedLength">
        ''' Optional. Applies only when writing a string. Specifies whether to write a two-byte descriptor for the string that describes the length. The default is False.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub FileGet(FileNumber As Integer, ByRef Value As Array, Optional RecordNumber As Long = -1L, Optional ArrayIsDynamic As Boolean = False, Optional StringIsFixedLength As Boolean = False)
            Microsoft.VisualBasic.FileSystem.FileGet(FileNumber, Value, RecordNumber, ArrayIsDynamic, StringIsFixedLength)
        End Sub

        ''' <summary>
        ''' Reads data from an open disk file into a variable. The My feature gives you better productivity and performance in file I/O operations than FileGet. For more information, see My.Computer.FileSystem Object.
        ''' </summary>
        ''' <param name="FileNumber">
        ''' Required. Any valid file number.
        ''' </param>
        ''' <param name="Value">
        ''' Required. Valid variable name into which data is read.
        ''' </param>
        ''' <param name="RecordNumber">
        ''' Optional. Record number (Random mode files) or byte number (Binary mode files) at which reading starts.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub FileGet(FileNumber As Integer, ByRef Value As Boolean, Optional RecordNumber As Long = -1L)
            Microsoft.VisualBasic.FileSystem.FileGet(FileNumber, Value, RecordNumber)
        End Sub

        ''' <summary>
        ''' Reads data from an open disk file into a variable. The My feature gives you better productivity and performance in file I/O operations than FileGet. For more information, see My.Computer.FileSystem Object.
        ''' </summary>
        ''' <param name="FileNumber">
        ''' Required. Any valid file number.
        ''' </param>
        ''' <param name="Value">
        ''' Required. Valid variable name into which data is read.
        ''' </param>
        ''' <param name="RecordNumber">
        ''' Optional. Record number (Random mode files) or byte number (Binary mode files) at which reading starts.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub FileGet(FileNumber As Integer, ByRef Value As Byte, Optional RecordNumber As Long = -1L)
            Microsoft.VisualBasic.FileSystem.FileGet(FileNumber, Value, RecordNumber)
        End Sub

        ''' <summary>
        ''' Reads data from an open disk file into a variable. The My feature gives you better productivity and performance in file I/O operations than FileGet. For more information, see My.Computer.FileSystem Object.
        ''' </summary>
        ''' <param name="FileNumber">
        ''' Required. Any valid file number.
        ''' </param>
        ''' <param name="Value">
        ''' Required. Valid variable name into which data is read.
        ''' </param>
        ''' <param name="RecordNumber">
        ''' Optional. Record number (Random mode files) or byte number (Binary mode files) at which reading starts.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub FileGet(FileNumber As Integer, ByRef Value As Short, Optional RecordNumber As Long = -1L)
            Microsoft.VisualBasic.FileSystem.FileGet(FileNumber, Value, RecordNumber)
        End Sub

        ''' <summary>
        ''' Reads data from an open disk file into a variable. The My feature gives you better productivity and performance in file I/O operations than FileGet. For more information, see My.Computer.FileSystem Object.
        ''' </summary>
        ''' <param name="FileNumber">
        ''' Required. Any valid file number.
        ''' </param>
        ''' <param name="Value">
        ''' Required. Valid variable name into which data is read.
        ''' </param>
        ''' <param name="RecordNumber">
        ''' Optional. Record number (Random mode files) or byte number (Binary mode files) at which reading starts.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub FileGet(FileNumber As Integer, ByRef Value As Integer, Optional RecordNumber As Long = -1L)
            Microsoft.VisualBasic.FileSystem.FileGet(FileNumber, Value, RecordNumber)
        End Sub

        ''' <summary>
        ''' Reads data from an open disk file into a variable. The My feature gives you better productivity and performance in file I/O operations than FileGet. For more information, see My.Computer.FileSystem Object.
        ''' </summary>
        ''' <param name="FileNumber">
        ''' Required. Any valid file number.
        ''' </param>
        ''' <param name="Value">
        ''' Required. Valid variable name into which data is read.
        ''' </param>
        ''' <param name="RecordNumber">
        ''' Optional. Record number (Random mode files) or byte number (Binary mode files) at which reading starts.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub FileGet(FileNumber As Integer, ByRef Value As Long, Optional RecordNumber As Long = -1L)
            Microsoft.VisualBasic.FileSystem.FileGet(FileNumber, Value, RecordNumber)
        End Sub

        ''' <summary>
        ''' Reads data from an open disk file into a variable. The My feature gives you better productivity and performance in file I/O operations than FileGet. For more information, see My.Computer.FileSystem Object.
        ''' </summary>
        ''' <param name="FileNumber">
        ''' Required. Any valid file number.
        ''' </param>
        ''' <param name="Value">
        ''' Required. Valid variable name into which data is read.
        ''' </param>
        ''' <param name="RecordNumber">
        ''' Optional. Record number (Random mode files) or byte number (Binary mode files) at which reading starts.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub FileGet(FileNumber As Integer, ByRef Value As Char, Optional RecordNumber As Long = -1L)
            Microsoft.VisualBasic.FileSystem.FileGet(FileNumber, Value, RecordNumber)
        End Sub

        ''' <summary>
        ''' Reads data from an open disk file into a variable. The My feature gives you better productivity and performance in file I/O operations than FileGet. For more information, see My.Computer.FileSystem Object.
        ''' </summary>
        ''' <param name="FileNumber">
        ''' Required. Any valid file number.
        ''' </param>
        ''' <param name="Value">
        ''' Required. Valid variable name into which data is read.
        ''' </param>
        ''' <param name="RecordNumber">
        ''' Optional. Record number (Random mode files) or byte number (Binary mode files) at which reading starts.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub FileGet(FileNumber As Integer, ByRef Value As Single, Optional RecordNumber As Long = -1L)
            Microsoft.VisualBasic.FileSystem.FileGet(FileNumber, Value, RecordNumber)
        End Sub

        ''' <summary>
        ''' Reads data from an open disk file into a variable. The My feature gives you better productivity and performance in file I/O operations than FileGet. For more information, see My.Computer.FileSystem Object.
        ''' </summary>
        ''' <param name="FileNumber">
        ''' Required. Any valid file number.
        ''' </param>
        ''' <param name="Value">
        ''' Required. Valid variable name into which data is read.
        ''' </param>
        ''' <param name="RecordNumber">
        ''' Optional. Record number (Random mode files) or byte number (Binary mode files) at which reading starts.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub FileGet(FileNumber As Integer, ByRef Value As Double, Optional RecordNumber As Long = -1L)
            Microsoft.VisualBasic.FileSystem.FileGet(FileNumber, Value, RecordNumber)
        End Sub

        ''' <summary>
        ''' Reads data from an open disk file into a variable. The My feature gives you better productivity and performance in file I/O operations than FileGet. For more information, see My.Computer.FileSystem Object.
        ''' </summary>
        ''' <param name="FileNumber">
        ''' Required. Any valid file number.
        ''' </param>
        ''' <param name="Value">
        ''' Required. Valid variable name into which data is read.
        ''' </param>
        ''' <param name="RecordNumber">
        ''' Optional. Record number (Random mode files) or byte number (Binary mode files) at which reading starts.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub FileGet(FileNumber As Integer, ByRef Value As Decimal, Optional RecordNumber As Long = -1L)
            Microsoft.VisualBasic.FileSystem.FileGet(FileNumber, Value, RecordNumber)
        End Sub

        ''' <summary>
        ''' Reads data from an open disk file into a variable. The My feature gives you better productivity and performance in file I/O operations than FileGet. For more information, see My.Computer.FileSystem Object.
        ''' </summary>
        ''' <param name="FileNumber">
        ''' Required. Any valid file number.
        ''' </param>
        ''' <param name="Value">
        ''' Required. Valid variable name into which data is read.
        ''' </param>
        ''' <param name="RecordNumber">
        ''' Optional. Record number (Random mode files) or byte number (Binary mode files) at which reading starts.
        ''' </param>
        ''' <param name="StringIsFixedLength">
        ''' Optional. Applies only when writing a string. Specifies whether to write a two-byte descriptor for the string that describes the length. The default is False.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub FileGet(FileNumber As Integer, ByRef Value As String, Optional RecordNumber As Long = -1L, Optional StringIsFixedLength As Boolean = False)
            Microsoft.VisualBasic.FileSystem.FileGet(FileNumber, Value, RecordNumber, StringIsFixedLength)
        End Sub

        ''' <summary>
        ''' Reads data from an open disk file into a variable. The My feature gives you better productivity and performance in file I/O operations than FileGet. For more information, see My.Computer.FileSystem Object.
        ''' </summary>
        ''' <param name="FileNumber">
        ''' Required. Any valid file number.
        ''' </param>
        ''' <param name="Value">
        ''' Required. Valid variable name into which data is read.
        ''' </param>
        ''' <param name="RecordNumber">
        ''' Optional. Record number (Random mode files) or byte number (Binary mode files) at which reading starts.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub FileGet(FileNumber As Integer, ByRef Value As DateTime, Optional RecordNumber As Long = -1L)
            Microsoft.VisualBasic.FileSystem.FileGet(FileNumber, Value, RecordNumber)
        End Sub

        ''' <summary>
        ''' Writes data from a variable to a disk file.  The My feature gives you better productivity and performance in file I/O operations than FilePutObject. For more information, see My.Computer.FileSystem Object.
        ''' </summary>
        ''' <param name="FileNumber">
        ''' Required. Any valid file number.
        ''' </param>
        ''' <param name="Value">
        ''' Required. Valid variable name that contains data written to disk.
        ''' </param>
        ''' <param name="RecordNumber">
        ''' Optional. Record number (Random mode files) or byte number (Binary mode files) at which writing starts.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub FilePutObject(FileNumber As Integer, Value As Object, Optional RecordNumber As Long = -1L)
            Microsoft.VisualBasic.FileSystem.FilePutObject(FileNumber, Value, RecordNumber)
        End Sub

        ''' <summary>
        ''' Writes data from a variable to a disk file. The My feature gives you better productivity and performance in file I/O operations than FilePut. For more information, see My.Computer.FileSystem Object.
        ''' </summary>
        ''' <param name="FileNumber">
        ''' Required. Any valid file number.
        ''' </param>
        ''' <param name="Value">
        ''' Required. Valid variable name that contains data written to disk.
        ''' </param>
        ''' <param name="RecordNumber">
        ''' Optional. Record number (Random mode files) or byte number (Binary mode files) at which writing starts.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        <Obsolete("This member has been deprecated. Please use FilePutObject to write Object types, or coerce FileNumber and RecordNumber to Integer for writing non-Object types. http://go.microsoft.com/fwlink/?linkid=14202")>
        Public Sub FilePut(FileNumber As Object, Value As Object, Optional RecordNumber As Object = -1)
            Microsoft.VisualBasic.FileSystem.FilePut(FileNumber, Value, RecordNumber)
        End Sub

        ''' <summary>
        ''' Writes data from a variable to a disk file. The My feature gives you better productivity and performance in file I/O operations than FilePut. For more information, see My.Computer.FileSystem Object.
        ''' </summary>
        ''' <param name="FileNumber">
        ''' Required. Any valid file number.
        ''' </param>
        ''' <param name="Value">
        ''' Required. Valid variable name that contains data written to disk.
        ''' </param>
        ''' <param name="RecordNumber">
        ''' Optional. Record number (Random mode files) or byte number (Binary mode files) at which writing starts.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub FilePut(FileNumber As Integer, Value As ValueType, Optional RecordNumber As Long = -1L)
            Microsoft.VisualBasic.FileSystem.FilePut(FileNumber, Value, RecordNumber)
        End Sub

        ''' <summary>
        ''' Writes data from a variable to a disk file. The My feature gives you better productivity and performance in file I/O operations than FilePut. For more information, see My.Computer.FileSystem Object.
        ''' </summary>
        ''' <param name="FileNumber">
        ''' Required. Any valid file number.
        ''' </param>
        ''' <param name="Value">
        ''' Required. Valid variable name that contains data written to disk.
        ''' </param>
        ''' <param name="RecordNumber">
        ''' Optional. Record number (Random mode files) or byte number (Binary mode files) at which writing starts.
        ''' </param>
        ''' <param name="ArrayIsDynamic">
        ''' Optional. Applies only when writing an array. Specifies whether the array is to be treated as dynamic, and whether to write an array descriptor for the string that describes the length. 
        ''' </param>
        ''' <param name="StringIsFixedLength">
        ''' Optional. Applies only when writing a string. Specifies whether to write a two-byte string length descriptor for the string to the file. The default is False.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub FilePut(FileNumber As Integer, Value As Array, Optional RecordNumber As Long = -1L, Optional ArrayIsDynamic As Boolean = False, Optional StringIsFixedLength As Boolean = False)
            Microsoft.VisualBasic.FileSystem.FilePut(FileNumber, Value, RecordNumber, ArrayIsDynamic, StringIsFixedLength)
        End Sub

        ''' <summary>
        ''' Writes data from a variable to a disk file. The My feature gives you better productivity and performance in file I/O operations than FilePut. For more information, see My.Computer.FileSystem Object.
        ''' </summary>
        ''' <param name="FileNumber">
        ''' Required. Any valid file number.
        ''' </param>
        ''' <param name="Value">
        ''' Required. Valid variable name that contains data written to disk.
        ''' </param>
        ''' <param name="RecordNumber">
        ''' Optional. Record number (Random mode files) or byte number (Binary mode files) at which writing starts.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub FilePut(FileNumber As Integer, Value As Boolean, Optional RecordNumber As Long = -1L)
            Microsoft.VisualBasic.FileSystem.FilePut(FileNumber, Value, RecordNumber)
        End Sub

        ''' <summary>
        ''' Writes data from a variable to a disk file. The My feature gives you better productivity and performance in file I/O operations than FilePut. For more information, see My.Computer.FileSystem Object.
        ''' </summary>
        ''' <param name="FileNumber">
        ''' Required. Any valid file number.
        ''' </param>
        ''' <param name="Value">
        ''' Required. Valid variable name that contains data written to disk.
        ''' </param>
        ''' <param name="RecordNumber">
        ''' Optional. Record number (Random mode files) or byte number (Binary mode files) at which writing starts.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub FilePut(FileNumber As Integer, Value As Byte, Optional RecordNumber As Long = -1L)
            Microsoft.VisualBasic.FileSystem.FilePut(FileNumber, Value, RecordNumber)
        End Sub

        ''' <summary>
        ''' Writes data from a variable to a disk file. The My feature gives you better productivity and performance in file I/O operations than FilePut. For more information, see My.Computer.FileSystem Object.
        ''' </summary>
        ''' <param name="FileNumber">
        ''' Required. Any valid file number.
        ''' </param>
        ''' <param name="Value">
        ''' Required. Valid variable name that contains data written to disk.
        ''' </param>
        ''' <param name="RecordNumber">
        ''' Optional. Record number (Random mode files) or byte number (Binary mode files) at which writing starts.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub FilePut(FileNumber As Integer, Value As Short, Optional RecordNumber As Long = -1L)
            Microsoft.VisualBasic.FileSystem.FilePut(FileNumber, Value, RecordNumber)
        End Sub

        ''' <summary>
        ''' Writes data from a variable to a disk file. The My feature gives you better productivity and performance in file I/O operations than FilePut. For more information, see My.Computer.FileSystem Object.
        ''' </summary>
        ''' <param name="FileNumber">
        ''' Required. Any valid file number.
        ''' </param>
        ''' <param name="Value">
        ''' Required. Valid variable name that contains data written to disk.
        ''' </param>
        ''' <param name="RecordNumber">
        ''' Optional. Record number (Random mode files) or byte number (Binary mode files) at which writing starts.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub FilePut(FileNumber As Integer, Value As Integer, Optional RecordNumber As Long = -1L)
            Microsoft.VisualBasic.FileSystem.FilePut(FileNumber, Value, RecordNumber)
        End Sub

        ''' <summary>
        ''' Writes data from a variable to a disk file. The My feature gives you better productivity and performance in file I/O operations than FilePut. For more information, see My.Computer.FileSystem Object.
        ''' </summary>
        ''' <param name="FileNumber">
        ''' Required. Any valid file number.
        ''' </param>
        ''' <param name="Value">
        ''' Required. Valid variable name that contains data written to disk.
        ''' </param>
        ''' <param name="RecordNumber">
        ''' Optional. Record number (Random mode files) or byte number (Binary mode files) at which writing starts.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub FilePut(FileNumber As Integer, Value As Long, Optional RecordNumber As Long = -1L)
            Microsoft.VisualBasic.FileSystem.FilePut(FileNumber, Value, RecordNumber)
        End Sub

        ''' <summary>
        ''' Writes data from a variable to a disk file. The My feature gives you better productivity and performance in file I/O operations than FilePut. For more information, see My.Computer.FileSystem Object.
        ''' </summary>
        ''' <param name="FileNumber">
        ''' Required. Any valid file number.
        ''' </param>
        ''' <param name="Value">
        ''' Required. Valid variable name that contains data written to disk.
        ''' </param>
        ''' <param name="RecordNumber">
        ''' Optional. Record number (Random mode files) or byte number (Binary mode files) at which writing starts.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub FilePut(FileNumber As Integer, Value As Char, Optional RecordNumber As Long = -1L)
            Microsoft.VisualBasic.FileSystem.FilePut(FileNumber, Value, RecordNumber)
        End Sub

        ''' <summary>
        ''' Writes data from a variable to a disk file. The My feature gives you better productivity and performance in file I/O operations than FilePut. For more information, see My.Computer.FileSystem Object.
        ''' </summary>
        ''' <param name="FileNumber">
        ''' Required. Any valid file number.
        ''' </param>
        ''' <param name="Value">
        ''' Required. Valid variable name that contains data written to disk.
        ''' </param>
        ''' <param name="RecordNumber">
        ''' Optional. Record number (Random mode files) or byte number (Binary mode files) at which writing starts.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub FilePut(FileNumber As Integer, Value As Single, Optional RecordNumber As Long = -1L)
            Microsoft.VisualBasic.FileSystem.FilePut(FileNumber, Value, RecordNumber)
        End Sub

        ''' <summary>
        ''' Writes data from a variable to a disk file. The My feature gives you better productivity and performance in file I/O operations than FilePut. For more information, see My.Computer.FileSystem Object.
        ''' </summary>
        ''' <param name="FileNumber">
        ''' Required. Any valid file number.
        ''' </param>
        ''' <param name="Value">
        ''' Required. Valid variable name that contains data written to disk.
        ''' </param>
        ''' <param name="RecordNumber">
        ''' Optional. Record number (Random mode files) or byte number (Binary mode files) at which writing starts.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub FilePut(FileNumber As Integer, Value As Double, Optional RecordNumber As Long = -1L)
            Microsoft.VisualBasic.FileSystem.FilePut(FileNumber, Value, RecordNumber)
        End Sub

        ''' <summary>
        ''' Writes data from a variable to a disk file. The My feature gives you better productivity and performance in file I/O operations than FilePut. For more information, see My.Computer.FileSystem Object.
        ''' </summary>
        ''' <param name="FileNumber">
        ''' Required. Any valid file number.
        ''' </param>
        ''' <param name="Value">
        ''' Required. Valid variable name that contains data written to disk.
        ''' </param>
        ''' <param name="RecordNumber">
        ''' Optional. Record number (Random mode files) or byte number (Binary mode files) at which writing starts.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub FilePut(FileNumber As Integer, Value As Decimal, Optional RecordNumber As Long = -1L)
            Microsoft.VisualBasic.FileSystem.FilePut(FileNumber, Value, RecordNumber)
        End Sub

        ''' <summary>
        ''' Writes data from a variable to a disk file. The My feature gives you better productivity and performance in file I/O operations than FilePut. For more information, see My.Computer.FileSystem Object.
        ''' </summary>
        ''' <param name="FileNumber">
        ''' Required. Any valid file number.
        ''' </param>
        ''' <param name="Value">
        ''' Required. Valid variable name that contains data written to disk.
        ''' </param>
        ''' <param name="RecordNumber">
        ''' Optional. Record number (Random mode files) or byte number (Binary mode files) at which writing starts.
        ''' </param>
        ''' <param name="StringIsFixedLength">
        ''' Optional. Applies only when writing a string. Specifies whether to write a two-byte string length descriptor for the string to the file. The default is False.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub FilePut(FileNumber As Integer, Value As String, Optional RecordNumber As Long = -1L, Optional StringIsFixedLength As Boolean = False)
            Microsoft.VisualBasic.FileSystem.FilePut(FileNumber, Value, RecordNumber, StringIsFixedLength)
        End Sub

        ''' <summary>
        ''' Writes data from a variable to a disk file. The My feature gives you better productivity and performance in file I/O operations than FilePut. For more information, see My.Computer.FileSystem Object.
        ''' </summary>
        ''' <param name="FileNumber">
        ''' Required. Any valid file number.
        ''' </param>
        ''' <param name="Value">
        ''' Required. Valid variable name that contains data written to disk.
        ''' </param>
        ''' <param name="RecordNumber">
        ''' Optional. Record number (Random mode files) or byte number (Binary mode files) at which writing starts.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub FilePut(FileNumber As Integer, Value As DateTime, Optional RecordNumber As Long = -1L)
            Microsoft.VisualBasic.FileSystem.FilePut(FileNumber, Value, RecordNumber)
        End Sub

        ''' <summary>
        ''' Writes display-formatted data to a sequential file.
        ''' </summary>
        ''' <param name="FileNumber">
        ''' Required. Any valid file number.
        ''' </param>
        ''' <param name="Output">
        ''' Optional. Zero or more comma-delimited expressions to write to a file.
        ''' 
        ''' The <paramref name="Output" /> argument settings are: 
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub Print(FileNumber As Integer, <System.Runtime.InteropServices.OutAttribute()> ParamArray Output As Object())
            Microsoft.VisualBasic.FileSystem.Print(FileNumber, Output)
        End Sub

        ''' <summary>
        ''' Writes display-formatted data to a sequential file.
        ''' </summary>
        ''' <param name="FileNumber">
        ''' Required. Any valid file number.
        ''' </param>
        ''' <param name="Output">
        ''' Optional. Zero or more comma-delimited expressions to write to a file.
        ''' 
        ''' The <paramref name="Output" /> argument settings are: 
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub PrintLine(FileNumber As Integer, <System.Runtime.InteropServices.OutAttribute()> ParamArray Output As Object())
            Microsoft.VisualBasic.FileSystem.PrintLine(FileNumber, Output)
        End Sub

        ''' <summary>
        ''' Reads data from an open sequential file and assigns the data to variables.
        ''' </summary>
        ''' <param name="FileNumber">
        ''' Required. Any valid file number.
        ''' </param>
        ''' <param name="Value">
        ''' Required. Variable that is assigned the values read from the file—cannot be an array or object variable.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub Input(FileNumber As Integer, ByRef Value As Object)
            Microsoft.VisualBasic.FileSystem.Input(FileNumber, Value)
        End Sub

        ''' <summary>
        ''' Reads data from an open sequential file and assigns the data to variables.
        ''' </summary>
        ''' <param name="FileNumber">
        ''' Required. Any valid file number.
        ''' </param>
        ''' <param name="Value">
        ''' Required. Variable that is assigned the values read from the file—cannot be an array or object variable.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub Input(FileNumber As Integer, ByRef Value As Boolean)
            Microsoft.VisualBasic.FileSystem.Input(FileNumber, Value)
        End Sub

        ''' <summary>
        ''' Reads data from an open sequential file and assigns the data to variables.
        ''' </summary>
        ''' <param name="FileNumber">
        ''' Required. Any valid file number.
        ''' </param>
        ''' <param name="Value">
        ''' Required. Variable that is assigned the values read from the file—cannot be an array or object variable.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub Input(FileNumber As Integer, ByRef Value As Byte)
            Microsoft.VisualBasic.FileSystem.Input(FileNumber, Value)
        End Sub

        ''' <summary>
        ''' Reads data from an open sequential file and assigns the data to variables.
        ''' </summary>
        ''' <param name="FileNumber">
        ''' Required. Any valid file number.
        ''' </param>
        ''' <param name="Value">
        ''' Required. Variable that is assigned the values read from the file—cannot be an array or object variable.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub Input(FileNumber As Integer, ByRef Value As Short)
            Microsoft.VisualBasic.FileSystem.Input(FileNumber, Value)
        End Sub

        ''' <summary>
        ''' Reads data from an open sequential file and assigns the data to variables.
        ''' </summary>
        ''' <param name="FileNumber">
        ''' Required. Any valid file number.
        ''' </param>
        ''' <param name="Value">
        ''' Required. Variable that is assigned the values read from the file—cannot be an array or object variable.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub Input(FileNumber As Integer, ByRef Value As Integer)
            Microsoft.VisualBasic.FileSystem.Input(FileNumber, Value)
        End Sub

        ''' <summary>
        ''' Reads data from an open sequential file and assigns the data to variables.
        ''' </summary>
        ''' <param name="FileNumber">
        ''' Required. Any valid file number.
        ''' </param>
        ''' <param name="Value">
        ''' Required. Variable that is assigned the values read from the file—cannot be an array or object variable.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub Input(FileNumber As Integer, ByRef Value As Long)
            Microsoft.VisualBasic.FileSystem.Input(FileNumber, Value)
        End Sub

        ''' <summary>
        ''' Reads data from an open sequential file and assigns the data to variables.
        ''' </summary>
        ''' <param name="FileNumber">
        ''' Required. Any valid file number.
        ''' </param>
        ''' <param name="Value">
        ''' Required. Variable that is assigned the values read from the file—cannot be an array or object variable.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub Input(FileNumber As Integer, ByRef Value As Char)
            Microsoft.VisualBasic.FileSystem.Input(FileNumber, Value)
        End Sub

        ''' <summary>
        ''' Reads data from an open sequential file and assigns the data to variables.
        ''' </summary>
        ''' <param name="FileNumber">
        ''' Required. Any valid file number.
        ''' </param>
        ''' <param name="Value">
        ''' Required. Variable that is assigned the values read from the file—cannot be an array or object variable.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub Input(FileNumber As Integer, ByRef Value As Single)
            Microsoft.VisualBasic.FileSystem.Input(FileNumber, Value)
        End Sub

        ''' <summary>
        ''' Reads data from an open sequential file and assigns the data to variables.
        ''' </summary>
        ''' <param name="FileNumber">
        ''' Required. Any valid file number.
        ''' </param>
        ''' <param name="Value">
        ''' Required. Variable that is assigned the values read from the file—cannot be an array or object variable.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub Input(FileNumber As Integer, ByRef Value As Double)
            Microsoft.VisualBasic.FileSystem.Input(FileNumber, Value)
        End Sub

        ''' <summary>
        ''' Reads data from an open sequential file and assigns the data to variables.
        ''' </summary>
        ''' <param name="FileNumber">
        ''' Required. Any valid file number.
        ''' </param>
        ''' <param name="Value">
        ''' Required. Variable that is assigned the values read from the file—cannot be an array or object variable.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub Input(FileNumber As Integer, ByRef Value As Decimal)
            Microsoft.VisualBasic.FileSystem.Input(FileNumber, Value)
        End Sub

        ''' <summary>
        ''' Reads data from an open sequential file and assigns the data to variables.
        ''' </summary>
        ''' <param name="FileNumber">
        ''' Required. Any valid file number.
        ''' </param>
        ''' <param name="Value">
        ''' Required. Variable that is assigned the values read from the file—cannot be an array or object variable.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub Input(FileNumber As Integer, ByRef Value As String)
            Microsoft.VisualBasic.FileSystem.Input(FileNumber, Value)
        End Sub

        ''' <summary>
        ''' Reads data from an open sequential file and assigns the data to variables.
        ''' </summary>
        ''' <param name="FileNumber">
        ''' Required. Any valid file number.
        ''' </param>
        ''' <param name="Value">
        ''' Required. Variable that is assigned the values read from the file—cannot be an array or object variable.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub Input(FileNumber As Integer, ByRef Value As DateTime)
            Microsoft.VisualBasic.FileSystem.Input(FileNumber, Value)
        End Sub

        ''' <summary>
        ''' Writes data to a sequential file. Data written with Write is usually read from a file by using Input.
        ''' </summary>
        ''' <param name="FileNumber">
        ''' Required. An Integer expression that contains any valid file number.
        ''' </param>
        ''' <param name="Output">
        ''' Optional. One or more comma-delimited expressions to write to a file.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub Write(FileNumber As Integer, <System.Runtime.InteropServices.OutAttribute()> ParamArray Output As Object())
            Microsoft.VisualBasic.FileSystem.Write(FileNumber, Output)
        End Sub

        ''' <summary>
        ''' Writes data to a sequential file. Data written with Write is usually read from a file by using Input.
        ''' </summary>
        ''' <param name="FileNumber">
        ''' Required. An Integer expression that contains any valid file number.
        ''' </param>
        ''' <param name="Output">
        ''' Optional. One or more comma-delimited expressions to write to a file.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub WriteLine(FileNumber As Integer, <System.Runtime.InteropServices.OutAttribute()> ParamArray Output As Object())
            Microsoft.VisualBasic.FileSystem.WriteLine(FileNumber, Output)
        End Sub

        ''' <summary>
        ''' Returns String value that contains characters from a file opened in Input or Binary mode. The My feature gives you better productivity and performance in file I/O operations than InputString. For more information, see My.Computer.FileSystem Object.
        ''' </summary>
        ''' <returns>
        ''' Returns String value that contains characters from a file opened in Input or Binary mode. The My feature gives you better productivity and performance in file I/O operations than InputString. For more information, see My.Computer.FileSystem Object.
        ''' </returns>
        ''' <param name="FileNumber">
        ''' Required. Any valid file number.
        ''' </param>
        ''' <param name="CharCount">
        ''' Required. Any valid numeric expression specifying the number of characters to read.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Function InputString(FileNumber As Integer, CharCount As Integer) As String
            Return Microsoft.VisualBasic.FileSystem.InputString(FileNumber, CharCount)
        End Function

        ''' <summary>
        ''' Reads a single line from an open sequential file and assigns it to a String variable.
        ''' </summary>
        ''' <returns>
        ''' Reads a single line from an open sequential file and assigns it to a String variable.
        ''' </returns>
        ''' <param name="FileNumber">
        ''' Required. Any valid file number.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Function LineInput(FileNumber As Integer) As String
            Return Microsoft.VisualBasic.FileSystem.LineInput(FileNumber)
        End Function

        ''' <summary>
        ''' Controls access by other processes to all or part of a file opened by using the Open function. The My feature gives you better productivity and performance in file I/O operations than Lock and Unlock. For more information, see My.Computer.FileSystem Object.
        ''' </summary>
        ''' <param name="FileNumber">
        ''' Required. Any valid file number.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub Lock(FileNumber As Integer)
            Microsoft.VisualBasic.FileSystem.Lock(FileNumber)
        End Sub

        ''' <summary>
        ''' Controls access by other processes to all or part of a file opened by using the Open function. The My feature gives you better productivity and performance in file I/O operations than Lock and Unlock. For more information, see My.Computer.FileSystem Object.
        ''' </summary>
        ''' <param name="FileNumber">
        ''' Required. Any valid file number.
        ''' </param>
        ''' <param name="Record">
        ''' Optional. Number of the only record or byte to lock or unlock
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub Lock(FileNumber As Integer, Record As Long)
            Microsoft.VisualBasic.FileSystem.Lock(FileNumber, Record)
        End Sub

        ''' <summary>
        ''' Controls access by other processes to all or part of a file opened by using the Open function. The My feature gives you better productivity and performance in file I/O operations than Lock and Unlock. For more information, see My.Computer.FileSystem Object.
        ''' </summary>
        ''' <param name="FileNumber">
        ''' Required. Any valid file number.
        ''' </param>
        ''' <param name="FromRecord">
        ''' Optional. Number of the first record or byte to lock or unlock.
        ''' </param>
        ''' <param name="ToRecord">
        ''' Optional. Number of the last record or byte to lock or unlock.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub Lock(FileNumber As Integer, FromRecord As Long, ToRecord As Long)
            Microsoft.VisualBasic.FileSystem.Lock(FileNumber, FromRecord, ToRecord)
        End Sub

        ''' <summary>
        ''' Controls access by other processes to all or part of a file opened by using the Open function. The My feature gives you better productivity and performance in file I/O operations than Lock and Unlock. For more information, see My.Computer.FileSystem Object.
        ''' </summary>
        ''' <param name="FileNumber">
        ''' Required. Any valid file number.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub Unlock(FileNumber As Integer)
            Microsoft.VisualBasic.FileSystem.Unlock(FileNumber)
        End Sub

        ''' <summary>
        ''' Controls access by other processes to all or part of a file opened by using the Open function. The My feature gives you better productivity and performance in file I/O operations than Lock and Unlock. For more information, see My.Computer.FileSystem Object.
        ''' </summary>
        ''' <param name="FileNumber">
        ''' Required. Any valid file number.
        ''' </param>
        ''' <param name="Record">
        ''' Optional. Number of the only record or byte to lock or unlock
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub Unlock(FileNumber As Integer, Record As Long)
            Microsoft.VisualBasic.FileSystem.Unlock(FileNumber, Record)
        End Sub

        ''' <summary>
        ''' Controls access by other processes to all or part of a file opened by using the Open function. The My feature gives you better productivity and performance in file I/O operations than Lock and Unlock. For more information, see My.Computer.FileSystem Object.
        ''' </summary>
        ''' <param name="FileNumber">
        ''' Required. Any valid file number.
        ''' </param>
        ''' <param name="FromRecord">
        ''' Optional. Number of the first record or byte to lock or unlock.
        ''' </param>
        ''' <param name="ToRecord">
        ''' Optional. Number of the last record or byte to lock or unlock.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub Unlock(FileNumber As Integer, FromRecord As Long, ToRecord As Long)
            Microsoft.VisualBasic.FileSystem.Unlock(FileNumber, FromRecord, ToRecord)
        End Sub

        ''' <summary>
        ''' Assigns an output line width to a file opened by using the FileOpen function.
        ''' </summary>
        ''' <param name="FileNumber">
        ''' Required. Any valid file number.
        ''' </param>
        ''' <param name="RecordWidth">
        ''' Required. Numeric expression in the range 0–255, inclusive, which indicates how many characters appear on a line before a new line is started. If <paramref name="RecordWidth" /> equals 0, there is no limit to the length of a line. The default value for <paramref name="RecordWidth" /> is 0.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub FileWidth(FileNumber As Integer, RecordWidth As Integer)
            Microsoft.VisualBasic.FileSystem.FileWidth(FileNumber, RecordWidth)
        End Sub

        ''' <summary>
        ''' Returns an Integer value that represents the next file number available for use by the FileOpen function.
        ''' </summary>
        ''' <returns>
        ''' Returns an Integer value that represents the next file number available for use by the FileOpen function.
        ''' </returns>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Function FreeFile() As Integer
            Return Microsoft.VisualBasic.FileSystem.FreeFile()
        End Function

        ''' <summary>
        ''' Returns a Long specifying the current read/write position in a file opened by using the FileOpen function, or sets the position for the next read/write operation in a file opened by using the FileOpen function. The My feature gives you better productivity and performance in file I/O operations than Seek. For more information, see My.Computer.FileSystem Object.
        ''' </summary>
        ''' <param name="FileNumber">
        ''' Required. An Integer that contains a valid file number.
        ''' </param>
        ''' <param name="Position">
        ''' Required. Number in the range 1–2,147,483,647, inclusive, that indicates where the next read/write operation should occur.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub Seek(FileNumber As Integer, Position As Long)
            Microsoft.VisualBasic.FileSystem.Seek(FileNumber, Position)
        End Sub

        ''' <summary>
        ''' Returns a Long specifying the current read/write position in a file opened by using the FileOpen function, or sets the position for the next read/write operation in a file opened by using the FileOpen function. The My feature gives you better productivity and performance in file I/O operations than Seek. For more information, see My.Computer.FileSystem Object.
        ''' </summary>
        ''' <returns>
        ''' Returns a Long specifying the current read/write position in a file opened by using the FileOpen function, or sets the position for the next read/write operation in a file opened by using the FileOpen function. The My feature gives you better productivity and performance in file I/O operations than Seek. For more information, see My.Computer.FileSystem Object.
        ''' </returns>
        ''' <param name="FileNumber">
        ''' Required. An Integer that contains a valid file number.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Function Seek(FileNumber As Integer) As Long
            Return Microsoft.VisualBasic.FileSystem.Seek(FileNumber)
        End Function

        ''' <summary>
        ''' Returns a Boolean value True when the end of a file opened for Random or sequential Input has been reached.
        ''' </summary>
        ''' <returns>
        ''' Returns a Boolean value True when the end of a file opened for Random or sequential Input has been reached.
        ''' </returns>
        ''' <param name="FileNumber">
        ''' Required. An Integer that contains any valid file number.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Function EOF(FileNumber As Integer) As Boolean
            Return Microsoft.VisualBasic.FileSystem.EOF(FileNumber)
        End Function

        ''' <summary>
        ''' Returns a Long value that specifies the current read/write position in an open file.
        ''' </summary>
        ''' <returns>
        ''' Returns a Long value that specifies the current read/write position in an open file.
        ''' </returns>
        ''' <param name="FileNumber">
        ''' Required. Any valid Integer file number.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Function Loc(FileNumber As Integer) As Long
            Return Microsoft.VisualBasic.FileSystem.Loc(FileNumber)
        End Function

        ''' <summary>
        ''' Returns a Long representing the size, in bytes, of a file opened by using the FileOpen function. The My feature gives you better productivity and performance in file I/O operations than LOF. For more information, see My.Computer.FileSystem Object.
        ''' </summary>
        ''' <returns>
        ''' Returns a Long representing the size, in bytes, of a file opened by using the FileOpen function. The My feature gives you better productivity and performance in file I/O operations than LOF. For more information, see My.Computer.FileSystem Object.
        ''' </returns>
        ''' <param name="FileNumber">
        ''' Required. An Integer that contains a valid file number.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Function LOF(FileNumber As Integer) As Long
            Return Microsoft.VisualBasic.FileSystem.LOF(FileNumber)
        End Function

        ''' <summary>
        ''' Used with the Print or PrintLine functions to position output.
        ''' </summary>
        ''' <returns>
        ''' Used with the Print or PrintLine functions to position output.
        ''' </returns>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Function TAB() As TabInfo
            Return Microsoft.VisualBasic.FileSystem.TAB()
        End Function

        ''' <summary>
        ''' Used with the Print or PrintLine functions to position output.
        ''' </summary>
        ''' <returns>
        ''' Used with the Print or PrintLine functions to position output.
        ''' </returns>
        ''' <param name="Column">
        ''' Optional. The column number moved to before displaying or printing the next expression in a list. If omitted, TAB moves the insertion point to the start of the next print zone. 
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Function TAB(Column As Short) As TabInfo
            Return Microsoft.VisualBasic.FileSystem.TAB(Column)
        End Function

        ''' <summary>
        ''' Used with the Print or PrintLine function to position output.
        ''' </summary>
        ''' <returns>
        ''' Used with the Print or PrintLine function to position output.
        ''' </returns>
        ''' <param name="Count">
        ''' Required. The number of spaces to insert before displaying or printing the next expression in a list.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Function SPC(Count As Short) As SpcInfo
            Return Microsoft.VisualBasic.FileSystem.SPC(Count)
        End Function

        ''' <summary>
        ''' Returns an enumeration representing the file mode for files opened using the FileOpen function. The My.Computer.FileSystem Object gives you better productivity and performance in file I/O operations than the FileAttr function. See My.Computer.FileSystem.GetFileInfo Method for more information.
        ''' </summary>
        ''' <returns>
        ''' Value 1, Mode OpenMode.Input. Value 2, Mode OpenMode.Output. Value 4, Mode OpenMode.Random. Value 8, Mode OpenMode.Append. Value 32, Mode OpenMode.Binary. </returns>
        ''' <param name="FileNumber">
        ''' Required. Integer. Any valid file number.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Function FileAttr(FileNumber As Integer) As OpenMode
            Return Microsoft.VisualBasic.FileSystem.FileAttr(FileNumber)
        End Function

        ''' <summary>
        ''' Closes all disk files opened by using the FileOpen function. The My feature gives you better productivity and performance in file I/O operations than Reset. For more information, see My.Computer.FileSystem Object.
        ''' </summary>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" PathDiscovery="*AllFiles*" />
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub Reset()
            Microsoft.VisualBasic.FileSystem.Reset()
        End Sub

        ''' <summary>
        ''' Renames a disk file or directory. The My feature gives you better productivity and performance in file I/O operations than Rename. For more information, see My.Computer.FileSystem Object.
        ''' </summary>
        ''' <param name="OldPath">
        ''' Required. String expression that specifies the existing file name and location. <paramref name="OldPath" /> may include the directory, and drive, of the file.
        ''' </param>
        ''' <param name="NewPath">
        ''' Required. String expression that specifies the new file name and location. <paramref name="NewPath" /> may include directory and drive of the destination location. The file name specified by <paramref name="NewPath" /> cannot already exist.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub Rename(OldPath As String, NewPath As String)
            Microsoft.VisualBasic.FileSystem.Rename(OldPath, NewPath)
        End Sub
    End Module

    Public Module Financial
        ''' <summary>
        ''' Returns a Double specifying the depreciation of an asset for a specific time period using the double-declining balance method or some other method you specify.
        ''' </summary>
        ''' <returns>
        ''' Returns a Double specifying the depreciation of an asset for a specific time period using the double-declining balance method or some other method you specify.
        ''' </returns>
        ''' <param name="Cost">
        ''' Required. Double specifying initial cost of the asset.
        ''' </param>
        ''' <param name="Salvage">
        ''' Required. Double specifying value of the asset at the end of its useful life.
        ''' </param>
        ''' <param name="Life">
        ''' Required. Double specifying length of useful life of the asset.
        ''' </param>
        ''' <param name="Period">
        ''' Required. Double specifying period for which asset depreciation is calculated.
        ''' </param>
        ''' <param name="Factor">
        ''' Optional. Double specifying rate at which the balance declines. If omitted, 2 (double-declining method) is assumed.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function DDB(Cost As Double, Salvage As Double, Life As Double, Period As Double, Optional Factor As Double = 2.0) As Double
            Return Microsoft.VisualBasic.Financial.DDB(Cost, Salvage, Life, Period, Factor)
        End Function

        ''' <summary>
        ''' Returns a Double specifying the future value of an annuity based on periodic, fixed payments and a fixed interest rate.
        ''' </summary>
        ''' <returns>
        ''' Returns a Double specifying the future value of an annuity based on periodic, fixed payments and a fixed interest rate.
        ''' </returns>
        ''' <param name="Rate">
        ''' Required. Double specifying interest rate per period. For example, if you get a car loan at an annual percentage rate (APR) of 10 percent and make monthly payments, the rate per period is 0.1/12, or 0.0083.
        ''' </param>
        ''' <param name="NPer">
        ''' Required. Double specifying total number of payment periods in the annuity. For example, if you make monthly payments on a four-year car loan, your loan has a total of 4 x 12 (or 48) payment periods.
        ''' </param>
        ''' <param name="Pmt">
        ''' Required. Double specifying payment to be made each period. Payments usually contain principal and interest that doesn't change over the life of the annuity.
        ''' </param>
        ''' <param name="PV">
        ''' Optional. Double specifying present value (or lump sum) of a series of future payments. For example, when you borrow money to buy a car, the loan amount is the present value to the lender of the monthly car payments you will make. If omitted, 0 is assumed.
        ''' </param>
        ''' <param name="Due">
        ''' Optional. Object of type DueDate Enumeration that specifies when payments are due. This argument must be either DueDate.EndOfPeriod if payments are due at the end of the payment period, or DueDate.BegOfPeriod if payments are due at the beginning of the period. If omitted, DueDate.EndOfPeriod is assumed.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function FV(Rate As Double, NPer As Double, Pmt As Double, Optional PV As Double = 0.0, Optional Due As DueDate = DueDate.EndOfPeriod) As Double
            Return Microsoft.VisualBasic.Financial.FV(Rate, NPer, Pmt, PV, Due)
        End Function

        ''' <summary>
        ''' Returns a Double specifying the interest payment for a given period of an annuity based on periodic, fixed payments and a fixed interest rate.
        ''' </summary>
        ''' <returns>
        ''' Returns a Double specifying the interest payment for a given period of an annuity based on periodic, fixed payments and a fixed interest rate.
        ''' </returns>
        ''' <param name="Rate">
        ''' Required. Double specifying interest rate per period. For example, if you get a car loan at an annual percentage rate (APR) of 10 percent and make monthly payments, the rate per period is 0.1/12, or 0.0083.
        ''' </param>
        ''' <param name="Per">
        ''' Required. Double specifying payment period in the range 1 through <paramref name="NPer" />.
        ''' </param>
        ''' <param name="NPer">
        ''' Required. Double specifying total number of payment periods in the annuity. For example, if you make monthly payments on a four-year car loan, your loan has a total of 4 x 12 (or 48) payment periods.
        ''' </param>
        ''' <param name="PV">
        ''' Required. Double specifying present value, or value today, of a series of future payments or receipts. For example, when you borrow money to buy a car, the loan amount is the present value to the lender of the monthly car payments you will make.
        ''' </param>
        ''' <param name="FV">
        ''' Optional. Double specifying future value or cash balance you want after you've made the final payment. For example, the future value of a loan is $0 because that's its value after the final payment. However, if you want to save $50,000 over 18 years for your child's education, then $50,000 is the future value. If omitted, 0 is assumed.
        ''' </param>
        ''' <param name="Due">
        ''' Optional. Object of type DueDate Enumeration that specifies when payments are due. This argument must be either DueDate.EndOfPeriod if payments are due at the end of the payment period, or DueDate.BegOfPeriod if payments are due at the beginning of the period. If omitted, DueDate.EndOfPeriod is assumed.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function IPmt(Rate As Double, Per As Double, NPer As Double, PV As Double, Optional FV As Double = 0.0, Optional Due As DueDate = DueDate.EndOfPeriod) As Double
            Return Microsoft.VisualBasic.Financial.IPmt(Rate, Per, NPer, PV, FV, Due)
        End Function

        ''' <summary>
        ''' Returns a Double specifying the internal rate of return for a series of periodic cash flows (payments and receipts).
        ''' </summary>
        ''' <returns>
        ''' Returns a Double specifying the internal rate of return for a series of periodic cash flows (payments and receipts).
        ''' </returns>
        ''' <param name="ValueArray">
        ''' Required. Array of Double specifying cash flow values. The array must contain at least one negative value (a payment) and one positive value (a receipt).
        ''' </param>
        ''' <param name="Guess">
        ''' Optional. Object specifying value you estimate will be returned by IRR. If omitted, <paramref name="Guess" /> is 0.1 (10 percent).
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function IRR(ByRef ValueArray As Double(), Optional Guess As Double = 0.1) As Double
            Return Microsoft.VisualBasic.Financial.IRR(ValueArray, Guess)
        End Function

        ''' <summary>
        ''' Returns a Double specifying the modified internal rate of return for a series of periodic cash flows (payments and receipts).
        ''' </summary>
        ''' <returns>
        ''' Returns a Double specifying the modified internal rate of return for a series of periodic cash flows (payments and receipts).
        ''' </returns>
        ''' <param name="ValueArray">
        ''' Required. Array of Double specifying cash-flow values. The array must contain at least one negative value (a payment) and one positive value (a receipt).
        ''' </param>
        ''' <param name="FinanceRate">
        ''' Required. Double specifying interest rate paid as the cost of financing.
        ''' </param>
        ''' <param name="ReinvestRate">
        ''' Required. Double specifying interest rate received on gains from cash reinvestment.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function MIRR(ByRef ValueArray As Double(), FinanceRate As Double, ReinvestRate As Double) As Double
            Return Microsoft.VisualBasic.Financial.MIRR(ValueArray, FinanceRate, ReinvestRate)
        End Function

        ''' <summary>
        ''' Returns a Double specifying the number of periods for an annuity based on periodic fixed payments and a fixed interest rate.
        ''' </summary>
        ''' <returns>
        ''' Returns a Double specifying the number of periods for an annuity based on periodic fixed payments and a fixed interest rate.
        ''' </returns>
        ''' <param name="Rate">
        ''' Required. Double specifying interest rate per period. For example, if you get a car loan at an annual percentage rate (APR) of 10 percent and make monthly payments, the rate per period is 0.1/12, or 0.0083.
        ''' </param>
        ''' <param name="Pmt">
        ''' Required. Double specifying payment to be made each period. Payments usually contain principal and interest that does not change over the life of the annuity.
        ''' </param>
        ''' <param name="PV">
        ''' Required. Double specifying present value, or value today, of a series of future payments or receipts. For example, when you borrow money to buy a car, the loan amount is the present value to the lender of the monthly car payments you will make.
        ''' </param>
        ''' <param name="FV">
        ''' Optional. Double specifying future value or cash balance you want after you have made the final payment. For example, the future value of a loan is $0 because that is its value after the final payment. However, if you want to save $50,000 over 18 years for your child's education, then $50,000 is the future value. If omitted, 0 is assumed.
        ''' </param>
        ''' <param name="Due">
        ''' Optional. Object of type DueDate Enumeration that specifies when payments are due. This argument must be either DueDate.EndOfPeriod if payments are due at the end of the payment period, or DueDate.BegOfPeriod if payments are due at the beginning of the period. If omitted, DueDate.EndOfPeriod is assumed.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function NPer(Rate As Double, Pmt As Double, PV As Double, Optional FV As Double = 0.0, Optional Due As DueDate = DueDate.EndOfPeriod) As Double
            Return Microsoft.VisualBasic.Financial.NPer(Rate, Pmt, PV, FV, Due)
        End Function

        ''' <summary>
        ''' Returns a Double specifying the net present value of an investment based on a series of periodic cash flows (payments and receipts) and a discount rate.
        ''' </summary>
        ''' <returns>
        ''' Returns a Double specifying the net present value of an investment based on a series of periodic cash flows (payments and receipts) and a discount rate.
        ''' </returns>
        ''' <param name="Rate">
        ''' Required. Double specifying discount rate over the length of the period, expressed as a decimal.
        ''' </param>
        ''' <param name="ValueArray">
        ''' Required. Array of Double specifying cash flow values. The array must contain at least one negative value (a payment) and one positive value (a receipt).
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function NPV(Rate As Double, ByRef ValueArray As Double()) As Double
            Return Microsoft.VisualBasic.Financial.NPV(Rate, ValueArray)
        End Function

        ''' <summary>
        ''' Returns a Double specifying the payment for an annuity based on periodic, fixed payments and a fixed interest rate.
        ''' </summary>
        ''' <returns>
        ''' Returns a Double specifying the payment for an annuity based on periodic, fixed payments and a fixed interest rate.
        ''' </returns>
        ''' <param name="Rate">
        ''' Required. Double specifies the interest rate per period. For example, if you get a car loan at an annual percentage rate (APR) of 10 percent and make monthly payments, the rate per period is 0.1/12, or 0.0083.
        ''' </param>
        ''' <param name="NPer">
        ''' Required. Double specifies the total number of payment periods in the annuity. For example, if you make monthly payments on a four-year car loan, your loan has a total of 4 × 12 (or 48) payment periods.
        ''' </param>
        ''' <param name="PV">
        ''' Required. Double specifies the present value (or lump sum) that a series of payments to be paid in the future is worth now. For example, when you borrow money to buy a car, the loan amount is the present value to the lender of the monthly car payments you will make.
        ''' </param>
        ''' <param name="FV">
        ''' Optional. Double specifying future value or cash balance you want after you have made the final payment. For example, the future value of a loan is $0 because that is its value after the final payment. However, if you want to save $50,000 during 18 years for your child's education, then $50,000 is the future value. If omitted, 0 is assumed.
        ''' </param>
        ''' <param name="Due">
        ''' Optional. Object of type DueDate Enumeration that specifies when payments are due. This argument must be either DueDate.EndOfPeriod if payments are due at the end of the payment period, or DueDate.BegOfPeriod if payments are due at the beginning of the period. If omitted, DueDate.EndOfPeriod is assumed.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function Pmt(Rate As Double, NPer As Double, PV As Double, Optional FV As Double = 0.0, Optional Due As DueDate = DueDate.EndOfPeriod) As Double
            Return Microsoft.VisualBasic.Financial.Pmt(Rate, NPer, PV, FV, Due)
        End Function

        ''' <summary>
        ''' Returns a Double specifying the principal payment for a given period of an annuity based on periodic fixed payments and a fixed interest rate.
        ''' </summary>
        ''' <returns>
        ''' Returns a Double specifying the principal payment for a given period of an annuity based on periodic fixed payments and a fixed interest rate.
        ''' </returns>
        ''' <param name="Rate">
        ''' Required. Double specifies the interest rate per period. For example, if you get a car loan at an annual percentage rate (APR) of 10 percent and make monthly payments, the rate per period is 0.1/12, or 0.0083.
        ''' </param>
        ''' <param name="Per">
        ''' Required. Double specifies the payment period in the range 1 through <paramref name="NPer" />.
        ''' </param>
        ''' <param name="NPer">
        ''' Required. Double specifies the total number of payment periods in the annuity. For example, if you make monthly payments on a four-year car loan, your loan has a total of 4 x 12 (or 48) payment periods.
        ''' </param>
        ''' <param name="PV">
        ''' Required. Double specifies the current value of a series of future payments or receipts. For example, when you borrow money to buy a car, the loan amount is the present value to the lender of the monthly car payments you will make.
        ''' </param>
        ''' <param name="FV">
        ''' Optional. Double specifying future value or cash balance you want after you have made the final payment. For example, the future value of a loan is $0 because that is its value after the final payment. However, if you want to save $50,000 over 18 years for your child's education, then $50,000 is the future value. If omitted, 0 is assumed.
        ''' </param>
        ''' <param name="Due">
        ''' Optional. Object of type DueDate Enumeration that specifies when payments are due. This argument must be either DueDate.EndOfPeriod if payments are due at the end of the payment period, or DueDate.BegOfPeriod if payments are due at the beginning of the period. If omitted, DueDate.EndOfPeriod is assumed.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function PPmt(Rate As Double, Per As Double, NPer As Double, PV As Double, Optional FV As Double = 0.0, Optional Due As DueDate = DueDate.EndOfPeriod) As Double
            Return Microsoft.VisualBasic.Financial.PPmt(Rate, Per, NPer, PV, FV, Due)
        End Function

        ''' <summary>
        ''' Returns a Double specifying the present value of an annuity based on periodic, fixed payments to be paid in the future and a fixed interest rate.
        ''' </summary>
        ''' <returns>
        ''' Returns a Double specifying the present value of an annuity based on periodic, fixed payments to be paid in the future and a fixed interest rate.
        ''' </returns>
        ''' <param name="Rate">
        ''' Required. Double specifies the interest rate per period. For example, if you get a car loan at an annual percentage rate (APR) of 10 percent and make monthly payments, the rate per period is 0.1/12, or 0.0083.
        ''' </param>
        ''' <param name="NPer">
        ''' Required. Double specifies the total number of payment periods in the annuity. For example, if you make monthly payments on a four-year car loan, your loan has 4 x 12 (or 48) payment periods.
        ''' </param>
        ''' <param name="Pmt">
        ''' Required. Double specifies the payment to be made each period. Payments usually contain principal and interest that does not change during the life of the annuity.
        ''' </param>
        ''' <param name="FV">
        ''' Optional. Double specifies the future value or cash balance you want after you make the final payment. For example, the future value of a loan is $0 because that is its value after the final payment. However, if you want to save $50,000 over 18 years for your child's education, then $50,000 is the future value. If omitted, 0 is assumed.
        ''' </param>
        ''' <param name="Due">
        ''' Optional. Object of type DueDate Enumeration that specifies when payments are due. This argument must be either DueDate.EndOfPeriod if payments are due at the end of the payment period, or DueDate.BegOfPeriod if payments are due at the beginning of the period. If omitted, DueDate.EndOfPeriod is assumed.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function PV(Rate As Double, NPer As Double, Pmt As Double, Optional FV As Double = 0.0, Optional Due As DueDate = DueDate.EndOfPeriod) As Double
            Return Microsoft.VisualBasic.Financial.PV(Rate, NPer, Pmt, FV, Due)
        End Function

        ''' <summary>
        ''' Returns a Double specifying the interest rate per period for an annuity.
        ''' </summary>
        ''' <returns>
        ''' Returns a Double specifying the interest rate per period for an annuity.
        ''' </returns>
        ''' <param name="NPer">
        ''' Required. Double specifies the total number of payment periods in the annuity. For example, if you make monthly payments on a four-year car loan, your loan has a total of 4 * 12 (or 48) payment periods.
        ''' </param>
        ''' <param name="Pmt">
        ''' Required. Double specifies the payment to be made each period. Payments usually contain principal and interest that doesn't change over the life of the annuity.
        ''' </param>
        ''' <param name="PV">
        ''' Required. Double specifies the present value, or value today, of a series of future payments or receipts. For example, when you borrow money to buy a car, the loan amount is the present value to the lender of the monthly car payments you will make.
        ''' </param>
        ''' <param name="FV">
        ''' Optional. Double specifies the future value or cash balance you want after you make the final payment. For example, the future value of a loan is $0 because that is its value after the final payment. However, if you want to save $50,000 over 18 years for your child's education, then $50,000 is the future value. If omitted, 0 is assumed.
        ''' </param>
        ''' <param name="Due">
        ''' Optional. Object of type DueDate Enumeration that specifies when payments are due. This argument must be either DueDate.EndOfPeriod if payments are due at the end of the payment period, or DueDate.BegOfPeriod if payments are due at the beginning of the period. If omitted, DueDate.EndOfPeriod is assumed.
        ''' </param>
        ''' <param name="Guess">
        ''' Optional. Double specifying value you estimate is returned by Rate. If omitted, <paramref name="Guess" /> is 0.1 (10 percent).
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function Rate(NPer As Double, Pmt As Double, PV As Double, Optional FV As Double = 0.0, Optional Due As DueDate = DueDate.EndOfPeriod, Optional Guess As Double = 0.1) As Double
            Return Microsoft.VisualBasic.Financial.Rate(NPer, Pmt, PV, FV, Due)
        End Function

        ''' <summary>
        ''' Returns a Double specifying the straight-line depreciation of an asset for a single period.
        ''' </summary>
        ''' <returns>
        ''' Returns a Double specifying the straight-line depreciation of an asset for a single period.
        ''' </returns>
        ''' <param name="Cost">
        ''' Required. Double specifying initial cost of the asset.
        ''' </param>
        ''' <param name="Salvage">
        ''' Required. Double specifying value of the asset at the end of its useful life.
        ''' </param>
        ''' <param name="Life">
        ''' Required. Double specifying length of the useful life of the asset.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function SLN(Cost As Double, Salvage As Double, Life As Double) As Double
            Return Microsoft.VisualBasic.Financial.SLN(Cost, Salvage, Life)
        End Function

        ''' <summary>
        ''' Returns a Double specifying the sum-of-years digits depreciation of an asset for a specified period.
        ''' </summary>
        ''' <returns>
        ''' Returns a Double specifying the sum-of-years digits depreciation of an asset for a specified period.
        ''' </returns>
        ''' <param name="Cost">
        ''' Required. Double specifying the initial cost of the asset.
        ''' </param>
        ''' <param name="Salvage">
        ''' Required. Double specifying the value of the asset at the end of its useful life.
        ''' </param>
        ''' <param name="Life">
        ''' Required. Double specifying the length of the useful life of the asset.
        ''' </param>
        ''' <param name="Period">
        ''' Required. Double specifying the period for which asset depreciation is calculated.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function SYD(Cost As Double, Salvage As Double, Life As Double, Period As Double) As Double
            Return Microsoft.VisualBasic.Financial.SYD(Cost, Salvage, Life, Period)
        End Function
    End Module

    Public Module Information
        ''' <summary>
        ''' Contains information about run-time errors.
        ''' </summary>
        ''' <returns>
        ''' Contains information about run-time errors.
        ''' </returns>
        ''' <filterpriority>1</filterpriority>
        Public Function Err() As ErrObject
            Return Microsoft.VisualBasic.Information.Err()
        End Function

        ''' <summary>
        ''' Returns an integer indicating the line number of the last executed statement. Read-only.
        ''' </summary>
        ''' <returns>
        ''' Returns an integer indicating the line number of the last executed statement. Read-only.
        ''' </returns>
        ''' <filterpriority>1</filterpriority>
        <EditorBrowsable(EditorBrowsableState.Never)>
        Public Function Erl() As Integer
            Return Microsoft.VisualBasic.Information.Erl()
        End Function

        ''' <summary>
        ''' Returns a Boolean value indicating whether a variable points to an array.
        ''' </summary>
        ''' <returns>
        ''' Returns a Boolean value indicating whether a variable points to an array.
        ''' </returns>
        ''' <param name="VarName">
        ''' Required. Object variable.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function IsArray(VarName As Object) As Boolean
            Return Microsoft.VisualBasic.Information.IsArray(VarName)
        End Function

        ''' <summary>
        ''' Returns a Boolean value indicating whether an expression represents a valid Date value.
        ''' </summary>
        ''' <returns>
        ''' Returns a Boolean value indicating whether an expression represents a valid Date value.
        ''' </returns>
        ''' <param name="Expression">
        ''' Required. Object expression.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function IsDate(Expression As Object) As Boolean
            Return Microsoft.VisualBasic.Information.IsDate(Expression)
        End Function

        ''' <summary>
        ''' Returns a Boolean value indicating whether an expression evaluates to the <see cref="T:System.DBNull" /> class.
        ''' </summary>
        ''' <returns>
        ''' Returns a Boolean value indicating whether an expression evaluates to the <see cref="T:System.DBNull" /> class.
        ''' </returns>
        ''' <param name="Expression">
        ''' Required. Object expression.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function IsDBNull(Expression As Object) As Boolean
            Return Microsoft.VisualBasic.Information.IsDBNull(Expression)
        End Function

        ''' <summary>
        ''' Returns a Boolean value indicating whether an expression has no object assigned to it.
        ''' </summary>
        ''' <returns>
        ''' Returns a Boolean value indicating whether an expression has no object assigned to it.
        ''' </returns>
        ''' <param name="Expression">
        ''' Required. Object expression.
        ''' </param>
        ''' <filterpriority>3</filterpriority>
        Public Function IsNothing(Expression As Object) As Boolean
            Return Microsoft.VisualBasic.Information.IsNothing(Expression)
        End Function

        ''' <summary>
        ''' Returns a Boolean value indicating whether an expression is an exception type.
        ''' </summary>
        ''' <returns>
        ''' Returns a Boolean value indicating whether an expression is an exception type.
        ''' </returns>
        ''' <param name="Expression">
        ''' Required. Object expression.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function IsError(Expression As Object) As Boolean
            Return Microsoft.VisualBasic.Information.IsError(Expression)
        End Function

        ''' <summary>
        ''' Returns a Boolean value indicating whether an expression evaluates to a reference type.
        ''' </summary>
        ''' <returns>
        ''' Returns a Boolean value indicating whether an expression evaluates to a reference type.
        ''' </returns>
        ''' <param name="Expression">
        ''' Required. Object expression.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function IsReference(Expression As Object) As Boolean
            Return Microsoft.VisualBasic.Information.IsReference(Expression)
        End Function

        ''' <summary>
        ''' Returns the lowest available subscript for the indicated dimension of an array.
        ''' </summary>
        ''' <returns>Integer. The lowest value the subscript for the specified dimension can contain. LBound always returns 0 as long as <paramref name="Array" /> has been initialized, even if it has no elements, for example if it is a zero-length string. If <paramref name="Array" /> is Nothing, LBound throws an <see cref="T:System.ArgumentNullException" />.
        ''' </returns>
        ''' <param name="Array">
        ''' Required. Array of any data type. The array in which you want to find the lowest possible subscript of a dimension.
        ''' </param>
        ''' <param name="Rank">
        ''' Optional. Integer. The dimension for which the lowest possible subscript is to be returned. Use 1 for the first dimension, 2 for the second, and so on. If <paramref name="Rank" /> is omitted, 1 is assumed.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function LBound(Array As Array, Optional Rank As Integer = 1) As Integer
            Return Microsoft.VisualBasic.Information.LBound(Array, Rank)
        End Function

        ''' <summary>
        ''' Returns the highest available subscript for the indicated dimension of an array.
        ''' </summary>
        ''' <returns>Integer. The highest value the subscript for the specified dimension can contain. If <paramref name="Array" /> has only one element, UBound returns 0. If <paramref name="Array" /> has no elements, for example if it is a zero-length string, UBound returns -1. 
        ''' </returns>
        ''' <param name="Array">
        ''' Required. Array of any data type. The array in which you want to find the highest possible subscript of a dimension.
        ''' </param>
        ''' <param name="Rank">
        ''' Optional. Integer. The dimension for which the highest possible subscript is to be returned. Use 1 for the first dimension, 2 for the second, and so on. If <paramref name="Rank" /> is omitted, 1 is assumed.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function UBound(Array As Array, Optional Rank As Integer = 1) As Integer
            Return Microsoft.VisualBasic.Information.UBound(Array, Rank)
        End Function

        ''' <summary>
        ''' Returns an Integer value representing the RGB color code corresponding to the specified color number.
        ''' </summary>
        ''' <returns>
        ''' Returns an Integer value representing the RGB color code corresponding to the specified color number.
        ''' </returns>
        ''' <param name="Color">
        ''' Required. A whole number in the range 0–15.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function QBColor(Color As Integer) As Integer
            Return Microsoft.VisualBasic.Information.QBColor(Color)
        End Function

        ''' <summary>
        ''' Returns an Integer value representing an RGB color value from a set of red, green and blue color components.
        ''' </summary>
        ''' <returns>
        ''' Returns an Integer value representing an RGB color value from a set of red, green and blue color components.
        ''' </returns>
        ''' <param name="Red">
        ''' Required. Integer in the range 0–255, inclusive, that represents the intensity of the red component of the color.
        ''' </param>
        ''' <param name="Green">
        ''' Required. Integer in the range 0–255, inclusive, that represents the intensity of the green component of the color.
        ''' </param>
        ''' <param name="Blue">
        ''' Required. Integer in the range 0–255, inclusive, that represents the intensity of the blue component of the color.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function RGB(Red As Integer, Green As Integer, Blue As Integer) As Integer
            Return Microsoft.VisualBasic.Information.RGB(Red, Green, Blue)
        End Function

        ''' <summary>
        ''' Returns an Integer value containing the data type classification of a variable.
        ''' </summary>
        ''' <returns>
        ''' Returns an Integer value containing the data type classification of a variable.
        ''' </returns>
        ''' <param name="VarName">
        ''' Required. Object variable. If Option Strict is Off, you can pass a variable of any data type except a structure.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function VarType(VarName As Object) As VariantType
            Return Microsoft.VisualBasic.Information.VarType(VarName)
        End Function



        ''' <summary>
        ''' Returns a Boolean value indicating whether an expression can be evaluated as a number.
        ''' </summary>
        ''' <returns>
        ''' Returns a Boolean value indicating whether an expression can be evaluated as a number.
        ''' </returns>
        ''' <param name="Expression">
        ''' Required. Object expression.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function IsNumeric(Expression As Object) As Boolean
            Return Microsoft.VisualBasic.Information.IsNumeric(Expression)
        End Function


        ''' <summary>
        ''' Returns a String value containing data-type information about a variable.
        ''' </summary>
        ''' <returns>
        ''' Returns a String value containing data-type information about a variable.
        ''' </returns>
        ''' <param name="VarName">
        ''' Required. Object variable. If Option Strict is Off, you can pass a variable of any data type except a structure.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
        ''' </PermissionSet>
        Public Function TypeName(VarName As Object) As String
            Return Microsoft.VisualBasic.Information.TypeName(VarName)
        End Function

        ''' <summary>
        ''' Returns a String value containing the system data type name of a variable.
        ''' </summary>
        ''' <returns>
        ''' Returns a String value containing the system data type name of a variable.
        ''' </returns>
        ''' <param name="VbName">
        ''' Required. A String variable containing a Visual Basic type name.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function SystemTypeName(VbName As String) As String
            Return Microsoft.VisualBasic.Information.SystemTypeName(VbName)
        End Function

        ''' <summary>
        ''' Returns a String value containing the Visual Basic data type name of a variable.
        ''' </summary>
        ''' <returns>
        ''' Returns a String value containing the Visual Basic data type name of a variable.
        ''' </returns>
        ''' <param name="UrtName">
        ''' Required. String variable containing a type name used by the common language runtime.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function VbTypeName(UrtName As String) As String
            Return Microsoft.VisualBasic.Information.VbTypeName(UrtName)
        End Function
    End Module

    Public Module Interaction
        ''' <summary>
        ''' Runs an executable program and returns an integer containing the program's process ID if it is still running.
        ''' </summary>
        ''' <returns>
        ''' Runs an executable program and returns an integer containing the program's process ID if it is still running.
        ''' </returns>
        ''' <param name="PathName">
        ''' Required. String. Name of the program to execute, together with any required arguments and command-line switches. <paramref name="PathName" /> can also include the drive and the directory path or folder.
        ''' 
        ''' If you do not know the path to the program, you can use the My.Computer.FileSystem.GetFiles Method to locate it. For example, you can call My.Computer.FileSystem.GetFiles("C:\", True, "testFile.txt"), which returns the full path of every file named testFile.txt anywhere on drive C:\.
        ''' </param>
        ''' <param name="Style">
        ''' Optional. AppWinStyle. A value chosen from the AppWinStyle Enumeration specifying the style of the window in which the program is to run. If <paramref name="Style" /> is omitted, Shell uses AppWinStyle.MinimizedFocus, which starts the program minimized and with focus. 
        ''' </param>
        ''' <param name="Wait">
        ''' Optional. Boolean. A value indicating whether the Shell function should wait for completion of the program. If <paramref name="Wait" /> is omitted, Shell uses False.
        ''' </param>
        ''' <param name="Timeout">
        ''' Optional. Integer. The number of milliseconds to wait for completion if <paramref name="Wait" /> is True. If <paramref name="Timeout" /> is omitted, Shell uses -1, which means there is no timeout and Shell does not return until the program finishes. Therefore, if you omit <paramref name="Timeout" /> or set it to -1, it is possible that Shell might never return control to your program.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
        ''' <IPermission class="System.Security.Permissions.UIPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Window="AllWindows" />
        ''' </PermissionSet>
        <SecurityPermission(SecurityAction.Demand, Flags:=SecurityPermissionFlag.UnmanagedCode)>
        Public Function Shell(PathName As String, Optional Style As AppWinStyle = AppWinStyle.MinimizedFocus, Optional Wait As Boolean = False, Optional Timeout As Integer = -1) As Integer
            Return Microsoft.VisualBasic.Interaction.Shell(PathName, Style, Wait, Timeout)
        End Function

        ''' <summary>
        ''' Activates an application that is already running.
        ''' </summary>
        ''' <param name="ProcessId">Integer specifying the Win32 process ID number assigned to this process. You can use the ID returned by the Shell Function, provided it is not zero.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
        ''' <IPermission class="System.Security.Permissions.UIPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Window="AllWindows" />
        ''' </PermissionSet>
        <SecurityPermission(SecurityAction.Demand, Flags:=SecurityPermissionFlag.UnmanagedCode)>
        Public Sub AppActivate(ProcessId As Integer)
            Microsoft.VisualBasic.Interaction.AppActivate(ProcessId)
        End Sub

        ''' <summary>
        ''' Activates an application that is already running.
        ''' </summary>
        ''' <param name="Title">String expression specifying the title in the title bar of the application you want to activate. You can use the title assigned to the application when it was launched.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
        ''' <IPermission class="System.Security.Permissions.UIPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Window="AllWindows" />
        ''' </PermissionSet>
        <SecurityPermission(SecurityAction.Demand, Flags:=SecurityPermissionFlag.UnmanagedCode)>
        Public Sub AppActivate(Title As String)
            Microsoft.VisualBasic.Interaction.AppActivate(Title)
        End Sub

        ''' <summary>
        ''' Returns the argument portion of the command line used to start Visual Basic or an executable program developed with Visual Basic. The My feature provides greater productivity and performance than the Command function. For more information, see My.Application.CommandLineArgs Property.
        ''' </summary>
        ''' <returns>
        ''' Returns the argument portion of the command line used to start Visual Basic or an executable program developed with Visual Basic.
        ''' 
        ''' The My feature provides greater productivity and performance than the Command function. For more information, see My.Application.CommandLineArgs Property.
        ''' </returns>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.EnvironmentPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Read="Path" />
        ''' </PermissionSet>
        Public Function Command() As String
            Return Microsoft.VisualBasic.Interaction.Command()
        End Function

        ''' <summary>
        ''' Returns the string associated with an operating-system environment variable. 
        ''' </summary>
        ''' <returns>
        ''' Returns the string associated with an operating-system environment variable. 
        ''' </returns>
        ''' <param name="Expression">
        ''' Required. Expression that evaluates either a string containing the name of an environment variable, or an integer corresponding to the numeric order of an environment string in the environment-string table.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.EnvironmentPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Function Environ(Expression As Integer) As String
            Return Microsoft.VisualBasic.Interaction.Environ(Expression)
        End Function

        ''' <summary>
        ''' Returns the string associated with an operating-system environment variable. 
        ''' </summary>
        ''' <returns>
        ''' Returns the string associated with an operating-system environment variable. 
        ''' </returns>
        ''' <param name="Expression">
        ''' Required. Expression that evaluates either a string containing the name of an environment variable, or an integer corresponding to the numeric order of an environment string in the environment-string table.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.EnvironmentPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Function Environ(Expression As String) As String
            Return Microsoft.VisualBasic.Interaction.Environ(Expression)
        End Function

        ''' <summary>
        ''' Sounds a tone through the computer's speaker.
        ''' </summary>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.UIPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Window="SafeSubWindows" />
        ''' </PermissionSet>
        Public Sub Beep()
            Microsoft.VisualBasic.Interaction.Beep()
        End Sub

        ''' <summary>
        ''' Displays a prompt in a dialog box, waits for the user to input text or click a button, and then returns a string containing the contents of the text box.
        ''' </summary>
        ''' <returns>
        ''' Displays a prompt in a dialog box, waits for the user to input text or click a button, and then returns a string containing the contents of the text box.
        ''' </returns>
        ''' <param name="Prompt">
        ''' Required String expression displayed as the message in the dialog box. The maximum length of <paramref name="Prompt" /> is approximately 1024 characters, depending on the width of the characters used. If <paramref name="Prompt" /> consists of more than one line, you can separate the lines using a carriage return character (Chr(13)), a line feed character (Chr(10)), or a carriage return/line feed combination (Chr(13) &amp; Chr(10)) between each line.
        ''' </param>
        ''' <param name="Title">
        ''' Optional. String expression displayed in the title bar of the dialog box. If you omit <paramref name="Title" />, the application name is placed in the title bar.
        ''' </param>
        ''' <param name="DefaultResponse">
        ''' Optional. String expression displayed in the text box as the default response if no other input is provided. If you omit <paramref name="DefaultResponse" />, the displayed text box is empty.
        ''' </param>
        ''' <param name="XPos">
        ''' Optional. Numeric expression that specifies, in twips, the distance of the left edge of the dialog box from the left edge of the screen. If you omit <paramref name="XPos" />, the dialog box is centered horizontally.
        ''' </param>
        ''' <param name="YPos">
        ''' Optional. Numeric expression that specifies, in twips, the distance of the upper edge of the dialog box from the top of the screen. If you omit <paramref name="YPos" />, the dialog box is positioned vertically approximately one-third of the way down the screen.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.EnvironmentPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode, ControlEvidence" />
        ''' </PermissionSet>
        <HostProtection(SecurityAction.LinkDemand, Resources:=HostProtectionResource.UI)>
        Public Function InputBox(Prompt As String, Optional Title As String = "", Optional DefaultResponse As String = "", Optional XPos As Integer = -1, Optional YPos As Integer = -1) As String
            Return Microsoft.VisualBasic.Interaction.InputBox(Prompt, Title, DefaultResponse, XPos, YPos)
        End Function

        ''' <summary>
        ''' Displays a message in a dialog box, waits for the user to click a button, and then returns an integer indicating which button the user clicked.
        ''' </summary>
        ''' <returns>
        ''' Constant OK, Value 1. Constant Cancel, Value 2. Constant Abort, Value 3. Constant Retry, Value 4. Constant Ignore, Value 5. Constant Yes, Value 6. Constant No, Value 7. 
        ''' </returns>
        ''' <param name="Prompt">
        ''' Required. String expression displayed as the message in the dialog box. The maximum length of <paramref name="Prompt" /> is approximately 1024 characters, depending on the width of the characters used. If <paramref name="Prompt" /> consists of more than one line, you can separate the lines using a carriage return character (Chr(13)), a line feed character (Chr(10)), or a carriage return/linefeed character combination (Chr(13) &amp; Chr(10)) between each line.
        ''' </param>
        ''' <param name="Buttons">
        ''' Optional. Numeric expression that is the sum of values specifying the number and type of buttons to display, the icon style to use, the identity of the default button, and the modality of the message box. If you omit <paramref name="Buttons" />, the default value is zero.
        ''' </param>
        ''' <param name="Title">
        ''' Optional. String expression displayed in the title bar of the dialog box. If you omit <paramref name="Title" />, the application name is placed in the title bar.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.EnvironmentPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode, ControlEvidence" />
        ''' </PermissionSet>
        <HostProtection(SecurityAction.LinkDemand, Resources:=HostProtectionResource.UI)>
        Public Function MsgBox(Prompt As Object, Optional Buttons As MsgBoxStyle = MsgBoxStyle.OkOnly, Optional Title As Object = Nothing) As MsgBoxResult
            Return Microsoft.VisualBasic.Interaction.MsgBox(Prompt, Buttons, Title)
        End Function

        ''' <summary>
        ''' Selects and returns a value from a list of arguments.
        ''' </summary>
        ''' <returns>
        ''' Selects and returns a value from a list of arguments.
        ''' </returns>
        ''' <param name="Index">
        ''' Required. Double. Numeric expression that results in a value between 1 and the number of elements passed in the <paramref name="Choice" /> argument.
        ''' </param>
        ''' <param name="Choice">
        ''' Required. Object parameter array. You can supply either a single variable or an expression that evaluates to the Object data type, to a list of Object variables or expressions separated by commas, or to a single-dimensional array of Object elements.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function Choose(Index As Double, <System.Runtime.InteropServices.OutAttribute()> ParamArray Choice As Object()) As Object
            Return Microsoft.VisualBasic.Interaction.Choose(Index, Choice)
        End Function

        ''' <summary>
        ''' Returns one of two objects, depending on the evaluation of an expression.
        ''' </summary>
        ''' <returns>
        ''' Returns one of two objects, depending on the evaluation of an expression.
        ''' </returns>
        ''' <param name="Expression">
        ''' Required. Boolean. The expression you want to evaluate.
        ''' </param>
        ''' <param name="TruePart">
        ''' Required. Object. Returned if <paramref name="Expression" /> evaluates to True.
        ''' </param>
        ''' <param name="FalsePart">
        ''' Required. Object. Returned if <paramref name="Expression" /> evaluates to False.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function IIf(Expression As Boolean, TruePart As Object, FalsePart As Object) As Object
            Return Microsoft.VisualBasic.Interaction.IIf(Expression, TruePart, FalsePart)
        End Function

        ''' <summary>
        ''' Returns a string representing the calculated range that contains a number.
        ''' </summary>
        ''' <returns>
        ''' Returns a string representing the calculated range that contains a number.
        ''' </returns>
        ''' <param name="Number">
        ''' Required. Long. Whole number that you want to locate within one of the calculated ranges.
        ''' </param>
        ''' <param name="Start">
        ''' Required. Long. Whole number that indicates the start of the set of calculated ranges. <paramref name="Start" /> cannot be less than 0.
        ''' </param>
        ''' <param name="Stop">
        ''' Required. Long. Whole number that indicates the end of the set of calculated ranges. <paramref name="Stop" /> cannot be less than or equal to <paramref name="Start" />.
        ''' </param>
        ''' <param name="Interval">
        ''' Required. Long. Whole number that indicates the size of each range calculated between <paramref name="Start" /> and <paramref name="Stop" />. <paramref name="Interval" /> cannot be less than 1.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function Partition(Number As Long, Start As Long, [Stop] As Long, Interval As Long) As String
            Return Microsoft.VisualBasic.Interaction.Partition(Number, Start, [Stop], Interval)
        End Function

        ''' <summary>
        ''' Evaluates a list of expressions and returns an Object value corresponding to the first expression in the list that is True.
        ''' </summary>
        ''' <returns>
        ''' Evaluates a list of expressions and returns an Object value corresponding to the first expression in the list that is True.
        ''' </returns>
        ''' <param name="VarExpr">
        ''' Required. Object parameter array. Must have an even number of elements. You can supply a list of Object variables or expressions separated by commas, or a single-dimensional array of Object elements.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
        ''' </PermissionSet>
        Public Function Switch(<System.Runtime.InteropServices.OutAttribute()> ParamArray VarExpr As Object()) As Object
            Return Microsoft.VisualBasic.Interaction.Switch(VarExpr)
        End Function

        ''' <summary>
        ''' Deletes a section or key setting from an application's entry in the Windows registry. The My feature gives you greater productivity and performance in registry operations than the DeleteSetting function. For more information, see My.Computer.Registry Object.
        ''' </summary>
        ''' <param name="AppName">
        ''' Required. String expression containing the name of the application or project to which the section or key setting applies.
        ''' </param>
        ''' <param name="Section">
        ''' Required. String expression containing the name of the section from which the key setting is being deleted. If only <paramref name="AppName" /> and <paramref name="Section" /> are provided, the specified section is deleted along with all related key settings.
        ''' </param>
        ''' <param name="Key">
        ''' Optional. String expression containing the name of the key setting being deleted.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" PathDiscovery="*AllFiles*" />
        ''' <IPermission class="System.Security.Permissions.RegistryPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
        ''' </PermissionSet>
        <HostProtection(SecurityAction.LinkDemand, Resources:=HostProtectionResource.ExternalProcessMgmt)>
        Public Sub DeleteSetting(AppName As String, Optional Section As String = Nothing, Optional Key As String = Nothing)
            Microsoft.VisualBasic.Interaction.DeleteSetting(AppName, Section, Key)
        End Sub

        ''' <summary>
        ''' Returns a list of key settings and their respective values (originally created with SaveSetting) from an application's entry in the Windows registry. Using the My feature gives you greater productivity and performance in registry operations than GetAllSettings. For more information, see My.Computer.Registry Object.
        ''' </summary>
        ''' <returns>
        ''' Returns a list of key settings and their respective values (originally created with SaveSetting) from an application's entry in the Windows registry.
        ''' 
        ''' Using the My feature gives you greater productivity and performance in registry operations than GetAllSettings. For more information, see My.Computer.Registry Object.
        ''' </returns>
        ''' <param name="AppName">
        ''' Required. String expression containing the name of the application or project whose key settings are requested.
        ''' </param>
        ''' <param name="Section">
        ''' Required. String expression containing the name of the section whose key settings are requested. GetAllSettings returns an object that contains a two-dimensional array of strings. The strings contain all the key settings in the specified section, plus their corresponding values.
        ''' </param>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.RegistryPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        ''' <filterpriority>1</filterpriority>
        Public Function GetAllSettings(AppName As String, Section As String) As String(,)
            Return Microsoft.VisualBasic.Interaction.GetAllSettings(AppName, Section)
        End Function

        ''' <summary>
        ''' Returns a key setting value from an application's entry in the Windows registry. The My feature gives you greater productivity and performance in registry operations than GetAllSettings. For more information, see My.Computer.Registry Object.
        ''' </summary>
        ''' <returns>
        ''' Returns a key setting value from an application's entry in the Windows registry.
        ''' 
        ''' The My feature gives you greater productivity and performance in registry operations than GetAllSettings. For more information, see My.Computer.Registry Object.
        ''' </returns>
        ''' <param name="AppName">
        ''' Required. String expression containing the name of the application or project whose key setting is requested.
        ''' </param>
        ''' <param name="Section">
        ''' Required. String expression containing the name of the section in which the key setting is found.
        ''' </param>
        ''' <param name="Key">
        ''' Required. String expression containing the name of the key setting to return.
        ''' </param>
        ''' <param name="Default">
        ''' Optional. Expression containing the value to return if no value is set in the <paramref name="Key" /> setting. If omitted, <paramref name="Default" /> is assumed to be a zero-length string ("").
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.RegistryPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Function GetSetting(AppName As String, Section As String, Key As String, Optional [Default] As String = "") As String
            Return Microsoft.VisualBasic.Interaction.GetSetting(AppName, Section, Key, [Default])
        End Function

        ''' <summary>
        ''' Saves or creates an application entry in the Windows registry. The My feature gives you greater productivity and performance in registry operations than SaveSetting. For more information, see My.Computer.Registry Object.
        ''' </summary>
        ''' <param name="AppName">
        ''' Required. String expression containing the name of the application or project to which the setting applies.
        ''' </param>
        ''' <param name="Section">
        ''' Required. String expression containing the name of the section in which the key setting is being saved.
        ''' </param>
        ''' <param name="Key">
        ''' Required. String expression containing the name of the key setting being saved.
        ''' </param>
        ''' <param name="Setting">
        ''' Required. Expression containing the value to which <paramref name="Key" /> is being set.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.RegistryPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Sub SaveSetting(AppName As String, Section As String, Key As String, Setting As String)
            Microsoft.VisualBasic.Interaction.SaveSetting(AppName, Section, Key, Setting)
        End Sub

        ''' <summary>
        ''' Creates and returns a reference to a COM object. CreateObject cannot be used to create instances of classes in Visual Basic unless those classes are explicitly exposed as COM components.
        ''' </summary>
        ''' <returns>
        ''' Creates and returns a reference to a COM object. CreateObject cannot be used to create instances of classes in Visual Basic unless those classes are explicitly exposed as COM components.
        ''' </returns>
        ''' <param name="ProgId">
        ''' Required. String. The program ID of the object to create.
        ''' </param>
        ''' <param name="ServerName">
        ''' Optional. String. The name of the network server where the object will be created. If <paramref name="ServerName" /> is an empty string (""), the local computer is used.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.EnvironmentPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Read="COMPUTERNAME" />
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
        ''' </PermissionSet>
        <SecurityPermission(SecurityAction.Demand, Flags:=SecurityPermissionFlag.UnmanagedCode), HostProtection(SecurityAction.LinkDemand, Resources:=HostProtectionResource.ExternalProcessMgmt)>
        Public Function CreateObject(ProgId As String, Optional ServerName As String = "") As Object
            Return Microsoft.VisualBasic.Interaction.CreateObject(ProgId, ServerName)
        End Function

        ''' <summary>
        ''' Returns a reference to an object provided by a COM component.
        ''' </summary>
        ''' <returns>
        ''' Returns a reference to an object provided by a COM component.
        ''' </returns>
        ''' <param name="PathName">
        ''' Optional. String. The full path and name of the file containing the object to retrieve. If <paramref name="PathName" /> is omitted, <paramref name="Class" /> is required.
        ''' </param>
        ''' <param name="Class">
        ''' Required if <paramref name="PathName" /> is not supplied. String. A string representing the class of the object. The <paramref name="Class" /> argument has the following syntax and parts:
        ''' <paramref name="appname" />.<paramref name="objecttype" />
        ''' Parameter
        ''' 
        ''' Description
        ''' <paramref name="appname" />
        ''' Required. String. The name of the application providing the object.
        ''' <paramref name="objecttype" />
        ''' Required. String. The type or class of object to create.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
        ''' </PermissionSet>
        <SecurityPermission(SecurityAction.Demand, Flags:=SecurityPermissionFlag.UnmanagedCode), HostProtection(SecurityAction.LinkDemand, Resources:=HostProtectionResource.ExternalProcessMgmt)>
        Public Function GetObject(Optional PathName As String = Nothing, Optional [Class] As String = Nothing) As Object
            Return Microsoft.VisualBasic.Interaction.GetObject(PathName, [Class])
        End Function

        ''' <summary>
        ''' Executes a method on an object, or sets or returns a property on an object.
        ''' </summary>
        ''' <returns>
        ''' Executes a method on an object, or sets or returns a property on an object.
        ''' </returns>
        ''' <param name="ObjectRef">
        ''' Required. Object. A pointer to the object exposing the property or method.
        ''' </param>
        ''' <param name="ProcName">
        ''' Required. String. A string expression containing the name of the property or method on the object.
        ''' </param>
        ''' <param name="UseCallType">
        ''' Required. An enumeration member of type CallType Enumeration representing the type of procedure being called. The value of CallType can be Method, Get, or Set.
        ''' </param>
        ''' <param name="Args">
        ''' Optional. ParamArray. A parameter array containing the arguments to be passed to the property or method being called.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ''' </PermissionSet>
        Public Function CallByName(ObjectRef As Object, ProcName As String, UseCallType As CallType, <System.Runtime.InteropServices.OutAttribute()> ParamArray Args As Object()) As Object
            Return Microsoft.VisualBasic.Interaction.CallByName(ObjectRef, ProcName, UseCallType, Args)
        End Function
    End Module

    Public Module Strings
        ''' <summary>
        ''' Returns an Integer value representing the character code corresponding to a character.
        ''' </summary>
        ''' <returns>
        ''' Returns an Integer value representing the character code corresponding to a character.
        ''' </returns>
        ''' <param name="String">
        ''' Required. Any valid Char or String expression. If <paramref name="String" /> is a String expression, only the first character of the string is used for input. If <paramref name="String" /> is Nothing or contains no characters, an <see cref="T:System.ArgumentException" /> error occurs.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function Asc([String] As Char) As Integer
            Return Microsoft.VisualBasic.Strings.Asc([String])
        End Function

        ''' <summary>
        ''' Returns an Integer value representing the character code corresponding to a character.
        ''' </summary>
        ''' <returns>
        ''' Returns an Integer value representing the character code corresponding to a character.
        ''' </returns>
        ''' <param name="String">
        ''' Required. Any valid Char or String expression. If <paramref name="String" /> is a String expression, only the first character of the string is used for input. If <paramref name="String" /> is Nothing or contains no characters, an <see cref="T:System.ArgumentException" /> error occurs.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function Asc([String] As String) As Integer
            Return Microsoft.VisualBasic.Strings.Asc([String])
        End Function

        ''' <summary>
        ''' Returns an Integer value representing the character code corresponding to a character.
        ''' </summary>
        ''' <returns>
        ''' Returns an Integer value representing the character code corresponding to a character.
        ''' </returns>
        ''' <param name="String">
        ''' Required. Any valid Char or String expression. If <paramref name="String" /> is a String expression, only the first character of the string is used for input. If <paramref name="String" /> is Nothing or contains no characters, an <see cref="T:System.ArgumentException" /> error occurs.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function AscW([String] As String) As Integer
            Return Microsoft.VisualBasic.Strings.AscW([String])
        End Function

        ''' <summary>
        ''' Returns an Integer value representing the character code corresponding to a character.
        ''' </summary>
        ''' <returns>
        ''' Returns an Integer value representing the character code corresponding to a character.
        ''' </returns>
        ''' <param name="String">
        ''' Required. Any valid Char or String expression. If <paramref name="String" /> is a String expression, only the first character of the string is used for input. If <paramref name="String" /> is Nothing or contains no characters, an <see cref="T:System.ArgumentException" /> error occurs.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function AscW([String] As Char) As Integer
            Return Microsoft.VisualBasic.Strings.AscW([String])
        End Function

        ''' <summary>
        ''' Returns the character associated with the specified character code.
        ''' </summary>
        ''' <returns>
        ''' Returns the character associated with the specified character code.
        ''' </returns>
        ''' <param name="CharCode">
        ''' Required. An Integer expression representing the code point, or character code, for the character. If <paramref name="CharCode" /> is outside the valid range, an <see cref="T:System.ArgumentException" /> error occurs. The valid range for Chr is 0 through 255, and the valid range for ChrW is -32768 through 65535.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function Chr(CharCode As Integer) As Char
            Return Microsoft.VisualBasic.Strings.Chr(CharCode)
        End Function

        ''' <summary>
        ''' Returns the character associated with the specified character code.
        ''' </summary>
        ''' <returns>
        ''' Returns the character associated with the specified character code.
        ''' </returns>
        ''' <param name="CharCode">
        ''' Required. An Integer expression representing the code point, or character code, for the character. If <paramref name="CharCode" /> is outside the valid range, an <see cref="T:System.ArgumentException" /> error occurs. The valid range for Chr is 0 through 255, and the valid range for ChrW is -32768 through 65535.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function ChrW(CharCode As Integer) As Char
            Return Microsoft.VisualBasic.Strings.ChrW(CharCode)
        End Function

        ''' <summary>
        ''' Returns a zero-based array containing a subset of a String array based on specified filter criteria.
        ''' </summary>
        ''' <returns>
        ''' Returns a zero-based array containing a subset of a String array based on specified filter criteria.
        ''' </returns>
        ''' <param name="Source">
        ''' Required. One-dimensional array of strings to be searched.
        ''' </param>
        ''' <param name="Match">
        ''' Required. String to search for.
        ''' </param>
        ''' <param name="Include">
        ''' Optional. Boolean value indicating whether to return substrings that include or exclude <paramref name="Match" />. If <paramref name="Include" /> is True, the Filter function returns the subset of the array that contains <paramref name="Match" /> as a substring. If <paramref name="Include" /> is False, the Filter function returns the subset of the array that does not contain <paramref name="Match" /> as a substring.
        ''' </param>
        ''' <param name="Compare">
        ''' Optional. Numeric value indicating the kind of string comparison to use. See "Settings" for values.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
        ''' </PermissionSet>
        Public Function Filter(Source As Object(), Match As String, Optional Include As Boolean = True, Optional Compare As CompareMethod = CompareMethod.Binary) As String()
            Return Microsoft.VisualBasic.Strings.Filter(Source, Match, Include, Compare)
        End Function

        ''' <summary>
        ''' Returns a zero-based array containing a subset of a String array based on specified filter criteria.
        ''' </summary>
        ''' <returns>
        ''' Returns a zero-based array containing a subset of a String array based on specified filter criteria.
        ''' </returns>
        ''' <param name="Source">
        ''' Required. One-dimensional array of strings to be searched.
        ''' </param>
        ''' <param name="Match">
        ''' Required. String to search for.
        ''' </param>
        ''' <param name="Include">
        ''' Optional. Boolean value indicating whether to return substrings that include or exclude <paramref name="Match" />. If <paramref name="Include" /> is True, the Filter function returns the subset of the array that contains <paramref name="Match" /> as a substring. If <paramref name="Include" /> is False, the Filter function returns the subset of the array that does not contain <paramref name="Match" /> as a substring.
        ''' </param>
        ''' <param name="Compare">
        ''' Optional. Numeric value indicating the kind of string comparison to use. See "Settings" for values.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
        ''' </PermissionSet>
        Public Function Filter(Source As String(), Match As String, Optional Include As Boolean = True, Optional Compare As CompareMethod = CompareMethod.Binary) As String()
            Return Microsoft.VisualBasic.Strings.Filter(Source, Match, Include, Compare)
        End Function

        ''' <summary>
        ''' Returns an integer specifying the start position of the first occurrence of one string within another.
        ''' </summary>
        ''' <returns>
        ''' If <paramref name="String1" /> is zero length or Nothing, InStr returns 0. If <paramref name="String2" /> is zero length or Nothing, InStr returns <paramref name="start" />. If <paramref name="String2" /> is not found, InStr returns 0. If <paramref name="String2" /> is found within <paramref name="String1" />, InStr returns position where match begins. If <paramref name="Start" /> &gt; <paramref name="String2" />, InStr returns 0. 
        ''' </returns>
        ''' <param name="String1">
        ''' Required. String expression being searched.
        ''' </param>
        ''' <param name="String2">
        ''' Required. String expression sought.
        ''' </param>
        ''' <param name="Compare">
        ''' Optional. Specifies the type of string comparison. If <paramref name="Compare" /> is omitted, the Option Compare setting determines the type of comparison. 
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function InStr(String1 As String, String2 As String, Optional Compare As CompareMethod = CompareMethod.Binary) As Integer
            Return Microsoft.VisualBasic.Strings.InStr(String1, String2, Compare)
        End Function

        ''' <summary>
        ''' Returns an integer specifying the start position of the first occurrence of one string within another.
        ''' </summary>
        ''' <returns>
        ''' If <paramref name="String1" /> is zero length or Nothing, InStr returns 0. If <paramref name="String2" /> is zero length or Nothing, InStr returns <paramref name="start" />. If <paramref name="String2" /> is not found, InStr returns 0. If <paramref name="String2" /> is found within <paramref name="String1" />, InStr returns position where match begins. If <paramref name="Start" /> &gt; <paramref name="String2" />, InStr returns 0. 
        ''' </returns>
        ''' <param name="Start">
        ''' Optional. Numeric expression that sets the starting position for each search. If omitted, search begins at the first character position. The start index is 1-based.
        ''' </param>
        ''' <param name="String1">
        ''' Required. String expression being searched.
        ''' </param>
        ''' <param name="String2">
        ''' Required. String expression sought.
        ''' </param>
        ''' <param name="Compare">
        ''' Optional. Specifies the type of string comparison. If <paramref name="Compare" /> is omitted, the Option Compare setting determines the type of comparison. 
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function InStr(Start As Integer, String1 As String, String2 As String, Optional Compare As CompareMethod = CompareMethod.Binary) As Integer
            Return Microsoft.VisualBasic.Strings.InStr(Start, String1, String2, Compare)
        End Function

        ''' <summary>
        ''' Returns the position of the first occurrence of one string within another, starting from the right side of the string.
        ''' </summary>
        ''' <returns>
        ''' If <paramref name="StringCheck" /> is zero-length, InStrRev returns 0. If <paramref name="StringMatch" /> is zero-length, InStrRev returns <paramref name="Start" />. If <paramref name="StringMatch" /> is not found, InStrRev returns 0. If <paramref name="StringMatch" /> is found within <paramref name="StringCheck" />, InStrRev returns position at which the first match is found, starting with the right side of the string. If <paramref name="Start" /> is greater than length of <paramref name="StringMatch" />, InStrRev returns 0. 
        ''' </returns>
        ''' <param name="StringCheck">
        ''' Required. String expression being searched.
        ''' </param>
        ''' <param name="StringMatch">
        ''' Required. String expression being searched for.
        ''' </param>
        ''' <param name="Start">
        ''' Optional. Numeric expression setting the one-based starting position for each search, starting from the left side of the string. If <paramref name="Start" /> is omitted then –1 is used, meaning the search begins at the last character position. Search then proceeds from right to left.
        ''' </param>
        ''' <param name="Compare">
        ''' Optional. Numeric value indicating the kind of comparison to use when evaluating substrings. If omitted, a binary comparison is performed. See Settings for values.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function InStrRev(StringCheck As String, StringMatch As String, Optional Start As Integer = -1, Optional Compare As CompareMethod = CompareMethod.Binary) As Integer
            Return Microsoft.VisualBasic.Strings.InStrRev(StringCheck, StringMatch, Start, Compare)
        End Function

        ''' <summary>
        ''' Returns a string created by joining a number of substrings contained in an array.
        ''' </summary>
        ''' <returns>
        ''' Returns a string created by joining a number of substrings contained in an array.
        ''' </returns>
        ''' <param name="SourceArray">
        ''' Required. One-dimensional array containing substrings to be joined.
        ''' </param>
        ''' <param name="Delimiter">
        ''' Optional. Any string, used to separate the substrings in the returned string. If omitted, the space character (" ") is used. If <paramref name="Delimiter" /> is a zero-length string ("") or Nothing, all items in the list are concatenated with no delimiters.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
        ''' </PermissionSet>
        Public Function Join(SourceArray As Object(), Optional Delimiter As String = " ") As String
            Return Microsoft.VisualBasic.Strings.Join(SourceArray, Delimiter)
        End Function

        ''' <summary>
        ''' Returns a string created by joining a number of substrings contained in an array.
        ''' </summary>
        ''' <returns>
        ''' Returns a string created by joining a number of substrings contained in an array.
        ''' </returns>
        ''' <param name="SourceArray">
        ''' Required. One-dimensional array containing substrings to be joined.
        ''' </param>
        ''' <param name="Delimiter">
        ''' Optional. Any string, used to separate the substrings in the returned string. If omitted, the space character (" ") is used. If <paramref name="Delimiter" /> is a zero-length string ("") or Nothing, all items in the list are concatenated with no delimiters.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
        ''' </PermissionSet>
        Public Function Join(SourceArray As String(), Optional Delimiter As String = " ") As String
            Return Microsoft.VisualBasic.Strings.Join(SourceArray, Delimiter)
        End Function

        ''' <summary>
        ''' Returns a string or character converted to lowercase.
        ''' </summary>
        ''' <returns>
        ''' Returns a string or character converted to lowercase.
        ''' </returns>
        ''' <param name="Value">
        ''' Required. Any valid String or Char expression.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function LCase(Value As String) As String
            Return Microsoft.VisualBasic.Strings.LCase(Value)
        End Function

        ''' <summary>
        ''' Returns a string or character converted to lowercase.
        ''' </summary>
        ''' <returns>
        ''' Returns a string or character converted to lowercase.
        ''' </returns>
        ''' <param name="Value">
        ''' Required. Any valid String or Char expression.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function LCase(Value As Char) As Char
            Return Microsoft.VisualBasic.Strings.LCase(Value)
        End Function

        ''' <summary>
        ''' Returns an integer containing either the number of characters in a string or the nominal number of bytes required to store a variable.
        ''' </summary>
        ''' <returns>
        ''' Returns an integer containing either the number of characters in a string or the nominal number of bytes required to store a variable.
        ''' </returns>
        ''' <param name="Expression">
        ''' Any valid String expression or variable name. If <paramref name="Expression" /> is of type Object, the Len function returns the size as it will be written to the file by the FilePut function.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function Len(Expression As Boolean) As Integer
            Return Microsoft.VisualBasic.Strings.Len(Expression)
        End Function

        ''' <summary>
        ''' Returns an integer containing either the number of characters in a string or the nominal number of bytes required to store a variable.
        ''' </summary>
        ''' <returns>
        ''' Returns an integer containing either the number of characters in a string or the nominal number of bytes required to store a variable.
        ''' </returns>
        ''' <param name="Expression">
        ''' Any valid String expression or variable name. If <paramref name="Expression" /> is of type Object, the Len function returns the size as it will be written to the file by the FilePut function.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        <CLSCompliant(False)>
        Public Function Len(Expression As SByte) As Integer
            Return Microsoft.VisualBasic.Strings.Len(Expression)
        End Function

        ''' <summary>
        ''' Returns an integer containing either the number of characters in a string or the nominal number of bytes required to store a variable.
        ''' </summary>
        ''' <returns>
        ''' Returns an integer containing either the number of characters in a string or the nominal number of bytes required to store a variable.
        ''' </returns>
        ''' <param name="Expression">
        ''' Any valid String expression or variable name. If <paramref name="Expression" /> is of type Object, the Len function returns the size as it will be written to the file by the FilePut function.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function Len(Expression As Byte) As Integer
            Return Microsoft.VisualBasic.Strings.Len(Expression)
        End Function

        ''' <summary>
        ''' Returns an integer containing either the number of characters in a string or the nominal number of bytes required to store a variable.
        ''' </summary>
        ''' <returns>
        ''' Returns an integer containing either the number of characters in a string or the nominal number of bytes required to store a variable.
        ''' </returns>
        ''' <param name="Expression">
        ''' Any valid String expression or variable name. If <paramref name="Expression" /> is of type Object, the Len function returns the size as it will be written to the file by the FilePut function.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function Len(Expression As Short) As Integer
            Return Microsoft.VisualBasic.Strings.Len(Expression)
        End Function

        ''' <summary>
        ''' Returns an integer containing either the number of characters in a string or the nominal number of bytes required to store a variable.
        ''' </summary>
        ''' <returns>
        ''' Returns an integer containing either the number of characters in a string or the nominal number of bytes required to store a variable.
        ''' </returns>
        ''' <param name="Expression">
        ''' Any valid String expression or variable name. If <paramref name="Expression" /> is of type Object, the Len function returns the size as it will be written to the file by the FilePut function.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        <CLSCompliant(False)>
        Public Function Len(Expression As UShort) As Integer
            Return Microsoft.VisualBasic.Strings.Len(Expression)
        End Function

        ''' <summary>
        ''' Returns an integer containing either the number of characters in a string or the nominal number of bytes required to store a variable.
        ''' </summary>
        ''' <returns>
        ''' Returns an integer containing either the number of characters in a string or the nominal number of bytes required to store a variable.
        ''' </returns>
        ''' <param name="Expression">
        ''' Any valid String expression or variable name. If <paramref name="Expression" /> is of type Object, the Len function returns the size as it will be written to the file by the FilePut function.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function Len(Expression As Integer) As Integer
            Return Microsoft.VisualBasic.Strings.Len(Expression)
        End Function

        ''' <summary>
        ''' Returns an integer containing either the number of characters in a string or the nominal number of bytes required to store a variable.
        ''' </summary>
        ''' <returns>
        ''' Returns an integer containing either the number of characters in a string or the nominal number of bytes required to store a variable.
        ''' </returns>
        ''' <param name="Expression">
        ''' Any valid String expression or variable name. If <paramref name="Expression" /> is of type Object, the Len function returns the size as it will be written to the file by the FilePut function.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        <CLSCompliant(False)>
        Public Function Len(Expression As UInteger) As Integer
            Return Microsoft.VisualBasic.Strings.Len(Expression)
        End Function

        ''' <summary>
        ''' Returns an integer containing either the number of characters in a string or the nominal number of bytes required to store a variable.
        ''' </summary>
        ''' <returns>
        ''' Returns an integer containing either the number of characters in a string or the nominal number of bytes required to store a variable.
        ''' </returns>
        ''' <param name="Expression">
        ''' Any valid String expression or variable name. If <paramref name="Expression" /> is of type Object, the Len function returns the size as it will be written to the file by the FilePut function.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function Len(Expression As Long) As Integer
            Return Microsoft.VisualBasic.Strings.Len(Expression)
        End Function

        ''' <summary>
        ''' Returns an integer containing either the number of characters in a string or the nominal number of bytes required to store a variable.
        ''' </summary>
        ''' <returns>
        ''' Returns an integer containing either the number of characters in a string or the nominal number of bytes required to store a variable.
        ''' </returns>
        ''' <param name="Expression">
        ''' Any valid String expression or variable name. If <paramref name="Expression" /> is of type Object, the Len function returns the size as it will be written to the file by the FilePut function.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        <CLSCompliant(False)>
        Public Function Len(Expression As ULong) As Integer
            Return Microsoft.VisualBasic.Strings.Len(Expression)
        End Function

        ''' <summary>
        ''' Returns an integer containing either the number of characters in a string or the nominal number of bytes required to store a variable.
        ''' </summary>
        ''' <returns>
        ''' Returns an integer containing either the number of characters in a string or the nominal number of bytes required to store a variable.
        ''' </returns>
        ''' <param name="Expression">
        ''' Any valid String expression or variable name. If <paramref name="Expression" /> is of type Object, the Len function returns the size as it will be written to the file by the FilePut function.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function Len(Expression As Decimal) As Integer
            Return Microsoft.VisualBasic.Strings.Len(Expression)
        End Function

        ''' <summary>
        ''' Returns an integer containing either the number of characters in a string or the nominal number of bytes required to store a variable.
        ''' </summary>
        ''' <returns>
        ''' Returns an integer containing either the number of characters in a string or the nominal number of bytes required to store a variable.
        ''' </returns>
        ''' <param name="Expression">
        ''' Any valid String expression or variable name. If <paramref name="Expression" /> is of type Object, the Len function returns the size as it will be written to the file by the FilePut function.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function Len(Expression As Single) As Integer
            Return Microsoft.VisualBasic.Strings.Len(Expression)
        End Function

        ''' <summary>
        ''' Returns an integer containing either the number of characters in a string or the nominal number of bytes required to store a variable.
        ''' </summary>
        ''' <returns>
        ''' Returns an integer containing either the number of characters in a string or the nominal number of bytes required to store a variable.
        ''' </returns>
        ''' <param name="Expression">
        ''' Any valid String expression or variable name. If <paramref name="Expression" /> is of type Object, the Len function returns the size as it will be written to the file by the FilePut function.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function Len(Expression As Double) As Integer
            Return Microsoft.VisualBasic.Strings.Len(Expression)
        End Function

        ''' <summary>
        ''' Returns an integer containing either the number of characters in a string or the nominal number of bytes required to store a variable.
        ''' </summary>
        ''' <returns>
        ''' Returns an integer containing either the number of characters in a string or the nominal number of bytes required to store a variable.
        ''' </returns>
        ''' <param name="Expression">
        ''' Any valid String expression or variable name. If <paramref name="Expression" /> is of type Object, the Len function returns the size as it will be written to the file by the FilePut function.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function Len(Expression As DateTime) As Integer
            Return Microsoft.VisualBasic.Strings.Len(Expression)
        End Function

        ''' <summary>
        ''' Returns an integer containing either the number of characters in a string or the nominal number of bytes required to store a variable.
        ''' </summary>
        ''' <returns>
        ''' Returns an integer containing either the number of characters in a string or the nominal number of bytes required to store a variable.
        ''' </returns>
        ''' <param name="Expression">
        ''' Any valid String expression or variable name. If <paramref name="Expression" /> is of type Object, the Len function returns the size as it will be written to the file by the FilePut function.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function Len(Expression As Char) As Integer
            Return Microsoft.VisualBasic.Strings.Len(Expression)
        End Function

        ''' <summary>
        ''' Returns an integer containing either the number of characters in a string or the nominal number of bytes required to store a variable.
        ''' </summary>
        ''' <returns>
        ''' Returns an integer containing either the number of characters in a string or the nominal number of bytes required to store a variable.
        ''' </returns>
        ''' <param name="Expression">
        ''' Any valid String expression or variable name. If <paramref name="Expression" /> is of type Object, the Len function returns the size as it will be written to the file by the FilePut function.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function Len(Expression As String) As Integer
            Return Microsoft.VisualBasic.Strings.Len(Expression)
        End Function

        ''' <summary>
        ''' Returns an integer containing either the number of characters in a string or the nominal number of bytes required to store a variable.
        ''' </summary>
        ''' <returns>
        ''' Returns an integer containing either the number of characters in a string or the nominal number of bytes required to store a variable.
        ''' </returns>
        ''' <param name="Expression">
        ''' Any valid String expression or variable name. If <paramref name="Expression" /> is of type Object, the Len function returns the size as it will be written to the file by the FilePut function.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function Len(Expression As Object) As Integer
            Return Microsoft.VisualBasic.Strings.Len(Expression)
        End Function

        ''' <summary>
        ''' Returns a string in which a specified substring has been replaced with another substring a specified number of times.
        ''' </summary>
        ''' <returns>
        ''' If <paramref name="Find" /> is zero-length or Nothing, Replace returns copy of <paramref name="Expression" />. If <paramref name="Replace" /> is zero-length, Replace returns copy of <paramref name="Expression" /> with no occurrences of <paramref name="Find" />. If <paramref name="Expression" /> is zero-length or Nothing, or <paramref name="Start" /> is greater than length of <paramref name="Expression" />, Replace returns Nothing. If <paramref name="Count" /> is 0, Replace returns copy of <paramref name="Expression" />. 
        ''' </returns>
        ''' <param name="Expression">
        ''' Required. String expression containing substring to replace.
        ''' </param>
        ''' <param name="Find">
        ''' Required. Substring being searched for.
        ''' </param>
        ''' <param name="Replacement">
        ''' Required. Replacement substring.
        ''' </param>
        ''' <param name="Start">
        ''' Optional. Position within <paramref name="Expression" /> where substring search is to begin. If omitted, 1 is assumed.
        ''' </param>
        ''' <param name="Count">
        ''' Optional. Number of substring substitutions to perform. If omitted, the default value is –1, which means "make all possible substitutions."
        ''' </param>
        ''' <param name="Compare">
        ''' Optional. Numeric value indicating the kind of comparison to use when evaluating substrings. See Settings for values.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function Replace(Expression As String, Find As String, Replacement As String, Optional Start As Integer = 1, Optional Count As Integer = -1, Optional Compare As CompareMethod = CompareMethod.Binary) As String
            Return Microsoft.VisualBasic.Strings.Replace(Expression, Find, Replacement, Start, Count, Compare)
        End Function

        ''' <summary>
        ''' Returns a string consisting of the specified number of spaces.
        ''' </summary>
        ''' <returns>
        ''' Returns a string consisting of the specified number of spaces.
        ''' </returns>
        ''' <param name="Number">
        ''' Required. Integer expression. The number of spaces you want in the string.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function Space(Number As Integer) As String
            Return Microsoft.VisualBasic.Strings.Space(Number)
        End Function

        ''' <summary>
        ''' Returns a zero-based, one-dimensional array containing a specified number of substrings.
        ''' </summary>
        ''' <returns>String array. If <paramref name="Expression" /> is a zero-length string (""), Split returns a single-element array containing a zero-length string. If <paramref name="Delimiter" /> is a zero-length string, or if it does not appear anywhere in <paramref name="Expression" />, Split returns a single-element array containing the entire <paramref name="Expression" /> string.
        ''' </returns>
        ''' <param name="Expression">
        ''' Required. String expression containing substrings and delimiters.
        ''' </param>
        ''' <param name="Delimiter">
        ''' Optional. Any single character used to identify substring limits. If <paramref name="Delimiter" /> is omitted, the space character (" ") is assumed to be the delimiter.
        ''' </param>
        ''' <param name="Limit">
        ''' Optional. Maximum number of substrings into which the input string should be split. The default, –1, indicates that the input string should be split at every occurrence of the <paramref name="Delimiter" /> string.
        ''' </param>
        ''' <param name="Compare">
        ''' Optional. Numeric value indicating the comparison to use when evaluating substrings. See "Settings" for values.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function Split(Expression As String, Optional Delimiter As String = " ", Optional Limit As Integer = -1, Optional Compare As CompareMethod = CompareMethod.Binary) As String()
            Return Microsoft.VisualBasic.Strings.Split(Expression, Delimiter, Limit, Compare)
        End Function


        ''' <summary>
        ''' Returns a left-aligned string containing the specified string adjusted to the specified length.
        ''' </summary>
        ''' <returns>
        ''' Returns a left-aligned string containing the specified string adjusted to the specified length.
        ''' </returns>
        ''' <param name="Source">
        ''' Required. String expression. Name of string variable.
        ''' </param>
        ''' <param name="Length">
        ''' Required. Integer expression. Length of returned string.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function LSet(Source As String, Length As Integer) As String
            Return Microsoft.VisualBasic.Strings.LSet(Source, Length)
        End Function

        ''' <summary>
        ''' Returns a right-aligned string containing the specified string adjusted to the specified length. 
        ''' </summary>
        ''' <returns>
        ''' Returns a right-aligned string containing the specified string adjusted to the specified length. 
        ''' </returns>
        ''' <param name="Source">
        ''' Required. String expression. Name of string variable.
        ''' </param>
        ''' <param name="Length">
        ''' Required. Integer expression. Length of returned string.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function RSet(Source As String, Length As Integer) As String
            Return Microsoft.VisualBasic.Strings.RSet(Source, Length)
        End Function

        ''' <summary>
        ''' Returns a string or object consisting of the specified character repeated the specified number of times.
        ''' </summary>
        ''' <returns>
        ''' Returns a string or object consisting of the specified character repeated the specified number of times.
        ''' </returns>
        ''' <param name="Number">
        ''' Required. Integer expression. The length to the string to be returned.
        ''' </param>
        ''' <param name="Character">
        ''' Required. Any valid Char, String, or Object expression. Only the first character of the expression will be used. If Character is of type Object, it must contain either a Char or a String value. 
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
        ''' </PermissionSet>
        Public Function StrDup(Number As Integer, Character As Object) As Object
            Return Microsoft.VisualBasic.Strings.StrDup(Number, Character)
        End Function

        ''' <summary>
        ''' Returns a string or object consisting of the specified character repeated the specified number of times.
        ''' </summary>
        ''' <returns>
        ''' Returns a string or object consisting of the specified character repeated the specified number of times.
        ''' </returns>
        ''' <param name="Number">
        ''' Required. Integer expression. The length to the string to be returned.
        ''' </param>
        ''' <param name="Character">
        ''' Required. Any valid Char, String, or Object expression. Only the first character of the expression will be used. If Character is of type Object, it must contain either a Char or a String value. 
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
        ''' </PermissionSet>
        Public Function StrDup(Number As Integer, Character As Char) As String
            Return Microsoft.VisualBasic.Strings.StrDup(Number, Character)
        End Function

        ''' <summary>
        ''' Returns a string or object consisting of the specified character repeated the specified number of times.
        ''' </summary>
        ''' <returns>
        ''' Returns a string or object consisting of the specified character repeated the specified number of times.
        ''' </returns>
        ''' <param name="Number">
        ''' Required. Integer expression. The length to the string to be returned.
        ''' </param>
        ''' <param name="Character">
        ''' Required. Any valid Char, String, or Object expression. Only the first character of the expression will be used. If Character is of type Object, it must contain either a Char or a String value. 
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
        ''' </PermissionSet>
        Public Function StrDup(Number As Integer, Character As String) As String
            Return Microsoft.VisualBasic.Strings.StrDup(Number, Character)
        End Function

        ''' <summary>
        ''' Returns a string in which the character order of a specified string is reversed.
        ''' </summary>
        ''' <returns>
        ''' Returns a string in which the character order of a specified string is reversed.
        ''' </returns>
        ''' <param name="Expression">
        ''' Required. String expression whose characters are to be reversed. If <paramref name="Expression" /> is a zero-length string (""), a zero-length string is returned.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function StrReverse(Expression As String) As String
            Return Microsoft.VisualBasic.Strings.StrReverse(Expression)
        End Function

        ''' <summary>
        ''' Returns a string or character containing the specified string converted to uppercase.
        ''' </summary>
        ''' <returns>
        ''' Returns a string or character containing the specified string converted to uppercase.
        ''' </returns>
        ''' <param name="Value">
        ''' Required. Any valid String or Char expression.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function UCase(Value As String) As String
            Return Microsoft.VisualBasic.Strings.UCase(Value)
        End Function

        ''' <summary>
        ''' Returns a string or character containing the specified string converted to uppercase.
        ''' </summary>
        ''' <returns>
        ''' Returns a string or character containing the specified string converted to uppercase.
        ''' </returns>
        ''' <param name="Value">
        ''' Required. Any valid String or Char expression.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function UCase(Value As Char) As Char
            Return Microsoft.VisualBasic.Strings.UCase(Value)
        End Function


        ''' <summary>
        ''' Returns a string formatted according to instructions contained in a format String expression.
        ''' </summary>
        ''' <returns>
        ''' Returns a string formatted according to instructions contained in a format String expression.
        ''' </returns>
        ''' <param name="Expression">
        ''' Required. Any valid expression.
        ''' </param>
        ''' <param name="Style">
        ''' Optional. A valid named or user-defined format String expression.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
        ''' </PermissionSet>
        Public Function Format(Expression As Object, Optional Style As String = "") As String
            Return Microsoft.VisualBasic.Strings.Format(Expression, Style)
        End Function

        ''' <summary>
        ''' Returns an expression formatted as a currency value using the currency symbol defined in the system control panel.
        ''' </summary>
        ''' <returns>
        ''' Returns an expression formatted as a currency value using the currency symbol defined in the system control panel.
        ''' </returns>
        ''' <param name="Expression">
        ''' Required. Expression to be formatted.
        ''' </param>
        ''' <param name="NumDigitsAfterDecimal">
        ''' Optional. Numeric value indicating how many places are displayed to the right of the decimal. Default value is –1, which indicates that the computer's regional settings are used.
        ''' </param>
        ''' <param name="IncludeLeadingDigit">
        ''' Optional. TriState enumeration that indicates whether or not a leading zero is displayed for fractional values. See "Settings" for values.
        ''' </param>
        ''' <param name="UseParensForNegativeNumbers">
        ''' Optional. TriState enumeration that indicates whether or not to place negative values within parentheses. See "Settings" for values.
        ''' </param>
        ''' <param name="GroupDigits">
        ''' Optional. TriState enumeration that indicates whether or not numbers are grouped using the group delimiter specified in the computer's regional settings. See "Settings" for values.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
        ''' </PermissionSet>
        Public Function FormatCurrency(Expression As Object, Optional NumDigitsAfterDecimal As Integer = -1, Optional IncludeLeadingDigit As TriState = TriState.UseDefault, Optional UseParensForNegativeNumbers As TriState = TriState.UseDefault, Optional GroupDigits As TriState = TriState.UseDefault) As String
            Return Microsoft.VisualBasic.Strings.FormatCurrency(Expression, NumDigitsAfterDecimal, IncludeLeadingDigit, UseParensForNegativeNumbers, GroupDigits)
        End Function

        ''' <summary>
        ''' Returns a string expression representing a date/time value.
        ''' </summary>
        ''' <returns>
        ''' Returns a string expression representing a date/time value.
        ''' </returns>
        ''' <param name="Expression">
        ''' Required. Date expression to be formatted.
        ''' </param>
        ''' <param name="NamedFormat">
        ''' Optional. Numeric value that indicates the date/time format used. If omitted, DateFormat.GeneralDate is used.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function FormatDateTime(Expression As DateTime, Optional NamedFormat As DateFormat = DateFormat.GeneralDate) As String
            Return Microsoft.VisualBasic.Strings.FormatDateTime(Expression, NamedFormat)
        End Function

        ''' <summary>
        ''' Returns an expression formatted as a number.
        ''' </summary>
        ''' <returns>
        ''' Returns an expression formatted as a number.
        ''' </returns>
        ''' <param name="Expression">
        ''' Required. Expression to be formatted.
        ''' </param>
        ''' <param name="NumDigitsAfterDecimal">
        ''' Optional. Numeric value indicating how many places are displayed to the right of the decimal. The default value is –1, which indicates that the computer's regional settings are used.
        ''' </param>
        ''' <param name="IncludeLeadingDigit">
        ''' Optional. TriState constant that indicates whether a leading 0 is displayed for fractional values. See "Settings" for values.
        ''' </param>
        ''' <param name="UseParensForNegativeNumbers">
        ''' Optional. TriState constant that indicates whether to place negative values within parentheses. See "Settings" for values.
        ''' </param>
        ''' <param name="GroupDigits">
        ''' Optional. TriState constant that indicates whether or not numbers are grouped using the group delimiter specified in the locale settings. See "Settings" for values.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
        ''' </PermissionSet>
        Public Function FormatNumber(Expression As Object, Optional NumDigitsAfterDecimal As Integer = -1, Optional IncludeLeadingDigit As TriState = TriState.UseDefault, Optional UseParensForNegativeNumbers As TriState = TriState.UseDefault, Optional GroupDigits As TriState = TriState.UseDefault) As String
            Return Microsoft.VisualBasic.Strings.FormatNumber(Expression, NumDigitsAfterDecimal, IncludeLeadingDigit, UseParensForNegativeNumbers, GroupDigits)
        End Function

        ''' <summary>
        ''' Returns an expression formatted as a percentage (that is, multiplied by 100) with a trailing % character.
        ''' </summary>
        ''' <returns>
        ''' Returns an expression formatted as a percentage (that is, multiplied by 100) with a trailing % character.
        ''' </returns>
        ''' <param name="Expression">
        ''' Required. Expression to be formatted.
        ''' </param>
        ''' <param name="NumDigitsAfterDecimal">
        ''' Optional. Numeric value indicating how many places to the right of the decimal are displayed. Default value is –1, which indicates that the locale settings are used.
        ''' </param>
        ''' <param name="IncludeLeadingDigit">
        ''' Optional. TriState constant that indicates whether or not a leading zero displays for fractional values. See "Settings" for values.
        ''' </param>
        ''' <param name="UseParensForNegativeNumbers">
        ''' Optional. TriState constant that indicates whether or not to place negative values within parentheses. See "Settings" for values.
        ''' </param>
        ''' <param name="GroupDigits">
        ''' Optional. TriState constant that indicates whether or not numbers are grouped using the group delimiter specified in the locale settings. See "Settings" for values.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        ''' <PermissionSet>
        ''' <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
        ''' </PermissionSet>
        Public Function FormatPercent(Expression As Object, Optional NumDigitsAfterDecimal As Integer = -1, Optional IncludeLeadingDigit As TriState = TriState.UseDefault, Optional UseParensForNegativeNumbers As TriState = TriState.UseDefault, Optional GroupDigits As TriState = TriState.UseDefault) As String
            Return Microsoft.VisualBasic.Strings.FormatPercent(Expression, NumDigitsAfterDecimal, IncludeLeadingDigit, UseParensForNegativeNumbers, GroupDigits)
        End Function

        ''' <summary>
        ''' Returns a Char value representing the character from the specified index in the supplied string.
        ''' </summary>
        ''' <returns>
        ''' Returns a Char value representing the character from the specified index in the supplied string.
        ''' </returns>
        ''' <param name="str">
        ''' Required. Any valid String expression.
        ''' </param>
        ''' <param name="Index">
        ''' Required. Integer expression. The (1-based) index of the character in <paramref name="str" /> to be returned.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function GetChar(str As String, Index As Integer) As Char
            Return Microsoft.VisualBasic.Strings.GetChar(str, Index)
        End Function

        ''' <summary>
        ''' Returns a string containing a specified number of characters from the left side of a string.
        ''' </summary>
        ''' <returns>
        ''' Returns a string containing a specified number of characters from the left side of a string.
        ''' </returns>
        ''' <param name="str">
        ''' Required. String expression from which the leftmost characters are returned.
        ''' </param>
        ''' <param name="Length">
        ''' Required. Integer expression. Numeric expression indicating how many characters to return. If 0, a zero-length string ("") is returned. If greater than or equal to the number of characters in <paramref name="str" />, the entire string is returned.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function Left(str As String, Length As Integer) As String
            Return Microsoft.VisualBasic.Strings.Left(str, Length)
        End Function

        ''' <summary>
        ''' Returns a string containing a copy of a specified string with no leading spaces (LTrim), no trailing spaces (RTrim), or no leading or trailing spaces (Trim).
        ''' </summary>
        ''' <returns>
        ''' Returns a string containing a copy of a specified string with no leading spaces (LTrim), no trailing spaces (RTrim), or no leading or trailing spaces (Trim).
        ''' </returns>
        ''' <param name="str">
        ''' Required. Any valid String expression.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function LTrim(str As String) As String
            Return Microsoft.VisualBasic.Strings.LTrim(str)
        End Function

        ''' <summary>
        ''' Returns a string containing a specified number of characters from a string.
        ''' </summary>
        ''' <returns>
        ''' Returns a string containing a specified number of characters from a string.
        ''' </returns>
        ''' <param name="str">
        ''' Required. String expression from which characters are returned.
        ''' </param>
        ''' <param name="Start">
        ''' Required. Integer expression. Starting position of the characters to return. If <paramref name="Start" /> is greater than the number of characters in <paramref name="str" />, the Mid function returns a zero-length string (""). <paramref name="Start" /> is one based.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function Mid(str As String, Start As Integer) As String
            Return Microsoft.VisualBasic.Strings.Mid(str, Start)
        End Function

        ''' <summary>
        ''' Returns a string containing a specified number of characters from a string.
        ''' </summary>
        ''' <returns>
        ''' Returns a string containing a specified number of characters from a string.
        ''' </returns>
        ''' <param name="str">
        ''' Required. String expression from which characters are returned.
        ''' </param>
        ''' <param name="Start">
        ''' Required. Integer expression. Starting position of the characters to return. If <paramref name="Start" /> is greater than the number of characters in <paramref name="str" />, the Mid function returns a zero-length string (""). <paramref name="Start" /> is one based.
        ''' </param>
        ''' <param name="Length">
        ''' Optional. Integer expression. Number of characters to return. If omitted or if there are fewer than <paramref name="Length" /> characters in the text (including the character at position <paramref name="Start" />), all characters from the start position to the end of the string are returned.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function Mid(str As String, Start As Integer, Length As Integer) As String
            Return Microsoft.VisualBasic.Strings.Mid(str, Start, Length)
        End Function

        ''' <summary>
        ''' Returns a string containing a specified number of characters from the right side of a string.
        ''' </summary>
        ''' <returns>
        ''' Returns a string containing a specified number of characters from the right side of a string.
        ''' </returns>
        ''' <param name="str">
        ''' Required. String expression from which the rightmost characters are returned.
        ''' </param>
        ''' <param name="Length">
        ''' Required. Integer. Numeric expression indicating how many characters to return. If 0, a zero-length string ("") is returned. If greater than or equal to the number of characters in <paramref name="str" />, the entire string is returned.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function Right(str As String, Length As Integer) As String
            Return Microsoft.VisualBasic.Strings.Right(str, Length)
        End Function

        ''' <summary>
        ''' Returns a string containing a copy of a specified string with no leading spaces (LTrim), no trailing spaces (RTrim), or no leading or trailing spaces (Trim).
        ''' </summary>
        ''' <returns>
        ''' Returns a string containing a copy of a specified string with no leading spaces (LTrim), no trailing spaces (RTrim), or no leading or trailing spaces (Trim).
        ''' </returns>
        ''' <param name="str">
        ''' Required. Any valid String expression.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function RTrim(str As String) As String
            Return Microsoft.VisualBasic.Strings.RTrim(str)
        End Function

        ''' <summary>
        ''' Returns a string containing a copy of a specified string with no leading spaces (LTrim), no trailing spaces (RTrim), or no leading or trailing spaces (Trim).
        ''' </summary>
        ''' <returns>
        ''' Returns a string containing a copy of a specified string with no leading spaces (LTrim), no trailing spaces (RTrim), or no leading or trailing spaces (Trim).
        ''' </returns>
        ''' <param name="str">
        ''' Required. Any valid String expression.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function Trim(str As String) As String
            Return Microsoft.VisualBasic.Strings.Trim(str)
        End Function

        ''' <summary>
        ''' Returns -1, 0, or 1, based on the result of a string comparison. 
        ''' </summary>
        ''' <returns>
        ''' If <paramref name="String1" /> sorts ahead of <paramref name="String2" />, StrComp returns -1. If <paramref name="String1" /> is equal to <paramref name="String2" />, StrComp returns  0. If <paramref name="String1" /> sorts after <paramref name="String2" />, StrComp returns  1. 
        ''' </returns>
        ''' <param name="String1">
        ''' Required. Any valid String expression.
        ''' </param>
        ''' <param name="String2">
        ''' Required. Any valid String expression.
        ''' </param>
        ''' <param name="Compare">
        ''' Optional. Specifies the type of string comparison. If <paramref name="Compare" /> is omitted, the Option Compare setting determines the type of comparison.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function StrComp(String1 As String, String2 As String, Optional Compare As CompareMethod = CompareMethod.Binary) As Integer
            Return Microsoft.VisualBasic.Strings.StrComp(String1, String2, Compare)
        End Function


        ''' <summary>
        ''' Returns a string converted as specified.
        ''' </summary>
        ''' <returns>
        ''' Returns a string converted as specified.
        ''' </returns>
        ''' <param name="str">
        ''' Required. String expression to be converted.
        ''' </param>
        ''' <param name="Conversion">
        ''' Required. VbStrConv Enumeration member. The enumeration value specifying the type of conversion to perform.
        ''' </param>
        ''' <param name="LocaleID">
        ''' Optional. The LocaleID value, if different from the system LocaleID value. (The system LocaleID value is the default.)
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function StrConv(str As String, Conversion As VbStrConv, Optional LocaleID As Integer = 0) As String
            Return Microsoft.VisualBasic.Strings.StrConv(str, Conversion, LocaleID)
        End Function
    End Module

    Public Module VBMath
        ''' <summary>
        ''' Returns a random number of type Single.
        ''' </summary>
        ''' <returns>
        ''' If number is less than zero, Rnd generates the same number every time, using <paramref name="Number" /> as the seed. If number is greater than zero, Rnd generates the next random number in the sequence. If number is equal to zero, Rnd generates the most recently generated number. If number is not supplied, Rnd generates the next random number in the sequence.
        ''' </returns>
        ''' <filterpriority>1</filterpriority>
        Public Function Rnd() As Single
            Return Microsoft.VisualBasic.VBMath.Rnd()
        End Function

        ''' <summary>
        ''' Returns a random number of type Single.
        ''' </summary>
        ''' <returns>
        ''' If number is less than zero, Rnd generates the same number every time, using <paramref name="Number" /> as the seed. If number is greater than zero, Rnd generates the next random number in the sequence. If number is equal to zero, Rnd generates the most recently generated number. If number is not supplied, Rnd generates the next random number in the sequence.
        ''' </returns>
        ''' <param name="Number">
        ''' Optional. A Single value or any valid Single expression.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Function Rnd(Number As Single) As Single
            Return Microsoft.VisualBasic.VBMath.Rnd(Number)
        End Function

        ''' <summary>
        ''' Initializes the random-number generator.
        ''' </summary>
        ''' <filterpriority>1</filterpriority>
        Public Sub Randomize()
            Microsoft.VisualBasic.VBMath.Randomize()
        End Sub

        ''' <summary>
        ''' Initializes the random-number generator.
        ''' </summary>
        ''' <param name="Number">
        ''' Optional. An Object or any valid numeric expression.
        ''' </param>
        ''' <filterpriority>1</filterpriority>
        Public Sub Randomize(Number As Double)
            Microsoft.VisualBasic.VBMath.Randomize(Number)
        End Sub
    End Module
End Namespace
