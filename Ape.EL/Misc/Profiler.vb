﻿
Imports System
Imports System.IO

Namespace Misc
    Public Class Profiler
        Private myStopwatch As New System.Diagnostics.Stopwatch

        Protected myCategory As String

        Protected myLogFilename As String

        ''' <summary>
        ''' Get the latest timespan after Stop is executed succcessfully.
        ''' </summary>
        Property LastTimespan As TimeSpan
            Get
                Return _LastTimespan
            End Get
            Set(value As TimeSpan)
                _LastTimespan = value
            End Set
        End Property
        Private _LastTimespan As TimeSpan

        Public Sub New()
        End Sub

        Public Sub New(category As String, Optional logFilename As String = "")
            Me.myCategory = category

            If Not String.IsNullOrEmpty(logFilename) Then
                Me.myLogFilename = logFilename
            Else
                Dim basePath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop)
                Dim tried As Integer = 0
                While basePath.IsEmpty() AndAlso tried <= 1
                    Try
                        tried += 1
                        basePath = System.IO.Directory.GetParent(Ape.EL.Utility.General.GetStartUpPathDirectory()).Parent.FullName
                    Catch
                    End Try
                End While
                Me.myLogFilename = String.Format("{0}\Profiler\Prf{1}.txt", basePath, category)
            End If
        End Sub

        ''' <summary>
        ''' Start counting.
        ''' </summary>
        Public Sub Start()
            Me.myStopwatch.Reset()
            Me.myStopwatch.Start()
        End Sub

        ''' <summary>
        ''' Get final timespan and write to log file.
        ''' </summary>
        Public Sub [Stop](description As String)
            Me.Stop()

            If Me.myLogFilename.Length = 0 OrElse myCategory.Trim.IsEmpty Then
                Return
            End If

            Utility.General.MakePath(Path.GetDirectoryName(myLogFilename))

            Dim value As String = String.Format("{0},{1},{2},0:{3}:{4}:{5} | {6}", New Object() {Now.ToString("dd/MM/yyyy hh:mm:ss"), Me.myCategory.Replace(vbCrLf, " ").Trim(), description.Replace(vbCrLf, " ").Trim(), _LastTimespan.Minutes, _LastTimespan.Seconds, _LastTimespan.Milliseconds, _LastTimespan.TotalMilliseconds})
            Try
                Using streamWriter As StreamWriter = File.AppendText(Me.myLogFilename)
                    streamWriter.WriteLine(value)
                    streamWriter.Flush()
                    streamWriter.Close()
                End Using
            Catch ex_B3 As Exception
            End Try
        End Sub

        ''' <summary>
        ''' Get final timespan on LastTimeSpan property.
        ''' </summary>
        Public Sub [Stop]()
            myStopwatch.Stop()
            _LastTimespan = myStopwatch.Elapsed
        End Sub

        Public Sub OpenLogFile()
            Try
                If System.IO.File.Exists(myLogFilename) Then
                    Process.Start(myLogFilename)
                End If
            Catch ex As Exception
                Debug.Assert(False, ex.ToString)
            End Try
        End Sub

        ''' <summary>
        ''' Run Profiler and generate log file.
        ''' </summary>
        Public Shared Function Execute(ByVal method As EventHandler) As Profiler
            Dim p As New Profiler("Execute")
            p.Start()
            method.Invoke(Nothing, Nothing)
            p.Stop("")
            Return p
        End Function

        ''' <summary>
        ''' Profiler that support IDisposable which directly write to log file.
        ''' </summary>
        Public Class Run
            Inherits Profiler
            Implements IDisposable

            Public Property Category As String
                Get
                    Return MyBase.myCategory
                End Get
                Set(value As String)
                    MyBase.myCategory = value
                End Set
            End Property

            Public Property Description As String
                Get
                    Return _Description
                End Get
                Set(value As String)
                    _Description = value
                End Set
            End Property
            Private _Description As String

            Public Sub New(ByVal category As String, ByVal description As String)
                MyBase.New(category)
                MyBase.Start()
                _Description = description
            End Sub

            Public Function GetLogFilename() As String
                Return MyBase.myLogFilename
            End Function

#Region "IDisposable Support"
            Private disposedValue As Boolean
            Protected Overridable Sub Dispose(disposing As Boolean)
                If Not Me.disposedValue Then
                    If disposing Then
                        MyBase.Stop(_Description)
                    End If
                End If
                Me.disposedValue = True
            End Sub
            Public Sub Dispose() Implements IDisposable.Dispose
                Dispose(True)
                GC.SuppressFinalize(Me)
            End Sub
#End Region

        End Class
    End Class
End Namespace
