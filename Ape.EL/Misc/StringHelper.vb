﻿
Imports System
Imports System.Collections
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Drawing

Namespace Misc
    Public Class StringHelper
        Public Shared Function IListToDelimiterString(list As IList, delimiter As String) As String
            If list Is Nothing OrElse delimiter Is Nothing Then
                Throw New ArgumentNullException()
            End If
            Dim stringBuilder As StringBuilder = New StringBuilder()
            For i As Integer = 0 To list.Count - 1
                stringBuilder.Append(list(i))
                If i <> list.Count - 1 Then
                    stringBuilder.Append(delimiter)
                End If
            Next
            Return stringBuilder.ToString()
        End Function

        Public Shared Function IListToCommaString(list As IList) As String
            Return StringHelper.IListToDelimiterString(list, ",")
        End Function

        Public Shared Function ArrayListToDelimiterString(arrayList As ArrayList, delimiter As String) As String
            If arrayList Is Nothing OrElse delimiter Is Nothing Then
                Throw New ArgumentNullException()
            End If
            Dim stringBuilder As StringBuilder = New StringBuilder()
            For i As Integer = 0 To arrayList.Count - 1
                stringBuilder.Append(arrayList(i))
                If i <> arrayList.Count - 1 Then
                    stringBuilder.Append(delimiter)
                End If
            Next
            Return stringBuilder.ToString()
        End Function

        Public Shared Function ArrayListToCommaString(arrayList As ArrayList) As String
            Return StringHelper.ArrayListToDelimiterString(arrayList, ",")
        End Function

        Public Shared Function StringArrayToDelimiterString(strings As String(), delimiter As String) As String
            If strings Is Nothing OrElse delimiter Is Nothing Then
                Throw New ArgumentNullException()
            End If
            Dim stringBuilder As StringBuilder = New StringBuilder()
            For i As Integer = 0 To strings.Length - 1
                stringBuilder.Append(strings(i))
                If i <> strings.Length - 1 Then
                    stringBuilder.Append(delimiter)
                End If
            Next
            Return stringBuilder.ToString()
        End Function

        Public Shared Function StringArrayToMultiLineString(strings As String()) As String
            Return StringHelper.StringArrayToDelimiterString(strings, Environment.NewLine)
        End Function

        Public Shared Function ToCSharpString(aStr As String) As String
            If aStr Is Nothing Then
                Throw New ArgumentNullException()
            End If
            aStr = aStr.Replace("\", "\\")
            Return aStr.Replace("""", "\""")
        End Function

        Public Shared Function ToSingleQuoteString(aStr As String) As String
            If aStr Is Nothing Then
                Throw New ArgumentNullException()
            End If
            Return aStr.Replace("'", "''")
        End Function

        Public Shared Function ToDoubleQuoteString(aStr As String) As String
            If aStr Is Nothing Then
                Throw New ArgumentNullException()
            End If
            Return aStr.Replace("""", """""")
        End Function

        Public Shared Function AddDoubleQuoteString(aStr As String) As String
            If aStr Is Nothing Then
                Throw New ArgumentNullException()
            End If
            Return String.Format("""{0}""", aStr.Replace("""", """"""))
        End Function

        Public Shared Function ToCSVString(aStr As String) As String
            If aStr Is Nothing Then
                Throw New ArgumentNullException()
            End If
            If aStr.IndexOf(",") >= 0 OrElse aStr.IndexOf("""") >= 0 Then
                Return String.Format("""{0}""", aStr.Replace("""", """"""))
            End If
            Return aStr
        End Function

        Public Shared Function ToMultiLineString(s As String) As String()
            If s Is Nothing Then
                Throw New ArgumentNullException()
            End If
            Using stringReader As StringReader = New StringReader(s)
                Dim arrayList As ArrayList = New ArrayList()
                While stringReader.Peek() >= 0
                    arrayList.Add(stringReader.ReadLine())
                End While
                Return CType(arrayList.ToArray(Type.[GetType]("System.String")), String())
            End Using
        End Function

        Public Shared Function ToList2String(ss As String()) As String
            If ss Is Nothing Then
                Throw New ArgumentNullException()
            End If
            If ss.Length = 0 Then
                Return ""
            End If
            Dim stringBuilder As StringBuilder = New StringBuilder()
            For i As Integer = 0 To ss.Length - 1
                Dim value As String = ss(i)
                stringBuilder.Append(value)
                stringBuilder.Append(vbLf & vbCr)
            Next
            Return stringBuilder.ToString(0, stringBuilder.Length - 2)
        End Function

        Public Shared Function CompareVersionString(ver1 As String, ver2 As String) As Integer
            If ver1 Is Nothing OrElse ver2 Is Nothing Then
                Throw New ArgumentNullException()
            End If
            Dim array As String() = ver1.Split(New Char() {"."})
            Dim array2 As String() = ver2.Split(New Char() {"."})
            Dim num As Integer = array.Length
            If num > array2.Length Then
                num = array2.Length
            End If
            For i As Integer = 0 To num - 1
                Dim num2 As Integer
                If array(i).Length = 0 Then
                    num2 = 0
                Else
                    num2 = Convert.ToInt32(array(i))
                End If
                Dim num3 As Integer
                If array2(i).Length = 0 Then
                    num3 = 0
                Else
                    num3 = Convert.ToInt32(array2(i))
                End If
                If num2 <> num3 Then
                    Return num2 - num3
                End If
            Next
            If array.Length > num Then
                Return Convert.ToInt32(array(num))
            End If
            If array2.Length > num Then
                Return -Convert.ToInt32(array2(num))
            End If
            Return 0
        End Function

        Public Shared Function TabDelimittedTextToDataTable(tabText As String) As DataTable
            Dim length As Integer = tabText.Length
            Dim num As Integer = length - 1
            Dim i As Integer = 0
            Dim num2 As Integer = 1
            Dim num3 As Integer = 1
            While i < length
                Dim c As Char = tabText(i)
                If c = vbTab Then
                    num2 += 1
                    If i < num AndAlso tabText(i + 1) = """" Then
                        i += 2
                        While i < length
                            c = tabText(i)
                            If c = """" AndAlso i < num AndAlso tabText(i + 1) <> """" Then
                                Exit While
                            End If
                            If c = vbTab Then
                                i -= 1
                                Exit While
                            End If
                            i += 1
                        End While
                    End If
                Else
                    If c = vbLf Then
                        If i < num AndAlso tabText(i + 1) = vbCr Then
                            i += 1
                        End If
                        If num2 > num3 Then
                            num3 = num2
                        End If
                        num2 = 1
                    End If
                End If
                i += 1
            End While
            Dim dataTable As DataTable = New DataTable()
            i = 0
            While i < num3
                dataTable.Columns.Add(i.ToString(), GetType(String))
                i += 1
            End While
            i = 0
            Dim stringBuilder As StringBuilder = New StringBuilder()
            Dim num4 As Integer = 0
            Dim dataRow As DataRow = dataTable.NewRow()
            While i < length
                Dim c2 As Char = tabText(i)
                If c2 = """" Then
                    i += 1
                    While i < length
                        c2 = tabText(i)
                        If c2 = """" Then
                            If i < num AndAlso tabText(i + 1) <> """" Then
                                Exit While
                            End If
                            i += 1
                        Else
                            If c2 = vbTab Then
                                i -= 1
                                Exit While
                            End If
                        End If
                        stringBuilder.Append(c2)
                        i += 1
                    End While
                Else
                    If c2 = vbTab Then
                        If num4 < num3 Then
                            Dim arg_176_0 As DataRow = dataRow
                            Dim expr_16A As Integer = num4
                            num4 = expr_16A + 1
                            arg_176_0(expr_16A) = stringBuilder.ToString()
                            stringBuilder = New StringBuilder()
                        End If
                        If i < num AndAlso tabText(i + 1) = """" Then
                            i += 2
                            While i < length
                                c2 = tabText(i)
                                If c2 = """" Then
                                    If i < num AndAlso tabText(i + 1) <> """" Then
                                        Exit While
                                    End If
                                    i += 1
                                Else
                                    If c2 = vbTab Then
                                        i -= 1
                                        Exit While
                                    End If
                                End If
                                stringBuilder.Append(c2)
                                i += 1
                            End While
                        End If
                    Else
                        If c2 = vbLf Then
                            If num4 < num3 Then
                                Dim arg_201_0 As DataRow = dataRow
                                Dim expr_1F5 As Integer = num4
                                num4 = expr_1F5 + 1
                                arg_201_0(expr_1F5) = stringBuilder.ToString()
                                stringBuilder = New StringBuilder()
                            End If
                            dataTable.Rows.Add(dataRow)
                            dataRow = dataTable.NewRow()
                            num4 = 0
                        Else
                            If c2 <> vbCr Then
                                stringBuilder.Append(c2)
                            End If
                        End If
                    End If
                End If
                i += 1
            End While
            Return dataTable
        End Function

        Public Shared Function ConvertToValidFilename(fileName As String) As String
            Dim invalidFileNameChars As Char() = Path.GetInvalidFileNameChars()
            Dim stringBuilder As StringBuilder = New StringBuilder()
            For i As Integer = 0 To fileName.Length - 1
                Dim c As Char = fileName(i)
                Dim flag As Boolean = True
                Dim array As Char() = invalidFileNameChars
                For j As Integer = 0 To array.Length - 1
                    Dim c2 As Char = array(j)
                    If c = c2 Then
                        flag = False
                        Exit For
                    End If
                Next
                If flag Then
                    stringBuilder.Append(c)
                End If
            Next
            Return stringBuilder.ToString()
        End Function

        Public Shared Function TruncateString(text As String, length As Integer, g As Graphics, font As Font) As String
            Dim chars As Array = text.ToCharArray()
            Dim output As New List(Of String)()
            Dim wordSize As SizeF
            Dim outputLength As Double = 0
            Dim addEllipse As Boolean = False
            Dim size As SizeF = g.MeasureString(text.ToString(), font)
            For Each c As Char In chars
                wordSize = g.MeasureString(c.ToString(), font)
                If outputLength + wordSize.Width <= length + length / 2.5 Then
                    output.Add(c.ToString())
                    outputLength += Convert.ToDouble(wordSize.Width)
                Else
                    addEllipse = True
                    Exit For
                End If
            Next

            Return [String].Join("", output.ToArray()) + (If((addEllipse), "...", String.Empty))
        End Function

        Public Shared Function IsAlphaNum(ByVal strInputText As String) As Boolean
            Dim IsAlpha As Boolean = False
            If System.Text.RegularExpressions.Regex.IsMatch(strInputText, "^[a-zA-Z0-9]+$") Then
                IsAlpha = True
            Else
                IsAlpha = False
            End If
            Return IsAlpha
        End Function
    End Class
End Namespace
