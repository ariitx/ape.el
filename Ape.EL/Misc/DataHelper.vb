﻿Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports Ape.EL.Utility.General
Imports Ape.EL.App.General
Imports Ape.EL.WinForm.General
Imports Ape.EL.Data
Imports Ape.EL.Data.DbUtility

Namespace Misc
    <System.Diagnostics.DebuggerStepThrough()> _
    Public Class DataHelper
        '        Private ReadOnly lic As Boolean = Internal.CheckMyLicense

        Public Shared Function ConvertToDataView(ByVal dt As DataTable) As DataView
            Dim dv As New DataView(dt)
            Return dv
        End Function

        Public Shared Function ConvertToDataTable(ByVal dv As DataView) As DataTable
            Dim dt As New DataTable()
            dt = dv.ToTable()
            Return dt
        End Function

        Public Shared Function ConvertExcelToDataTable(ByVal filepath As String, _
                                                Optional ByVal sheetname As String = "Sheet1", _
                                                Optional ByVal extendedprop As String = """Excel 12.0;HDR=YES;IMEX=1;ImportMixedTypes=Text;""") As DataTable
            If Not System.IO.File.Exists(filepath) Then
                Throw New System.IO.FileNotFoundException("Invalid filepath.")
            ElseIf String.IsNullOrEmpty(sheetname) Then
                Throw New Exception("You haven't assigned sheetname parameter.")
            End If

            Dim dt As New DataTable
            Dim olecon As OleDbConnection
            Dim olecomm As OleDbCommand
            Dim oleadpt As OleDbDataAdapter
            Dim connExcelstring As String
            Try

                connExcelstring = String.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties={1}", filepath, extendedprop)

                'Read From Excel
                olecon = New OleDbConnection
                olecon.ConnectionString = connExcelstring
                olecomm = New OleDbCommand
                olecomm.CommandText = "Select * from [" & sheetname & "$]"
                olecomm.Connection = olecon
                oleadpt = New OleDbDataAdapter(olecomm)
                olecon.Open()
                oleadpt.Fill(dt)

                Dim strColumns As String = ""
                Dim inTotal As Integer = dt.Columns.Count - 1

                For introwTotal = 0 To inTotal
                    If introwTotal <= inTotal Then
                        If dt.Columns(introwTotal).ToString.Substring(0, 1) = "F" And IsNumeric(dt.Columns(introwTotal).ToString.Substring(1)) Then
                            dt.Columns.RemoveAt(introwTotal)
                            inTotal = inTotal - 1
                            introwTotal = introwTotal - 1
                        End If
                    End If
                Next

                olecon.Close()

                Return dt
            Catch ex As Exception
                Throw ex
            End Try

            Return dt
        End Function

        Public Shared Function ConvertExcelToDataView(ByVal filepath As String, _
                                                Optional ByVal sheetname As String = "Sheet1", _
                                                Optional ByVal extendedprop As String = """Excel 12.0;HDR=YES;IMEX=1;ImportMixedTypes=Text;""") As DataView
            Return ConvertToDataView(ConvertExcelToDataTable(filepath, sheetname, extendedprop))
        End Function

        Public Shared Function CountDataRowView(ByVal Dataview As DataView, _
                                                ByVal ColumnName As String, _
                                                ByVal Filter As Object) As Integer
            Return CountDataRowView(ConvertToDataTable(Dataview), _
                                    ColumnName, _
                                    Filter)
        End Function

        Public Shared Function CountDataRowView(ByVal Datatable As DataTable, _
                                        ByVal ColumnName As String, _
                                        ByVal Filter As Object) As Integer
            Try
                Return Datatable.Select(String.Format("{0}='{1}'", ColumnName, Filter)).Count
            Catch ex As Exception
            End Try
            Return 0
        End Function

        Public Shared Function FindDataRowView(ByVal Dataview As DataView, _
                                               ByVal ColumnName As String, _
                                               ByVal Filter As Object) As DataRowView
            Try
                Dataview.Sort = ColumnName
                Dim index As Integer = Dataview.Find(Filter)

                If index <> -1 Then
                    Return Dataview(index)
                End If
            Catch ex As Exception
            End Try
            Return Nothing
        End Function

        Public Shared Function FindDataRowView(ByVal Datatable As DataTable, _
                                       ByVal ColumnName As String, _
                                       ByVal Filter As Object) As DataRowView
            Return FindDataRowView(ConvertToDataView(Datatable), _
                                   ColumnName, _
                                   Filter)
        End Function
    End Class

End Namespace
