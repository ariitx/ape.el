﻿Namespace Misc
    Public Class ListHelper
        Public Shared Sub SwapList(Of T)(ByVal myList As List(Of T), ByVal id1 As Integer, ByVal id2 As Integer)
            If myList Is Nothing Then Return
            If myList.Count = 0 Then Return
            If id1 = id2 Then Return
            If id1 > myList.Count + 1 Or id2 > myList.Count + 1 Or id1 < 0 Or id2 < 0 Then Return

            Dim arrayRowList1 As New ArrayList
            Dim arrayRowList2 As New ArrayList

            arrayRowList1.Add(myList(id1))
            arrayRowList2.Add(myList(id2))

            myList(id2) = arrayRowList1(0)
            myList(id1) = arrayRowList2(0)
        End Sub
    End Class
End Namespace
