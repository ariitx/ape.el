﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.HtmlControls
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Net.Mail
Imports System.Net
Imports System.Text.RegularExpressions

Namespace Misc
    Public Class Email
        ''' <summary>
        ''' Wrapper for MailMessage class. Send email to one recipient.
        ''' </summary>
        ''' <param name="strRecipient">Recipient's email address.</param>
        ''' <param name="strSubj">Email subject.</param>
        ''' <param name="strMessage">Email content.</param>
        ''' <returns>Email sending status</returns>
        Public Shared Function SendEmail(strSenderAddress As String,
                                  strSenderPass As String,
                                  strSenderHost As String,
                                  bSenderEnableSsl As Boolean,
                                  intSenderPort As Integer,
                                  strRecipient As String,
                                  strCC As String,
                                  strBCC As String,
                                  strSubj As String,
                                  strMessage As String) As Boolean
            Return SendEmail(strSenderAddress, strSenderPass, strSenderHost, bSenderEnableSsl, intSenderPort, strRecipient, strCC, strBCC, strSubj, strMessage, Nothing)
        End Function

        ''' <summary>
        ''' Wrapper for MailMessage class. Send email to one recipient.
        ''' </summary>
        ''' <param name="strRecipient">Recipient's email address.</param>
        ''' <param name="strSubj">Email subject.</param>
        ''' <param name="strMessage">Email content.</param>
        ''' <returns>Email sending status</returns>
        Public Shared Function SendEmail(strSenderAddress As String,
                                  strSenderPass As String,
                                  strSenderHost As String,
                                  bSenderEnableSsl As Boolean,
                                  intSenderPort As Integer,
                                  strRecipient As String,
                                  strCC As String,
                                  strBCC As String,
                                  strSubj As String,
                                  strMessage As String,
                                  ParamArray attachments As Attachment()) As Boolean
            Try
                Dim mm As New MailMessage()
                mm.From = New MailAddress(strSenderAddress)
                mm.Subject = strSubj
                mm.Body = strMessage
                mm.IsBodyHtml = True
                mm.[To].Add(strRecipient)
                If strCC <> "" Then
                    mm.CC.Add(strCC)
                End If
                If strBCC <> "" Then
                    mm.Bcc.Add(strBCC)
                End If
                If attachments IsNot Nothing Then
                    For j As Integer = 0 To attachments.Length - 1
                        Dim item As Attachment = attachments(j)
                        mm.Attachments.Add(item)
                    Next
                End If

                Dim nc As New NetworkCredential()
                nc.UserName = mm.From.Address
                nc.Password = strSenderPass

                Dim smtp As New SmtpClient()
                smtp.Host = strSenderHost
                smtp.EnableSsl = bSenderEnableSsl

                smtp.UseDefaultCredentials = False
                smtp.Credentials = nc
                smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network
                smtp.Port = intSenderPort
                smtp.Timeout = 1200000
                smtp.Send(mm)

                Return True
            Catch ex As Exception
                Throw ex
            End Try

            Return False
        End Function

        ''' <summary>
        ''' Check if the email is valid.
        ''' </summary>
        ''' <param name="email"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function IsValidEmail(ByVal email As String) As Boolean
            'http://stackoverflow.com/a/17712290
            Const pattern As String = "^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$"
            Dim options As RegexOptions = RegexOptions.IgnoreCase Or RegexOptions.IgnorePatternWhitespace Or RegexOptions.Multiline
            Return Regex.IsMatch(email, pattern, options)
        End Function
    End Class
End Namespace