﻿Namespace Misc
    Public Class SqlHelper
        ''' <summary>
        ''' Format SQL query. Throw exception if query has issue.
        ''' </summary>
        ''' <param name="bStandardFormat">If true, standardize the format, eg. keywords are in upper case.</param>
        ''' <param name="bAddNationalString">If true, add N prefix to literal string if it contains ANSI / Unicode character.</param>
        Shared Function FormatSql(ByVal sql As String,
                                  Optional ByVal bStandardFormat As Boolean = False,
                                  Optional ByVal bAddNationalString As Boolean = False) As String
            Dim tokenizer As PoorMansTSqlFormatterLib.Tokenizers.TSqlStandardTokenizer = New PoorMansTSqlFormatterLib.Tokenizers.TSqlStandardTokenizer()
            Dim tokens = tokenizer.TokenizeSQL(sql)

            If bAddNationalString Then
                For Each n In tokens
                    If n.Type <> PoorMansTSqlFormatterLib.Interfaces.SqlTokenType.String Then Continue For
                    If ContainsUnicodeCharacter(n.Value) Then n.Type = PoorMansTSqlFormatterLib.Interfaces.SqlTokenType.NationalString
                Next
            End If

            Dim newSql As String = ""
            Dim errorOutputPrefix = "--Exception--"
            If bStandardFormat Then
                Dim formatter As PoorMansTSqlFormatterLib.Formatters.TSqlStandardFormatter = New PoorMansTSqlFormatterLib.Formatters.TSqlStandardFormatter()
                Dim parser As PoorMansTSqlFormatterLib.Parsers.TSqlStandardParser = New PoorMansTSqlFormatterLib.Parsers.TSqlStandardParser()
                formatter.Options.NewClauseLineBreaks = 0
                formatter.ErrorOutputPrefix = errorOutputPrefix
                newSql = formatter.FormatSQLTree(parser.ParseSQL(tokens))
            Else
                Dim formatter As PoorMansTSqlFormatterLib.Formatters.TSqlIdentityFormatter = New PoorMansTSqlFormatterLib.Formatters.TSqlIdentityFormatter()
                formatter.ErrorOutputPrefix = errorOutputPrefix
                newSql = formatter.FormatSQLTokens(tokens)
            End If

            If newSql.StartsWith(errorOutputPrefix) Then Throw New Exception("Error encountered during SQL parsing")

            Return newSql
        End Function
    End Class
End Namespace