﻿Imports System
Imports System.Data
Imports System.Text

Namespace Misc
    Public Class DataTableHelper
        Public Shared Function GetSelectedKeyArray(selectedColumnName As String, keyColumnName As String, table As DataTable) As Long()
            Dim array As DataRow()
            If selectedColumnName.Length = 0 Then
                array = table.[Select]("")
            Else
                array = table.[Select](String.Format("{0} = true", selectedColumnName))
            End If
            Dim array2 As Long() = New Long(array.Length - 1) {}
            For i As Integer = 0 To array.Length - 1
                array2(i) = Convert.ToInt64(array(i)(keyColumnName))
            Next
            Return array2
        End Function

        Public Shared Function GetSelectedKeyCommaString(selectedColumnName As String, keyColumnName As String, table As DataTable) As String
            Dim array As DataRow()
            If selectedColumnName.Length = 0 Then
                array = table.[Select]("")
            Else
                array = table.[Select](String.Format("{0} = true", selectedColumnName))
            End If
            Dim stringBuilder As StringBuilder = New StringBuilder()
            For i As Integer = 0 To array.Length - 1
                If i > 0 Then
                    stringBuilder.Append(",")
                End If
                stringBuilder.Append(array(i)(keyColumnName).ToString())
            Next
            Return stringBuilder.ToString()
        End Function

        Public Shared Function IsTablesEqual(t1 As DataTable, t2 As DataTable) As Boolean
            If t1.Rows.Count <> t2.Rows.Count Then
                Return False
            End If
            If t1.Columns.Count <> t2.Columns.Count Then
                Return False
            End If
            For i As Integer = 0 To t1.Columns.Count - 1
                If t1.Columns(i).ColumnName <> t2.Columns(i).ColumnName Then
                    Return False
                End If
            Next
            For j As Integer = 0 To t1.Rows.Count - 1
                Dim dataRow As DataRow = t1.Rows(j)
                Dim dataRow2 As DataRow = t2.Rows(j)
                If dataRow.RowState = DataRowState.Deleted OrElse dataRow2.RowState = DataRowState.Deleted Then
                    If dataRow.RowState <> dataRow2.RowState Then
                        Return False
                    End If
                Else
                    For k As Integer = 0 To t1.Columns.Count - 1
                        If Not dataRow(k).Equals(dataRow2(k)) Then
                            Return False
                        End If
                    Next
                End If
            Next
            Return True
        End Function

        ''' <summary>
        ''' Modify binary or image columns as string.
        ''' </summary>
        Public Shared Function MorphBinaryColumns(ByVal table As DataTable) As DataTable
            Dim targetNames As List(Of String) = table.Columns.Cast(Of DataColumn)().Where(Function(col) col.DataType.Equals(GetType(Byte()))).[Select](Function(col) col.ColumnName).ToList()
            For Each colName As String In targetNames
                ' add new column and put it where the old column was
                Dim tmpName = "new"
                table.Columns.Add(New DataColumn(tmpName, GetType(String)))
                table.Columns(tmpName).SetOrdinal(table.Columns(colName).Ordinal)

                ' fill in values in new column for every row
                For Each row As DataRow In table.Rows
                    If row(colName) IsNot DBNull.Value Then
                        row(tmpName) = "0x" + String.Join("", DirectCast(row(colName), Byte()).[Select](Function(b) b.ToString("X2")).ToArray())
                    Else
                        row(tmpName) = "NULL"
                    End If
                Next

                ' cleanup
                table.Columns.Remove(colName)
                table.Columns(tmpName).ColumnName = colName
            Next

            Return table
        End Function
    End Class
End Namespace
