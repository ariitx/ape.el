﻿Imports System
Imports System.Security.Cryptography
Imports System.Text
Imports System.Threading
Imports Ape.EL.Localization

Namespace Misc
    Public Class PasswordHelper
        <LocalizableString()>
        Public Enum PasswordStrength
            <DefaultString("Weak")>
            Weak
            <DefaultString("Medium")>
            Medium
            <DefaultString("Strong")>
            Strong
        End Enum

        Private Shared Function GetRandomPrefix() As String
            Dim array As Byte() = New Byte(16 - 1) {}
            For i As Integer = 0 To 16 - 1
                array(i) = Convert.ToByte(Misc.Randomizer.[Next](32, 127))
            Next
            Return Convert.ToBase64String(array)
        End Function

        Public Shared Function GetPasswordStrength(password As String) As PasswordHelper.PasswordStrength
            If password.Length < 8 Then
                Return PasswordHelper.PasswordStrength.Weak
            End If
            Dim flag As Boolean = False
            Dim flag2 As Boolean = False
            Dim flag3 As Boolean = False
            Dim flag4 As Boolean = False
            For i As Integer = 0 To password.Length - 1
                Dim c As Char = password(i)
                If Char.IsDigit(c) Then
                    flag = True
                Else
                    If Char.IsUpper(c) Then
                        flag2 = True
                    Else
                        If Char.IsLower(c) Then
                            flag3 = True
                        Else
                            flag4 = True
                        End If
                    End If
                End If
            Next
            If flag AndAlso flag2 AndAlso flag3 AndAlso flag4 Then
                Return PasswordHelper.PasswordStrength.Strong
            End If
            If flag AndAlso flag2 AndAlso flag3 Then
                Return PasswordHelper.PasswordStrength.Medium
            End If
            Return PasswordHelper.PasswordStrength.Weak
        End Function

        Public Shared Function ComputePasswordHash(password As String) As String
            Dim aSCIIEncoding As ASCIIEncoding = New ASCIIEncoding()
            Dim sHA As SHA512 = New SHA512Managed()
            Dim randomPrefix As String = PasswordHelper.GetRandomPrefix()
            password = randomPrefix + password
            Dim bytes As Byte() = aSCIIEncoding.GetBytes(password)
            Dim inArray As Byte() = sHA.ComputeHash(bytes)
            Return randomPrefix + Convert.ToBase64String(inArray)
        End Function

        Public Shared Function VerifyPassword(hashPassword As String, password As String) As Boolean
            If hashPassword.Length < 24 Then
                Return False
            End If
            Dim str As String = hashPassword.Substring(0, 24)
            Dim s As String = str + password
            Dim aSCIIEncoding As ASCIIEncoding = New ASCIIEncoding()
            Dim sHA As SHA512 = New SHA512Managed()
            Dim bytes As Byte() = aSCIIEncoding.GetBytes(s)
            Dim inArray As Byte() = sHA.ComputeHash(bytes)
            Return hashPassword = str + Convert.ToBase64String(inArray)
        End Function

        Public Shared Function CreateResetPasswordKey(code As String) As String
            Dim mD As MD5 = New MD5CryptoServiceProvider()
            Dim str As String = "JK9u31kjko9uqajkadsiuqen1k23JvsOIasd98KadsIADaji9182"
            Dim aSCIIEncoding As ASCIIEncoding = New ASCIIEncoding()
            Dim bytes As Byte() = aSCIIEncoding.GetBytes(code + str)
            Dim array As Byte() = mD.ComputeHash(bytes)
            Dim text As String = ""
            For i As Integer = 0 To 8 - 1
                Dim b As Byte = array(i)
                Dim num As Integer = CInt((b Mod 10))
                text += Chr((48 + num))
            Next
            Return text
        End Function

        Public Shared Function Generate8DigitsRandomNumber() As String
            Dim stringBuilder As StringBuilder = New StringBuilder()
            For i As Integer = 0 To 8 - 1
                stringBuilder.Append(Chr(Misc.Randomizer.[Next](48, 57)))
            Next
            Return stringBuilder.ToString()
        End Function

        Public Shared Function GenerateRandomAphanumeric(ByVal length As Integer) As String
            If length <= 0 Then length = 8
            Dim stringBuilder As StringBuilder = New StringBuilder()
            For i As Integer = 0 To length - 1
                Dim c As String = ""
                While Not System.Text.RegularExpressions.Regex.IsMatch(c, "^[a-zA-Z0-9]+$")
                    c = Chr(Misc.Randomizer.[Next](48, 90))
                End While
                stringBuilder.Append(c)
            Next
            Return stringBuilder.ToString()
        End Function
    End Class
End Namespace
