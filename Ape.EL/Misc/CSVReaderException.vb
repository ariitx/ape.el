
Namespace Misc
    Public Class CSVReaderException
        Inherits Ape.EL.App.AppException

        Public Sub New(message As String)
            MyBase.New(message)
        End Sub
    End Class
End Namespace
