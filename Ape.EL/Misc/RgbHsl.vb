﻿
Imports System
Imports System.Drawing

Namespace Misc
    Public Class RgbHsl
        Public Class HSL
            Private _h As Double

            Private _s As Double

            Private _l As Double

            Public Property H() As Double
                Get
                    Return Me._h
                End Get
                Set(value As Double)
                    Me._h = value
                    Me._h = (If((Me._h > 1.0), 1.0, (If((Me._h < 0.0), 0.0, Me._h))))
                End Set
            End Property

            Public Property S() As Double
                Get
                    Return Me._s
                End Get
                Set(value As Double)
                    Me._s = value
                    Me._s = (If((Me._s > 1.0), 1.0, (If((Me._s < 0.0), 0.0, Me._s))))
                End Set
            End Property

            Public Property L() As Double
                Get
                    Return Me._l
                End Get
                Set(value As Double)
                    Me._l = value
                    Me._l = (If((Me._l > 1.0), 1.0, (If((Me._l < 0.0), 0.0, Me._l))))
                End Set
            End Property

            Public Sub New()
                Me._h = 0.0
                Me._s = 0.0
                Me._l = 0.0
            End Sub
        End Class

        Public Shared Function SetBrightness(c As Color, brightness As Double) As Color
            Dim hSL As RgbHsl.HSL = RgbHsl.RGB_to_HSL(c)
            hSL.L = brightness
            Return RgbHsl.HSL_to_RGB(hSL)
        End Function

        Public Shared Function ModifyBrightness(c As Color, brightness As Double) As Color
            Dim hSL As RgbHsl.HSL = RgbHsl.RGB_to_HSL(c)
            hSL.L *= brightness
            Return RgbHsl.HSL_to_RGB(hSL)
        End Function

        Public Shared Function SetSaturation(c As Color, Saturation As Double) As Color
            Dim hSL As RgbHsl.HSL = RgbHsl.RGB_to_HSL(c)
            hSL.S = Saturation
            Return RgbHsl.HSL_to_RGB(hSL)
        End Function

        Public Shared Function ModifySaturation(c As Color, Saturation As Double) As Color
            Dim hSL As RgbHsl.HSL = RgbHsl.RGB_to_HSL(c)
            hSL.S *= Saturation
            Return RgbHsl.HSL_to_RGB(hSL)
        End Function

        Public Shared Function SetHue(c As Color, Hue As Double) As Color
            Dim hSL As RgbHsl.HSL = RgbHsl.RGB_to_HSL(c)
            hSL.H = Hue
            Return RgbHsl.HSL_to_RGB(hSL)
        End Function

        Public Shared Function ModifyHue(c As Color, Hue As Double) As Color
            Dim hSL As RgbHsl.HSL = RgbHsl.RGB_to_HSL(c)
            hSL.H *= Hue
            Return RgbHsl.HSL_to_RGB(hSL)
        End Function

        Public Shared Function HSL_to_RGB(hsl As RgbHsl.HSL) As Color
            Dim num As Double
            Dim num2 As Double
            Dim num3 As Double
            If hsl.L = 0.0 Then
                Dim expr_38 As Double = 0.0
                num = expr_38
                num2 = expr_38
                num3 = expr_38
            Else
                If hsl.S = 0.0 Then
                    Dim expr_59 As Double = hsl.L
                    num = expr_59
                    num2 = expr_59
                    num3 = expr_59
                Else
                    Dim num4 As Double = If((hsl.L <= 0.5), (hsl.L * (1.0 + hsl.S)), (hsl.L + hsl.S - hsl.L * hsl.S))
                    Dim num5 As Double = 2.0 * hsl.L - num4
                    Dim array As Double() = New Double() {hsl.H + 0.33333333333333331, hsl.H, hsl.H - 0.33333333333333331}
                    Dim array2 As Double() = New Double(3 - 1) {}
                    Dim array3 As Double() = array2
                    For i As Integer = 0 To 3 - 1
                        If array(i) < 0.0 Then
                            array(i) += 1.0
                        End If
                        If array(i) > 1.0 Then
                            array(i) -= 1.0
                        End If
                        If 6.0 * array(i) < 1.0 Then
                            array3(i) = num5 + (num4 - num5) * array(i) * 6.0
                        Else
                            If 2.0 * array(i) < 1.0 Then
                                array3(i) = num4
                            Else
                                If 3.0 * array(i) < 2.0 Then
                                    array3(i) = num5 + (num4 - num5) * (0.66666666666666663 - array(i)) * 6.0
                                Else
                                    array3(i) = num5
                                End If
                            End If
                        End If
                    Next
                    num3 = array3(0)
                    num2 = array3(1)
                    num = array3(2)
                End If
            End If
            Return Color.FromArgb(CInt((255.0 * num3)), CInt((255.0 * num2)), CInt((255.0 * num)))
        End Function

        Public Shared Function RGB_to_HSL(c As Color) As RgbHsl.HSL
            Return New RgbHsl.HSL() With {.H = CDec(c.GetHue()) / 360.0, .L = CDec(c.GetBrightness()), .S = CDec(c.GetSaturation())}
        End Function

        Public Shared Function GetOppositeColor(c As Color) As Color
            Dim brightness As Single = c.GetBrightness()
            Dim num As Double = If((CDec(brightness) < 0.5), 1.0, 0.0)
            Dim color As Color = RgbHsl.SetBrightness(c, num)
            If Not RgbHsl.IsContrast(c, color) Then
                If num = 0.0 Then
                    num = 1.0
                Else
                    num = 0.0
                End If
                color = RgbHsl.SetBrightness(c, num)
            End If
            Return color
        End Function

        Public Shared Function GetOppositeColor2(c As Color) As Color
            Dim hSL As RgbHsl.HSL = RgbHsl.RGB_to_HSL(c)
            hSL.H = (hSL.H + 0.5) Mod 1.0
            hSL.L = (If((hSL.L < 0.5), 1.0, 0.0))
            hSL.S = (If((hSL.S < 0.5), 1.0, 0.0))
            Return RgbHsl.HSL_to_RGB(hSL)
        End Function

        Private Shared Function IsContrast(color1 As Color, color2 As Color) As Boolean
            Dim r As Integer = CInt(color1.R)
            Dim g As Integer = CInt(color1.G)
            Dim b As Integer = CInt(color1.B)
            Dim r2 As Integer = CInt(color2.R)
            Dim g2 As Integer = CInt(color2.G)
            Dim b2 As Integer = CInt(color2.B)
            Dim num As Integer = (r * 299 + g * 587 + b * 114) / 1000
            Dim num2 As Integer = (r2 * 299 + g2 * 587 + b2 * 114) / 1000
            Dim num3 As Integer = Math.Abs(num - num2)
            Dim num4 As Integer = Math.Max(r, r2) - Math.Min(r, r2) + Math.Max(g, g2) - Math.Min(g, g2) + Math.Max(b, b2) - Math.Min(b, b2)
            Return num3 >= 125 AndAlso num4 >= 250
        End Function
    End Class
End Namespace
