﻿
Imports System
Imports System.Collections
Imports System.IO
Imports System.Runtime.InteropServices

Namespace Misc
    Public Class Mail
        <StructLayout(LayoutKind.Sequential)>
        Private Class MapiMessage
            Public Reserved As Integer

            Public Subject As String

            Public NoteText As String

            Public MessageType As String

            Public DateReceived As String

            Public ConversationID As String

            Public Flags As Integer

            Public Originator As IntPtr

            Public RecipCount As Integer

            Public Recips As IntPtr

            Public FileCount As Integer

            Public Files As IntPtr
        End Class

        <StructLayout(LayoutKind.Sequential)>
        Private Class MapiRecipDesc
            Public Reserved As Integer

            Public RecipClass As Integer

            Public Name As String

            Public Address As String

            Public EIDSize As Integer

            Public EntryID As IntPtr
        End Class

        <StructLayout(LayoutKind.Sequential)>
        Private Class MapiFileDesc
            Public Reserved As Integer

            Public Flags As Integer

            Public Position As Integer

            Public PathName As String

            Public FileName As String

            Public FileType As IntPtr
        End Class

        Private Class NativeMethods
            Private Sub New()
            End Sub

            Friend Declare Ansi Function MAPILogon Lib "MAPI32.DLL" (hwnd As IntPtr, profileName As String, password As String, flags As Integer, reserved As Integer, ByRef session As IntPtr) As Integer

            Friend Declare Function MAPILogoff Lib "MAPI32.DLL" (session As IntPtr, hwnd As IntPtr, flags As Integer, reserved As Integer) As Integer

            Friend Declare Function MAPISendMail Lib "MAPI32.DLL" (session As IntPtr, hwnd As IntPtr, message As Mail.MapiMessage, flags As Integer, reserved As Integer) As Integer
        End Class

        Private Const MAPI_TO As Integer = 1

        Private Const MAPI_CC As Integer = 2

        Private Const MAPI_LOGON_UI As Integer = 1

        Private Const MAPI_DIALOG As Integer = 8

        Private Const MAPI_E_USER_ABORT As Integer = 1

        Private mySubject As String

        Private myBody As String

        Private myRecipients As ArrayList = New ArrayList()

        Private myAttachments As ArrayList = New ArrayList()

        Private Shared Function Logon(hwnd As IntPtr, ByRef session As IntPtr) As Boolean
            Dim num As Integer = Mail.NativeMethods.MAPILogon(hwnd, Nothing, Nothing, 0, 0, session)
            If num <> 0 Then
                num = Mail.NativeMethods.MAPILogon(hwnd, Nothing, Nothing, 1, 0, session)
            End If
            Return num = 0
        End Function

        Private Shared Sub Logoff(hwnd As IntPtr, ByRef session As IntPtr)
            If session <> IntPtr.Zero Then
                Mail.NativeMethods.MAPILogoff(session, hwnd, 0, 0)
                session = IntPtr.Zero
            End If
        End Sub

        Private Function CreateMessage() As Mail.MapiMessage
            Dim mapiMessage As Mail.MapiMessage = New Mail.MapiMessage()
            mapiMessage.Reserved = 0
            mapiMessage.Subject = Me.mySubject
            mapiMessage.NoteText = Me.myBody
            mapiMessage.MessageType = Nothing
            mapiMessage.DateReceived = Nothing
            mapiMessage.ConversationID = Nothing
            mapiMessage.Flags = 0
            mapiMessage.Originator = IntPtr.Zero
            mapiMessage.Recips = Me.AllocRecips(mapiMessage.RecipCount)
            mapiMessage.Files = Me.AllocFiles(mapiMessage.FileCount)
            Return mapiMessage
        End Function

        Private Function AllocRecips(<Out()> ByRef count As Integer) As IntPtr
            count = Me.myRecipients.Count
            If count = 0 Then
                Return IntPtr.Zero
            End If
            Dim typeFromHandle As Type = GetType(Mail.MapiRecipDesc)
            Dim num As Integer = Marshal.SizeOf(typeFromHandle)
            Dim result As IntPtr = Marshal.AllocHGlobal(count * num)
            Dim num2 As Long = result.ToInt64()
            For i As Integer = 0 To count - 1
                Marshal.StructureToPtr(Me.myRecipients(i), CType(num2, IntPtr), False)
                num2 += CLng(num)
            Next
            Return result
        End Function

        Private Function AllocFiles(<Out()> ByRef count As Integer) As IntPtr
            count = Me.myAttachments.Count
            If count = 0 Then
                Return IntPtr.Zero
            End If
            Dim typeFromHandle As Type = GetType(Mail.MapiFileDesc)
            Dim num As Integer = Marshal.SizeOf(typeFromHandle)
            Dim result As IntPtr = Marshal.AllocHGlobal(count * num)
            Dim mapiFileDesc As Mail.MapiFileDesc = New Mail.MapiFileDesc()
            mapiFileDesc.Reserved = 0
            mapiFileDesc.Position = -1
            mapiFileDesc.Flags = 0
            mapiFileDesc.FileType = IntPtr.Zero
            Dim num2 As Long = result.ToInt64()
            For i As Integer = 0 To count - 1
                Dim array As String() = CType(Me.myAttachments(i), String())
                mapiFileDesc.PathName = array(0)
                mapiFileDesc.FileName = array(1)
                Marshal.StructureToPtr(mapiFileDesc, CType(num2, IntPtr), False)
                num2 += CLng(num)
            Next
            Return result
        End Function

        Private Shared Sub DisposeMessage(msg As Mail.MapiMessage)
            If msg.Recips <> IntPtr.Zero Then
                Dim typeFromHandle As Type = GetType(Mail.MapiRecipDesc)
                Dim num As Integer = Marshal.SizeOf(typeFromHandle)
                Dim num2 As Long = msg.Recips.ToInt64()
                For i As Integer = 0 To msg.RecipCount - 1
                    Marshal.DestroyStructure(CType(num2, IntPtr), typeFromHandle)
                    num2 += CLng(num)
                Next
                Marshal.FreeHGlobal(msg.Recips)
            End If
            If msg.Files <> IntPtr.Zero Then
                Dim typeFromHandle2 As Type = GetType(Mail.MapiFileDesc)
                Dim num3 As Integer = Marshal.SizeOf(typeFromHandle2)
                Dim num4 As Long = msg.Files.ToInt64()
                For j As Integer = 0 To msg.FileCount - 1
                    Marshal.DestroyStructure(CType(num4, IntPtr), typeFromHandle2)
                    num4 += CLng(num3)
                Next
                Marshal.FreeHGlobal(msg.Files)
            End If
        End Sub

        Public Sub SetSubject(subject As String)
            Me.mySubject = subject
        End Sub

        Public Sub SetBody(body As String)
            Me.myBody = body
        End Sub

        Public Sub AddRecipient(name As String, address As String, cc As Boolean)
            If name Is Nothing Then
                Throw New ArgumentNullException("name")
            End If
            name = name.Trim()
            If name.Length = 0 Then
                Throw New ArgumentException("name is empty.", "name")
            End If
            If address Is Nothing Then
                Throw New ArgumentNullException("address")
            End If
            address = address.Trim()
            If address.Length = 0 Then
                Throw New ArgumentException("address is empty.", "address")
            End If
            Dim mapiRecipDesc As Mail.MapiRecipDesc = New Mail.MapiRecipDesc()
            mapiRecipDesc.Reserved = 0
            mapiRecipDesc.RecipClass = (If(cc, 2, 1))
            mapiRecipDesc.Name = address
            mapiRecipDesc.EIDSize = 0
            mapiRecipDesc.EntryID = IntPtr.Zero
            Me.myRecipients.Add(mapiRecipDesc)
        End Sub

        Public Sub ClearRecipients()
            Me.myRecipients.Clear()
        End Sub

        Public Sub AddAttachment(myPath As String, filename As String)
            If myPath Is Nothing Then
                Throw New ArgumentNullException("myPath")
            End If
            If Not File.Exists(myPath) Then
                Throw New ArgumentException(String.Format("File: {0} not exist.", myPath))
            End If
            If filename IsNot Nothing Then
                filename = filename.Trim()
            End If
            If filename Is Nothing OrElse filename.Length = 0 Then
                filename = Path.GetFileName(myPath)
            End If
            Me.myAttachments.Add(New String() {myPath, filename})
        End Sub

        Public Sub ClearAttachments()
            Me.myAttachments.Clear()
        End Sub

        Public Function Send() As Boolean
            Return Me.Send(IntPtr.Zero, False)
        End Function

        Public Function Send(hwnd As IntPtr, showDialog As Boolean) As Boolean
            Dim zero As IntPtr = IntPtr.Zero
            If Mail.Logon(hwnd, zero) Then
                Dim mapiMessage As Mail.MapiMessage = Me.CreateMessage()
                Dim num As Integer = Mail.NativeMethods.MAPISendMail(zero, hwnd, mapiMessage, If(showDialog, 8, 0), 0)
                Mail.Logoff(hwnd, zero)
                Mail.DisposeMessage(mapiMessage)
                Return num = 0 OrElse num = 1
            End If
            Return False
        End Function
    End Class
End Namespace
