﻿Namespace Misc
    Public Class EnumHelper
        ''' <summary>
        ''' Get array of enum.
        ''' </summary>
        Public Shared Function GetEnumArray(id As Type) As Array
            Try
                If id.IsEnum Then Return System.Enum.GetValues(id)
            Catch ex As Exception
                Debug.Assert(False, ex.ToString)
            End Try
            Return Nothing
        End Function

        ''' <summary>
        ''' Get enum array to list of enum.
        ''' </summary>
        Public Shared Function GetEnumArray(Of T)() As List(Of T)
            Dim arr As System.Array = GetEnumArray(GetType(T))
            If arr IsNot Nothing Then
                Return DirectCast(arr, T()).ToList()
            End If
            Return Nothing
        End Function

        ''' <summary>
        ''' Convert enum member in string to enum member.
        ''' Returns nothing if string cant be found.
        ''' </summary>
        Public Shared Function StringToEnum(enumType As Type, value As String) As Object
            If String.IsNullOrEmpty(value) Then Return Nothing

            Dim enumList As Array = GetEnumArray(enumType)
            For Each e In enumList
                If e.ToString.ToLower = value.ToLower Then
                    Return e
                End If
            Next
            Return Nothing
        End Function
    End Class
End Namespace

