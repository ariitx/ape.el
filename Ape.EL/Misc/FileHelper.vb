﻿Imports System.IO

Namespace Misc
    ''' <summary>
    ''' This class contains directory helper method(s).
    ''' </summary>
    ''' <remarks>https://www.dotnetperls.com/recursive-file-directory-vbnet</remarks>
    Public Class FileHelper
        ''' <summary>
        ''' Ported command line xcopy as a class with some functions.
        ''' </summary>
        Public Class XCopy
            Property Source As String
            Property Destination As String
            Property ShowCommandLine As Boolean = False
            ''' <summary>
            ''' Copy only newer files. Default is true.
            ''' </summary>
            Property CopyNewerOnly As Boolean = True
            ''' <summary>
            ''' Copy hidden files as well. Default is true.
            ''' </summary>
            Property CopyHidden As Boolean = True
            ''' <summary>
            ''' Copying read-only files to retain that file attribute in destination. Default is true.
            ''' </summary>
            Property RetainReadonlyAttribute As Boolean = True
            ''' <summary>
            ''' Please use the switch as in command prompt. This will ignore all other properties (eg. CopyNewerOnly)
            ''' </summary>
            Property AdditionalArguments As String

            Public Sub Copy()
                Copy(Source, Destination)
            End Sub

            Public Sub Copy(ByVal source As String, ByVal dest As String)
                Dim sourceIsFile As Boolean = System.IO.File.Exists(source)
                Dim sourceIsDir As Boolean = System.IO.Directory.Exists(source)

                If sourceIsFile Or sourceIsDir Then
                    If dest.EndsWith(System.IO.Path.DirectorySeparatorChar) Or sourceIsDir Then
                        Ape.EL.Utility.General.MakePath(dest)
                        Ape.EL.Utility.General.SetFullAccess(dest)
                    End If
                    If sourceIsDir Then
                        If source.EndsWith("\") Or source.EndsWith("/") Then
                            source = source.First(source.Count - 1)
                        End If
                    End If

                    Dim sbCmd As New System.Text.StringBuilder
                    sbCmd.Append(" echo")
                    If sourceIsFile Then
                        sbCmd.Append(" F")
                    ElseIf sourceIsDir Then
                        sbCmd.Append(" D")
                    End If
                    sbCmd.Append(" |")
                    sbCmd.Append(" xcopy")
                    sbCmd.Append(String.Format(" ""{0}""", source))
                    sbCmd.Append(String.Format(" ""{0}""", dest))

                    If Not AdditionalArguments.IsEmpty Then
                        sbCmd.Append(" " & AdditionalArguments)
                    Else
                        If sourceIsDir Then
                            sbCmd.Append(" /E")
                        End If
                        If CopyHidden Then
                            sbCmd.Append(" /H")
                        End If
                        If RetainReadonlyAttribute Then
                            sbCmd.Append(" /K")
                        End If
                        If CopyNewerOnly Then
                            sbCmd.Append(" /D")
                        End If
                    End If

                    Ape.EL.App.Cmd.Run(sbCmd.ToStr, , , Not ShowCommandLine, True)
                End If
            End Sub
        End Class

        ''' <summary>
        ''' This method starts at the specified directory.
        ''' It traverses all subdirectories.
        ''' It returns a List of those directories.
        ''' </summary>
        Public Shared Function GetFilesRecursive(ByVal initial As String) As List(Of String)
            ' This list stores the results.
            Dim result As New List(Of String)

            ' This stack stores the directories to process.
            Dim stack As New Stack(Of String)

            ' Add the initial directory
            stack.Push(initial)

            ' Continue processing for each stacked directory
            Do While (stack.Count > 0)
                ' Get top directory string
                Dim dir As String = stack.Pop
                Try
                    ' Add all immediate file paths
                    result.AddRange(Directory.GetFiles(dir, "*.*"))

                    ' Loop through all subdirectories and add them to the stack.
                    Dim directoryName As String
                    For Each directoryName In Directory.GetDirectories(dir)
                        stack.Push(directoryName)
                    Next

                Catch ex As Exception
                End Try
            Loop

            ' Return the list
            Return result
        End Function

        ''' <summary>
        ''' System.IO.File.Copy with ability to copy only newer files.
        ''' </summary>
        Public Shared Sub CopyNew(ByVal source As String, ByVal dest As String)
            Dim doCopy As Boolean = True
            If System.IO.File.Exists(source) Then
                If dest.EndsWith("\") Then 'destination is a directory, change the path to file as source.
                    If Not System.IO.Directory.Exists(dest) Then
                        Ape.EL.Utility.General.MakePath(dest)
                        Ape.EL.Utility.General.SetFullAccess(dest)
                    End If
                    dest = dest & System.IO.Path.GetFileName(source)
                End If
                If System.IO.File.Exists(dest) Then
                    If System.IO.File.GetLastWriteTimeUtc(source) = System.IO.File.GetLastWriteTimeUtc(dest) Then
                        doCopy = False
                    End If
                End If

                If doCopy Then
                    System.IO.File.Copy(source, dest, True)
                End If
            End If
        End Sub
    End Class
End Namespace
