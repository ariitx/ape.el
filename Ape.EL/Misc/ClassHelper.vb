﻿Imports System.Reflection

Namespace Misc
    Public Class ClassHelper
        ''' http://weblogs.asp.net/whaggard/2708
        ''' <summary>
        ''' Getting a list of constants using Reflection.
        ''' </summary>
        Public Shared Function GetStringsFromClassConstants(ByVal type As System.Type) As String()
            Dim constants As New ArrayList()
            Dim fieldInfos As FieldInfo() = type.GetFields(BindingFlags.[Public] Or _
                BindingFlags.[Static] Or BindingFlags.FlattenHierarchy)

            For Each fi As FieldInfo In fieldInfos
                If fi.IsLiteral AndAlso Not fi.IsInitOnly Then
                    constants.Add(fi)
                End If
            Next

            Dim ConstantsStringArray As New System.Collections.Specialized.StringCollection

            For Each fi As FieldInfo In _
                DirectCast(constants.ToArray(GetType(FieldInfo)), FieldInfo())
                ConstantsStringArray.Add(fi.GetValue(Nothing))
            Next

            Dim retVal(ConstantsStringArray.Count - 1) As String
            ConstantsStringArray.CopyTo(retVal, 0)
            Return retVal
        End Function

        Public Shared Function CreateNew(Of T As New)(obj As T) As T
            Return New T()
        End Function

        Public Shared Function CreateNewList(Of T)(obj As T) As List(Of T)
            Return New List(Of T)
        End Function
    End Class
End Namespace
