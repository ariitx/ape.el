﻿Imports System
Imports System.Collections
Imports System.IO
Imports System.Text

Namespace Misc
    Public Class CSVReader
        Implements IDisposable


        Private stream As Stream

        Private reader As StreamReader

        Public Sub New(s As Stream)
            Me.New(s, Nothing)
        End Sub

        Public Sub New(s As Stream, enc As Encoding)
            Me.stream = s
            If Not s.CanRead Then
                Throw New CSVReaderException("Could not read the given CSV stream!")
            End If
            Me.reader = (If((enc IsNot Nothing), New StreamReader(s, enc), New StreamReader(s)))
        End Sub

        Public Sub New(filename As String)
            Me.New(filename, Nothing)
        End Sub

        Public Sub New(filename As String, enc As Encoding)
            Me.New(New FileStream(filename, FileMode.Open), enc)
        End Sub

        Public Function GetCSVLine() As String()
            Dim text As String = Me.reader.ReadLine()
            If text Is Nothing Then
                Return Nothing
            End If
            If text.Length = 0 Then
                Return New String(0 - 1) {}
            End If
            Dim arrayList As ArrayList = New ArrayList()
            Me.ParseCSVFields(arrayList, text)
            Return CType(arrayList.ToArray(GetType(String)), String())
        End Function

        Private Sub ParseCSVFields(result As ArrayList, data As String)
            Dim i As Integer = -1
            While i < data.Length
                result.Add(Me.ParseCSVField(data, i))
            End While
        End Sub

        Private Function ParseCSVField(data As String, ByRef startSeparatorPosition As Integer) As String
            If startSeparatorPosition = data.Length - 1 Then
                startSeparatorPosition += 1
                Return ""
            End If
            Dim num As Integer = startSeparatorPosition + 1
            If data(num) = """" Then
                If num = data.Length - 1 Then
                    num += 1
                    Return """"
                End If
                Dim num2 As Integer = Me.FindSingleQuote(data, num + 1)
                startSeparatorPosition = num2 + 1
                Return data.Substring(num + 1, num2 - num - 1).Replace("""""", """")
            Else
                Dim num3 As Integer = data.IndexOf(",", num)
                If num3 = -1 Then
                    startSeparatorPosition = data.Length
                    Return data.Substring(num)
                End If
                startSeparatorPosition = num3
                Return data.Substring(num, num3 - num)
            End If
        End Function

        Private Function FindSingleQuote(data As String, startFrom As Integer) As Integer
            Dim num As Integer = startFrom - 1
            While True
                Dim expr_34 As Integer = num + 1
                num = expr_34
                If expr_34 >= data.Length Then
                    Return num
                End If
                If data(num) = """" Then
                    If num >= data.Length - 1 OrElse data(num + 1) <> """" Then
                        Exit While
                    End If
                    num += 1
                End If
            End While
            Return num
        End Function

        Public Sub Dispose() Implements IDisposable.Dispose
            Try
                If Me.reader IsNot Nothing Then
                    Me.reader.Close()
                Else
                    If Me.stream IsNot Nothing Then
                        Me.stream.Close()
                    End If
                End If
            Catch ex_2A As Exception
            End Try
            GC.SuppressFinalize(Me)
        End Sub
    End Class
End Namespace
