﻿Imports System.IO

Namespace Misc
    Public Class MemoryStreamHelper
        Public Shared Function CompareMemoryStreams(ms1 As MemoryStream, ms2 As MemoryStream) As Boolean
            If ms1.Length <> ms2.Length Then
                Return False
            End If
            ms1.Position = 0
            ms2.Position = 0

            Dim msArray1 = ms1.ToArray()
            Dim msArray2 = ms2.ToArray()

            Return msArray1.SequenceEqual(msArray2)
        End Function
    End Class
End Namespace
