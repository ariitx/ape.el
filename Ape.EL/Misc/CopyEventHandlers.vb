﻿
Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Linq
Imports System.Reflection
Imports System.Windows.Forms

Namespace Misc
    Public Class CopyEventHandlers
        Private _keyEventNameMapping As Dictionary(Of String, String) = Nothing

        Public ReadOnly Property KeyEventNameMapping() As Dictionary(Of String, String)
            Get
                Return Me._keyEventNameMapping
            End Get
        End Property

        Public Sub New()
            Dim dictionary As New Dictionary(Of String, String)
            dictionary.Add("EventAutoSizeChanged", "AutoSizeChanged")
            dictionary.Add("EventBackColor", "BackColorChanged")
            dictionary.Add("EventBackgroundImage", "BackgroundImageChanged")
            dictionary.Add("EventBackgroundImageLayout", "BackgroundImageLayoutChanged")
            dictionary.Add("EventBindingContext", "BindingContextChanged")
            dictionary.Add("EventCausesValidation", "CausesValidationChanged")
            dictionary.Add("EventClick", "Click")
            dictionary.Add("EventClientSize", "ClientSizeChanged")
            dictionary.Add("EventContextMenu", "ContextMenuChanged")
            dictionary.Add("EventContextMenuStrip", "ContextMenuStripChanged")
            dictionary.Add("EventCursor", "CursorChanged")
            dictionary.Add("EventDock", "DockChanged")
            dictionary.Add("EventDoubleClick", "DoubleClick")
            dictionary.Add("EventDragLeave", "DragLeave")
            dictionary.Add("EventEnabled", "EnabledChanged")
            dictionary.Add("EventEnter", "Enter")
            dictionary.Add("EventFont", "FontChanged")
            dictionary.Add("EventForeColor", "ForeColorChanged")
            dictionary.Add("EventGotFocus", "GotFocus")
            dictionary.Add("EventHandleCreated", "HandleCreated")
            dictionary.Add("EventHandleDestroyed", "HandleDestroyed")
            dictionary.Add("EventLeave", "Leave")
            dictionary.Add("EventLocation", "LocationChanged")
            dictionary.Add("EventLostFocus", "LostFocus")
            dictionary.Add("EventMarginChanged", "MarginChanged")
            dictionary.Add("EventMouseCaptureChanged", "MouseCaptureChanged")
            dictionary.Add("EventMouseEnter", "MouseEnter")
            dictionary.Add("EventMouseHover", "MouseHover")
            dictionary.Add("EventMouseLeave", "MouseLeave")
            dictionary.Add("EventMove", "Move")
            dictionary.Add("EventPaddingChanged", "PaddingChanged")
            dictionary.Add("EventParent", "ParentChanged")
            dictionary.Add("EventRegionChanged", "RegionChanged")
            dictionary.Add("EventResize", "Resize")
            dictionary.Add("EventRightToLeft", "RightToLeftChanged")
            dictionary.Add("EventSize", "SizeChanged")
            dictionary.Add("EventStyleChanged", "StyleChanged")
            dictionary.Add("EventSystemColorsChanged", "SystemColorsChanged")
            dictionary.Add("EventTabIndex", "TabIndexChanged")
            dictionary.Add("EventTabStop", "TabStopChanged")
            dictionary.Add("EventText", "TextChanged")
            dictionary.Add("EventValidated", "Validated")
            dictionary.Add("EventVisible", "VisibleChanged")
            Me._keyEventNameMapping = dictionary
        End Sub

        Public Function GetHandlersFrom(ctrl As Control) As Dictionary(Of String, Dictionary(Of Object, [Delegate]()))
            Dim eventHandlersList As EventHandlerList = DirectCast(GetType(Control).GetProperty("Events", (BindingFlags.GetProperty Or (BindingFlags.NonPublic Or BindingFlags.Instance))).GetValue(ctrl, Nothing), EventHandlerList)
            Dim field As FieldInfo = GetType(EventHandlerList).GetField("head", (BindingFlags.NonPublic Or BindingFlags.Instance))
            Dim source As Dictionary(Of Object, [Delegate]()) = Me.BuildList(field, eventHandlersList)
            Dim eventNameFromKey As String = Me.GetEventNameFromKey(ctrl, Enumerable.First(Of KeyValuePair(Of Object, [Delegate]()))(source).Key)
            Dim dictionary3 As New Dictionary(Of String, Dictionary(Of Object, [Delegate]()))
            dictionary3.Add(eventNameFromKey, source)
            Return dictionary3
        End Function

        Private Function GetEventNameFromKey(ctrl As Control, key As Object) As String
            Dim str As String
            For Each str In Me.KeyEventNameMapping.Keys
                Dim field As FieldInfo = GetType(Control).GetField(str, (BindingFlags.GetField Or (BindingFlags.NonPublic Or (BindingFlags.Static Or BindingFlags.DeclaredOnly))))
                If ((Not field Is Nothing) AndAlso (field.GetValue(ctrl) Is key)) Then
                    Return Me.KeyEventNameMapping.Item(str)
                End If
            Next
            Return Nothing
        End Function

        Private Function BuildList(headInfo As FieldInfo, eventHandlersList As Object) As Dictionary(Of Object, [Delegate]())
            Dim dict As New Dictionary(Of Object, [Delegate]())
            Dim entry As Object = headInfo.GetValue(eventHandlersList)
            If (Not entry Is Nothing) Then
                Dim type As Type = entry.GetType
                Dim field As FieldInfo = type.GetField("handler", (BindingFlags.NonPublic Or BindingFlags.Instance))
                Dim keyInfo As FieldInfo = type.GetField("key", (BindingFlags.NonPublic Or BindingFlags.Instance))
                Dim nextKeyInfo As FieldInfo = type.GetField("next", (BindingFlags.NonPublic Or BindingFlags.Instance))
                dict = Me.BuildListWalk(dict, entry, field, keyInfo, nextKeyInfo)
            End If
            Return dict
        End Function

        Private Function BuildListWalk(dict As Dictionary(Of Object, [Delegate]()), entry As Object, delegateInfo As FieldInfo, keyInfo As FieldInfo, nextKeyInfo As FieldInfo) As Dictionary(Of Object, [Delegate]())
            If (Not entry Is Nothing) Then
                Dim delegate2 As [Delegate] = DirectCast(delegateInfo.GetValue(entry), [Delegate])
                Dim key As Object = keyInfo.GetValue(entry)
                Dim obj3 As Object = nextKeyInfo.GetValue(entry)
                If (Not delegate2 Is Nothing) Then
                    Dim invocationList As [Delegate]() = delegate2.GetInvocationList
                    If (invocationList.Length > 0) Then
                        dict.Add(key, invocationList)
                    End If
                End If
                If (Not obj3 Is Nothing) Then
                    dict = Me.BuildListWalk(dict, obj3, delegateInfo, keyInfo, nextKeyInfo)
                End If
            End If
            Return dict
        End Function
    End Class
End Namespace
