﻿Imports System.Reflection
Imports System.Text.RegularExpressions

Namespace Misc
    ''' <summary>
    ''' Perform secret code parsing.
    ''' </summary>
    ''' <remarks>Read CodeList example and usage example in the source code footer note.</remarks>
    Public Class SecretCodesHelper 'max secret code length is 15 chars
        Private Shared SecretCodeKeyIn As String = String.Empty
        Private Shared PredictKeyInValue As String = String.Empty
        Private Shared LastPredictValue As String = String.Empty

        ''' <summary>
        ''' <para>Return matching keyed in secret code.</para>
        ''' <para>CodeList must be a class with constants as its codes.</para>
        ''' </summary>
        Public Shared Function GetSecretCodes(KeyInput As String, ByVal CodeList As Type) As String
            Try
                SecretCodeKeyIn = SecretCodeKeyIn & KeyInput.ToUpper

                'Check for new key prediction
                SetPredictKeyIn(SecretCodeKeyIn, CodeList)

                'check if last prediction is difference from new one
                'if yes, then replace LastPredictValue to new and 
                ' set reset SecretCodeKeyIn to only the last character 
                ' (last user input which was recognized for PredictKeyInValue)
                If LastPredictValue <> PredictKeyInValue Then
                    LastPredictValue = PredictKeyInValue
                    If String.IsNullOrEmpty(PredictKeyInValue) Then
                        SecretCodeKeyIn = Microsoft.VisualBasic.Strings.Right(SecretCodeKeyIn, 1)
                    End If
                End If

                'If Debugger.IsAttached Then
                '    Console.WriteLine("********************************")
                '    Console.WriteLine("Prediction: " & PredictKeyInValue)
                '    Console.WriteLine("UserInput : " & SecretCodeKeyIn)
                'End If

                If SecretCodeKeyIn.ToUpper.EndsWith(PredictKeyInValue.ToUpper) Then
                    Dim bufferCode As String = PredictKeyInValue
                    PredictKeyInValue = String.Empty
                    LastPredictValue = String.Empty
                    SecretCodeKeyIn = String.Empty
                    Return bufferCode
                Else
                    If String.IsNullOrEmpty(PredictKeyInValue) Then
                        SecretCodeKeyIn = String.Empty
                    End If
                End If

            Catch ex As Exception
                'Clear input and prediction on release and prompt assert message when debug.
                Debug.Assert(False, ex.ToString)
                PredictKeyInValue = String.Empty
                LastPredictValue = String.Empty
                SecretCodeKeyIn = String.Empty
            End Try

            Return String.Empty
        End Function

        ''' <summary>
        ''' Compare against all possible Half Pyramid Format and find the most matching code.
        ''' </summary>
        Private Shared Sub SetPredictKeyIn(ByVal UserInput As String, ByVal CodeList As Type)
            Dim SecretCodeKeyInRightLength As Integer = 0
            Dim MatchCount As Integer = 0 'contains the current matching prediction length
            Dim CodeListString() As String = Ape.EL.Misc.ClassHelper.GetStringsFromClassConstants(CodeList)
            System.Array.Sort(Of String)(CodeListString) 'sort CodeListString

            'Check if old prediction is still correct with the new one
            If Not String.IsNullOrEmpty(PredictKeyInValue) Then
                If UserInput <> GetHalfPyramidFormat(PredictKeyInValue)(UserInput.Length - 1) Then
                    PredictKeyInValue = String.Empty
                End If
            End If

            'Predict new key if PredictKeyInValue is empty
            If String.IsNullOrEmpty(PredictKeyInValue) Then
                'Loop through each CodeListString
                'Get the Half Pyramid Format from the code
                'Compare whether the user input match any of Half Pyramid format
                'If match, check if the new match is greater than previous MatchCount
                'If yes, then update PredictKeyInValue
                For Each MyCode As String In CodeListString
                    SecretCodeKeyInRightLength = 0
                    Dim MyPyramid() As String = GetHalfPyramidFormat(MyCode)
                    Dim strval As String = String.Empty

                    For Each pystring As String In MyPyramid
                        If UserInput = pystring Then
                            SecretCodeKeyInRightLength = 0
                            strval = pystring
                            Exit For
                        End If

                        If String.IsNullOrEmpty(strval) Then
                            For i As Integer = 1 To UserInput.Length
                                If pystring.Length <= i Then
                                    If Microsoft.VisualBasic.Strings.Right(UserInput, pystring.Length) = pystring Then
                                        SecretCodeKeyInRightLength = pystring.Length
                                        strval = pystring
                                        Exit For
                                    End If
                                End If
                            Next
                        End If
                    Next

                    If strval.Count > MatchCount Then
                        If SecretCodeKeyInRightLength > 0 Then
                            SecretCodeKeyIn = Microsoft.VisualBasic.Strings.Right(UserInput, SecretCodeKeyInRightLength)
                        End If

                        MatchCount = strval.Count
                        PredictKeyInValue = MyCode
                    End If
                Next
            End If
        End Sub

        ''' <summary>
        ''' <para>This function returns something like this:</para>
        ''' <para>Input value: ATARI</para>
        ''' <para>Result (in a string array):</para>
        ''' <para>A</para>
        ''' <para>AT</para>
        ''' <para>ATA</para>
        ''' <para>ATAR</para>
        ''' <para>ATARI</para>
        ''' </summary>
        Private Shared Function GetHalfPyramidFormat(ByVal LiteralString As String) As String()
            Dim MyPyramid As New List(Of String)()
            If Not String.IsNullOrEmpty(LiteralString) Then
                For i As Integer = 1 To LiteralString.Length
                    MyPyramid.Add(Microsoft.VisualBasic.Strings.Left(LiteralString, i))
                Next
            End If
            Return MyPyramid.ToArray
        End Function
    End Class
End Namespace


'Dim strval As String = Regex.Match(code, _
'                                       "matching pattern" & s, _
'                                        RegexOptions.IgnoreCase).Value


'Footer note:
'
' CodeList sample
'
'Class Codes
'    Public Const ConfigDecryptCode As String = "MYPASS"
'    Public Const AllowChangeCatalogueCode As String = "ATARI000"
'End Class
'
' Implementation sample
'
' (In form's KeyDown)
'Dim SecretCodeFeature As String = Ape.EL.Misc.SecretCodesHelper.GetSecretCodes(e.KeyChar.ToString.ToUpper, GetType(Ape.EL.Program.Misc.SecretCodes.Codes))
'Select Case SecretCodeFeature
'    Case Ape.EL.Program.Misc.SecretCodes.Codes.ConfigDecryptCode
'        UI.Utility.DataTool.Preview(MySetting.GetDataViewEx(Setting.MyConfig.MyConfigFilename))
'    Case Ape.EL.Program.Misc.SecretCodes.Codes.AllowChangeCatalogueCode
'        txtDbCatalogue.Properties.ReadOnly = False
'End Select