﻿Imports System.Windows.Forms

Namespace Interfaces
    Public Interface IDialogBox
        ''' <summary>
        ''' Displays a message box in front of the specified object and with the specified text, caption, buttons, icon, and default button.
        ''' </summary>
        ''' <param name="owner">An implementation of System.Windows.Forms.IWin32Window that will own the modal dialog box.</param>
        ''' <param name="text">The text to display in the message box.</param>
        ''' <param name="caption">The text to display in the title bar of the message box.</param>
        ''' <param name="buttons">One of the System.Windows.Forms.MessageBoxButtons values that specifies which buttons to display in the message box.</param>
        ''' <param name="icon">One of the System.Windows.Forms.MessageBoxIcon values that specifies which icon to display in the message box.</param>
        ''' <param name="defaultButton">One of the System.Windows.Forms.MessageBoxDefaultButton values that specifies the default button for the message box.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Function Show(ByVal owner As IWin32Window, _
                      ByVal text As String, _
                      ByVal caption As String, _
                      ByVal buttons As MessageBoxButtons, _
                      ByVal icon As MessageBoxIcon, _
                      ByVal defaultButton As MessageBoxDefaultButton) As DialogResult
    End Interface
End Namespace
