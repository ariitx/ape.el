﻿Imports System.Reflection
Imports System.ComponentModel

Namespace Reflex
    Public Class PropertyHelper
        ''' <summary>
        ''' Returns a _private_ Property Value from a given Object. Uses Reflection.
        ''' Throws a ArgumentOutOfRangeException if the Property is not found.
        ''' </summary>
        ''' <typeparam name="T">Type of the Property</typeparam>
        ''' <param name="obj">Object from where the Property Value is returned</param>
        ''' <param name="propName">Propertyname as string.</param>
        ''' <returns>PropertyValue</returns>
        Public Shared Function GetPrivatePropertyValue(Of T)(obj As Object, propName As String) As T
            If obj Is Nothing Then
                Throw New ArgumentNullException("obj")
            End If
            Dim pi As PropertyInfo = obj.[GetType]().GetProperty(propName, BindingFlags.[Public] Or BindingFlags.NonPublic Or BindingFlags.Instance)
            If pi Is Nothing Then
                Throw New ArgumentOutOfRangeException("propName", String.Format("Property {0} was not found in Type {1}", propName, obj.[GetType]().FullName))
            End If
            Return DirectCast(pi.GetValue(obj, Nothing), T)
        End Function

        ''' <summary>
        ''' Returns a private Property Value from a given Object. Uses Reflection.
        ''' Throws a ArgumentOutOfRangeException if the Property is not found.
        ''' </summary>
        ''' <typeparam name="T">Type of the Property</typeparam>
        ''' <param name="obj">Object from where the Property Value is returned</param>
        ''' <param name="propName">Propertyname as string.</param>
        ''' <returns>PropertyValue</returns>
        Public Shared Function GetPrivateFieldValue(Of T)(obj As Object, propName As String) As T
            If obj Is Nothing Then
                Throw New ArgumentNullException("obj")
            End If
            Dim tt As Type = obj.[GetType]()
            Dim fi As FieldInfo = Nothing
            While fi Is Nothing AndAlso tt IsNot Nothing
                fi = tt.GetField(propName, BindingFlags.[Public] Or BindingFlags.NonPublic Or BindingFlags.Instance)
                tt = tt.BaseType
            End While
            If fi Is Nothing Then
                Throw New ArgumentOutOfRangeException("propName", String.Format("Field {0} was not found in Type {1}", propName, obj.[GetType]().FullName))
            End If
            Return DirectCast(fi.GetValue(obj), T)
        End Function

        ''' <summary>
        ''' Sets a _private_ Property Value from a given Object. Uses Reflection.
        ''' Throws a ArgumentOutOfRangeException if the Property is not found.
        ''' </summary>
        ''' <typeparam name="T">Type of the Property</typeparam>
        ''' <param name="obj">Object from where the Property Value is set</param>
        ''' <param name="propName">Propertyname as string.</param>
        ''' <param name="val">Value to set.</param>
        Public Shared Sub SetPrivatePropertyValue(Of T)(obj As Object, propName As String, val As T)
            Dim tt As Type = obj.[GetType]()
            If tt.GetProperty(propName, BindingFlags.[Public] Or BindingFlags.NonPublic Or BindingFlags.Instance) Is Nothing Then
                Throw New ArgumentOutOfRangeException("propName", String.Format("Property {0} was not found in Type {1}", propName, obj.[GetType]().FullName))
            End If
            tt.InvokeMember(propName, BindingFlags.[Public] Or BindingFlags.NonPublic Or BindingFlags.SetProperty Or BindingFlags.Instance, Nothing, obj, New Object() {val})
        End Sub

        ''' <summary>
        ''' Set a private Property Value on a given Object. Uses Reflection.
        ''' </summary>
        ''' <typeparam name="T">Type of the Property</typeparam>
        ''' <param name="obj">Object from where the Property Value is returned</param>
        ''' <param name="propName">Propertyname as string.</param>
        ''' <param name="val">the value to set</param>
        ''' <exception cref="ArgumentOutOfRangeException">if the Property is not found</exception>
        Public Shared Sub SetPrivateFieldValue(Of T)(obj As Object, propName As String, val As T)
            If obj Is Nothing Then
                Throw New ArgumentNullException("obj")
            End If
            Dim tt As Type = obj.[GetType]()
            Dim fi As FieldInfo = Nothing
            While fi Is Nothing AndAlso tt IsNot Nothing
                fi = tt.GetField(propName, BindingFlags.[Public] Or BindingFlags.NonPublic Or BindingFlags.Instance)
                tt = tt.BaseType
            End While
            If fi Is Nothing Then
                Throw New ArgumentOutOfRangeException("propName", String.Format("Field {0} was not found in Type {1}", propName, obj.[GetType]().FullName))
            End If
            fi.SetValue(obj, val)
        End Sub

        ''' <summary>
        ''' Invoke a method or function out of an instance.
        ''' </summary>
        ''' <param name="o"></param>
        ''' <param name="methodName"></param>
        ''' <param name="args"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Invoke(Of T)(o As Object, methodName As String, ParamArray args As Object()) As T
            Dim mi = o.[GetType]().GetMethod(methodName, BindingFlags.[Public] Or BindingFlags.NonPublic Or BindingFlags.Instance Or BindingFlags.Static)
            If mi IsNot Nothing Then
                Try
                    Return DirectCast(mi.Invoke(o, args), T)
                Catch ex As Exception
                    Throw ex.InnerException
                End Try
            End If
            Return Nothing
        End Function

        ''' <summary>
        ''' Invoke a static hidden or public function.
        ''' </summary>
        ''' <typeparam name="T">Return object type.</typeparam>
        ''' <param name="type">Object type that contains the method or function.</param>
        ''' <param name="methodName">The method name.</param>
        ''' <param name="args"></param>
        Public Shared Function Invoke(Of T)(type As Type, methodName As String, ParamArray args As Object()) As T
            Dim mi = type.GetMethod(methodName, BindingFlags.Public Or BindingFlags.NonPublic Or BindingFlags.Instance Or BindingFlags.Static)
            If mi IsNot Nothing Then
                Try
                    Return DirectCast(mi.Invoke(Nothing, args), T)
                Catch ex As Exception
                    Throw ex.InnerException
                End Try
            End If
            Return Nothing
        End Function

        ''' <summary>
        ''' Create a datatable based on list of visible properties in a type.
        ''' </summary>
        Public Shared Function CreateDataTable(Of T)(Optional ByVal datalist As List(Of T) = Nothing) As DataTable
            If datalist Is Nothing Then datalist = New List(Of T)

            Dim dt As New DataTable

            For Each p As System.Reflection.PropertyInfo In GetType(T).GetProperties()
                If p.CanRead Then
                    Try
                        dt.Columns.Add(p.Name, p.PropertyType)
                    Catch ex As Exception
                        Debug.Assert(False, ex.ToString)
                    End Try
                End If
            Next

            Dim dv As DataView = Misc.DataHelper.ConvertToDataView(dt)

            For Each dat As T In datalist
                Try

                    Dim drv As DataRowView = dv.AddNew
                    drv.BeginEdit()
                    For Each col As DataColumn In dt.Columns
                        Dim o As Object = GetPrivatePropertyValue(Of Object)(dat, col.ColumnName)
                        If o IsNot Nothing Then
                            drv(col.ColumnName) = o
                        End If
                    Next
                    drv.EndEdit()
                Catch ex As Exception
                    Ape.EL.WinForm.General.ExceptionErrorMsg(ex)
                End Try
            Next

            Return Misc.DataHelper.ConvertToDataTable(dv)
        End Function

        ''' <summary>
        ''' Create a dataview based on list of visible properties in a type.
        ''' </summary>
        Public Shared Function CreateDataView(Of T)(Optional ByVal datalist As List(Of T) = Nothing) As DataView
            Dim dv As DataView = Misc.DataHelper.ConvertToDataView(CreateDataTable(Of T)(datalist))
            Return dv
        End Function

        Public Shared Function CreateListOfT(Of T)(ByVal dv As DataView) As List(Of T)
            Dim li As New List(Of T)

            For Each drv As DataRowView In dv
                li.Add(Ape.EL.Data.Convert.DrvToStructure(drv, GetType(T)))
            Next

            Return li
        End Function

        ''' <summary>
        ''' Get value of private static field.
        ''' </summary>
        ''' <typeparam name="T"></typeparam>
        ''' <param name="typ"></param>
        ''' <param name="fieldName"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetPrivateStaticField(Of T)(typ As Type, fieldName As String) As T
            Dim tt As Type = typ
            Dim fi As FieldInfo = Nothing
            While fi Is Nothing AndAlso tt IsNot Nothing
                fi = tt.GetField(fieldName, BindingFlags.[Public] Or BindingFlags.NonPublic Or BindingFlags.Static)
                tt = tt.BaseType
            End While
            If fi Is Nothing Then
                Throw New ArgumentOutOfRangeException("fieldName", String.Format("Field {0} was not found in Type {1}", fieldName, typ.FullName))
            End If
            Return DirectCast(fi.GetValue(Nothing), T)
        End Function

        ''' <summary>
        ''' Set value of private static field.
        ''' </summary>
        ''' <typeparam name="T"></typeparam>
        ''' <param name="typ"></param>
        ''' <param name="fieldName"></param>
        ''' <param name="val"></param>
        ''' <remarks></remarks>
        Public Shared Sub SetPrivateStaticField(Of T)(typ As Type, fieldName As String, val As T)
            Dim tt As Type = typ
            Dim fi As FieldInfo = Nothing
            While fi Is Nothing AndAlso tt IsNot Nothing
                fi = tt.GetField(fieldName, BindingFlags.[Public] Or BindingFlags.NonPublic Or BindingFlags.Static)
                tt = tt.BaseType
            End While
            If fi Is Nothing Then
                Throw New ArgumentOutOfRangeException("fieldName", String.Format("Field {0} was not found in Type {1}", fieldName, typ.FullName))
            End If
            fi.SetValue(Nothing, val)
        End Sub

        ''' <summary>
        ''' Helper method to retrieve control properties.
        ''' Use of GetProperties enables undo and menu updates to work properly.
        ''' Use GetPropertyByName("PropName").SetValue(obj, value) to set value to the object.
        ''' </summary>
        Public Shared Function GetPropertyByName(obj As Object, ByVal propName As String) As PropertyDescriptor
            Dim prop As PropertyDescriptor
            prop = TypeDescriptor.GetProperties(obj)(propName)
            If prop Is Nothing Then
                Throw New ArgumentException("Matching property not found!", propName)
            Else
                Return prop
            End If
        End Function
    End Class
End Namespace
