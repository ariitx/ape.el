﻿Namespace Reflex
    Public Class ObjectHelper
        ''' <summary>
        ''' Get default instance of the object type using default constructor.
        ''' </summary>
        Public Shared Function GetDefaultObject(type As System.Type) As Object
            Return System.Activator.CreateInstance(type)
        End Function

        ''' <summary>
        ''' Get uninitialized instance of the type.
        ''' </summary>
        Public Shared Function GetUninitializedObject(type As System.Type) As Object
            Return Runtime.Serialization.FormatterServices.GetUninitializedObject(type)
        End Function

        ''' <summary>
        ''' Get list of functions and methods of a class in string.
        ''' </summary>
        Public Shared Function GetMethodNameList(type As System.Type) As List(Of String)
            Return type.GetMethods.Select(Of String)(Function(x) (x.Name)).ToList
        End Function

        ''' <summary>
        ''' Get list of constant from a class in string.
        ''' </summary>
        Public Shared Function GetConstantValueList(ByVal type As System.Type) As List(Of String)
            Dim constants As New ArrayList()
            Dim fieldInfos As Reflection.FieldInfo() = type.GetFields(Reflection.BindingFlags.[Public] Or _
                Reflection.BindingFlags.[Static] Or Reflection.BindingFlags.FlattenHierarchy)

            For Each fi As Reflection.FieldInfo In fieldInfos
                If fi.IsLiteral AndAlso Not fi.IsInitOnly Then
                    constants.Add(fi)
                End If
            Next

            Dim ConstantsStringArray As New System.Collections.Specialized.StringCollection

            For Each fi As Reflection.FieldInfo In _
                DirectCast(constants.ToArray(GetType(Reflection.FieldInfo)), Reflection.FieldInfo())
                ConstantsStringArray.Add(fi.GetValue(Nothing))
            Next

            Dim retVal(ConstantsStringArray.Count - 1) As String
            ConstantsStringArray.CopyTo(retVal, 0)

            Return retVal.ToList
        End Function
    End Class
End Namespace
