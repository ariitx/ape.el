﻿Imports System

Namespace Utils
    Public Class Memory
        ' Methods
        Public Shared Function GetObjectSize(ByVal objectCreate As Func(Of Object)) As Long
            Dim obj2 As Object
            Dim num As Long
            Dim num2 As Long
            obj2 = objectCreate.Invoke
            num = GC.GetTotalMemory(1)
            obj2 = objectCreate.Invoke
            num2 = GC.GetTotalMemory(1)
            If (Not obj2 Is Nothing) Then
                goto Label_0022
            End If
            Return 0
        Label_0022:
            Return ((num2 - num) - 8)
        End Function

    End Class
End Namespace

