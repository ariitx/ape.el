﻿<DebuggerStepThrough> _
Public Class Guard
    ' Methods
    Public Shared Sub ArgumentIndexInRange(ByVal value As Integer, ByVal name As String, ByVal rangeLength As Integer, Optional ByVal startIndex As Integer = 0)
        Guard.ArgumentNonNegative(rangeLength, "rangeLength")
        If ((value < startIndex) OrElse (value >= (startIndex + rangeLength))) Then
            If (rangeLength = 0) Then
                Throw New ArgumentOutOfRangeException(name, String.Format("Index={0} is out of range {{}}", New Object(0 - 1) {}))
            End If
            Throw New ArgumentOutOfRangeException(name, String.Format("Index={0} is out of range [{1} ... {2}].", value, startIndex, (startIndex + rangeLength)))
        End If
    End Sub

    Public Shared Sub ArgumentIndexInRange(ByVal value As Long, ByVal name As String, ByVal rangeLength As Long, Optional ByVal startIndex As Long = 0)
        Guard.ArgumentNonNegative(rangeLength, "rangeLength")
        If ((value < startIndex) OrElse (value >= (startIndex + rangeLength))) Then
            If (rangeLength = 0) Then
                Throw New ArgumentOutOfRangeException(name, String.Format("Index={0} is out of range {{}}", New Object(0 - 1) {}))
            End If
            Throw New ArgumentOutOfRangeException(name, String.Format("Index={0} is out of range [{1} ... {2}].", value, startIndex, (startIndex + rangeLength)))
        End If
    End Sub

    Public Shared Sub ArgumentInRange(ByVal value As Decimal, ByVal name As String, ByVal from As Decimal, ByVal [to] As Decimal)
        If ((value < from) OrElse (value > [to])) Then
            Throw New ArgumentOutOfRangeException(name, String.Format("Argument {0} is out of range [{1} ... {2}].", value, from, [to]))
        End If
    End Sub

    Public Shared Sub ArgumentInRange(ByVal value As Double, ByVal name As String, ByVal from As Double, ByVal [to] As Double)
        If ((value < from) OrElse (value > [to])) Then
            Throw New ArgumentOutOfRangeException(name, String.Format("Argument {0} is out of range [{1} ... {2}].", value, from, [to]))
        End If
    End Sub

    Public Shared Sub ArgumentInRange(ByVal value As Integer, ByVal name As String, ByVal from As Integer, ByVal [to] As Integer)
        If ((value < from) OrElse (value > [to])) Then
            Throw New ArgumentOutOfRangeException(name, String.Format("Argument {0} is out of range [{1} ... {2}].", value, from, [to]))
        End If
    End Sub

    Public Shared Sub ArgumentInRange(ByVal value As Long, ByVal name As String, ByVal from As Long, ByVal [to] As Long)
        If ((value < from) OrElse (value > [to])) Then
            Throw New ArgumentOutOfRangeException(name, String.Format("Argument {0} is out of range [{1} ... {2}].", value, from, [to]))
        End If
    End Sub

    Public Shared Sub ArgumentInRange(ByVal value As Single, ByVal name As String, ByVal from As Single, ByVal [to] As Single)
        If ((value < from) OrElse (value > [to])) Then
            Throw New ArgumentOutOfRangeException(name, String.Format("Argument {0} is out of range [{1} ... {2}].", value, from, [to]))
        End If
    End Sub

    Public Shared Sub ArgumentIsEnumType(ByVal value As Object, ByVal name As String)
        If (value Is Nothing) Then
            Throw New ArgumentNullException(name)
        End If
        If Not value.GetType.IsEnum Then
            Throw New ArgumentException(String.Format("Argument {0} is not enum type.", value.GetType.Name))
        End If
    End Sub

    Public Shared Sub ArgumentIsEnumType(ByVal value As Type, ByVal name As String)
        If (value Is Nothing) Then
            Throw New ArgumentNullException(name)
        End If
        If Not value.IsEnum Then
            Throw New ArgumentException(String.Format("Argument {0} is not enum type.", value.Name))
        End If
    End Sub

    Public Shared Sub ArgumentIsEnumValue(Of T As Structure)(ByVal value As Object, ByVal name As String)
        If Not [Enum].IsDefined(GetType(T), value) Then
            Throw New ArgumentOutOfRangeException(name, String.Format("Argument {0} is out of enum {1} range.", name, GetType(T).Name))
        End If
    End Sub

    Public Shared Sub ArgumentIsNotNull_WithNotNullItems(Of T As Class)(ByVal value As IEnumerable(Of T), ByVal name As String)
        Guard.ArgumentNotNull(Of IEnumerable(Of T))(value, name)
        Dim local As T
        For Each local In value
            If (local Is Nothing) Then
                Guard.ThrowArgumentException(name, value)
            End If
        Next
    End Sub

    Public Shared Sub ArgumentIsNotNull_WithNotNullOrEmptyItems(ByVal value As IEnumerable(Of String), ByVal name As String)
        Guard.ArgumentNotNull(Of IEnumerable(Of String))(value, name)
        Dim str As String
        For Each str In value
            If String.IsNullOrEmpty(str) Then
                Guard.ThrowArgumentException(name, value)
            End If
        Next
    End Sub

    Public Shared Sub ArgumentIsNotNullOrEmpty(ByVal value As ICollection, ByVal name As String)
        Guard.ArgumentNotNull(Of ICollection)(value, name)
        If (value.Count = 0) Then
            Guard.ThrowArgumentException(name, value)
        End If
    End Sub

    Public Shared Sub ArgumentIsNotNullOrEmpty(ByVal value As IEnumerable, ByVal name As String)
        Guard.ArgumentNotNull(Of IEnumerable)(value, name)
        If Not value.GetEnumerator.MoveNext Then
            Guard.ThrowArgumentException(name, value)
        End If
    End Sub

    Public Shared Sub ArgumentIsNotNullOrEmpty(ByVal value As String, ByVal name As String)
        If String.IsNullOrEmpty(value) Then
            Guard.ThrowArgumentException(name, value)
        End If
    End Sub

    Public Shared Sub ArgumentIsNotNullOrEmpty_WithNotNullItems(Of T As Class)(ByVal value As IEnumerable(Of T), ByVal name As String)
        Guard.ArgumentNotNull(Of IEnumerable(Of T))(value, name)
        Dim flag As Boolean = False
        Dim local As T
        For Each local In value
            If (local Is Nothing) Then
                Guard.ThrowArgumentException(name, value)
            Else
                flag = True
            End If
        Next
        If Not flag Then
            Guard.ThrowArgumentException(name, value)
        End If
    End Sub

    Public Shared Sub ArgumentIsNotNullOrEmpty_WithNotNullOrEmptyItems(ByVal value As IEnumerable(Of String), ByVal name As String)
        Guard.ArgumentNotNull(Of IEnumerable(Of String))(value, name)
        Dim flag As Boolean = False
        Dim str As String
        For Each str In value
            If String.IsNullOrEmpty(str) Then
                Guard.ThrowArgumentException(name, value)
            Else
                flag = True
            End If
        Next
        If Not flag Then
            Guard.ThrowArgumentException(name, value)
        End If
    End Sub

    Public Shared Sub ArgumentIsNotNullOrWhiteSpace(ByVal value As String, ByVal name As String)
        If String.IsNullOrWhiteSpace(value) Then
            Guard.ThrowArgumentException(name, value)
        End If
    End Sub

    Public Shared Sub ArgumentIsType(Of T)(ByVal value As Object, ByVal name As String)
        If Not TypeOf value Is T Then
            Throw New ArgumentException(String.Format("Argument is not type of {0}.", GetType(T).Name), name)
        End If
    End Sub

    Public Shared Sub ArgumentIsValidDirectoryName(ByVal value As String, ByVal name As String)
        If Not PathHelper.IsValidDirectoryName(value) Then
            Guard.ThrowArgumentException(name, value)
        End If
    End Sub

    Public Shared Sub ArgumentIsValidFileName(ByVal value As String, ByVal name As String)
        If Not PathHelper.IsValidFileName(value) Then
            Guard.ThrowArgumentException(name, value)
        End If
    End Sub

    Public Shared Sub ArgumentIsValidFullFileName(ByVal value As String, ByVal name As String)
        If Not PathHelper.IsValidFullFileName(value) Then
            Guard.ThrowArgumentException(name, value)
        End If
    End Sub

    Public Shared Sub ArgumentIsValidPropertyName(ByVal value As String, ByVal name As String)
        If ((String.IsNullOrEmpty(value) AndAlso Not Char.IsLetter(value.Chars(0))) AndAlso (value.Chars(0) <> "_"c)) Then
            Guard.ThrowArgumentException(name, value)
        End If
        Dim i As Integer
        For i = 1 To value.Length - 1
            If (Not Char.IsLetterOrDigit(value.Chars(i)) AndAlso (value.Chars(i) <> "_"c)) Then
                Guard.ThrowArgumentException(name, value)
            End If
        Next i
    End Sub

    Public Shared Sub ArgumentNonNegative(ByVal value As Decimal, ByVal name As String)
        If (value < 0) Then
            Guard.ThrowArgumentException(name, value)
        End If
    End Sub

    Public Shared Sub ArgumentNonNegative(ByVal value As Double, ByVal name As String)
        If (value < 0) Then
            Guard.ThrowArgumentException(name, value)
        End If
    End Sub

    Public Shared Sub ArgumentNonNegative(ByVal value As Short, ByVal name As String)
        If (value < 0) Then
            Guard.ThrowArgumentException(name, value)
        End If
    End Sub

    Public Shared Sub ArgumentNonNegative(ByVal value As Integer, ByVal name As String)
        If (value < 0) Then
            Guard.ThrowArgumentException(name, value)
        End If
    End Sub

    Public Shared Sub ArgumentNonNegative(ByVal value As Long, ByVal name As String)
        If (value < 0) Then
            Guard.ThrowArgumentException(name, value)
        End If
    End Sub

    Public Shared Sub ArgumentNonNegative(ByVal value As Single, ByVal name As String)
        If (value < 0.0!) Then
            Guard.ThrowArgumentException(name, value)
        End If
    End Sub

    Public Shared Sub ArgumentNotNull(ByVal value As Object, ByVal name As String)
        If Object.ReferenceEquals(value, Nothing) Then
            Guard.ThrowArgumentNullException(name)
        End If
    End Sub

    Public Shared Sub ArgumentNotNull(Of T)(ByVal value As T, ByVal name As String)
        If Object.ReferenceEquals(value, Nothing) Then
            Guard.ThrowArgumentNullException(name)
        End If
    End Sub

    Public Shared Sub ArgumentPositive(ByVal value As Decimal, ByVal name As String)
        If (value <= 0.0) Then
            Guard.ThrowArgumentException(name, value)
        End If
    End Sub

    Public Shared Sub ArgumentPositive(ByVal value As Double, ByVal name As String)
        If (value <= 0) Then
            Guard.ThrowArgumentException(name, value)
        End If
    End Sub

    Public Shared Sub ArgumentPositive(ByVal value As Integer, ByVal name As String)
        If (value <= 0) Then
            Guard.ThrowArgumentException(name, value)
        End If
    End Sub

    Public Shared Sub ArgumentPositive(ByVal value As Long, ByVal name As String)
        If (value <= 0) Then
            Guard.ThrowArgumentException(name, value)
        End If
    End Sub

    Public Shared Sub ArgumentPositive(ByVal value As Single, ByVal name As String)
        If (value <= 0.0!) Then
            Guard.ThrowArgumentException(name, value)
        End If
    End Sub

    Private Shared Sub ThrowArgumentException(ByVal propName As String, ByVal val As Object)
        Dim str As String = If(Object.ReferenceEquals(val, String.Empty), "String.Empty", If((val Is Nothing), "null", val.ToString))
        Throw New ArgumentException(String.Format("'{0}' is not a valid value for '{1}'", str, propName))
    End Sub

    Private Shared Sub ThrowArgumentNullException(ByVal propName As String)
        Throw New ArgumentNullException(propName)
    End Sub

    Public Shared Sub ThrowNotSupportedArgumentValue(ByVal value As Object, ByVal name As String)
        Throw New ArgumentException(String.Format("'{0}' is not supported value for argument '{1}'.", If(value <> Nothing, value, String.Empty), name))
    End Sub

End Class
