Imports Ape.EL.Internal
Imports System.IO
Imports Ape.EL.InternalGlobal

Namespace Setting
    'To change pass: Ape.EL.InternalGlobal, change private field IntegralValMod value using reflection
    'To change path: Update Path property

    ' Any code changes to this class is very critical to users experience. Please think twice before editing anything.
    ' Last updated on 23 Dec 2016 - Ari

    ''' <summary>
    ''' Registry like setting.
    ''' </summary>
#If DEBUG Then
    Public Class MySetting
#Else
    <System.Diagnostics.DebuggerStepThrough()> _
    Public Class MySetting
#End If
        '        Private ReadOnly lic As Boolean = Internal.CheckMyLicense

        ''' <summary>
        ''' Processing read/write IO, must wait for each other. Otherwise it will cause file access lock.
        ''' The functions that use this _processing to lock must not call each other! (eg. ReadFile must not call WriteFile)
        ''' </summary>
        Private Shared _processing As Boolean

        'usage (key1val1)(key2val2)
        Private Const DmKey As String = Chr(3) '
        Private Const DmVal As String = Chr(2) '

        'Use to escape dmkey and dmval.
        Private Const esc As String = Chr(5)

        ''' <summary>
        ''' GetDataViewEx key column fieldname.
        ''' </summary>
        Public Const STR_DvEx_Key As String = "Key"
        ''' <summary>
        ''' GetDataViewEx setting column fieldname.
        ''' </summary>
        Public Const STR_DvEx_Setting As String = "Setting"

        Private Const m_Salt As String = ")U9e*#+!Wd7t8Ltp3?83a6#Ln$a3/t"

        ''' <summary>
        ''' Get the namespace for the caller assembly. For external use only! Dont use this on this assembly.
        ''' </summary>
        Friend Shared ReadOnly AssemblyNamespace As String = GetAsmNamespace()

        ''' <summary>
        ''' Consume all exception in this class, no exception will be thrown.
        ''' </summary>
        Shared Property ConsumeException As Boolean
            Get
                Return _ConsumeException
            End Get
            Set(value As Boolean)
                _ConsumeException = value
            End Set
        End Property
        Private Shared _ConsumeException As Boolean = False

        ''' <summary>
        ''' Get path of the setting file.
        ''' </summary>
        Shared Property Path() As String
            Get
                Return _Path
            End Get
            Set(ByVal value As String)
                If Ape.EL.Utility.General.IsValidFileNameOrPath(value) Then _
                Ape.EL.Utility.General.MakePath(value)

                If Directory.Exists(value) Then
                    _Path = value
                Else
                    Throw New Exception("Invalid path exception. Path doesnt exist.")
                End If
            End Set
        End Property
        Private Shared _Path As String = String.Format("{0}\{1}\MySettings", Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), GetAsmNamespace())

        Shared ReadOnly Property LayoutPath As String
            Get
                Return String.Format("{0}\{1}", Path, "Layout")
            End Get
        End Property

        ''' <summary>
        ''' Reset the folder path of the setting files if Path properties has ever been updated.
        ''' </summary>
        ''' <remarks></remarks>
        Shared Sub ResetPath()
            _Path = String.Format("{0}\{1}\MySettings", Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), GetAsmNamespace())
        End Sub

        Private Shared Function ReadSection(ByVal Section As String, Optional ByVal DeleteCorrupted As Boolean = True) As String
            If String.IsNullOrEmpty(Section) Then
                Throw New Exception("Empty Section parameter.")
            End If

            Dim FilePath As String = String.Format("{0}\{1}", _Path, Section)

            Return ReadFile(FilePath, DeleteCorrupted)
        End Function

        Private Shared Function ReadFile(ByVal FilePath As String, Optional ByVal DeleteCorrupted As Boolean = True, Optional MyIntegralVal As String = "") As String
            While _processing
                Threading.Thread.Sleep(1)
            End While
            _processing = True

            Try
                If MyIntegralVal.IsEmpty Then MyIntegralVal = IntegralVal Else MyIntegralVal = NewIntegralVal(MyIntegralVal)
                Dim dirPath As String = System.IO.Path.GetDirectoryName(FilePath)
                Dim Section As String = System.IO.Path.GetFileName(FilePath)

                Dim content As String = String.Empty
                Dim sr As StreamReader = Nothing
                Dim count As Integer = 0
                Dim exthrow As Exception = Nothing

                'Try for 3 times before give up and throw an exception
                While count < 3
                    Try
                        If System.IO.File.Exists(FilePath) Then
                            sr = New StreamReader(FilePath)
                            content = sr.ReadToEnd()

                            ''Decryption of string can be applied here [TEST]
                            Dim str() As String = content.Split(vbCrLf) 'Split the content to 2 parts
                            Dim tmpChkSum As String = str(0) 'First Line contains the decrypted content checksum
                            Dim tmpValues As String = str(1) 'Second Line contains the encrypted content 
                            content = Utility.Data.Crypt.Decrypt(tmpValues, Utility.General.GetGUID(1, 1, Utility.Data.Crypt.GetHash(MyIntegralVal))) 'decrypt the Second Line

                            'Compare the checksum, if different throw InvalidCheckSumException
                            If tmpChkSum <> Utility.Data.Crypt.GetHash(content, m_Salt) Then
                                content = String.Empty
                                Throw New InvalidCheckSumException
                            End If

                            'Convert string to byte array
                            Dim bytConfigFile As Byte() = System.Text.Encoding.GetEncoding(437).GetBytes(content)

                            'Convert byte array to Config object
                            content = Utility.Data.Crypt.ByteArrayToObject(bytConfigFile)

                        End If

                        'if successful getting the content, previous exthrow must set to null
                        exthrow = Nothing
                    Catch ex As Exception
                        'count number of unsuccessful reads
                        count += 1

                        'set content to nothing
                        content = Nothing

                        'close previous opened file to be deleted
                        If sr IsNot Nothing Then
                            sr.Close()
                        End If

                        'delete the section / file since it is already corrupted. all setting will be gone
                        'deleted section will have backup in DEL directory
                        If DeleteCorrupted AndAlso Not TypeOf ex Is System.IO.IOException Then
                            Debug.Assert(False, ex.Message & vbCrLf & "Corrupted or incorrect password file. File will be deleted.")
                            DeleteFile(FilePath)
                        End If

                        If TypeOf ex Is System.IO.IOException Then
                            'Ari: i have monitored the issue which causes IOException, so far it is only caused by file being accessed by another program / multi-thread
                            '     thus i am assuming that we can simply repeatly try to read until the file is unlocked.
                            count -= 1
                        End If

                        'set the exthrow for later checking
                        exthrow = ex

                        Threading.Thread.Sleep(1)
                    Finally
                        'close previous opened file
                        If sr IsNot Nothing Then
                            sr.Close()
                        End If
                    End Try

                    'exit the loop when no error is set in exthrow
                    If exthrow Is Nothing Then Exit While
                End While

                If exthrow IsNot Nothing Then
                    If Not ConsumeException Then Throw exthrow
                End If

                Return content
            Finally
                _processing = False
            End Try
        End Function

        Private Shared Sub WriteSection(ByVal Section As String, ByVal Key As String, ByVal Setting As String, ByVal Delete As Boolean)
            If String.IsNullOrEmpty(Section) Then
                Throw New Exception("Empty Section parameter.")
            End If
            If String.IsNullOrEmpty(Key) And Delete = False Then
                Throw New Exception("Empty Key parameter.")
            End If

            Dim FilePath As String = String.Format("{0}\{1}", _Path, Section)

            'replace delimiter characters to readable chars for section, key and setting
            Section = Section.Replace(esc, esc & "1").Replace(DmKey, esc & "3").Replace(DmVal, esc & "2").ToString
            Key = If(Delete, Key, Key.Replace(esc, esc & "1").Replace(DmKey, esc & "3").Replace(DmVal, esc & "2").ToString) 'if delete then let it remain as is, no need to remove the DmKey and DmVal
            Setting = Setting.Replace(esc, esc & "1").Replace(DmKey, esc & "3").Replace(DmVal, esc & "2").ToString

            Try
                Dim CurrentValue As String = If(Delete AndAlso String.IsNullOrEmpty(Key), String.Empty, ReadFile(FilePath))
                Dim keys As String() = CurrentValue.Split(DmKey)
                Dim updateKeys As New System.Text.StringBuilder

                If Not (Delete = True And String.IsNullOrEmpty(Key)) Then
                    If keys.Count > 0 Then
                        Dim bExisted As Boolean = False
                        For i As Integer = 0 To keys.Count - 1
                            If keys(i).Split(DmVal)(0) = Key Then
                                If Delete = False Then
                                    If Not String.IsNullOrEmpty(updateKeys.ToString) Then
                                        updateKeys.Append(DmKey)
                                    End If
                                    updateKeys.Append(Key & DmVal & Setting)
                                    bExisted = True
                                End If
                            ElseIf Delete And (keys(i) = DmVal Or keys(i) = DmKey) Then
                                'Do nothing and just remove the key
                            Else
                                If Not String.IsNullOrEmpty(updateKeys.ToString) Then
                                    updateKeys.Append(DmKey)
                                End If
                                updateKeys.Append(keys(i))
                            End If
                        Next
                        If bExisted = False And Delete = False Then
                            If Not String.IsNullOrEmpty(updateKeys.ToString) Then
                                updateKeys.Append(DmKey)
                            End If
                            updateKeys.Append(Key & DmVal & Setting)
                        End If
                    Else
                        updateKeys.Append(Key & DmVal & Setting)
                    End If
                End If

                WriteFile(FilePath, updateKeys.ToString, Delete)
            Catch ex As Exception
                If Not ConsumeException Then Throw ex
            End Try
        End Sub

        Private Shared Sub WriteFile(ByVal FilePath As String, ByVal Content As String, ByVal Delete As Boolean, Optional MyIntegralVal As String = "")
            While _processing
                Threading.Thread.Sleep(1)
            End While
            _processing = True

            Try
                If MyIntegralVal.IsEmpty Then MyIntegralVal = IntegralVal Else MyIntegralVal = NewIntegralVal(MyIntegralVal)
                Dim dirPath As String = System.IO.Path.GetDirectoryName(FilePath)
                Dim Section As String = System.IO.Path.GetFileName(FilePath)

                Try
                    'Create directory if not exist
                    Utility.General.MakePath(System.IO.Path.GetDirectoryName(_Path & "\"))

                    'Convert Config object to byte array
                    Dim bytConfigFile As Byte() = Utility.Data.Crypt.ObjectToByteArray(Content.ToString)

                    'Convert byte array to string
                    Dim strConfigFile As String = System.Text.ASCIIEncoding.GetEncoding(437).GetString(bytConfigFile)

                    ''Encryption of string can be applied here [TEST]
                    Dim tmpChkSum As String = Utility.Data.Crypt.GetHash(strConfigFile, m_Salt)
                    strConfigFile = Utility.Data.Crypt.Encrypt(strConfigFile, Utility.General.GetGUID(1, 1, Utility.Data.Crypt.GetHash(MyIntegralVal))) 'encrypt the content
                    Dim sb As New System.Text.StringBuilder
                    sb.AppendLine(tmpChkSum) 'First Line contains the checksum of decrypted setting
                    sb.Append(strConfigFile) 'Second Line contains the encrypted setting


                    Dim count As Integer = 0
                    Dim exthrow As Exception = Nothing
                    'Try for 3 times before give up and throw an exception
                    While count < 3
                        Try
                            'Start writing into file
                            Using objWriter As New System.IO.StreamWriter(FilePath)
                                objWriter.Write(sb.ToString)
                                objWriter.Close()
                            End Using

                            'Delete file totally if updateKeys is empty
                            If Delete And String.IsNullOrEmpty(Content.ToString) Then
                                Try
                                    Utility.General.MakePath(System.IO.Path.GetDirectoryName(String.Format("{0}\DEL\", _Path)))
                                    System.IO.File.Copy(FilePath, String.Format("{0}\DEL\{1}.BAK", dirPath, Section), True) 'copy the file for backup in DEL directory (always replace)
                                    System.IO.File.Delete(FilePath)
                                Catch ex As Exception
                                    Debug.Assert(False, ex.ToString)
                                End Try
                            End If

                            'if successful, previous exthrow must set to null
                            exthrow = Nothing
                        Catch ex As Exception
                            'count number of unsuccessful reads
                            count += 1

                            'set the exthrow for later checking
                            exthrow = ex

                            Threading.Thread.Sleep(1)
                        Finally

                        End Try
                        'exit the loop when no error is set in exthrow
                        If exthrow Is Nothing Then Exit While
                    End While
                Catch ex As Exception
                    If Not ConsumeException Then Throw ex
                End Try
            Finally
                _processing = False
            End Try
        End Sub

        ''' <summary>
        ''' Delete a section; if key is specified, then only delete the key.
        ''' </summary>
        Shared Sub DeleteSettingEx(ByVal Section As String, Optional ByVal Key As String = "")
            WriteSection(Section, Key, String.Empty, True)
        End Sub

        ''' <summary>
        ''' Save persistent data in a setting file, does not accept chr(3) and chr(2).
        ''' </summary>
        Shared Sub SaveSettingEx(ByVal Section As String, ByVal Key As String, ByVal Setting As String)
            WriteSection(Section, Key, Setting, False)
        End Sub

        'always delete corrupted file
        Shared Function GetSettingEx(ByVal Section As String, ByVal Key As String, Optional ByVal DefaultValue As String = "") As String
            If String.IsNullOrEmpty(Section) Then
                If Ape.EL.App.Debug.IsDebug Then
                    Throw New Exception("Empty Section parameter.")
                End If
                Return DefaultValue
            End If
            If String.IsNullOrEmpty(Key) Then
                If Ape.EL.App.Debug.IsDebug Then
                    Throw New Exception("Empty Key parameter.")
                End If
                Return DefaultValue
            End If

            Dim result As String = DefaultValue
            Try
                If Not System.IO.File.Exists(String.Format("{0}\{1}", _Path, Section)) Then
                    Return DefaultValue
                Else
                    'Start reading from file
                    Dim keys As String() = ReadSection(Section).Split(DmKey)
                    If keys.Count > 0 Then
                        For i As Integer = 0 To keys.Count - 1
                            If keys(i).Split(DmVal)(0) = Key Then
                                Dim val As String = keys(i).Split(DmVal)(1)
                                val = val.Replace(esc & "3", DmKey).Replace(esc & "2", DmVal).Replace(esc & "1", esc)
                                Return val
                            End If
                        Next
                    End If
                End If
            Catch ex As Exception
                If Not ConsumeException Then Throw ex
            End Try
            Return DefaultValue
        End Function

        ''' <summary>
        ''' Return dataview with following columns: Key, Setting . Delete corrupted file whenever necessary.
        ''' </summary>
        Shared Function GetDataViewEx(ByVal Section As String, Optional ByVal DeleteCorrupted As Boolean = True) As DataView
            'Create a datatable to define the columns
            Dim dt As New DataTable
            dt.Columns.Add(STR_DvEx_Key)
            dt.Columns.Add(STR_DvEx_Setting)

            'convert it to dataview
            Dim dv As DataView = Misc.DataHelper.ConvertToDataView(dt)

            'start retrieving data
            Dim CurrentValue As String = ReadSection(Section, DeleteCorrupted)
            Dim keys As String() = CurrentValue.Split(DmKey)

            'inject data to dataview
            If Not String.IsNullOrEmpty(CurrentValue) Then
                If keys.Count > 0 Then
                    For i As Integer = 0 To keys.Count - 1
                        Dim drv As DataRowView = dv.AddNew
                        Dim key As String = keys(i).Split(DmVal)(0)
                        Dim setting As String = keys(i).Split(DmVal)(1)
                        key = key.Replace(esc & "3", DmKey).Replace(esc & "2", DmVal).Replace(esc & "1", esc)
                        setting = setting.Replace(esc & "3", DmKey).Replace(esc & "2", DmVal).Replace(esc & "1", esc)
                        drv.Item(STR_DvEx_Key) = key
                        drv.Item(STR_DvEx_Setting) = setting
                        drv.EndEdit()
                    Next
                End If
            End If

            'return dataview
            Return dv
        End Function

        ''' <summary>
        ''' Create an empty section and get default section name.
        ''' </summary>
        Shared Function CreateSection() As String
            Dim NewSection As String = System.IO.Path.GetFileName(Ape.EL.WinForm.General.GetUniqueFileName(Path, "NewSection"))
            CreateSection(NewSection)
            Return NewSection
        End Function

        ''' <summary>
        ''' Create an empty section with provided Section.
        ''' </summary>
        Shared Sub CreateSection(ByVal Section As String)
            WriteSection(Section, DmKey, String.Empty, False)
        End Sub

        ''' <summary>
        ''' Delete a section / delete the registry file.
        ''' </summary>
        Shared Sub DeleteSection(ByVal Section As String)
            Dim dirPath As String = Path
            If System.IO.Directory.Exists(dirPath) Then
                Dim filePath As String = dirPath & System.IO.Path.DirectorySeparatorChar & Section
                DeleteFile(filePath)
            End If
        End Sub

        Private Shared Sub DeleteFile(ByVal FilePath As String)
            Dim dirPath As String = System.IO.Path.GetDirectoryName(FilePath)
            Try
                If System.IO.Directory.Exists(dirPath) Then
                    Dim Section As String = System.IO.Path.GetFileName(FilePath)
                    If File.Exists(FilePath) Then
                        Utility.General.MakePath(System.IO.Path.GetDirectoryName(String.Format("{0}\DEL\", dirPath)))
                        System.IO.File.Copy(String.Format("{0}\{1}", dirPath, Section), String.Format("{0}\DEL\{1}.BAK", dirPath, Section), True) 'copy the file for backup in DEL directory (always replace)
                        System.IO.File.Delete(FilePath)
                    End If
                End If
            Catch ex As System.IO.IOException
                If Not ConsumeException Then Throw ex
            Catch ex1 As Exception
            End Try
        End Sub

        ''' <summary>
        ''' Get list of string of available sections in Path directory. No test is performed to check whether the sections / files are valid.
        ''' </summary>
        Shared Function GetSectionListFromPath() As String()
            Dim arr As New List(Of String)
            Dim dirpath As String = Path

            If System.IO.Directory.Exists(dirpath) Then
                ' make a reference to a directory
                Dim di As New IO.DirectoryInfo(dirpath) 'GetAppDataDirFullPath & "\"
                Dim diar1 As IO.FileInfo() = di.GetFiles()
                Dim dra As IO.FileInfo

                'list the names of all files in the specified directory
                For Each dra In diar1
                    If System.IO.Path.HasExtension(dra.Name) = False Then
                        arr.Add(dra.Name)
                    End If
                Next
            End If

            Return arr.ToArray()
        End Function

        ''' <summary>
        ''' Change the section name.
        ''' </summary>
        Shared Function RenameSection(ByVal Section As String, ByVal NewSection As String) As Boolean
            Dim dirpath As String = Path
            Try
                If System.IO.Directory.Exists(dirpath) Then

                    Dim filePath As String = dirpath & System.IO.Path.DirectorySeparatorChar & Section
                    If File.Exists(filePath) Then
                        My.Computer.FileSystem.RenameFile(filePath, NewSection)
                    End If

                    Return True
                End If
            Catch ex As System.IO.IOException
                If Not ConsumeException Then Throw ex
            Catch ex1 As Exception
            End Try

            Return False
        End Function

        ''' <summary>
        ''' Change encryption password used on a section (file). Use with care!
        ''' </summary>
        Shared Function UpdatePassword(ByVal Section As String, ByVal OldPwd As String, ByVal NewPwd As String)
            Dim dirpath As String = Path
            Try
                If System.IO.Directory.Exists(dirpath) Then
                    Dim filePath As String = dirpath & System.IO.Path.DirectorySeparatorChar & Section
                    If File.Exists(filePath) Then
                        Dim content As String = ReadFile(filePath, False, OldPwd)
                        WriteFile(filePath, content, False, NewPwd)
                    End If

                    Return True
                End If
            Catch ex As Exception
                If Not ConsumeException Then Throw ex
            End Try

            Return False
        End Function
    End Class
End Namespace
