﻿'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
' This class is an example of singleton implementation
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
' Value is always in string type, i suggest always use
' default conversion to easily remember.
' for eg. boolean type : 
'           Set> value.ToString 
'           Get> Data.Convert.ToBoolean(value)
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Imports Ape.EL.Setting.MySetting

Namespace Setting
    ''' <summary>
    ''' <para>Base Registry provides generic implementation of new MySetting.</para>
    ''' <para>You can directly access the persistent data in the encrypted MySetting file with the descendant of this class.</para>
    ''' </summary>
    ''' <typeparam name="T">Type of a class, will be MySetting's Key; wherelse the class' Namespace will be MySetting's Section.</typeparam>
    ''' <remarks></remarks>
    <Obsolete("Use MySetting", True)>
    Public MustInherit Class BaseRegistry(Of T As New)
        Implements Ape.EL.Interfaces.IRegistry

        ''' <summary>
        ''' Create singleton object of descendant class to inherit functions from this class.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Shared ReadOnly Property Instance() As T
            Get
                If _instance Is Nothing Then
                    _instance = New T()

                End If

                Return _instance
            End Get
        End Property
        Private Shared _instance As T = Nothing

        ''' <summary>
        ''' Get Key of selected registry.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared ReadOnly Property Key() As String
            Get
                Return Instance.ToString.Split("."c).Last.ToString
            End Get
        End Property

        ''' <summary>
        ''' Get Section of selected registry.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared ReadOnly Property Section() As String
            Get
                Dim a As String() = Instance.ToString.Split("."c)
                Return a(a.Count - 2).ToString
            End Get
        End Property

        ''' <summary>
        ''' Get the path to the registry file.
        ''' </summary>
        Public Shared ReadOnly Property PathFile As String
            Get
                Return String.Format("{0}\{1}", Path, Section)
            End Get
        End Property

        ''' <summary>
        ''' Get the default value of selected registry.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared ReadOnly Property DefaultValue As String
            Get
                'get default value if implement IRegistry
                Dim _defaultValue As String = String.Empty
                If GetType(Ape.EL.Interfaces.IRegistry).IsAssignableFrom(Instance.GetType) Then
                    _defaultValue = CType(System.Activator.CreateInstance(Instance.GetType), Ape.EL.Interfaces.IRegistry).MyDefaultValue
                End If
                Return _defaultValue
            End Get
        End Property

        ''' <summary>
        ''' Get or set persistent value to selected registry.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Property Value() As String
            Get
                Return mValue
            End Get
            Set(value As String)
                mValue = value
            End Set
        End Property

        Private Shared Property mValue As String
            Get
                Return GetSettingEx(Section, Key, DefaultValue)
            End Get
            Set(value As String)
                SaveSettingEx(Section, Key, If(value Is Nothing, String.Empty, value))
            End Set
        End Property

        ''' <summary>
        ''' Must override value of selected registry's default value in string.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Protected MustOverride ReadOnly Property MyDefaultValue As String Implements Interfaces.IRegistry.MyDefaultValue

#Region "Converted Data Type Value"
        ''' <summary>
        ''' Get or set Value property as boolean, string must be in "True" or "False" or empty.
        ''' </summary>
        Public Shared Property ValBool As Boolean
            Get
                Return Convert.ToBoolean(mValue)
            End Get
            Set(value As Boolean)
                mValue = value.ToString
            End Set
        End Property

        ''' <summary>
        ''' Get or set Value property as decimal.
        ''' </summary>
        Public Shared Property ValDec As Decimal
            Get
                Return Convert.ToDecimal(mValue)
            End Get
            Set(value As Decimal)
                mValue = value.ToString
            End Set
        End Property

        ''' <summary>
        ''' Get or set Value property as integer.
        ''' </summary>
        Public Shared Property ValInt As Integer
            Get
                Return Convert.ToInt32(mValue)
            End Get
            Set(value As Integer)
                mValue = value.ToString
            End Set
        End Property

        ''' <summary>
        ''' Get or set Value property as Long.
        ''' </summary>
        Public Shared Property ValLong As Long
            Get
                Return Convert.ToInt64(mValue)
            End Get
            Set(value As Long)
                mValue = value.ToString
            End Set
        End Property

        ''' <summary>
        ''' Get or set Value property as datetime.
        ''' </summary>
        Public Shared Property ValDt As DateTime
            Get
                Return Convert.ToDateTime(mValue)
            End Get
            Set(value As DateTime)
                mValue = value.ToString
            End Set
        End Property
#End Region
    End Class
End Namespace
