﻿Imports System

Namespace Setting
    <System.Serializable()>
    Public Class InvalidCheckSumException
        Inherits Ape.EL.App.AppException

        Public Sub New()
            MyBase.New("Invalid setting checksum.")
        End Sub
    End Class
End Namespace