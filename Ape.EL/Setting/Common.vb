﻿Imports Ape.EL.Internal
Imports System.IO

Namespace Setting
    ''' <summary>
    ''' Provides common / default setting names to be used as key in MySetting and other stuffs...
    ''' </summary>
    ''' <remarks></remarks>
    Public Class Common
'        Private ReadOnly lic As Boolean = Internal.CheckMyLicense
        Public Const LoginLogoFile As String = "LoginLogo.gif"

        ''' <summary> Related to enum Config. </summary>
        Public Enum ConfigEdition
            Basic = 0
            Standard = 1
            Advance = 2
        End Enum

        ''' <summary> Related to enum Config. </summary>
        Public Enum ConfigLoginMode
            Authentication = 0
            PIN = 1
            FP = 2
            MSR = 3
            Touch = 4
        End Enum

        ''' <summary> Related to enum Config. </summary>
        Public Enum ConfigDefaultScreen
            Sales = 0
            BackOffice = 1
        End Enum

        ''' <summary> Related to enum Config. </summary>
        Public Enum ConfigConnectionMode
            Server = 0
            Remote = 1
            Network = 2
        End Enum

        ''' <summary>
        ''' List of Item Type (including legacy).
        ''' </summary>
        ''' <remarks></remarks>
        Public Enum ItemType
            StockItem = 0
            ServiceItem = 1
            Serialized = 2
            MatrixItem = 3
            PackageItem = 4
            WeightItem = 5
            ExpiryItem = 6
        End Enum
    End Class
End Namespace
