﻿Namespace Helper
    Public NotInheritable Class LockHolder(Of T As Class)
        Implements IDisposable

        Private ReadOnly handle As T
        Private holdsLock As Boolean

        Public Sub New(ByVal handle As T, ByVal milliSecondTimeout As Integer)
            Me.handle = handle
            holdsLock = System.Threading.Monitor.TryEnter(handle, milliSecondTimeout)
        End Sub

        Public ReadOnly Property LockSuccessful As Boolean
            Get
                Return holdsLock
            End Get
        End Property

        Public Sub Dispose() Implements IDisposable.Dispose
            If holdsLock Then System.Threading.Monitor.[Exit](handle)
            holdsLock = False
        End Sub
    End Class
End Namespace
