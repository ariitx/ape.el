﻿Imports System

Namespace Cryptography
    Public Enum HashAlgorithmType
        ' Fields
        MD5 = 1
        SHA1 = 2
        SHA256 = 3
        SHA384 = 4
        SHA512 = 5
        Unknown = 0
    End Enum
End Namespace

