﻿Imports System
Imports System.IO
Imports System.Runtime.InteropServices
Imports System.Security.Cryptography
Imports System.Text

Namespace Cryptography
    Public NotInheritable Class HashManager
        Implements IDisposable
        ' Methods
        Public Sub New(ByVal algorithm As HashAlgorithm)
            MyBase.New()
            Me._encoding = Encoding.UTF8
            Me.m_needDispose = 0
            Me.m_algorithm = algorithm
            If (TryCast(algorithm, MD5) Is Nothing) Then
                GoTo Label_003A
            End If
            Me._algorithmType = 1
            Return
Label_003A:
            If (TryCast(algorithm, SHA1CryptoServiceProvider) Is Nothing) Then
                GoTo Label_004A
            End If
            Me._algorithmType = 2
            Return
Label_004A:
            If (TryCast(algorithm, SHA256Managed) Is Nothing) Then
                GoTo Label_005A
            End If
            Me._algorithmType = 3
            Return
Label_005A:
            If (TryCast(algorithm, SHA384Managed) Is Nothing) Then
                GoTo Label_006A
            End If
            Me._algorithmType = 4
            Return
Label_006A:
            If (TryCast(algorithm, SHA512Managed) Is Nothing) Then
                GoTo Label_007A
            End If
            Me._algorithmType = 5
            Return
Label_007A:
            Me._algorithmType = 0
            Return
        End Sub

        Public Sub New(ByVal algorithmType As HashAlgorithmType)
            MyBase.New()
            Dim type As HashAlgorithmType
            Me._encoding = Encoding.UTF8
            Me.m_needDispose = 1
            Me._algorithmType = algorithmType
            type = algorithmType
            Select Case (type - 1)
                Case 0
                    GoTo Label_003F
                Case 1
                    GoTo Label_004B
                Case 2
                    GoTo Label_0057
                Case 3
                    GoTo Label_0063
                Case 4
                    GoTo Label_006F
            End Select
            GoTo Label_007B
Label_003F:
            Me.m_algorithm = MD5.Create
            Return
Label_004B:
            Me.m_algorithm = SHA1.Create
            Return
Label_0057:
            Me.m_algorithm = SHA256.Create
            Return
Label_0063:
            Me.m_algorithm = SHA384.Create
            Return
Label_006F:
            Me.m_algorithm = SHA512.Create
            Return
Label_007B:
            Return
        End Sub

        Public Sub Dispose() Implements IDisposable.Dispose
            If (Me.m_needDispose = Nothing) Then
                GoTo Label_001A
            End If
            Me.m_algorithm.Dispose()
            Me.m_needDispose = 0
Label_001A:
            Return
        End Sub

        Public Function Hash(ByVal stream As Stream) As String
            Dim buffer As Byte()
            buffer = Me.m_algorithm.ComputeHash(stream)
            Return Me.HashBytesToString(buffer)
        End Function

        Public Function Hash(ByVal data As String, Optional ByVal salt As String = Nothing) As String
            Return Me.Hash(Me.StringToBytes(data), salt)
        End Function

        Public Shared Function Hash(ByVal algorithm As HashAlgorithmType, ByVal stream As Stream) As String
            Dim manager As HashManager
            Dim str As String
            manager = New HashManager(algorithm)
Label_0007:
            Try
                str = manager.Hash(stream)
                GoTo Label_001B
            Finally
Label_0011:
                If (manager Is Nothing) Then
                    GoTo Label_001A
                End If
                manager.Dispose()
Label_001A:
            End Try
Label_001B:
            Return str
        End Function

        Public Function Hash(ByVal data As Byte(), Optional ByVal salt As String = Nothing) As String
            Dim buffer As Byte()
            If (salt Is Nothing) Then
                GoTo Label_001D
            End If
            data = ArrayCombine(Of Byte)(data, Me.StringToBytes(salt))
Label_001D:
            buffer = Me.m_algorithm.ComputeHash(data)
            Return Me.HashBytesToString(buffer)
        End Function

        Private Shared Function ArrayCombine(Of T)(ByVal array1 As T(), ByVal array2 As T()) As T()
            Dim destinationArray As T() = New T((array1.Length + array2.Length) - 1) {}
            Array.Copy(array1, 0, destinationArray, 0, array1.Length)
            Array.Copy(array2, 0, destinationArray, array1.Length, array2.Length)
            Return destinationArray
        End Function


        Public Shared Function Hash(ByVal algorithm As HashAlgorithmType, ByVal data As String, Optional ByVal salt As String = Nothing) As String
            Dim manager As HashManager
            Dim str As String
            manager = New HashManager(algorithm)
Label_0007:
            Try
                str = manager.Hash(data, salt)
                GoTo Label_001C
            Finally
Label_0012:
                If (manager Is Nothing) Then
                    GoTo Label_001B
                End If
                manager.Dispose()
Label_001B:
            End Try
Label_001C:
            Return str
        End Function

        Public Shared Function Hash(ByVal algorithm As HashAlgorithmType, ByVal data As Byte(), Optional ByVal salt As String = Nothing) As String
            Dim manager As HashManager
            Dim str As String
            manager = New HashManager(algorithm)
Label_0007:
            Try
                str = manager.Hash(data, salt)
                GoTo Label_001C
            Finally
Label_0012:
                If (manager Is Nothing) Then
                    GoTo Label_001B
                End If
                manager.Dispose()
Label_001B:
            End Try
Label_001C:
            Return str
        End Function

        Private Function HashBytesToString(ByVal hash As Byte()) As String
            Dim builder As StringBuilder
            Dim num As Integer
            builder = New StringBuilder
            num = 0
            GoTo Label_0035
Label_000A:
            builder.Append(Me.m_algorithm.Hash(num).ToString("X2").ToUpper)
            num = (num + 1)
Label_0035:
            If (num < CInt(Me.m_algorithm.Hash.Length)) Then
                GoTo Label_000A
            End If
            Return builder.ToString
        End Function

        Private Function StringToBytes(ByVal data As String) As Byte()
            Return Me.Encoding.GetBytes(data)
        End Function


        ' Properties
        Public ReadOnly Property AlgorithmType As HashAlgorithmType
            Get
                Return Me._algorithmType
            End Get
        End Property

        Public Property Encoding As Encoding
            Get
                Return Me._encoding
            End Get
            Set(ByVal value As Encoding)
                Me._encoding = value
                Return
            End Set
        End Property


        ' Fields
        Private ReadOnly _algorithmType As HashAlgorithmType
        Private _encoding As Encoding
        Private ReadOnly m_algorithm As HashAlgorithm
        Private m_needDispose As Boolean
    End Class
End Namespace

