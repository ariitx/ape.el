﻿Imports System
Imports System.IO
Imports System.Runtime.InteropServices
Imports System.Security.Cryptography
Imports System.Text

Namespace Cryptography
    Public NotInheritable Class SymmetricCipher
        Implements IDisposable
        ' Methods
        Public Sub New(ByVal algorithm As SymmetricAlgorithm)
            MyBase.New()
            Me.m_needDispose = 0
            Me.m_algorithm = algorithm
            If (TryCast(algorithm,AesManaged) Is Nothing) Then
                goto Label_002F
            End If
            Me._algorithmType = 1
            Return
        Label_002F:
            If (TryCast(algorithm,TripleDESCryptoServiceProvider) Is Nothing) Then
                goto Label_003F
            End If
            Me._algorithmType = 2
            Return
        Label_003F:
            If (TryCast(algorithm,RijndaelManaged) Is Nothing) Then
                goto Label_004F
            End If
            Me._algorithmType = 3
            Return
        Label_004F:
            Me._algorithmType = 0
            Return
        End Sub

        Public Sub New(ByVal algorithmType As SymmetricAlgorithmType)
            MyBase.New()
            Dim type As SymmetricAlgorithmType
            Me.m_needDispose = 1
            Me._algorithmType = algorithmType
            type = algorithmType
            Select Case (type - 1)
                Case 0
                    goto Label_002C
                Case 1
                    goto Label_0038
                Case 2
                    goto Label_0044
            End Select
            goto Label_0050
        Label_002C:
            Me.m_algorithm = Aes.Create
            Return
        Label_0038:
            Me.m_algorithm = TripleDES.Create
            Return
        Label_0044:
            Me.m_algorithm = Rijndael.Create
            Return
        Label_0050:
            Return
        End Sub

        Private Function CreateDecryptor(ByVal password As String, ByVal salt As String) As ICryptoTransform
            Return Me.CreateTransform(password, salt, 1)
        End Function

        Private Function CreateEncryptor(ByVal password As String, ByVal salt As String) As ICryptoTransform
            Return Me.CreateTransform(password, salt, 0)
        End Function

        Private Function CreateTransform(ByVal password As String, ByVal salt As String, ByVal decryptor As Boolean) As ICryptoTransform
            Dim bytes As Rfc2898DeriveBytes
            Dim buffer As Byte()
            Dim buffer2 As Byte()
            If (salt Is Nothing) Then
                goto Label_0017
            End If
            If (salt.Length >= 8) Then
                goto Label_002D
            End If
        Label_0017:
        Label_0021:
            salt = (If(salt <>  Nothing , salt, String.Empty) & "1_a/N)9+")
        Label_002D:
            bytes = New Rfc2898DeriveBytes(Me.Encoding.GetBytes(password), Me.Encoding.GetBytes(salt), &H3E8)
            buffer = bytes.GetBytes((Me.m_algorithm.KeySize >> 3))
            buffer2 = bytes.GetBytes((Me.m_algorithm.BlockSize >> 3))
            If (Not decryptor = Nothing) Then
                GoTo Label_0089
            End If
            Return Me.m_algorithm.CreateEncryptor(buffer, buffer2)
        Label_0089:
            Return Me.m_algorithm.CreateDecryptor(buffer, buffer2)
        End Function

        Public Function DecryptUsingBase64(ByVal value As String, ByVal password As String, ByVal Optional salt As String = Nothing) As String
            Dim transform As ICryptoTransform
            Dim stream As MemoryStream
            Dim stream2 As CryptoStream
            Dim reader As StreamReader
            Dim str As String
            transform = Me.CreateDecryptor(password, salt)
        Label_0009:
            Try 
                stream = New MemoryStream(Convert.FromBase64String(value))
            Label_0015:
                Try 
                    stream2 = New CryptoStream(stream, transform, 0)
                Label_001E:
                    Try 
                        reader = New StreamReader(stream2, Me.Encoding)
                    Label_002B:
                        Try 
                            str = reader.ReadToEnd
                            goto Label_005D
                        Finally
                        Label_0035:
                            If (reader Is Nothing) Then
                                goto Label_003E
                            End If
                            reader.Dispose
                        Label_003E:
                        End Try
                    Finally
                    Label_003F:
                        If (stream2 Is Nothing) Then
                            goto Label_0048
                        End If
                        stream2.Dispose
                    Label_0048:
                    End Try
                Finally
                Label_0049:
                    If (stream Is Nothing) Then
                        goto Label_0052
                    End If
                    stream.Dispose
                Label_0052:
                End Try
            Finally
            Label_0053:
                If (transform Is Nothing) Then
                    goto Label_005C
                End If
                transform.Dispose
            Label_005C:
            End Try
        Label_005D:
            Return str
        End Function

        Public Shared Function DecryptUsingBase64(ByVal algorithm As SymmetricAlgorithmType, ByVal value As String, ByVal password As String, ByVal Optional salt As String = Nothing) As String
            Dim cipher As SymmetricCipher
            Dim str As String
            cipher = New SymmetricCipher(algorithm)
        Label_0007:
            Try 
                str = cipher.DecryptUsingBase64(value, password, salt)
                goto Label_001D
            Finally
            Label_0013:
                If (cipher Is Nothing) Then
                    goto Label_001C
                End If
                cipher.Dispose
            Label_001C:
            End Try
        Label_001D:
            Return str
        End Function

        Public Sub Decrypy(ByVal source As Stream, ByVal destination As Stream, ByVal password As String, ByVal Optional salt As String = Nothing)
            Dim transform As ICryptoTransform
            Dim stream As CryptoStream
            transform = Me.CreateDecryptor(password, salt)
        Label_000A:
            Try 
                stream = New CryptoStream(source, transform, 0)
            Label_0013:
                Try 
                    stream.CopyTo(destination)
                    goto Label_0026
                Finally
                Label_001C:
                    If (stream Is Nothing) Then
                        goto Label_0025
                    End If
                    stream.Dispose
                Label_0025:
                End Try
            Label_0026:
                goto Label_0032
            Finally
            Label_0028:
                If (transform Is Nothing) Then
                    goto Label_0031
                End If
                transform.Dispose
            Label_0031:
            End Try
        Label_0032:
            Return
        End Sub

        Public Shared Sub Decrypy(ByVal algorithm As SymmetricAlgorithmType, ByVal source As Stream, ByVal destination As Stream, ByVal password As String, ByVal Optional salt As String = Nothing)
            Dim cipher As SymmetricCipher
            cipher = New SymmetricCipher(algorithm)
        Label_0007:
            Try 
                cipher.Decrypy(source, destination, password, salt)
                goto Label_001E
            Finally
            Label_0014:
                If (cipher Is Nothing) Then
                    goto Label_001D
                End If
                cipher.Dispose
            Label_001D:
            End Try
        Label_001E:
            Return
        End Sub

        Public Sub DecrypyFile(ByVal sourceFileName As String, ByVal destinationFileName As String, ByVal password As String, ByVal Optional salt As String = Nothing)
            Dim stream As FileStream
            Dim stream2 As FileStream
            stream = File.OpenRead(sourceFileName)
        Label_0007:
            Try 
                stream2 = File.Create(destinationFileName)
            Label_000E:
                Try 
                    Me.Decrypy(stream, stream2, password, salt)
                    goto Label_0025
                Finally
                Label_001B:
                    If (stream2 Is Nothing) Then
                        goto Label_0024
                    End If
                    stream2.Dispose
                Label_0024:
                End Try
            Label_0025:
                goto Label_0031
            Finally
            Label_0027:
                If (stream Is Nothing) Then
                    goto Label_0030
                End If
                stream.Dispose
            Label_0030:
            End Try
        Label_0031:
            Return
        End Sub

        Public Shared Sub DecrypyFile(ByVal algorithm As SymmetricAlgorithmType, ByVal sourceFileName As String, ByVal destinationFileName As String, ByVal password As String, ByVal Optional salt As String = Nothing)
            Dim cipher As SymmetricCipher
            cipher = New SymmetricCipher(algorithm)
        Label_0007:
            Try 
                cipher.DecrypyFile(sourceFileName, destinationFileName, password, salt)
                goto Label_001E
            Finally
            Label_0014:
                If (cipher Is Nothing) Then
                    goto Label_001D
                End If
                cipher.Dispose
            Label_001D:
            End Try
        Label_001E:
            Return
        End Sub

        Public Sub Dispose() Implements IDisposable.Dispose
            If (Me.m_needDispose = Nothing) Then
                GoTo Label_001A
            End If
            Me.m_algorithm.Dispose()
            Me.m_needDispose = 0
Label_001A:
            Return
        End Sub

        Public Sub Encrypt(ByVal source As Stream, ByVal destination As Stream, ByVal password As String, ByVal Optional salt As String = Nothing)
            Dim transform As ICryptoTransform
            Dim stream As CryptoStream
            transform = Me.CreateEncryptor(password, salt)
        Label_000A:
            Try 
                stream = New CryptoStream(destination, transform, 1)
            Label_0013:
                Try 
                    source.CopyTo(stream)
                    goto Label_0026
                Finally
                Label_001C:
                    If (stream Is Nothing) Then
                        goto Label_0025
                    End If
                    stream.Dispose
                Label_0025:
                End Try
            Label_0026:
                goto Label_0032
            Finally
            Label_0028:
                If (transform Is Nothing) Then
                    goto Label_0031
                End If
                transform.Dispose
            Label_0031:
            End Try
        Label_0032:
            Return
        End Sub

        Public Shared Sub Encrypt(ByVal algorithm As SymmetricAlgorithmType, ByVal source As Stream, ByVal destination As Stream, ByVal password As String, ByVal Optional salt As String = Nothing)
            Dim cipher As SymmetricCipher
            cipher = New SymmetricCipher(algorithm)
        Label_0007:
            Try 
                cipher.Encrypt(source, destination, password, salt)
                goto Label_001E
            Finally
            Label_0014:
                If (cipher Is Nothing) Then
                    goto Label_001D
                End If
                cipher.Dispose
            Label_001D:
            End Try
        Label_001E:
            Return
        End Sub

        Public Sub EncryptFile(ByVal sourceFileName As String, ByVal destinationFileName As String, ByVal password As String, ByVal Optional salt As String = Nothing)
            Dim stream As FileStream
            Dim stream2 As FileStream
            stream = File.OpenRead(sourceFileName)
        Label_0007:
            Try 
                stream2 = File.Create(destinationFileName)
            Label_000E:
                Try 
                    Me.Encrypt(stream, stream2, password, salt)
                    goto Label_0025
                Finally
                Label_001B:
                    If (stream2 Is Nothing) Then
                        goto Label_0024
                    End If
                    stream2.Dispose
                Label_0024:
                End Try
            Label_0025:
                goto Label_0031
            Finally
            Label_0027:
                If (stream Is Nothing) Then
                    goto Label_0030
                End If
                stream.Dispose
            Label_0030:
            End Try
        Label_0031:
            Return
        End Sub

        Public Shared Sub EncryptFile(ByVal algorithm As SymmetricAlgorithmType, ByVal sourceFileName As String, ByVal destinationFileName As String, ByVal password As String, ByVal Optional salt As String = Nothing)
            Dim cipher As SymmetricCipher
            cipher = New SymmetricCipher(algorithm)
        Label_0007:
            Try 
                cipher.EncryptFile(sourceFileName, destinationFileName, password, salt)
                goto Label_001E
            Finally
            Label_0014:
                If (cipher Is Nothing) Then
                    goto Label_001D
                End If
                cipher.Dispose
            Label_001D:
            End Try
        Label_001E:
            Return
        End Sub

        Public Function EncryptUsingBase64(ByVal value As String, ByVal password As String, ByVal Optional salt As String = Nothing) As String
            Dim transform As ICryptoTransform
            Dim stream As MemoryStream
            Dim stream2 As CryptoStream
            Dim writer As StreamWriter
            Dim str As String
            transform = Me.CreateEncryptor(password, salt)
        Label_0009:
            Try 
                stream = New MemoryStream
            Label_000F:
                Try 
                    stream2 = New CryptoStream(stream, transform, 1)
                Label_0018:
                    Try 
                        writer = New StreamWriter(stream2, Me.Encoding)
                    Label_0025:
                        Try 
                            writer.Write(value)
                            goto Label_0038
                        Finally
                        Label_002E:
                            If (writer Is Nothing) Then
                                goto Label_0037
                            End If
                            writer.Dispose
                        Label_0037:
                        End Try
                    Label_0038:
                        goto Label_0044
                    Finally
                    Label_003A:
                        If (stream2 Is Nothing) Then
                            goto Label_0043
                        End If
                        stream2.Dispose
                    Label_0043:
                    End Try
                Label_0044:
                    str = Convert.ToBase64String(stream.ToArray)
                    goto Label_0067
                Finally
                Label_0053:
                    If (stream Is Nothing) Then
                        goto Label_005C
                    End If
                    stream.Dispose
                Label_005C:
                End Try
            Finally
            Label_005D:
                If (transform Is Nothing) Then
                    goto Label_0066
                End If
                transform.Dispose
            Label_0066:
            End Try
        Label_0067:
            Return str
        End Function

        Public Shared Function EncryptUsingBase64(ByVal algorithm As SymmetricAlgorithmType, ByVal value As String, ByVal password As String, ByVal Optional salt As String = Nothing) As String
            Dim cipher As SymmetricCipher
            Dim str As String
            cipher = New SymmetricCipher(algorithm)
        Label_0007:
            Try 
                str = cipher.EncryptUsingBase64(value, password, salt)
                goto Label_001D
            Finally
            Label_0013:
                If (cipher Is Nothing) Then
                    goto Label_001C
                End If
                cipher.Dispose
            Label_001C:
            End Try
        Label_001D:
            Return str
        End Function


        ' Properties
        Public ReadOnly Property AlgorithmType As SymmetricAlgorithmType
            Get
                Return Me._algorithmType
            End Get
        End Property

        Public Property Encoding As Encoding
            Get
            Label_000F:
                Return If(Me._encoding IsNot Nothing, Me._encoding, Encoding.UTF8)
            End Get
            Set(ByVal value As Encoding)
                Me._encoding = value
                Return
            End Set
        End Property


        ' Fields
        Private ReadOnly _algorithmType As SymmetricAlgorithmType
        Private _encoding As Encoding
        Private ReadOnly m_algorithm As SymmetricAlgorithm
        Private m_needDispose As Boolean
    End Class
End Namespace

