﻿Imports System

Namespace Cryptography
    Public Enum SymmetricAlgorithmType
        ' Fields
        AES = 1
        Rijndael = 3
        TripleDES = 2
        Unknown = 0
    End Enum
End Namespace

