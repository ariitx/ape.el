﻿Namespace Net40
    Friend Class [String40]
        ''' <summary>
        ''' Indicates whether a specified string is null, empty, or consists only of white-space characters.
        ''' </summary>
        ''' <param name="value">The string to test.</param>
        ''' <returns>true if the value parameter is null or Empty, or if value consists exclusively of white-space characters. </returns>
        Shared Function IsNullOrWhiteSpace(value As String) As Boolean
            If (Not value Is Nothing) Then
                Dim i As Integer
                For i = 0 To value.Length - 1
                    If Not Char.IsWhiteSpace(value.Chars(i)) Then
                        Return False
                    End If
                Next i
            End If
            Return True
        End Function
    End Class
End Namespace
