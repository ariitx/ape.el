﻿
Imports System
Imports System.IO
Imports System.Linq
Imports System.Security
Imports System.Security.Permissions

Namespace App
    Public Class PathHelper2
        Public Shared Function GetTempFileName() As String
            Return PathHelper2.GetTempFileName("tmp")
        End Function

        <SecurityCritical()>
        Friend Shared Function GetTempFileName(extension As String) As String
            If String40.IsNullOrWhiteSpace(extension) Then
                Throw New ArgumentException()
            End If
            If Not extension.StartsWith(".") Then
                extension = "." + extension
            End If
            Dim tempPath As String = Path.GetTempPath()
            Dim f2 As New FileIOPermission(FileIOPermissionAccess.Write, tempPath)
            f2.Demand()
            Return Path.Combine(tempPath, String.Format("{0}{1}{2}", "tmp", Guid.NewGuid().ToString(), extension))
        End Function

        Public Shared Function CorrectAddressForUri(add As String) As String
            Return add.Replace("\", "/")
        End Function

        Public Shared Function CorrectDirectoryName(directoryName As String) As String
            Dim invalidPathChars As Char() = Path.GetInvalidPathChars()
            For i As Integer = 0 To invalidPathChars.Length - 1
                Dim c As Char = invalidPathChars(i)
                directoryName = directoryName.Replace(c.ToString(), String.Empty)
            Next
            Return directoryName
        End Function

        Public Shared Function CorrectFileName(fileName As String) As String
            Dim invalidFileNameChars As Char() = Path.GetInvalidFileNameChars()
            For i As Integer = 0 To invalidFileNameChars.Length - 1
                Dim c As Char = invalidFileNameChars(i)
                fileName = fileName.Replace(c.ToString(), String.Empty)
            Next
            Return fileName
        End Function

        Public Shared Function IsValidDirectoryName(directoryName As String) As Boolean
            If String40.IsNullOrWhiteSpace(directoryName) Then
                Return False
            End If
            Dim invalidPathChars As Char() = Path.GetInvalidPathChars()
            For i As Integer = 0 To directoryName.Length - 1
                If invalidPathChars.Contains(directoryName(i)) Then
                    Return False
                End If
            Next
            Return True
        End Function

        Public Shared Function IsValidFileName(fileName As String) As Boolean
            If String40.IsNullOrWhiteSpace(fileName) Then
                Return False
            End If
            Dim invalidFileNameChars As Char() = Path.GetInvalidFileNameChars()
            For i As Integer = 0 To fileName.Length - 1
                If invalidFileNameChars.Contains(fileName(i)) Then
                    Return False
                End If
            Next
            Return True
        End Function

        Public Shared Function IsValidFullFileName(fullFileName As String) As Boolean
            If String40.IsNullOrWhiteSpace(fullFileName) Then
                Return False
            End If
            Dim num As Integer = Math.Max(fullFileName.LastIndexOf("\"), fullFileName.LastIndexOf("/"))
            Dim text As String = If((num = -1), Nothing, fullFileName.Substring(0, num))
            Dim fileName As String = If((num = -1), fullFileName, fullFileName.Substring(num + 1, fullFileName.Length - num - 1))
            Return (String40.IsNullOrWhiteSpace(text) OrElse PathHelper2.IsValidDirectoryName(text)) AndAlso PathHelper2.IsValidFileName(fileName)
        End Function
    End Class
End Namespace
