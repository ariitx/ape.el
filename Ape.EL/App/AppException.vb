﻿Imports System
Imports System.Runtime.Serialization

Namespace App
    <System.Serializable()>
    Public Class AppException
        Inherits System.Exception

        Public Sub New()
        End Sub

        Public Sub New(message As String)
            MyBase.New(message)
        End Sub

        Public Sub New(message As String, innerException As System.Exception)
            MyBase.New(message, innerException)
        End Sub

        Protected Sub New(info As System.Runtime.Serialization.SerializationInfo, context As System.Runtime.Serialization.StreamingContext)
            MyBase.New(info, context)
        End Sub
    End Class
End Namespace