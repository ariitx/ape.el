﻿Namespace App
    Public Class Cmd
        Public Class ParseResult
            Property Found As Boolean
            Property Value As String
        End Class

        ''' <summary>
        ''' Parse argument passed to this program when loaded. Each argument must not have space (otherwise need to be wrapped in double quotes).
        ''' </summary>
        Public Shared Function ParseCommandLineArgs(Of T As Structure)(arg As T, _args() As String) As ParseResult
            If Not GetType(T).IsEnum Then Throw New InvalidOperationException("Generic type argument is not a System.Enum")

            Dim v As New ParseResult
            Try
                Dim regexArg As New System.Text.RegularExpressions.Regex("(.*)(?=\=)", Text.RegularExpressions.RegexOptions.IgnoreCase Or Text.RegularExpressions.RegexOptions.IgnorePatternWhitespace) 'Accept 
                Dim regexVal As New System.Text.RegularExpressions.Regex("(?<=\=).*", Text.RegularExpressions.RegexOptions.IgnoreCase Or Text.RegularExpressions.RegexOptions.IgnorePatternWhitespace)
                Dim found As Boolean = False
                For Each s As String In _args
                    'Find by pattern (with value, must have equal symbol)
                    Dim s1 As String = s.ToString().Replace("-", "").Replace("/", "").Replace("\", "").ToString.ToLower.Trim 'Replace some characters.
                    v.Found = regexArg.IsMatch(s1)
                    If v.Found AndAlso regexArg.Match(s1).Value.Trim.ToLower = arg.ToString.ToLower Then
                        v.Value = regexVal.Match(s).Value.Trim
                    End If

                    'Find exact (without any value)
                    If s1.Trim.ToLower = arg.ToString.ToLower Then
                        v.Found = True
                    End If

                    If v.Found Then
                        Exit For
                    End If
                Next
            Catch ex As Exception
                'do nothing..
            End Try
            Return v
        End Function

        ''' <summary>
        ''' Execute a command line.
        ''' </summary>
        ''' <param name="command">Your command.</param>
        ''' <param name="arguments">Your command arguments.</param>
        ''' <param name="permanent">Retain command window after execution.</param>
        ''' <param name="noWindow">Hide the command prompt window when executed.</param>
        ''' <remarks></remarks>
        Public Shared Sub Run(command As String, Optional arguments As String = "", Optional permanent As Boolean = False, Optional noWindow As Boolean = True, Optional waitForExit As Boolean = False)
            Dim p As Process = New Process()
            Dim pi As ProcessStartInfo = New ProcessStartInfo()
            'Options   
            '   /C     Run Command and then terminate

            '   /K     Run Command and then return to the CMD prompt.
            '          This is useful for testing, to examine variables
            pi.Arguments = " " + If(permanent = True, "/K", "/C") + " " + command + " " + arguments
            pi.FileName = "cmd.exe"
            If noWindow Then
                pi.WindowStyle = ProcessWindowStyle.Hidden
                pi.CreateNoWindow = True
            End If
            p.StartInfo = pi
            p.Start()
            If waitForExit Then
                p.WaitForExit()
            End If
        End Sub

        ''' <summary>
        ''' Launch a program and terminate cmd that calls it.
        ''' </summary>
        Public Shared Sub LaunchProgram(programPath As String, Optional arguments As String = "")
            Run(String.Format("Start """" ""{0}""", programPath), arguments)
        End Sub
    End Class
End Namespace

