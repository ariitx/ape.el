﻿Imports System
Imports System.IO

Namespace App
    Public Class OEM
        Protected myCompanyName As String

        Protected myProductName As String

        Protected myInternalProductName As String

        Private Shared myCurrentOEM As OEM

        Public ReadOnly Property CompanyName() As String
            Get
                Return Me.myCompanyName
            End Get
        End Property

        Public ReadOnly Property ProductName() As String
            Get
                Return Me.myProductName
            End Get
        End Property

        Public ReadOnly Property InternalProductName() As String
            Get
                Return Me.myInternalProductName
            End Get
        End Property

        Public ReadOnly Property ApplicationDataPath() As String
            Get
                Return Me.CompanyName + System.IO.Path.DirectorySeparatorChar + Me.InternalProductName
            End Get
        End Property

        Public Shared Function GetCurrentOEM() As OEM
            If OEM.myCurrentOEM Is Nothing Then
                OEM.myCurrentOEM = New OEM("CompanyName", "ProductName", "InternalProductName")
            End If
            Return OEM.myCurrentOEM
        End Function

        Public Shared Function SetCurrentOEM(oem As OEM) As Boolean
            If oem.myCurrentOEM Is Nothing Then
                oem.myCurrentOEM = oem
                Return True
            End If
            Return False
        End Function

        Public Sub New(companyName As String, productName As String, internalProductName As String)
            Me.myCompanyName = companyName
            Me.myProductName = productName
            Me.myInternalProductName = internalProductName
        End Sub
    End Class
End Namespace
