﻿Imports Ape.EL.Localization

Namespace App
    Public Class UserAccessControl
        ''' <summary>
        ''' Initialization must include ObjectID.
        ''' </summary>
        ''' <param name="ObjectID"></param>
        ''' <remarks></remarks>
        Public Sub New(ByVal ObjectID As String)
            Me.ObjectID = ObjectID
        End Sub

        <LocalizableString()>
        Public Enum AccessID
            <DefaultString("Allow To View")>
            AllowView
            <DefaultString("Allow To Add")>
            AllowAdd
            <DefaultString("Allow To Edit")>
            AllowEdit
            <DefaultString("Allow To Delete")>
            AllowDelete
            <DefaultString("Allow To Print")>
            AllowPrint
            <DefaultString("Allow To Export")>
            AllowExport
            <DefaultString("Allow To Save Layout")>
            AllowSaveLayout
            <DefaultString("Allow To Restore Layout")>
            AllowRestoreLayout
            <DefaultString("Allow To Clone")>
            AllowClone
            <DefaultString("Allow Column Chooser")>
            AllowColumnChooser
            <DefaultString("Allow Copy From")>
            AllowCopyFrom
            <DefaultString("Allow Copy To")>
            AllowCopyTo
            <DefaultString("Require Password To View")>
            ViewPassword
            <DefaultString("Require Password To Add")>
            AddPassword
            <DefaultString("Require Password To Edit")>
            EditPassword
            <DefaultString("Require Password To Delete")>
            DeletePassword
        End Enum

        Shared Function GetAccessIdDesc(ByVal aid As AccessID) As String
            Return Localizer.GetString(aid)
        End Function

#Region "Has"
        Property HasView() As Boolean
            Get
                Return _HasView
            End Get
            Set(ByVal value As Boolean)
                _HasView = value
            End Set
        End Property
        Private _HasView As Boolean

        Property HasAdd() As Boolean
            Get
                Return _HasAdd
            End Get
            Set(ByVal value As Boolean)
                _HasAdd = value
            End Set
        End Property
        Private _HasAdd As Boolean

        Property HasEdit() As Boolean
            Get
                Return _HasEdit
            End Get
            Set(ByVal value As Boolean)
                _HasEdit = value
            End Set
        End Property
        Private _HasEdit As Boolean

        Property HasDelete() As Boolean
            Get
                Return _HasDelete
            End Get
            Set(ByVal value As Boolean)
                _HasDelete = value
            End Set
        End Property
        Private _HasDelete As Boolean

        Property HasPrint() As Boolean
            Get
                Return _HasPrint
            End Get
            Set(ByVal value As Boolean)
                _HasPrint = value
            End Set
        End Property
        Private _HasPrint As Boolean

        Property HasExport() As Boolean
            Get
                Return _HasExport
            End Get
            Set(ByVal value As Boolean)
                _HasExport = value
            End Set
        End Property
        Private _HasExport As Boolean

        ''' <summary>
        ''' Indicate whether this UA use AllowColumnChooser.
        ''' </summary>
        Property HasColumnChooser As Boolean
            Get
                Return _HasColumnChooser
            End Get
            Set(value As Boolean)
                _HasColumnChooser = value
            End Set
        End Property
        Private _HasColumnChooser As Boolean

        ''' <summary>
        ''' Indicate whether this UA use AllowSaveLayout and AllowRestoreLayout.
        ''' </summary>
        Property HasGridLayout As Boolean
            Get
                Return _HasGridLayout
            End Get
            Set(value As Boolean)
                _HasGridLayout = value
            End Set
        End Property
        Private _HasGridLayout As Boolean

        ''' <summary>
        ''' Indicate whether this UA use AllowClone.
        ''' </summary>
        Property HasCloneFunction As Boolean
            Get
                Return _HasCloneFunction
            End Get
            Set(value As Boolean)
                _HasCloneFunction = value
            End Set
        End Property
        Private _HasCloneFunction As Boolean

        ''' <summary>
        ''' Indicate whether this UA use AllowCopyTo and AllowCopyFrom.
        ''' </summary>
        Property HasCopyFunction As Boolean
            Get
                Return _HasCopyFunction
            End Get
            Set(value As Boolean)
                _HasCopyFunction = value
            End Set
        End Property
        Private _HasCopyFunction As Boolean
#End Region

#Region "Allow"
        Property AllowView() As Boolean
            Get
                Return _AllowView
            End Get
            Set(ByVal value As Boolean)
                _AllowView = value
            End Set
        End Property
        Private _AllowView As Boolean

        Property AllowAdd() As Boolean
            Get
                Return _AllowAdd
            End Get
            Set(ByVal value As Boolean)
                _AllowAdd = value
            End Set
        End Property
        Private _AllowAdd As Boolean

        Property AllowEdit() As Boolean
            Get
                Return _AllowEdit
            End Get
            Set(ByVal value As Boolean)
                _AllowEdit = value
            End Set
        End Property
        Private _AllowEdit As Boolean

        Property AllowDelete() As Boolean
            Get
                Return _AllowDelete
            End Get
            Set(ByVal value As Boolean)
                _AllowDelete = value
            End Set
        End Property
        Private _AllowDelete As Boolean

        Property AllowPrint() As Boolean
            Get
                Return _AllowPrint
            End Get
            Set(ByVal value As Boolean)
                _AllowPrint = value
            End Set
        End Property
        Private _AllowPrint As Boolean

        Property AllowExport() As Boolean
            Get
                Return _AllowExport
            End Get
            Set(ByVal value As Boolean)
                _AllowExport = value
            End Set
        End Property
        Private _AllowExport As Boolean

        Property AllowSaveLayout As Boolean
            Get
                Return _AllowSaveLayout
            End Get
            Set(value As Boolean)
                _AllowSaveLayout = value
            End Set
        End Property
        Private _AllowSaveLayout As Boolean

        Property AllowRestoreLayout As Boolean
            Get
                Return _AllowRestoreLayout
            End Get
            Set(value As Boolean)
                _AllowRestoreLayout = value
            End Set
        End Property
        Private _AllowRestoreLayout As Boolean

        Property AllowClone As Boolean
            Get
                Return _AllowClone
            End Get
            Set(value As Boolean)
                _AllowClone = value
            End Set
        End Property
        Private _AllowClone As Boolean

        Property AllowColumnChooser As Boolean
            Get
                Return _AllowColumnChooser
            End Get
            Set(value As Boolean)
                _AllowColumnChooser = value
            End Set
        End Property
        Private _AllowColumnChooser As Boolean

        Property AllowCopyTo As Boolean
            Get
                Return _AllowCopyTo
            End Get
            Set(value As Boolean)
                _AllowCopyTo = value
            End Set
        End Property
        Private _AllowCopyTo As Boolean

        Property AllowCopyFrom As Boolean
            Get
                Return _AllowCopyFrom
            End Get
            Set(value As Boolean)
                _AllowCopyFrom = value
            End Set
        End Property
        Private _AllowCopyFrom As Boolean
#End Region

#Region "Password"
        Property ViewPassword() As Boolean
            Get
                Return _ViewPwd
            End Get
            Set(ByVal value As Boolean)
                _ViewPwd = value
            End Set
        End Property
        Private _ViewPwd As Boolean

        Property AddPassword() As Boolean
            Get
                Return _AddPwd
            End Get
            Set(ByVal value As Boolean)
                _AddPwd = value
            End Set
        End Property
        Private _AddPwd As Boolean

        Property EditPassword() As Boolean
            Get
                Return _EditPwd
            End Get
            Set(ByVal value As Boolean)
                _EditPwd = value
            End Set
        End Property
        Private _EditPwd As Boolean

        Property DeletePassword() As Boolean
            Get
                Return _DeletePwd
            End Get
            Set(ByVal value As Boolean)
                _DeletePwd = value
            End Set
        End Property
        Private _DeletePwd As Boolean
#End Region

#Region "Object name"
        ''' <summary>
        ''' Eg. Home, Customer, Vendor, Branch.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Property ObjectGroup() As String
            Get
                Return _ObjectGroup
            End Get
            Set(ByVal value As String)
                _ObjectGroup = value
            End Set
        End Property
        Private _ObjectGroup As String

        ''' <summary>
        ''' The module identifier, Eg. Sales, CustomerHistory, Export, Import.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Property ObjectID() As String
            Get
                Return _ObjectID
            End Get
            Set(ByVal value As String)
                _ObjectID = value
            End Set
        End Property
        Private _ObjectID As String

        ''' <summary>
        ''' The module description, Eg. frmImport.Name >> result as Import.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Property ObjectDescription() As String
            Get
                Return _ObjectDescription
            End Get
            Set(ByVal value As String)
                _ObjectDescription = value
            End Set
        End Property
        Private _ObjectDescription As String

        ''' <summary>
        ''' Indicate whether this UA is not used.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Property ObjectTruncated() As Boolean
            Get
                Return _ObjectTruncated
            End Get
            Set(ByVal value As Boolean)
                _ObjectTruncated = value
            End Set
        End Property
        Private _ObjectTruncated As Boolean
#End Region

#Region "Additional"
        ''' <summary>
        ''' Add additional information to extends this user access control.
        ''' </summary>
        Property CustomRepository As Object
            Get
                Return _CustomRepository
            End Get
            Set(value As Object)
                _CustomRepository = value
            End Set
        End Property
        Private _CustomRepository As Object
#End Region
    End Class
End Namespace
