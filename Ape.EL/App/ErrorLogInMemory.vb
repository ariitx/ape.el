﻿Namespace App
    Public Class ErrorLogInMemory
        Public Class ErrorInMemory
            Public Property UtcTime As DateTime
            Public Property LocalTime As DateTime
            Public Property Message As String
            Public Property Exception As Exception
        End Class

        Public Shared ReadOnly Property Errors() As List(Of ErrorInMemory)
            Get
                Return _Errors
            End Get
        End Property
        Private Shared _Errors As New List(Of ErrorInMemory)

        Public Shared Function GetLastError() As ErrorInMemory
            Return _Errors.LastOrDefault()
        End Function

        Public Shared Sub WriteError(message As String, Optional exception As Exception = Nothing)
            _Errors.Add(New ErrorInMemory With {
                        .UtcTime = DateTime.UtcNow,
                        .LocalTime = DateTime.Now,
                        .Message = message,
                        .Exception = exception})
        End Sub
    End Class
End Namespace
