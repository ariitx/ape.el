﻿Imports Ape.EL.Extensions

Namespace App
    ''' <summary>
    ''' Only for internal project usage.
    ''' </summary>
    <System.Diagnostics.DebuggerStepThrough()> _
    Friend Class Debug
        ''' <summary>
        ''' Check for compile configuration if it is Debug
        ''' </summary>
        Public Shared Function IsDebug() As Boolean
#If DEBUG Then
            Return True
#Else
            Return False
#End If
        End Function

        ''' <summary>
        ''' Checks for a condition; if the condition is false, outputs a specified message and displays a message box that shows the call stack.
        ''' </summary>
        Public Shared Sub Assert(ByVal condition As Boolean, ByVal message As String)
            If IsDebug() And Not condition Then
                Dim exMsg As String = ""
                exMsg.AppendLn("You've got this message because program is asserting errors.")
                exMsg.Append("Please summon the programmers if this message annoy you.")

                Ape.EL.WinForm.General.ExceptionErrorMsg(message, exMsg, 2, "Debugging Assert")
            End If
        End Sub

        ''' <summary>
        ''' Checks for a condition; if the condition is false, outputs a specified message and displays a message box that shows the call stack.
        ''' </summary>
        Public Shared Sub Assert(ByVal condition As Boolean, ByVal ex As Exception)
            Assert(condition, ex.ToString)
        End Sub
    End Class
End Namespace
