﻿Namespace App
    Public Class ReportBase
        ''' <summary>
        ''' Initialization must include ObjectID.
        ''' </summary>
        Public Sub New(ByVal ObjectID As String)
            Me.ObjectID = ObjectID
        End Sub

        'Object name
        ''' <summary>
        ''' Eg. Home, Customer, Vendor, Branch.
        ''' </summary>
        Property ObjectGroup() As String
            Get
                Return _ObjectGroup
            End Get
            Set(ByVal value As String)
                _ObjectGroup = value
            End Set
        End Property
        Private _ObjectGroup As String

        ''' <summary>
        ''' The module identifier, Eg. ViewSalesOrder, ViewCheckReport, etc.
        ''' </summary>
        Property ObjectID() As String
            Get
                Return _ObjectID
            End Get
            Set(ByVal value As String)
                _ObjectID = value
            End Set
        End Property
        Private _ObjectID As String

        ''' <summary>
        ''' The module description, Eg. frmImport.Name >> result as Import.
        ''' </summary>
        Property ObjectDescription() As String
            Get
                Return _ObjectDescription
            End Get
            Set(ByVal value As String)
                _ObjectDescription = value
            End Set
        End Property
        Private _ObjectDescription As String

        ''' <summary>
        ''' Indicate whether this report is not used.
        ''' </summary>
        Property ObjectTruncated() As Boolean
            Get
                Return _ObjectTruncated
            End Get
            Set(ByVal value As Boolean)
                _ObjectTruncated = value
            End Set
        End Property
        Private _ObjectTruncated As Boolean
    End Class
End Namespace
