﻿Imports Microsoft.Win32
Imports System
Imports System.IO
Imports System.Runtime.Serialization.Formatters.Binary
Imports System.Runtime.Serialization.Formatters.Soap
Imports System.Security.AccessControl

Namespace App
    Public Class PathHelper
        Private Shared myStartupPath As String

        Public Shared ReadOnly Property CommonApplicationDataPath() As String
            Get
                Dim text As String = System.Environment.GetFolderPath(System.Environment.SpecialFolder.CommonApplicationData) + System.IO.Path.DirectorySeparatorChar + OEM.GetCurrentOEM().ApplicationDataPath
                If Not System.IO.Directory.Exists(text) Then
                    System.IO.Directory.CreateDirectory(text)
                End If
                Try
                    PathHelper.OpenFolderFullControlAccessRight(text, "Authenticated Users")
                    PathHelper.OpenFolderFullControlAccessRight(text, "Users")
                Catch ex_48 As Exception
                End Try
                Return text
            End Get
        End Property

        Public Shared ReadOnly Property ApplicationDataPath() As String
            Get
                Dim text As String = System.Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData) + System.IO.Path.DirectorySeparatorChar + OEM.GetCurrentOEM().ApplicationDataPath
                If Not System.IO.Directory.Exists(text) Then
                    System.IO.Directory.CreateDirectory(text)
                End If
                Return text
            End Get
        End Property

        Public Shared ReadOnly Property UserSettingDataPath() As String
            Get
                Dim text As String = PathHelper.ApplicationDataPath + System.IO.Path.DirectorySeparatorChar + "User Settings"
                If Not System.IO.Directory.Exists(text) Then
                    System.IO.Directory.CreateDirectory(text)
                End If
                Return text
            End Get
        End Property

        Friend Sub New()
        End Sub

        Public Shared Sub SetStartupPath(path As String)
            PathHelper.myStartupPath = path
        End Sub

        Public Shared Function GetStartupFile(fileName As String) As String
            If PathHelper.myStartupPath Is Nothing Then
                Try
                    PathHelper.myStartupPath = System.AppDomain.CurrentDomain.BaseDirectory
                Catch ex_18 As Exception
                    PathHelper.myStartupPath = "C:\"
                    Try
                        Dim registryKey As Microsoft.Win32.RegistryKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(String.Format("SOFTWARE\{0}\MyProgram", Utility.General.GetAsmNamespace(Misc.AssemblyHelper.GetTrueEntryAssembly)))
                        If registryKey IsNot Nothing Then
                            Dim value As Object = registryKey.GetValue("InstallFolder")
                            If value IsNot Nothing Then
                                PathHelper.myStartupPath = value.ToString()
                            End If
                        End If
                    Catch ex_52 As Exception
                    End Try
                End Try
            End If
            If fileName.Length = 0 Then
                Return PathHelper.myStartupPath
            End If
            Return System.IO.Path.Combine(PathHelper.myStartupPath, fileName)
        End Function

        Private Shared Sub OpenFolderFullControlAccessRight(directory As String, identity As String)
            If System.IO.Directory.Exists(directory) Then
                Dim fileSystemAccessRule As System.Security.AccessControl.FileSystemAccessRule = New System.Security.AccessControl.FileSystemAccessRule(identity, System.Security.AccessControl.FileSystemRights.FullControl, System.Security.AccessControl.InheritanceFlags.ContainerInherit Or System.Security.AccessControl.InheritanceFlags.ObjectInherit, System.Security.AccessControl.PropagationFlags.None, System.Security.AccessControl.AccessControlType.Allow)
                Dim accessControl As System.Security.AccessControl.DirectorySecurity = System.IO.Directory.GetAccessControl(directory, System.Security.AccessControl.AccessControlSections.Access)
                accessControl.AddAccessRule(fileSystemAccessRule)
                accessControl.SetAccessRule(fileSystemAccessRule)
                System.IO.Directory.SetAccessControl(directory, accessControl)
            End If
        End Sub

        Private Shared Sub SaveSetting(obj As Object, fileName As String)
            Try
                Using stream As System.IO.Stream = System.IO.File.Open(fileName, System.IO.FileMode.Create)
                    Dim binaryFormatter As System.Runtime.Serialization.Formatters.Binary.BinaryFormatter = New System.Runtime.Serialization.Formatters.Binary.BinaryFormatter()
                    binaryFormatter.Serialize(stream, obj)
                End Using
            Catch ex_24 As Exception
            End Try
        End Sub

        Private Shared Function LoadSetting(fileName As String) As Object
            If Not System.IO.File.Exists(fileName) Then
                Return Nothing
            End If
            Dim result As Object
            Try
                Using stream As System.IO.Stream = System.IO.File.Open(fileName, System.IO.FileMode.Open)
                    Try
                        Dim binaryFormatter As System.Runtime.Serialization.Formatters.Binary.BinaryFormatter = New System.Runtime.Serialization.Formatters.Binary.BinaryFormatter()
                        result = binaryFormatter.Deserialize(stream)
                    Catch ex_22 As Exception
                        stream.Position = 0L
                        Dim soapFormatter As SoapFormatter = New SoapFormatter()
                        result = soapFormatter.Deserialize(stream)
                    End Try
                End Using
            Catch ex_49 As Exception
                result = Nothing
            End Try
            Return result
        End Function

        Public Shared Sub SaveUserSetting(obj As Object, fileName As String)
            Dim fileName2 As String = PathHelper.UserSettingDataPath + System.IO.Path.DirectorySeparatorChar + fileName
            PathHelper.SaveSetting(obj, fileName2)
        End Sub

        Public Shared Function LoadUserSetting(fileName As String) As Object
            Dim fileName2 As String = PathHelper.UserSettingDataPath + System.IO.Path.DirectorySeparatorChar + fileName
            Return PathHelper.LoadSetting(fileName2)
        End Function

    End Class
End Namespace
