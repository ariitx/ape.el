﻿Imports System.IO
Imports System.Reflection
Imports System.Runtime.InteropServices

Namespace App
    ''' <summary>
    ''' Use this to control the RDP concurrent user.
    ''' It will create a locked access file when LogInSession is initiated.
    ''' Existing locked access files will determine whether another _
    ''' session can be created based on number of concurrent terminals access using the main program. 
    ''' </summary>
    Public Class ConcurrentRDP
        Implements IDisposable
        Implements System.ComponentModel.IComponent 'you can add this to form's component to automatically dispose / log out session.

#Region "Nested object"
        Private Class LockFile
            Const STR_FileType As String = "session"

            Public SessionID As String
            Public GroupID As String
            Public InstanceNo As Integer

            Public Shared Function GetFileName(ByVal sessionID As String, ByVal groupID As String, ByVal instance As Integer) As String
                Return String.Format("{0}_{1}_{2}.{3}", sessionID, groupID, instance, STR_FileType)
            End Function

            ''' <summary>
            ''' Return nothing if file path is invalid.
            ''' </summary>
            Public Shared Function GetLockFile(ByVal filePath As String) As LockFile
                If System.IO.Path.GetExtension(filePath).ToLower = "." & STR_FileType Then
                    Dim llf As List(Of String) = System.IO.Path.GetFileNameWithoutExtension(filePath).Split("_").ToList
                    If llf.Count = 3 Then
                        Dim lf As New LockFile
                        lf.SessionID = llf(0)
                        lf.GroupID = llf(1)
                        lf.InstanceNo = CInt(llf(2))
                        Return lf
                    End If
                End If

                Return Nothing
            End Function
        End Class
#End Region

#Region "Fields"
        'This is the file path where we will read/write. C:\ProgramData\myApp\
        Private Shared ReadOnly m_FilePath As String = String.Format("{0}{1}{{{2}}}{1}sid{1}", _
                                                                     Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), _
                                                                     System.IO.Path.DirectorySeparatorChar, _
                                                                     Utility.General.GetAssemblyGUID)
        Private m_FileName As String = ""
        Private m_Writer As System.IO.FileStream
#End Region

#Region "Properties"
        ''' <summary>
        ''' Get or set maximum concurrent user.
        ''' </summary>
        Property MaxConcurrentUser As Integer
            Get
                Return _MaxConcurrentUser
            End Get
            Set(value As Integer)
                _MaxConcurrentUser = value
            End Set
        End Property
        Private _MaxConcurrentUser As Integer

        ''' <summary>
        ''' Get or set unique group identifier.
        ''' Let's say Advance version is only limited by 5 MaxConcurrentUser,
        ''' we will set the filename as "SessionID_GroupID_InstanceNo.session".
        ''' </summary>
        Property GroupID As String
            Get
                Return _GroupID
            End Get
            Set(value As String)
                _GroupID = value
            End Set
        End Property
        Private _GroupID As String
#End Region

#Region "Methods"
        Public Sub New()
            'Create lock files directory if not exist.
            If Not System.IO.Directory.Exists(m_FilePath) Then
                Utility.General.MakePath(m_FilePath)
            End If

            'Delete ended sessions lock.
            ClearEndedSessions()
        End Sub

        Public Sub New(ByVal maxConcurrentUser As Integer, ByVal groupID As String)
            Me.New()
            _MaxConcurrentUser = maxConcurrentUser
            _GroupID = groupID
        End Sub

        ''' <summary>
        ''' Delete abruptly ended session lock files.
        ''' </summary>
        Private Sub ClearEndedSessions()
            Dim sessions As List(Of String) = System.IO.Directory.GetFiles(m_FilePath).ToList
            For Each session In sessions
                Try
                    System.IO.File.Delete(session)
                Catch ex As Exception
                End Try
            Next
        End Sub

        ''' <summary>
        ''' Get list of unique sessions by concurrent GroupID. If GroupID is empty, we assume all sessions are concurrent.
        ''' </summary>
        Private Function GetLoggedSessionList() As List(Of String)
            Dim sessions As New List(Of LockFile)
            Dim paths As List(Of String) = System.IO.Directory.GetFiles(m_FilePath).ToList
            For Each path In paths
                Dim lf As LockFile = LockFile.GetLockFile(path)
                If lf IsNot Nothing Then
                    sessions.Add(lf)
                End If
            Next

            'Remove other sessions not in GroupID
            If Not GroupID.IsEmpty Then
                sessions.RemoveAll(Function(x) x.GroupID <> GroupID)
            End If

            Dim sessionNames As List(Of String) = (From x As LockFile In sessions Select x.SessionID).ToList
            Return sessionNames.Distinct.ToList
        End Function

        ''' <summary>
        ''' Get identifier for current terminal session.
        ''' </summary>
        Public Shared Function GetSessionID() As String
            Static id As String
            If id.IsEmpty Then
                Dim session As Network.RemoteDesktop.Session = Network.RemoteDesktop.GetRDPInfo()
                id = session.WorkstationName
            End If
            Return id
        End Function

        ''' <summary>
        ''' Create a lock file. Return true if successfully logged in. You must LogOutSession.
        ''' </summary>
        Public Function LogInSession() As Boolean
            Static loggedIn As Boolean

            If loggedIn Then Return True

            Try
                'Check allowed to log in or not.
                Dim sessions As List(Of String) = GetLoggedSessionList()
                If Not sessions.Contains(GetSessionID, StringComparer.CurrentCultureIgnoreCase) AndAlso sessions.Count >= _MaxConcurrentUser Then
                    'You have reached maximum session by current group.
                    Return False
                End If

                'Get exclusive access to the file. Lock out other process.
                Dim instance As Integer = 0
                While True
                    Try
                        m_FileName = LockFile.GetFileName(GetSessionID, GroupID, instance)
                        m_Writer = System.IO.File.Create(m_FilePath & m_FileName) 'lock the file access. #fileaccess
                        Exit While
                    Catch ex As Exception
                        instance += 1
                    End Try
                End While

                loggedIn = True

                Return True
            Catch ex As Exception
                m_FileName = String.Empty
                If m_Writer IsNot Nothing Then
                    m_Writer.Dispose()
                    m_Writer = Nothing
                End If
                loggedIn = False
                Throw ex
            End Try

            Return False
        End Function

        ''' <summary>
        ''' Log out session.
        ''' </summary>
        Public Function LogOutSession() As Boolean
            Try
                If m_Writer IsNot Nothing Then
                    m_Writer.Close() 'unlock the file access. #fileaccess
                    System.IO.File.Delete(m_FilePath & m_FileName) 'delete the file.
                End If

                Return True
            Catch ex As Exception
                Throw ex
            End Try

            Return False
        End Function
#End Region

#Region "IDisposable Support"
        Private disposedValue As Boolean ' To detect redundant calls

        ' IDisposable
        Protected Overridable Sub Dispose(disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                    ' TODO: dispose managed state (managed objects).
                    LogOutSession()
                End If

                ' TODO: free unmanaged resources (unmanaged objects) and override Finalize() below.
                ' TODO: set large fields to null.
            End If
            Me.disposedValue = True
        End Sub

        ' TODO: override Finalize() only if Dispose(ByVal disposing As Boolean) above has code to free unmanaged resources.
        'Protected Overrides Sub Finalize()
        '    ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        '    Dispose(False)
        '    MyBase.Finalize()
        'End Sub

        ' This code added by Visual Basic to correctly implement the disposable pattern.
        Public Sub Dispose() Implements IDisposable.Dispose
            ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
            Dispose(True)
            GC.SuppressFinalize(Me)
            RaiseEvent Disposed(Me, Nothing)
        End Sub
#End Region

#Region "IComponent"
        Public Event Disposed(sender As Object, e As EventArgs) Implements System.ComponentModel.IComponent.Disposed

        Public Property Site As System.ComponentModel.ISite Implements System.ComponentModel.IComponent.Site
#End Region
    End Class
End Namespace
