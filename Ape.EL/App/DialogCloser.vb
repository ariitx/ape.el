﻿Imports System.Text
Imports System.Windows.Forms
Imports System.Runtime.InteropServices
Imports System.Drawing
Imports Ape.EL.Win32.User32
Imports Ape.EL.Win32.WndProcHelper.Msg

Namespace App
    ''' <summary>
    ''' Close any dialog message (MessageBox.Show or MsgBox) on same thread.
    ''' </summary>
    ''' <remarks> Methods inside DialogCloser wrapper must be in same thread. Do not create new thread inside wrapper as the messagebox will not be caught. </remarks>
#If DEBUG Then
    Public Class DialogCloser
#Else
    <System.Diagnostics.DebuggerStepThrough()> _
    Public Class DialogCloser
#End If
        Implements IDisposable

        Public Class DlgMsg
            Property Title As String
            Property Content As String
        End Class

        ReadOnly Property Messages As List(Of DlgMsg)
            Get
                Return _Messages
            End Get
        End Property
        Private _Messages As New List(Of DlgMsg)

        Private cancelled As Boolean
        Private tmpForm As Form = Nothing
        Private curThreadID = GetCurrentThreadId()

        Public Sub New()
            'a form need to be created to fake a message loop.
            tmpForm = New Form
            tmpForm.Size = New Size(0, 0)
            tmpForm.StartPosition = FormStartPosition.Manual
            tmpForm.Location = New Point(Int16.MinValue, Int16.MinValue) 'windows min/max bound location is in int16.
            tmpForm.FormBorderStyle = FormBorderStyle.None
            tmpForm.Opacity = 0.05
            tmpForm.ShowInTaskbar = False
            tmpForm.Name = "DialogCloser"
            tmpForm.Show()

            tmpForm.BeginInvoke(New Action(Sub()
                                               'Keep running the checking in the backend until the dialog closer is disposed.
                                               Dim th As New Threading.Thread(Sub()
                                                                                  Dim callback As New EnumThreadWndProc(AddressOf CheckWindowHandler)
                                                                                  While Not cancelled
                                                                                      Try
                                                                                          ' Enumerate windows to find dialogs and pump checkWindow method to the form.
                                                                                          EnumThreadWindows(curThreadID, callback, IntPtr.Zero)
                                                                                      Catch ex As Exception
                                                                                          'absorb exception
                                                                                      End Try
                                                                                  End While
                                                                                  GC.KeepAlive(callback)
                                                                              End Sub)
                                               th.IsBackground = True
                                               th.Start()
                                           End Sub))
        End Sub

        Public Sub Dispose() Implements IDisposable.Dispose
            cancelled = True
            If tmpForm IsNot Nothing AndAlso tmpForm.IsDisposed = False Then tmpForm.Dispose()
            _Messages.Clear()
        End Sub

        Private Function CheckWindowHandler(hWnd As IntPtr, lp As IntPtr) As Boolean
            ' Checks if <hWnd> is a Windows dialog
            Dim sb As New StringBuilder(260)
            GetClassName(hWnd, sb, sb.Capacity)

            '32770 is MessageBox dialog handler id.
            If sb.ToString() = "#32770" Then
                Try
                    'Add the closing messagebox title and content into Messages property.
                    Dim msg As New DlgMsg
                    'get title text
                    msg.Title = (Win32.Helper.GetControlText(hWnd))
                    'get content tex
                    Dim hText As IntPtr = GetDlgItem(hWnd, &HFFFF)
                    If hText <> IntPtr.Zero Then
                        msg.Content = (Win32.Helper.GetControlText(hText))
                    End If
                    If msg.Content.IsEmpty = False Then _Messages.Add(msg)
                Catch ex As Exception
                    'absorb exception
                End Try

                ' Close it by sending WM_CLOSE to the window
                SendMessage(hWnd, &H10, IntPtr.Zero, IntPtr.Zero)
            End If
            Return True
        End Function
    End Class
End Namespace
