﻿
Imports System
Imports System.IO
Imports System.Net
Imports System.Net.Mail
Imports System.Text

Namespace App
    ''' <summary>
    ''' Universal error message logger, file will be saved in "C:\Users\[user]\AppData\Local\[root namespace]\error.log."
    ''' </summary>
    Public Class ErrorLog
        Public Enum Priority
            ''' <summary>Off only.</summary>
            OFF = 200
            ''' <summary>Off, Debug.</summary>
            DEBUG = 100
            ''' <summary>Off, Debug, Info.</summary>
            INFO = 75
            ''' <summary>Off, Debug, Info, Warning.</summary>
            WARN = 50
            ''' <summary>Off, Debug, Info, Warning, Error.</summary>
            [ERROR] = 25
            ''' <summary>Off, Debug, Info, Warning, Error, Fatal.</summary>
            FATAL = 0
        End Enum

        ''' <summary>
        ''' Set an event handler before begin WriteMessage routine. Not affected by error log priority.
        ''' </summary>
        Shared WriteOnly Property BeforeWriteMessageEventHandler As EventHandler
            Set(value As EventHandler)
                _BeforeWriteMessageEventHandler = value
            End Set
        End Property
        Private Shared _BeforeWriteMessageEventHandler As EventHandler

        ''' <summary>
        ''' Set ErrorLog's settings.
        ''' </summary>
        Public Shared ReadOnly Property Properties As Repository.RepositoryItemErrorLog
            Get
                Return _Properties
            End Get
        End Property
        Private Shared _Properties As New Repository.RepositoryItemErrorLog

        Private Const OffSetHour As Integer = 0

        ''' <summary>
        ''' Log error to error.log file in "AppData\Local\AppNamespace\error.log."
        ''' </summary>
        ''' <param name="message">String message to be appended to existing log file.</param>
        ''' <param name="level">Priority of the message.</param>
        Public Shared Sub WriteMessage(message As String, ByVal level As Priority)
            ErrorLogInMemory.WriteError(String.Format("ErrorLog.WriteMessage: {0} - {1}", level.ToString(), message))

            Try
                If _BeforeWriteMessageEventHandler IsNot Nothing AndAlso Not message.IsEmpty Then
                    _BeforeWriteMessageEventHandler.Invoke(message, New EventArgs)
                End If
            Catch ex As Exception
                ErrorLogInMemory.WriteError("ErrorLog.WriteMessage.BeforeWriteMessageEventHandler: " & ex.Message, ex)
            End Try

            If Not CInt(level) >= CInt(_Properties.LogLevel) Then
                'program's log level must be higher than writing level.
                'if setting is fatal, log everything
                'if setting is off, only off can be logged
                Exit Sub
            End If

            Try
                Dim path As String = _Properties.ErrorLogPath
                Dim asm As System.Reflection.Assembly = Internal.GetAsm
                Dim internalAsm As String = If(asm IsNot Nothing, System.IO.Path.GetFileName(asm.BinaryPath), "N/A")
                Dim internalAsmVersion As String = If(asm IsNot Nothing, asm.BuildFileVersion, "N/A")
                Utility.General.MakePath(IO.Path.GetDirectoryName(path))
                Dim streamWriter As System.IO.StreamWriter = System.IO.File.AppendText(path)
                streamWriter.WriteLine("::-------------------------------------------------------")
                streamWriter.WriteLine(":: " & internalAsm & " v" & internalAsmVersion & If(Ape.EL.App.Debug.IsDebug, " (Debug)", "") & String.Format(" [{0}]", level.ToString))
                streamWriter.WriteLine(":: " & DateTime.Now.AddHours(_Properties.TimeZoneOffset).ToString("dd/MMM/yyyy HH:mm:ss zzz") & " Offset=" & _Properties.TimeZoneOffset.ToString)
                streamWriter.WriteLine("::-------------------------------------------------------")
                Dim array As String() = Ape.EL.Misc.StringHelper.ToMultiLineString(message)
                Dim array2 As String() = array
                For i As Integer = 0 To array2.Length - 1
                    Dim value As String = array2(i)
                    streamWriter.WriteLine(value)
                Next
                streamWriter.Flush()
                streamWriter.Close()
            Catch ex_64 As Exception
                ErrorLogInMemory.WriteError("ErrorLog.WriteMessage.Writing: " & ex_64.Message, ex_64)
            End Try
        End Sub

        ''' <summary>
        ''' Log error to error.log file in "AppData\Local\AppNamespace\error.log."
        ''' </summary>
        Public Shared Sub WriteMessage(message As String)
            WriteMessage(message, Priority.INFO)
        End Sub

        ''' <summary>
        ''' Log error to error.log file in "AppData\Local\AppNamespace\error.log."
        ''' </summary>
        Public Shared Sub WriteStrings(sb As System.Text.StringBuilder)
            WriteMessage(sb.ToString)
        End Sub

        ''' <summary>
        ''' Log error to error.log with DEBUG priority.
        ''' </summary>
        Public Shared Sub Debug(message As [String])
            WriteMessage(message, Priority.DEBUG)
        End Sub

        ''' <summary>
        ''' Log error to error.log with INFO priority.
        ''' </summary>
        Public Shared Sub Info(message As [String])
            WriteMessage(message, Priority.INFO)
        End Sub

        ''' <summary>
        ''' Log error to error.log with WARN priority.
        ''' </summary>
        Public Shared Sub Warn(message As [String])
            WriteMessage(message, Priority.WARN)
        End Sub

        ''' <summary>
        ''' Log error to error.log with ERROR priority.
        ''' </summary>
        Public Shared Sub [Error](message As [String])
            WriteMessage(message, Priority.[ERROR])
        End Sub

        ''' <summary>
        ''' Log error to error.log with FATAL priority.
        ''' </summary>
        Public Shared Sub Fatal(message As [String])
            WriteMessage(message, Priority.FATAL)
        End Sub

        ''' <summary>
        ''' Log error to error.log with DEBUG priority.
        ''' </summary>
        Public Shared Sub Debug(e As Exception)
            If e Is Nothing Then Exit Sub
            WriteMessage(e.ToString, Priority.DEBUG)
        End Sub

        ''' <summary>
        ''' Log error to error.log with INFO priority.
        ''' </summary>
        Public Shared Sub Info(e As Exception)
            If e Is Nothing Then Exit Sub
            WriteMessage(e.Message, Priority.INFO)
        End Sub

        ''' <summary>
        ''' Log error to error.log with WARN priority.
        ''' </summary>
        Public Shared Sub Warn(e As Exception)
            If e Is Nothing Then Exit Sub
            WriteMessage(e.Message, Priority.WARN)
        End Sub

        ''' <summary>
        ''' Log error to error.log with ERROR priority.
        ''' </summary>
        Public Shared Sub [Error](e As Exception)
            If e Is Nothing Then Exit Sub
            WriteMessage(e.ToString, Priority.[ERROR])
        End Sub

        ''' <summary>
        ''' Log error to error.log with FATAL priority.
        ''' </summary>
        Public Shared Sub Fatal(e As Exception)
            If e Is Nothing Then Exit Sub
            WriteMessage(e.ToString, Priority.FATAL)
        End Sub

        ''' <summary>
        ''' Read the error log and return as string.
        ''' </summary>
        Public Shared Function OpenLogFile() As String
            Dim strFileText As String = ""
            Dim path As String = _Properties.ErrorLogPath

            If (System.IO.File.Exists(path)) Then
                'System.Diagnostics.Process.Start(strFileName)
                strFileText = File.ReadAllText(path)
            End If

            Return strFileText
        End Function

        Public Shared Function EmailToDevError(subject As String, body As String, ParamArray attachments As Attachment()) As Boolean
            Dim result As Boolean = False
            For i As Integer = 0 To 3 - 1
                Try
                    result = Misc.Email.SendEmail(_Properties.Email.Address, _Properties.Email.Password, _Properties.Email.Host, _Properties.Email.UseSSL, _Properties.Email.Port, _Properties.EmailRecipient, "", "", subject, body, attachments)
                    If result Then Exit For
                Catch ex As Exception
                End Try
            Next
            Return result
        End Function

        Public Shared Function EmailErrorLogToDevError(subject As String, body As String) As Boolean
            Return System.IO.File.Exists(_Properties.ErrorLogPath) AndAlso ErrorLog.EmailToDevError(subject, body, New Attachment() {New Attachment(_Properties.ErrorLogPath)})
        End Function
    End Class
End Namespace
