﻿Imports System.Runtime.InteropServices
Imports Ape.EL.Utility.General
Imports Ape.EL.Win32.Kernel32
Imports System.Windows.Forms
Imports System.Drawing

Namespace App
    Public Class General
        '        Private ReadOnly lic As Boolean = Internal.CheckMyLicense

        Public Shared ReadOnly MinSqlDateTime As System.DateTime = New System.DateTime(1753, 1, 1)
        Public Shared ReadOnly MaxSqlDateTime As System.DateTime = New System.DateTime(9999, 12, 31)

        Public Class FlushMemoryOption
            ''' <summary>
            ''' When flushing, use GC. Slower for .net 4.0. Default value is set to false.
            ''' </summary>
            Public Shared Property UseGC As Boolean = False
            ''' <summary>
            ''' When flushing, use SetWorkingProcess. Default value is set to true.
            ''' </summary>
            Public Shared Property UseSetWorkingProcess As Boolean = True
        End Class

        ''' <summary>
        ''' Count number of FlushMemory execution.
        ''' </summary>
        Public Shared ReadOnly Property FlushMemoryCount As Integer
            Get
                Return _FlushMemoryCount
            End Get
        End Property
        Private Shared _FlushMemoryCount As Integer = 0

        ''' <summary>
        ''' When using FlushMemory, flushes all processes as well.
        ''' </summary>
        Public Shared Property FlushAllProcesses As Boolean = False

        <DllImport("User32.dll")> _
        Private Shared Function GetLastInputInfo(ByRef lii As LASTINPUTINFO) As Boolean
        End Function

        Private Structure LASTINPUTINFO
            Public cbSize As UInteger
            Public dwTime As UInteger
        End Structure

        ''' <summary>
        ''' Get application's idle process.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetIdle() As UInt64
            Try
                Dim lii As New LASTINPUTINFO()
                lii.cbSize = Convert.ToUInt64((Marshal.SizeOf(lii)))
                GetLastInputInfo(lii)
                Return Convert.ToUInt64(Environment.TickCount) - lii.dwTime
            Catch ex As Exception
                Return 0
            End Try
        End Function

        Declare Function SetProcessWorkingSetSize Lib "kernel32.dll" (ByVal process As IntPtr, ByVal minimumWorkingSetSize As Integer, ByVal maximumWorkingSetSize As Integer) As Integer

        ''' <summary>
        ''' Flush used memory used by application. In FlushMemoryOption, either UseGC or UseSetWorkingProcess must be true to use this function.
        ''' </summary>
        ''' <param name="strAppName">Application name to find the processes.</param>
        ''' <remarks></remarks>
        Public Shared Sub FlushMemory(Optional ByVal strAppName As String = "")
            PerformFlushMemory(strAppName)
        End Sub

        Private Shared Sub PerformFlushMemory(strAppName As String)
            Dim p = Process.GetCurrentProcess
            If strAppName = "" Then
                If p IsNot Nothing Then
                    strAppName = p.ProcessName
                Else
                    Dim asm As System.Reflection.Assembly = Misc.AssemblyHelper.GetTrueEntryAssembly
                    If asm IsNot Nothing Then
                        strAppName = System.IO.Path.GetFileNameWithoutExtension(GetStartUpName(Misc.AssemblyHelper.GetTrueEntryAssembly.GetName().CodeBase).ToUpper)
                    End If
                End If
            End If

            If strAppName = p.ProcessName Then
                PerformFlushMemory(p)
            End If

            If FlushAllProcesses Then
                Dim myProcesses As Process() = Process.GetProcessesByName(strAppName)
                Dim myProcess As Process

                For Each myProcess In myProcesses
                    PerformFlushMemory(myProcess)
                Next
            End If
        End Sub

        ''' <summary>
        ''' Flush used memory used by application. In FlushMemoryOption, either UseGC or UseSetWorkingProcess must be true to use this function.
        ''' </summary>
        ''' <param name="pid">PID to find the process.</param>
        ''' <remarks></remarks>
        Public Shared Sub FlushMemory(pid As Integer)
            PerformFlushMemory(pid)
        End Sub

        Private Shared Sub PerformFlushMemory(pid As Integer)
            Dim p As Process = Process.GetProcessById(pid)
            If p IsNot Nothing Then
                PerformFlushMemory(p)
            End If
        End Sub

        ''' <summary>
        ''' Flush used memory used by application.
        ''' </summary>
        ''' <param name="p">The process.</param>
        ''' <remarks></remarks>
        Public Shared Sub FlushMemory(p As Process)
            PerformFlushMemory(p)
        End Sub

        Private Shared Sub PerformFlushMemory(p As Process)
            Try
                If FlushMemoryOption.UseGC Then
                    'GC.GetTotalMemory(True)
                    GC.Collect()
                    GC.WaitForPendingFinalizers()
                End If
                If FlushMemoryOption.UseSetWorkingProcess Then
                    If (Environment.OSVersion.Platform = PlatformID.Win32NT) Then
                        If p IsNot Nothing Then
                            SetProcessWorkingSetSize(p.Handle, -1, -1)
                        End If
                    End If
                End If
                _FlushMemoryCount += 1
            Catch ex As Exception

            End Try
        End Sub

        ''' <summary>
        ''' Disable device sleeping while running program.
        ''' </summary>
        Public Shared Sub DisableSleep()
            SetThreadExecutionState(EXECUTION_STATE.ES_SYSTEM_REQUIRED Or EXECUTION_STATE.ES_CONTINUOUS Or EXECUTION_STATE.ES_DISPLAY_REQUIRED)
        End Sub

        ''' <summary>
        ''' Kill executing program / service.
        ''' </summary>
        Public Shared Sub KillProgram(Optional ByVal logMsg As String = "")
            If Not logMsg.IsEmpty Then
                Ape.EL.App.ErrorLog.Info(logMsg)
            End If

            Try
                Using sbase As New System.ServiceProcess.ServiceBase
                    If sbase.CanStop Then sbase.Stop() 'try to stop a service if it is web app
                End Using
            Catch ex2 As Exception
            End Try
            System.Windows.Forms.Application.ExitThread() 'exit application safely
            System.Windows.Forms.Application.Exit() '
            Environment.Exit(0) 'terminate process
        End Sub

        ''' <summary>
        ''' Use this method on main thread's form. When the main thread is closed, all other forms from other threads will be closed as well.
        ''' </summary>
        Public Shared Sub SetMainThreadForm(ByRef mainForm As Windows.Forms.Form)
            RemoveHandler mainForm.Disposed, AddressOf SetMainThreadForm_Disposed
            AddHandler mainForm.Disposed, AddressOf SetMainThreadForm_Disposed
        End Sub

        ''' <summary>
        ''' Required by SetMainThreadForm.
        ''' </summary>
        Private Shared Sub SetMainThreadForm_Disposed(sender As Object, e As Object)
            For i As Integer = System.Windows.Forms.Application.OpenForms.Count - 1 To 0 Step -1
                Dim form As Windows.Forms.Form = System.Windows.Forms.Application.OpenForms(i)
                If Not form.Equals(sender) Then
                    form.BeginInvokeIfRequired(Sub(x As Windows.Forms.Form)
                                                   Try
                                                       x.Dispose()
                                                   Catch ex As Exception
                                                   End Try
                                               End Sub)
                End If
            Next
        End Sub

        ''' <summary>
        ''' Run form in a new thread independent of main form thread. You have to use BeginInvokeIfRequired on the form to use cross-threading functions.
        ''' </summary>
        Public Shared Function RunFormOnNewThread(ByVal type As Type) As Windows.Forms.Form
            'usage:
            'RunFormOnNewThread(GetType(myForm))

            If GetType(Windows.Forms.Form).IsAssignableFrom(type) Then
                If System.Windows.Forms.Application.OpenForms.Count > 0 Then
                    'set main thread form, so that the new form on another thread will be closed automatically when the main form is closed.
                    SetMainThreadForm(System.Windows.Forms.Application.OpenForms(0))
                End If

                Dim th As Threading.Thread = Nothing
                Dim frm As Windows.Forms.Form = Nothing
                Dim exception As Exception = Nothing

                th = New Threading.Thread(Sub()
                                              Try
                                                  frm = type.GetInstance
                                                  Windows.Forms.Application.Run(frm)
                                              Catch ex As Exception
                                                  WinForm.General.WarningMsg("Failed to open form in a new thread.")
                                                  exception = ex
                                              End Try
                                          End Sub)
                th.IsBackground = True 'this is to make sure the thread will be killed when main thread is closed.
                th.SetApartmentState(Threading.ApartmentState.STA)
                th.Start()

                While exception Is Nothing AndAlso frm Is Nothing  'wait until the form is created.
                    Threading.Thread.Sleep(100)
                End While

                If exception IsNot Nothing Then 'throw exception if there is any.
                    Throw exception
                End If

                Return frm
            Else
                Throw New ApplicationException("Only System.Windows.Forms.Form type is allowed.")
            End If
        End Function

        ''' <summary>
        ''' Run form in a new thread independent of main form thread. You have to use BeginInvokeIfRequired on the form to use cross-threading functions.
        ''' </summary>
        Public Shared Function RunFormOnNewThread(ByVal frm As Windows.Forms.Form) As Windows.Forms.Form
            'usage:
            'RunFormOnNewThread(myForm)
            If System.Windows.Forms.Application.OpenForms.Count > 0 Then
                'set main thread form, so that the new form on another thread will be closed automatically when the main form is closed.
                SetMainThreadForm(System.Windows.Forms.Application.OpenForms(0))
            End If

            Dim th As Threading.Thread = Nothing
            Dim exception As Exception = Nothing

            th = New Threading.Thread(Sub()
                                          Try
                                              Windows.Forms.Application.Run(frm)
                                          Catch ex As Exception
                                              WinForm.General.WarningMsg("Failed to open form in a new thread.")
                                              exception = ex
                                          End Try
                                      End Sub)
            th.IsBackground = True 'this is to make sure the thread will be killed when main thread is closed.
            th.SetApartmentState(Threading.ApartmentState.STA)
            th.Start()

            If exception IsNot Nothing Then 'throw exception if there is any.
                Throw exception
            End If

            Return frm
        End Function

        ''' <summary>
        ''' Run method in a new thread independent of main form thread. Thread will be closed once the function is done.
        ''' The provided EventHandler's parameter's sender will receive the thread.
        ''' </summary>
        Public Shared Function RunMethodOnNewThread(ByVal method As EventHandler, Optional priority As Threading.ThreadPriority = Threading.ThreadPriority.Normal) As Threading.Thread
            'usage:
            'RunMethodOnNewThread(Sub(sender As Object, e As EventArgs)
            '                         While True
            '                             HeavyFunction()
            '                         End While
            '                     End Sub)

            If method IsNot Nothing Then
                Dim th As Threading.Thread = Nothing
                th = New Threading.Thread(Sub()
                                              method.Invoke(th, New EventArgs)
                                          End Sub)

                th.IsBackground = True 'this is to make sure the thread will be killed when main thread is closed.
                th.SetApartmentState(Threading.ApartmentState.STA)
                th.Priority = priority
                th.SetName("RunMethodOnNewThread")
                th.Start()

                Return th
            End If

            Return Nothing
        End Function

        Public Class Threader
            Public Property method As EventHandler
            Public Property priority As Threading.ThreadPriority = Threading.ThreadPriority.Normal
            Public Sub RunMethodOnNewThread()
                Ape.EL.App.General.RunMethodOnNewThread(method, priority)
            End Sub
        End Class
    End Class
End Namespace
