﻿
Imports Ape.EL.Localization
Imports System
Imports System.Text
Imports System.Windows.Forms

Namespace App
    Public Class ProductVersion
        Friend Sub New()
        End Sub

        Public Shared Function GetFullProductVersion() As String
            Return Application.ProductVersion
        End Function

        Public Shared Function GetCompleteProductVersion(verSuffix As String) As String
            Dim text As String = ProductVersion.GetMajorMinorProductVersion() + verSuffix
            Dim fullProductVersion As String = ProductVersion.GetFullProductVersion()
            Dim array As String() = fullProductVersion.Split(New Char() {"."})
            If array.Length >= 4 Then
                Return Localizer.GetString(StringId.CompleteVersion, New Object() {text, array(2), array(3)})
            End If
            If array.Length >= 3 Then
                Return Localizer.GetString(StringId.CompleteVersion, New Object() {text, array(2), ""})
            End If
            Return Localizer.GetString(StringId.CompleteVersion, New Object() {text, "", ""})
        End Function

        Public Shared Function GetCompleteProductVersion() As String
            Return ProductVersion.GetCompleteProductVersion("")
        End Function

        Public Shared Function GetProductVersion() As String
            Dim fullProductVersion As String = ProductVersion.GetFullProductVersion()
            Dim array As String() = fullProductVersion.Split(New Char() {"."})
            Dim stringBuilder As StringBuilder = New StringBuilder(10)
            For i As Integer = 0 To 3 - 1
                If i < array.Length Then
                    If i > 0 Then
                        stringBuilder.Append(".")
                    End If
                    stringBuilder.Append(array(i))
                End If
            Next
            Return stringBuilder.ToString()
        End Function

        Public Shared Function GetMajorMinorProductVersion() As String
            Dim fullProductVersion As String = ProductVersion.GetFullProductVersion()
            Dim array As String() = fullProductVersion.Split(New Char() {"."})
            Dim stringBuilder As StringBuilder = New StringBuilder(10)
            For i As Integer = 0 To 2 - 1
                If i < array.Length Then
                    If i > 0 Then
                        stringBuilder.Append(".")
                    End If
                    stringBuilder.Append(array(i))
                End If
            Next
            Return stringBuilder.ToString()
        End Function
    End Class
End Namespace
