﻿
Imports System

Namespace Data
    Public Class DbDataTypes
'        Private ReadOnly lic As Boolean = Internal.CheckMyLicense

        <System.Serializable()>
        Public Structure DbBoolean
            Private myValue As Object

            Public ReadOnly Property HasValue() As Boolean
                Get
                    Return Me.myValue IsNot System.DBNull.Value
                End Get
            End Property

            Public Sub New(aDBNull As System.DBNull)
                Me.myValue = aDBNull
            End Sub

            Public Sub New(newValue As Boolean)
                Me.myValue = newValue
            End Sub

            Public Overrides Function ToString() As String
                Return Me.myValue.ToString()
            End Function

            Public Overrides Function Equals(obj As Object) As Boolean
                If Not Me.HasValue Then
                    Return obj Is Nothing OrElse obj Is System.DBNull.Value
                End If
                Return Me.myValue.ToString() = obj.ToString()
            End Function

            Public Overrides Function GetHashCode() As Integer
                Return Me.myValue.GetHashCode()
            End Function

            Public Shared Widening Operator CType(newValue As Boolean) As DbBoolean
                Return New DbBoolean(newValue)
            End Operator

            Public Shared Widening Operator CType(aDBNull As System.DBNull) As DbBoolean
                Return New DbBoolean(aDBNull)
            End Operator

            Public Shared Widening Operator CType(aDBType As DbBoolean) As Boolean
                Return Convert.ToBoolean(aDBType.myValue)
            End Operator

            Public Shared Operator =(aDBType As DbBoolean, aDBNull As System.DBNull) As Boolean
                Return aDBType.myValue Is aDBNull
            End Operator

            Public Shared Operator <>(aDBType As DbBoolean, aDBNull As System.DBNull) As Boolean
                Return aDBType.myValue IsNot aDBNull
            End Operator
        End Structure

        <System.Serializable()>
        Public Structure DbDateTime
            Private myValue As Object

            Public ReadOnly Property HasValue() As Boolean
                Get
                    Return Me.myValue IsNot System.DBNull.Value
                End Get
            End Property

            Public Sub New(aDBNull As System.DBNull)
                Me.myValue = aDBNull
            End Sub

            Public Sub New(newValue As System.DateTime)
                Me.myValue = newValue
            End Sub

            Public Overrides Function ToString() As String
                Return Me.myValue.ToString()
            End Function

            Public Overrides Function Equals(obj As Object) As Boolean
                If Not Me.HasValue Then
                    Return obj Is Nothing OrElse obj Is System.DBNull.Value
                End If
                Return Me.myValue.ToString() = obj.ToString()
            End Function

            Public Overrides Function GetHashCode() As Integer
                Return Me.myValue.GetHashCode()
            End Function

            Public Shared Widening Operator CType(newValue As System.DateTime) As DbDateTime
                Return New DbDateTime(newValue)
            End Operator

            Public Shared Widening Operator CType(aDBNull As System.DBNull) As DbDateTime
                Return New DbDateTime(aDBNull)
            End Operator

            Public Shared Widening Operator CType(aDBType As DbDateTime) As System.DateTime
                Return Convert.ToDateTime(aDBType.myValue)
            End Operator

            Public Shared Operator =(aDBType As DbDateTime, aDBNull As System.DBNull) As Boolean
                Return aDBType.myValue Is aDBNull
            End Operator

            Public Shared Operator <>(aDBType As DbDateTime, aDBNull As System.DBNull) As Boolean
                Return aDBType.myValue IsNot aDBNull
            End Operator
        End Structure



        <System.Serializable()>
        Public Structure DbDecimal
            Private myValue As Object

            Public ReadOnly Property HasValue() As Boolean
                Get
                    Return Me.myValue IsNot System.DBNull.Value
                End Get
            End Property

            Public Sub New(aDBNull As System.DBNull)
                Me.myValue = aDBNull
            End Sub

            Public Sub New(newValue As Decimal)
                Me.myValue = newValue
            End Sub

            Public Overrides Function ToString() As String
                Return Me.myValue.ToString()
            End Function

            Public Overrides Function Equals(obj As Object) As Boolean
                If Not Me.HasValue Then
                    Return obj Is Nothing OrElse obj Is System.DBNull.Value
                End If
                Return Me.myValue.ToString() = obj.ToString()
            End Function

            Public Overrides Function GetHashCode() As Integer
                Return Me.myValue.GetHashCode()
            End Function

            Public Shared Widening Operator CType(newValue As Decimal) As DbDecimal
                Return New DbDecimal(newValue)
            End Operator

            Public Shared Widening Operator CType(aDBNull As System.DBNull) As DbDecimal
                Return New DbDecimal(aDBNull)
            End Operator

            Public Shared Widening Operator CType(aDBDecimal As DbDecimal) As Decimal
                Return Convert.ToDecimal(aDBDecimal.myValue)
            End Operator

            Public Shared Operator =(aDBType As DbDecimal, aDBNull As System.DBNull) As Boolean
                Return aDBType.myValue Is aDBNull
            End Operator

            Public Shared Operator <>(aDBType As DbDecimal, aDBNull As System.DBNull) As Boolean
                Return aDBType.myValue IsNot aDBNull
            End Operator
        End Structure

        <System.Serializable()>
        Public Structure DbInt16
            Private myValue As Object

            Public ReadOnly Property HasValue() As Boolean
                Get
                    Return Me.myValue IsNot System.DBNull.Value
                End Get
            End Property

            Public Sub New(aDBNull As System.DBNull)
                Me.myValue = aDBNull
            End Sub

            Public Sub New(newValue As Short)
                Me.myValue = newValue
            End Sub

            Public Overrides Function ToString() As String
                Return Me.myValue.ToString()
            End Function

            Public Overrides Function Equals(obj As Object) As Boolean
                If Not Me.HasValue Then
                    Return obj Is Nothing OrElse obj Is System.DBNull.Value
                End If
                Return Me.myValue.ToString() = obj.ToString()
            End Function

            Public Overrides Function GetHashCode() As Integer
                Return Me.myValue.GetHashCode()
            End Function

            Public Shared Widening Operator CType(newValue As Short) As DbInt16
                Return New DbInt16(newValue)
            End Operator

            Public Shared Widening Operator CType(aDBNull As System.DBNull) As DbInt16
                Return New DbInt16(aDBNull)
            End Operator

            Public Shared Widening Operator CType(aDBType As DbInt16) As Short
                Return Convert.ToInt16(aDBType.myValue)
            End Operator

            Public Shared Operator =(aDBType As DbInt16, aDBNull As System.DBNull) As Boolean
                Return aDBType.myValue Is aDBNull
            End Operator

            Public Shared Operator <>(aDBType As DbInt16, aDBNull As System.DBNull) As Boolean
                Return aDBType.myValue IsNot aDBNull
            End Operator
        End Structure

        <System.Serializable()>
        Public Structure DbInt32
            Private myValue As Object

            Public ReadOnly Property HasValue() As Boolean
                Get
                    Return Me.myValue IsNot System.DBNull.Value
                End Get
            End Property

            Public Sub New(aDBNull As System.DBNull)
                Me.myValue = aDBNull
            End Sub

            Public Sub New(newValue As Integer)
                Me.myValue = newValue
            End Sub

            Public Overrides Function ToString() As String
                Return Me.myValue.ToString()
            End Function

            Public Overrides Function Equals(obj As Object) As Boolean
                If Not Me.HasValue Then
                    Return obj Is Nothing OrElse obj Is System.DBNull.Value
                End If
                Return Me.myValue.ToString() = obj.ToString()
            End Function

            Public Overrides Function GetHashCode() As Integer
                Return Me.myValue.GetHashCode()
            End Function

            Public Shared Widening Operator CType(newValue As Integer) As DbInt32
                Return New DbInt32(newValue)
            End Operator

            Public Shared Widening Operator CType(aDBNull As System.DBNull) As DbInt32
                Return New DbInt32(aDBNull)
            End Operator

            Public Shared Widening Operator CType(aDBType As DbInt32) As Integer
                Return Convert.ToInt32(aDBType.myValue)
            End Operator

            Public Shared Operator =(aDBType As DbInt32, aDBNull As System.DBNull) As Boolean
                Return aDBType.myValue Is aDBNull
            End Operator

            Public Shared Operator <>(aDBType As DbInt32, aDBNull As System.DBNull) As Boolean
                Return aDBType.myValue IsNot aDBNull
            End Operator
        End Structure

        <System.Serializable()>
        Public Structure DbInt64
            Private myValue As Object

            Public ReadOnly Property HasValue() As Boolean
                Get
                    Return Me.myValue IsNot System.DBNull.Value
                End Get
            End Property

            Public Sub New(aDBNull As System.DBNull)
                Me.myValue = aDBNull
            End Sub

            Public Sub New(newValue As Long)
                Me.myValue = newValue
            End Sub

            Public Overrides Function ToString() As String
                Return Me.myValue.ToString()
            End Function

            Public Overrides Function Equals(obj As Object) As Boolean
                If Not Me.HasValue Then
                    Return obj Is Nothing OrElse obj Is System.DBNull.Value
                End If
                Return Me.myValue.ToString() = obj.ToString()
            End Function

            Public Overrides Function GetHashCode() As Integer
                Return Me.myValue.GetHashCode()
            End Function

            Public Shared Widening Operator CType(newValue As Long) As DbInt64
                Return New DbInt64(newValue)
            End Operator

            Public Shared Widening Operator CType(aDBNull As System.DBNull) As DbInt64
                Return New DbInt64(aDBNull)
            End Operator

            Public Shared Widening Operator CType(aDBType As DbInt64) As Long
                Return Convert.ToInt64(aDBType.myValue)
            End Operator

            Public Shared Operator =(aDBType As DbInt64, aDBNull As System.DBNull) As Boolean
                Return aDBType.myValue Is aDBNull
            End Operator

            Public Shared Operator <>(aDBType As DbInt64, aDBNull As System.DBNull) As Boolean
                Return aDBType.myValue IsNot aDBNull
            End Operator
        End Structure

        <System.Serializable()>
        Public Structure DbString
            Private myValue As Object

            Public ReadOnly Property HasValue() As Boolean
                Get
                    Return Me.myValue IsNot System.DBNull.Value
                End Get
            End Property

            Public Sub New(aDBNull As System.DBNull)
                Me.myValue = aDBNull
            End Sub

            Public Sub New(newValue As String)
                Me.myValue = newValue
            End Sub

            Public Overrides Function ToString() As String
                Return Me.myValue.ToString()
            End Function

            Public Shared Widening Operator CType(newValue As String) As DbString
                Return New DbString(newValue)
            End Operator

            Public Shared Widening Operator CType(aDBNull As System.DBNull) As DbString
                Return New DbString(aDBNull)
            End Operator

            Public Shared Widening Operator CType(aDBType As DbString) As String
                Return aDBType.myValue.ToString()
            End Operator

            Public Overrides Function Equals(obj As Object) As Boolean
                If Not Me.HasValue Then
                    Return obj Is Nothing OrElse obj Is System.DBNull.Value
                End If
                Return Me.myValue.ToString() = obj.ToString()
            End Function

            Public Overrides Function GetHashCode() As Integer
                Return Me.myValue.GetHashCode()
            End Function

            Public Shared Operator =(aDBType As DbString, aDBNull As System.DBNull) As Boolean
                Return aDBType.myValue Is aDBNull
            End Operator

            Public Shared Operator <>(aDBType As DbString, aDBNull As System.DBNull) As Boolean
                Return aDBType.myValue IsNot aDBNull
            End Operator
        End Structure
    End Class
End Namespace
