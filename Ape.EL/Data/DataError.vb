﻿Imports Ape.EL.Localization
Imports System
Imports System.Collections
Imports System.Data.SqlClient

Namespace Data
    Public Class DataError
        '        Private ReadOnly lic As Boolean = Internal.CheckMyLicense

        Public Const ERR_CONNECTION As Integer = 17

        Public Const ERR_NETWORK As Integer = 11

        Public Const ERR_TIMEOUT As Integer = -2

        Public Const ERR_LOCATING_INSTANCE As Integer = -1

        Public Const ERR_CONNECTING_SQL2005 As Integer = 53

        Public Const ERR_THE_SPECIFIED_NETWORK_NAME_IS_NO_LONGER_AVAILABLE As Integer = 64

        Public Const ERR_CANNOT_ALLOCATE_SPACE As Integer = 1105

        Public Const ERR_CONNECTING_SQL2005_2 As Integer = 1326

        Public Const ERR_CONNECTING_SQL2005_3 As Integer = 10060

        Public Const ERR_CONNECTING_SQL2005_4 As Integer = 10061

        Public Const ERR_CONNECTING_SQL2005_5 As Integer = 1231

        Public Const ERR_CONNECTING_SQL2005_6 As Integer = 11001

        Public Const ERR_CONNECTING_SQL2005_7 As Integer = 10051

        Public Const ERR_CONNECTING_SQL2005_8 As Integer = 10065

        Public Const ERR_CONNECTING_SQL2005_9 As Integer = 1311

        Public Const ERR_CONNECTION_FORCIBLY_CLOSED As Integer = 10054

        Public Const ERR_PRIMARYKEY As Integer = 2627

        Public Const ERR_FOREIGNKEY As Integer = 547

        Public Const ERR_DEADLOCK As Integer = 1205

        Public Const ERR_DATABASE_ALREADY_EXISTS As Integer = 1801

        Public Const ERR_CANNOT_OVERWRITTEN As Integer = 1834

        Public Const ERR_BACKUP_IN_TRANSACTION As Integer = 3021

        Public Const ERR_OPENBACKUPDEVICE As Integer = 3201

        Public Const ERR_WRITEBACKUPDEVICE As Integer = 3202

        Public Const ERR_NOT_SUPPORTED_MICROSOFT_TAPE_FORMAT As Integer = 3239

        Public Const ERR_INVALID_MICROSOFT_TAPE_FORMAT As Integer = 3242

        Public Const ERR_OTHER_MICROSOFT_TAPE_FORMAT_VERSION As Integer = 3243

        Public Const ERR_INSUFFICIENT_FREE_SPACE_TO_CREATEDATABASE As Integer = 3257

        Public Const ERR_NOT_EXISTS_OR_NOT_HAVE_PERMISSION As Integer = 3701

        Public Const ERR_CANNOT_DROP_BECAUSE_IN_USE As Integer = 3702

        Public Const ERR_CANNOT_DETACH_BECAUSE_IN_USE As Integer = 3703

        Public Const ERR_CANNOT_OPEN_DATABASE_LOGINFAILED As Integer = 4060

        Public Const ERR_DEVICE_ACTIVATION_ERROR As Integer = 5105

        Public Const ERR_COMPRESSED_DRIVE_ERROR As Integer = 5118

        Public Const ERR_CANNOT_OPEN_DATABASE_FILE_NOT_FOUND_LOGINFAILED As Integer = 5120

        Public Const ERR_CREATEDATABASE As Integer = 5170

        Public Const ERR_CREATEDATABASE_FAILED As Integer = 1802

        Public Const ERR_LOGINFAILED As Integer = 18456

        Public Const ERR_DROPDATABASEFAILED As Integer = 15181

        Public Const ERR_TRANSPORT_LEVEL_ERROR As Integer = 121

        Public Const ERR_LOGIN_PROCESS As Integer = 233

        Public Const ERR_5242 As Integer = 5242

        Public Const ERR_11004 As Integer = 11004

        Private Shared myExceptionHandlerList As System.Collections.ArrayList

        Public Shared Sub AddExceptionHandler(handler As ExceptionHandlerDelegate)
            If DataError.myExceptionHandlerList Is Nothing Then
                DataError.myExceptionHandlerList = New System.Collections.ArrayList()
            End If
            DataError.myExceptionHandlerList.Add(handler)
        End Sub

        Private Shared Function GetConstraintName(msg As String) As String
            Dim num As Integer = msg.IndexOf("FK_")
            If num >= 0 Then
                Dim num2 As Integer = num + 3
                Dim length As Integer = msg.Length
                While num2 < length AndAlso msg(num2) <> """" AndAlso msg(num2) <> "'"
                    num2 += 1
                End While
                Return msg.Substring(num, num2 - num)
            End If
            Return ""
        End Function

        Private Shared Function GetUniqueKeyConstraintName(msg As String) As String
            Dim num As Integer = msg.IndexOf("UNIQUE KEY constraint '")
            If num = -1 Then
                Return ""
            End If
            Dim num2 As Integer = msg.IndexOf("'", num + 23)
            If num2 = -1 Then
                Return ""
            End If
            Return msg.Substring(num + 23, num2 - num - 23)
        End Function

        Private Shared Function HandleBasicSqlException(ex As SqlException) As System.Exception
            If ex.Number = 17 Then
                Return New CriticalSqlException(Localizer.GetString(StringId.ErrorConnecting, New Object(0 - 1) {}), ex)
            End If
            If ex.Number = 53 Then
                Return New CriticalSqlException(Localizer.GetString(StringId.ErrorConnectingSQL2005, New Object(0 - 1) {}), ex)
            End If
            If ex.Number = 1326 Then
                Return New CriticalSqlException(Localizer.GetString(StringId.ErrorConnectingSQL2005, New Object(0 - 1) {}), ex)
            End If
            If ex.Number = 10060 Then
                Dim [string] As String = Localizer.GetString(StringId.ErrorConnectingSQL2005, New Object(0 - 1) {})
                Dim string2 As String = Localizer.GetString(StringId.ErrorSQL2005_10060, New Object(0 - 1) {})
                Return New CriticalSqlException(String.Format("{0}" & vbLf & vbLf & "{1}", [string], string2), ex)
            End If
            If ex.Number = 10061 Then
                Dim string3 As String = Localizer.GetString(StringId.ErrorConnectingSQL2005, New Object(0 - 1) {})
                Dim string4 As String = Localizer.GetString(StringId.ErrorSQL2005_10061, New Object(0 - 1) {})
                Return New CriticalSqlException(String.Format("{0}" & vbLf & vbLf & "{1}", string3, string4), ex)
            End If
            If ex.Number = 1231 Then
                Dim string5 As String = Localizer.GetString(StringId.ErrorConnectingSQL2005, New Object(0 - 1) {})
                Dim string6 As String = Localizer.GetString(StringId.ErrorSQL2005_1231, New Object(0 - 1) {})
                Return New CriticalSqlException(String.Format("{0}" & vbLf & vbLf & "{1}", string5, string6), ex)
            End If
            If ex.Number = 11001 Then
                Dim string7 As String = Localizer.GetString(StringId.ErrorConnectingSQL2005, New Object(0 - 1) {})
                Dim string8 As String = Localizer.GetString(StringId.ErrorSQL2005_11001, New Object(0 - 1) {})
                Return New CriticalSqlException(String.Format("{0}" & vbLf & vbLf & "{1}", string7, string8), ex)
            End If
            If ex.Number = 10051 Then
                Dim string9 As String = Localizer.GetString(StringId.ErrorConnectingSQL2005, New Object(0 - 1) {})
                Dim string10 As String = Localizer.GetString(StringId.ErrorSQL2005_11001, New Object(0 - 1) {})
                Return New CriticalSqlException(String.Format("{0}" & vbLf & vbLf & "{1}", string9, string10), ex)
            End If
            If ex.Number = 10065 Then
                Dim string11 As String = Localizer.GetString(StringId.ErrorConnectingSQL2005, New Object(0 - 1) {})
                Dim string12 As String = Localizer.GetString(StringId.ErrorSQL2005_10065, New Object(0 - 1) {})
                Return New CriticalSqlException(String.Format("{0}" & vbLf & vbLf & "{1}", string11, string12), ex)
            End If
            If ex.Number = 1311 Then
                Dim string13 As String = Localizer.GetString(StringId.ErrorConnectingSQL2005, New Object(0 - 1) {})
                Dim string14 As String = Localizer.GetString(StringId.ErrorSQL2005_1311, New Object(0 - 1) {})
                Return New CriticalSqlException(String.Format("{0}" & vbLf & vbLf & "{1}", string13, string14), ex)
            End If
            If ex.Number = 10054 Then
                Return New CriticalSqlException(Localizer.GetString(StringId.ErrorConnectionForciblyClosed, New Object(0 - 1) {}), ex)
            End If
            If ex.Number = 11 Then
                Return New CriticalSqlException(Localizer.GetString(StringId.ErrorGeneralNetwork, New Object(0 - 1) {}), ex)
            End If
            If ex.Number = -2 Then
                Return New CriticalSqlException(Localizer.GetString(StringId.ErrorTimeoutExpired, New Object(0 - 1) {}), ex)
            End If
            If ex.Number = -1 Then
                Return New CriticalSqlException(Localizer.GetString(StringId.ErrorLocatingServerOrInstance, New Object(0 - 1) {}), ex)
            End If
            If ex.Number = 64 Then
                Return New CriticalSqlException(Localizer.GetString(StringId.ErrorTheSpecifiedNetworkNameIsNoLongerAvailable, New Object(0 - 1) {}), ex)
            End If
            If ex.Number = 18456 Then
                Return New CriticalSqlException(Localizer.GetString(StringId.ErrorLoginFailed, New Object(0 - 1) {}), ex)
            End If
            If ex.Number = 1205 Then
                Return New CriticalSqlException(Localizer.GetString(StringId.ErrorDeadLock, New Object(0 - 1) {}), ex)
            End If
            If ex.Number = 5170 OrElse ex.Number = 1802 OrElse ex.Number = 1105 OrElse ex.Number = 3201 OrElse ex.Number = 3202 OrElse ex.Number = 3021 OrElse ex.Number = 1834 OrElse ex.Number = 1801 OrElse ex.Number = 3239 OrElse ex.Number = 3257 OrElse ex.Number = 3242 OrElse ex.Number = 3243 OrElse ex.Number = 15181 OrElse ex.Number = 3702 OrElse ex.Number = 3701 OrElse ex.Number = 3703 OrElse ex.Number = 5105 OrElse ex.Number = 5118 OrElse ex.Number = 4060 OrElse ex.Number = 5120 OrElse ex.Number = 121 OrElse ex.Number = 233 OrElse ex.Number = 5242 OrElse ex.Number = 11004 Then
                Return New CriticalSqlException(ex.Message, ex)
            End If
            If ex.Number = 2627 Then
                Dim uniqueKeyConstraintName As String = DataError.GetUniqueKeyConstraintName(ex.Message)
                Return New PrimaryKeyException(Localizer.GetString(StringId.ErrorPrimaryKey, New Object(0 - 1) {}), uniqueKeyConstraintName, ex)
            End If
            If ex.Number = 547 Then
                Dim constraintName As String = DataError.GetConstraintName(ex.Message)
                If constraintName.Length = 0 Then
                    Return New ForeignKeyException(Localizer.GetString(StringId.ErrorForeignKeyMessage, New Object() {ex.Message}), "", ex)
                End If
                Return New ForeignKeyException(Localizer.GetString(StringId.ErrorForeignKeyConstraint, New Object() {constraintName}), constraintName, ex)
            Else
                If ex.Number < 100 Then
                    Return New CriticalSqlException(Localizer.GetString(StringId.ErrorCriticalSql, New Object() {ex.Number, ex.Message}), ex)
                End If
                Return New System.Exception(Localizer.GetString(StringId.ErrorUnknownSQLError, New Object() {ex.Number, ex.Message}), ex)
            End If
        End Function

        Public Shared Sub HandleSqlException(ex As SqlException)
            Dim ex2 As System.Exception = DataError.HandleBasicSqlException(ex)
            If DataError.myExceptionHandlerList IsNot Nothing Then
                For i As Integer = DataError.myExceptionHandlerList.Count - 1 To 0 Step -1
                    Dim exceptionHandlerDelegate As ExceptionHandlerDelegate = CType(DataError.myExceptionHandlerList(i), ExceptionHandlerDelegate)
                    If exceptionHandlerDelegate IsNot Nothing Then
                        Dim exceptionHandlerEventArgs As ExceptionHandlerEventArgs = New ExceptionHandlerEventArgs(ex2)
                        exceptionHandlerDelegate(exceptionHandlerEventArgs)
                        If exceptionHandlerEventArgs.Handled Then
                            Throw exceptionHandlerEventArgs.Exception
                        End If
                    End If
                Next
            End If
            Throw ex2
        End Sub

    End Class
End Namespace
