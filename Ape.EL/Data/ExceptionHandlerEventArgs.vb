﻿Namespace Data
    Public Class ExceptionHandlerEventArgs
        Private myException As System.Exception

        Private myHandled As Boolean

        Public Property Exception() As System.Exception
            Get
                Return Me.myException
            End Get
            Set(value As System.Exception)
                Me.myException = value
            End Set
        End Property

        Public Property Handled() As Boolean
            Get
                Return Me.myHandled
            End Get
            Set(value As Boolean)
                Me.myHandled = value
            End Set
        End Property

        Public Sub New(ex As System.Exception)
            Me.myException = ex
        End Sub
    End Class
End Namespace
