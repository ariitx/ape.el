﻿
Imports System
Imports System.Data
Imports System.Runtime.InteropServices
Imports System.Text
Imports System.Data.Sql

Namespace Data
    Public Class SqlLocator
'        Private ReadOnly lic As Boolean = Internal.CheckMyLicense

        Private Const SQL_HANDLE_ENV As Short = 1

        Private Const SQL_HANDLE_DBC As Short = 2

        Private Const SQL_ATTR_ODBC_VERSION As Integer = 200

        Private Const SQL_OV_ODBC3 As Integer = 3

        Private Const SQL_SUCCESS As Short = 0

        Private Const SQL_NEED_DATA As Short = 99

        Private Const DEFAULT_RESULT_SIZE As Short = 1024

        Private Const SQL_DRIVER_STR As String = "DRIVER=SQL SERVER"

        Private Declare Function SQLAllocHandle Lib "odbc32.dll" (hType As Short, inputHandle As System.IntPtr, <Out()> ByRef outputHandle As System.IntPtr) As Short

        Private Declare Function SQLSetEnvAttr Lib "odbc32.dll" (henv As System.IntPtr, attribute As Integer, valuePtr As System.IntPtr, strLength As Integer) As Short

        Private Declare Function SQLFreeHandle Lib "odbc32.dll" (hType As Short, handle As System.IntPtr) As Short

        Private Declare Ansi Function SQLBrowseConnect Lib "odbc32.dll" (hconn As System.IntPtr, inString As System.Text.StringBuilder, inStringLength As Short, outString As System.Text.StringBuilder, outStringLength As Short, ByRef outLengthNeeded As Short) As Short

        Private Sub New()
        End Sub

        Public Shared Function GetServers() As String()
            Dim result As String() = Nothing
            Dim text As String = String.Empty
            Dim zero As System.IntPtr = System.IntPtr.Zero
            Dim zero2 As System.IntPtr = System.IntPtr.Zero
            Dim stringBuilder As System.Text.StringBuilder = New System.Text.StringBuilder("DRIVER=SQL SERVER")
            Dim stringBuilder2 As System.Text.StringBuilder = New System.Text.StringBuilder(1024)
            Dim inStringLength As Short = CShort(stringBuilder.Length)
            Dim num As Short = 0
            Try
                If SqlLocator.SQLAllocHandle(1, zero, zero) = 0 AndAlso SqlLocator.SQLSetEnvAttr(zero, 200, CType(3, System.IntPtr), 0) = 0 AndAlso SqlLocator.SQLAllocHandle(2, zero, zero2) = 0 AndAlso 99 = SqlLocator.SQLBrowseConnect(zero2, stringBuilder, inStringLength, stringBuilder2, 1024, num) Then
                    If 1024 < num Then
                        stringBuilder2.Capacity = CInt(num)
                        If 99 <> SqlLocator.SQLBrowseConnect(zero2, stringBuilder, inStringLength, stringBuilder2, num, num) Then
                            Throw New System.ApplicationException("Unabled to acquire SQL Servers from ODBC driver.")
                        End If
                    End If
                    text = stringBuilder2.ToString()
                    Dim num2 As Integer = text.IndexOf("{") + 1
                    Dim num3 As Integer = text.IndexOf("}") - num2
                    If num2 > 0 AndAlso num3 > 0 Then
                        text = text.Substring(num2, num3)
                    Else
                        text = String.Empty
                    End If
                End If
            Catch ex_FA As Exception
                text = String.Empty
            Finally
                If zero2 <> System.IntPtr.Zero Then
                    SqlLocator.SQLFreeHandle(2, zero2)
                End If
                If zero <> System.IntPtr.Zero Then
                    SqlLocator.SQLFreeHandle(1, zero2)
                End If
            End Try
            If text.Length > 0 Then
                result = text.Split(",".ToCharArray())
            End If
            Return result
        End Function

        Public Shared Function GetServersTable() As DataTable
            Dim servers As String() = SqlLocator.GetServers()
            Dim dataTable As DataTable = New DataTable()
            dataTable.Columns.Add("SQL Server", System.Type.[GetType]("System.String"))
            If servers IsNot Nothing Then
                Dim array As String() = servers
                For i As Integer = 0 To array.Length - 1
                    Dim text As String = array(i)
                    dataTable.Rows.Add(New Object() {text})
                Next
            End If
            Return dataTable
        End Function

        ''' <summary>
        ''' Shorter cleaner code to get server names instances, includes database version, still cant show local instances!
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetServersTableEx() As DataTable
            Dim instance As SqlDataSourceEnumerator = SqlDataSourceEnumerator.Instance
            Dim dt As DataTable = instance.GetDataSources()
            Return dt
        End Function
    End Class
End Namespace
