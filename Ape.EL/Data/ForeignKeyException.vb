﻿
Imports System
Imports System.Runtime.Serialization

Namespace Data
    <System.Serializable()>
    Public Class ForeignKeyException
        Inherits DataAccessException

        Private myConstraintName As String

        Public ReadOnly Property ConstraintName() As String
            Get
                Return Me.myConstraintName
            End Get
        End Property

        Public Sub New(message As String)
            MyBase.New(message)
        End Sub

        Public Sub New(message As String, constraintName As String)
            MyBase.New(message)
            Me.myConstraintName = constraintName
        End Sub

        Public Sub New(message As String, constraintName As String, innerException As System.Exception)
            MyBase.New(message, innerException)
            Me.myConstraintName = constraintName
        End Sub

        Protected Sub New(info As System.Runtime.Serialization.SerializationInfo, context As System.Runtime.Serialization.StreamingContext)
            MyBase.New(info, context)
        End Sub
    End Class
End Namespace
