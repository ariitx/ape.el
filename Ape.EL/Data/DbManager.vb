﻿Imports Ape.EL.Setting.MySetting
Imports Ape.EL.WinForm.General
Imports System.IO
Imports System.Data.SqlClient

Namespace Data
    Public Class DbManager
#Region "ConnectionInfo Class"
        Public Class ConnectionInfo
            Property Description As String
                Get
                    Return _Description
                End Get
                Set(value As String)
                    _Description = value
                End Set
            End Property
            Private _Description As String

            Property Catalogue As String
                Get
                    Return _Catalogue
                End Get
                Set(value As String)
                    _Catalogue = value
                End Set
            End Property
            Private _Catalogue As String

            Property ServerName As String
                Get
                    Return _ServerName
                End Get
                Set(value As String)
                    _ServerName = value
                End Set
            End Property
            Private _ServerName As String

            Property IntegratedSecurity As String
                Get
                    Return _IntegratedSecurity
                End Get
                Set(value As String)
                    _IntegratedSecurity = IIf(value = "", False, value)
                End Set
            End Property
            Private _IntegratedSecurity As Boolean

            Property Username As String
                Get
                    Return _Username
                End Get
                Set(value As String)
                    _Username = value
                End Set
            End Property
            Private _Username As String

            Property Password As String
                Get
                    Return _Password
                End Get
                Set(value As String)
                    _Password = value
                End Set
            End Property
            Private _Password As String

            Function GetConnectionString(Optional ByVal bMultipleActiveResultSets As Boolean = True) As String
                Return Ape.EL.Data.DbUtility.GetConnectionString(_ServerName, _
                                                                  _Catalogue, _
                                                                  _Username, _
                                                                  _Password, _
                                                                  _IntegratedSecurity, _
                                                                  bMultipleActiveResultSets)
            End Function

            Function GetCSB(Optional ByVal bMultipleActiveResultSets As Boolean = True) As SqlConnectionStringBuilder
                Return Ape.EL.Data.DbUtility.GetCsb(GetConnectionString(bMultipleActiveResultSets))
            End Function

            Shared Function GetConnectionInfo(connString As String) As ConnectionInfo
                Return GetConnectionInfo(Ape.EL.Data.DbUtility.GetCsb(connString))
            End Function

            Shared Function GetConnectionInfo(csb As SqlConnectionStringBuilder) As ConnectionInfo
                Dim ci As New ConnectionInfo
                ci.ServerName = csb.DataSource
                ci.Catalogue = csb.InitialCatalog
                ci.IntegratedSecurity = csb.IntegratedSecurity
                ci.Username = csb.UserID
                ci.Password = csb.Password
                Return ci
            End Function
        End Class
#End Region

#Region "DbPhysicalPath"
        Public Class DbPhysicalPath
            Property RowPath As String
                Get
                    Return _RowPath
                End Get
                Set(value As String)
                    _RowPath = value
                End Set
            End Property
            Private _RowPath As String

            Property LogPath As String
                Get
                    Return _LogPath
                End Get
                Set(value As String)
                    _LogPath = value
                End Set
            End Property
            Private _LogPath As String
        End Class
#End Region

#Region "Database Manager"
        Shared ReadOnly Property DefaultDatabasePath As String
            Get
                Return String.Format("{0}{2}{1}DATA{1}", _
                                     System.IO.Path.GetPathRoot(Environment.SystemDirectory), _
                                     System.IO.Path.DirectorySeparatorChar, _
                                     Ape.EL.Utility.General.GetAsmNamespace(Internal.GetMyAssembly, True))
            End Get
        End Property

        Shared Function CreateDatabase(ByVal oGeneral As DbGeneral,
                                       ByVal databaseName As String,
                                       ByVal createPath As String) As Boolean
            Try
                If Not createPath.EndsWith(System.IO.Path.DirectorySeparatorChar) Then createPath = createPath & System.IO.Path.DirectorySeparatorChar

                Dim SQL As String = <a>
                                        CREATE DATABASE [<%= databaseName %>] ON PRIMARY (
	                                        NAME = N'<%= databaseName %>'
	                                        ,FILENAME = N'<%= createPath & databaseName %>.mdf'
	                                        ) LOG ON (
	                                        NAME = N'<%= databaseName %>_Log'
	                                        ,FILENAME = N'<%= createPath & databaseName %>_Log.ldf'
	                                        )
                                    </a>.Value

                Return oGeneral.ExecuteNonQuery(SQL)
            Catch exSQL As SqlClient.SqlException
                If Not String.IsNullOrEmpty(exSQL.Message) Then
                    WarningMsg(exSQL.Message)
                End If
            Catch ex As Exception
                ExceptionErrorMsg(ex)
            End Try

            Return False
        End Function

        Shared Function DropDatabase(ByVal oGeneral As DbGeneral,
                                     ByVal databaseName As String) As Boolean
            Try
                Dim SQL As String = <a>
                                        DROP DATABASE [<%= databaseName %>]
                                    </a>.Value

                Return oGeneral.ExecuteNonQuery(SQL)
            Catch exSQL As SqlClient.SqlException
                If Not String.IsNullOrEmpty(exSQL.Message) Then
                    WarningMsg(exSQL.Message)
                End If
            Catch ex As Exception
                ExceptionErrorMsg(ex)
            End Try

            Return False
        End Function

        Shared Function BackupDatabase(ByVal oGeneral As DbGeneral,
                                       ByVal databaseName As String,
                                       ByVal backupPath As String,
                                       ByVal backupFileName As String,
                                       ByVal useZip As Boolean) As Boolean
            Dim bSuceed As Boolean = True
            Dim strAppDataDir As String = Ape.EL.Utility.General.GetStartUpPathDirectory()
            Dim path As String = IO.Path.GetTempPath & strAppDataDir

            Try
                Dim strSQL As String = ""
                Dim tmpBackupFullFilePath As String = String.Format("{0}{1}{2}.bak", path, System.IO.Path.DirectorySeparatorChar, backupFileName)

                'Create temporary folder to contain the database file
                If Not Directory.Exists(path) Then
                    Ape.EL.Utility.General.MakePath(path)
                End If
                Ape.EL.Utility.General.SetFullAccess(path)

                'back up database file into that temporary folder
                strSQL = String.Format("BACKUP DATABASE {0} TO DISK = '{1}'", databaseName, tmpBackupFullFilePath)
                If Not oGeneral.ExecuteNonQuery(strSQL) Then
                    bSuceed = False
                End If

                If bSuceed Then
                    If useZip Then
                        'zip the file and copy to the DbPath
                        Ape.EL.Utility.Data.Zip.Create(String.Format("{0}{1}{2}.zip", backupPath, System.IO.Path.DirectorySeparatorChar, backupFileName), path)
                    Else
                        IO.File.Copy(tmpBackupFullFilePath, path, True)
                    End If
                End If
            Catch ex As Exception
                Ape.EL.WinForm.General.ExceptionErrorMsg(ex)
                bSuceed = False
            End Try

            Try
                'Delete the temporary folder
                If Directory.Exists(path) Then
                    System.IO.Directory.Delete(path, True)
                End If
            Catch ex As Exception
            End Try

            Return bSuceed
        End Function

        ''' <summary>
        ''' Restore database file from zip or bak.
        ''' </summary>
        ''' <param name="backupFileName">file as zip or bak</param>
        ''' <param name="restorePath">Physical file path</param>
        Shared Function RestoreDatabase(ByVal oGeneral As DbGeneral,
                                       ByVal backupFileName As String,
                                       ByVal databaseName As String,
                                       ByVal restorePath As String,
                                       ByVal withReplace As Boolean) As Boolean
            If Not restorePath.EndsWith(System.IO.Path.DirectorySeparatorChar) Then restorePath = restorePath & System.IO.Path.DirectorySeparatorChar

            Dim bSuceed As Boolean = True
            Dim strAppDataDir As String = Ape.EL.Utility.General.GetStartUpPathDirectory()
            Dim path As String = IO.Path.GetTempPath & strAppDataDir
            Dim useZip As Boolean = backupFileName.ToUpperInvariant.EndsWith(".ZIP")
            Dim bakFile As String = ""
            Try
                Dim strSQL As String = ""

                'Create temporary folder to contain the database file
                If Not Directory.Exists(path) Then
                    Ape.EL.Utility.General.MakePath(path)
                End If
                Ape.EL.Utility.General.SetFullAccess(path)

                If useZip Then
                    'zip the file and copy to the DbPath
                    Ape.EL.Utility.Data.Zip.ExtractZipFile(backupFileName, Nothing, path)
                    Dim files As String() = System.IO.Directory.GetFiles(path)
                    For Each f As String In files
                        bakFile = backupFileName
                    Next
                Else
                    bakFile = backupFileName
                End If

                If Ape.EL.Utility.General.IsValidFileNameOrPath(bakFile) Then
                    'Restore backup file
                    strSQL = String.Format(<SQL>
                                               RESTORE DATABASE [{0}] FROM DISK = N'{1}'
                                               {2},
                                               MOVE N'{0}' TO N'{3}{0}.mdf',
                                               MOVE N'{0}_Log' TO N'{3}{0}_Log.ldf'
                                           </SQL>.Value, _
                                           databaseName, bakFile, If(withReplace, "WITH REPLACE", ""), restorePath)
                    If Not oGeneral.ExecuteNonQuery(strSQL) Then
                        bSuceed = False
                    End If
                Else
                    bSuceed = False
                End If
            Catch ex As Exception
                Ape.EL.WinForm.General.ExceptionErrorMsg(ex)
                bSuceed = False
            End Try

            Try
                'Delete the temporary folder
                If Directory.Exists(path) Then
                    System.IO.Directory.Delete(path, True)
                End If
            Catch ex As Exception
            End Try

            Return bSuceed
        End Function

        Shared Function AttachDatabase(ByVal oGeneral As DbGeneral,
                                       ByVal mdfFilePath As String,
                                       ByVal ldfFilePath As String)
            Dim bSucceed As Boolean = True

            Try
                Dim strSQL As String = ""

                strSQL = String.Format(<SQL>
                                           CREATE DATABASE {0}
                                           ON (FILENAME = '{1}') -- Main Data File .mdf
                                           ,(FILENAME = '{2}') -- Log file .ldf
                                           FOR ATTACH 
                                       </SQL>.Value, _
                                       System.IO.Path.GetFileNameWithoutExtension(mdfFilePath), _
                                       mdfFilePath, _
                                       ldfFilePath)
                bSucceed = oGeneral.ExecuteNonQuery(strSQL)

            Catch ex As Exception
                Ape.EL.WinForm.General.ExceptionErrorMsg(ex)
                bSucceed = False
            End Try

            Return bSucceed
        End Function

        Shared Function DetachDatabase(ByVal oGeneral As DbGeneral,
                                       ByVal databaseName As String)
            Dim bSuceed As Boolean = True

            Try
                Dim strSQL As String = ""

                strSQL = String.Format(<SQL>
                                           EXEC sp_detach_db '{0}'
                                       </SQL>.Value, _
                                       databaseName)
                oGeneral.ExecuteNonQuery(strSQL)

            Catch ex As Exception
                Ape.EL.WinForm.General.ExceptionErrorMsg(ex)
                bSuceed = False
            End Try

            Return bSuceed
        End Function

        ''' <summary>
        ''' Get database's physical path of log and mdf files.
        ''' </summary>
        Shared Function GetDatabasePath(ByVal oGeneral As DbGeneral,
                                        ByVal databaseName As String) As DbPhysicalPath
            Dim oDb As New DbPhysicalPath

            Try
                Dim SQL As String

                '1. Get MDF path
                SQL = String.Format(
                        <a>
                            SELECT physical_name
                            FROM sys.master_files
                            WHERE physical_name LIKE '%{0}{1}.mdf'
                        </a>.Value _
                        , System.IO.Path.DirectorySeparatorChar _
                        , databaseName)

                oDb.RowPath = oGeneral.ExecuteScalar(SQL)

                '2. Get LDF path
                SQL = String.Format(
                        <a>
                            SELECT physical_name
                            FROM sys.master_files
                            WHERE physical_name LIKE '%{0}{1}_Log.ldf'
                        </a>.Value _
                        , System.IO.Path.DirectorySeparatorChar _
                        , databaseName)

                oDb.LogPath = oGeneral.ExecuteScalar(SQL)

            Catch ex As Exception
                ExceptionErrorMsg(ex)
                oDb = Nothing
            End Try

            Return oDb
        End Function

        ''' <summary>
        ''' Check if specified database name exist.
        ''' </summary>
        Shared Function IsDatabaseExist(ByVal oGeneral As DbGeneral,
                                        ByVal databaseName As String) As Boolean
            Try
                Dim SQL As String = <a>
                                        SELECT CASE 
		                                    WHEN EXISTS (
				                                    SELECT 1
				                                    FROM sys.Databases
				                                    WHERE NAME = '<%= databaseName %>'
				                                    )
			                                    THEN 'True'
		                                    ELSE 'False'
		                                    END AS DbExists
                                    </a>.Value

                Return oGeneral.ExecuteScalar(SQL).ToBoolean
            Catch ex As Exception
                ExceptionErrorMsg(ex)
            End Try

            Return False
        End Function
#End Region

#Region "Remote Connection Registry"
        Private Const DmSetting As Char = Chr(13) 'delimiter for mysetting REMCON's setting value in dvRegHistory
        Private Const STR_REMCON As String = "REMCON" 'section to save remote connection history 

        ''' <summary>
        ''' Get list of saved remote connection from setting.
        ''' </summary>
        Shared Function GetRemconList() As List(Of ConnectionInfo)
            'Get records from REMCON
            Dim dvRegHistory As DataView = GetDataViewEx(STR_REMCON)
            If dvRegHistory.Count > 0 Then
                Dim cilist As New List(Of ConnectionInfo)
                For Each drvRegHistory As DataRowView In dvRegHistory
                    'dvRegHistory >> Key    | Setting
                    'ServerName+Catalogue   | ServerName(chr13)Description(chr13)Catalogue(chr13)IntegratedSecurity(chr13)Username(chr13)Password
                    Dim RegHistorySettings As String() = drvRegHistory("Setting").ToString.Split(DmSetting)
                    Dim ci As ConnectionInfo = Nothing

                    'populate data from the selected key to temporary variable of ConnectionInfo
                    If RegHistorySettings.Count = 6 And Not String.IsNullOrEmpty(drvRegHistory("Key").ToString) Then
                        ci = New ConnectionInfo
                        ci.ServerName = RegHistorySettings(0).ToString
                        ci.Description = RegHistorySettings(1).ToString
                        ci.Catalogue = RegHistorySettings(2).ToString
                        ci.IntegratedSecurity = Ape.EL.Data.Convert.ToBoolean(RegHistorySettings(3).ToString)
                        ci.Username = RegHistorySettings(4).ToString
                        ci.Password = RegHistorySettings(5).ToString
                        cilist.Add(ci)
                    Else
                        Throw New ApplicationException(STR_REMCON & " has corrupted record.")
                        Exit For
                    End If
                Next
                Return cilist
            End If

            Return New List(Of ConnectionInfo)
        End Function

        ''' <summary>
        ''' Save into remote connection history list.
        ''' </summary>
        Shared Function SaveRemcon(ByVal ci As ConnectionInfo) As Boolean
            'skip save when ci is empty
            If ci Is Nothing OrElse (ci.ServerName & ci.Catalogue).IsEmpty Then Return True

            'save setting to history
            Dim sbConn As New System.Text.StringBuilder
            sbConn.Append(ci.ServerName & DmSetting)
            sbConn.Append(ci.Description & DmSetting)
            sbConn.Append(ci.Catalogue & DmSetting)
            sbConn.Append(ci.IntegratedSecurity.ToString & DmSetting)
            sbConn.Append(ci.Username & DmSetting)
            sbConn.Append(ci.Password)
            SaveSettingEx(STR_REMCON, ci.ServerName & ci.Catalogue, sbConn.ToString)
            Return True
        End Function

        ''' <summary>
        ''' Delete from remote connection history list.
        ''' </summary>
        Shared Function DeleteRemcon(ByVal ci As ConnectionInfo) As Boolean
            Try
                DeleteSettingEx(STR_REMCON, ci.ServerName & ci.Catalogue)
                Return True
            Catch ex As Exception
            End Try
            Return False
        End Function

        ''' <summary>
        ''' Get path of remove connection setting file.
        ''' </summary>
        Shared ReadOnly Property RemconPath() As String
            Get
                Return Ape.EL.Setting.MySetting.Path & System.IO.Path.DirectorySeparatorChar & STR_REMCON
            End Get
        End Property
#End Region
    End Class
End Namespace
