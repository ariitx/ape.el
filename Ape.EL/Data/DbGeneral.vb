Imports Ape.EL.WinForm.General
Imports Ape.EL.Data
Imports Ape.EL.Extensions
Imports System.Data.SqlClient

Namespace Data
    ''' <summary>
    ''' Provide main interface to query database.
    ''' </summary>
    ''' <remarks></remarks>
    <System.Diagnostics.DebuggerStepThrough()> _
    Public Class DbGeneral
        Inherits DbStandardConnection
        '        Private ReadOnly lic As Boolean = Internal.CheckMyLicense

        ''' <summary> Indicate that this object is instantiated optionally and should be disposed. Please check ClGeneralHelper. Internal use only. </summary>
        Friend Property WithGeneral As Boolean
            Get
                Return _WithGeneral
            End Get
            Set(value As Boolean)
                _WithGeneral = value
            End Set
        End Property
        Private _WithGeneral As Boolean = False

        ''' <summary> Indicate that this object's Transaction property is instantiated optionally and should be disposed. Please check ClGeneralHelper. Internal use only. </summary>
        Friend Property WithTransaction As Boolean
            Get
                Return _WithTransaction
            End Get
            Set(value As Boolean)
                _WithTransaction = value
            End Set
        End Property
        Private _WithTransaction As Boolean = False

        ''' <summary>
        ''' Park an SqlTransaction to be used further in the data layer.
        ''' </summary>
        Public Property Transaction As SqlClient.SqlTransaction
            Get
                Return _Transaction
            End Get
            Set(value As SqlClient.SqlTransaction)
                _Transaction = value
            End Set
        End Property
        Private _Transaction As SqlClient.SqlTransaction = Nothing

        ''' <summary>
        ''' Get this object's method initializer.
        ''' </summary>
        Public ReadOnly Property Creator As String
            Get
                Return _Creator
            End Get
        End Property
        Private _Creator As String

        Public Sub New(ByVal strConnString As String)
            MyBase.New(strConnString)

            'Set creator
            Try
                Dim frame As System.Diagnostics.StackFrame = (New System.Diagnostics.StackTrace(App.Debug.IsDebug)).GetFrame(1)
                If frame IsNot Nothing Then
                    Dim strGetMethod As String = If(frame.GetMethod Is Nothing, "", frame.GetMethod.Name)
                    Dim strDeclaringType As String = If(frame.GetMethod.DeclaringType Is Nothing, "", frame.GetMethod.DeclaringType.ToString)
                    _Creator = String.Format("{0}.{1}{2}", strDeclaringType, strGetMethod, If(frame.GetFileLineNumber > 0, ":" & frame.GetFileLineNumber, ""))
                End If
            Catch ex As Exception
                _Creator.AppendLn("Couldn't parse StackFrame.")
                _Creator.AppendLn(ex.ToString)
            End Try
        End Sub

        ''' <summary>
        ''' Set Transaction parameters to get records based on transaction. Please use SqlString.Format to encapsulate single quote.
        ''' </summary>
        Function GetRecords(ByVal strSQL As String, Optional ByVal Transaction As SqlClient.SqlTransaction = Nothing) As SqlClient.SqlDataReader
            Return ConnectionSQL.GetRecords(strSQL, Transaction)
        End Function

        Function GetRecords(ByVal strSQL As String, ByVal param As List(Of SqlParameter), Optional ByVal Transaction As SqlClient.SqlTransaction = Nothing) As SqlClient.SqlDataReader
            Return ConnectionSQL.GetRecords(strSQL, param, Transaction)
        End Function

        ''' <summary>
        ''' Please use SqlString.Format to encapsulate single quote.
        ''' </summary>
        Function GetAdapter(ByVal strSQL As String) As SqlClient.SqlDataAdapter
            Return ConnectionSQL.GetAdapter(strSQL)
        End Function

        ''' <summary>
        ''' Please use SqlString.Format to encapsulate single quote.
        ''' </summary>
        Function GetDataTable(ByVal strSQL As String) As DataTable
            Return ConnectionSQL.GetDataTable(strSQL)
        End Function

        Function GetDataTable(ByVal strSQL As String, ByVal param As List(Of SqlParameter)) As DataTable
            Return ConnectionSQL.GetDataTable(strSQL, param)
        End Function

        ''' <summary>
        ''' Please use SqlString.Format to encapsulate single quote.
        ''' </summary>
        Function GetDataView(ByVal strSQL As String) As DataView
            Return ConnectionSQL.GetDataView(strSQL)
        End Function

        Function GetDataView(ByVal strSQL As String, ByVal param As List(Of SqlParameter)) As DataView
            Return ConnectionSQL.GetDataView(strSQL, param)
        End Function

        ''' <summary>
        ''' Please use SqlString.Format to encapsulate single quote.
        ''' </summary>
        Function GetDataRowView(ByVal strSQL As String, Optional ByVal Row As Integer = 0) As DataRowView
            Return ConnectionSQL.GetDataRowView(strSQL, Row)
        End Function

        Function GetDataRowView(ByVal strSQL As String, ByVal param As List(Of SqlParameter), Optional ByVal Row As Integer = 0) As DataRowView
            Return ConnectionSQL.GetDataRowView(strSQL, param, Row)
        End Function

        ''' <summary>
        ''' Get first column result from query. Please use SqlString.Format to encapsulate single quote.
        ''' </summary>
        Function ExecuteScalar(ByVal strSQL As String, Optional ByVal Row As Integer = 0) As String
            Return ConnectionSQL.ExecuteScalar(strSQL, Row)
        End Function

        Function ExecuteScalar(ByVal strSQL As String, ByVal param As List(Of SqlParameter), Optional ByVal Row As Integer = 0) As String
            Return ConnectionSQL.ExecuteScalar(strSQL, param, Row)
        End Function

        ''' <summary>
        ''' Please use SqlString.Format to encapsulate single quote.
        ''' </summary>
        Function ExecuteNonQuery(ByVal strSQL As String, Optional ByVal Transaction As SqlClient.SqlTransaction = Nothing) As Boolean
            Return ConnectionSQL.ExecuteNonQuery(strSQL, Transaction)
        End Function

        ''' <summary>
        ''' Separate batch query into several statements.
        ''' The GO must not between block-comments like /* GO */
        ''' </summary>
        Function GetBatchStatements(ByVal SQL As String) As List(Of String)
            Dim res As New List(Of String)

            If Not SQL.IsEmpty Then
                Dim sqlBatch As String = String.Empty
                SQL += vbLf & "GO" 'must end with GO to make sure last batch is included.
                ' make sure last batch is executed.
                For Each line As String In SQL.Split(New String(1) {vbLf, vbCr}, StringSplitOptions.RemoveEmptyEntries)
                    If line.ToUpperInvariant().Trim() <> "GO" Then
                        'append to current batch
                        sqlBatch += line & vbLf.ToString
                    Else
                        'if not empty, add to res and reset the batch
                        If Not sqlBatch.Trim.IsEmpty Then
                            res.Add(sqlBatch)
                            sqlBatch = String.Empty
                        End If
                    End If
                Next
            End If

            Return res
        End Function

#Region "SQL Version Info"
        Function GetSQLServerFullVersion() As String
            Return ConnectionSQL.GetSQLServerFullVersion
        End Function

        Function GetSQLServerEdition() As String
            Return ConnectionSQL.GetSQLServerEdition
        End Function

        Function GetSQLServerProductVersion() As String
            Return ConnectionSQL.GetSQLServerProductVersion
        End Function

        Function GetSQLServerProductLevel() As String
            Return ConnectionSQL.GetSQLServerProductLevel
        End Function
#End Region
    End Class
End Namespace
