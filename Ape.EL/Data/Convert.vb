﻿
Imports System
Imports System.Globalization
Imports System.Text
Imports Ape.EL.Data.DbDataTypes
Imports System.IO


Namespace Data
    Public Class Convert
        '        Private ReadOnly lic As Boolean = Internal.CheckMyLicense

        Private Shared Function BooleanToText(TF As Boolean) As String
            If TF Then
                Return "T"
            End If
            Return "F"
        End Function

        Private Shared Function TextToBoolean(obj As Object) As Boolean
            Return obj IsNot Nothing AndAlso obj IsNot System.DBNull.Value AndAlso (String.Compare(obj.ToString(), "T", True) = 0 OrElse String.Compare(obj.ToString(), "True", True) = 0)
        End Function

        Private Shared Function ToBooleanText(obj As Object) As String
            If obj Is Nothing OrElse obj.Equals(Nothing) OrElse obj Is System.DBNull.Value Then
                Return "F"
            End If
            Return BooleanToText(System.Convert.ToBoolean(obj))
        End Function

        Public Shared Function ToBoolean(obj As Object) As Boolean
            'the last part is behaving like CBool, convert 0 to false and other integer to true.
            Return obj IsNot Nothing AndAlso obj IsNot System.DBNull.Value AndAlso Not String.IsNullOrEmpty(obj) AndAlso (((obj.ToString.ToUpper = "TRUE" Or obj.ToString.ToUpper = "FALSE") AndAlso System.Convert.ToBoolean(obj)) OrElse (IsNumeric(obj) AndAlso obj <> "0"))
        End Function

        Public Shared Function ToDateTime(obj As Object) As System.DateTime
            If obj Is Nothing OrElse obj.Equals(Nothing) OrElse obj Is System.DBNull.Value Then
                Return System.DateTime.MinValue
            End If
            Return System.Convert.ToDateTime(obj)
        End Function

        Public Shared Function ToDecimal(obj As Object) As Decimal
            If obj Is Nothing OrElse obj.Equals(Nothing) OrElse obj Is System.DBNull.Value Or String.IsNullOrEmpty(obj) Then
                Return 0D
            End If
            Return System.Convert.ToDecimal(obj)
        End Function

        Public Shared Function ToSByte(obj As Object) As SByte
            If obj Is Nothing OrElse obj.Equals(Nothing) OrElse obj Is System.DBNull.Value Then
                Return 0
            End If
            Return System.Convert.ToSByte(obj)
        End Function

        Public Shared Function ToInt16(obj As Object) As Short
            If obj Is Nothing OrElse obj.Equals(Nothing) OrElse obj Is System.DBNull.Value Then
                Return 0
            End If
            Return System.Convert.ToInt16(obj)
        End Function

        Public Shared Function ToInt32(obj As Object) As Integer
            If obj Is Nothing OrElse obj Is System.DBNull.Value Or String.IsNullOrEmpty(obj) Then
                Return 0
            End If
            Return System.Convert.ToInt32(obj)
        End Function

        Public Shared Function ToInt64(obj As Object) As Long
            If obj Is Nothing OrElse obj Is System.DBNull.Value Then
                Return 0L
            End If
            Return System.Convert.ToInt64(obj)
        End Function

        Public Shared Function ToUInt64(obj As Object) As ULong
            If obj Is Nothing OrElse obj Is System.DBNull.Value Then
                Return 0UL
            End If
            Return System.Convert.ToUInt64(obj)
        End Function

        Public Shared Function ToGuid(obj As Object) As System.Guid
            If obj Is Nothing OrElse obj.Equals(Nothing) OrElse obj Is System.DBNull.Value Then
                Return System.Guid.Empty
            End If
            Return CType(obj, System.Guid)
        End Function

        Public Shared Function ToDBInt16(obj As Object) As DbInt16
            If obj Is Nothing OrElse obj.Equals(Nothing) OrElse obj Is System.DBNull.Value Then
                Return New DbInt16(System.DBNull.Value)
            End If
            Return New DbInt16(ToInt16(obj))
        End Function

        Public Shared Function ToDBInt32(obj As Object) As DbInt32
            If obj Is Nothing OrElse obj.Equals(Nothing) OrElse obj Is System.DBNull.Value Then
                Return New DbInt32(System.DBNull.Value)
            End If
            Return New DbInt32(ToInt32(obj))
        End Function

        Public Shared Function ToDBInt64(obj As Object) As DbInt64
            If obj Is Nothing OrElse obj.Equals(Nothing) OrElse obj Is System.DBNull.Value Then
                Return New DbInt64(System.DBNull.Value)
            End If
            Return New DbInt64(ToInt64(obj))
        End Function

        Public Shared Function ToDBDecimal(obj As Object) As DbDecimal
            If obj Is Nothing OrElse obj.Equals(Nothing) OrElse obj Is System.DBNull.Value Then
                Return New DbDecimal(System.DBNull.Value)
            End If
            Return New DbDecimal(ToDecimal(obj))
        End Function

        Public Shared Function ToDBString(obj As Object) As DbString
            If obj Is Nothing OrElse obj.Equals(Nothing) OrElse obj Is System.DBNull.Value Then
                Return New DbString(System.DBNull.Value)
            End If
            Return New DbString(obj.ToString())
        End Function

        Public Shared Function ToDBDateTime(obj As Object) As DbDateTime
            If obj Is Nothing OrElse obj.Equals(Nothing) OrElse obj Is System.DBNull.Value Then
                Return New DbDateTime(System.DBNull.Value)
            End If
            Return New DbDateTime(ToDateTime(obj))
        End Function

        Public Shared Function ToDBBoolean(obj As Object) As DbBoolean
            If obj Is Nothing OrElse obj.Equals(Nothing) OrElse obj Is System.DBNull.Value Then
                Return New DbBoolean(System.DBNull.Value)
            End If
            Return New DbBoolean(ToBoolean(obj))
        End Function

        Public Shared Function ToDBObject(aDBType As DbString) As Object
            If aDBType.Equals(Nothing) OrElse Not aDBType.HasValue Then
                Return System.DBNull.Value
            End If
            Return aDBType.ToString()
        End Function

        Public Shared Function ToDBObject(aDBType As DbDecimal) As Object
            If aDBType.Equals(Nothing) OrElse Not aDBType.HasValue Then
                Return System.DBNull.Value
            End If
            Dim num As Decimal = aDBType
            Return num
        End Function

        Public Shared Function ToDBObject(aDBType As DbInt16) As Object
            If aDBType.Equals(Nothing) OrElse Not aDBType.HasValue Then
                Return System.DBNull.Value
            End If
            Dim num As Short = aDBType
            Return num
        End Function

        Public Shared Function ToDBObject(aDBType As DbInt32) As Object
            If aDBType.Equals(Nothing) OrElse Not aDBType.HasValue Then
                Return System.DBNull.Value
            End If
            Dim num As Integer = aDBType
            Return num
        End Function

        Public Shared Function ToDBObject(aDBType As DbInt64) As Object
            If aDBType.Equals(Nothing) OrElse Not aDBType.HasValue Then
                Return System.DBNull.Value
            End If
            Dim num As Long = aDBType
            Return num
        End Function

        Public Shared Function ToDBObject(aDBType As DbDateTime) As Object
            If aDBType.Equals(Nothing) OrElse Not aDBType.HasValue Then
                Return System.DBNull.Value
            End If
            Dim dateTime As System.DateTime = aDBType
            Return dateTime
        End Function

        Public Shared Function ToDBObject(aDBType As DbBoolean) As Object
            If aDBType.Equals(Nothing) OrElse Not aDBType.HasValue Then
                Return System.DBNull.Value
            End If
            Dim tF As Boolean = aDBType
            Return BooleanToText(tF)
        End Function

        Public Shared Function RoundToNearest5Cents(d As Decimal) As Decimal
            d = Decimal.Round(d, 2, System.MidpointRounding.AwayFromZero)
            Dim flag As Boolean = False
            If d < 0D Then
                d = -d
                flag = True
            End If
            Dim num As Decimal = Decimal.Round(d, 1, System.MidpointRounding.AwayFromZero)
            Dim d2 As Decimal = num - d
            If d2 <= -0.03D Then
                num += 0.05D
            Else
                If d2 >= 0.03D Then
                    num -= 0.05D
                End If
            End If
            If Not flag Then
                Return num
            End If
            Return -num
        End Function

        Public Shared Function ToCultureInfoNumberWording(amount As Decimal, cultureInfo As System.Globalization.CultureInfo) As String
            If cultureInfo.Name = "zh-Hant" OrElse cultureInfo.Name = "zh-CHT" OrElse cultureInfo.Name = "zh-TW" Then
                Return ToChinese(amount, False)
            End If
            If cultureInfo.TwoLetterISOLanguageName = "zh" Then
                Return ToChinese(amount, True)
            End If
            If cultureInfo.TwoLetterISOLanguageName = "id" Then
                Return ToIndonesian(amount, "")
            End If
            If cultureInfo.TwoLetterISOLanguageName = "ms" Then
                Return ToBahasa(amount)
            End If
            If cultureInfo.TwoLetterISOLanguageName = "vi" Then
                Return ToVietnamese(amount)
            End If
            Return ToEnglish(amount)
        End Function

        Private Shared Sub ToThousandEnglish(amount As Integer, sb As System.Text.StringBuilder)
            Dim array As String() = New String() {"ONE", "TWO", "THREE", "FOUR", "FIVE", "SIX", "SEVEN", "EIGHT", "NINE", "TEN", "ELEVEN", "TWELVE", "THIRTEEN", "FOURTEEN", "FIFTEEN", "SIXTEEN", "SEVENTEEN", "EIGHTEEN", "NINETEEN"}
            Dim array2 As String() = New String() {"TWENTY", "THIRTY", "FORTY", "FIFTY", "SIXTY", "SEVENTY", "EIGHTY", "NINETY"}
            amount = amount Mod 1000
            Dim num As Integer = amount / 100
            If num > 0 Then
                sb.Append(array(num - 1))
                sb.Append(" HUNDRED")
            End If
            amount = amount Mod 100
            Dim num2 As Integer = amount / 10
            If num2 >= 2 Then
                If num > 0 Then
                    sb.Append(" ")
                End If
                sb.Append(array2(num2 - 2))
                Dim num3 As Integer = amount Mod 10
                If num3 > 0 Then
                    sb.Append(" ")
                    sb.Append(array(num3 - 1))
                    Return
                End If
            Else
                Dim num4 As Integer = amount Mod 20
                If num4 > 0 Then
                    If num > 0 Then
                        sb.Append(" ")
                    End If
                    sb.Append(array(num4 - 1))
                End If
            End If
        End Sub

        Public Shared Function ToEnglish(amount As Decimal) As String
            amount = Decimal.Round(amount, 2)
            Dim flag As Boolean
            If amount < 0D Then
                amount = -amount
                flag = True
            Else
                flag = False
            End If
            Dim stringBuilder As System.Text.StringBuilder = New System.Text.StringBuilder(256)
            If flag Then
                stringBuilder.Append("(")
            End If
            Dim flag2 As Boolean = False
            Dim num As Long = CLng(amount)
            Dim num2 As Long = 1000000000000000L
            Dim num3 As Long = num / num2
            If num3 > 0L Then
                ToThousandEnglish(CInt(num3), stringBuilder)
                stringBuilder.Append(" QUADRILLION")
                flag2 = True
            End If
            num = num Mod num2
            Dim num4 As Long = 1000000000000L
            num3 = num / num4
            If num3 > 0L Then
                If flag2 Then
                    stringBuilder.Append(" ")
                End If
                ToThousandEnglish(CInt(num3), stringBuilder)
                stringBuilder.Append(" TRILLION")
                flag2 = True
            End If
            num = num Mod num4
            Dim num5 As Long = 1000000000L
            num3 = num / num5
            If num3 > 0L Then
                If flag2 Then
                    stringBuilder.Append(" ")
                End If
                ToThousandEnglish(CInt(num3), stringBuilder)
                stringBuilder.Append(" BILLION")
                flag2 = True
            End If
            num = num Mod num5
            Dim num6 As Long = 1000000L
            num3 = num / num6
            If num3 > 0L Then
                If flag2 Then
                    stringBuilder.Append(" ")
                End If
                ToThousandEnglish(CInt(num3), stringBuilder)
                stringBuilder.Append(" MILLION")
                flag2 = True
            End If
            num = num Mod num6
            num3 = num / 1000L
            If num3 > 0L Then
                If flag2 Then
                    stringBuilder.Append(" ")
                End If
                ToThousandEnglish(CInt(num3), stringBuilder)
                stringBuilder.Append(" THOUSAND")
                flag2 = True
            End If
            num = num Mod 1000L
            If num > 0L Then
                If flag2 Then
                    stringBuilder.Append(" ")
                End If
                ToThousandEnglish(CInt(num), stringBuilder)
                flag2 = True
            End If
            amount -= Decimal.Truncate(amount)
            amount *= 100D
            num3 = CLng(amount)
            If num3 > 0L Then
                If flag2 Then
                    stringBuilder.Append(" AND CENTS ")
                    ToThousandEnglish(CInt(num3), stringBuilder)
                Else
                    ToThousandEnglish(CInt(num3), stringBuilder)
                    stringBuilder.Append(" CENTS")
                End If
                flag2 = True
            End If
            If Not flag2 Then
                stringBuilder.Append("ZERO")
            End If
            stringBuilder.Append(" ONLY")
            If flag Then
                stringBuilder.Append(")")
            End If
            Return stringBuilder.ToString()
        End Function

        Private Shared Sub ToThousandMalay(amount As Integer, sb As System.Text.StringBuilder)
            Dim array As String() = New String() {"SATU", "DUA", "TIGA", "EMPAT", "LIMA", "ENAM", "TUJUH", "LAPAN", "SEMBILAN", "SEPULUH", "SEBELAS", "DUA BELAS", "TIGA BELAS", "EMPAT BELAS", "LIMA BELAS", "ENAM BELAS", "TUJUH BELAS", "LAPAN BELAS", "SEMBILAN BELAS"}
            Dim array2 As String() = New String() {"DUA PULUH", "TIGA PULUH", "EMPAT PULUH", "LIMA PULUH", "ENAM PULUH", "TUJUH PULUH", "LAPAN PULUH", "SEMBILAN PULUH"}
            amount = amount Mod 1000
            Dim num As Integer = amount / 100
            If num > 0 Then
                sb.Append(array(num - 1))
                sb.Append(" RATUS")
            End If
            amount = amount Mod 100
            Dim num2 As Integer = amount / 10
            If num2 >= 2 Then
                If num > 0 Then
                    sb.Append(" ")
                End If
                sb.Append(array2(num2 - 2))
                Dim num3 As Integer = amount Mod 10
                If num3 > 0 Then
                    sb.Append(" ")
                    sb.Append(array(num3 - 1))
                    Return
                End If
            Else
                Dim num4 As Integer = amount Mod 20
                If num4 > 0 Then
                    If num > 0 Then
                        sb.Append(" ")
                    End If
                    sb.Append(array(num4 - 1))
                End If
            End If
        End Sub

        Public Shared Function ToBahasa(amount As Decimal) As String
            amount = Decimal.Round(amount, 2)
            Dim flag As Boolean
            If amount < 0D Then
                amount = -amount
                flag = True
            Else
                flag = False
            End If
            Dim stringBuilder As System.Text.StringBuilder = New System.Text.StringBuilder(256)
            If flag Then
                stringBuilder.Append("(")
            End If
            Dim flag2 As Boolean = False
            Dim num As Long = CLng(amount)
            Dim num2 As Long = 1000000000000000L
            Dim num3 As Long = num / num2
            If num3 > 0L Then
                ToThousandMalay(CInt(num3), stringBuilder)
                stringBuilder.Append(" QUADRILION")
                flag2 = True
            End If
            num = num Mod num2
            Dim num4 As Long = 1000000000000L
            num3 = num / num4
            If num3 > 0L Then
                If flag2 Then
                    stringBuilder.Append(" ")
                End If
                ToThousandMalay(CInt(num3), stringBuilder)
                stringBuilder.Append(" TRILION")
                flag2 = True
            End If
            num = num Mod num4
            Dim num5 As Long = 1000000000L
            num3 = num / num5
            If num3 > 0L Then
                If flag2 Then
                    stringBuilder.Append(" ")
                End If
                ToThousandMalay(CInt(num3), stringBuilder)
                stringBuilder.Append(" BILION")
                flag2 = True
            End If
            num = num Mod num5
            Dim num6 As Long = 1000000L
            num3 = num / num6
            If num3 > 0L Then
                If flag2 Then
                    stringBuilder.Append(" ")
                End If
                ToThousandMalay(CInt(num3), stringBuilder)
                stringBuilder.Append(" JUTA")
                flag2 = True
            End If
            num = num Mod num6
            num3 = num / 1000L
            If num3 > 0L Then
                If flag2 Then
                    stringBuilder.Append(" ")
                End If
                ToThousandMalay(CInt(num3), stringBuilder)
                stringBuilder.Append(" RIBU")
                flag2 = True
            End If
            num = num Mod 1000L
            If num > 0L Then
                If flag2 Then
                    stringBuilder.Append(" ")
                End If
                ToThousandMalay(CInt(num), stringBuilder)
                flag2 = True
            End If
            amount -= Decimal.Truncate(amount)
            amount *= 100D
            num3 = CLng(amount)
            If num3 > 0L Then
                If flag2 Then
                    stringBuilder.Append(" DAN SEN ")
                    ToThousandMalay(CInt(num3), stringBuilder)
                Else
                    ToThousandMalay(CInt(num3), stringBuilder)
                    stringBuilder.Append(" SEN")
                End If
                flag2 = True
            End If
            If Not flag2 Then
                stringBuilder.Append("KOSONG")
            End If
            stringBuilder.Append(" SAHAJA")
            If flag Then
                stringBuilder.Append(")")
            End If
            Return stringBuilder.ToString()
        End Function

        Private Shared Sub ToThousandIndonesian(amount As Integer, sb As System.Text.StringBuilder)
            Dim array As String() = New String() {"SATU", "DUA", "TIGA", "EMPAT", "LIMA", "ENAM", "TUJUH", "DELAPAN", "SEMBILAN", "SEPULUH", "SEBELAS", "DUA BELAS", "TIGA BELAS", "EMPAT BELAS", "LIMA BELAS", "ENAM BELAS", "TUJUH BELAS", "DELAPAN BELAS", "SEMBILAN BELAS"}
            Dim array2 As String() = New String() {"DUA PULUH", "TIGA PULUH", "EMPAT PULUH", "LIMA PULUH", "ENAM PULUH", "TUJUH PULUH", "DELAPAN PULUH", "SEMBILAN PULUH"}
            amount = amount Mod 1000
            Dim num As Integer = amount / 100
            If num > 0 Then
                If num = 1 Then
                    sb.Append("SERATUS")
                Else
                    sb.Append(array(num - 1))
                    sb.Append(" RATUS")
                End If
            End If
            amount = amount Mod 100
            Dim num2 As Integer = amount / 10
            If num2 >= 2 Then
                If num > 0 Then
                    sb.Append(" ")
                End If
                sb.Append(array2(num2 - 2))
                Dim num3 As Integer = amount Mod 10
                If num3 > 0 Then
                    sb.Append(" ")
                    sb.Append(array(num3 - 1))
                    Return
                End If
            Else
                Dim num4 As Integer = amount Mod 20
                If num4 > 0 Then
                    If num > 0 Then
                        sb.Append(" ")
                    End If
                    sb.Append(array(num4 - 1))
                End If
            End If
        End Sub

        Public Shared Function ToIndonesian(amount As Decimal, currencyWord As String) As String
            amount = Decimal.Round(amount, 2)
            Dim flag As Boolean
            If amount < 0D Then
                amount = -amount
                flag = True
            Else
                flag = False
            End If
            Dim stringBuilder As System.Text.StringBuilder = New System.Text.StringBuilder(256)
            If flag Then
                stringBuilder.Append("(")
            End If
            Dim flag2 As Boolean = False
            Dim num As Long = CLng(amount)
            Dim num2 As Long = 1000000000000000L
            Dim num3 As Long = num / num2
            If num3 > 0L Then
                ToThousandIndonesian(CInt(num3), stringBuilder)
                stringBuilder.Append(" QUADRILION")
                flag2 = True
            End If
            num = num Mod num2
            Dim num4 As Long = 1000000000000L
            num3 = num / num4
            If num3 > 0L Then
                If flag2 Then
                    stringBuilder.Append(" ")
                End If
                ToThousandIndonesian(CInt(num3), stringBuilder)
                stringBuilder.Append(" TRILION")
                flag2 = True
            End If
            num = num Mod num4
            Dim num5 As Long = 1000000000L
            num3 = num / num5
            If num3 > 0L Then
                If flag2 Then
                    stringBuilder.Append(" ")
                End If
                ToThousandIndonesian(CInt(num3), stringBuilder)
                stringBuilder.Append(" MILYAR")
                flag2 = True
            End If
            num = num Mod num5
            Dim num6 As Long = 1000000L
            num3 = num / num6
            If num3 > 0L Then
                If flag2 Then
                    stringBuilder.Append(" ")
                End If
                ToThousandIndonesian(CInt(num3), stringBuilder)
                stringBuilder.Append(" JUTA")
                flag2 = True
            End If
            num = num Mod num6
            num3 = num / 1000L
            If num3 > 0L Then
                If flag2 Then
                    stringBuilder.Append(" ")
                End If
                If num3 = 1L Then
                    stringBuilder.Append("SERIBU")
                Else
                    ToThousandIndonesian(CInt(num3), stringBuilder)
                    stringBuilder.Append(" RIBU")
                End If
                flag2 = True
            End If
            num = num Mod 1000L
            If num > 0L Then
                If flag2 Then
                    stringBuilder.Append(" ")
                End If
                ToThousandIndonesian(CInt(num), stringBuilder)
                flag2 = True
            End If
            If flag2 Then
                stringBuilder.Append(" ")
                If currencyWord.Length > 0 Then
                    stringBuilder.Append(currencyWord)
                Else
                    stringBuilder.Append("KOMA")
                End If
            End If
            amount -= Decimal.Truncate(amount)
            amount *= 100D
            num3 = CLng(amount)
            If num3 > 0L Then
                If flag2 Then
                    stringBuilder.Append(" ")
                    ToThousandIndonesian(CInt(num3), stringBuilder)
                    stringBuilder.Append(" SEN")
                Else
                    ToThousandIndonesian(CInt(num3), stringBuilder)
                    stringBuilder.Append(" ")
                    stringBuilder.Append(currencyWord)
                    stringBuilder.Append(" SEN")
                End If
                flag2 = True
            End If
            If Not flag2 Then
                stringBuilder.Append("NOL")
            End If
            If flag Then
                stringBuilder.Append(")")
            End If
            Return stringBuilder.ToString()
        End Function

        Private Shared Sub ToChineseWan(amount As Integer, isSimplifiedChinese As Boolean, sb As System.Text.StringBuilder)
            Dim array As String() = New String() {ChrW(22777), ChrW(36144), ChrW(21441), ChrW(32902), ChrW(20237), ChrW(38470), ChrW(26578), ChrW(25420), ChrW(29590), ChrW(25342)}
            Dim array2 As String() = New String() {ChrW(22777), ChrW(36019), ChrW(21444), ChrW(32902), ChrW(20237), ChrW(38520), ChrW(26578), ChrW(25420), ChrW(29590), ChrW(25342)}
            Dim array3 As String()
            If isSimplifiedChinese Then
                array3 = array
            Else
                array3 = array2
            End If
            Dim flag As Boolean = False
            amount = amount Mod 10000
            Dim num As Integer = amount / 1000
            If num > 0 Then
                sb.Append(array3(num - 1))
                sb.Append(ChrW(20191))
                flag = True
            End If
            amount = amount Mod 1000
            Dim num2 As Integer = amount / 100
            If num2 > 0 Then
                sb.Append(array3(num2 - 1))
                sb.Append(ChrW(20336))
                flag = True
            End If
            amount = amount Mod 100
            Dim num3 As Integer = amount / 10
            If num3 > 0 Then
                If num3 > 1 OrElse flag Then
                    sb.Append(array3(num3 - 1))
                End If
                sb.Append(ChrW(25342))
            End If
            amount = amount Mod 10
            If amount > 0 Then
                sb.Append(array3(amount - 1))
            End If
        End Sub

        Public Shared Function ToChinese(amount As Decimal, isSimplifiedChinese As Boolean) As String
            amount = Decimal.Round(amount, 2)
            Dim flag As Boolean
            If amount < 0D Then
                amount = -amount
                flag = True
            Else
                flag = False
            End If
            Dim stringBuilder As System.Text.StringBuilder = New System.Text.StringBuilder(128)
            Dim value As String
            Dim value2 As String
            If isSimplifiedChinese Then
                value = ChrW(20159)
                value2 = ChrW(19975)
            Else
                value = ChrW(20740)
                value2 = ChrW(33836)
            End If
            If flag Then
                stringBuilder.Append("(")
            End If
            Dim flag2 As Boolean = False
            Dim num As Long = CLng(amount)
            Dim num2 As Long = 1000000000000L
            Dim num3 As Long = num / num2
            If num3 > 0L Then
                ToChineseWan(CInt(num3), isSimplifiedChinese, stringBuilder)
                stringBuilder.Append(ChrW(20806))
                flag2 = True
            End If
            num = num Mod num2
            Dim num4 As Long = 100000000L
            num3 = num / num4
            If num3 > 0L Then
                ToChineseWan(CInt(num3), isSimplifiedChinese, stringBuilder)
                stringBuilder.Append(value)
                flag2 = True
            End If
            num = num Mod num4
            num3 = num / 10000L
            If num3 > 0L Then
                ToChineseWan(CInt(num3), isSimplifiedChinese, stringBuilder)
                stringBuilder.Append(value2)
                flag2 = True
            End If
            num = num Mod 10000L
            If num > 0L Then
                ToChineseWan(CInt(num), isSimplifiedChinese, stringBuilder)
                flag2 = True
            End If
            If flag2 Then
                stringBuilder.Append(ChrW(20803))
            End If
            amount -= Decimal.Truncate(amount)
            amount *= 100D
            num3 = CLng(amount)
            If num3 > 0L Then
                Dim num5 As Long = num3 / 10L
                If num5 > 0L Then
                    ToChineseWan(CInt(num5), isSimplifiedChinese, stringBuilder)
                    stringBuilder.Append(ChrW(35282))
                End If
                num5 = num3 Mod 10L
                If num5 > 0L Then
                    ToChineseWan(CInt(num5), isSimplifiedChinese, stringBuilder)
                    stringBuilder.Append(ChrW(20998))
                End If
                flag2 = True
            End If
            If Not flag2 Then
                stringBuilder.Append(ChrW(38646) & ChrW(20803))
            End If
            stringBuilder.Append(ChrW(27491))
            If flag Then
                stringBuilder.Append(")")
            End If
            Return stringBuilder.ToString()
        End Function

        Private Shared Function ToThousandVietnamese(amount As Integer, useLeu As Boolean) As String
            Dim array As String() = New String() {String.Format("m{0}t", ChrW(7897)), "hai", "ba", String.Format("b{0}n", ChrW(7889)), String.Format("n{0}m", ChrW(259)), "sáu", String.Format("b{0}y", ChrW(7843)), "tám", "chín", String.Format("m{0}{1}i", ChrW(432), ChrW(7901))}
            Dim stringBuilder As System.Text.StringBuilder = New System.Text.StringBuilder(64)
            amount = amount Mod 1000
            Dim num As Integer = amount / 100
            If num > 0 Then
                stringBuilder.Append(array(num - 1))
                stringBuilder.AppendFormat(" tr{0}m", ChrW(259))
            End If
            amount = amount Mod 100
            Dim num2 As Integer = amount / 10
            If num2 >= 2 Then
                stringBuilder.Append(" ")
                stringBuilder.Append(array(num2 - 1))
                stringBuilder.AppendFormat(" m{0}{1}i", ChrW(432), ChrW(7901))
            Else
                If num2 = 1 Then
                    stringBuilder.Append(" ")
                    stringBuilder.Append(array(9))
                Else
                    If num2 = 0 AndAlso useLeu Then
                        stringBuilder.Append(" leû")
                    End If
                End If
            End If
            Dim num3 As Integer = amount Mod 10
            If num3 > 0 Then
                stringBuilder.Append(" ")
                If num3 = 5 AndAlso stringBuilder.Length > 1 Then
                    stringBuilder.AppendFormat("l{0}m", ChrW(259))
                Else
                    stringBuilder.Append(array(num3 - 1))
                End If
            End If
            Return stringBuilder.ToString().Trim()
        End Function

        Public Shared Function ToVietnamese(amount As Decimal) As String
            amount = Decimal.Truncate(amount)
            Dim flag As Boolean
            If amount < 0D Then
                amount = -amount
                flag = True
            Else
                flag = False
            End If
            Dim text As String = ""
            Dim num As Long = CLng(amount)
            Dim num2 As Long = 1000000000000000L
            Dim num3 As Long = num / num2
            If num3 > 0L Then
                text = String.Format("{0}{1} quadrillion", text, ToThousandVietnamese(CInt(num3), False))
            End If
            num = num Mod num2
            Dim num4 As Long = 1000000000000L
            num3 = num / num4
            If num3 > 0L Then
                If text.Length > 0 Then
                    text += " "
                End If
                text = String.Format("{0}{1} nghìn t{2}", text, ToThousandVietnamese(CInt(num3), False), ChrW(7927))
            End If
            num = num Mod num4
            Dim num5 As Long = 1000000000L
            num3 = num / num5
            If num3 > 0L Then
                If text.Length > 0 Then
                    text += " "
                End If
                text = String.Format("{0}{1} t{2}", text, ToThousandVietnamese(CInt(num3), False), ChrW(7927))
            End If
            num = num Mod num5
            Dim num6 As Long = 1000000L
            num3 = num / num6
            If num3 > 0L Then
                If text.Length > 0 Then
                    text += " "
                End If
                text = String.Format("{0}{1} tri{2}u", text, ToThousandVietnamese(CInt(num3), False), ChrW(7879))
            End If
            num = num Mod num6
            num3 = num / 1000L
            If num3 > 0L Then
                If text.Length > 0 Then
                    text += " "
                End If
                text = String.Format("{0}{1} ngàn", text, ToThousandVietnamese(CInt(num3), False))
            End If
            num = num Mod 1000L
            If num > 0L Then
                If amount < 10D Then
                    text = String.Format("{0} {1}", text, ToThousandVietnamese(CInt(num), False))
                Else
                    text = String.Format("{0} {1}", text, ToThousandVietnamese(CInt(num), True))
                End If
            End If
            If text.Length = 0 Then
                text = String.Format("s{0} không", ChrW(7889))
            End If
            text = text.Trim()
            text = Char.ToUpper(text(0), New System.Globalization.CultureInfo("vi")) + text.Substring(1)
            If flag Then
                Return String.Format("({0} {1}{2}ng ch{3}n)", text.Trim(), ChrW(273), ChrW(7891), ChrW(7859))
            End If
            Return String.Format("{0} {1}{2}ng ch{3}n", text.Trim(), ChrW(273), ChrW(7891), ChrW(7859))
        End Function

        ''' <summary>
        ''' Convert enum member in string to enum member.
        ''' Returns nothing if string cant be found.
        ''' </summary>
        ''' <param name="enumType"></param>
        ''' <param name="value"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function StringToEnum(enumType As Type, value As String) As Object
            Return Misc.EnumHelper.StringToEnum(enumType, value)
        End Function

        ''' <summary>
        ''' Convert enum member in string to enum member.
        ''' Returns nothing if string cant be found.
        ''' </summary>
        Public Shared Function StringToEnum(Of T)(value As String) As T
            Return DirectCast(StringToEnum(GetType(T), value), T)
        End Function

        ''' <summary>
        ''' Copy datarowview values (with columns same as provided structure properties) to the class properties.
        ''' </summary>
        ''' <param name="drv"></param>
        ''' <param name="type"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function DrvToStructure(drv As DataRowView, type As Type) As Object
            If drv Is Nothing Then Return Nothing
            Dim rt As Object = Activator.CreateInstance(type)
            For Each p As System.Reflection.PropertyInfo In rt.GetType().GetProperties()
                If p.CanWrite And drv.Row.Table.Columns.Contains(p.Name) Then
                    Try
                        Select Case p.PropertyType.FullName
                            Case GetType(Byte()).FullName
                                If Not IsDBNull(drv(p.Name)) Then
                                    Dim val() As Byte = drv(p.Name)
                                    p.SetValue(rt, val, Nothing)
                                End If
                            Case Else
                                Select Case drv(p.Name).GetType.FullName
                                    Case GetType(Boolean).FullName
                                        p.SetValue(rt, ToBoolean(drv(p.Name).ToString()), Nothing)

                                    Case GetType(DateTime).FullName, GetType(Date).FullName
                                        'Dim val As Boolean = IIf(String.IsNullOrEmpty(drv(p.Name)), False, drv(p.Name))
                                        p.SetValue(rt, ToDateTime(drv(p.Name).ToString()), Nothing)

                                    Case Else
                                        Try
                                            Dim o As Object = drv(p.Name)
                                            If Not IsDBNull(o) Then
                                                p.SetValue(rt, Ape.EL.Utility.TypeHelper.ChangeType(o, p.PropertyType), Nothing)
                                            Else
                                                p.SetValue(rt, Nothing, Nothing)
                                            End If
                                        Catch ex As Exception
                                            Debugger.Break()
                                        End Try
                                End Select

                        End Select
                    Catch ex As Exception
                        'ignore 
                        'Console.WriteLine("Error: " & ex.Message & "--- value: " & drv(p.Name))
                    End Try
                End If
            Next

            Return rt
        End Function

        ''' <summary>
        ''' Copy datarowview values (with columns same as provided structure properties) to the class properties.
        ''' </summary>
        Public Shared Function DrvToStructure(Of T)(drv As DataRowView) As T
            Return DirectCast(DrvToStructure(drv, GetType(T)), T)
        End Function

        ''' <summary>
        ''' Convert whole data of dataview with specific structure into list of structures.
        ''' </summary>
        Public Shared Function DvToStructureList(Of T)(dv As DataView) As List(Of T)
            If dv IsNot Nothing Then
                Try
                    Dim lis As New List(Of T)
                    For Each drv As DataRowView In dv
                        lis.Add(DrvToStructure(Of T)(drv))
                    Next
                    Return lis
                Catch ex As Exception
                End Try
            End If
            Return Nothing
        End Function

        ''' <summary>
        ''' Add a structure into dataview. st's property must exist in dv's column.
        ''' </summary>
        Public Shared Sub AddStructureToDv(Of T)(stl As List(Of T), ByRef dv As DataView)
            If dv Is Nothing Or stl Is Nothing Then Return

            Dim pi() As System.Reflection.PropertyInfo = GetType(T).GetProperties()
            For Each st As T In stl
                Dim drv As DataRowView = dv.AddNew
                drv.BeginEdit()
                For Each p As System.Reflection.PropertyInfo In pi
                    If p.CanRead And dv.Table.Columns.Contains(p.Name) Then
                        Try
                            If Not IsDBNull(p.GetValue(st, Nothing)) Then
                                drv(p.Name) = p.GetValue(st, Nothing)
                            Else
                                drv(p.Name) = Nothing
                            End If
                        Catch ex As Exception
                            'ignore 
                            'Console.WriteLine("Error: " & ex.Message & "--- value: " & drv(p.Name))
                        End Try
                    End If
                Next
                drv.EndEdit()
            Next
        End Sub

        ''' <summary>
        ''' Add a structure into dataview. st's property must exist in dv's column.
        ''' </summary>
        Public Shared Sub AddStructureToDv(Of T)(st As T, ByRef dv As DataView)
            Dim stl As New List(Of T)
            stl.Add(st)
            AddStructureToDv(Of T)(stl, dv)
        End Sub

        ''' <summary>
        ''' Build a DataView with structure from class' properties.
        ''' </summary>
        Public Shared Function CreateDvFromStructure(Of T)() As DataView
            Dim dt As New DataTable
            Dim pi() As System.Reflection.PropertyInfo = GetType(T).GetProperties()
            For Each p As System.Reflection.PropertyInfo In pi
                If p.CanRead Then
                    dt.Columns.Add(p.Name, p.PropertyType)
                End If
            Next

            Return Misc.DataHelper.ConvertToDataView(dt)
        End Function

        ''' <summary>
        ''' Get empty string when obj is nothing. This is to avoid null reference exception.
        ''' </summary>
        Public Shared Function GetStr(ByVal obj As Object) As String
            If obj Is Nothing Then
                Return String.Empty
            ElseIf obj Is System.DBNull.Value Then
                Return String.Empty
            End If
            Return obj.ToString
        End Function

        ''' <summary>
        ''' Same as GetStr.
        ''' Get empty string when obj is nothing. This is to avoid null reference exception.
        ''' </summary>
        Public Shared Function ToStr(ByVal obj As Object) As String
            Return GetStr(obj)
        End Function

        <Obsolete("Dont use, use Crypt.ByteArrayToObject instead.", True)>
        Public Shared Function BinaryToImage(ByVal Binary As Byte()) As System.Drawing.Image
            If Binary IsNot Nothing Then
                Dim stmBLOBData As New System.IO.MemoryStream(Binary)
                Return System.Drawing.Image.FromStream(stmBLOBData)
            Else
                Return Nothing
            End If
        End Function

        <Obsolete("Dont use, use Crypt.ObjectToByteArray instead.", True)>
        Public Shared Function ImageToBinary(ByVal Img As Drawing.Image) As Byte()
            Return Ape.EL.Utility.Data.Crypt.ObjectToByteArray(Img)
        End Function

        Public Shared Function BitmapToImage(ByVal bmp As Drawing.Bitmap) As System.Drawing.Image
            If bmp IsNot Nothing Then
                Return DirectCast(bmp, System.Drawing.Image)
            Else
                Return Nothing
            End If
        End Function

        Public Shared Function ImageToBitmap(ByVal img As Drawing.Image) As System.Drawing.Bitmap
            If img IsNot Nothing Then
                Return New System.Drawing.Bitmap(img)
            Else
                Return Nothing
            End If
        End Function

        ''' <summary>
        ''' Convert Byte array to MemoryStream.
        ''' </summary>
        Public Shared Function ToStream(buffer As Byte()) As MemoryStream
            If buffer Is Nothing Then
                Throw New ArgumentNullException("buffer")
            End If
            Dim myStream As New MemoryStream
            myStream.Write(buffer, 0, buffer.Length)
            Return myStream
        End Function

        ''' <summary>
        ''' Convert string with encoding to MemoryStream.
        ''' </summary>
        Public Shared Function ToStream(str As String, ByVal enc As Encoding) As MemoryStream
            If str Is Nothing Then
                Throw New ArgumentNullException("str")
            ElseIf enc Is Nothing Then
                Throw New ArgumentNullException("enc")
            End If
            Return ToStream(enc.GetBytes(str))
        End Function
    End Class

End Namespace
