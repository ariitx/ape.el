﻿Namespace Data
    Public Class SqlVersionInfo
        Public Sub New(ByVal oGeneral As DbGeneral)
            Try
                Dim csb As SqlClient.SqlConnectionStringBuilder = Ape.EL.Data.DbUtility.GetCsb(oGeneral.ConnectionString)
                Me.myServerName = csb.DataSource
                Me.myDatabaseName = csb.InitialCatalog
                Me.myConnectionString = csb
                Me.myFullVersion = oGeneral.GetSQLServerFullVersion()
                Me.myProductVersion = oGeneral.GetSQLServerProductVersion()
                Me.myProductLevel = oGeneral.GetSQLServerProductLevel()
                Me.myEdition = oGeneral.GetSQLServerEdition()
            Catch ex As Exception
            End Try

            If Me.myProductVersion.Length = 0 Then
                If String.Compare(Me.myFullVersion, 0, "Microsoft SQL Server ", 0, 21, True) = 0 Then
                    Dim text As String = Me.myFullVersion.Substring(21)
                    Dim i As Integer
                    i = 0
                    While i < text.Length
                        If text(i) <> " " Then
                            Exit While
                        End If
                        i += 1
                    End While
                    While i < text.Length
                        If text(i) = "-" Then
                            Exit While
                        End If
                        i += 1
                    End While
                    While i < text.Length AndAlso (text(i) < "0" OrElse text(i) > "9")
                        i += 1
                    End While
                    For j As Integer = 0 To 4 - 1
                        Dim text2 As String = ""
                        While i < text.Length AndAlso text(i) >= "0"
                            If text(i) > "9" Then
                                Exit While
                            End If
                            Dim arg_132_0 As Object = text2
                            Dim arg_128_0 As String = text
                            Dim expr_124 As Integer = i
                            i = expr_124 + 1
                            text2 = arg_132_0 + arg_128_0(expr_124)
                        End While
                        Try
                            Me.myVersionNumber(j) = Ape.EL.Data.Convert.ToInt32(text2)
                        Catch ex_169 As Exception
                        End Try
                        i += 1
                    Next
                Else
                    Me.myMainVersion = "Unknown"
                End If
            Else
                Dim array As String() = Me.myProductVersion.Split(New Char() {"."})
                For k As Integer = 0 To 4 - 1
                    Me.myVersionNumber(k) = 0
                    If k < array.Length Then
                        Integer.TryParse(array(k), Me.myVersionNumber(k))
                    End If
                Next
            End If
            If Me.myVersionNumber(0) = 6 Then
                If Me.myVersionNumber(1) = 5 Then
                    Me.myMainVersion = "6.5"
                    If Me.myVersionNumber(2) >= 479 Then
                        Me.myProductLevel = "SP5a Update"
                    Else
                        If Me.myVersionNumber(2) >= 416 Then
                            Me.myProductLevel = "SP5a"
                        Else
                            If Me.myVersionNumber(2) >= 415 Then
                                Me.myProductLevel = "SP5"
                            Else
                                If Me.myVersionNumber(2) >= 281 Then
                                    Me.myProductLevel = "SP4"
                                Else
                                    If Me.myVersionNumber(2) >= 258 Then
                                        Me.myProductLevel = "SP3"
                                    Else
                                        If Me.myVersionNumber(2) >= 240 Then
                                            Me.myProductLevel = "SP2"
                                        Else
                                            If Me.myVersionNumber(2) >= 213 Then
                                                Me.myProductLevel = "SP1"
                                            Else
                                                Me.myProductLevel = "RTM"
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End If
                Else
                    Me.myMainVersion = "6.0"
                    If Me.myVersionNumber(2) >= 151 Then
                        Me.myProductLevel = "SP3"
                    Else
                        If Me.myVersionNumber(2) >= 139 Then
                            Me.myProductLevel = "SP2"
                        Else
                            If Me.myVersionNumber(2) >= 124 Then
                                Me.myProductLevel = "SP1"
                            Else
                                Me.myProductLevel = "RTM"
                            End If
                        End If
                    End If
                End If
            Else
                If Me.myVersionNumber(0) = 7 Then
                    Me.myMainVersion = "7.0"
                    If Me.myVersionNumber(2) >= 1063 Then
                        Me.myProductLevel = "SP4"
                    Else
                        If Me.myVersionNumber(2) >= 961 Then
                            Me.myProductLevel = "SP3"
                        Else
                            If Me.myVersionNumber(2) >= 842 Then
                                Me.myProductLevel = "SP2"
                            Else
                                If Me.myVersionNumber(2) >= 699 Then
                                    Me.myProductLevel = "SP1"
                                Else
                                    Me.myProductLevel = "RTM"
                                End If
                            End If
                        End If
                    End If
                Else
                    If Me.myVersionNumber(0) = 8 Then
                        Me.myMainVersion = "2000"
                    Else
                        If Me.myVersionNumber(0) = 9 Then
                            Me.myMainVersion = "2005"
                        Else
                            If Me.myVersionNumber(0) = 10 Then
                                If Me.myVersionNumber(1) = 50 Then
                                    Me.myMainVersion = "2008 R2"
                                Else
                                    Me.myMainVersion = "2008"
                                End If
                            Else
                                If Me.myVersionNumber(0) = 11 Then
                                    Me.myMainVersion = "2012"
                                Else
                                    If Me.myVersionNumber(0) = 12 Then
                                        Me.myMainVersion = "2014"
                                    Else
                                        If Me.myVersionNumber(0) > 12 Then
                                            Me.myMainVersion = "Beyond 2014"
                                        Else
                                            Me.myMainVersion = "Unknown Version"
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            End If
            If Me.myProductLevel.Length > 0 Then
                Me.myVersionWithEditionAndServicePack = String.Format("{0} {1} {2}", Me.myMainVersion, Me.myProductLevel, Me.myEdition)
                Me.myVersionWithServicePack = String.Format("{0} {1}", Me.myMainVersion, Me.myProductLevel)
                Return
            End If
            Me.myVersionWithEditionAndServicePack = String.Format("{0} {1}", Me.myMainVersion, Me.myEdition)
            Me.myVersionWithServicePack = String.Format("{0}", Me.myMainVersion)

        End Sub

        ReadOnly Property ServerName As String
            Get
                Return myServerName
            End Get
        End Property
        Private myServerName As String = ""

        ReadOnly Property DatabaseName As String
            Get
                Return myDatabaseName
            End Get
        End Property
        Private myDatabaseName As String = ""

        ReadOnly Property ConnectionString As SqlClient.SqlConnectionStringBuilder
            Get
                Return myConnectionString
            End Get
        End Property
        Private myConnectionString As SqlClient.SqlConnectionStringBuilder

        ReadOnly Property FullVersion() As String
            Get
                Return Me.myFullVersion
            End Get
        End Property
        Private myFullVersion As String = ""

        ReadOnly Property ProductVersion() As String
            Get
                Return Me.myProductVersion
            End Get
        End Property
        Private myProductVersion As String = ""

        ReadOnly Property ProductLevel() As String
            Get
                Return Me.myProductLevel
            End Get
        End Property
        Private myProductLevel As String = ""

        ReadOnly Property MainVersion() As String
            Get
                Return Me.myMainVersion
            End Get
        End Property
        Private myMainVersion As String = ""

        ReadOnly Property VersionNumber() As Integer()
            Get
                Return Me.myVersionNumber
            End Get
        End Property
        Private myVersionNumber As Integer() = New Integer(4 - 1) {}

        ReadOnly Property Edition() As String
            Get
                Return Me.myEdition
            End Get
        End Property
        Private myEdition As String = ""

        ReadOnly Property VersionWithServicePack() As String
            Get
                Return Me.myVersionWithServicePack
            End Get
        End Property
        Private myVersionWithServicePack As String

        ReadOnly Property VersionWithEditionAndServicePack() As String
            Get
                Return Me.myVersionWithEditionAndServicePack
            End Get
        End Property
        Private myVersionWithEditionAndServicePack As String
    End Class
End Namespace
