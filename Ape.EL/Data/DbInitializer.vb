Imports Ape.EL.WinForm.General
Imports Ape.EL.InternalGlobal

Namespace Data
    ''' <summary>
    ''' Class to initiate SqlConnection.
    ''' </summary>
    ''' <remarks></remarks>
    Public Class DbInitializer
'        Private ReadOnly lic As Boolean = Internal.CheckMyLicense

        Public Property ConnectionString() As String
            Get
                Return _ConnectionString
            End Get
            Set(ByVal value As String)
                _ConnectionString = value
            End Set
        End Property
        Private _ConnectionString As String = ""

        Public Property ConnectionTimeout As Integer
            Get
                Return _ConnectionTimeout
            End Get
            Set(value As Integer)
                _ConnectionTimeout = value
            End Set
        End Property
        Private _ConnectionTimeout As Integer = -1

        Public Sub ConnectSql(ByRef bStatus As Boolean, ByRef oConnSQL As SqlClient.SqlConnection)
            'if connection string is not set, but previous connection was established, try to get the connection string from previous connection
            If String.IsNullOrEmpty(ConnectionString) Then
                If MyConnection IsNot Nothing Then
                    ConnectionString = MyConnection.ConnectionString
                End If
            End If

            'abort initialization if connection string is empty
            If String.IsNullOrEmpty(ConnectionString) Then
                WarningMsg("Please assign ConnectionString property.")
                Exit Sub
            End If

            'Assign timeout into connectionstring
            If Not ConnectionString.ToUpper.Contains("CONNECTION TIMEOUT") And ConnectionTimeout >= 0 Then
                If Not ConnectionString.Trim.EndsWith(";") Then
                    ConnectionString = String.Concat(ConnectionString, ";")
                End If
                ConnectionString = String.Concat(ConnectionString, String.Format("Connection Timeout={0};", ConnectionTimeout))
            End If

            'initialize connection
            Try
                oConnSQL = New SqlClient.SqlConnection
                oConnSQL.ConnectionString = ConnectionString
                oConnSQL.Open()
                bStatus = True
            Catch ex As Exception
                bStatus = False
                ExceptionErrorMsg(ex.Message)
            End Try
        End Sub
    End Class
End Namespace
