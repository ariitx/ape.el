﻿Imports System.Drawing
Imports System.ComponentModel

Namespace Data
    ''' <summary>
    ''' Generally a helper to wrap database's setting list types tables (eg. BusinessSetting, PriceLevelSetting) which have ID, Value and Binary columns.
    ''' </summary>
    ''' <remarks></remarks>
    Public Class DbSetting
#Region "Alternate set methods"
        ''' <summary>
        ''' Get or set a method to be called after Value property is set. You can use this for save value to registry or database.
        ''' </summary>
        Property MySetValue As Action(Of String)

        ''' <summary>
        ''' Get or set a method to be called after Binary property is set. You can use this for save value to registry or database.
        ''' </summary>
        Property MySetBinary As Action(Of Byte())

        ''' <summary>
        ''' Required to invoke MySetValue.
        ''' </summary>
        Private Sub SetValue(sender As String)
            If MySetValue IsNot Nothing Then
                MySetValue.Invoke(sender)
            End If
        End Sub

        ''' <summary>
        ''' Required to invoke MySetBinary.
        ''' </summary>
        Private Sub SetBinary(sender As Byte())
            If MySetBinary IsNot Nothing Then
                MySetBinary.Invoke(sender)
            End If
        End Sub
#End Region

        ''' <summary>
        ''' Get or set main identifier for the DbSetting.
        ''' </summary>
        Public Property ID As String
            Get
                Return _ID
            End Get
            Set(value As String)
                _ID = value
            End Set
        End Property
        Private _ID As String = String.Empty

        ''' <summary>
        ''' Get or set value in string.
        ''' </summary>
        Public Property Value As String
            Get
                If _Value IsNot Nothing Then
                    Return _Value
                Else
                    If DefaultValue IsNot Nothing Then
                        Return DefaultValue
                    Else
                        Return Nothing
                    End If
                End If
            End Get
            Set(value As String)
                _Value = value

                SetValue(_Value)
            End Set
        End Property
        Private _Value As String = Nothing

        ''' <summary>
        ''' Get or set value for binary data in byte array.
        ''' </summary>
        Public Property Binary() As Byte()
            Get
                Return _Binary
            End Get
            Set(value As Byte())
                If IsDBNull(value) Then
                    _Binary = Nothing
                Else
                    _Binary = value
                End If

                SetBinary(_Binary)
            End Set
        End Property
        Private _Binary() As Byte = Nothing

        '~~~~~~ overridable ~~~~~~
        ''' <summary>
        ''' Return Value property.
        ''' </summary>
        Public Overrides Function ToString() As String
            Return Value
        End Function

        '~~~~~~ converted data type values ~~~~~~

        ''' <summary>
        ''' Get or set Value property as boolean, string must be in "True" or "False" or empty.
        ''' </summary>
        Public Property ValBool As Boolean
            Get
                Return Convert.ToBoolean(Me.Value)
            End Get
            Set(value As Boolean)
                Me.Value = value.ToString
            End Set
        End Property

        ''' <summary>
        ''' Get or set Value property as decimal.
        ''' </summary>
        Public Property ValDec As Decimal
            Get
                Return Convert.ToDecimal(Me.Value)
            End Get
            Set(value As Decimal)
                Me.Value = value.ToString
            End Set
        End Property

        ''' <summary>
        ''' Get or set Value property as integer.
        ''' </summary>
        Public Property ValInt As Integer
            Get
                Return Convert.ToInt32(Me.Value)
            End Get
            Set(value As Integer)
                Me.Value = value.ToString
            End Set
        End Property

        ''' <summary>
        ''' Get or set Value property as Long.
        ''' </summary>
        Public Property ValLong As Long
            Get
                Return Convert.ToInt64(Me.Value)
            End Get
            Set(value As Long)
                Me.Value = value.ToString
            End Set
        End Property

        ''' <summary>
        ''' Get or set Value property as datetime.
        ''' </summary>
        Public Property ValDt As DateTime
            Get
                Return Convert.ToDateTime(Me.Value)
            End Get
            Set(value As DateTime)
                Me.Value = value.ToString
            End Set
        End Property

        ''' <summary>
        ''' Get Value property as an enum object type.
        ''' </summary>
        Public ReadOnly Property ValEnum(ByVal T As Type) As Object
            Get
                Try
                    If T.IsEnum Then
                        Return Ape.EL.Data.Convert.StringToEnum(T, Me.Value)
                    End If
                Catch ex As Exception
                    Debug.Assert(False, ex.ToString)
                End Try
                Return Nothing
            End Get
        End Property

        ''' <summary>
        ''' Get or set Binary property as Image.
        ''' </summary>
        Public Property ValImage As Image
            Get
                If Binary IsNot Nothing Then
                    Dim stmBLOBData As New System.IO.MemoryStream(Binary)
                    Return Image.FromStream(stmBLOBData)
                Else
                    Return Nothing
                End If
            End Get
            Set(value As Image)
                Dim converter As New ImageConverter
                Binary = converter.ConvertTo(value, GetType(Byte()))
            End Set
        End Property

        ''' <summary>
        ''' Get or set Binary property as object which directly translate to Binary using ByteArrayToObject or ObjectToByteArray converter.
        ''' </summary>
        Public Property BinObj As Object
            Get
                If Binary IsNot Nothing Then
                    Return Ape.EL.Utility.Data.Crypt.ByteArrayToObject(Binary)
                Else
                    Return Nothing
                End If
            End Get
            Set(value As Object)
                Binary = Ape.EL.Utility.Data.Crypt.ObjectToByteArray(value)
            End Set
        End Property

        ''' <summary>
        ''' Get or set Remark of the selected DbSetting.
        ''' </summary>
        Public Property Remark As String
            Get
                Return _Remark
            End Get
            Set(value As String)
                _Remark = value
            End Set
        End Property
        Private _Remark As String = String.Empty

        ''' <summary>
        ''' Get or set default value of the selected DbSetting.
        ''' </summary>
        Public Property DefaultValue As String
            Get
                Return _DefaultValue
            End Get
            Set(value As String)
                _DefaultValue = value
            End Set
        End Property
        Private _DefaultValue As String = Nothing

        Public Shared Function Create(ByVal ID As String, Optional ByVal Value As String = Nothing, Optional ByVal Binary As Object = Nothing, Optional ByVal Remark As String = "", Optional ByVal DefaultValue As Object = "") As DbSetting
            Dim dbs As New DbSetting
            dbs.ID = ID
            dbs.Value = Value
            dbs.Binary = Binary
            dbs.Remark = Remark
            dbs.DefaultValue = DefaultValue
            Return dbs
        End Function
    End Class
End Namespace
