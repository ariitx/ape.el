﻿Imports System.Data.SqlClient
Imports Ape.EL.WinForm.General
Imports System.Data.Common
Imports System.Reflection

Namespace Data
    Public Class DbUtility
        '        Private ReadOnly lic As Boolean = Internal.CheckMyLicense

        ''' <summary>
        ''' Create ConnectionString using integrated security.
        ''' </summary>
        ''' <param name="strDataSource">A server name or data source.</param>
        ''' <param name="strInitialCatalog">Database name to be accessed.</param>
        ''' <param name="bMultipleActiveResultSets"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetConnectionString(ByVal strDataSource As String, _
                                            ByVal strInitialCatalog As String, _
                                            Optional ByVal bMultipleActiveResultSets As Boolean = True) As String
            Try
                Dim builder As New SqlConnectionStringBuilder
                builder("Data Source") = strDataSource
                builder("Initial Catalog") = strInitialCatalog
                builder("Integrated Security") = True
                builder("MultipleActiveResultSets") = bMultipleActiveResultSets

                Return builder.ConnectionString
            Catch ex As Exception
                Throw New Exception("GetConnectionString (0x1) failure.", ex)
            End Try

            Return ""
        End Function

        ''' <summary>
        ''' Create ConnectionString with username and password.
        ''' </summary>
        ''' <param name="strDataSource">A server name or data source.</param>
        ''' <param name="strInitialCatalog">Database name to be accessed.</param>
        ''' <param name="strUsername">Username to login.</param>
        ''' <param name="strPassword">Password to login.</param>
        ''' <param name="bMultipleActiveResultSets"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetConnectionString(ByVal strDataSource As String, _
                                            ByVal strInitialCatalog As String, _
                                            ByVal strUsername As String, _
                                            ByVal strPassword As String, _
                                            Optional bIntegratedSecurity As Boolean = False, _
                                            Optional ByVal bMultipleActiveResultSets As Boolean = True) As String
            Try
                Dim builder As New SqlConnectionStringBuilder
                builder("Data Source") = strDataSource
                builder("Initial Catalog") = strInitialCatalog
                builder("User ID") = strUsername
                builder("Password") = strPassword
                builder("Integrated Security") = bIntegratedSecurity
                builder("MultipleActiveResultSets") = bMultipleActiveResultSets

                Return builder.ConnectionString
            Catch ex As Exception
                Throw New Exception("GetConnectionString (0x2) failure.", ex)
            End Try

            Return ""
        End Function

        ''' <summary>
        ''' Return a SQLConnectionStringBuilder in a shorter function.
        ''' </summary>
        ''' <param name="strConnString"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetCsb(ByVal strConnString As String) As SqlConnectionStringBuilder
            Return New SqlConnectionStringBuilder(strConnString)
        End Function

        ''' <summary>
        ''' Get an initialized DbGeneral based on provided connection string members.
        ''' </summary>
        ''' <param name="serverName"></param>
        ''' <param name="useWindowsAuthentication"></param>
        ''' <param name="userId"></param>
        ''' <param name="password"></param>
        ''' <param name="initialCatalogue"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Shared Function GetTestGeneral(serverName As String, useWindowsAuthentication As Boolean, userId As String, password As String, Optional ByVal initialCatalogue As String = "master") As DbGeneral
            Dim cnstr As String = String.Empty
            If useWindowsAuthentication Then
                cnstr = GetConnectionString(serverName, initialCatalogue)
            Else
                cnstr = GetConnectionString(serverName, initialCatalogue, userId, password)
            End If

            'Additional connection string parameter
            Dim csb As SqlConnectionStringBuilder = GetCsb(cnstr)
            csb.ConnectTimeout = 5 'Set timeout value, 0 will be forever, dont set it to 0!
            cnstr = csb.ToString

            Dim oGeneral As DbGeneral = Nothing
            Try
                oGeneral = New DbGeneral(cnstr)
            Catch ex As Exception
                ExceptionErrorMsg(ex)
            End Try
            Return oGeneral
        End Function

        ''' <summary>
        ''' Test connection to SQL Server master database.
        ''' </summary>
        ''' <param name="serverName"></param>
        ''' <param name="useWindowsAuthentication"></param>
        ''' <param name="userId"></param>
        ''' <param name="password"></param>
        ''' <param name="initialCatalogue"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function TestConnection(serverName As String, useWindowsAuthentication As Boolean, userId As String, password As String, Optional ByVal initialCatalogue As String = "master") As Boolean
            Try
                Dim oGeneral As DbGeneral = GetTestGeneral(serverName, useWindowsAuthentication, userId, password, initialCatalogue)

                If oGeneral.ConnectionSQL.State = ConnectionState.Open Then
                    oGeneral.Dispose()
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception
            End Try

            Return False
        End Function

        ''' <summary>
        ''' Test connection to SQL Server master database.
        ''' </summary>
        Public Shared Function TestConnection(csb As SqlConnectionStringBuilder) As Boolean
            Return TestConnection(csb.DataSource, csb.IntegratedSecurity, csb.UserID, csb.Password, csb.InitialCatalog)
        End Function

        ''' <summary>
        ''' Create SqlConnection from a connection string.
        ''' </summary>
        Public Shared Sub CreateConnection(ByRef bConnected As String, ByRef nConn As SqlClient.SqlConnection, ByVal cs As String)
            Try
                Dim oConnectToDB As New DbInitializer
                oConnectToDB.ConnectionString = cs
                oConnectToDB.ConnectSql(bConnected, nConn)

                oConnectToDB = Nothing
            Catch ex As Exception
            End Try
        End Sub

        ''' <summary>
        ''' Create SqlConnection from a connection string.
        ''' </summary>
        Public Shared Sub CreateConnection(ByRef bConnected As String, ByRef nConn As SqlClient.SqlConnection, ByVal csb As SqlConnectionStringBuilder)
            CreateConnection(bConnected, nConn, csb.ConnectionString)
        End Sub

        ''' <summary>
        ''' Get list of user database in targeted SQL Server. Not compatible with SQL 2000.
        ''' </summary>
        ''' <param name="serverName"></param>
        ''' <param name="useWindowsAuthentication"></param>
        ''' <param name="userId"></param>
        ''' <param name="password"></param>
        ''' <param name="initialCatalogue"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetUserDatabaseList(serverName As String, useWindowsAuthentication As Boolean, userId As String, password As String, Optional ByVal initialCatalogue As String = "master") As List(Of String)
            Dim oGeneral As DbGeneral = Nothing
            Dim result As List(Of String)
            Try
                oGeneral = GetTestGeneral(serverName, useWindowsAuthentication, userId, password, initialCatalogue)
                result = GetUserDatabaseList(oGeneral.ConnectionSQL)
            Finally
                oGeneral.Dispose()
            End Try

            Return result
        End Function

        ''' <summary>
        ''' Get list of user database in targeted SQL Server. Not compatible with SQL 2000.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetUserDatabaseList(conn As SqlConnection, Optional onlineOnly As Boolean = False) As List(Of String)
            Dim result As New List(Of String)
            Try
                Dim sql = "SELECT Name FROM sys.databases WHERE name NOT IN ('master', 'model', 'msdb', 'tempdb')"
                If onlineOnly Then
                    sql += " AND State_Desc = 'ONLINE'"
                End If
                Using r As SqlDataReader = conn.GetRecords(sql)
                    While r.Read
                        result.Add(r("Name").ToString())
                    End While
                End Using
            Catch ex As Exception
                ExceptionErrorMsg(ex)
                result = Nothing
            End Try

            If result.Count = 0 Then
                Return Nothing
            Else
                Return result
            End If
        End Function

        ''' <summary>
        ''' Get associated objects that are still using the connection, preventing the connection to create any new transaction.
        ''' Normally forgotten to close datareaders are the trouble maker.
        ''' https://blogs.msdn.microsoft.com/dataaccesstechnologies/2009/04/08/how-to-find-out-the-data-reader-referencing-an-ado-net-connection-object-to-fix-the-error-quotthere-is-already-an-open-datareader-associated-with-this-command-which-must-be-closed-firstquo/
        ''' </summary>
        Public Shared Function GetReferencedObjects(ByVal con As DbConnection) As List(Of Object)
            Dim res As New List(Of Object)
            Dim t As Type = con.[GetType]()
            Dim innerConnection As Object = t.GetField("_innerConnection", BindingFlags.Instance Or BindingFlags.NonPublic).GetValue(con)
            Dim tin As Type = innerConnection.[GetType]()
            Dim rc As Object
            Dim fi As FieldInfo
            If TypeOf con Is System.Data.SqlClient.SqlConnection Then fi = tin.BaseType.BaseType.GetField("_referenceCollection", BindingFlags.Instance Or BindingFlags.NonPublic) Else fi = tin.BaseType.GetField("_referenceCollection", BindingFlags.Instance Or BindingFlags.NonPublic)
            If fi Is Nothing Then Return res
            rc = fi.GetValue(innerConnection)
            If rc Is Nothing Then Return res
            Dim items As Object = rc.[GetType]().BaseType.GetField("_items", BindingFlags.Instance Or BindingFlags.NonPublic).GetValue(rc)
            Dim count As Integer = Convert.ToInt32(items.[GetType]().GetProperty("Length", BindingFlags.Instance Or BindingFlags.[Public]).GetValue(items, Nothing))
            Dim miGetValue As MethodInfo = items.[GetType]().GetMethod("GetValue", New Type() {GetType(Integer)})
            For i As Integer = 0 To count - 1
                Dim referencedObj As Object = miGetValue.Invoke(items, New Object() {i})
                Dim hasTarget As Boolean = Convert.ToBoolean(referencedObj.[GetType]().GetProperty("HasTarget").GetValue(referencedObj, Nothing))
                If hasTarget Then
                    Dim objTarget As Object = referencedObj.[GetType]().GetProperty("Target").GetValue(referencedObj, Nothing)
                    res.Add(objTarget)
                End If
            Next

            Return res
        End Function

        Public Class Sql
            '        Private ReadOnly lic As Boolean = Internal.CheckMyLicense

            Public Shared Function EscapeLikeValue(ByVal value As String) As String
                Dim sb As New System.Text.StringBuilder(value.Length)
                For i As Integer = 0 To value.Length - 1
                    Dim c As Char = value(i)
                    Select Case c
                        Case "]"c, "["c, "%"c, "*"c
                            sb.Append("[").Append(c).Append("]")
                            Exit Select
                        Case "'"c
                            sb.Append("''")
                            Exit Select
                        Case Else
                            sb.Append(c)
                            Exit Select
                    End Select
                Next
                Return sb.ToString()
            End Function

            Public Shared Function EscapeInsertSingleQuote(ByVal value As String) As String
                'Dim intWhere As Integer = InStr(value.ToUpper, "WHERE")

                'Dim sb As New System.Text.StringBuilder(value.Length)
                'For i As Integer = 0 To value.Length - 1
                '    Dim c As Char = value(i)
                '    If i >= intWhere Then
                '        Select Case c
                '            'Case "'"c
                '            '    sb.Append("''")
                '            '    Exit Select
                '            Case Else
                '                sb.Append(c)
                '                Exit Select
                '        End Select
                '    Else
                '        sb.Append(c)
                '    End If
                'Next
                'Return sb.ToString()
                Return value
            End Function

            ''' <summary>
            ''' Get query to retrieve all table name in connected database.
            ''' </summary>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Public Shared Function GetTableListQuery() As String
                Return "SELECT sobjects.name As 'Table' FROM sysobjects sobjects WHERE sobjects.xtype = 'U' Order By sobjects.name"
            End Function

            ''' <summary>
            ''' Get query to retrieve all columns in the connected database.
            ''' </summary>
            ''' <param name="TableName">Optional, will be % wildcard if not set.</param>
            ''' <param name="ColumnFilter">Optional, will be % wildcard if not set.</param>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Public Shared Function GetColumnListQuery(Optional ByVal TableName As String = "%", Optional ByVal ColumnFilter As String = "%") As String
                Dim sb As New System.Text.StringBuilder
                sb.AppendLine(" Declare @Table nvarchar(1000), @Col nvarchar(1000)")
                sb.AppendLine(" Set @Table = '" & TableName & "'")
                sb.AppendLine(" Set @Col = '" & ColumnFilter & "'")
                sb.AppendLine(" SELECT TB.Name AS 'Table'")
                sb.AppendLine(" 	,C.Name as 'Column'")
                sb.AppendLine(" 	,T.Name AS 'Type'")
                sb.AppendLine(" 	,C.Max_Length")
                sb.AppendLine(" 	,C.Is_Nullable")
                sb.AppendLine(" 	,IsNull(PK.CONSTRAINT_NAME,'') AS 'Constraint'")
                sb.AppendLine(" FROM SYS.COLUMNS C INNER JOIN SYS.TABLES tb ON tb.[object_id] = C.[object_id]")
                sb.AppendLine(" 	INNER JOIN SYS.TYPES T ON C.system_type_id = T.user_type_id")
                sb.AppendLine(" 	LEFT JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE PK ON TB.Name = PK.TABLE_NAME AND C.NAME = PK.COLUMN_NAME")
                sb.AppendLine(" WHERE tb.[is_ms_shipped] = 0")
                sb.AppendLine(" And TB.Name LIKE @Table")
                sb.AppendLine(" And C.Name LIKE @Col")
                sb.AppendLine(" ORDER BY tb.[Name], C.Is_Nullable")

                Return sb.ToString
            End Function

            ''' <summary>
            ''' Get query to retrieve number of connected users from the current connection.
            ''' </summary>
            ''' <param name="DbName">Optional, will be % wildcard if not set.</param>
            ''' <param name="LoginName">Optional, will be % wildcard if not set.</param>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Public Shared Function GetConnectedUsers(Optional ByVal DbName As String = "%", Optional ByVal LoginName As String = "%") As String
                Dim sb As New System.Text.StringBuilder
                sb.AppendLine(" SELECT DB_NAME(dbid) as DBName, ")
                sb.AppendLine("        COUNT(dbid) as NumberOfConnections, ")
                sb.AppendLine("        loginame as LoginName ")
                sb.AppendLine(" FROM sys.sysprocesses ")
                sb.AppendLine(" WHERE dbid > 0 ")
                sb.AppendLine(" And DB_NAME(dbid) LIKE '" & DbName & "'")
                sb.AppendLine(" And loginame LIKE '" & LoginName & "'")
                sb.AppendLine(" GROUP BY dbid, loginame")
                Return sb.ToString
            End Function

            ''' <summary>
            ''' Prettify SQL query into a one liner with paragraph-like spacing.
            ''' </summary>
            Public Shared Function SQLFormatter(ByVal SQL As String) As String
                Dim _SQL = SQL

                'Format query - replace unliked characters with better ones
                _SQL = _SQL.Replace("\n", " ") _
                            .Replace("\r", " ") _
                            .Replace("\r\n", " ") _
                            .Replace(vbCrLf, " ") _
                            .Replace("	", " ") _
                            .Replace(",", ", ") _
                            .Trim.ToUpper()

                'Format query - remove all multiple spaces
                _SQL = System.Text.RegularExpressions.Regex.Replace(_SQL, " {2,}", " ")

                'Format query - prettify the query
                _SQL = _SQL.Replace(" ,", ",")

                Return _SQL
            End Function
        End Class

        Public Class SqlString
            ''' <summary>
            ''' Safely execute your query with WHERE clause by closing single quote with String.Format like structure. 
            ''' Eg. SqlString.Format("Select * From Staff Where ID = '{0}' Or Name = '{1}'", "0123'456", "Ch'ng KS") 
            ''' It will return "Select * From Staff Where ID = '0123''456' Or Name = 'Ch''ng KS'"
            ''' </summary>
            Public Shared Function Format(txt As String, ParamArray args() As Object) As String
                Dim res As String = txt

                Dim obList As New List(Of Object)

                For Each obj As Object In args
                    If TypeOf obj Is String AndAlso obj.ToString.Contains("'") Then
                        obList.Add(obj.ToString.Replace("'", "''"))
                    Else
                        obList.Add(obj)
                    End If
                Next

                res = String.Format(txt, obList.ToArray)

                Return res
            End Function
        End Class
    End Class
End Namespace
