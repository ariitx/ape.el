Imports System.ServiceProcess
Imports System.Net.NetworkInformation
Imports System.Net
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports Ape.EL.Data.DbUtility
Imports Ape.EL.WinForm.General
Imports Ape.EL.InternalGlobal
Imports Ape.EL.App.General

Namespace Data
    ''' <summary>
    ''' Abstract class to provide SQL connection object.
    ''' </summary>
    ''' <remarks></remarks>
    Public MustInherit Class DbStandardConnection
        Implements IDisposable
        '        Private ReadOnly lic As Boolean = Internal.CheckMyLicense

        'General
        Protected bDBConnected As Boolean  'Database Connection Status

        Public Property ConnectionSQL() As SqlConnection
            Get
                Return oConnSQL
            End Get
            Set(ByVal value As SqlConnection)
                oConnSQL = value
            End Set
        End Property
        Private oConnSQL As New SqlConnection

        Public Property ConnectionString() As String
            Get
                Return _ConnectionString
            End Get
            Set(ByVal value As String)
                _ConnectionString = value
            End Set
        End Property
        Private _ConnectionString As String = ""

        Public Property IsDisposed As Boolean
            Get
                Return _IsDisposed
            End Get
            Set(value As Boolean)
                _IsDisposed = value
            End Set
        End Property
        Private _IsDisposed As Boolean

        ''' <summary>
        ''' Create a new connection based on provided connectionn string.
        ''' </summary>
        Public Sub New(Optional ByVal strConnString As String = "")
            If Not String.IsNullOrEmpty(strConnString) Then
                ConnectionString = strConnString
            End If

            Initialize()
        End Sub

        ''' <summary>
        ''' Reuse provided connection.
        ''' </summary>
        Public Sub New(ByVal SqlConn As SqlConnection)
            MyConnection = SqlConn
            ConnectionString = SqlConn.ConnectionString

            Initialize()
        End Sub

        ''' <summary>
        ''' Renew connection.
        ''' </summary>
        Public Sub Reconnect()
            Initialize()
        End Sub

        Private Sub Initialize()
            Dim oConnectToDB As New DbInitializer

            'try to connect using MyConnection, must have same connection string
            If MyConnection IsNot Nothing Then
                If MyConnection.State = ConnectionState.Open Then
                    Dim csbMyConnection As SqlConnectionStringBuilder = GetCsb(MyConnection.ConnectionString)
                    Dim csbConnString As SqlConnectionStringBuilder = GetCsb(ConnectionString)

                    If csbMyConnection.DataSource = csbConnString.DataSource And _
                            csbMyConnection.InitialCatalog = csbConnString.InitialCatalog And _
                            csbMyConnection.IntegratedSecurity = csbConnString.IntegratedSecurity And _
                            csbMyConnection.UserID = csbConnString.UserID And _
                            csbMyConnection.Password = csbConnString.Password And _
                            csbMyConnection.MultipleActiveResultSets = csbConnString.MultipleActiveResultSets Then
                        oConnSQL = MyConnection
                        bDBConnected = True
                    End If
                End If
            End If

            'if connection not established, then start create a new connection
            If bDBConnected = False Then
                oConnectToDB.ConnectionString = ConnectionString
                oConnectToDB.ConnectSql(bDBConnected, oConnSQL)
                MyConnection = oConnSQL
            End If

            oConnectToDB = Nothing
        End Sub

        Public Overridable Sub Dispose() Implements IDisposable.Dispose
            If Not (Me IsNot Nothing AndAlso Not Me.IsDisposed) Then
                Exit Sub
            End If

            Try
                'By closing the connection, it will kill the thread to the database which will remove it from connection pool
                ' if only set it as nothing, the pool will exhaust faster and needed to be GC'ed more
                ' conn.Close and conn.Dispose have same function
                If oConnSQL IsNot Nothing Then oConnSQL.Close()
            Catch ex As Exception
            Finally
                oConnSQL = Nothing
                bDBConnected = False
                IsDisposed = True

                'GC the memory when workingset exceed or equal 128 mb
                ' keep GC'ing will make the program lag, as GC'ing requires about 850 ms to execute
                ' we only need to GC as necessary
                If Process.GetCurrentProcess().WorkingSet64 >> 20 >= 128 Then
                    FlushMemory()
                End If
            End Try
        End Sub

        ''' <summary>
        ''' Clear connection from pool to close connection this program has opened, avoiding exclusive lock, then perform Dispose method.
        ''' </summary>
        Public Sub ClearPool()
            SqlConnection.ClearPool(ConnectionSQL)
            Dispose()
        End Sub
    End Class
End Namespace
