﻿Imports System.Drawing
Imports System.ComponentModel

Namespace Data
    ''' <summary>
    ''' General helper to wrap device setting's structure (based on registry). Inherited from DbSetting.
    ''' </summary>
    Public Class DbSettingDevice
        Inherits DbSetting

        <Obsolete("Deprecated", True), EditorBrowsable(EditorBrowsableState.Never)> Public Shadows Property Binary
        <Obsolete("Deprecated", True), EditorBrowsable(EditorBrowsableState.Never)> Public Shadows Property BinObj
        <Obsolete("Deprecated", True), EditorBrowsable(EditorBrowsableState.Never)> Public Shadows Property ValImage
        <Obsolete("Deprecated", True), EditorBrowsable(EditorBrowsableState.Never)> Public Shadows Property Remark

        Shared Shadows Function Create(ByVal ID As String, Optional ByVal Value As String = Nothing, Optional ByVal DefaultValue As Object = "") As DbSetting
            Return DbSetting.Create(ID, Value, , , DefaultValue)
        End Function
    End Class
End Namespace
