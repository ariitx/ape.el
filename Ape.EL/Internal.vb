﻿Imports System.Reflection
Imports System.Runtime.InteropServices
Imports Ape.EL.WinForm.General

''' <summary>
''' Support internal program processes
''' </summary>
''' <remarks></remarks>
Public Class Internal
    ''' <summary>
    ''' Return root namespace of the entry assembly.
    ''' Return empty string if catch an error.
    ''' </summary>
    Friend Shared Function GetAsmNamespace(Optional ByVal root As Boolean = True) As String
        Return Utility.General.GetAsmNamespace(GetAsm, root)
    End Function

    ''' <summary>
    ''' Return file path of the entry assembly.
    ''' </summary>
    Friend Shared Function GetAsmFilePath() As String
        Dim str As String = ""

        str = GetAsm.BinaryPath
        Return str
    End Function

    ''' <summary>
    ''' Return the origin of entry assembly
    ''' </summary>
    Friend Shared Function GetAsm() As Assembly
        Return Misc.AssemblyHelper.GetTrueEntryAssembly
    End Function

    ''' <summary>
    ''' Internal utility to change icon to either calling assembly's icon.
    ''' </summary>
    ''' <param name="asmicon">The icon to be changed.</param>
    ''' <remarks></remarks>
    Public Shared Sub SetIcon(ByRef asmicon As Drawing.Icon)
        Dim ic As Drawing.Icon = Utility.General.GetAssemblyIcon(Internal.GetAsmFilePath, True)
        If _DefaultIcon IsNot Nothing Then
            asmicon = _DefaultIcon
        ElseIf ic IsNot Nothing Then
            asmicon = ic 'need to retrieve icon from sender
        End If
    End Sub

    ''' <summary>
    ''' Set icon to replace assembly's icon in SetIcon function.
    ''' </summary>
    Public Shared WriteOnly Property DefaultIcon As Drawing.Icon
        Set(value As Drawing.Icon)
            _DefaultIcon = value
        End Set
    End Property
    Private Shared _DefaultIcon As Drawing.Icon = Nothing

    ''' <summary>
    ''' Get this method assembly.
    ''' </summary>
    Friend Shared Function GetMyAssembly() As Assembly
        Return (New System.Diagnostics.StackTrace).GetFrame(0).GetMethod.Module.Assembly
    End Function
End Class
