﻿Imports System.Globalization
Imports System.Text
Imports System.Runtime.CompilerServices

Namespace ExtensionsSystem
    ''' <summary>
    ''' Override original System.String public functions' original value with ToStr to make it compatible with null (avoid null exception).
    ''' </summary>
    <HideModuleName()> _
    Public Module StringExtension
        <Extension>
        Public Function CloneEx(obj1 As String) As Object
            Return obj1.ToStr.Clone
        End Function

        <Extension>
        Public Function CompareToEx(obj1 As String, ByVal value As Object) As Integer
            Return obj1.ToStr.CompareTo(value)
        End Function

        <Extension>
        Public Function CompareToEx(obj1 As String, ByVal strB As String) As Integer
            Return obj1.ToStr.CompareTo(strB)
        End Function

        <Extension>
        Public Function ContainsEx(obj1 As String, ByVal value As String) As Boolean
            Return obj1.ToStr.Contains(value)
        End Function

        <Extension>
        Public Function EndsWithEx(obj1 As String, ByVal value As String) As Boolean
            Return obj1.ToStr.EndsWith(value)
        End Function

        <Extension>
        Public Function EndsWithEx(obj1 As String, ByVal value As String, ByVal comparisonType As StringComparison) As Boolean
            Return obj1.ToStr.EndsWith(value, comparisonType)
        End Function

        <Extension>
        Public Function EndsWithEx(obj1 As String, ByVal value As String, ByVal ignoreCase As Boolean, ByVal culture As CultureInfo) As Boolean
            Return obj1.ToStr.EndsWith(value, ignoreCase, culture)
        End Function

        <Extension>
        Public Function EqualsEx(obj1 As String, ByVal value As String) As Boolean
            Return obj1.ToStr.Equals(value)
        End Function

        <Extension>
        Public Function EqualsEx(obj1 As String, ByVal value As String, ByVal comparisonType As StringComparison) As Boolean
            Return obj1.ToStr.Equals(comparisonType)
        End Function

        <Extension>
        Public Function GetEnumeratorEx(obj1 As String) As CharEnumerator
            Return obj1.ToStr.GetEnumerator
        End Function

        <Extension>
        Public Function GetTypeCodeEx(obj1 As String) As TypeCode
            Return obj1.ToStr.GetTypeCode
        End Function

        <Extension>
        Public Function IndexOfEx(obj1 As String, ByVal value As Char) As Integer
            Return obj1.ToStr.IndexOf(value)
        End Function

        <Extension>
        Public Function IndexOfEx(obj1 As String, ByVal value As String) As Integer
            Return obj1.ToStr.IndexOf(value)
        End Function

        <Extension>
        Public Function IndexOfEx(obj1 As String, ByVal value As Char, ByVal startIndex As Integer) As Integer
            Return obj1.ToStr.IndexOf(value, startIndex)
        End Function

        <Extension>
        Public Function IndexOfEx(obj1 As String, ByVal value As String, ByVal startIndex As Integer) As Integer
            Return obj1.ToStr.IndexOf(value, startIndex)
        End Function

        <Extension>
        Public Function IndexOfEx(obj1 As String, ByVal value As String, ByVal comparisonType As StringComparison) As Integer
            Return obj1.ToStr.IndexOf(value, comparisonType)
        End Function

        <Extension>
        Public Function IndexOfEx(obj1 As String, ByVal value As Char, ByVal startIndex As Integer, ByVal count As Integer) As Integer
            Return obj1.ToStr.IndexOf(value, startIndex, count)
        End Function

        <Extension>
        Public Function IndexOfEx(obj1 As String, ByVal value As String, ByVal startIndex As Integer, ByVal count As Integer) As Integer
            Return obj1.ToStr.IndexOf(value, startIndex, count)
        End Function

        <Extension>
        Public Function IndexOfEx(obj1 As String, ByVal value As String, ByVal startIndex As Integer, ByVal comparisonType As StringComparison) As Integer
            Return obj1.ToStr.IndexOf(value, startIndex, comparisonType)
        End Function

        <Extension>
        Public Function IndexOfEx(obj1 As String, ByVal value As String, ByVal startIndex As Integer, ByVal count As Integer, ByVal comparisonType As StringComparison) As Integer
            Return obj1.ToStr.IndexOf(value, startIndex, count, comparisonType)
        End Function

        <Extension>
        Public Function IndexOfAnyEx(obj1 As String, ByVal anyOf As Char()) As Integer
            Return obj1.ToStr.IndexOfAny(anyOf)
        End Function

        <Extension>
        Public Function IndexOfAnyEx(obj1 As String, ByVal anyOf As Char(), ByVal startIndex As Integer) As Integer
            Return obj1.ToStr.IndexOfAny(anyOf, startIndex)
        End Function

        <Extension>
        Public Function IndexOfAnyEx(obj1 As String, ByVal anyOf As Char(), ByVal startIndex As Integer, ByVal count As Integer) As Integer
            Return obj1.ToStr.IndexOfAny(anyOf, startIndex, count)
        End Function

        <Extension>
        Public Function InsertEx(obj1 As String, ByVal startIndex As Integer, ByVal value As String) As String
            Return obj1.ToStr.Insert(startIndex, value)
        End Function

        <Extension>
        Public Function IsNormalizedEx(obj1 As String) As Boolean
            Return obj1.ToStr.IsNormalized
        End Function

        <Extension>
        Public Function IsNormalizedEx(obj1 As String, ByVal normalizationForm As NormalizationForm) As Boolean
            Return obj1.ToStr.IsNormalized(normalizationForm)
        End Function

        <Extension>
        Public Function LastIndexOfEx(obj1 As String, ByVal value As Char) As Integer
            Return obj1.ToStr.LastIndexOf(value)
        End Function

        <Extension>
        Public Function LastIndexOfEx(obj1 As String, ByVal value As String) As Integer
            Return obj1.ToStr.LastIndexOf(value)
        End Function

        <Extension>
        Public Function LastIndexOfEx(obj1 As String, ByVal value As Char, ByVal startIndex As Integer) As Integer
            Return obj1.ToStr.LastIndexOf(value, startIndex)
        End Function

        <Extension>
        Public Function LastIndexOfEx(obj1 As String, ByVal value As String, ByVal startIndex As Integer) As Integer
            Return obj1.ToStr.LastIndexOf(value, startIndex)
        End Function

        <Extension>
        Public Function LastIndexOfEx(obj1 As String, ByVal value As String, ByVal comparisonType As StringComparison) As Integer
            Return obj1.ToStr.LastIndexOf(value, comparisonType)
        End Function

        <Extension>
        Public Function LastIndexOfEx(obj1 As String, ByVal value As Char, ByVal startIndex As Integer, ByVal count As Integer) As Integer
            Return obj1.ToStr.LastIndexOf(value, startIndex, count)
        End Function

        <Extension>
        Public Function LastIndexOfEx(obj1 As String, ByVal value As String, ByVal startIndex As Integer, ByVal count As Integer) As Integer
            Return obj1.ToStr.LastIndexOf(value, startIndex, count)
        End Function

        <Extension>
        Public Function LastIndexOfEx(obj1 As String, ByVal value As String, ByVal startIndex As Integer, ByVal comparisonType As StringComparison) As Integer
            Return obj1.ToStr.LastIndexOf(value, startIndex, comparisonType)
        End Function

        <Extension>
        Public Function LastIndexOfEx(obj1 As String, ByVal value As String, ByVal startIndex As Integer, ByVal count As Integer, ByVal comparisonType As StringComparison) As Integer
            Return obj1.ToStr.LastIndexOf(value, startIndex, count, comparisonType)
        End Function

        <Extension>
        Public Function LastIndexOfAnyEx(obj1 As String, ByVal anyOf As Char()) As Integer
            Return obj1.ToStr.LastIndexOfAny(anyOf)
        End Function

        <Extension>
        Public Function LastIndexOfAnyEx(obj1 As String, ByVal anyOf As Char(), ByVal startIndex As Integer) As Integer
            Return obj1.ToStr.LastIndexOf(anyOf, startIndex)
        End Function

        <Extension>
        Public Function LastIndexOfAnyEx(obj1 As String, ByVal anyOf As Char(), ByVal startIndex As Integer, ByVal count As Integer) As Integer
            Return obj1.ToStr.LastIndexOf(anyOf, startIndex, count)
        End Function

        <Extension>
        Public Function NormalizeEx(obj1 As String) As String
            Return obj1.ToStr.Normalize
        End Function

        <Extension>
        Public Function NormalizeEx(obj1 As String, ByVal normalizationForm As NormalizationForm) As String
            Return obj1.ToStr.Normalize(normalizationForm)
        End Function

        <Extension>
        Public Function PadLeftEx(obj1 As String, ByVal totalWidth As Integer) As String
            Return obj1.ToStr.PadLeft(totalWidth)
        End Function

        <Extension>
        Public Function PadLeftEx(obj1 As String, ByVal totalWidth As Integer, ByVal paddingChar As Char) As String
            Return obj1.ToStr.PadLeft(totalWidth, paddingChar)
        End Function

        <Extension>
        Public Function PadRightEx(obj1 As String, ByVal totalWidth As Integer) As String
            Return obj1.ToStr.PadRight(totalWidth)
        End Function

        <Extension>
        Public Function PadRightEx(obj1 As String, ByVal totalWidth As Integer, ByVal paddingChar As Char) As String
            Return obj1.ToStr.PadRight(totalWidth, paddingChar)
        End Function

        <Extension>
        Public Function RemoveEx(obj1 As String, ByVal startIndex As Integer) As String
            Return obj1.ToStr.Remove(startIndex)
        End Function

        <Extension>
        Public Function RemoveEx(obj1 As String, ByVal startIndex As Integer, ByVal count As Integer) As String
            Return obj1.ToStr.Remove(startIndex, count)
        End Function

        <Extension>
        Public Function ReplaceEx(obj1 As String, ByVal oldChar As Char, ByVal newChar As Char) As String
            Return obj1.ToStr.Replace(oldChar, newChar)
        End Function

        <Extension>
        Public Function ReplaceEx(obj1 As String, ByVal oldValue As String, ByVal newValue As String) As String
            Return obj1.ToStr.Replace(oldValue, newValue)
        End Function

        <Extension>
        Public Function SplitEx(obj1 As String, ByVal ParamArray separator As Char()) As String()
            Return obj1.ToStr.Split(separator)
        End Function

        <Extension>
        Public Function SplitEx(obj1 As String, ByVal separator As Char(), ByVal count As Integer) As String()
            Return obj1.ToStr.Split(separator, count)
        End Function

        <Extension>
        Public Function SplitEx(obj1 As String, ByVal separator As Char(), ByVal options As StringSplitOptions) As String()
            Return obj1.ToStr.Split(separator, options)
        End Function

        <Extension>
        Public Function SplitEx(obj1 As String, ByVal separator As String(), ByVal options As StringSplitOptions) As String()
            Return obj1.ToStr.Split(separator, options)
        End Function

        <Extension>
        Public Function SplitEx(obj1 As String, ByVal separator As Char(), ByVal count As Integer, ByVal options As StringSplitOptions) As String()
            Return obj1.ToStr.Split(separator, count, options)
        End Function

        <Extension>
        Public Function SplitEx(obj1 As String, ByVal separator As String(), ByVal count As Integer, ByVal options As StringSplitOptions) As String()
            Return obj1.ToStr.Split(separator, count, options)
        End Function

        <Extension>
        Public Function StartsWithEx(obj1 As String, ByVal value As String) As Boolean
            Return obj1.ToStr.StartsWith(value)
        End Function

        <Extension>
        Public Function StartsWithEx(obj1 As String, ByVal value As String, ByVal comparisonType As StringComparison) As Boolean
            Return obj1.ToStr.StartsWith(value, comparisonType)
        End Function

        <Extension>
        Public Function StartsWithEx(obj1 As String, ByVal value As String, ByVal ignoreCase As Boolean, ByVal culture As CultureInfo) As Boolean
            Return obj1.ToStr.StartsWith(value, ignoreCase, culture)
        End Function

        <Extension>
        Public Function SubstringEx(obj1 As String, ByVal startIndex As Integer) As String
            Return obj1.ToStr.Substring(startIndex)
        End Function

        <Extension>
        Public Function SubstringEx(obj1 As String, ByVal startIndex As Integer, ByVal length As Integer) As String
            Return obj1.ToStr.Substring(startIndex, length)
        End Function

        <Extension>
        Public Function ToCharArrayEx(obj1 As String) As Char()
            Return obj1.ToStr.ToCharArray
        End Function

        <Extension>
        Public Function ToCharArrayEx(obj1 As String, ByVal startIndex As Integer, ByVal length As Integer) As Char()
            Return obj1.ToStr.ToCharArray(startIndex, length)
        End Function

        <Extension>
        Public Function ToLowerEx(obj1 As String) As String
            Return obj1.ToStr.ToLower
        End Function

        <Extension>
        Public Function ToLowerEx(obj1 As String, ByVal culture As CultureInfo) As String
            Return obj1.ToStr.ToLower(culture)
        End Function

        <Extension>
        Public Function ToLowerInvariantEx(obj1 As String) As String
            Return obj1.ToStr.ToLowerInvariant
        End Function

        <Extension>
        Public Function ToStringEx(obj1 As String, ByVal provider As IFormatProvider) As String
            Return obj1.ToStr.ToString(provider)
        End Function

        <Extension>
        Public Function ToUpperEx(obj1 As String) As String
            Return obj1.ToStr.ToUpper
        End Function

        <Extension>
        Public Function ToUpperEx(obj1 As String, ByVal culture As CultureInfo) As String
            Return obj1.ToStr.ToUpper(culture)
        End Function

        <Extension>
        Public Function ToUpperInvariantEx(obj1 As String) As String
            Return obj1.ToStr.ToUpperInvariant
        End Function

        <Extension>
        Public Function TrimEx(obj1 As String) As String
            Return obj1.ToStr.Trim
        End Function

        <Extension>
        Public Function TrimEx(obj1 As String, ByVal ParamArray trimChars As Char()) As String
            Return obj1.ToStr.Trim(trimChars)
        End Function

        <Extension>
        Public Function TrimEndEx(obj1 As String, ByVal ParamArray trimChars As Char()) As String
            Return obj1.ToStr.TrimEnd(trimChars)
        End Function

        <Extension>
        Public Function TrimStartEx(obj1 As String, ByVal ParamArray trimChars As Char()) As String
            Return obj1.ToStr.TrimStart(trimChars)
        End Function
    End Module
End Namespace
