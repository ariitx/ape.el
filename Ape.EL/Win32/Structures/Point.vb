﻿Imports System
Imports System.Runtime.InteropServices

Namespace Win32.Structures
    <StructLayout(LayoutKind.Sequential)> _
    Friend Structure Point
        Public X As Integer
        Public Y As Integer
    End Structure
End Namespace

