﻿Imports System
Imports System.Runtime.InteropServices

Namespace Win32.Structures
    <StructLayout(LayoutKind.Sequential)> _
    Friend Structure MouseLLHookStruct
        Public Point As Point
        Public MouseData As Integer
        Public Flags As Integer
        Public Time As Integer
        Public ExtraInfo As Integer
    End Structure
End Namespace

