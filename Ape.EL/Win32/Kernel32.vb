﻿Imports System.Runtime.InteropServices

Namespace Win32
    ''' <summary>
    ''' This class is to control error in visual studio by not catching uncaught exception while debugging.
    ''' To apply the fix, open Application.Designer.vb file and add the following .
    '''      MyBase.New(Global.Microsoft.VisualBasic.ApplicationServices.AuthenticationMode.Windows)
    ''' ++   Kernel32.DisableUMCallbackFilter()
    '''      Me.IsSingleInstance = False
    ''' </summary>
    ''' <remarks></remarks>
    Public NotInheritable Class Kernel32
'        Private ReadOnly lic As Boolean = Internal.CheckMyLicense

        Private Sub New()

        End Sub

        Public Const PROCESS_CALLBACK_FILTER_ENABLED As UInteger = &H1

        <DllImport("Kernel32.dll")> _
        Public Shared Function SetProcessUserModeExceptionPolicy(ByVal dwFlags As UInt32) As Boolean
        End Function

        <DllImport("Kernel32.dll")> _
        Public Shared Function GetProcessUserModeExceptionPolicy(ByRef lpFlags As UInt32) As Boolean
        End Function

        Public Shared Sub DisableUMCallbackFilter()
            Dim flags As UInteger
            GetProcessUserModeExceptionPolicy(flags)

            flags = flags And Not PROCESS_CALLBACK_FILTER_ENABLED
            SetProcessUserModeExceptionPolicy(flags)
        End Sub

        ' API call to prevent sleep (until the application exits)
        Friend Declare Function SetThreadExecutionState Lib "kernel32" (ByVal esflags As EXECUTION_STATE) As EXECUTION_STATE

        ' Define the API execution states
        Friend Enum EXECUTION_STATE
            ES_SYSTEM_REQUIRED = &H1    ' Stay in working state by resetting display idle timer
            ES_DISPLAY_REQUIRED = &H2   ' Force display on by resetting system idle timer
            ES_CONTINUOUS = &H80000000  ' Force this state until next ES_CONTINUOUS call and one of the other flags are cleared
        End Enum
    End Class
End Namespace
