﻿Imports System.Runtime.InteropServices
Imports System.Drawing

Namespace Win32

    Public Class MouseInputEmulation
        <DllImport("user32.dll")> _
        Private Shared Sub mouse_event(dwFlags As UInteger, dx As UInteger, dy As UInteger, dwData As UInteger, dwExtraInfo As Integer)
        End Sub

        <DllImport("user32.dll", SetLastError:=True)> _
        Private Shared Function SetCursorPos(ByVal X As Integer, ByVal Y As Integer) As Boolean
        End Function

        <DllImport("user32.dll", ExactSpelling:=True, SetLastError:=True)> _
        Public Shared Function GetCursorPos(ByRef lpPoint As System.Drawing.Point) As <MarshalAs(UnmanagedType.Bool)> Boolean
        End Function

        Public Const MOUSEEVENTF_LEFTDOWN = &H2
        Public Const MOUSEEVENTF_LEFTUP = &H4
        Public Const MOUSEEVENTF_MIDDLEDOWN = &H20
        Public Const MOUSEEVENTF_MIDDLEUP = &H40
        Public Const MOUSEEVENTF_RIGHTDOWN = &H8
        Public Const MOUSEEVENTF_RIGHTUP = &H10
        Public Const MOUSEEVENTF_MOVE = &H1

        Public Function GetCurrentX() As Long
            Dim Position As Point
            GetCursorPos(Position)
            GetCurrentX = Position.X
        End Function

        Public Function GetCurrentY() As Long
            Dim Position As Point
            GetCursorPos(Position)
            GetCurrentY = Position.Y
        End Function

        Public Shared Sub LeftClick()
            LeftDown()
            LeftUp()
        End Sub

        Public Shared Sub LeftDown()
            mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0)
        End Sub

        Public Shared Sub LeftUp()
            mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, 0)
        End Sub

        Public Shared Sub MiddleClick()
            MiddleDown()
            MiddleUp()
        End Sub

        Public Shared Sub MiddleDown()
            mouse_event(MOUSEEVENTF_MIDDLEDOWN, 0, 0, 0, 0)
        End Sub

        Public Shared Sub MiddleUp()
            mouse_event(MOUSEEVENTF_MIDDLEUP, 0, 0, 0, 0)
        End Sub

        Public Shared Sub MoveMouse(xMove As Long, yMove As Long)
            mouse_event(MOUSEEVENTF_MOVE, xMove, yMove, 0, 0)
        End Sub

        Public Shared Sub RightClick()
            RightDown()
            RightUp()
        End Sub

        Public Shared Sub RightDown()
            mouse_event(MOUSEEVENTF_RIGHTDOWN, 0, 0, 0, 0)
        End Sub

        Public Shared Sub RightUp()
            mouse_event(MOUSEEVENTF_RIGHTUP, 0, 0, 0, 0)
        End Sub
    End Class
End Namespace
