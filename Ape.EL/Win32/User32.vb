﻿Imports System
Imports System.Runtime.InteropServices
Imports System.Drawing

Namespace Win32
    Public Class User32
        Public Const ULW_COLORKEY As Int32 = &H1
        Public Const ULW_ALPHA As Int32 = &H2
        Public Const ULW_OPAQUE As Int32 = &H4
        Public Const AC_SRC_OVER As Byte = &H0
        Public Const AC_SRC_ALPHA As Byte = &H1

        Private Shared lastHandle As IntPtr = IntPtr.Zero

        ''' <summary>
        ''' Get a handle to an application window.
        ''' </summary>
        Public Declare Function FindWindow Lib "User32.dll" (lpClassName As String, lpWindowName As String) As IntPtr

        Public Declare Function GetDesktopWindow Lib "User32.dll" () As IntPtr

        Public Declare Function GetWindowDC Lib "User32.dll" (hWnd As IntPtr) As IntPtr

        Public Declare Function ReleaseDC Lib "User32.dll" (hWnd As IntPtr, hDC As IntPtr) As Integer

        Public Declare Function SetActiveWindow Lib "User32.dll" (handle As IntPtr) As IntPtr

        Public Declare Function GetForegroundWindow Lib "User32.dll" () As IntPtr

        Private Declare Function GetShellWindow Lib "User32.dll" () As IntPtr

        <DllImport("user32.dll")> _
        Public Shared Function GetWindowRect(hWnd As IntPtr, ByRef rect As RECT) As IntPtr
        End Function

        <StructLayout(LayoutKind.Sequential)> _
        Public Structure RECT
            Public left As Integer
            Public top As Integer
            Public right As Integer
            Public bottom As Integer
        End Structure

        Public Shared Function GetLastActive() As IntPtr
            Dim curHandle As IntPtr = GetForegroundWindow()
            Dim retHandle As IntPtr = IntPtr.Zero

            If curHandle <> lastHandle Then
                'Keep previous for our check
                retHandle = lastHandle

                'Always set last 
                lastHandle = curHandle

                If retHandle <> IntPtr.Zero Then
                    Return retHandle
                End If
            End If
        End Function

        <StructLayout(LayoutKind.Sequential, Pack:=1)> _
        Public Structure BLENDFUNCTION
            Public BlendOp As Byte
            Public BlendFlags As Byte
            Public SourceConstantAlpha As Byte
            Public AlphaFormat As Byte
        End Structure

        Public Declare Auto Function UpdateLayeredWindow Lib "user32.dll" (hwnd As IntPtr, hdcDst As IntPtr, ByRef pptDst As Point, ByRef psize As Size, hdcSrc As IntPtr, ByRef pprSrc As Point, _
            crKey As Int32, ByRef pblend As BLENDFUNCTION, dwFlags As Int32) As Boolean

        Public Declare Auto Function GetDC Lib "user32.dll" (hWnd As IntPtr) As IntPtr

        Public Declare Function HideCaret Lib "user32.dll" (ByVal hWnd As IntPtr) As Boolean

        ''' <summary>
        ''' Activate an application window.
        ''' </summary>
        Public Declare Function SetForegroundWindow Lib "user32.dll" (ByVal hWnd As IntPtr) As Boolean

        <DllImport("kernel32.dll", CharSet:=CharSet.Auto)> _
        Public Shared Function GetModuleHandle(ByVal moduleName As String) As IntPtr
        End Function

        <DllImport("kernel32", CharSet:=CharSet.Auto, SetLastError:=True)> _
        Public Shared Function GetProcAddress(ByVal hModule As IntPtr, <MarshalAs(UnmanagedType.LPStr)> ByVal procName As String) As IntPtr
        End Function

        <DllImport("kernel32.dll", CharSet:=CharSet.Auto, SetLastError:=True)> _
        Public Shared Function IsWow64Process(ByVal hProcess As IntPtr, <Out> ByRef wow64Process As Boolean) As <MarshalAs(UnmanagedType.Bool)> Boolean
        End Function

        <DllImport("kernel32.dll")> _
        Public Shared Function GetCurrentProcess() As IntPtr
        End Function

        ''' <summary>
        ''' Check whether any win32 method exist.
        ''' </summary>
        Public Shared Function DoesWin32MethodExist(ByVal moduleName As String, ByVal methodName As String) As Boolean
            Dim moduleHandle As IntPtr = GetModuleHandle(moduleName)
            If (moduleHandle = IntPtr.Zero) Then
                Return False
            End If
            Return (GetProcAddress(moduleHandle, methodName) <> IntPtr.Zero)
        End Function

        <DllImportAttribute("user32.dll")> _
        Public Shared Function SendMessage(ByVal hWnd As IntPtr, ByVal Msg As Integer, ByVal wParam As Integer, ByVal lParam As Integer) As Integer
        End Function

        <DllImport("user32.dll", EntryPoint:="SendMessage", CharSet:=System.Runtime.InteropServices.CharSet.Auto)> _
        Public Shared Function SendMessage(hWnd As IntPtr, Msg As UInteger, wParam As Integer, lParam As System.Text.StringBuilder) As Boolean
        End Function

        Public Delegate Function EnumThreadWndProc(hWnd As IntPtr, lp As IntPtr) As Boolean

        <DllImport("user32.dll")> _
        Public Shared Function EnumThreadWindows(tid As Integer, callback As EnumThreadWndProc, lp As IntPtr) As Boolean
        End Function

        <DllImport("kernel32.dll")> _
        Public Shared Function GetCurrentThreadId() As Integer
        End Function

        <DllImport("user32.dll")> _
        Public Shared Function GetClassName(hWnd As IntPtr, buffer As System.Text.StringBuilder, buflen As Integer) As Integer
        End Function

        <DllImport("user32.dll")> _
        Public Shared Function GetDlgItem(hWnd As IntPtr, item As Integer) As IntPtr
        End Function

        <DllImportAttribute("user32.dll")> _
        Public Shared Function ReleaseCapture() As Boolean
        End Function

        <DllImportAttribute("user32.dll")> _
        Public Shared Function SetCapture(hWnd As IntPtr) As IntPtr
        End Function

        ''' <summary>
        ''' Lock or relase the window for updating.
        ''' </summary>
        <DllImport("user32")> _
        Public Shared Function LockWindowUpdate(hwnd As IntPtr) As IntPtr
        End Function

        <DllImport("user32.dll", EntryPoint:="SetLayeredWindowAttributes")>
        Public Shared Function SetLayeredWindowAttributes(ByVal hWnd As IntPtr, ByVal crKey As Integer, ByVal alpha As Byte, ByVal dwFlags As Integer) As Boolean
        End Function

        Public Structure PointStruct
            Public x As Int32
            Public y As Int32
        End Structure

        <DllImport("user32.dll")> _
        Public Shared Function WindowFromPoint(ByVal Point As PointStruct) As IntPtr
        End Function

#Region "Hotkey"
        Public Const MOD_CONTROL As Integer = &H2
        Public Const MOD_SHIFT As Integer = &H4
        Public Const WM_HOTKEY As Integer = &H312

        Public Declare Function RegisterHotKey Lib "User32.dll" (ByVal hwnd As IntPtr, _
                                      ByVal id As Integer, ByVal fsModifiers As Integer, _
                                      ByVal vk As Integer) As Integer

        Public Declare Function UnregisterHotKey Lib "user32.dll" (ByVal hwnd As IntPtr, _
                                      ByVal id As Integer) As Integer
#End Region

    End Class
End Namespace
