﻿Namespace Win32
    Public Class WindowStylesExtended
        Public Shared ReadOnly WS_EX_ACCEPTFILES As Int32 = &H10
        Public Shared ReadOnly WS_EX_APPWINDOW As Int32 = &H40000
        Public Shared ReadOnly WS_EX_CLIENTEDGE As Int32 = &H200
        Public Shared ReadOnly WS_EX_COMPOSITED As Int32 = &H2000000
        Public Shared ReadOnly WS_EX_CONTEXTHELP As Int32 = &H400
        Public Shared ReadOnly WS_EX_CONTROLPARENT As Int32 = &H10000
        Public Shared ReadOnly WS_EX_DLGMODALFRAME As Int32 = &H1
        Public Shared ReadOnly WS_EX_LAYERED As Int32 = &H80000
        Public Shared ReadOnly WS_EX_LAYOUTRTL As Int32 = &H400000
        Public Shared ReadOnly WS_EX_LEFT As Int32 = &H0
        Public Shared ReadOnly WS_EX_LEFTSCROLLBAR As Int32 = &H4000
        Public Shared ReadOnly WS_EX_LTRREADING As Int32 = &H0
        Public Shared ReadOnly WS_EX_MDICHILD As Int32 = &H40
        Public Shared ReadOnly WS_EX_NOACTIVATE As Int32 = &H8000000
        Public Shared ReadOnly WS_EX_NOINHERITLAYOUT As Int32 = &H100000
        Public Shared ReadOnly WS_EX_NOPARENTNOTIFY As Int32 = &H4
        Public Shared ReadOnly WS_EX_NOREDIRECTIONBITMAP As Int32 = &H200000
        Public Shared ReadOnly WS_EX_OVERLAPPEDWINDOW As Int32 = (WS_EX_WINDOWEDGE Or WS_EX_CLIENTEDGE)
        Public Shared ReadOnly WS_EX_PALETTEWINDOW As Int32 = (WS_EX_WINDOWEDGE Or WS_EX_TOOLWINDOW Or WS_EX_TOPMOST)
        Public Shared ReadOnly WS_EX_RIGHT As Int32 = &H1000
        Public Shared ReadOnly WS_EX_RIGHTSCROLLBAR As Int32 = &H0
        Public Shared ReadOnly WS_EX_RTLREADING As Int32 = &H2000
        Public Shared ReadOnly WS_EX_STATICEDGE As Int32 = &H20000
        Public Shared ReadOnly WS_EX_TOOLWINDOW As Int32 = &H80
        Public Shared ReadOnly WS_EX_TOPMOST As Int32 = &H8
        Public Shared ReadOnly WS_EX_TRANSPARENT As Int32 = &H20
        Public Shared ReadOnly WS_EX_WINDOWEDGE As Int32 = &H100
    End Class
End Namespace