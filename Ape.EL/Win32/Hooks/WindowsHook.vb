
Imports System.Runtime.InteropServices

Namespace Win32.Hooks
    ''' <summary>
    ''' Base class for working with Windows hooks.
    ''' </summary>
    Public Class WindowsHook

#Region "Class Fields"

        Protected hhook As IntPtr = IntPtr.Zero
        Protected callbackFunc As HookCallbackFunction = Nothing
        Protected hookType As HookType

#End Region

#Region "Properties"

        ''' <summary>
        ''' Gets property indication whether the hook is installed into the system's hook chain.
        ''' </summary>
        Public ReadOnly Property Installed() As Boolean
            Get
                Return hhook <> IntPtr.Zero
            End Get
        End Property

#End Region

#Region "Constructors"

        ''' <summary>
        ''' Creates new hook of specified type with gereral handling function.
        ''' </summary>
        ''' <param name="hook">The type of the hook.</param>
        Public Sub New(hook As HookType)
            hookType = hook
            callbackFunc = New HookCallbackFunction(AddressOf Me.CoreHookProc)
        End Sub

        ''' <summary>
        ''' Creates new hook of specified type with user defined handling function.
        ''' </summary>
        ''' <param name="hook">The type of the hook.</param>
        ''' <param name="func">Delegate of the user defined function.</param>
        Public Sub New(hook As HookType, func As HookCallbackFunction)
            hookType = hook
            callbackFunc = func
        End Sub

#End Region

#Region "Events"

        ''' <summary>
        ''' Occurs after the hook has been invoked.
        ''' </summary>
        Public Event HookInvoked As HookEventHandler
        Protected Sub RaiseHookInvoked(e As HookEventArgs)
            RaiseEvent HookInvoked(Me, e)
        End Sub
        Public Delegate Sub HookEventHandler(sender As Object, e As HookEventArgs)

#End Region

        ''' <summary>
        ''' General hook handling function prototype.
        ''' </summary>
        ''' <param name="code">Specifies the hook code passed to the current hook procedure.</param>
        ''' <param name="wParam">Specifies the wParam code passed to the current hook procedure.</param>
        ''' <param name="lParam">Specifies the lParam code passed to the current hook procedure.</param>
        ''' <returns></returns>
        Public Delegate Function HookCallbackFunction(code As Integer, wParam As IntPtr, lParam As IntPtr) As Integer

        ''' <summary>
        ''' General Hook hadling function, only raises event with hook's parameters.
        ''' </summary>
        ''' <param name="code">Specifies the hook code passed to the current hook procedure.</param>
        ''' <param name="wParam">Specifies the hook wParam passed to the current hook procedure.</param>
        ''' <param name="lParam">Specifies the hook lParam passed to the current hook procedure.</param>
        ''' <returns>The value returned by CallNextHookEx function</returns>
        Protected Function CoreHookProc(code As Integer, wParam As IntPtr, lParam As IntPtr) As Integer
            ' According to Platform SDK we must return the value returned by CallNextHookEx
            If code < 0 Then
                Return CallNextHookEx(hhook, code, wParam, lParam)
            End If

            ' Raise HookInvoked event
            Dim e As New HookEventArgs()
            e.HookCode = code
            e.wParam = wParam
            e.lParam = lParam
            RaiseHookInvoked(e)

            ' Call the next hook in the chain
            Return CallNextHookEx(hhook, code, wParam, lParam)
        End Function

        ''' <summary>
        ''' Insatalls the hook to the system's hook chain.
        ''' </summary>
        Public Sub Install()
            'hhook = SetWindowsHookEx(hookType, callbackFunc, IntPtr.Zero, CInt(System.Threading.Thread.CurrentThread.ManagedThreadId))
            hhook = SetWindowsHookEx(hookType, callbackFunc, IntPtr.Zero, CInt(AppDomain.GetCurrentThreadId()))
        End Sub

        ''' <summary>
        ''' Uninstalls the hook from the system's hook chain.
        ''' </summary>
        Public Sub Uninstall()
            UnhookWindowsHookEx(hhook)
            hhook = IntPtr.Zero
        End Sub

#Region "WinAPI Imports"
        ''' <summary>
        ''' The SetWindowsHookEx function installs an application-defined hook procedure into a hook chain.
        ''' </summary>
        ''' <param name="code">Specifies the type of hook procedure to be installed.</param>
        ''' <param name="func">Pointer to the hook procedure.</param>
        ''' <param name="hInstance">Handle to the DLL containing the hook procedure. The hMod parameter must be set to NULL if the dwThreadId parameter specifies a thread created by the current process.</param>
        ''' <param name="threadID">Specifies the identifier of the thread with which the hook procedure is to be associated.</param>
        ''' <returns>If the function succeeds, the return value is the handle to the hook procedure, otherwise returns NULL.</returns>
        <DllImport("user32.dll")> _
        Protected Shared Function SetWindowsHookEx(code As HookType, func As HookCallbackFunction, hInstance As IntPtr, threadID As Integer) As IntPtr
        End Function

        ''' <summary>
        ''' The UnhookWindowsHookEx function removes a hook procedure installed in a hook chain.
        ''' </summary>
        ''' <param name="hhook">Handle to the hook to be removed.</param>
        ''' <returns>If the function succeeds, the return value is nonzero, otherwise return zero.</returns>
        <DllImport("user32.dll")> _
        Protected Shared Function UnhookWindowsHookEx(hhook As IntPtr) As Integer
        End Function

        ''' <summary>
        ''' The CallNextHookEx function passes the hook information to the next hook procedure in the current hook chain.
        ''' </summary>
        ''' <param name="hhook">Ignored.</param>
        ''' <param name="code">Specifies the hook code passed to the current hook procedure. </param>
        ''' <param name="wParam">Specifies the wParam value passed to the current hook procedure.</param>
        ''' <param name="lParam">Specifies the lParam value passed to the current hook procedure.</param>
        ''' <returns>This value is returned by the next hook procedure in the chain. The current hook procedure must also return this value. The meaning of the return value depends on the hook type.</returns>
        <DllImport("user32.dll")> _
        Protected Shared Function CallNextHookEx(hhook As IntPtr, code As Integer, wParam As IntPtr, lParam As IntPtr) As Integer
        End Function

#End Region
    End Class
End Namespace
