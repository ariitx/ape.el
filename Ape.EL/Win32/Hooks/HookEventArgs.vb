
Imports System.Collections.Generic
Imports System.Text

Namespace Win32.Hooks
    ''' <summary>
    ''' Class containing the information about invoked hook.
    ''' </summary>
    Public Class HookEventArgs
        Inherits EventArgs
        Public HookCode As Integer
        Public wParam As IntPtr
        Public lParam As IntPtr
    End Class
End Namespace
