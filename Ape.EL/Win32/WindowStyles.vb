﻿Namespace Win32
    Public Class WindowStyles
        Public Shared ReadOnly WS_BORDER As Int32 = &H800000
        Public Shared ReadOnly WS_CAPTION As Int32 = &HC00000
        Public Shared ReadOnly WS_CHILD As Int32 = &H40000000
        Public Shared ReadOnly WS_CHILDWINDOW As Int32 = &H40000000
        Public Shared ReadOnly WS_CLIPCHILDREN As Int32 = &H2000000
        Public Shared ReadOnly WS_CLIPSIBLINGS As Int32 = &H4000000
        Public Shared ReadOnly WS_DISABLED As Int32 = &H8000000
        Public Shared ReadOnly WS_DLGFRAME As Int32 = &H400000
        Public Shared ReadOnly WS_GROUP As Int32 = &H20000
        Public Shared ReadOnly WS_HSCROLL As Int32 = &H100000
        Public Shared ReadOnly WS_ICONIC As Int32 = &H20000000
        Public Shared ReadOnly WS_MAXIMIZE As Int32 = &H1000000
        Public Shared ReadOnly WS_MAXIMIZEBOX As Int32 = &H10000
        Public Shared ReadOnly WS_MINIMIZE As Int32 = &H20000000
        Public Shared ReadOnly WS_MINIMIZEBOX As Int32 = &H20000
        Public Shared ReadOnly WS_OVERLAPPED As Int32 = &H0
        Public Shared ReadOnly WS_OVERLAPPEDWINDOW As Int32 = WS_OVERLAPPED Or WS_CAPTION Or WS_SYSMENU Or WS_THICKFRAME Or WS_MINIMIZEBOX Or WS_MAXIMIZEBOX
        Public Shared ReadOnly WS_POPUP As Int32 = &H80000000
        Public Shared ReadOnly WS_POPUPWINDOW As Int32 = WS_POPUP Or WS_BORDER Or WS_SYSMENU
        Public Shared ReadOnly WS_SIZEBOX As Int32 = &H40000
        Public Shared ReadOnly WS_SYSMENU As Int32 = &H80000
        Public Shared ReadOnly WS_TABSTOP As Int32 = &H10000
        Public Shared ReadOnly WS_THICKFRAME As Int32 = &H40000
        Public Shared ReadOnly WS_TILED As Int32 = &H0
        Public Shared ReadOnly WS_TILEDWINDOW As Int32 = WS_OVERLAPPED Or WS_CAPTION Or WS_SYSMENU Or WS_THICKFRAME Or WS_MINIMIZEBOX Or WS_MAXIMIZEBOX
        Public Shared ReadOnly WS_VISIBLE As Int32 = &H10000000
        Public Shared ReadOnly WS_VSCROLL As Int32 = &H200000
    End Class
End Namespace
