﻿Imports System
Imports System.Management

Namespace Win32
    <Obsolete("Have issue on Immediate Window and threading for ManagementObjectSearcher. Highly not recommended to use.")>
    Public Class WmiHelper

        Friend Shared Function GetProcessorId() As String
            Dim strProcessorId As String = String.Empty
            Dim query As New SelectQuery("Win32_processor")
            Dim search As New ManagementObjectSearcher(query)
            Dim info As ManagementObject

            For Each info In search.Get()
                strProcessorId = info("processorId").ToString()
            Next
            Return strProcessorId

        End Function

        Friend Shared Function GetMACAddress() As String

            Dim mc As ManagementClass = New ManagementClass("Win32_NetworkAdapterConfiguration")
            Dim moc As ManagementObjectCollection = mc.GetInstances()
            Dim MACAddress As String = String.Empty
            For Each mo As ManagementObject In moc

                If (MACAddress.Equals(String.Empty)) Then
                    If CBool(mo("IPEnabled")) Then MACAddress = mo("MacAddress").ToString()

                    mo.Dispose()
                End If
                MACAddress = MACAddress.Replace(":", String.Empty)

            Next
            Return MACAddress
        End Function

        Friend Shared Function GetVolumeSerial(Optional ByVal strDriveLetter As String = "C") As String

            Dim disk As ManagementObject = New ManagementObject(String.Format("win32_logicaldisk.deviceid=""{0}:""", strDriveLetter))
            disk.Get()
            Return disk("VolumeSerialNumber").ToString()
        End Function

        Friend Shared Function GetMotherBoardID() As String

            Dim strMotherBoardID As String = String.Empty
            Dim query As New SelectQuery("Win32_BaseBoard")
            Dim search As New ManagementObjectSearcher(query)
            Dim info As ManagementObject
            For Each info In search.Get()

                strMotherBoardID = info("SerialNumber").ToString()

            Next
            Return strMotherBoardID

        End Function

        ''' <summary>
        ''' Get datatable from a WMI query.
        ''' </summary>
        Public Shared Function WMIQuery(query As String) As DataTable
            Dim qry As New SelectQuery(query)

            Dim ds As New DataSet()
            Dim Table As DataTable = ds.Tables.Add("WMI")

            Dim ms As New ManagementObjectSearcher(qry)
            Dim mc As ManagementObjectCollection = ms.[Get]()

            Dim loaded As [Boolean] = False

            For Each mo As ManagementObject In ms.[Get]()
                If Not loaded Then
                    For Each prop As PropertyData In mo.Properties
                        Table.Columns.Add(prop.Name)
                    Next
                    loaded = True
                End If
                Dim row As DataRow = Table.NewRow()
                For Each prop As PropertyData In mo.Properties
                    row(prop.Name) = prop.Value
                Next
                Table.Rows.Add(row)
            Next

            Return Table
        End Function

    End Class
End Namespace
