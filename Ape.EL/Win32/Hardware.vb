﻿Imports System
Imports System.ComponentModel
Imports System.Runtime.InteropServices

Namespace Win32
    Public Class Hardware
        ' Methods
        Shared Sub New()
            Hardware.lastInPutNfo = New LASTINPUTINFO
            Hardware.lastInPutNfo.cbSize = Marshal.SizeOf(Hardware.lastInPutNfo)
            Return
        End Sub

        Public Sub New()
            MyBase.New()
            Return
        End Sub

        <DllImport("User32.dll")> _
        Private Shared Function GetLastInputInfo(ByRef plii As LASTINPUTINFO) As Boolean
        End Function


        ' Properties
        Public Shared ReadOnly Property LastInputDateTime As DateTime
            Get
                Return DateTime.Now.AddMilliseconds(CDbl(-(Environment.TickCount - Hardware.LastInputTicks)))
            End Get
        End Property

        Public Shared ReadOnly Property LastInputTicks As Integer
            Get
                If (Not Hardware.GetLastInputInfo(Hardware.lastInPutNfo) = Nothing) Then
                    GoTo Label_0017
                End If
                Throw New Win32Exception(Marshal.GetLastWin32Error)
            Label_0017:
                Return Hardware.lastInPutNfo.dwTime
            End Get
        End Property


        ' Fields
        Private Shared lastInPutNfo As LASTINPUTINFO

        ' Nested Types
        <StructLayout(LayoutKind.Sequential)> _
        Private Structure LASTINPUTINFO
            Public cbSize As UInt32
            Public dwTime As UInt32
        End Structure
    End Class
End Namespace

