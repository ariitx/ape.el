﻿Imports System
Imports System.Drawing
Imports System.Runtime.InteropServices
Imports System.Windows.Forms
Imports Ape.EL.Win32
Imports Ape.EL.Win32.User32
Imports Ape.EL.Win32.WndProcHelper.Msg

Namespace Win32
    Public Class Helper
        Public Shared Function SetUniqueWindowCaption(title As String, aForm As Form) As String
            Dim text As String = title
            Dim num As Integer = 1
            Dim flag As Boolean
            Do
                flag = False
                For i As Integer = 0 To System.Windows.Forms.Application.OpenForms.Count - 1
                    Dim form As Form = System.Windows.Forms.Application.OpenForms(i)
                    If Not (form.Name = "FormLaunchThreadForm") AndAlso aForm IsNot form AndAlso Not form.IsDisposed AndAlso form.Visible AndAlso form.Text = text Then
                        Dim array As Object() = New Object(4 - 1) {}
                        array(0) = title
                        array(1) = "("
                        Dim arg_76_0 As Object() = array
                        Dim arg_76_1 As Integer = 2
                        Dim expr_6D As Integer = num
                        num = expr_6D + 1
                        arg_76_0(arg_76_1) = expr_6D
                        array(3) = ")"
                        text = String.Concat(array)
                        flag = True
                        Exit For
                    End If
                Next
            Loop While flag
            Return text
        End Function

        ''' <summary>
        ''' Get text of a control by pointer.
        ''' </summary>
        Public Shared Function GetControlText(hWnd As IntPtr) As String
            Try
                Dim title As New System.Text.StringBuilder(65535)

                ' Get the size of the string required to hold the window title. 
                Dim size As Int32 = SendMessage(CInt(hWnd), WM_GETTEXTLENGTH, 0, 0)

                ' If the return is 0, there is no title. 
                If size > 0 Then
                    SendMessage(hWnd, WM_GETTEXT, title.Capacity, title)
                End If
                Return title.ToString()
            Catch ex As Exception
                Return String.Empty
            End Try
        End Function

        Public Shared Function CaptureScreen() As Bitmap
            Dim result As Bitmap
            Try
                Dim windowDC As IntPtr = User32.GetWindowDC(User32.GetDesktopWindow())
                Dim intPtr As IntPtr = Gdi32.CreateCompatibleDC(windowDC)
                Dim intPtr2 As IntPtr = Gdi32.CreateCompatibleBitmap(windowDC, Gdi32.GetDeviceCaps(windowDC, 8), Gdi32.GetDeviceCaps(windowDC, 10))
                Try
                    Gdi32.SelectObject(intPtr, intPtr2)
                    Gdi32.BitBlt(intPtr, 0, 0, Gdi32.GetDeviceCaps(windowDC, 8), Gdi32.GetDeviceCaps(windowDC, 10), windowDC, 0, 0, 13369376)
                    Dim bitmap As Bitmap = New Bitmap(Image.FromHbitmap(intPtr2), Image.FromHbitmap(intPtr2).Width, Image.FromHbitmap(intPtr2).Height)
                    result = bitmap
                Finally
                    Helper.Cleanup(intPtr2, windowDC, intPtr)
                End Try
            Catch ex_80 As Exception
                result = Nothing
            End Try
            Return result
        End Function

        Private Shared Sub Cleanup(hBitmap As IntPtr, hdcSrc As IntPtr, hdcDest As IntPtr)
            User32.ReleaseDC(User32.GetDesktopWindow(), hdcSrc)
            Gdi32.DeleteDC(hdcDest)
            Gdi32.DeleteObject(hBitmap)
        End Sub

        Public Shared Function Is64BitOperatingSystem() As Boolean
            Dim flag As Boolean
            Return IntPtr.Size = 8 OrElse (Helper.DoesWin32MethodExist("kernel32.dll", "IsWow64Process") AndAlso Helper.IsWow64Process(Helper.GetCurrentProcess(), flag) AndAlso flag)
        End Function

        Private Shared Function DoesWin32MethodExist(moduleName As String, methodName As String) As Boolean
            Dim moduleHandle As IntPtr = Helper.GetModuleHandle(moduleName)
            Return Not (moduleHandle = IntPtr.Zero) AndAlso Helper.GetProcAddress(moduleHandle, methodName) <> IntPtr.Zero
        End Function

        Private Declare Function GetCurrentProcess Lib "kernel32.dll" () As IntPtr

        Private Declare Auto Function GetModuleHandle Lib "kernel32.dll" (moduleName As String) As IntPtr

        Private Declare Auto Function GetProcAddress Lib "kernel32" (hModule As IntPtr, <MarshalAs(UnmanagedType.LPStr)> procName As String) As IntPtr

        Private Declare Auto Function IsWow64Process Lib "kernel32.dll" (hProcess As IntPtr, <Out()> ByRef wow64Process As Boolean) As <MarshalAs(UnmanagedType.Bool)>
        Boolean
    End Class
End Namespace
