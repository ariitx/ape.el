﻿Imports System
Imports System.Runtime.InteropServices
Imports System.Text

Namespace Win32
    Public Class Win32Native
        ' Methods
        <DllImport("kernel32.dll", CharSet:=CharSet.Auto)> _
        Friend Shared Function FormatMessage(ByVal dwFlags As Integer, ByVal lpSource As IntPtr, ByVal dwMessageId As Integer, ByVal dwLanguageId As Integer, <Out> ByVal lpBuffer As StringBuilder, ByVal nSize As Integer, ByVal va_list_arguments As IntPtr) As Integer
        End Function

        Friend Shared Function GetMessage(ByVal errorCode As Integer) As String
            Dim builder As StringBuilder
            builder = New StringBuilder(&H200)
            If (Win32Native.FormatMessage(&H3200, IntPtr.Zero, errorCode, 0, builder, builder.Capacity, IntPtr.Zero) = Nothing) Then
                GoTo Label_0033
            End If
            Return builder.ToString
Label_0033:
            Return String.Format("Unknown error code: {0}.", CInt(errorCode))
        End Function

        <DllImport("kernel32.dll", CharSet:=CharSet.Auto, SetLastError:=True)> _
        Friend Shared Function GetTempFileName(ByVal tmpPath As String, ByVal prefix As String, ByVal uniqueIdOrZero As UInt32, <Out> ByVal tmpFileName As StringBuilder) As UInt32
        End Function

        Friend Shared Function MakeHRFromErrorCode(ByVal errorCode As Integer) As Integer
            Return (-2147024896 Or errorCode)
        End Function

    End Class
End Namespace

