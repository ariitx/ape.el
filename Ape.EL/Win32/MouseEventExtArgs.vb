﻿Imports System
Imports System.Windows.Forms

Namespace Win32
    Public Class MouseEventExtArgs
        Inherits MouseEventArgs
        ' Methods
        Friend Sub New(ByVal e As MouseEventArgs)
            MyBase.New(e.Button, e.Clicks, e.X, e.Y, e.Delta)
            Return
        End Sub

        Public Sub New(ByVal buttons As MouseButtons, ByVal clicks As Integer, ByVal x As Integer, ByVal y As Integer, ByVal delta As Integer)
            MyBase.New(buttons, clicks, x, y, delta)
            Return
        End Sub


        ' Properties
        Public Property Handled As Boolean
            Get
                Return Me.m_Handled
            End Get
            Set(ByVal value As Boolean)
                Me.m_Handled = value
                Return
            End Set
        End Property


        ' Fields
        Private m_Handled As Boolean
    End Class
End Namespace

