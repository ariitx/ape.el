﻿Namespace Win32.Wmi
    Public Enum UserAccount
        Win32_SystemUsers
        Win32_Account
        Win32_AccountSID
        Win32_SecurityDescriptor
        Win32_SecuritySetting
        Win32_SecuritySettingAccess
        Win32_SecuritySettingAuditing
        Win32_SecuritySettingGroup
        Win32_SecuritySettingOfLogicalFile
        Win32_SecuritySettingOfLogicalShare
        Win32_SecuritySettingOfObject
        Win32_SecuritySettingOwner
        Win32_NTEventlogFile
        Win32_NTLogEvent
        Win32_NTLogEventComputer
        Win32_NTLogEventLog
        Win32_NTLogEventUser
    End Enum
End Namespace
