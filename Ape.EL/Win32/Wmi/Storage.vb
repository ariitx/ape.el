﻿Namespace Win32.Wmi
    Public Enum Storage
        Win32_DiskDrive
        Win32_DiskDriveToDiskPartition
        Win32_DiskPartition
        Win32_LogicalDisk
        Win32_LogicalDiskRootDirectory
        Win32_LogicalDiskToPartition
        Win32_LogicalFileAccess
        Win32_LogicalFileAuditing
        Win32_LogicalFileGroup
        Win32_LogicalFileOwner
        Win32_LogicalFileSecuritySetting
        Win32_TapeDrive
    End Enum
End Namespace
