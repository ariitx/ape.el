﻿Namespace Win32.Wmi
    Public Enum Network
        Win32_NetworkAdapter
        Win32_NetworkAdapterConfiguration
        Win32_NetworkAdapterSetting
        Win32_NetworkClient
        Win32_NetworkConnection
        Win32_NetworkLoginProfile
        Win32_NetworkProtocol
        Win32_PerfRawData_Tcpip_ICMP
        Win32_PerfRawData_Tcpip_IP
        Win32_PerfRawData_Tcpip_NBTConnection
        Win32_PerfRawData_Tcpip_NetworkInterface
        Win32_PerfRawData_Tcpip_TCP
        Win32_PerfRawData_Tcpip_UDP
        Win32_PerfRawData_W3SVC_WebService
        Win32_SystemNetworkConnections
    End Enum
End Namespace
