﻿Namespace Win32.Wmi
    Public Enum Memory
        Win32_CacheMemory
        Win32_MemoryArray
        Win32_MemoryArrayLocation
        Win32_MemoryDevice
        Win32_MemoryDeviceArray
        Win32_MemoryDeviceLocation
        Win32_AssociatedProcessorMemory
        Win32_DeviceMemoryAddress
        Win32_LogicalMemoryConfiguration
        Win32_PerfRawData_PerfOS_Memory
        Win32_PhysicalMemory
        Win32_PhysicalMemoryArray
        Win32_PhysicalMemoryLocation
        Win32_SMBIOSMemory
        Win32_SystemLogicalMemoryConfiguration
        Win32_SystemMemoryResource
    End Enum
End Namespace
