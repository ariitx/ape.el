﻿Imports System.Runtime.InteropServices

Namespace Win32
    Public Class WindowStylesHelper
        'For 64 bit
        Private Declare Function SetWindowLongPtr Lib "user32" Alias "SetWindowLongPtrA" _
            (ByVal hWnd As Long, _
             ByVal nIndex As WindowLongFlags, _
             ByVal dwNewLong As Long) As Long

        Private Declare Function GetWindowLongPtr Lib "user32" Alias "GetWindowLongPtrA" _
            (ByVal hWnd As Long, _
             ByVal nIndex As WindowLongFlags
            ) As Long

        'For 32 bit
        <DllImport("user32.dll")> _
        Private Shared Function SetWindowLong(hWnd As IntPtr, _
            <MarshalAs(UnmanagedType.I4)> nIndex As WindowLongFlags, _
            dwNewLong As IntPtr) As Integer
        End Function

        <DllImport("user32.dll", SetLastError:=True)> _
        Private Shared Function GetWindowLong(hWnd As IntPtr, _
            <MarshalAs(UnmanagedType.I4)> nIndex As WindowLongFlags) As Integer
        End Function

        'For general use
        Public Shared Function SetWindowLongEx _
            (ByVal hWnd As Long, _
             ByVal nIndex As WindowLongFlags, _
             ByVal dwNewLong As Long) As Long

            If Utility.General.Is64Proc Then
                Return SetWindowLongPtr(hWnd, nIndex, dwNewLong)
            Else
                Return SetWindowLong(hWnd, nIndex, dwNewLong)
            End If
        End Function

        Public Shared Function GetWindowLongEx _
            (ByVal hWnd As Long, _
             ByVal nIndex As WindowLongFlags
            ) As Long

            If Utility.General.Is64Proc Then
                Return GetWindowLongPtr(hWnd, nIndex)
            Else
                Return GetWindowLong(hWnd, nIndex)
            End If
        End Function

        <DllImport("User32.dll")> _
        Public Shared Function SetWindowPos(hWnd As IntPtr, hWndInsertAfter As IntPtr, X As Integer, Y As Integer, cx As Integer, cy As Integer, uFlags As UInteger) As Boolean
        End Function
    End Class
End Namespace

