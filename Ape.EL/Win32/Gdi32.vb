﻿
Imports System
Imports System.Runtime.InteropServices
Imports System.Reflection

Namespace Win32
    Public Class Gdi32
        <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi)> _
        Public Structure RAMP
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=256)> Public Red As UInt16()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=256)> Public Green As UInt16()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=256)> Public Blue As UInt16()
        End Structure

        Public Const SRCCOPY As Integer = &HCC0020

        Public Declare Function BitBlt Lib "GDI32.dll" (hdcDest As IntPtr, nXDest As Integer, nYDest As Integer, nWidth As Integer, nHeight As Integer, hdcSrc As IntPtr, nXSrc As Integer, nYSrc As Integer, dwRop As Integer) As Boolean

        Public Declare Function CreateCompatibleBitmap Lib "GDI32.dll" (hdc As IntPtr, nWidth As Integer, nHeight As Integer) As IntPtr

        Public Declare Function CreateCompatibleDC Lib "GDI32.dll" (hdc As IntPtr) As IntPtr

        Public Declare Function DeleteDC Lib "GDI32.dll" (hdc As IntPtr) As Boolean

        Public Declare Function DeleteObject Lib "GDI32.dll" (hObject As IntPtr) As Boolean

        Public Declare Function GetDeviceCaps Lib "GDI32.dll" (hdc As IntPtr, nIndex As Integer) As Integer

        Public Declare Function SelectObject Lib "GDI32.dll" (hdc As IntPtr, hgdiobj As IntPtr) As IntPtr

        Public Declare Function CreateRoundRectRgn Lib "GDI32.dll" (nLeftRect As Integer, nTopRect As Integer, nRightRect As Integer, nBottomRect As Integer, nWidthEllipse As Integer, nHeightEllipse As Integer) As IntPtr

        <DllImport("gdi32.dll")> _
        Public Shared Function SetDeviceGammaRamp(ByVal hDC As IntPtr, ByRef lpRamp As RAMP) As Boolean
        End Function

        <DllImport("gdi32.dll")> _
        Public Shared Function GetDeviceGammaRamp(ByVal hdc As Int32, ByRef lpv As RAMP) As Boolean
        End Function
    End Class
End Namespace
