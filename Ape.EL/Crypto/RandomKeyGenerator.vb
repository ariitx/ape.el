﻿
Imports Microsoft.VisualBasic
Imports Microsoft.VisualBasic.CompilerServices
Imports System
Imports System.Diagnostics
Imports System.Text

Namespace Crypto
    Public Class RandomKeyGenerator
        Private string_0 As String

        Private string_1 As String

        Private int_0 As Integer

        Private char_0 As Char()

        Private char_1 As Char()

        Protected Friend WriteOnly Property KeyLetters() As String
            Set(value As String)
                Me.string_0 = value
            End Set
        End Property

        Protected Friend WriteOnly Property KeyNumbers() As String
            Set(value As String)
                Me.string_1 = value
            End Set
        End Property

        Protected Friend WriteOnly Property KeyChars() As Integer
            Set(value As Integer)
                Me.int_0 = value
            End Set
        End Property

        <DebuggerNonUserCode()>
        Public Sub New()
        End Sub

        Public Function Generate() As String
            Dim stringBuilder As StringBuilder = New StringBuilder()
            Me.char_0 = Me.string_0.ToCharArray()
            Me.char_1 = Me.string_1.ToCharArray()
            Dim arg_30_0 As Integer = 1
            Dim num As Integer = Me.int_0
            Dim num2 As Integer = arg_30_0
            ' The following expression was wrapped in a checked-statement
            While True
                Dim arg_108_0 As Integer = num2
                Dim num3 As Integer = num
                If arg_108_0 > num3 Then
                    Exit While
                End If
                VBMath.Randomize()
                Dim num4 As Single = VBMath.Rnd()
                Dim num5 As Short = -1
                ' The following expression was wrapped in a unchecked-expression
                If CInt(Math.Round(CDec(num4 * 111.0F))) Mod 2 = 0 Then
                    While num5 < 0
                        ' The following expression was wrapped in a unchecked-expression
                        num5 = Convert.ToInt16(CSng(Me.char_0.GetUpperBound(0)) * num4)
                    End While
                    Dim text As String = Conversions.ToString(Me.char_0(CInt(num5)))
                    ' The following expression was wrapped in a unchecked-expression
                    If CInt(Math.Round(CDec(CSng(num5) * num4 * 99.0F))) Mod 2 <> 0 Then
                        text = Me.char_0(CInt(num5)).ToString()
                        text = text.ToUpper()
                    End If
                    stringBuilder.Append(text)
                Else
                    While num5 < 0
                        ' The following expression was wrapped in a unchecked-expression
                        num5 = Convert.ToInt16(CSng(Me.char_1.GetUpperBound(0)) * num4)
                    End While
                    stringBuilder.Append(Me.char_1(CInt(num5)))
                End If
                num2 += 1
            End While
            Return stringBuilder.ToString()
        End Function
    End Class
End Namespace
