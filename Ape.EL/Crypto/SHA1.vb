﻿
Imports System
Imports System.Diagnostics
Imports System.IO
Imports System.Security.Cryptography
Imports System.Text

Namespace Crypto
    Public Class SHA1
        Implements SHA1.Interface0

        Private Interface Interface0
            Function hashOfString(stringToHash As String) As String

            Function hashOfFile(fileToHash As String) As String
        End Interface

        <DebuggerNonUserCode()>
        Public Sub New()
        End Sub

        Public Function hashOfFile(fileToHash As String) As String Implements Interface0.hashOfFile
            Dim sHA1CryptoServiceProvider As SHA1CryptoServiceProvider = New SHA1CryptoServiceProvider()
            Dim result As String = String.Empty
            If File.Exists(fileToHash) Then
                Dim inputStream As FileStream = New FileStream(fileToHash, FileMode.Open, FileAccess.Read)
                Dim byte_ As Byte() = sHA1CryptoServiceProvider.ComputeHash(inputStream)
                result = Me.method_0(byte_)
            End If
            Return result
        End Function

        Public Function hashOfString(stringToHash As String) As String Implements Interface0.hashOfString
            Dim sHA1CryptoServiceProvider As SHA1CryptoServiceProvider = New SHA1CryptoServiceProvider()
            Dim array As Byte() = Encoding.ASCII.GetBytes(stringToHash)
            array = sHA1CryptoServiceProvider.ComputeHash(array)
            Dim text As String = String.Empty
            Dim array2 As Byte() = array
            ' The following expression was wrapped in a checked-statement
            For i As Integer = 0 To array2.Length - 1
                Dim b As Byte = array2(i)
                text += b.ToString("x2")
            Next
            Return text
        End Function

        Private Function method_0(byte_0 As Byte()) As String
            ' The following expression was wrapped in a checked-statement
            Dim stringBuilder As StringBuilder = New StringBuilder(byte_0.Length * 2)
            Dim arg_12_0 As Integer = 0
            Dim num As Integer = byte_0.Length - 1
            Dim num2 As Integer = arg_12_0
            While True
                Dim arg_37_0 As Integer = num2
                Dim num3 As Integer = num
                If arg_37_0 > num3 Then
                    Exit While
                End If
                stringBuilder.Append(byte_0(num2).ToString("X2"))
                num2 += 1
            End While
            Return stringBuilder.ToString().ToLower()
        End Function
    End Class
End Namespace
