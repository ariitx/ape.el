﻿
Imports Microsoft.VisualBasic.CompilerServices
Imports System
Imports System.Diagnostics
Imports System.IO
Imports System.Security.Cryptography
Imports System.Text

Namespace Crypto
    Public Class Rijndael256
        <DebuggerNonUserCode()>
        Public Sub New()
        End Sub

        Public Function iEncryptString256Bit(plainText As String, passPhrase As String, Optional saltValue As String = "5534ESDSFRYafsdgSEg5534Z634G6feXdgfZFGBdRT46T6RdfsgbhDssjr", Optional keySize As Integer = 256, Optional passwordIterations As Integer = 7321, Optional initVector As String = "NMB2GMA4154887H8", Optional hashAlgorithm As String = "SHA1") As String
            Return Rijndael256.EncryptString256Bit(plainText, passPhrase, saltValue, keySize, passwordIterations, initVector, hashAlgorithm)
        End Function

        Public Function iDecryptString256Bit(cipherText As String, passPhrase As String, Optional saltValue As String = "5534ESDSFRYafsdgSEg5534Z634G6feXdgfZFGBdRT46T6RdfsgbhDssjr", Optional keySize As Integer = 256, Optional passwordIterations As Integer = 7321, Optional initVector As String = "NMB2GMA4154887H8", Optional hashAlgorithm As String = "SHA1") As String
            Return Rijndael256.DecryptString256Bit(cipherText, passPhrase, saltValue, keySize, passwordIterations, initVector, hashAlgorithm)
        End Function

        Public Function iEncryptBytes256Bit(plainBytes As Byte(), passPhrase As String, Optional saltValue As String = "5534ESDSFRYafsdgSEg5534Z634G6feXdgfZFGBdRT46T6RdfsgbhDssjr", Optional keySize As Integer = 256, Optional passwordIterations As Integer = 7321, Optional initVector As String = "NMB2GMA4154887H8", Optional hashAlgorithm As String = "SHA1", Optional ReturnBase64String As Boolean = True) As Object
            Return Rijndael256.EncryptBytes256Bit(plainBytes, passPhrase, saltValue, keySize, passwordIterations, initVector, hashAlgorithm, ReturnBase64String)
        End Function

        Public Function iDecryptBytes256Bit(cipherText As String, passPhrase As String, Optional saltValue As String = "5534ESDSFRYafsdgSEg5534Z634G6feXdgfZFGBdRT46T6RdfsgbhDssjr", Optional keySize As Integer = 256, Optional passwordIterations As Integer = 7321, Optional initVector As String = "NMB2GMA4154887H8", Optional hashAlgorithm As String = "SHA1") As Byte()
            Return Rijndael256.DecryptBytes256Bit(cipherText, passPhrase, saltValue, keySize, passwordIterations, initVector, hashAlgorithm)
        End Function

        Public Function iDecryptBytes256Bit(cipherBytes As Byte(), passPhrase As String, Optional saltValue As String = "5534ESDSFRYafsdgSEg5534Z634G6feXdgfZFGBdRT46T6RdfsgbhDssjr", Optional keySize As Integer = 256, Optional passwordIterations As Integer = 7321, Optional initVector As String = "NMB2GMA4154887H8", Optional hashAlgorithm As String = "SHA1") As Byte()
            Return Rijndael256.DecryptBytes256Bit(cipherBytes, passPhrase, saltValue, keySize, passwordIterations, initVector, hashAlgorithm)
        End Function

        Public Shared Function EncryptString256Bit(plainText As String, passPhrase As String, Optional saltValue As String = "5534ESDSFRYafsdgSEg5534Z634G6feXdgfZFGBdRT46T6RdfsgbhDssjr", Optional keySize As Integer = 256, Optional passwordIterations As Integer = 7321, Optional initVector As String = "NMB2GMA4154887H8", Optional hashAlgorithm As String = "SHA1") As String
            Dim bytes As Byte() = Encoding.ASCII.GetBytes(initVector)
            Dim bytes2 As Byte() = Encoding.ASCII.GetBytes(saltValue)
            Dim bytes3 As Byte() = Encoding.UTF8.GetBytes(plainText)
            Dim rfc2898DeriveBytes As Rfc2898DeriveBytes = New Rfc2898DeriveBytes(passPhrase, bytes2, passwordIterations)
            ' The following expression was wrapped in a checked-expression
            Dim bytes4 As Byte() = rfc2898DeriveBytes.GetBytes(CInt(Math.Round(CDec(keySize) / 8.0)))
            Dim transform As ICryptoTransform = New RijndaelManaged() With {.Mode = CipherMode.CBC}.CreateEncryptor(bytes4, bytes)
            Dim memoryStream As MemoryStream = New MemoryStream()
            Dim cryptoStream As CryptoStream = New CryptoStream(memoryStream, transform, CryptoStreamMode.Write)
            cryptoStream.Write(bytes3, 0, bytes3.Length)
            cryptoStream.FlushFinalBlock()
            Dim inArray As Byte() = memoryStream.ToArray()
            memoryStream.Close()
            cryptoStream.Close()
            Return Convert.ToBase64String(inArray)
        End Function

        Public Shared Function DecryptString256Bit(cipherText As String, passPhrase As String, Optional saltValue As String = "5534ESDSFRYafsdgSEg5534Z634G6feXdgfZFGBdRT46T6RdfsgbhDssjr", Optional keySize As Integer = 256, Optional passwordIterations As Integer = 7321, Optional initVector As String = "NMB2GMA4154887H8", Optional hashAlgorithm As String = "SHA1") As String
            cipherText = cipherText.Replace(" ", "+")
            Dim bytes As Byte() = Encoding.ASCII.GetBytes(initVector)
            Dim bytes2 As Byte() = Encoding.ASCII.GetBytes(saltValue)
            Dim array As Byte() = Convert.FromBase64String(cipherText)
            Dim rfc2898DeriveBytes As Rfc2898DeriveBytes = New Rfc2898DeriveBytes(passPhrase, bytes2, passwordIterations)
            ' The following expression was wrapped in a checked-statement
            Dim bytes3 As Byte() = rfc2898DeriveBytes.GetBytes(CInt(Math.Round(CDec(keySize) / 8.0)))
            Dim transform As ICryptoTransform = New RijndaelManaged() With {.Mode = CipherMode.CBC}.CreateDecryptor(bytes3, bytes)
            Dim memoryStream As MemoryStream = New MemoryStream(array)
            Dim cryptoStream As CryptoStream = New CryptoStream(memoryStream, transform, CryptoStreamMode.Read)
            Dim array2 As Byte() = New Byte(array.Length + 1 - 1) {}
            Dim count As Integer = cryptoStream.Read(array2, 0, array2.Length)
            memoryStream.Close()
            cryptoStream.Close()
            Return Encoding.UTF8.GetString(array2, 0, count)
        End Function

        Public Shared Function EncryptBytes256Bit(plainBytes As Byte(), passPhrase As String, Optional saltValue As String = "5534ESDSFRYafsdgSEg5534Z634G6feXdgfZFGBdRT46T6RdfsgbhDssjr", Optional keySize As Integer = 256, Optional passwordIterations As Integer = 7321, Optional initVector As String = "NMB2GMA4154887H8", Optional hashAlgorithm As String = "SHA1", Optional ReturnBase64String As Boolean = True) As Object
            Dim bytes As Byte() = Encoding.ASCII.GetBytes(initVector)
            Dim bytes2 As Byte() = Encoding.ASCII.GetBytes(saltValue)
            Dim rfc2898DeriveBytes As Rfc2898DeriveBytes = New Rfc2898DeriveBytes(passPhrase, bytes2, passwordIterations)
            ' The following expression was wrapped in a checked-expression
            Dim bytes3 As Byte() = rfc2898DeriveBytes.GetBytes(CInt(Math.Round(CDec(keySize) / 8.0)))
            Dim transform As ICryptoTransform = New RijndaelManaged() With {.Mode = CipherMode.CBC}.CreateEncryptor(bytes3, bytes)
            Dim memoryStream As MemoryStream = New MemoryStream()
            Dim cryptoStream As CryptoStream = New CryptoStream(memoryStream, transform, CryptoStreamMode.Write)
            cryptoStream.Write(plainBytes, 0, plainBytes.Length)
            cryptoStream.FlushFinalBlock()
            Dim array As Byte() = memoryStream.ToArray()
            memoryStream.Close()
            cryptoStream.Close()
            Dim text As String = Convert.ToBase64String(array)
            Dim result As Object
            If ReturnBase64String Then
                result = text
            Else
                result = array
            End If
            Return result
        End Function

        Public Shared Function DecryptBytes256Bit(cipherText As String, passPhrase As String, Optional saltValue As String = "5534ESDSFRYafsdgSEg5534Z634G6feXdgfZFGBdRT46T6RdfsgbhDssjr", Optional keySize As Integer = 256, Optional passwordIterations As Integer = 7321, Optional initVector As String = "NMB2GMA4154887H8", Optional hashAlgorithm As String = "SHA1") As Byte()
            cipherText = cipherText.Replace(" ", "+")
            Dim bytes As Byte() = Encoding.ASCII.GetBytes(initVector)
            Dim bytes2 As Byte() = Encoding.ASCII.GetBytes(saltValue)
            Dim array As Byte() = Convert.FromBase64String(cipherText)
            Dim rfc2898DeriveBytes As Rfc2898DeriveBytes = New Rfc2898DeriveBytes(passPhrase, bytes2, passwordIterations)
            ' The following expression was wrapped in a checked-statement
            Dim bytes3 As Byte() = rfc2898DeriveBytes.GetBytes(CInt(Math.Round(CDec(keySize) / 8.0)))
            Dim transform As ICryptoTransform = New RijndaelManaged() With {.Mode = CipherMode.CBC}.CreateDecryptor(bytes3, bytes)
            Dim memoryStream As MemoryStream = New MemoryStream(array)
            Dim cryptoStream As CryptoStream = New CryptoStream(memoryStream, transform, CryptoStreamMode.Read)
            Dim array2 As Byte() = New Byte(array.Length + 1 - 1) {}
            Dim num As Integer = cryptoStream.Read(array2, 0, array2.Length)
            memoryStream.Close()
            cryptoStream.Close()
            Encoding.UTF8.GetString(array2, 0, num)
            Return CType(Utils.CopyArray(CType(array2, Array), New Byte(num - 1 + 1 - 1) {}), Byte())
        End Function

        Public Shared Function DecryptBytes256Bit(cipherBytes As Byte(), passPhrase As String, Optional saltValue As String = "5534ESDSFRYafsdgSEg5534Z634G6feXdgfZFGBdRT46T6RdfsgbhDssjr", Optional keySize As Integer = 256, Optional passwordIterations As Integer = 7321, Optional initVector As String = "NMB2GMA4154887H8", Optional hashAlgorithm As String = "SHA1") As Byte()
            Dim bytes As Byte() = Encoding.ASCII.GetBytes(initVector)
            Dim bytes2 As Byte() = Encoding.ASCII.GetBytes(saltValue)
            Dim rfc2898DeriveBytes As Rfc2898DeriveBytes = New Rfc2898DeriveBytes(passPhrase, bytes2, passwordIterations)
            ' The following expression was wrapped in a checked-statement
            Dim bytes3 As Byte() = rfc2898DeriveBytes.GetBytes(CInt(Math.Round(CDec(keySize) / 8.0)))
            Dim transform As ICryptoTransform = New RijndaelManaged() With {.Mode = CipherMode.CBC}.CreateDecryptor(bytes3, bytes)
            Dim memoryStream As MemoryStream = New MemoryStream(cipherBytes)
            Dim cryptoStream As CryptoStream = New CryptoStream(memoryStream, transform, CryptoStreamMode.Read)
            Dim array As Byte() = New Byte(cipherBytes.Length + 1 - 1) {}
            Dim num As Integer = cryptoStream.Read(array, 0, array.Length)
            memoryStream.Close()
            cryptoStream.Close()
            Encoding.UTF8.GetString(array, 0, num)
            Return CType(Utils.CopyArray(CType(array, Array), New Byte(num - 1 + 1 - 1) {}), Byte())
        End Function
    End Class
End Namespace
