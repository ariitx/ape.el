﻿
Imports System.Linq
Imports System.ComponentModel
Imports System.Reflection
Imports Ape.EL.Internal

Namespace Licensing
    ''' <summary>
    ''' License granted to components.
    ''' </summary>
    Friend Class ComponentLicense
        Inherits License
        Implements IDisposable

#Region "Fields"

        Private _licKey As String = String.Empty
        Private _isDemo As Boolean = False
        Private _disposed As Boolean = False

#End Region

#Region "Properties"

        ''' <summary>
        ''' Gets the license key granted to this component.
        ''' </summary>
        Public Overrides ReadOnly Property LicenseKey() As String
            Get
                Return _licKey
            End Get
        End Property

        ''' <summary>
        ''' Gets if this component is running in demo mode.
        ''' </summary>
        Public ReadOnly Property IsDemo() As Boolean
            Get
                Return _isDemo
            End Get
        End Property

#End Region

#Region "Construction / Deconstruction"

        ''' <summary>
        ''' Creates a new <see cref="ComponentLicense"/> object.
        ''' </summary>
        ''' <param name="licKey">License key to use.</param>
        Private Sub New(licKey As String)
            _licKey = licKey

            If Not VerifyKey() Then
                _isDemo = True
                MsgBox("DEMO")
            End If
        End Sub

        ''' <summary>
        ''' Disposes this object.
        ''' </summary>
        Public Overrides Sub Dispose()
            Dispose(True)
        End Sub

        ''' <summary>
        ''' Disposes this object.
        ''' </summary>
        ''' <param name="disposing">true if the object is disposing.</param>
        Public Overloads Sub Dispose(disposing As Boolean)
            If disposing Then
                'Custom disposing here.
                If Not _disposed Then
                End If
                _disposed = True
            End If
        End Sub

#End Region

#Region "Public Methods"

        ''' <summary>
        ''' Creates a demo <see cref="ComponentLicense"/>.
        ''' </summary>
        ''' <returns>A demo <see cref="ComponentLicense"/>.</returns>
        Friend Shared Function CreateDemoLicense() As ComponentLicense
            Return New ComponentLicense(String.Empty)
        End Function

        ''' <summary>
        ''' Attempts to create a new <see cref="ComponentLicense"/> with the specified key.
        ''' </summary>
        ''' <param name="developerKey">Developer Key</param>
        ''' <returns><see cref="ComponentLicense"/> with the specified fields set.</returns>
        Friend Shared Function CreateLicense(developerKey As String) As ComponentLicense
            Return New ComponentLicense(developerKey)
        End Function

#End Region

#Region "Private Methods"

        Private Function VerifyKey() As Boolean
            Try
                If Not String.IsNullOrEmpty(_licKey) Then
                    If _licKey.Length >= 12 Then
                        Dim asm As Assembly = GetAsm()
                        Dim fv As FileVersionInfo = FileVersionInfo.GetVersionInfo(asm.BinaryPath)
                        Dim str As String = fv.CompanyName

                        Dim sbHash As System.Text.StringBuilder = Utility.General.GetCalc(str, GetAsmNamespace)

                        If Not String.IsNullOrEmpty(sbHash.ToString) Then
                            If _licKey.Replace("-", "").ToLower = sbHash.ToString.ToLower Then
                                Return True
                            End If
                        End If
                    End If
                End If
            Catch ex As Exception
                WinForm.General.WarningMsg(Utility.General.GetAsmNamespace(GetMyAssembly) & vbCrLf & ex.Message)
                System.Windows.Forms.Application.Exit()
            End Try

            Return False
        End Function
#End Region

    End Class
End Namespace
