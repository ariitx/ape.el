﻿
Imports System.ComponentModel
Imports System.Reflection

Namespace Licensing

    ''' <summary>
    ''' Provides Component License Validation.
    ''' </summary>
    Public Class ComponentLicenseProvider
        Inherits LicenseProvider

#Region "Public Methods"

        ''' <summary>
        ''' Gets a license for an instance or type of component.
        ''' </summary>
        ''' <param name="context">A <see cref="LicenseContext"/> that specifies where you can use the licensed object.</param>
        ''' <param name="type">A <see cref="System.Type"/> that represents the component requesting the license.</param>
        ''' <param name="instance">An object that is requesting the license.</param>
        ''' <param name="allowExceptions">true if a <see cref="LicenseException"/> should be thrown when the component cannot be granted a license; otherwise, false.</param>
        ''' <returns>A valid <see cref="License"/>.</returns>
        Public Overrides Function GetLicense(context As LicenseContext, type As Type, instance As Object, allowExceptions As Boolean) As License
            'Here you can check the context to see if it is running in Design or Runtime. You can also do more
            'fun things like limit the number of instances by tracking the keys (only allow X number of controls
            'on a form for example). You can add additional data to the instance if you want, etc. 

            Try
                Dim devKey As String = GetDeveloperKey(type)

                'Returns a demo license if no key.
                Return ComponentLicense.CreateLicense(devKey)
            Catch le As LicenseException
                If allowExceptions Then
                    Throw le
                Else
                    Return ComponentLicense.CreateDemoLicense()
                End If
            End Try
        End Function

        Public Sub New()
            MsgBox("component license provider")
        End Sub

#End Region

#Region "Private Methods"

        ''' <summary>
        ''' Returns the string value of the DeveloperKey static property on the control.
        ''' </summary>
        ''' <param name="type">Type of licensed component with a DeveloperKey property.</param>
        ''' <returns>String value of the developer key.</returns>
        Private Function GetDeveloperKey(type As Type) As String
            Dim pInfo As PropertyInfo = type.GetProperty("DeveloperKey")

            If pInfo Is Nothing Then
                Throw New LicenseException(type, Nothing, "The licensed control does not contain a DeveloperKey static field. Contact the developer for assistance.")
            End If

            Dim value As String = TryCast(pInfo.GetValue(Nothing, Nothing), String)

            Return value
        End Function

#End Region

    End Class
End Namespace
