﻿Imports System
Imports System.Drawing
Imports System.Drawing.Imaging
Imports System.IO
Imports System.Runtime.Serialization.Formatters.Binary

Namespace Converters
    Public Class Base64String
        ' Methods
        Public Shared Function ImageToString(ByVal _image As Image) As String
            Try 
                If (Not _image Is Nothing) Then
                    Using stream As MemoryStream = New MemoryStream
                        _image.Save(stream, ImageFormat.Jpeg)
                        Return Convert.ToBase64String(stream.ToArray)
                    End Using
                End If
            Catch obj1 As Exception
                Return Nothing
            End Try
            Return Nothing
        End Function

        Public Shared Function ObjectToString(ByVal _object As Object) As String
            Dim str As String
            Try 
                Using stream As MemoryStream = New MemoryStream
                    Dim bf As New BinaryFormatter()
                    bf.Serialize(stream, _object)
                    str = Convert.ToBase64String(stream.ToArray)
                End Using
            Catch obj1 As Exception
                str = Nothing
            End Try
            Return str
        End Function

        Public Shared Function StringToImage(ByVal _stringBitmap As String) As Image
            Try 
                If Not String.IsNullOrEmpty(_stringBitmap) Then
                    Using stream As MemoryStream = New MemoryStream(Convert.FromBase64String(_stringBitmap))
                        Return Image.FromStream(stream)
                    End Using
                End If
            Catch obj1 As Exception
                Return Nothing
            End Try
            Return Nothing
        End Function

        Public Shared Function StringToObject(ByVal _string As String) As Object
            Dim obj2 As Object
            Try 
                Using stream As MemoryStream = New MemoryStream(Convert.FromBase64String(_string))
                    obj2 = New BinaryFormatter().Deserialize(stream)
                End Using
            Catch obj1 As Exception
                obj2 = Nothing
            End Try
            Return obj2
        End Function

    End Class
End Namespace

