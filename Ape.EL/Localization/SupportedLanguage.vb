﻿Imports System.Globalization
Imports System.ComponentModel

Namespace Localization
    Public Class SupportedLanguage

#Region "List"
        ''' <summary>
        ''' Internally used. Listing out all language culture based on NeutralCulture.
        ''' EXTERNALLY FOR REFERENCE USAGE ONLY!!! DO NOT MODIFY THE VALUE OUT OF THIS CLASS!!!
        ''' This function is hidden from Intellisense.
        ''' </summary>
        <EditorBrowsable(EditorBrowsableState.Never)>
        Public Shared ReadOnly CultureDictionary As Dictionary(Of String, String) = InitCultureDictionary()
        Private Shared Function InitCultureDictionary() As Dictionary(Of String, String)
            Dim dic As New Dictionary(Of String, String)
            Dim ci As CultureInfo
            For Each ci In CultureInfo.GetCultures(CultureTypes.NeutralCultures)
                dic.Add(ci.Name, ci.DisplayName)
            Next

            Return dic
        End Function

        ''' <summary>
        ''' List of supported languages in here. You can add using AddSupportedLanguage function or delete using DelSupportedLanguage.
        ''' </summary>
        ''' <remarks>By default "en" is in SupportedLanguage.</remarks>
        Public Shared SupportedLanguage As Dictionary(Of String, String)
        Private Shared ReadOnly InitSupportedLanguage_ As Boolean = InitSupportedLanguage()
        Private Shared Function InitSupportedLanguage() As Boolean
            'Default supported languages.
            Dim listSupported As List(Of String) = (New String() {"en"}).ToList
            AddSupportedLanguage(listSupported.ToArray)

            Return True
        End Function

        ''' <summary>
        ''' Add new supported language.
        ''' </summary>
        ''' <param name="id">NeutralCulture name.</param>
        Public Shared Sub AddSupportedLanguage(ByVal id As String)
            If SupportedLanguage Is Nothing Then SupportedLanguage = New Dictionary(Of String, String)

            If CultureDictionary.ContainsKey(id) Then
                If Not SupportedLanguage.ContainsKey(id) Then
                    'only add new language if it is not exist yet.
                    Dim Name As String = ""
                    CultureDictionary.TryGetValue(id, Name)
                    SupportedLanguage.Add(id, Name)
                End If
            Else
                'Throw New ApplicationException(String.Format("Language culture '{0}' not registered.", Id))
                App.ErrorLog.Debug(String.Format("Language culture '{0}' not registered.", id))
            End If
        End Sub

        ''' <summary>
        ''' Add new supported languages in range.
        ''' </summary>
        ''' <param name="ids">NeutralCulture name.</param>
        Public Shared Sub AddSupportedLanguage(ByVal ids() As String)
            If ids IsNot Nothing AndAlso ids.Count > 0 Then
                For Each id As String In ids
                    AddSupportedLanguage(id)
                Next
            End If
        End Sub

        ''' <summary>
        ''' Remove supported language.
        ''' </summary>
        ''' <param name="id">NeutralCulture name.</param>
        Public Shared Sub DelSupportedLanguage(ByVal id As String)
            If SupportedLanguage Is Nothing Then Exit Sub

            SupportedLanguage.Remove(id)
        End Sub

        ''' <summary>
        ''' Remove supported languages in range.
        ''' </summary>
        ''' <param name="ids">NeutralCulture name.</param>
        Public Shared Sub DelSupportedLanguage(ByVal ids() As String)
            If ids IsNot Nothing AndAlso ids.Count > 0 Then
                For Each id As String In ids
                    DelSupportedLanguage(id)
                Next
            End If
        End Sub

        ''' <summary>
        ''' Delete all supported languages.
        ''' </summary>
        Public Shared Sub DelAllSupportedLanguage()
            If SupportedLanguage Is Nothing Then Exit Sub

            SupportedLanguage = New Dictionary(Of String, String)
        End Sub
#End Region

        Private Id As String
        Private Name As String

        Public Sub New()
        End Sub

        Public Sub New(Id As String)
            SetLanguage(Id)
        End Sub

        Public Sub SetLanguage(id As String)
            If Not id.IsEmpty AndAlso CultureDictionary.ContainsKey(id) Then
                Me.Id = id
                CultureDictionary.TryGetValue(id, Me.Name)
            ElseIf Not id.IsEmpty Then
                App.ErrorLog.Debug(String.Format("Language culture '{0}' not in CultureDictionary.", id))
                Me.Name = "UNREGISTERED"
            End If
        End Sub

        Public Overrides Function ToString() As String
            Return Id
        End Function

        Public Function DisplayName() As String
            Return Name
        End Function
    End Class
End Namespace
