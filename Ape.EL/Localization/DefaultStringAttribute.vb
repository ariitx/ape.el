﻿Imports System

Namespace Localization
    <System.AttributeUsage(System.AttributeTargets.Field)>
    Public Class DefaultStringAttribute
        Inherits System.Attribute

        Private myText As String

        Public ReadOnly Property Text() As String
            Get
                Return Me.myText
            End Get
        End Property

        Public Sub New(text As String)
            If text Is Nothing Then
                Throw New System.ArgumentNullException("text")
            End If
            Me.myText = text
        End Sub
    End Class
End Namespace
