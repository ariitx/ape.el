﻿
Imports System

Namespace Localization
    <System.AttributeUsage(System.AttributeTargets.[Enum])>
    Public Class LocalizableStringAttribute
        Inherits System.Attribute

        Private myBaseName As String

        Public ReadOnly Property BaseName() As String
            Get
                Return Me.myBaseName
            End Get
        End Property

        Public Sub New(baseName As String)
            If baseName Is Nothing Then
                Throw New System.ArgumentNullException("baseName")
            End If
            Me.myBaseName = baseName
        End Sub

        Public Sub New()
            Me.myBaseName = ""
        End Sub
    End Class
End Namespace
