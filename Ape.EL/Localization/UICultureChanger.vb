﻿'
' * Copyright © 2006 Stefan Troschütz (stefan@troschuetz.de)
' * 
' * This file is part of UICultureChanger Class Library.
' * 
' * UICultureChanger is free software; you can redistribute it and/or
' * modify it under the terms of the GNU Lesser General Public
' * License as published by the Free Software Foundation; either
' * version 2.1 of the License, or any later version.
' * This library is distributed in the hope that it will be useful,
' * but WITHOUT ANY WARRANTY; without even the implied warranty of
' * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
' * Lesser General Public License for more details.
' * You should have received a copy of the GNU Lesser General Public
' * License along with this library; if not, write to the Free Software
' * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
' * 
' * UICultureChanger.cs, 20.08.2006
' * 

Imports System.CodeDom
Imports System.ComponentModel
Imports System.ComponentModel.Design
Imports System.ComponentModel.Design.Serialization
#If Prior2 Then
Imports System.Collections
#Else
Imports System.Collections.Generic
#End If
Imports System.Drawing
Imports System.Globalization
Imports System.Reflection
Imports System.Threading
Imports System.Windows.Forms

Namespace Localization 'Troschuetz
    ''' <summary>
    ''' Enables changes of the UI culture of a collection of <see cref="Form"/> objects at runtime.
    ''' </summary>
    <DesignerSerializer(GetType(UICultureChanger.UICultureChangerCodeDomSerializer), GetType(CodeDomSerializer))> _
    Public Class UICultureChanger
        Inherits Component
#Region "UICultureChangerCodeDomSerializer class"
        ''' <summary>
        ''' Serializes an object graph of <see cref="UICultureChanger"/> class to a series of CodeDOM statements.
        ''' </summary>
        ''' <remarks>
        ''' The method is customized, so CodeStatement for the object construction 
        '''   doesn't use the default constructor of class.
        ''' </remarks>
        Friend Class UICultureChangerCodeDomSerializer
            Inherits CodeDomSerializer
            ''' <summary>
            ''' Deserializes the specified serialized CodeDOM object into an object.
            ''' </summary>
            ''' <param name="manager">
            ''' A serialization manager interface that is used during the deserialization process.
            ''' </param>
            ''' <param name="codeObject">A serialized CodeDOM object to deserialize.</param>
            ''' <returns>The deserialized CodeDOM object.</returns>
            Public Overrides Function Deserialize(manager As IDesignerSerializationManager, codeObject As Object) As Object
                Dim baseClassSerializer As CodeDomSerializer = DirectCast(manager.GetSerializer(GetType(UICultureChanger).BaseType, GetType(CodeDomSerializer)), CodeDomSerializer)

                Return baseClassSerializer.Deserialize(manager, codeObject)
            End Function

            ''' <summary>
            ''' Serializes the specified object into a CodeDOM object.
            ''' </summary>
            ''' <param name="manager">The serialization manager to use during serialization.</param>
            ''' <param name="value">The object to serialize.</param>
            ''' <returns>A CodeDOM object representing the object that has been serialized.</returns>
            Public Overrides Function Serialize(manager As IDesignerSerializationManager, value As Object) As Object
                Dim baseClassSerializer As CodeDomSerializer = DirectCast(manager.GetSerializer(GetType(UICultureChanger).BaseType, GetType(CodeDomSerializer)), CodeDomSerializer)
                Dim codeStatementCollection As CodeStatementCollection = DirectCast(baseClassSerializer.Serialize(manager, value), CodeStatementCollection)
                Dim variableName As String = DirectCast(value, Component).Site.Name

                ' If the CodeStatementCollection only contains the constructor and/or member variable definition, 
                '   add comment-block with name of the member variable.
                If codeStatementCollection.Count <= 2 Then
                    codeStatementCollection.Add(New CodeCommentStatement(""))
                    codeStatementCollection.Add(New CodeCommentStatement(variableName))
                    codeStatementCollection.Add(New CodeCommentStatement(""))
                End If

                ' Add a call to the UICultureChanger.AddForm method.
                Dim codeMethodInvokeExpression As New CodeMethodInvokeExpression(New CodeFieldReferenceExpression(New CodeThisReferenceExpression(), variableName), "AddForm", New CodeExpression() {New CodeThisReferenceExpression()})
                codeStatementCollection.Add(codeMethodInvokeExpression)

                Return codeStatementCollection
            End Function
        End Class
#End Region

#Region "ChangeInfo class"
        ''' <summary>
        ''' Encapsulates all information needed to apply localized resources to a form or field.
        ''' </summary>
        Private Class ChangeInfo
#Region "instance fields"
            ''' <summary>
            ''' Gets the name of the form or field.
            ''' </summary>
            Public ReadOnly Property Name() As String
                Get
                    Return m_name
                End Get
            End Property

            ''' <summary>
            ''' Stores the name of the form or field.
            ''' </summary>
            Private m_name As String

            ''' <summary>
            ''' Gets the instance of the form or field.
            ''' </summary>
            Public ReadOnly Property Value() As Object
                Get
                    Return Me.m_value
                End Get
            End Property

            ''' <summary>
            ''' Stores the instance of the form or field.
            ''' </summary>
            Private m_value As Object

            ''' <summary>
            ''' Gets the <see cref="Type"/> object of the form or field.
            ''' </summary>
            Public ReadOnly Property Type() As Type
                Get
                    Return m_type
                End Get
            End Property

            ''' <summary>
            ''' Stores the <see cref="Type"/> object of the form or field.
            ''' </summary>
            Private m_type As Type
#End Region

#Region "construction"
            ''' <summary>
            ''' Initializes a new instance of the <see cref="ChangeInfo"/> class.
            ''' </summary>
            ''' <param name="name">The name of the form or field.</param>
            ''' <param name="value">The instance of the form or field.</param>
            ''' <param name="type">The <see cref="Type"/> object of the form or field.</param>
            Public Sub New(name As String, value As Object, type As Type)
                Me.m_name = name
                Me.m_value = value
                Me.m_type = type
            End Sub
#End Region
        End Class
#End Region

#If Prior2 Then
		#Region "ChangeInfoList class"
		''' <summary>
		''' A strongly typed collection of <see cref="ChangeInfoList"/> objects.
		''' </summary>
		Private Class ChangeInfoList
			Inherits CollectionBase
			#Region "instance fields"
			''' <value>
			''' Gets or sets the <see cref="ChangeInfo"/>object at the specified index.<br />
			''' In C#, this property is the indexer for the <see cref="ChangeInfoList"/> class.
			''' </value>
			''' <remarks>
			''' <see cref="ChangeInfoList"/> accepts a null reference (<see langword="Nothing"/> in Visual Basic) as a valid value 
			'''   and allows duplicate elements.<br />
			''' This property provides the ability to access a specific element in the collection by using the following syntax: 
			'''   <c>myCollection[index]</c>.
			''' </remarks>
			''' <param name="index">The zero-based index of the element to get or set.</param>
			''' <exception cref="ArgumentOutOfRangeException">
			''' <paramref name="index"/> is less than zero.<br />
			''' -or-<br />
			''' <paramref name="index"/>  is equal to or greater than <see cref="CollectionBase.Count"/>.
			''' </exception>
			Public Default Property Item(index As Integer) As ChangeInfo
				Get
					Return DirectCast(Me.InnerList(index), ChangeInfo)
				End Get
				Set
					Me.InnerList(index) = value
				End Set
			End Property
			#End Region

			#Region "construction"
			Public Sub New(capacity As Integer)
				MyBase.InnerList.Capacity = capacity
			End Sub
			#End Region

			#Region "instance methods"
			''' <summary>
			''' Adds a <see cref="ChangeInfo"/> object to the end of the <see cref="ChangeInfoList"/>.
			''' </summary>
			''' <remarks>
			''' <see cref="ChangeInfoList"/> accepts a null reference (<see langword="Nothing"/> in Visual Basic) as a valid value 
			'''   and allows duplicate elements.
			''' </remarks>
			''' <param name="changeInfo">
			''' The <see cref="ChangeInfo"/> object to be added to the end of the <see cref="ChangeInfoList"/>. 
			''' The value can be a null reference (<see langword="Nothing"/> in Visual Basic).
			''' </param>
			''' <returns>The <see cref="ChangeInfoList"/> index at which the <paramref name="changeInfo"/> has been added.</returns>
			Public Function Add(changeInfo As ChangeInfo) As Integer
				Return Me.InnerList.Add(changeInfo)
			End Function

			''' <summary>
			''' Removes the first occurrence of a specific <see cref="ChangeInfo"/> object from the 
			'''   <see cref="ChangeInfoList"/>.
			''' </summary>
			''' <param name="changeInfo">
			''' The <see cref="ChangeInfo"/> object to remove from the <see cref="ChangeInfoList"/>. 
			''' The value can be a null reference (<see langword="Nothing"/> in Visual Basic).
			''' </param>
			Public Sub Remove(changeInfo As ChangeInfo)
				Me.InnerList.Remove(changeInfo)
			End Sub

			''' <summary>
			''' Sets the capacity to the actual number of elements in the <see cref="ChangeInfoList"/>.
			''' </summary>
			''' <remarks>
			''' This method can be used to minimize a list's memory overhead if no new elements will be added to the list.<br />
			'''	To completely clear all elements in a list, call the <see cref="CollectionBase.Clear"/> method before calling 
			'''	  <see cref="TrimToSize"/>. Trimming an empty <see cref="ChangeInfoList"/> sets the capacity of the 
			'''	  <see cref="ChangeInfoList"/> to the default capacity, not zero.
			''' </remarks>
			Public Sub TrimToSize()
				Me.InnerList.TrimToSize()
			End Sub
			#End Region
		End Class
		#End Region
#End If

#Region "instance fields"
        ''' <summary>
        ''' Stores a collection of <see cref="Form"/> objects whose UI culture will be changed.
        ''' </summary>
#If Prior2 Then
		Private forms As ArrayList
#Else
        Private forms As List(Of Form)
#End If

        ''' <summary>
        ''' Gets or sets a value indicating whether localized Text values are applied when changing the UI culture.
        ''' </summary>
        <Browsable(True), DefaultValue(True)> _
        <Description("Indicates whether localized Text values are applied when changing the UI culture."), Category("Behavior")> _
        Public Property ApplyText() As Boolean
            Get
                Return Me.m_applyText
            End Get
            Set(value As Boolean)
                Me.m_applyText = value
            End Set
        End Property

        ''' <summary>
        ''' Stores a value indicating whether localized Text values are applied when changing the UI culture.
        ''' </summary>
        Private m_applyText As Boolean

        ''' <summary>
        ''' Gets or sets a value indicating whether localized Size values are applied when changing the UI culture.
        ''' </summary>
        <Browsable(True), DefaultValue(False)> _
        <Description("Indicates whether localized Size values are applied when changing the UI culture."), Category("Behavior")> _
        Public Property ApplySize() As Boolean
            Get
                Return Me.m_applySize
            End Get
            Set(value As Boolean)
                Me.m_applySize = value
            End Set
        End Property

        ''' <summary>
        ''' Stores a value indicating whether localized Size values are applied when changing the UI culture.
        ''' </summary>
        Private m_applySize As Boolean

        ''' <summary>
        ''' Gets or sets a value indicating whether localized Location values are applied when changing the UI culture.
        ''' </summary>
        <Browsable(True), DefaultValue(False)> _
        <Description("Indicates whether localized Location values are applied when changing the UI culture."), Category("Behavior")> _
        Public Property ApplyLocation() As Boolean
            Get
                Return Me.m_applyLocation
            End Get
            Set(value As Boolean)
                Me.m_applyLocation = value
            End Set
        End Property

        ''' <summary>
        ''' Stores a value indicating whether localized Location values are applied when changing the UI culture.
        ''' </summary>
        Private m_applyLocation As Boolean

        ''' <summary>
        ''' Gets or sets a value indicating whether localized RightToLeft values are applied when changing the UI culture.
        ''' </summary>
        <Browsable(True), DefaultValue(False)> _
        <Description("Indicates whether localized RightToLeft values are applied when changing the UI culture."), Category("Behavior")> _
        Public Property ApplyRightToLeft() As Boolean
            Get
                Return Me.m_applyRightToLeft
            End Get
            Set(value As Boolean)
                Me.m_applyRightToLeft = value
            End Set
        End Property

        ''' <summary>
        ''' Stores a value indicating whether localized RightToLeft values are applied when changing the UI culture.
        ''' </summary>
        Private m_applyRightToLeft As Boolean

#If Not Prior2 Then
        ''' <summary>
        ''' Gets or sets a value indicating whether localized RightToLeftLayout values are applied when changing the UI culture.
        ''' </summary>
        <Browsable(True), DefaultValue(False)> _
        <Description("Indicates whether localized RightToLeftLayout values are applied when changing the UI culture."), Category("Behavior")> _
        Public Property ApplyRightToLeftLayout() As Boolean
            Get
                Return Me.m_applyRightToLeftLayout
            End Get
            Set(value As Boolean)
                Me.m_applyRightToLeftLayout = value
            End Set
        End Property

        ''' <summary>
        ''' Stores a value indicating whether localized RightToLeftLayout values are applied when changing the UI culture.
        ''' </summary>
        Private m_applyRightToLeftLayout As Boolean
#End If

        ''' <summary>
        ''' Gets or sets a value indicating whether localized ToolTip values are applied when changing the UI culture.
        ''' </summary>
        <Browsable(True), DefaultValue(False)> _
        <Description("Indicates whether localized tooltips are applied when changing the UI culture."), Category("Behavior")> _
        Public Property ApplyToolTip() As Boolean
            Get
                Return Me.m_applyToolTip
            End Get
            Set(value As Boolean)
                Me.m_applyToolTip = value
            End Set
        End Property

        ''' <summary>
        ''' Stores a value indicating whether localized ToolTip values are applied when changing the UI culture.
        ''' </summary>
        Private m_applyToolTip As Boolean

        ''' <summary>
        ''' Gets or sets a value indicating whether localized Help values are applied when changing the UI culture.
        ''' </summary>
        <Browsable(True), DefaultValue(False)> _
        <Description("Indicates whether localized help contents are applied when changing the UI culture."), Category("Behavior")> _
        Public Property ApplyHelp() As Boolean
            Get
                Return Me.m_applyHelp
            End Get
            Set(value As Boolean)
                Me.m_applyHelp = value
            End Set
        End Property

        ''' <summary>
        ''' Stores a value indicating whether localized Help values are applied when changing the UI culture.
        ''' </summary>
        Private m_applyHelp As Boolean

        ''' <summary>
        ''' Gets or sets a value indicating whether the Size values of forms remain unchanged when changing the UI culture.
        ''' </summary>
        ''' <remarks>
        ''' This property has no effect unless <see cref="ApplySize"/> is <see langword="true"/>.
        ''' </remarks>
        <Browsable(True), DefaultValue(True)> _
        <Description("Indicates whether the Size values of forms are preserved when changing the UI culture."), Category("Behavior")> _
        Public Property PreserveFormSize() As Boolean
            Get
                Return Me.m_preserveFormSize
            End Get
            Set(value As Boolean)
                Me.m_preserveFormSize = value
            End Set
        End Property

        ''' <summary>
        ''' Stores a value indicating whether the Size values of forms remain unchanged when changing the UI culture.
        ''' </summary>
        Private m_preserveFormSize As Boolean

        ''' <summary>
        ''' Gets or sets a value indicating whether the Location values of forms remain unchanged when changing the UI culture.
        ''' </summary>
        ''' <remarks>
        ''' This property has no effect unless <see cref="ApplyLocation"/> is <see langword="true"/>.
        ''' </remarks>
        <Browsable(True), DefaultValue(True)> _
        <Description("Indicates whether the Location values of forms are preserved when changing the UI culture."), Category("Behavior")> _
        Public Property PreserveFormLocation() As Boolean
            Get
                Return Me.m_preserveFormLocation
            End Get
            Set(value As Boolean)
                Me.m_preserveFormLocation = value
            End Set
        End Property

        ''' <summary>
        ''' Stores a value indicating whether the Location values of forms remain unchanged when changing the UI culture.
        ''' </summary>
        Private m_preserveFormLocation As Boolean
#End Region

#Region "construction, deconstruction"
        ''' <summary>
        ''' Initializes a new instance of the <see cref="UICultureChanger"/> class.
        ''' </summary>
        Public Sub New()
            ' Create List with size 1, so it can take the container form of the component without resizing.
#If Prior2 Then
			Me.forms = New ArrayList(1)
#Else
            Me.forms = New List(Of Form)(1)
#End If

            Me.m_applyText = True
            Me.m_applySize = False
            Me.m_applyLocation = False
            Me.m_applyRightToLeft = False
#If Not Prior2 Then
            Me.m_applyRightToLeftLayout = False
#End If
            Me.m_applyToolTip = False
            Me.m_applyHelp = False
            Me.m_preserveFormSize = True
            Me.m_preserveFormLocation = True
        End Sub

        ''' <summary> 
        ''' Releases the unmanaged resources used by the <see cref="UICultureChanger"/> and optionally releases the managed 
        '''   resources.
        ''' </summary>
        ''' <param name="disposing">
        ''' <see langword="true"/> to release both managed and unmanaged resources; <see langword="false"/> to release only 
        '''   unmanaged resources. 
        ''' </param>
        Protected Overrides Sub Dispose(disposing As Boolean)
            If disposing Then
                For index As Integer = 0 To Me.forms.Count - 1
#If Prior2 Then
					Me.RemoveForm(DirectCast(Me.forms(index), Form))
#Else
#End If
                    Me.RemoveForm(Me.forms(index))
                Next
            End If

            MyBase.Dispose(disposing)
        End Sub
#End Region

#Region "instance methods"
        ''' <summary>
        ''' Adds the specified <see cref="Form"/> object to the collection of forms whose UI cultures will be changed.
        ''' </summary>
        ''' <remarks>
        ''' The <see cref="UICultureChanger"/> component registers to the <see cref="Form.FormClosed"/> event of the specified
        '''   <see cref="Form"/> object, so after being closed it can automatically be removed from the form collection.
        ''' </remarks>
        ''' <param name="form">The <see cref="Form"/> object to add to the end of the form collection.</param>
        Public Sub AddForm(form As Form)
            If form IsNot Nothing Then
                Me.forms.Add(form)

#If Prior2 Then
				form.Closed += New EventHandler(AddressOf Me.Form_Closed)
#Else
#End If
                AddHandler form.FormClosed, AddressOf Me.Form_FormClosed '   form.FormClosed += New FormClosedEventHandler(AddressOf Me.Form_FormClosed)
            End If
        End Sub

        ''' <summary>
        ''' Removes the first occurrence of the specified <see cref="Form"/> object from the collection of forms 
        '''   whose UI cultures will be changed.
        ''' </summary>
        ''' <param name="form">The <see cref="Form"/> object to remove from the form collection.</param>
        ''' <returns>
        ''' <see langword="true"/> if <paramref name="form"/> is successfully removed; otherwise, <see langword="false"/>. 
        ''' This method also returns <see langword="false"/> if <paramref name="form"/> is a null reference 
        '''   (<see langword="Nothing"/> in Visual Basic) or was not found in the form collection. 
        ''' </returns>
        Public Function RemoveForm(form As Form) As Boolean
            If form Is Nothing Then
                Return False
            Else
#If Prior2 Then
				form.Closed -= New EventHandler(AddressOf Me.Form_Closed)
				Me.forms.Remove(form)
				Return True
#Else
                AddHandler form.FormClosed, AddressOf Me.Form_FormClosed  '   form.FormClosed -= New FormClosedEventHandler(AddressOf Me.Form_FormClosed)
#End If
                Return Me.forms.Remove(form)
            End If
        End Function

#If Prior2 Then
		''' <summary>
		''' Removes the specified sender object from the collection of forms whose UI cultures will be changed, if it is a 
		'''   <see cref="Form"/> object
		''' </summary>
		''' <param name="sender">The source of the event.</param>
		''' <param name="e">A <see cref="EventArgs"/> object that contains the event data.</param>
		Private Sub Form_Closed(sender As Object, e As EventArgs)
			Dim form As Form = TryCast(sender, Form)
			If form IsNot Nothing Then
				Me.RemoveForm(form)
			End If
		End Sub
#Else
        ''' <summary>
        ''' Removes the specified sender object from the collection of forms whose UI cultures will be changed, if it is a 
        '''   <see cref="Form"/> object
        ''' </summary>
        ''' <param name="sender">The source of the event.</param>
        ''' <param name="e">A <see cref="FormClosedEventArgs"/> object that contains the event data.</param>
        Private Sub Form_FormClosed(sender As Object, e As FormClosedEventArgs)
            Dim form As Form = TryCast(sender, Form)
            If form IsNot Nothing Then
                Me.RemoveForm(form)
            End If
        End Sub
#End If

        ''' <summary>
        ''' Applies the specified <see cref="CultureInfo"/> object to the <see cref="Thread.CurrentUICulture"/> field and 
        '''   corresponding localized resources to all collected forms.
        ''' </summary>
        ''' <param name="cultureInfo">A <see cref="CultureInfo"/> object representing the wanted UI culture.</param>
        Public Sub ApplyCulture(cultureInfo As CultureInfo)
            ' Applies culture to current Thread.
            Thread.CurrentThread.CurrentUICulture = cultureInfo

            For index As Integer = 0 To Me.forms.Count - 1
#If Prior2 Then
				Me.ApplyCultureToForm(DirectCast(Me.forms(index), Form))
#Else
#End If
                Me.ApplyCultureToForm(Me.forms(index))
            Next
        End Sub

        ''' <summary>
        ''' Applies localized resources to the specified <see cref="Form"/> object according to the 
        '''   <see cref="Thread.CurrentUICulture"/>.
        ''' </summary>
        ''' <param name="form">The <see cref="Form"/> object to which changed UI culture should be applied.</param>
        Private Sub ApplyCultureToForm(form As Form)
            ' Create a resource manager for the form and determine its fields via reflection.
            ' Create and fill a collection, containing all infos needed to apply localized resources.
            Dim resources As New ComponentResourceManager(form.[GetType]())
            Dim fields As FieldInfo() = form.[GetType]().GetFields(BindingFlags.Instance Or BindingFlags.DeclaredOnly Or BindingFlags.NonPublic)
#If Prior2 Then
			Dim changeInfos As New ChangeInfoList(fields.Length + 1)
#Else
            Dim changeInfos As New List(Of ChangeInfo)(fields.Length + 1)
#End If
            changeInfos.Add(New ChangeInfo("$this", form, form.[GetType]()))
            For index As Integer = 0 To fields.Length - 1
                changeInfos.Add(New ChangeInfo(fields(index).Name, fields(index).GetValue(form), fields(index).FieldType))
            Next
#If Prior2 Then
			changeInfos.TrimToSize()
#Else
            changeInfos.TrimExcess()
#End If

            ' Call SuspendLayout for Form and all fields derived from Control, so assignment of 
            '   localized resources doesn't change layout immediately.
            For index As Integer = 0 To changeInfos.Count - 1
                If changeInfos(index).Type.IsSubclassOf(GetType(Control)) Then
                    changeInfos(index).Type.InvokeMember("SuspendLayout", BindingFlags.InvokeMethod, Nothing, changeInfos(index).Value, Nothing)
                End If
            Next

            If Me.m_applyText Then
                ' If available, assign localized text to Form and fields.
                Dim text As [String]
                For index As Integer = 0 To changeInfos.Count - 1
                    If changeInfos(index).Type.GetProperty("Text", GetType([String])) IsNot Nothing AndAlso changeInfos(index).Type.GetProperty("Text", GetType([String])).CanWrite Then
                        text = resources.GetString(changeInfos(index).Name & Convert.ToString(".Text"))
                        If text IsNot Nothing AndAlso text <> "" Then
                            changeInfos(index).Type.InvokeMember("Text", BindingFlags.SetProperty, Nothing, changeInfos(index).Value, New Object() {text})
                        End If
                    End If
                Next
            End If

            If Me.m_applySize Then
                ' If available, assign localized sizes to Form and fields.
                Dim size As Object
                Dim control As Control
                Dim index As Integer = 0
                If Me.m_preserveFormSize Then
                    ' Skip the form entry in changeInfos collection.
                    index = 1
                End If
                While index < changeInfos.Count
                    If changeInfos(index).Type.GetProperty("Size", GetType(Size)) IsNot Nothing AndAlso changeInfos(index).Type.GetProperty("Size", GetType(Size)).CanWrite Then
                        size = resources.GetObject(changeInfos(index).Name & Convert.ToString(".Size"))
                        If size IsNot Nothing AndAlso size.[GetType]() = GetType(Size) Then
                            If changeInfos(index).Type.IsSubclassOf(GetType(Control)) Then
                                ' In case of an inheritor of Control take into account the Anchor property.
                                control = DirectCast(changeInfos(index).Value, Control)
                                If (control.Anchor And (AnchorStyles.Left Or AnchorStyles.Right)) = (AnchorStyles.Left Or AnchorStyles.Right) Then
                                    ' Control is bound to the left and right edge, so preserve its width.
                                    size = New Size(control.Width, DirectCast(size, Size).Height)
                                End If
                                If (control.Anchor And (AnchorStyles.Top Or AnchorStyles.Bottom)) = (AnchorStyles.Top Or AnchorStyles.Bottom) Then
                                    ' Control is bound to the top and bottom edge, so preserve its height.
                                    size = New Size(DirectCast(size, Size).Width, control.Height)
                                End If
                                control.Size = DirectCast(size, Size)
                            Else
                                changeInfos(index).Type.InvokeMember("Size", BindingFlags.SetProperty, Nothing, changeInfos(index).Value, New Object() {size})
                            End If
                        End If
                    End If

                    If changeInfos(index).Type.GetProperty("ClientSize", GetType(Size)) IsNot Nothing AndAlso changeInfos(index).Type.GetProperty("ClientSize", GetType(Size)).CanWrite Then
                        size = resources.GetObject(changeInfos(index).Name & Convert.ToString(".ClientSize"))
                        If size IsNot Nothing AndAlso size.[GetType]() = GetType(Size) Then
                            If changeInfos(index).Type.IsSubclassOf(GetType(Control)) Then
                                ' In case of an inheritor of Control take into account the Anchor property.
                                control = DirectCast(changeInfos(index).Value, Control)
                                If (control.Anchor And (AnchorStyles.Left Or AnchorStyles.Right)) = (AnchorStyles.Left Or AnchorStyles.Right) Then
                                    ' Control is bound to the left and right edge, so preserve the width of its client area.
                                    size = New Size(control.ClientSize.Width, DirectCast(size, Size).Height)
                                End If
                                If (control.Anchor And (AnchorStyles.Top Or AnchorStyles.Bottom)) = (AnchorStyles.Top Or AnchorStyles.Bottom) Then
                                    ' Control is bound to the top and bottom edge, so preserve the height of its client area.
                                    size = New Size(DirectCast(size, Size).Width, control.ClientSize.Height)
                                End If
                                control.ClientSize = DirectCast(size, Size)
                            Else
                                changeInfos(index).Type.InvokeMember("ClientSize", BindingFlags.SetProperty, Nothing, changeInfos(index).Value, New Object() {size})
                            End If
                        End If
                    End If
                    index += 1
                End While
            End If

            If Me.m_applyLocation Then
                ' If available, assign localized locations to Form and fields.
                Dim location As Object
                Dim control As Control
                Dim index As Integer = 0
                If Me.m_preserveFormLocation Then
                    ' Skip the form entry in changeInfos collection.
                    index = 1
                End If
                While index < changeInfos.Count
                    If changeInfos(index).Type.GetProperty("Location", GetType(Point)) IsNot Nothing AndAlso changeInfos(index).Type.GetProperty("Location", GetType(Point)).CanWrite Then
                        location = resources.GetObject(changeInfos(index).Name & Convert.ToString(".Location"))
                        If location IsNot Nothing AndAlso location.[GetType]() = GetType(Point) Then
                            If changeInfos(index).Type.IsSubclassOf(GetType(Control)) Then
                                ' In case of an inheritor of Control take into account the Anchor property.
                                control = DirectCast(changeInfos(index).Value, Control)
                                If (control.Anchor And (AnchorStyles.Left Or AnchorStyles.Right)) = AnchorStyles.Right Then
                                    ' Control is bound to the right but not the left edge, so preserve its x-coordinate.
                                    location = New Point(control.Left, DirectCast(location, Point).Y)
                                End If
                                If (control.Anchor And (AnchorStyles.Top Or AnchorStyles.Bottom)) = AnchorStyles.Bottom Then
                                    ' Control is bound to the bottom but not the top edge, so preserve its y-coordinate.
                                    location = New Point(DirectCast(location, Point).X, control.Top)
                                End If
                                control.Location = DirectCast(location, Point)
                            Else
                                changeInfos(index).Type.InvokeMember("Location", BindingFlags.SetProperty, Nothing, changeInfos(index).Value, New Object() {location})
                            End If
                        End If
                    End If
                    index += 1
                End While
            End If

            If Me.m_applyRightToLeft Then
                ' If available, assign localized RightToLeft values to Form and fields.
                Dim rightToLeft As Object
                For index As Integer = 0 To changeInfos.Count - 1
                    If changeInfos(index).Type.GetProperty("RightToLeft", GetType(RightToLeft)) IsNot Nothing AndAlso changeInfos(index).Type.GetProperty("RightToLeft", GetType(RightToLeft)).CanWrite Then
                        rightToLeft = resources.GetObject(changeInfos(index).Name & Convert.ToString(".RightToLeft"))
                        If rightToLeft IsNot Nothing AndAlso rightToLeft.[GetType]() = GetType(RightToLeft) Then
                            changeInfos(index).Type.InvokeMember("RightToLeft", BindingFlags.SetProperty, Nothing, changeInfos(index).Value, New Object() {rightToLeft})
                        End If
                    End If
                Next
            End If

#If Not Prior2 Then
            If Me.m_applyRightToLeftLayout Then
                ' If available, assign localized RightToLeftLayout values to Form and fields.
                Dim rightToLeftLayout As Object
                For index As Integer = 0 To changeInfos.Count - 1
                    If changeInfos(index).Type.GetProperty("RightToLeftLayout", GetType(Boolean)) IsNot Nothing AndAlso changeInfos(index).Type.GetProperty("RightToLeftLayout", GetType(Boolean)).CanWrite Then
                        rightToLeftLayout = resources.GetObject(changeInfos(index).Name & Convert.ToString(".RightToLeftLayout"))
                        If rightToLeftLayout IsNot Nothing AndAlso rightToLeftLayout.[GetType]() = GetType(Boolean) Then
                            changeInfos(index).Type.InvokeMember("RightToLeftLayout", BindingFlags.SetProperty, Nothing, changeInfos(index).Value, New Object() {rightToLeftLayout})
                        End If
                    End If
                Next
            End If
#End If

            If Me.m_applyToolTip Then
                ' If available, assign localized ToolTipText to fields.
                ' Also search for a ToolTip component in the current form.
                Dim toolTip As ToolTip = Nothing
                For index As Integer = 1 To changeInfos.Count - 1
                    If changeInfos(index).Type = GetType(ToolTip) Then
                        toolTip = DirectCast(changeInfos(index).Value, ToolTip)
                        resources.ApplyResources(toolTip, changeInfos(index).Name)
                        changeInfos.Remove(changeInfos(index))
                    End If
#If Not Prior2 Then
                    Dim text As [String]
                    If changeInfos(index).Type.GetProperty("ToolTipText", GetType([String])) IsNot Nothing AndAlso changeInfos(index).Type.GetProperty("ToolTipText", GetType([String])).CanWrite Then
                        text = resources.GetString(changeInfos(index).Name & Convert.ToString(".ToolTipText"))
                        If text IsNot Nothing Then
                            changeInfos(index).Type.InvokeMember("ToolTipText", BindingFlags.SetProperty, Nothing, changeInfos(index).Value, New Object() {text})
                        End If
#End If
                    End If
                Next

                If toolTip IsNot Nothing Then
                    ' Form contains a ToolTip component.
                    ' If available, assign localized tooltips to Form and fields.
                    Dim text As [String]
                    For index As Integer = 0 To changeInfos.Count - 1
                        If changeInfos(index).Type.IsSubclassOf(GetType(Control)) Then
                            text = resources.GetString(changeInfos(index).Name & Convert.ToString(".ToolTip"))
                            If text IsNot Nothing Then
                                toolTip.SetToolTip(DirectCast(changeInfos(index).Value, Control), text)
                            End If
                        End If
                    Next
                End If
            End If

            If Me.m_applyHelp Then
                ' Search for a HelpProvider component in the current form.
                Dim helpProvider As HelpProvider = Nothing
                For index As Integer = 1 To changeInfos.Count - 1
                    If changeInfos(index).Type = GetType(HelpProvider) Then
                        helpProvider = DirectCast(changeInfos(index).Value, HelpProvider)
                        resources.ApplyResources(helpProvider, changeInfos(index).Name)
                        changeInfos.Remove(changeInfos(index))
                        Exit For
                    End If
                Next

                If helpProvider IsNot Nothing Then
                    ' If available, assign localized help to Form and fields.
                    Dim text As [String]
                    Dim help As Object
                    For index As Integer = 0 To changeInfos.Count - 1
                        If changeInfos(index).Type.IsSubclassOf(GetType(Control)) Then
                            text = resources.GetString(changeInfos(index).Name & Convert.ToString(".HelpKeyword"))
                            If text IsNot Nothing Then
                                helpProvider.SetHelpKeyword(DirectCast(changeInfos(index).Value, Control), text)
                            End If
                            help = resources.GetObject(changeInfos(index).Name & Convert.ToString(".HelpNavigator"))
                            If help IsNot Nothing AndAlso help.[GetType]() = GetType(HelpNavigator) Then
                                helpProvider.SetHelpNavigator(DirectCast(changeInfos(index).Value, Control), DirectCast(help, HelpNavigator))
                            End If
                            text = resources.GetString(changeInfos(index).Name & Convert.ToString(".HelpString"))
                            If text IsNot Nothing Then
                                helpProvider.SetHelpString(DirectCast(changeInfos(index).Value, Control), text)
                            End If
                            help = resources.GetObject(changeInfos(index).Name & Convert.ToString(".ShowHelp"))
                            If help IsNot Nothing AndAlso help.[GetType]() = GetType(Boolean) Then
                                helpProvider.SetShowHelp(DirectCast(changeInfos(index).Value, Control), CBool(help))
                            End If
                        End If
                    Next
                End If
            End If

            ' Call ResumeLayout for Form and all fields derived from Control to resume layout logic.
            For index As Integer = changeInfos.Count - 1 To 0 Step -1
                If changeInfos(index).Type.IsSubclassOf(GetType(Control)) Then
                    changeInfos(index).Type.InvokeMember("ResumeLayout", BindingFlags.InvokeMethod, Nothing, changeInfos(index).Value, New Object() {True})
                End If
            Next
        End Sub
#End Region
    End Class
End Namespace