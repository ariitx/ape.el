﻿Imports System.Collections
Imports System.Diagnostics
Imports System.IO
Imports System.Reflection
Imports System.Globalization

Namespace Localization
    Public Class CultureInfoDisplayItem

        Public Sub New(displayName__1 As String, cultureInfo__2 As CultureInfo)
            DisplayName = displayName__1
            CultureInfo = cultureInfo__2
        End Sub

        Public Overrides Function ToString() As String
            Return DisplayName
        End Function

        Public DisplayName As String

        Public ReadOnly CultureInfo As CultureInfo
    End Class


    ''' <summary>
    '''   Class responsible to collect available localized resources.
    ''' </summary>
    Public Class LanguageCollector

        Public Enum LanguageNameDisplay
            DisplayName
            EnglishName
            NativeName
        End Enum

        ''' <summary>
        '''   Initializes <c>LanguageCollector</c> object with a list of 
        '''   available localized resources based on subfolders names.
        ''' </summary>
        Public Sub New()
            m_avalableCutureInfos = GetApplicationAvailableCultures()
            Debug.Assert(m_avalableCutureInfos IsNot Nothing)
        End Sub

        ''' <summary>
        '''   Initializes <c>LanguageCollector</c> object with a list of 
        '''   available localized resources based on subfolders names plus
        '''   <c>CultureInfo</c> supplied as a default culture.
        ''' </summary>
        ''' <param name="defaultCultureInfo">
        '''   Default culure for which application did not create subfolder.
        ''' </param>
        Public Sub New(defaultCultureInfo As CultureInfo)
            Me.New()
            If Not m_avalableCutureInfos.Contains(defaultCultureInfo) Then
                m_avalableCutureInfos.Add(defaultCultureInfo)
                m_avalableCutureInfos.Sort(New CultureInfoComparer())
            End If
            Debug.Assert(m_avalableCutureInfos IsNot Nothing)
        End Sub

        Private Class CultureInfoComparer
            Implements IComparer

            Private Function IComparer_Compare(x As Object, y As Object) As Integer Implements IComparer.Compare
                Return Compare(DirectCast(x, CultureInfo), DirectCast(y, CultureInfo))
            End Function

            Public Function Compare(cix As CultureInfo, ciy As CultureInfo) As Integer
                Return String.Compare(cix.Name, ciy.Name)
            End Function

        End Class

        ''' <summary>
        '''   Returns an array of <c>CultureInfoDisplayItem</c> objects for
        '''   all available localized resources.
        ''' </summary>
        ''' <param name="languageNameToDisplay">
        '''   <c>LanguageNameDisplay</c> value defining how language will be displayed.
        ''' </param>
        ''' <param name="currentLanguage">
        '''   Index of currently active UI culture.
        ''' </param>
        ''' <returns>
        '''   An array of <c>CultureInfoDisplayItem</c> objects, sorted by their 
        '''   names (not <c>DisplayName</c>s).
        ''' </returns>
        Public Function GetLanguages(languageNameToDisplay As LanguageNameDisplay, ByRef currentLanguage As Integer) As Localization.CultureInfoDisplayItem()
            Dim cidi As Localization.CultureInfoDisplayItem() = New Localization.CultureInfoDisplayItem(m_avalableCutureInfos.Count - 1) {}
            currentLanguage = -1
            Dim currentCulture As String = System.Threading.Thread.CurrentThread.CurrentUICulture.Name
            Dim parentCulture As String = System.Threading.Thread.CurrentThread.CurrentUICulture.Parent.Name
            For i As Integer = 0 To m_avalableCutureInfos.Count - 1
                Dim ci As CultureInfo = DirectCast(m_avalableCutureInfos(i), CultureInfo)
                Dim displayName As String = GetDisplayName(ci, languageNameToDisplay)
                cidi(i) = New Localization.CultureInfoDisplayItem(displayName, ci)
                If currentCulture = ci.Name OrElse (currentLanguage = -1 AndAlso parentCulture = ci.Name) Then
                    currentLanguage = i
                End If
            Next
            Debug.Assert(currentLanguage > -1 AndAlso currentLanguage < m_avalableCutureInfos.Count)
            Return cidi
        End Function

        Private Function GetDisplayName(cultureInfo As CultureInfo, languageNameToDisplay As LanguageNameDisplay) As String
            Select Case languageNameToDisplay
                Case LanguageNameDisplay.DisplayName
                    Return cultureInfo.DisplayName
                Case LanguageNameDisplay.EnglishName
                    Return cultureInfo.EnglishName
                Case LanguageNameDisplay.NativeName
                    Return cultureInfo.NativeName
            End Select
            Debug.Assert(False, String.Format("Not supported LanguageNameDisplay value {0}", languageNameToDisplay))
            Return ""
        End Function

        Private Function GetAllCultures() As Hashtable
            Dim cis As CultureInfo() = CultureInfo.GetCultures(CultureTypes.AllCultures)
            Dim allCultures As New Hashtable(cis.Length)
            For Each ci As CultureInfo In cis
                allCultures.Add(ci.Name, ci)
            Next
            Return allCultures
        End Function

        Private Function GetApplicationAvailableCultures() As ArrayList
            Dim availableCultures As New ArrayList()
            Dim allCultures As Hashtable = GetAllCultures()
            Dim executableRoot As String = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)
            For Each directory__1 As String In Directory.GetDirectories(executableRoot)
                Dim subDirectory As String = Path.GetFileName(directory__1)
                Dim ci As CultureInfo = DirectCast(allCultures(subDirectory), CultureInfo)
                If ci IsNot Nothing Then
                    availableCultures.Add(ci)
                End If
            Next
            Return availableCultures
        End Function

        Private m_avalableCutureInfos As ArrayList
    End Class
End Namespace