﻿Imports System

Namespace Localization
    Public Class SupportedLanguageHelper
        Public Shared Function GetSupportedLanguage(id As String) As SupportedLanguage
            If SupportedLanguage.SupportedLanguage.ContainsKey(id) Then
                Return New SupportedLanguage(id)
            End If
            Return New SupportedLanguage("en")
        End Function

        Public Shared Function GetLanguageID(lang As SupportedLanguage) As String
            Return lang.ToString
        End Function

        ''' <summary>
        ''' Columns: Name, Id.
        ''' </summary>
        Public Shared Function GetSupportedLanguageTable() As DataTable
            'Dim myLanguageID As String() = New String() {"", "en", "ms-MY", "id", "zh-CHS", "zh-CHT", "th"}
            Dim dt As New DataTable
            dt.Columns.Add("Name", GetType(String))
            dt.Columns.Add("Id", GetType(String))

            For Each key As String In SupportedLanguage.SupportedLanguage.Keys
                Dim value As String = ""
                SupportedLanguage.SupportedLanguage.TryGetValue(key, value)

                Dim drv As DataRowView = Ape.EL.Misc.DataHelper.ConvertToDataView(dt).AddNew
                drv.BeginEdit()
                drv("Name") = value
                drv("Id") = key
                drv.EndEdit()
            Next

            Return dt
        End Function
    End Class
End Namespace
