﻿Imports System
Imports System.IO
Imports System.Security.Cryptography
Imports System.Text

Namespace Security.Cryptography
    Public Class CryptographyHelper
        ' Methods
        Public Shared Function Decrypt(ByVal value As String) As String
            Dim str As String
            If String.IsNullOrEmpty(value) Then
                Return value
            End If
            Using stream As MemoryStream = New MemoryStream(Convert.FromBase64String(value))
                Using provider As DESCryptoServiceProvider = New DESCryptoServiceProvider
                    Using stream2 As CryptoStream = New CryptoStream(stream, provider.CreateDecryptor(CryptographyHelper.GetKey, CryptographyHelper.GetKey), CryptoStreamMode.Read)
                        Using reader As StreamReader = New StreamReader(stream2)
                            str = reader.ReadToEnd
                        End Using
                    End Using
                End Using
            End Using
            Return str
        End Function

        Public Shared Function Encrypt(ByVal value As String) As String
            Dim str As String
            If String.IsNullOrEmpty(value) Then
                Return value
            End If
            Using stream As MemoryStream = New MemoryStream
                Using provider As DESCryptoServiceProvider = New DESCryptoServiceProvider
                    Using stream2 As CryptoStream = New CryptoStream(stream, provider.CreateEncryptor(CryptographyHelper.GetKey, CryptographyHelper.GetKey), CryptoStreamMode.Write)
                        Using writer As StreamWriter = New StreamWriter(stream2)
                            writer.Write(value)
                            writer.Flush
                            stream2.FlushFinalBlock
                            writer.Flush
                            str = Convert.ToBase64String(stream.GetBuffer, 0, CInt(stream.Length))
                        End Using
                    End Using
                End Using
            End Using
            Return str
        End Function

        Private Shared Function GetIV() As Byte()
            Return CryptographyHelper.GetKey
        End Function

        Private Shared Function GetKey() As Byte()
            Return Encoding.ASCII.GetBytes("!@c_+6Sk*")
        End Function

        Public Shared Function ToMD5(ByVal value As String) As String
            If String.IsNullOrEmpty(value) Then
                Return value
            End If
            Using md As MD5 = MD5.Create
                Dim buffer As Byte() = md.ComputeHash(Encoding.UTF8.GetBytes(value))
                Dim builder As New StringBuilder
                Dim i As Integer
                For i = 0 To buffer.Length - 1
                    builder.Append(buffer(i).ToString("x2"))
                Next i
                Return builder.ToString
            End Using
        End Function

    End Class
End Namespace

