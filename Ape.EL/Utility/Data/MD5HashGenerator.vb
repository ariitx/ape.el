﻿Imports System.Collections.Generic
Imports System.Text
Imports System.Security.Cryptography
Imports System.IO
Imports System.Runtime.Serialization.Formatters.Binary
Imports System.Runtime.Serialization
Imports System.Reflection
Imports System.Threading

Namespace Utility.Data
    ''' <summary>
    ''' This class takes an object, and generates a key to it. There are several possibilities:
    ''' This generator can generate keys of type integer,float,double. The generated key is not necessarily
    ''' unique!
    ''' </summary>
    Public Class MD5HashGenerator
        Private Shared ReadOnly locker As New [Object]()

        ''' <summary>
        ''' Generates a hashed - key for an instance of a class.
        ''' The hash is a classic MD5 hash (e.g. BF20EB8D2C4901112179BF5D242D996B). So you can distinguish different 
        ''' instances of a class. Because the object is hashed on the internal state, you can also hash it, then send it to
        ''' someone in a serialized way. Your client can then deserialize it and check if it is in
        ''' the same state.
        ''' The method just just estimates that the object implements the ISerializable interface. What's
        ''' needed to save the state or so, is up to the implementer of the interface.
        ''' <b>The method is thread-safe!</b>
        ''' </summary>
        ''' <param name="sourceObject">The object you'd like to have a key out of it.</param>
        ''' <returns>An string representing a MD5 Hashkey corresponding to the object or null if the object couldn't be serialized.</returns>
        ''' <exception cref="ApplicationException">Will be thrown if the key cannot be generated.</exception>
        Public Shared Function GenerateKey(sourceObject As [Object]) As [String]
            Dim hashString As [String] = ""

            'Catch unuseful parameter values
            If sourceObject Is Nothing Then
                Throw New ArgumentNullException("Null as parameter is not allowed")
            Else
                'We determine if the passed object is really serializable.
                Try
                    'Now we begin to do the real work.
                    hashString = ComputeHash(ObjectToByteArray(sourceObject))
                    Return hashString
                Catch ame As AmbiguousMatchException
                    Throw New ApplicationException("Could not definitely decide if object is serializable. Message:" + ame.Message)
                End Try
            End If
        End Function

        ''' <summary>
        ''' Converts an object to an array of bytes. This array is used to hash the object.
        ''' </summary>
        ''' <param name="objectToSerialize">Just an object</param>
        ''' <returns>A byte - array representation of the object.</returns>
        ''' <exception cref="SerializationException">Is thrown if something went wrong during serialization.</exception>
        Private Shared Function ObjectToByteArray(objectToSerialize As [Object]) As Byte()
            Dim fs As New MemoryStream()
            Dim formatter As New BinaryFormatter()
            Try
                'Here's the core functionality! One Line!
                'To be thread-safe we lock the object
                SyncLock locker
                    formatter.Serialize(fs, objectToSerialize)
                End SyncLock
                Return fs.ToArray()
            Catch se As SerializationException
                Console.WriteLine("Error occured during serialization. Message: " + se.Message)
                Return Nothing
            Finally
                fs.Close()
            End Try
        End Function

        ''' <summary>
        ''' Generates the hashcode of an given byte-array. The byte-array can be an object. Then the
        ''' method "hashes" this object. The hash can then be used e.g. to identify the object.
        ''' </summary>
        ''' <param name="objectAsBytes">bytearray representation of an object.</param>
        ''' <returns>The MD5 hash of the object as a string or null if it couldn't be generated.</returns>
        Private Shared Function ComputeHash(objectAsBytes As Byte()) As String
            Dim md5 As MD5 = New MD5CryptoServiceProvider()
            Try
                Dim result As Byte() = md5.ComputeHash(objectAsBytes)

                ' Build the final string by converting each byte
                ' into hex and appending it to a StringBuilder
                Dim sb As New StringBuilder()
                For i As Integer = 0 To result.Length - 1
                    sb.Append(result(i).ToString("X2"))
                Next

                ' And return it
                Return sb.ToString()
            Catch ane As ArgumentNullException
                'If something occured during serialization, this method is called with an null argument. 
                Console.WriteLine("Hash has not been generated.")
                Return Nothing
            End Try
        End Function
    End Class
End Namespace
