﻿Imports System.IO

Namespace Utility.Data
    ''' <summary>
    ''' Provide simple functions to read and write file.
    ''' </summary>
    Public Class FileStreamer
        ''' <summary>
        ''' Retrieve byte array from file.
        ''' </summary>
        Public Shared Function ReadFileToByteArray(ByVal path As String, Optional ByVal codepage As Integer = 437) As Byte()
            Try
                Dim content As String = ReadFile(path)
                Return System.Text.Encoding.GetEncoding(codepage).GetBytes(content) 'Convert string to byte array
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''' <summary>
        ''' Append byte array as string to file.
        ''' </summary>
        Public Shared Function WriteFileFromByteArray(ByVal path As String, ByVal byteMessage As Byte(), Optional ByVal codepage As Integer = 437) As Boolean
            Try
                Dim content As String = System.Text.Encoding.GetEncoding(codepage).GetString(byteMessage) 'Convert byte array as string
                Return WriteFile(path, content)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''' <summary>
        ''' Retrieve string from file.
        ''' </summary>
        Public Shared Function ReadFile(ByVal path As String) As String
            Dim content As String = String.Empty

            Try
                If System.IO.File.Exists(path) Then
                    Using sr As New StreamReader(path)
                        content = sr.ReadToEnd
                    End Using
                End If
            Catch ex As Exception
                Throw ex
            End Try

            Return content
        End Function

        ''' <summary>
        ''' Append string to file.
        ''' </summary>
        ''' <param name="append">If true, append the message to file. Otherwise create as new file. Default is true.</param>
        Public Shared Function WriteFile(ByVal path As String, ByVal message As String, Optional ByVal append As Boolean = True) As Boolean
            Try
                If append Then
                    Using streamWriter As StreamWriter = System.IO.File.AppendText(path)
                        streamWriter.Write(message)
                        streamWriter.Flush()
                    End Using
                Else
                    Using streamWriter As StreamWriter = System.IO.File.CreateText(path)
                        streamWriter.Write(message)
                        streamWriter.Flush()
                    End Using
                End If
            Catch ex As Exception
                Throw ex
            End Try

            Return True
        End Function
    End Class
End Namespace
