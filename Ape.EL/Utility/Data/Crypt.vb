﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.IO
Imports System.Security.Cryptography
Imports System.Text.RegularExpressions
Imports System.Runtime.Serialization.Formatters.Binary

Namespace Utility.Data
    <System.Diagnostics.DebuggerStepThrough()> _
    Public Class Crypt
        '        Private ReadOnly lic As Boolean = Internal.CheckMyLicense

        ''' <summary>
        ''' Customizable Rijndael encryption class.
        ''' </summary>
        <System.Diagnostics.DebuggerStepThrough()> _
        Public Class Rijndael
            ''' <summary>
            ''' Password algorithm types that can be used in deriving password bytes.
            ''' </summary>
            Public Enum EPasswordAlgorithm
                ''' <summary>PasswordDeriveBytes</summary>
                PBKDF1
                ''' <summary>Rfc2898DeriveBytes</summary>
                PBKDF2
            End Enum

            Property PasswordAlgorithm As EPasswordAlgorithm
                Get
                    Return _PasswordAlgorithm
                End Get
                Set(value As EPasswordAlgorithm)
                    _PasswordAlgorithm = value
                End Set
            End Property
            Private _PasswordAlgorithm As EPasswordAlgorithm

            Private Const keysize As Integer = 256
            Private Const hashAlgorithm As String = "SHA1"
            Private Shared InitVectorBytes As Byte() = Nothing
            Private Shared SaltValueBytes As Byte() = Nothing
            Private Shared PasswordIteration As Integer = 100

            ''' <summary>
            ''' Construct a new Rijndael encryption using default setting. Rfc2898 will be used as password derivatives.
            ''' </summary>
            Public Sub New()
                'This default setting is used Crypt.Decyrpt and Crypt.Encrypt functions
                PasswordAlgorithm = EPasswordAlgorithm.PBKDF2
                Initiate("@1B2c3D4e5F6g7H8", "s@1tValue", 1)
            End Sub

            ''' <summary>
            ''' Construct a new Rijndael encryption using preferred setting.
            ''' </summary>
            Public Sub New(ByVal initVector As String, ByVal salt As String, ByVal passIterator As Integer)
                Initiate(initVector, salt, passIterator)
            End Sub

            ''' <summary>
            ''' Construct a new Rijndael encryption using preferred setting.
            ''' </summary>
            Public Sub New(ByVal initVector As String, ByVal salt As String, ByVal passIterator As Integer, ByVal passAlgo As EPasswordAlgorithm)
                PasswordAlgorithm = passAlgo
                Initiate(initVector, salt, passIterator)
            End Sub

            Private Sub Initiate(ByVal initVector As String, ByVal salt As String, ByVal passIterator As Integer)
                If initVector.IsEmpty Then
                    Rijndael.InitVectorBytes = Nothing
                Else
                    Rijndael.InitVectorBytes = Encoding.ASCII.GetBytes(initVector)
                End If

                If salt.IsEmpty Then
                    Rijndael.SaltValueBytes = Nothing
                Else
                    Rijndael.SaltValueBytes = Encoding.ASCII.GetBytes(salt)
                End If

                Rijndael.PasswordIteration = passIterator
            End Sub

            Public Function Encrypt(ByVal plainText As String, ByVal passPhrase As String) As String
                Dim str As String
                Dim buffer As Byte() = Encoding.UTF8.GetBytes(plainText)
                Dim bytes As DeriveBytes = Nothing
                Select Case PasswordAlgorithm
                    Case EPasswordAlgorithm.PBKDF1
                        bytes = New PasswordDeriveBytes(passPhrase, Rijndael.SaltValueBytes, hashAlgorithm, PasswordIteration)
                    Case EPasswordAlgorithm.PBKDF2
                        bytes = New Rfc2898DeriveBytes(passPhrase, Rijndael.SaltValueBytes, PasswordIteration)
                End Select
                Dim rgbKey As Byte() = bytes.GetBytes(keysize \ 8)
                Using managed As RijndaelManaged = New RijndaelManaged
                    managed.Mode = CipherMode.CBC
                    Using transform As ICryptoTransform = managed.CreateEncryptor(rgbKey, Rijndael.InitVectorBytes)
                        Using stream As MemoryStream = New MemoryStream
                            Using stream2 As CryptoStream = New CryptoStream(stream, transform, CryptoStreamMode.Write)
                                stream2.Write(buffer, 0, buffer.Length)
                                stream2.FlushFinalBlock()
                                str = Convert.ToBase64String(stream.ToArray)
                            End Using
                        End Using
                    End Using
                End Using
                bytes = Nothing
                Return str
            End Function

            Public Function Decrypt(ByVal cipherText As String, ByVal passPhrase As String) As String
                Dim str As String
                Dim buffer As Byte() = Convert.FromBase64String(cipherText)
                Dim bytes As DeriveBytes = Nothing
                Select Case PasswordAlgorithm
                    Case EPasswordAlgorithm.PBKDF1
                        bytes = New PasswordDeriveBytes(passPhrase, Rijndael.SaltValueBytes, hashAlgorithm, PasswordIteration)
                    Case EPasswordAlgorithm.PBKDF2
                        bytes = New Rfc2898DeriveBytes(passPhrase, Rijndael.SaltValueBytes, PasswordIteration)
                End Select
                Dim rgbKey As Byte() = bytes.GetBytes(keysize \ 8)
                Using managed As RijndaelManaged = New RijndaelManaged
                    managed.Mode = CipherMode.CBC
                    Using transform As ICryptoTransform = managed.CreateDecryptor(rgbKey, Rijndael.InitVectorBytes)
                        Using stream As MemoryStream = New MemoryStream(buffer)
                            Using stream2 As CryptoStream = New CryptoStream(stream, transform, CryptoStreamMode.Read)
                                Dim buffer3 As Byte() = New Byte(buffer.Length - 1) {}
                                Dim count As Integer = stream2.Read(buffer3, 0, buffer3.Length)
                                str = Encoding.UTF8.GetString(buffer3, 0, count)
                            End Using
                        End Using
                    End Using
                End Using
                bytes = Nothing
                Return str
            End Function
        End Class

        ''' <summary>
        ''' md5 hashing algo; sauce: http://goo.gl/VlMFl
        ''' </summary>
        ''' <param name="input"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetHash(ByVal input As String, Optional ByVal salt As String = "") As String
            ' step 1, calculate MD5 hash from input
            Dim md5 As MD5 = System.Security.Cryptography.MD5.Create()
            Dim inputBytes As Byte() = System.Text.Encoding.ASCII.GetBytes(input + salt)
            Dim hash As Byte() = md5.ComputeHash(inputBytes)

            ' step 2, convert byte array to hex string
            Dim sb As New StringBuilder()
            For i As Integer = 0 To hash.Length - 1
                sb.Append(hash(i).ToString("x2"))
            Next
            Return sb.ToString()
        End Function

        ''' <summary>
        ''' encryption/decryption process based on http://www.obviex.com/samples/Encryption.aspx
        ''' Obviex (c.2002). How To: Encrypt and Decrypt Data Using a Symmetric (Rijndael) Key (C#/VB.NET)
        ''' </summary>
        ''' <param name="plainText"></param>
        ''' <param name="passPhrase"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Encrypt(ByVal plainText As String, ByVal passPhrase As String) As String
            Dim cipherText As String
            Dim oRijndael As New Rijndael
            cipherText = oRijndael.Encrypt(plainText, passPhrase)
            Return cipherText
        End Function

        Public Shared Function Decrypt(ByVal cipherText As String, ByVal passPhrase As String) As String
            Dim plainText As String
            Dim oRijndael As New Rijndael
            plainText = oRijndael.Decrypt(cipherText, passPhrase)
            Return plainText
        End Function

        Public Shared Function chkPass(ByVal pw As [String]) As Boolean
            Dim Minimum_Length As Integer = 8
            Dim Upper_Case_length As Integer = 1
            Dim Lower_Case_length As Integer = 1
            Dim NonAlpha_length As Integer = 0
            Dim Numeric_length As Integer = 1

            If pw.Length < Minimum_Length Then
                Throw New Exception("Password must be at least 8 characters long!")
            ElseIf Regex.Matches(pw, "[A-Z]").Count < Upper_Case_length Then
                Throw New Exception("Password must be at least have an upper-case character!")
            ElseIf Regex.Matches(pw, "[a-z]").Count < Lower_Case_length Then
                Throw New Exception("Password must be at least have a lower-case character!")
            ElseIf Regex.Matches(pw, "[0-9]").Count < Numeric_length Then
                Throw New Exception("Password must be at least have a number!")
            ElseIf Regex.Matches(pw, "[^0-9a-zA-Z\._]").Count < NonAlpha_length Then
                Throw New Exception("Password must be at least have a non-alpha character!")
            Else
                Return False
            End If
            'everything is ok
            Return True
            'not ok, in the usage, make return, cancel everything ahead
        End Function

        Public Shared Function Rot13(ByVal value As String) As String
            Dim intN As UInt32 = 13
            ' Could be stored as integers directly.
            Dim lowerA As Integer = Asc("a"c)
            Dim lowerZ As Integer = Asc("z"c)
            Dim lowerM As Integer = Asc("m"c)

            Dim upperA As Integer = Asc("A"c)
            Dim upperZ As Integer = Asc("Z"c)
            Dim upperM As Integer = Asc("M"c)

            Dim no0 As Integer = Asc("0"c)
            Dim no4 As Integer = Asc("4"c)
            Dim no9 As Integer = Asc("9"c)

            ' Convert to character array.
            Dim array As Char() = value.ToCharArray

            ' Loop over string.
            Dim i As Integer
            For i = 0 To array.Length - 1

                ' Convert to integer.
                Dim number As Integer = Asc(array(i))

                If number >= upperA And number <= lowerZ Then
                    ' Shift letters.
                    If ((number >= lowerA) AndAlso (number <= lowerZ)) Then
                        If (number > lowerM) Then
                            number -= intN
                        Else
                            number += intN
                        End If
                    ElseIf ((number >= upperA) AndAlso (number <= upperZ)) Then
                        If (number > upperM) Then
                            number -= intN
                        Else
                            number += intN
                        End If
                    End If
                ElseIf number >= no0 And number <= no9 Then
                    ' Shift numeric
                    If number > no4 Then
                        number -= 5
                    Else
                        number += 5
                    End If
                End If
                ' Convert to character.
                array(i) = Chr(number)
            Next i

            ' Return string.
            Return New String(array)
        End Function

        ''' <summary>
        ''' Convert an object to a byte array.
        ''' </summary>
        ''' <param name="obj"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function ObjectToByteArray(ByVal obj As [Object]) As Byte()
            If obj Is Nothing Then
                Return Nothing
            End If
            Dim bf As New BinaryFormatter()
            Dim ms As New MemoryStream()
            bf.Serialize(ms, obj)
            Return ms.ToArray()
        End Function

        ''' <summary>
        ''' Convert a byte array to an Object.
        ''' </summary>
        ''' <param name="arrBytes"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function ByteArrayToObject(ByVal arrBytes As Byte()) As [Object]
            If arrBytes Is Nothing Then
                Return Nothing
            End If

            Dim memStream As New MemoryStream()
            Dim binForm As New BinaryFormatter()
            memStream.Write(arrBytes, 0, arrBytes.Length)
            memStream.Seek(0, SeekOrigin.Begin)
            Dim obj As [Object] = DirectCast(binForm.Deserialize(memStream), [Object])
            Return obj
        End Function

        'Concat license key hash
        Public Shared Function ConcatHash(ByVal str As String) As System.Text.StringBuilder
            Dim sbHash As New System.Text.StringBuilder
            If str.Length >= 32 Then
                sbHash.Append(Mid(str, 1, 4))
                sbHash.Append(Mid(str, 15, 4))
                sbHash.Append(Mid(str, 29, 4))
            Else
                sbHash.Append(Mid(str, 1, 4))
                sbHash.Append(Mid(str, str.Length \ 2, 4))
                sbHash.Append(Mid(str, str.Length - 3, 4))
            End If

            Return sbHash
        End Function
    End Class
End Namespace
