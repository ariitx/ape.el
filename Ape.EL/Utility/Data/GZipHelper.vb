﻿Imports System
Imports System.IO
Imports System.IO.Compression
Imports System.Text

Namespace Utility.Data
    Public Class GZipHelper
        ' Methods
        Public Shared Function CompressString(ByVal rawString As String) As String
            If String.IsNullOrEmpty(rawString) OrElse rawString.Length = 0 Then Return ""
            Return Convert.ToBase64String(GZipHelper.Compress(Encoding.UTF8.GetBytes(rawString.ToString())))
        End Function

        Private Shared Function Compress(ByVal rawData As Byte()) As Byte()
            Dim memoryStream As MemoryStream = New MemoryStream()
            Dim num1 As Integer = 1
            Dim num2 As Integer = 1
            Dim gzipStream As GZipStream = New GZipStream(CType(memoryStream, Stream), CType(num1, CompressionMode), num2 <> 0)
            Dim buffer As Byte() = rawData
            Dim offset As Integer = 0
            Dim length As Integer = rawData.Length
            gzipStream.Write(buffer, offset, length)
            gzipStream.Close()
            Return memoryStream.ToArray()
        End Function

        Public Shared Function DecompressString(ByVal zippedString As String) As String
            If String.IsNullOrEmpty(zippedString) OrElse zippedString.Length = 0 Then Return ""
            Return Encoding.UTF8.GetString(GZipHelper.Decompress(Convert.FromBase64String(zippedString.ToString())))
        End Function

        Public Shared Function Decompress(ByVal zippedData As Byte()) As Byte()
            Dim gzipStream As GZipStream = New GZipStream(CType(New MemoryStream(zippedData), Stream), CompressionMode.Decompress)
            Dim memoryStream As MemoryStream = New MemoryStream()
            Dim buffer As Byte() = New Byte(1023) {}

            While True
                Dim count As Integer = gzipStream.Read(buffer, 0, buffer.Length)

                If count > 0 Then
                    memoryStream.Write(buffer, 0, count)
                Else
                    Exit While
                End If
            End While

            gzipStream.Close()
            Return memoryStream.ToArray()
        End Function
    End Class
End Namespace

