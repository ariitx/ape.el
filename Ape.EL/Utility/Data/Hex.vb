﻿Namespace Utility.Data
    Public Class Hex
        Public Shared Function ByteToHex(ByVal comByte As Byte()) As String
            'create a new StringBuilder object
            Dim builder As New System.Text.StringBuilder(comByte.Length * 3)
            'loop through each byte in the array
            For Each data As Byte In comByte
                'convert the byte to a string and add to the stringbuilder
                builder.Append(Convert.ToString(data, 16).PadLeft(2, "0"c).PadRight(3, " "c))
            Next
            'return the converted value
            Return builder.ToString().ToUpper()
        End Function

        Public Shared Function HexToByte(ByVal hexString As String) As Byte()
            'Dim hexString As String = "01050001FFFF8FFB"
            Dim length As Integer = hexString.Length
            Dim upperBound As Integer = length \ 2
            If length Mod 2 = 0 Then
                upperBound -= 1
            Else
                hexString = "0" & hexString
            End If
            Dim bytes(upperBound) As Byte
            For i As Integer = 0 To upperBound
                bytes(i) = Convert.ToByte(hexString.Substring(i * 2, 2), 16)
            Next
            Return bytes
        End Function

        Public Shared Function StrToHex(ByRef Data As String, Optional ByVal DigitPerConvert As Integer = 2) As String
            Dim sVal As String
            Dim sHex As String = ""
            While Data.Length > 0
                sVal = Conversion.Hex(Strings.Asc(Data.Substring(0, 1).ToString()))
                sVal = sVal.PadLeft(DigitPerConvert, "0"c)
                Data = Data.Substring(1, Data.Length - 1)
                sHex = sHex & sVal
            End While
            Return sHex
        End Function

        Public Shared Function HexToStr(ByRef data As String) As String
            Dim text As New System.Text.StringBuilder(data.Length \ 2)
            For i As Integer = 0 To data.Length - 2 Step 2
                text.Append(Chr(Convert.ToByte(data.Substring(i, 2), 16)))
            Next
            Return text.ToString
        End Function

        Public Shared Function HexToDec(ByVal sHex As String) As Decimal
            Dim sResult As Decimal
            Dim S As String = sHex
            Dim I As Long = 0 'Integer = 0
            If IsHex(sHex) Then
                I = CInt("&H" & sHex)
                sResult = I
            Else
                sResult = 0
            End If

            Return sResult
        End Function

        Public Shared Function IsHex(ByVal strHex As String) As Boolean
            If IsNumeric(strHex) Then
                Return True
            Else
                If strHex.Trim <> "" Then
                    For i = 1 To Len(strHex)
                        Select Case UCase(Mid(strHex, i, 1))
                            Case 0 To 9
                            Case "A" To "F"
                            Case Else
                                Return False
                        End Select
                    Next
                    Return True
                Else
                    Return False
                End If
            End If
        End Function
    End Class
End Namespace
