﻿Namespace Utility
    Public Class TypeHelper
'        Private ReadOnly lic As Boolean = Internal.CheckMyLicense

        ''' <summary>
        ''' Alternative to Convert.ChangeType and CTypeDynamic to handle object type conversion through dynamic variable type.
        ''' sauce: http://forums.asp.net/post/4167011.aspx
        ''' </summary>
        ''' <param name="value"></param>
        ''' <param name="type"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function ChangeType(value As Object, type As Type) As Object
            If type Is GetType(Boolean) Then
                If value Is Nothing Then
                    value = False
                ElseIf TypeOf value Is Boolean Then
                    value = CBool(value)
                Else
                    Dim d As Double
                    Dim s As String = value.ToString().Trim()
                    ' t/f
                    ' true/false
                    ' y/n
                    ' yes/no
                    ' <>0/0
                    If s.StartsWith("F", StringComparison.OrdinalIgnoreCase) OrElse s.StartsWith("N", StringComparison.OrdinalIgnoreCase) Then
                        value = False
                    ElseIf Double.TryParse(s, d) AndAlso d = 0 Then
                        ' numeric zero
                        value = False
                    Else
                        value = True
                    End If
                End If
            ElseIf type.IsEnum Then
                value = [Enum].Parse(type, value.ToString(), True)
            ElseIf type Is GetType(Guid) Then
                ' If it's already a guid, return it.
                If Not (TypeOf value Is Guid) Then
                    If TypeOf value Is String Then
                        value = New Guid(value.ToString())
                    Else
                        value = New Guid(DirectCast(value, Byte()))
                    End If
                End If
            Else
                value = Convert.ChangeType(value, type)
            End If
            Return value
        End Function
    End Class
End Namespace
