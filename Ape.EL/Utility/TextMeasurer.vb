﻿
Imports System
Imports System.Drawing

Namespace Utility
    Public Class TextMeasurer
        Private m_graphics As Graphics

        Public Sub New()
            Me.m_graphics = Graphics.FromHwnd(IntPtr.Zero)
        End Sub

        Public Function MeasureString(text As String, font As Font) As SizeF
            Return Me.m_graphics.MeasureString(text, font)
        End Function

        Public Function MeasureString(text As String, font As Font, format As StringFormat) As SizeF
            Return Me.m_graphics.MeasureString(text, font, Integer.MaxValue, format)
        End Function

        Public Function MeasureString(text As String, font As Font, maxWidth As Integer, format As StringFormat) As SizeF
            Return Me.m_graphics.MeasureString(text, font, maxWidth, format)
        End Function
    End Class
End Namespace
