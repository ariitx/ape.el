﻿Imports System.Net
Imports System.Net.NetworkInformation
Imports System.ServiceProcess
Imports Ape.EL.Utility.General
Imports System
Imports System.Globalization
Imports System.Net.Sockets
Imports System.Security
Imports System.Text

Namespace Utility
    Public Class Network
'        Private ReadOnly lic As Boolean = Internal.CheckMyLicense

        Private Shared Function GetServerName(ByVal appPath As String) As IPHostEntry
            Dim strHostName As String = ""

            Dim strClient As String = appPath.ToString.Substring(2)
            strHostName = strClient.ToString.Substring(0, InStr(strClient, "\") - 1)
            Return Dns.GetHostEntry(strHostName)
        End Function

        Public Shared Function GetServerIP(ByVal AppPath As String) As String
            Dim hostname As IPHostEntry = GetServerName(AppPath)
            Dim strHostIP As String = ""

            For intCount = 0 To hostname.AddressList.Count - 1
                If Mid(hostname.AddressList(intCount).ToString, 1, 1) = "1" Then
                    strHostIP = hostname.AddressList(intCount).ToString
                    Exit For
                End If
            Next
            If strHostIP <> "" Then
                Return strHostIP
            Else
                Return hostname.AddressList(0).ToString
            End If
        End Function

        Public Shared Function GetIP(Optional ByVal strRemoteIP As String = "") As String
            Dim strIPAddress As String
            Dim strMyStartUpPath As String = GetStartUpPathDirectory()

            If Not String.IsNullOrEmpty(strRemoteIP) Then
                'Remote IP
                strIPAddress = strRemoteIP

            ElseIf IsNetworkUser(GetStartUpPath) Then
                'Network IP
                strIPAddress = GetServerIP(strMyStartUpPath)

            Else
                'Local IP
                strIPAddress = "127.0.0.1"
            End If

            Return strIPAddress
        End Function

        ''' <summary>
        ''' Check whether targeted IP can be connected.
        ''' </summary>
        ''' <param name="strIP"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function CheckPingIP(ByVal strIP As String) As Boolean
            Try
                Dim ChkPing As New Ping

                Dim PingReply As System.Net.NetworkInformation.PingReply
                PingReply = ChkPing.Send(strIP)

                If PingReply.Status = IPStatus.Success Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception
                Return False
            End Try
        End Function

        ''' <summary>
        ''' Checks to see if a TCP port is open on a specified device.
        ''' </summary>
        ''' <param name="Host">The name or IP address of the device to check for the open port on</param>
        ''' <param name="PortNumber">The TCP port to test</param>
        Public Shared Function IsPortOpen(ByVal Host As String, ByVal PortNumber As Integer) As Boolean
            Dim Client As System.Net.Sockets.TcpClient = Nothing
            Try
                Client = New System.Net.Sockets.TcpClient(Host, PortNumber)
                Return True
            Catch ex As System.Net.Sockets.SocketException
                Return False
            Finally
                If Not Client Is Nothing Then
                    Client.Close()
                End If
            End Try
        End Function

        ''' <summary>
        ''' Check and reset default MSSQL$SQLEXPRESS service if it does not respond.
        ''' </summary>
        ''' <param name="strServiceName">By default is MSSQL$SQLEXPRESS.</param>
        ''' <remarks></remarks>
        Public Shared Sub CheckDBStatus(Optional ByVal strServiceName As String = "MSSQL$SQLEXPRESS")
            Dim myServiceName As String = strServiceName ' use service name to check
            Dim status As String = ""
            Dim mySC As ServiceController = New ServiceController(myServiceName)
            Dim intYes As Integer = 0

            Try
                status = mySC.Status.ToString
            Catch ex As Exception
            End Try

            Select Case status
                Case "Stopped"
                    mySC.Start()
                Case "Paused"
                    mySC.Continue()
            End Select

            mySC.Close()
        End Sub

        ''' <summary>
        ''' Perform sql instance restart.
        ''' </summary>
        ''' <param name="strServiceName"></param>
        ''' <remarks></remarks>
        Public Shared Sub RestartDBService(Optional ByVal strServiceName As String = "MSSQL$SQLEXPRESS")
            Dim myServiceName As String = strServiceName ' use service name to check
            Dim status As String = ""
            Dim mySC As ServiceController = New ServiceController(myServiceName)
            Dim intYes As Integer = 0
            mySC.Stop()
            Do
                mySC.Refresh()
                If mySC.Status = ServiceControllerStatus.Stopped Then
                    mySC.Start()
                    Exit Do
                End If
            Loop
            mySC.Close()
        End Sub

        '~~~~stolen code NetHelper
        Public Class Net
    '        Private ReadOnly lic As Boolean = Internal.CheckMyLicense

            Private Shared Function IsPrivateIPAddress(ip As IPAddress) As Boolean
                If ip.AddressFamily <> AddressFamily.InterNetwork Then
                    Return False
                End If
                Dim array As String() = ip.ToString().Split(New Char() {"."c})
                If Convert.ToInt32(array(0)) = 10 Then
                    Return True
                End If
                If Convert.ToInt32(array(0)) = 172 AndAlso Convert.ToInt32(array(1)) >= 16 AndAlso Convert.ToInt32(array(1)) <= 31 Then
                    Return True
                End If
                If Convert.ToInt32(array(0)) = 192 AndAlso Convert.ToInt32(array(1)) = 168 Then
                    Return True
                End If
                If Convert.ToInt32(array(0)) = 195 AndAlso Convert.ToInt32(array(1)) = 196 Then
                    Return True
                End If
                If Convert.ToInt32(array(0)) = 169 AndAlso Convert.ToInt32(array(1)) = 100 Then
                    Return True
                End If
                If Convert.ToInt32(array(0)) = 53 AndAlso Convert.ToInt32(array(1)) = 129 Then
                    Return True
                End If
                If Convert.ToInt32(array(0)) = 169 AndAlso Convert.ToInt32(array(1)) = 254 Then
                    Return True
                End If
                If Convert.ToInt32(array(0)) = 127 AndAlso Convert.ToInt32(array(1)) = 0 AndAlso Convert.ToInt32(array(2)) = 0 AndAlso Convert.ToInt32(array(3)) = 1 Then
                    Return True
                End If
                Try
                    Dim hostName As String = Dns.GetHostName()
                    Dim hostEntry As IPHostEntry = Dns.GetHostEntry(hostName)
                    For i As Integer = 0 To hostEntry.AddressList.Length - 1
                        Dim array2 As String() = hostEntry.AddressList(i).ToString().Split(New Char() {"."c})
                        If array2.Length >= 3 AndAlso array2(0) = array(0) AndAlso array2(1) = array(1) AndAlso array2(2) = array(2) Then
                            Return True
                        End If
                    Next
                Catch ex_1A1 As SocketException
                Catch ex_1A4 As SecurityException
                End Try
                Return False
            End Function

            Private Shared Function GetIPAddresses(host As String) As IPHostEntry
                host = host.ToLowerInvariant().Trim()
                host = host.Split(New Char() {"\"c, ","c})(0)
                If host.Length = 0 OrElse String.Compare(host, "(local)", True, CultureInfo.InvariantCulture) = 0 OrElse String.Compare(host, "(localdb)", True, CultureInfo.InvariantCulture) = 0 OrElse String.Compare(host, "localhost", True, CultureInfo.InvariantCulture) = 0 OrElse host = "." Then
                    Return SimpleGetHostByAddress(IPAddress.Parse("127.0.0.1"))
                End If
                Dim ip As IPAddress = Nothing
                If IPAddress.TryParse(host, ip) Then
                    Return SimpleGetHostByAddress(ip)
                End If
                Try
                    Return Dns.GetHostEntry(host)
                Catch ex_A1 As Exception
                End Try
                Return SimpleGetHostByAddress(IPAddress.Parse("127.0.0.1"))
            End Function

            Private Shared Function SimpleGetHostByAddress(ip As IPAddress) As IPHostEntry
                Dim iPHostEntry As IPHostEntry = New IPHostEntry()
                iPHostEntry.AddressList = New IPAddress(1 - 1) {}
                iPHostEntry.AddressList(0) = ip
                Return iPHostEntry
            End Function

            Public Shared Function IsIntranetHost(host As String) As Boolean
                Dim iPAddresses As IPHostEntry = GetIPAddresses(host)
                If iPAddresses.AddressList.Length = 1 Then
                    Return IsPrivateIPAddress(iPAddresses.AddressList(0))
                End If
                For i As Integer = 0 To iPAddresses.AddressList.Length - 1
                    If IsPrivateIPAddress(iPAddresses.AddressList(i)) Then
                        Return True
                    End If
                Next
                Return False
            End Function
        End Class

        '~~~~stolen code ServerNameHelper
        Public Class ServerName
    '        Private ReadOnly lic As Boolean = Internal.CheckMyLicense

            Public Shared Function GetHostName(serverName As String) As String
                Dim stringBuilder As StringBuilder = New StringBuilder(serverName.Length)
                For i As Integer = 0 To serverName.Length - 1
                    Dim c As Char = serverName(i)
                    If c = "\" OrElse c = "," Then
                        Exit For
                    End If
                    stringBuilder.Append(Char.ToLowerInvariant(c))
                Next
                Return stringBuilder.ToString()
            End Function

            Public Shared Function IsLocalServerName(serverName As String) As Boolean
                Dim hostName As String = GetHostName(serverName)
                Return IsLocalHostName(hostName)
            End Function

            Public Shared Function GetRealHostName(serverName As String) As String
                Dim result As String = String.Empty
                IsLocalPC(serverName, result)
                Return result
            End Function

            Public Shared Function IsLocalPC(serverName As String, ByRef realHostName As String) As Boolean
                Dim hostName As String = GetHostName(serverName)
                If IsLocalHostName(hostName) Then
                    realHostName = Dns.GetHostName()
                    Return True
                End If
                realHostName = hostName
                Try
                    Dim hostEntry As IPHostEntry = Dns.GetHostEntry(Dns.GetHostName())
                    Dim hostEntry2 As IPHostEntry = Dns.GetHostEntry(hostName)
                    realHostName = hostEntry2.HostName
                    For i As Integer = 0 To hostEntry.AddressList.Length - 1
                        For j As Integer = 0 To hostEntry2.AddressList.Length - 1
                            If hostEntry.AddressList(i).ToString() = hostEntry2.AddressList(j).ToString() Then
                                Return True
                            End If
                        Next
                    Next
                Catch ex_88 As SocketException
                Catch ex_8B As SecurityException
                End Try
                Return False
            End Function

            Public Shared Function ConvertToRealServerName(serverName As String) As String
                Dim stringBuilder As StringBuilder = New StringBuilder(serverName.Length)
                Dim stringBuilder2 As StringBuilder = New StringBuilder(serverName.Length)
                Dim flag As Boolean = False
                For i As Integer = 0 To serverName.Length - 1
                    Dim c As Char = serverName(i)
                    If c = "\" OrElse c = "," Then
                        flag = True
                    End If
                    If Not flag Then
                        stringBuilder.Append(Char.ToLowerInvariant(c))
                    Else
                        stringBuilder2.Append(c)
                    End If
                Next
                Dim hostname As String = stringBuilder.ToString()
                Dim stringBuilder3 As StringBuilder = New StringBuilder(100)
                Try
                    If IsLocalHostName(hostname) Then
                        stringBuilder3.Append(Dns.GetHostName())
                    Else
                        stringBuilder3.Append(Dns.GetHostEntry(serverName).HostName)
                    End If
                Catch ex_A1 As SocketException
                Catch ex_A4 As SecurityException
                End Try
                If stringBuilder3.Length = 0 Then
                    Return serverName
                End If
                stringBuilder3.Append(stringBuilder2.ToString())
                Return stringBuilder3.ToString()
            End Function

            Private Shared Function IsLocalHostName(hostname As String) As Boolean
                Return hostname = "(local)" OrElse hostname = "(localdb)" OrElse hostname = "127.0.0.1" OrElse hostname = "localhost" OrElse hostname = "."
            End Function
        End Class
    End Class
End Namespace
