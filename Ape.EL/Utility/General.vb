﻿Imports System.IO
Imports System.Reflection
Imports System.Runtime.InteropServices
Imports System.Drawing
Imports System.Drawing.Drawing2D
Imports System.Security.AccessControl
Imports System.Security.Principal
Imports Ape.EL.Internal
Imports Ape.EL.InternalGlobal
Imports Ape.EL.Utility.Data
Imports System.ComponentModel
Imports Ape.EL.Win32.User32
Imports System.Net
Imports Microsoft.Win32

Namespace Utility
    Public Class General
        '        Private ReadOnly lic As Boolean = Internal.CheckMyLicense

        ''' <summary>
        ''' Get main application's name that calls this library.
        ''' If strPath param is set, it will get strPath instead.
        ''' eg. File.EXE
        ''' </summary>
        ''' <param name="strPath">System.Reflection.Assembly.GetExecutingAssembly().Codebase</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetStartUpName(Optional ByVal strPath As String = "") As String
            If String.IsNullOrEmpty(strPath) Then
                'set calling assembly's location if strPath is not set
                strPath = GetAsmFilePath()
            End If
            Return Path.GetFileName(strPath)
        End Function

        ''' <summary>
        ''' Get main application's full path that calls this library. Include Utils.dll name.
        ''' eg. D:\Program\File.EXE
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetStartUpPath() As String
            Return GetAsmFilePath()
        End Function

        ''' <summary>
        ''' Get executing application's directory.
        ''' eg. D:\Program
        ''' Interchangeable with GetAspPhysicalPath.
        ''' </summary>
        ''' <param name="refresh">Refresh directory path again.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetStartUpPathDirectory(Optional ByVal refresh As Boolean = False) As String
            Static p As String = ""
            If refresh Then p = ""

            If p = "" Then
                p = Path.GetDirectoryName(GetAsmFilePath())
                If Not p.EndsWith("\") Then p = p & "\"
            End If

            Return p
        End Function

        ''' <summary>
        ''' Return the assembly of selected filepath.
        ''' </summary>
        Public Shared Function GetAsm(Optional ByVal filepath As String = Nothing) As Assembly
            If filepath Is Nothing Then Return Assembly.GetExecutingAssembly()

            If File.Exists(filepath) Then
                Return Assembly.LoadFrom(filepath)
            End If

            Return Nothing
        End Function

        ''' <summary>
        ''' Return namespace of the selected file path.
        ''' Return empty string if catch an error.
        ''' </summary>
        ''' <param name="root">Default is true.</param>
        Public Shared Function GetAsmNamespace(Optional ByVal filepath As String = Nothing, Optional ByVal root As Boolean = True) As String
            Dim asm As Assembly = If(filepath Is Nothing, Assembly.GetExecutingAssembly(), GetAsm(filepath))
            Return GetAsmNamespace(asm, root)
        End Function

        ''' <summary>
        ''' Return namespace of the selected assembly.
        ''' Return empty string if catch an error.
        ''' </summary>
        ''' <param name="asm"></param>
        ''' <param name="root">Default is true.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetAsmNamespace(ByVal asm As Assembly, Optional ByVal root As Boolean = True) As String
            Try
                'get the original namespace based on ".Resources.resources"
                'if none of the GetManifestResourceNames ends with the text above, take the last manifest as the namespace
                Dim strNamespace As String = String.Empty
                For i As Integer = 0 To asm.GetManifestResourceNames.Count - 1
                    If asm.GetManifestResourceNames(i).ToString.EndsWith(".Resources.resources") Or _
                            i = asm.GetManifestResourceNames.Count - 1 Then
                        strNamespace = asm.GetManifestResourceNames(i).ToString.Replace(".Resources.resources", "")
                        Exit For
                    End If
                Next

                If root Then
                    Return strNamespace.Split(".")(0)
                Else
                    Return strNamespace
                End If

                Return ""
            Catch ex As Exception
                Return ""
            End Try
        End Function

        ''' <summary>
        ''' Get members in a namepace.
        ''' </summary>
        ''' <param name="assembly"></param>
        ''' <param name="nameSpace"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetTypesInNamespace(assembly As Assembly, [nameSpace] As String) As Type()
            Return assembly.GetTypes().Where(Function(t) [String].Equals(t.[Namespace], [nameSpace], StringComparison.Ordinal)).ToArray()
        End Function

        ''' <summary>
        ''' Get all classes from an assembly to list of string.
        ''' </summary>
        Public Shared Function GetClasses(asm As Assembly) As List(Of String)
            Dim namespacelist As New List(Of String)()
            Dim classlist As New List(Of String)()

            For Each type As Type In asm.GetTypes()
                namespacelist.Add(type.FullName)
            Next

            For Each classname As String In namespacelist
                classlist.Add(classname)
            Next

            Return classlist
        End Function

        ''' <summary>
        ''' Get current an assembly's GUID. If assembly not provided, it will get executing assembly.
        ''' </summary>
        Public Shared Function GetAssemblyGUID(Optional ByVal asm As Assembly = Nothing) As String
            If asm Is Nothing Then asm = Assembly.GetExecutingAssembly()
            Dim attribute As Object = DirectCast(asm.GetCustomAttributes(GetType(GuidAttribute), True)(0), GuidAttribute)
            Dim id As String = attribute.Value
            Return id
        End Function

        ''' <summary>
        ''' Check whether the assembly is executed within network resource.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function IsNetworkUser(Optional ByVal strPath As String = "", Optional ByVal refresh As Boolean = False) As Boolean
            Static hasResult As Boolean
            Static result As Boolean

            If refresh Then hasResult = False
            If hasResult Then Return result

            If String.IsNullOrEmpty(strPath) Then
                'set calling assembly's location if strPath is not set
                Static startUpPath As String = ""
                If startUpPath = "" Or refresh Then
                    startUpPath = GetStartUpPath()
                End If
                strPath = startUpPath
            End If

            Dim strMyStartUpPath As String = Path.GetDirectoryName(strPath)

            If strMyStartUpPath.Substring(0, 1) = "\" Then
                result = True
            Else
                result = False
            End If
            hasResult = True
            Return result
        End Function

        ''' <summary>
        ''' Please read source's comment.
        ''' </summary>
        ''' <param name="bEn">Encrypt using some algorithm.</param>
        ''' <param name="bNoDash">Remove dash from GUID.</param>
        ''' <param name="str">Will be used to return MD5 hash with some operation.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetGUID(Optional ByVal bEn As Boolean = False, Optional ByVal bNoDash As Boolean = False, Optional ByVal str As String = "") As String
            If String.IsNullOrEmpty(str) Then str = IntegralVal

            'This function will return a password by by combining MD5, GUID and Rot13 algorithm.
            'Please use this function as GetGUID(1, 1, Utils.Crypt.GetHash(IntegralVal))

            Dim asm As Assembly = Assembly.GetExecutingAssembly()
            Dim attribute As Object = DirectCast(asm.GetCustomAttributes(GetType(GuidAttribute), True)(0), GuidAttribute)
            Dim id As String = attribute.Value 'GUID ac008d54-c217-919c-8dd2-ec28f3a1437d

            If bNoDash Then
                id = Replace(id, "-", "")
            End If

            If bEn Then
                id = Crypt.Rot13(id)
            End If

            str = HalfHash(str)
            If str <> "4dfc0fbf7a9f718b" Then 'only apply if str is not default value
                id = HalfHash(str) & HalfHash(id) 'half hash of str + half hash of current guid
            End If

            Return id
        End Function

        ''' <summary>
        ''' Used by GetGUID.
        ''' </summary>
        ''' <param name="str"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Shared Function HalfHash(ByVal str As String) As String
            str = Crypt.GetHash(str) 'get md5 hash of the string
            Dim array As Char() = str.ToCharArray 'convert to char array
            For i = array.Length - 1 To 0 Step -1
                If i Mod 2 = 0 Then array(i) = "-" 'for each odd number, replace with dash
            Next
            str = New String(array) 'convert char array back to string
            str = Replace(str, "-"c, "") 'remove all dash from the string
            Return str
        End Function

        ''' <summary>
        ''' Extract icon from selected path assembly and return the icon.
        ''' http://www.pinvoke.net/default.aspx/shell32/ExtractIconEx.html
        ''' </summary>
        ''' <param name="strPath">Can use Utils.General.GetStartUpPath</param>
        Public Shared Function GetAssemblyIcon(Optional ByVal strPath As String = "", Optional ByVal large As Boolean = True) As Icon
            If String.IsNullOrEmpty(strPath) Then
                'set calling assembly's location if strPath is not set
                strPath = GetAsmFilePath()
            End If

            Dim readIconCount As Integer = 0
            Dim hDummy As IntPtr() = New IntPtr(0) {IntPtr.Zero}
            Dim hIconEx As IntPtr() = New IntPtr(0) {IntPtr.Zero}

            Try
                If (large) Then
                    readIconCount = ExtractIconEx(strPath, 0, hIconEx, hDummy, 1)
                Else
                    readIconCount = ExtractIconEx(strPath, 0, hDummy, hIconEx, 1)
                End If

                If (readIconCount > 0 AndAlso Not hIconEx(0).Equals(IntPtr.Zero)) Then
                    ' GET FIRST EXTRACTED ICON
                    Dim extractedIcon As Icon = Icon.FromHandle(hIconEx(0)).Clone()
                    Return extractedIcon
                Else ' NO ICONS READ
                    Return Nothing
                End If
            Catch ex As Exception
                ' EXTRACT ICON ERROR 
                ' BUBBLE UP
                'Throw New ApplicationException("Could not extract icon", ex)
                Return Nothing
            Finally
                'RELEASE RESOURCES
                For Each ptr As IntPtr In hIconEx
                    If (Not ptr.Equals(IntPtr.Zero)) Then
                        DestroyIcon(ptr)
                    End If
                Next ptr

                For Each ptr As IntPtr In hDummy
                    If Not (ptr.Equals(IntPtr.Zero)) Then
                        DestroyIcon(ptr)
                    End If
                Next ptr
            End Try
        End Function

        ''' <summary>
        ''' Used by GetAssemblyIcon.
        ''' </summary>
        <DllImport("shell32.dll", CharSet:=CharSet.Auto)> _
        Private Shared Function ExtractIconEx(ByVal szFileName As String, _
            ByVal nIconIndex As Integer, _
            ByVal phiconLarge() As IntPtr, _
            ByVal phiconSmall() As IntPtr, _
            ByVal nIcons As UInteger) As UInteger
        End Function

        ''' <summary>
        ''' Used by GetAssemblyIcon.
        ''' </summary>
        <DllImport("user32.dll", EntryPoint:="DestroyIcon", SetLastError:=True)> _
        Private Shared Function DestroyIcon(ByVal hIcon As IntPtr) As Integer
        End Function

        ''' <summary>
        ''' Extract image from file to memory as bitmap.
        ''' </summary>
        ''' <param name="filename"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetGraphics(ByVal filename As String) As Bitmap
            Dim filepath As String = String.Format("{0}\{1}", Path.GetDirectoryName(GetStartUpPath()), filename)
            Try
                If System.IO.File.Exists(filepath) Then
                    Dim tmp As Image = New Bitmap(filepath)
                    Dim tmp2 As Image = tmp.Clone 'clone image to tmp2
                    tmp.Dispose() 'dispose tmp to release resource
                    Return New Bitmap(tmp2) 'return tmp2 that was cloned from tmp
                Else
                    'Throw New Exception
                    Return Nothing
                End If
            Catch ex As Exception
                Return Nothing
            End Try
        End Function

        ''' <summary>
        ''' Resize Image size.
        ''' </summary>
        ''' <param name="image"></param>
        ''' <param name="size"></param>
        ''' <param name="preserveAspectRatio"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function ResizeImage(ByVal image As Image, ByVal size As Size, Optional ByVal preserveAspectRatio As Boolean = True) As Image
            Try
                If image Is Nothing Then Return Nothing
                Dim newWidth As Integer
                Dim newHeight As Integer
                If preserveAspectRatio Then
                    Dim originalWidth As Integer = image.Width
                    Dim originalHeight As Integer = image.Height
                    Dim percentWidth As Single = CSng(size.Width) / CSng(originalWidth)
                    Dim percentHeight As Single = CSng(size.Height) / CSng(originalHeight)
                    Dim percent As Single = If(percentHeight < percentWidth, percentHeight, percentWidth)
                    newWidth = CInt(originalWidth * percent)
                    newHeight = CInt(originalHeight * percent)
                Else
                    newWidth = size.Width
                    newHeight = size.Height
                End If
                Dim newImage As Image = New Bitmap(newWidth, newHeight)
                Using graphicsHandle As Graphics = Graphics.FromImage(newImage)
                    graphicsHandle.InterpolationMode = InterpolationMode.HighQualityBicubic
                    graphicsHandle.DrawImage(image, 0, 0, newWidth, newHeight)
                End Using
                Return newImage
            Catch ex As Exception
                Return Nothing
            End Try
        End Function

        ''' <summary>
        ''' Reduce image resolution to reduce its size.
        ''' </summary>
        ''' <param name="img"></param>
        ''' <param name="intMaxPicSize"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function ReducePictureSize(ByVal img As System.Drawing.Image, ByVal intMaxPicSize As Integer) As Image
            Dim bmActual As System.Drawing.Image
            Dim ms As System.IO.MemoryStream = New System.IO.MemoryStream()
            Dim intLength As Integer = 0
            img.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg)
            intLength = ms.Length / 1024

            Do Until intLength <= intMaxPicSize
                img = New Bitmap(img, New Size(img.Size.Width * 0.9, img.Size.Height * 0.9))
                ms = New System.IO.MemoryStream()
                img.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg)
                intLength = ms.Length / 1024
            Loop

            bmActual = img

            Return bmActual
        End Function

        ''' <summary>
        ''' Convert image to base64 string.
        ''' </summary>
        Public Shared Function ImageToBase64(image As Image, format As System.Drawing.Imaging.ImageFormat) As String
            Using ms As New MemoryStream()
                ' Convert Image to byte[]
                image.Save(ms, format)
                Dim imageBytes As Byte() = ms.ToArray()

                ' Convert byte[] to Base64 String
                Dim base64String As String = Convert.ToBase64String(imageBytes)
                Return base64String
            End Using
        End Function

        ''' <summary>
        ''' Convert base64 string to image.
        ''' </summary>
        Public Shared Function Base64ToImage(base64String As String) As Image
            ' Convert Base64 String to byte[]
            Dim imageBytes As Byte() = Convert.FromBase64String(base64String)
            Dim ms As New MemoryStream(imageBytes, 0, imageBytes.Length)

            ' Convert byte[] to Image
            ms.Write(imageBytes, 0, imageBytes.Length)
            Dim image__1 As Image = Image.FromStream(ms, True)
            Return image__1
        End Function

        ''' <summary>
        ''' Returns Windows version in string.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetOSVersion() As String
            Select Case Environment.OSVersion.Platform
                Case PlatformID.Win32S
                    Return "Win 3.1"
                Case PlatformID.Win32Windows
                    Select Case Environment.OSVersion.Version.Minor
                        Case 0
                            Return "Win95"
                        Case 10
                            Return "Win98"
                        Case 90
                            Return "WinME"
                        Case Else
                            Return "Unknown"
                    End Select
                Case PlatformID.Win32NT
                    Select Case Environment.OSVersion.Version.Major
                        Case 3
                            Return "NT 3.51"
                        Case 4
                            Return "NT 4.0"
                        Case 5
                            Select Case _
                                Environment.OSVersion.Version.Minor
                                Case 0
                                    Return "Win2000"
                                Case 1
                                    Return "WinXP"
                                Case 2
                                    Return "Win2003"
                            End Select
                        Case 6
                            Return "Vista/Win2008Server"
                        Case Else
                            Return "Unknown"
                    End Select
                Case PlatformID.WinCE
                    Return "Win CE"
            End Select
            Return ""
        End Function

        ''' <summary>
        ''' Set the directory or file security to everyone.
        ''' </summary>
        ''' <param name="path"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function SetFullAccess(ByVal path As String) As Boolean
            Try
                If Directory.Exists(path) Then
                    Dim sec As DirectorySecurity = Directory.GetAccessControl(path)
                    ' Using this instead of the "Everyone" string means we work on non-English systems.
                    Dim everyone As New SecurityIdentifier(WellKnownSidType.WorldSid, Nothing)
                    sec.AddAccessRule(New FileSystemAccessRule(everyone, _
                                                               FileSystemRights.Modify Or FileSystemRights.Synchronize, _
                                                               InheritanceFlags.ContainerInherit Or InheritanceFlags.ObjectInherit, _
                                                               PropagationFlags.None, _
                                                               AccessControlType.Allow))
                    Directory.SetAccessControl(path, sec)

                    Return True
                ElseIf File.Exists(path) Then
                    Dim sec As FileSecurity = File.GetAccessControl(path)
                    ' Using this instead of the "Everyone" string means we work on non-English systems.
                    Dim everyone As New SecurityIdentifier(WellKnownSidType.WorldSid, Nothing)
                    sec.AddAccessRule(New FileSystemAccessRule(everyone, _
                                                               FileSystemRights.FullControl,
                                                               AccessControlType.Allow))
                    File.SetAccessControl(path, sec)

                    Return True
                End If
            Catch ex As Exception
            End Try
            Return False
        End Function

        Public Shared Sub MakePath(ByVal myPath As String)
            Try
                If Not String.IsNullOrEmpty(myPath) Then
                    If UCase(Path.GetPathRoot(myPath).ToUpper) <> UCase(myPath) Then
                        Directory.CreateDirectory(myPath)
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        ''' <summary>
        ''' Validate File Names and Paths.
        ''' </summary>
        Public Shared Function IsValidFileNameOrPath(ByVal name As String) As Boolean
            ' Determines if the name is Nothing.
            If name Is Nothing Then
                Return False
            End If

            ' Determines if there are bad characters in the name.
            For Each badChar As Char In System.IO.Path.GetInvalidPathChars
                If InStr(name, badChar) > 0 Then
                    Return False
                End If
            Next

            ' The name passes basic validation.
            Return True
        End Function

        ''' <summary>
        ''' Generate a path based on preference. For example, passing "myfolder" will give you "c:\xxx\myfolder" if the executing program is located in "c:\xxx\".
        ''' </summary>
        ''' <remarks>If path is empty, executing program directory will be used.</remarks>
        Public Shared Function GetCleanPath(ByVal path As String) As String
            path = path.Replace("/", "\")
            Dim wasLogical As Boolean = If(path.Length >= 3, Mid(path, 2, 2) = ":\", False)
            Dim replaceable As List(Of String) = New String() {":", "*", "?", """", "<", ">", "|"}.ToList 'not allowed by windows.
            For Each c As String In replaceable
                path = path.Replace(c, "")
            Next
            If wasLogical Then path = path.Insert(1, ":")
            Dim wasUNC As Boolean = path.StartsWith("\\")
            While path.StartsWith("\")
                path = path.Remove(0, 1)
            End While
            While path.Contains("\\")
                path = path.Replace("\\", "\")
            End While

            If path.Trim.IsEmpty Then path = Ape.EL.Utility.General.GetAspPhysicalPath

            Dim hasLogical As Boolean = HasLogicalDrivePath(path)


            If wasUNC AndAlso Not hasLogical Then
                path = "\\" & path 'append UNC path prefix
            Else
                If Not hasLogical Then path = Ape.EL.Utility.General.GetAspPhysicalPath & path 'if not starts with logical drive, set program directory as prefix.
            End If

            Return path
        End Function

        ''' <summary>
        ''' Determine whether the path started with available logical drives installed on running computer.
        ''' </summary>
        Public Shared Function HasLogicalDrivePath(path As String) As Boolean
            Dim listLogicalDrive As List(Of String) = System.IO.Directory.GetLogicalDrives.ToList
            For Each drive As String In listLogicalDrive
                If path.StartsWith(drive, StringComparison.OrdinalIgnoreCase) Then
                    Return True
                End If
            Next
            Return False
        End Function

        ''' <summary>
        ''' Return AMD64 for 64 bit or x86 for 32 bit.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <Obsolete("Misleading value.", True)>
        Private Shared Function GetProcessorArchitechture() As String
            Return Environment.GetEnvironmentVariable("PROCESSOR_ARCHITECTURE")
        End Function

        ''' <summary>
        ''' Check if the OS architechture is 64 bit, use Isx64En to check for the runtime process bit. Determine by checking if PROCESSOR_ARCHITECTURE is AMD64.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <Obsolete("Use Is64OS instead.", True)>
        Public Shared Function Isx64() As Boolean
            Return False
        End Function

        ''' <summary>
        ''' Alternative use of Isx64, compatible with .net 3.5 (maybe).
        ''' </summary>
        <Obsolete("Use Is64OS instead.", True)>
        Public Shared Function Isx64OS() As Boolean
            Return False
        End Function

        ''' <summary>
        ''' Alternative use of Isx64, based on mscorlib. Only avail in .net 4.0 above. On 64 Windows, it will always return true, and 32 Windows always false.
        ''' </summary>
        <Obsolete("Use Is64OS instead.", True)>
        Public Shared Function Isx64OSEx() As Boolean
            Return False
        End Function

        ''' <summary>
        ''' Check if the program is running in 64 bit, use Isx64 to check for OS architecture bit.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <Obsolete("Use Is64Proc instead.", True)>
        Public Shared Function Isx64Env() As Boolean
            Return False
        End Function

        ''' <summary>
        ''' Check if the Windows OS architechture is 64 bit.
        ''' </summary>
        Public Shared Function Is64OS() As Boolean
            Dim flag As Boolean
            Return ((IntPtr.Size = 8) OrElse ((DoesWin32MethodExist("kernel32.dll", "IsWow64Process") AndAlso IsWow64Process(GetCurrentProcess, flag)) AndAlso flag))
        End Function

        ''' <summary>
        ''' Check if the program is running in 64 bit, use Is64OS to check for OS architecture bit.
        ''' </summary>
        Public Shared Function Is64Proc() As Boolean
            Return IntPtr.Size > 4
        End Function

        ''' <summary>
        ''' Determine whether .Net registry has been set to run as 32 bit program.
        ''' It will return true if running in 32 bit OS or executing program compiled in 32 bit.
        ''' it will return true if OS is 64 bit, but .Net registry has been modified to enforce 32 bit.
        ''' </summary>
        Public Shared Function Enable64Bit() As Boolean
            Dim _Enable64Bit As String = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\.NETFramework", "Enable64Bit", Nothing)
            Return _Enable64Bit = 0
        End Function

        ''' <summary>
        ''' Check if assembly is executed in Remote Desktop environment.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function IsRdp() As Boolean
            Return System.Windows.Forms.SystemInformation.TerminalServerSession
        End Function

        ''' <summary>
        ''' Check if the current process is running as a Windows process or IIS / Asp.net service.
        ''' </summary>
        Public Shared Function IsAspNet() As Boolean
            Return Web.HttpRuntime.AppDomainAppId IsNot Nothing 'Not Environment.UserInteractive
        End Function

        ''' <summary>
        ''' Get path where the ASP program is hosted.
        ''' Interchangeable with GetStartUpPathDirectory.
        ''' </summary>
        Public Shared Function GetAspPhysicalPath() As String
            Return GetStartUpPathDirectory()
        End Function

        'Calculate license key for you, you must provide company and namespace of the assembly.
        Public Shared Function GetCalc(ByVal c As String, ByVal n As String) As System.Text.StringBuilder
            'only this assembly or any assembly with same root namespace will be able to call this function, it will throw FileNotFoundException if none of the conditions are met

            'Check if the calling assembly namespace is same as atlib (this assembly) namespace
            Dim bAllow As Boolean
            bAllow = GetAsmNamespace((New StackTrace).GetFrame(1).GetMethod.Module.Assembly) = GetAsmNamespace(Assembly.GetExecutingAssembly)
            'Dim a As New LicenseManager

            'If GetAsmNamespace((New StackTrace).GetFrame(1).GetMethod.Module.Assembly) = GetAsmNamespace(Assembly.GetExecutingAssembly) Then
            '    bAllow = IIf(((New StackTrace).GetFrame(1).GetMethod.ReflectedType.ToString) = (New LicenseManager).ToString, True, False)
            'End If

            'Check for external assembly, see if same namespace and has valid license, if invalid then return filenotfoundexception
            If bAllow = False Then
                'If Internal.GetAsmNamespace = GetAsmNamespace(Assembly.GetExecutingAssembly) Then
                '    bAllow = Internal.CheckMyLicense
                'End If
                bAllow = Internal.GetAsmNamespace = GetAsmNamespace(Assembly.GetExecutingAssembly)
            End If

            If bAllow Then
                Return GetCalcVal(c, n)
            Else
                Return New System.Text.StringBuilder
            End If
        End Function

        'for above method ^
        Private Shared Function GetCalcVal(ByVal c As String, ByVal n As String) As System.Text.StringBuilder
            If String.IsNullOrEmpty(c) Or String.IsNullOrEmpty(n) Then
                Return Nothing
            End If

            Dim myHash As String = Crypt.GetHash(c, n)
            Dim sbHash As System.Text.StringBuilder = Crypt.ConcatHash(myHash)
            Dim str As String = Crypt.Rot13(sbHash.ToString)

            sbHash = New System.Text.StringBuilder
            sbHash.Append(str)
            Return sbHash
        End Function

        ''' <summary>
        ''' Get the assembly version of this program, default use entry assembly.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetVersion(Optional ByVal asmSelect As Assembly = Nothing) As String
            ' Gets a reference to the same assembly that 
            ' contains the type that is creating the ResourceManager.
            Dim asm As Assembly
            asm = Internal.GetAsm

            ' Use targeted assembly
            If asmSelect IsNot Nothing Then
                asm = asmSelect
            End If

            Return asm.GetName.Version.Major.ToString & _
                    "." & _
                    asm.GetName.Version.Minor.ToString & _
                    "." & _
                    asm.GetName.Version.Build.ToString
        End Function

        ''' <summary>
        ''' Get the program name (from assembly information title), default use entry assembly.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetProgramName(Optional ByVal asmSelect As Assembly = Nothing) As String
            ' Gets a reference to the same assembly that 
            ' contains the type that is creating the ResourceManager.
            Dim asm As Assembly
            asm = Internal.GetAsm

            ' Use targeted assembly
            If asmSelect IsNot Nothing Then
                asm = asmSelect
            End If

            Return FileVersionInfo.GetVersionInfo(asm.BinaryPath).FileDescription
        End Function

        ''' <summary>
        ''' Return the full version of this program with the program's name, default use entry assembly.
        ''' eg. MyProgram Version 1.0.0
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetFullVersion(Optional ByVal asmSelect As Assembly = Nothing) As String
            Return String.Format("{0} Version {1}", GetProgramName(asmSelect), GetVersion(asmSelect))
        End Function

        ''' <summary>
        ''' Get file version of this program, default use entry assembly.
        ''' </summary>
        Public Shared Function GetFileVersionInfo(Optional ByVal asmSelect As Assembly = Nothing) As FileVersionInfo
            ' Gets a reference to the same assembly that 
            ' contains the type that is creating the ResourceManager.
            Dim asm As Assembly
            asm = Internal.GetAsm

            ' Use targeted assembly
            If asmSelect IsNot Nothing Then
                asm = asmSelect
            End If

            Return FileVersionInfo.GetVersionInfo(asm.BinaryPath)
        End Function

        ''' <summary>
        ''' Indicate that the code is running in runtime. Use DesignMode for designing mode check (1 level only).
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function IsRuntime() As Boolean
            Return Not IsDesignerMode()
        End Function

        Private Shared Function IsDesignerMode() As Boolean
            'Dim str As String = (System.ComponentModel.LicenseManager.UsageMode.ToString & " / " & _
            '                  System.Diagnostics.Process.GetCurrentProcess().ProcessName & " / " & _
            '                  Ape.EL.Reflex.PropertyHelper.GetPrivatePropertyValue(Of Boolean)(New System.ComponentModel.Component, "DesignMode").ToString)
            'Console.WriteLine(str)
            'MsgBox(str)
            'Dim x As Assembly = System.Reflection.Assembly.GetExecutingAssembly()

            'Note: UsageMode only works for first level object (eg. Parent has child, this child use IsDesignerMode, it will return false in the designer)
            ' thus we try to check the ProcessName. checking ProcessName will return false if you have changed the visual studio program name other than devenv. 
            ' naming your program to devenv will return true, so be careful when using this method.
            Return System.ComponentModel.LicenseManager.UsageMode = System.ComponentModel.LicenseUsageMode.Designtime OrElse
                System.Diagnostics.Process.GetCurrentProcess().ProcessName = "devenv"

            'Ape.EL.Reflex.PropertyHelper.GetPrivatePropertyValue(Of Boolean)(New System.ComponentModel.Component, "DesignMode") ' << this doesnt work properly
            'System.Reflection.Assembly.GetExecutingAssembly().Codebase.Contains("VisualStudio")) ' << another method, check whether the original host runs contains VisualStudio directory
        End Function

        Public Shared Function GetClassPropertyMembers(a As Object, t As Type) As String
            Dim b As New System.Text.StringBuilder
            If a IsNot Nothing And a.GetType Is t Then
                For Each p As System.Reflection.PropertyInfo In t.GetProperties
                    b.AppendLine(p.Name & " : " & p.GetValue(a, Nothing))
                Next
            End If
            Return b.ToString
        End Function

        ''' <summary>
        ''' Get PC's name based on host address (eg. localhost / 127.0.0.1 / 192.168.0.131 / RD_07-NB will return RD_07-NB).
        ''' </summary>
        Public Shared Function GetHostName(ByVal hostAddress As String) As String
            Try
                If Not hostAddress.IsEmpty Then
                    Dim host As System.Net.IPHostEntry = Dns.GetHostEntry(hostAddress)
                    Return host.HostName
                End If
            Catch ex As Exception
            End Try

            Return ""
        End Function

        ''' <summary>
        ''' Check if terminal connect locally. Using GetHostName(hostAddress) to determine if the current environment machine name equals with provided connection host.
        ''' </summary>
        Public Shared Function IsLocalhost(ByVal hostAddress As String) As String
            Return Environment.MachineName = GetHostName(hostAddress)
        End Function

        ''' <summary>
        ''' Check whether program is running under system administrator.
        ''' </summary>
        Public Shared Function IsAdministrator() As Boolean
            Dim current As WindowsIdentity = WindowsIdentity.GetCurrent()
            Dim windowsPrincipal As WindowsPrincipal = New WindowsPrincipal(current)
            Return windowsPrincipal.IsInRole(WindowsBuiltInRole.Administrator)
        End Function

        ''' <summary>
        ''' Check whether program is running under system administrator. It will check by writing a text file in Windows directory.
        ''' </summary>
        Public Shared Function IsAdministrator2() As Boolean
            Try
                Dim windowsDir = System.IO.Directory.GetParent(System.Environment.GetFolderPath(System.Environment.SpecialFolder.System)).FullName
                Dim path As String = System.IO.Path.Combine(windowsDir, String.Format("tfadxp{0}.txt", "80"))

                If File.Exists(path) Then
                    File.Delete(path)
                End If
                Using streamWriter As StreamWriter = File.CreateText(path)
                    streamWriter.WriteLine("test successful - {0}", DateTime.Now)
                    streamWriter.Close()
                End Using
                If Not File.Exists(path) Then
                    Return False
                End If
                File.Delete(path)
            Catch ex As Exception
                Return False
            End Try
            Return True
        End Function

        ''' <summary>
        ''' Set application auto run on startup setting (CurrentUser).
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub SetProgramAutoRun(ByVal enable As Boolean, Optional options As String = "")
            Dim registryKey As RegistryKey = Nothing
            Try
                registryKey = Registry.CurrentUser.OpenSubKey("SOFTWARE\Microsoft\Windows\CurrentVersion\Run", True)
            Catch ex As Exception
                Throw New ApplicationException("Unable to open registry.")
            End Try
            Try
                If Not options.IsEmpty AndAlso options.StartsWith(" ") Then
                    options = " " & options
                End If
                If enable Then
                    registryKey.SetValue(System.Windows.Forms.Application.ProductName, """" & System.Windows.Forms.Application.ExecutablePath & """" & options)
                Else
                    registryKey.DeleteValue(System.Windows.Forms.Application.ProductName, False)
                End If
            Catch ex As Exception
                Throw New ApplicationException("Unable to write to registry.")
            End Try
        End Sub

        ''' <summary>
        ''' Set application auto run on startup setting (LocalMachine).
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub SetProgramAutoRunLM(ByVal enable As Boolean, Optional options As String = "")
            Dim registryKey As RegistryKey = Nothing
            Try
                registryKey = Registry.LocalMachine.OpenSubKey("SOFTWARE\Microsoft\Windows\CurrentVersion\Run", True)
            Catch ex As Exception
                Throw New ApplicationException("Unable to open registry.")
            End Try
            Try
                If Not options.IsEmpty AndAlso options.StartsWith(" ") Then
                    options = " " & options
                End If
                If enable Then
                    registryKey.SetValue(System.Windows.Forms.Application.ProductName, """" & System.Windows.Forms.Application.ExecutablePath & """" & options)
                Else
                    registryKey.DeleteValue(System.Windows.Forms.Application.ProductName, False)
                End If
            Catch ex As Exception
                Throw New ApplicationException("Unable to write to registry.")
            End Try
        End Sub

        ''' <summary>
        ''' Set application auto run on startup setting (by creating shortcut on current user's Startup folder).
        ''' </summary>
        Public Shared Sub SetProgramAutoRunStartup(ByVal enable As Boolean)
            Dim programName As String = System.Windows.Forms.Application.ProductName
            Dim startupPath As String = Environment.GetFolderPath(Environment.SpecialFolder.Startup)
            Dim exePath As String = System.Windows.Forms.Application.ExecutablePath
            Try
                If enable Then
                    CreateShortCut(programName, startupPath, exePath)
                Else
                    Dim shortcutFilePath As String = String.Format("{0}\{1}.lnk", startupPath, programName)
                    System.IO.File.Delete(shortcutFilePath)
                End If
            Catch ex As Exception
                Throw New ApplicationException("Unable to create autorun shortcut.")
            End Try
        End Sub

        ''' <summary>
        ''' Create a shortcut file.
        ''' </summary>
        Public Shared Function CreateShortCut(ByVal strShortcutName As String, ByVal strShortcutDir As String, ByVal strTargetFile As String) As Boolean
            Try
                Dim shortcutFilePath As String = String.Format("{0}\{1}.lnk", strShortcutDir, strShortcutName)
                Dim wshShell As New IWshRuntimeLibrary.WshShell
                Dim shortcut As IWshRuntimeLibrary.IWshShortcut = CType(wshShell.CreateShortcut(shortcutFilePath), IWshRuntimeLibrary.IWshShortcut)
                With shortcut
                    .TargetPath = strTargetFile.Replace(Chr(34), String.Empty)
                    .WorkingDirectory = strShortcutDir
                    .Description = strShortcutName
                    .Save()
                End With

                CreatePermission(strTargetFile)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''' <summary>
        ''' Legacy code from installer program. (Probably for access to network files or directories)
        ''' </summary>
        Private Shared Function CreatePermission(ByVal strAppExePath As String) As Boolean
            Dim strAppPath As String = ""

            Try
                If System.IO.File.Exists(strAppExePath) Then
                    Dim exename As String = System.IO.Path.GetFileName(strAppExePath)
                    If InStr(strAppExePath.ToString.Substring(2, InStr(strAppExePath.ToString, "\" & exename) - 3), "\") > 0 Then
                        Dim strCurrPath As String = strAppExePath.ToString.Substring(2, InStr(strAppExePath.ToString, "\" & exename) - 3)
                        strAppPath = "\\" & strCurrPath.ToString.Substring(0, InStr(strAppExePath.ToString.Substring(2, InStr(strAppExePath.ToString, "\" & exename) - 3), "\") - 1)
                    Else
                        strAppPath = "\\" & strAppExePath.ToString.Substring(2, InStr(strAppExePath.ToString, "\" & exename) - 3)
                    End If

                    Dim strWinDir As String = Environment.SystemDirectory.ToString.Substring(0, InStr(Environment.SystemDirectory.ToString, "\system32") - 1)

                    Dim strBatchFilePath As String = strWinDir & "\Permission.bat"
                    Dim fi As FileInfo = New FileInfo(strBatchFilePath)
                    Dim fw As StreamWriter = fi.CreateText
                    fw.WriteLine(strWinDir & "\Microsoft.Net\Framework\v2.0.50727\caspol -m -ag 1 -url file:" & strAppPath & "\* FullTrust")
                    fw.Close()

                    Ape.EL.App.Cmd.Run("""" & strBatchFilePath & """", "yes")
                    System.IO.File.Delete(strBatchFilePath)
                End If
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''' <summary>
        ''' Determine if a service has been installed (search by name).
        ''' </summary>
        Public Shared Function IsServiceInstalled(ByVal name As String) As Boolean
            Dim res As Boolean
            Dim svcs = ServiceProcess.ServiceController.GetServices().ToList
            res = (From x In svcs Where String.Compare(name, x.ServiceName, True) = 0 Select x).FirstOrDefault IsNot Nothing
            Return res
        End Function

        ''' <summary>
        ''' Returns the date and time that the specified assembly was compiled on.
        ''' </summary>
        ''' <param name="filePath">A full path to a .NET assembly.</param>
        ''' <returns>A DateTime value.</returns>
        Public Shared Function RetrieveAssemblyBuildTime(ByVal filePath As String) As DateTime
            Const PortableExecutableHeaderOffset As Integer = 60
            Const LinkerTimestampOffset As Integer = 8

            Dim b(2047) As Byte
            Dim s As IO.Stream = Nothing

            Try
                s = New IO.FileStream(filePath, IO.FileMode.Open, IO.FileAccess.Read)
                s.Read(b, 0, 2048)
            Finally
                If Not s Is Nothing Then s.Close()
            End Try

            Dim i As Integer = BitConverter.ToInt32(b, PortableExecutableHeaderOffset)
            Dim secondsSince1970 As Integer = BitConverter.ToInt32(b, i + LinkerTimestampOffset)
            Dim dt As New DateTime(1970, 1, 1, 0, 0, 0)

            dt = dt.AddSeconds(secondsSince1970)
            dt = dt.AddHours(TimeZone.CurrentTimeZone.GetUtcOffset(dt).Hours)

            Return dt
        End Function
    End Class
End Namespace
