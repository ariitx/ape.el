﻿Imports System.Reflection

Namespace Utility
    ''' <summary>
    ''' A resource manager class.
    ''' </summary>
    Public Class ResManager
        '        Private ReadOnly lic As Boolean = Internal.CheckMyLicense

        ''' <summary>
        ''' Return resources of selected assembly. Must provide ResName.
        ''' </summary>
        ''' <param name="ResName">The resource name</param>
        ''' <param name="MyAsm">The assembly that contains the resource. If not set, will use this library assembly instead.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Friend Shared Function GetObject(ByVal ResName As String, Optional ByVal MyAsm As Assembly = Nothing) As Object
            ' Gets a reference to the same assembly that 
            ' contains the type that is creating the ResourceManager.
            Dim asm As System.Reflection.Assembly = Internal.GetMyAssembly

            ' Use targeted assembly
            If MyAsm IsNot Nothing Then
                asm = MyAsm
            End If

            ' Creates the ResourceManager
            Dim myManager As New System.Resources.ResourceManager(Utility.General.GetAsmNamespace(asm, False) & ".Resources", asm)

            Return myManager.GetObject(ResName)
        End Function


        ''' <summary>
        ''' Return resources of selected assembly. Must provide ResName.
        ''' </summary>
        ''' <param name="ResName">The resource name</param>
        ''' <param name="MyAsmPath">Set assembly through assembly file path</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetObject(ByVal ResName As String, ByVal MyAsmPath As String) As Object
            Dim asm As Assembly = Nothing
            If System.IO.File.Exists(MyAsmPath) Then
                asm = Utility.General.GetAsm(MyAsmPath)
            End If

            Return GetObject(ResName, asm)
        End Function
    End Class

End Namespace
