﻿Imports System.Diagnostics
Imports System.Runtime.InteropServices

Namespace Network
    Public Class RemoteDesktop

#Region "Constants"
        Private Const WTS_CURRENT_SESSION As Integer = -1
#End Region

#Region "Dll Imports"
        <DllImport("wtsapi32.dll")> _
        Private Shared Function WTSEnumerateSessions(pServer As IntPtr, <MarshalAs(UnmanagedType.U4)> iReserved As Integer, <MarshalAs(UnmanagedType.U4)> iVersion As Integer, ByRef pSessionInfo As IntPtr, <MarshalAs(UnmanagedType.U4)> ByRef iCount As Integer) As Integer
        End Function

        <DllImport("Wtsapi32.dll")> _
        Private Shared Function WTSQuerySessionInformation(pServer As System.IntPtr, iSessionID As Integer, oInfoClass As WTS_INFO_CLASS, ByRef pBuffer As System.IntPtr, ByRef iBytesReturned As UInteger) As Boolean
        End Function

        <DllImport("wtsapi32.dll")> _
        Private Shared Sub WTSFreeMemory(pMemory As IntPtr)
        End Sub

        <DllImport("wtsapi32.dll", CharSet:=CharSet.Auto, SetLastError:=True)> _
        Private Shared Function WTSOpenServer(ByVal pServerName As String) As IntPtr
        End Function

        <DllImport("wtsapi32.dll", CharSet:=CharSet.Auto, SetLastError:=True)> _
        Private Shared Sub WTSCloseServer(ByVal hServer As IntPtr)
        End Sub

        Private Declare Function GetCurrentProcessId Lib "Kernel32.dll" Alias "GetCurrentProcessId" () As Int32
        Private Declare Function ProcessIdToSessionId Lib "Kernel32.dll" Alias "ProcessIdToSessionId" (ByVal processID As Int32, ByRef sessionID As Int32) As Boolean
        Private Declare Function WTSGetActiveConsoleSessionId Lib "Kernel32.dll" Alias "WTSGetActiveConsoleSessionId" () As Int32
#End Region

#Region "Structures"
        'Structure for Terminal Service Client IP Address
        <StructLayout(LayoutKind.Sequential)> _
        Private Structure WTS_CLIENT_ADDRESS
            Public iAddressFamily As Integer
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=20)> _
            Public bAddress As Byte()
        End Structure

        'Structure for Terminal Service Session Info
        <StructLayout(LayoutKind.Sequential)> _
        Private Structure WTS_SESSION_INFO
            Public iSessionID As Integer
            <MarshalAs(UnmanagedType.LPStr)> _
            Public sWinsWorkstationName As String
            Public oState As WTS_CONNECTSTATE_CLASS
        End Structure

        'Structure for Terminal Service Session Client Display
        <StructLayout(LayoutKind.Sequential)> _
        Private Structure WTS_CLIENT_DISPLAY
            Public iHorizontalResolution As Integer
            Public iVerticalResolution As Integer
            '1 = The display uses 4 bits per pixel for a maximum of 16 colors.
            '2 = The display uses 8 bits per pixel for a maximum of 256 colors.
            '4 = The display uses 16 bits per pixel for a maximum of 2^16 colors.
            '8 = The display uses 3-byte RGB values for a maximum of 2^24 colors.
            '16 = The display uses 15 bits per pixel for a maximum of 2^15 colors.
            Public iColorDepth As Integer
        End Structure

        Structure Session
            Dim SessionID As String
            Dim SessionState As String
            Dim WorkstationName As String
            Dim UserName As String
            Dim DomainName As String
            Dim ClientDisplayResolution As String
            Dim ClientDisplayColourDepth As String

            Dim ClientAddress As String
            Dim ClientBuildNumber As String
            Dim ClientName As String
            Dim ClientDirectory As String
            Dim ClientProductId As String
            Dim ClientHardwareId As String
            Dim ClientProtocolType As String
        End Structure
#End Region

#Region "Enumurations"
        Private Enum WTS_CONNECTSTATE_CLASS
            WTSActive
            WTSConnected
            WTSConnectQuery
            WTSShadow
            WTSDisconnected
            WTSIdle
            WTSListen
            WTSReset
            WTSDown
            WTSInit
        End Enum

        Private Enum WTS_INFO_CLASS
            WTSInitialProgram
            WTSApplicationName
            WTSWorkingDirectory
            WTSOEMId
            WTSSessionId
            WTSUserName
            WTSWinStationName
            WTSDomainName
            WTSConnectState
            WTSClientBuildNumber
            WTSClientName
            WTSClientDirectory
            WTSClientProductId
            WTSClientHardwareId
            WTSClientAddress
            WTSClientDisplay
            WTSClientProtocolType
            WTSIdleTime
            WTSLogonTime
            WTSIncomingBytes
            WTSOutgoingBytes
            WTSIncomingFrames
            WTSOutgoingFrames
            WTSClientInfo
            WTSSessionInfo
            WTSConfigInfo
            WTSValidationInfo
            WTSSessionAddressV4
            WTSIsRemoteSession
        End Enum
#End Region

#Region "Methods"
        Private Function GetConnectionState(ByVal State As WTS_CONNECTSTATE_CLASS) As String
            Dim RetVal As String
            Select Case State
                Case WTS_CONNECTSTATE_CLASS.WTSActive
                    RetVal = "Active"
                Case WTS_CONNECTSTATE_CLASS.WTSConnected
                    RetVal = "Connected"
                Case WTS_CONNECTSTATE_CLASS.WTSConnectQuery
                    RetVal = "Query"
                Case WTS_CONNECTSTATE_CLASS.WTSDisconnected
                    RetVal = "Disconnected"
                Case WTS_CONNECTSTATE_CLASS.WTSDown
                    RetVal = "Down"
                Case WTS_CONNECTSTATE_CLASS.WTSIdle
                    RetVal = "Idle"
                Case WTS_CONNECTSTATE_CLASS.WTSInit
                    RetVal = "Initializing."
                Case WTS_CONNECTSTATE_CLASS.WTSListen
                    RetVal = "Listen"
                Case WTS_CONNECTSTATE_CLASS.WTSReset
                    RetVal = "reset"
                Case WTS_CONNECTSTATE_CLASS.WTSShadow
                    RetVal = "Shadowing"
                Case Else
                    RetVal = "Unknown connect state"
            End Select
            Return RetVal
        End Function

        Private Shared Function GetSessionId() As UInteger
            'Get ProcessID of TS Session that executed this TS Session
            Dim active_process As Int32 = GetCurrentProcessId()
            Dim active_session As Int32 = 0
            Dim success1 As Boolean = ProcessIdToSessionId(active_process, active_session)
            If success1 = False Then
                Throw New InvalidOperationException("No session attached to the physical console.")
            End If

            Return active_session
        End Function

        ''' <summary>
        ''' Get an array of remote desktop sessions information.
        ''' </summary>
        Public Shared Function GetSessions() As Session()
            Dim pServer As IntPtr = IntPtr.Zero
            Dim oClientAddres As New WTS_CLIENT_ADDRESS()
            Dim oClientDisplay As New WTS_CLIENT_DISPLAY()

            Dim pSessionInfo As IntPtr = IntPtr.Zero

            Dim iCount As Integer = 0
            Dim iReturnValue As Integer = WTSEnumerateSessions(pServer, 0, 1, pSessionInfo, iCount)
            Dim iDataSize As Integer = Marshal.SizeOf(GetType(WTS_SESSION_INFO))

            Dim iCurrent As Integer = CInt(pSessionInfo)

            Dim lSessions As New List(Of Session)

            If iReturnValue <> 0 Then
                'Go to all sessions
                For i As Integer = 0 To iCount - 1
                    Dim pBuffer As IntPtr = IntPtr.Zero
                    Dim oSessionInfo As WTS_SESSION_INFO = CType(Marshal.PtrToStructure(CType(iCurrent, System.IntPtr), GetType(WTS_SESSION_INFO)), WTS_SESSION_INFO)
                    iCurrent += iDataSize

                    Dim iReturned As UInteger = 0

                    'The session structure
                    Dim rd As New Session
                    rd.SessionID = oSessionInfo.iSessionID
                    rd.SessionState = oSessionInfo.oState.ToString
                    rd.WorkstationName = oSessionInfo.sWinsWorkstationName
                    If WTSQuerySessionInformation(pServer, oSessionInfo.iSessionID, WTS_INFO_CLASS.WTSUserName, pBuffer, iReturned) = True Then
                        rd.UserName = Marshal.PtrToStringAnsi(pBuffer)
                    End If
                    If WTSQuerySessionInformation(pServer, oSessionInfo.iSessionID, WTS_INFO_CLASS.WTSDomainName, pBuffer, iReturned) = True Then
                        rd.DomainName = Marshal.PtrToStringAnsi(pBuffer)
                    End If
                    If WTSQuerySessionInformation(pServer, oSessionInfo.iSessionID, WTS_INFO_CLASS.WTSClientDisplay, pBuffer, iReturned) = True Then
                        oClientDisplay = CType(Marshal.PtrToStructure(pBuffer, oClientDisplay.[GetType]()), WTS_CLIENT_DISPLAY)
                        rd.ClientDisplayResolution = oClientDisplay.iHorizontalResolution & " x " & oClientDisplay.iVerticalResolution
                        rd.ClientDisplayColourDepth = oClientDisplay.iColorDepth
                    End If
                    If WTSQuerySessionInformation(pServer, oSessionInfo.iSessionID, WTS_INFO_CLASS.WTSClientAddress, pBuffer, iReturned) = True Then
                        oClientAddres = CType(Marshal.PtrToStructure(pBuffer, oClientAddres.[GetType]()), WTS_CLIENT_ADDRESS)
                        rd.ClientAddress = oClientAddres.bAddress(2) & "." & oClientAddres.bAddress(3) & "." & oClientAddres.bAddress(4) & "." & oClientAddres.bAddress(5)
                    End If
                    If WTSQuerySessionInformation(pServer, oSessionInfo.iSessionID, WTS_INFO_CLASS.WTSClientBuildNumber, pBuffer, iReturned) = True Then
                        rd.ClientBuildNumber = Marshal.PtrToStringAnsi(pBuffer)
                    End If
                    If WTSQuerySessionInformation(pServer, oSessionInfo.iSessionID, WTS_INFO_CLASS.WTSClientName, pBuffer, iReturned) = True Then
                        rd.ClientName = Marshal.PtrToStringAnsi(pBuffer)
                    End If
                    If WTSQuerySessionInformation(pServer, oSessionInfo.iSessionID, WTS_INFO_CLASS.WTSClientDirectory, pBuffer, iReturned) = True Then
                        rd.ClientDirectory = Marshal.PtrToStringAnsi(pBuffer)
                    End If
                    If WTSQuerySessionInformation(pServer, oSessionInfo.iSessionID, WTS_INFO_CLASS.WTSClientProductId, pBuffer, iReturned) = True Then
                        rd.ClientProductId = Marshal.PtrToStringAnsi(pBuffer)
                    End If
                    If WTSQuerySessionInformation(pServer, oSessionInfo.iSessionID, WTS_INFO_CLASS.WTSClientHardwareId, pBuffer, iReturned) = True Then
                        rd.ClientHardwareId = Marshal.PtrToStringAnsi(pBuffer)
                    End If
                    If WTSQuerySessionInformation(pServer, oSessionInfo.iSessionID, WTS_INFO_CLASS.WTSClientProtocolType, pBuffer, iReturned) = True Then
                        rd.ClientProtocolType = Marshal.PtrToStringAnsi(pBuffer)
                    End If

                    lSessions.Add(rd)
                Next

                WTSFreeMemory(pSessionInfo)
            End If

            Return lSessions.ToArray
        End Function

        ''' <summary>
        ''' Get remote desktop info, if WTSStationName is "Console", then it is local and not RDP.
        ''' </summary>
        Public Shared Function GetRDPInfo(Optional ByVal ServerName As String = "") As Session
            Try
                Dim sessionID As UInteger = GetSessionId()
                Dim sessionList As List(Of Session) = GetSessions.ToList
                Return (From x As Session In sessionList Where x.SessionID = sessionID).FirstOrDefault
            Catch ex As Exception
            End Try
            Return Nothing
        End Function
#End Region
    End Class
End Namespace
