﻿Namespace Repository
    Public Class RepositoryItemEmail
        Inherits RepositoryItem

        ''' <summary>
        ''' Get or set the email address.
        ''' </summary>
        Property Address As String
            Get
                Return _Address
            End Get
            Set(value As String)
                _Address = value
            End Set
        End Property
        Private _Address As String

        ''' <summary>
        ''' Get or set the email log in password.
        ''' </summary>
        Property Password As String
            Get
                Return _Password
            End Get
            Set(value As String)
                _Password = value
            End Set
        End Property
        Private _Password As String

        ''' <summary>
        ''' Get or set the email hosting website.
        ''' </summary>
        Property Host As String
            Get
                Return _Host
            End Get
            Set(value As String)
                _Host = value
            End Set
        End Property
        Private _Host As String


        ''' <summary>
        ''' Get or set the email SSL setting.
        ''' </summary>
        Property UseSSL As Boolean
            Get
                Return _UseSSL
            End Get
            Set(value As Boolean)
                _UseSSL = value
            End Set
        End Property
        Private _UseSSL As Boolean


        ''' <summary>
        ''' Get or set the email port.
        ''' </summary>
        Property Port As Integer
            Get
                Return _Port
            End Get
            Set(value As Integer)
                _Port = value
            End Set
        End Property
        Private _Port As Integer
    End Class
End Namespace
