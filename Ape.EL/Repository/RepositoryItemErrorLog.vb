﻿Namespace Repository
    Public Class RepositoryItemErrorLog
        Inherits RepositoryItem

        Public Sub New()
            Try
                _LogLevel = App.ErrorLog.Priority.INFO
                _TimeZoneOffset = 0
                _Email = New RepositoryItemEmail
                _EmailRecipient = ""
                _ErrorLogDirectory = String.Format("{0}{1}{2}", Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), System.IO.Path.DirectorySeparatorChar, Internal.GetAsmNamespace())
                _ErrorLogFilename = "error.log"
            Catch ex As Exception
                App.ErrorLogInMemory.WriteError(ex.Message, ex)
            End Try
        End Sub

        ''' <summary>
        ''' Get or set logging level. Default value is INFO.
        ''' </summary>
        Property LogLevel As App.ErrorLog.Priority
            Get
                Return _LogLevel
            End Get
            Set(value As App.ErrorLog.Priority)
                _LogLevel = value
            End Set
        End Property
        Private _LogLevel As App.ErrorLog.Priority

        ''' <summary>
        ''' Get or set logging timestamp timezone offset by hour.
        ''' </summary>
        Property TimeZoneOffset As Integer
            Get
                Return _TimeZoneOffset
            End Get
            Set(value As Integer)
                _TimeZoneOffset = value
            End Set
        End Property
        Private _TimeZoneOffset As Integer

        ''' <summary>
        ''' Get an object containing properties and methods specific to an email account.
        ''' </summary>
        ReadOnly Property Email As RepositoryItemEmail
            Get
                Return _Email
            End Get
        End Property
        Private _Email As RepositoryItemEmail

        ''' <summary>
        ''' Get or set email recipient of the error log.
        ''' </summary>
        Property EmailRecipient As String
            Get
                Return _EmailRecipient
            End Get
            Set(value As String)
                _EmailRecipient = value
            End Set
        End Property
        Private _EmailRecipient As String

        ''' <summary>
        ''' Get or set error log directory. Not recommended to change. Default value points to [AppData\Local\App\ProgramRootNamespace].
        ''' </summary>
        Property ErrorLogDirectory As String
            Get
                Return _ErrorLogDirectory
            End Get
            Set(value As String)
                _ErrorLogDirectory = value
            End Set
        End Property
        Private _ErrorLogDirectory As String

        ''' <summary>
        ''' Get or set error log filename. Not recommended to change. Default value is [error.log].
        ''' </summary>
        Property ErrorLogFilename As String
            Get
                Return _ErrorLogFilename
            End Get
            Set(value As String)
                _ErrorLogFilename = value
            End Set
        End Property
        Private _ErrorLogFilename As String

        ''' <summary>
        ''' Get error log full path.
        ''' </summary>
        ReadOnly Property ErrorLogPath As String
            Get
                Return String.Format("{0}{1}{2}", _ErrorLogDirectory, System.IO.Path.DirectorySeparatorChar, _ErrorLogFilename)
            End Get
        End Property
    End Class
End Namespace
