﻿Namespace Repository
    ''' <summary>
    ''' List of default commands for raw printer usage.
    ''' </summary>
    Public Class RepositoryPrintTextCmd
        Inherits RepositoryItem

        Public ReadOnly DefaultFontName As String = "Courier New"
        Public ReadOnly DefaultFontSize As Single = 9

        ''' <summary>
        ''' Line feed.
        ''' </summary>
        Property LF As String
            Get
                Return _LF
            End Get
            Set(value As String)
                _LF = value
            End Set
        End Property
        Private _LF As String = "   "

        ''' <summary>
        ''' Align left.
        ''' </summary>
        Property Left As String
            Get
                Return _Left
            End Get
            Set(value As String)
                _Left = value
            End Set
        End Property
        Private _Left As String = "w"

        ''' <summary>
        ''' Align right.
        ''' </summary>
        Property Right As String
            Get
                Return _Right
            End Get
            Set(value As String)
                _Right = value
            End Set
        End Property
        Private _Right As String = "y"

        ''' <summary>
        ''' Align center.
        ''' </summary>
        Property Center As String
            Get
                Return _Center
            End Get
            Set(value As String)
                _Center = value
            End Set
        End Property
        Private _Center As String = "x"

        ''' <summary>
        ''' Partial cut.
        ''' </summary>
        Property Cut As String
            Get
                Return _Cut
            End Get
            Set(value As String)
                _Cut = value
            End Set
        End Property
        Private _Cut As String = "P"
    End Class
End Namespace
