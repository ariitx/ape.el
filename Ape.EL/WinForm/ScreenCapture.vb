﻿Imports System.Runtime.InteropServices
Imports System.Drawing
Imports System.Drawing.Imaging
Imports Ape.EL.Win32

Namespace WinForm
    ''' <summary>
    ''' Provides functions to capture the entire screen, or a particular window, and save it to a file.
    ''' </summary>
    Public Class ScreenCapture
        ''' <summary>
        ''' Creates an Image object containing a screen shot of the entire desktop.
        ''' </summary>
        ''' <returns></returns>
        Public Shared Function CaptureScreen() As Image
            Return CaptureWindow(User32.GetDesktopWindow())
        End Function
        ''' <summary>
        ''' Creates an Image object containing a screen shot of a specific window.
        ''' </summary>
        ''' <param name="handle">The handle to the window. (In windows forms, this is obtained by the Handle property)</param>
        ''' <returns></returns>
        Public Shared Function CaptureWindow(handle As IntPtr) As Image
            ' get te hDC of the target window
            Dim hdcSrc As IntPtr = User32.GetWindowDC(handle)
            ' get the size
            Dim windowRect As New User32.RECT()
            User32.GetWindowRect(handle, windowRect)
            Dim width As Integer = windowRect.right - windowRect.left
            Dim height As Integer = windowRect.bottom - windowRect.top
            ' create a device context we can copy to
            Dim hdcDest As IntPtr = Gdi32.CreateCompatibleDC(hdcSrc)
            ' create a bitmap we can copy it to,
            ' using GetDeviceCaps to get the width/height
            Dim hBitmap As IntPtr = Gdi32.CreateCompatibleBitmap(hdcSrc, width, height)
            ' select the bitmap object
            Dim hOld As IntPtr = Gdi32.SelectObject(hdcDest, hBitmap)
            ' bitblt over
            Gdi32.BitBlt(hdcDest, 0, 0, width, height, hdcSrc, _
                0, 0, Gdi32.SRCCOPY)
            ' restore selection
            Gdi32.SelectObject(hdcDest, hOld)
            ' clean up 
            Gdi32.DeleteDC(hdcDest)
            User32.ReleaseDC(handle, hdcSrc)
            ' get a .NET image object for it
            Dim img As Image = Image.FromHbitmap(hBitmap)
            ' free up the Bitmap object
            Gdi32.DeleteObject(hBitmap)
            Return img
        End Function
        ''' <summary>
        ''' Captures a screen shot of a specific window, and saves it to a file.
        ''' </summary>
        ''' <param name="handle"></param>
        ''' <param name="filename"></param>
        ''' <param name="format"></param>
        Public Shared Sub CaptureWindowToFile(handle As IntPtr, filename As String, format As ImageFormat)
            Dim img As Image = CaptureWindow(handle)
            img.Save(filename, format)
        End Sub
        ''' <summary>
        ''' Captures a screen shot of the entire desktop, and saves it to a file.
        ''' </summary>
        ''' <param name="filename"></param>
        ''' <param name="format"></param>
        Public Shared Sub CaptureScreenToFile(filename As String, format As ImageFormat)
            Dim img As Image = CaptureScreen()
            img.Save(filename, format)
        End Sub
    End Class
End Namespace