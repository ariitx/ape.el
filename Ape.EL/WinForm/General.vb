﻿Imports System.Windows.Forms
Imports System.Windows
Imports System.Drawing
Imports System.Management
Imports Ape.EL.Internal
Imports Ape.EL.Win32.User32

Namespace WinForm
#If DEBUG Then
    Public Class General
#Else
        <System.Diagnostics.DebuggerStepThrough()> _
        Public Class General
#End If
        '        Private ReadOnly lic As Boolean = Internal.CheckMyLicense

        Public Delegate Sub ExceptionEventHandler(ByRef ex As Exception)

        ''' <summary>
        ''' Raised when calling ExceptionErrorMsg(Exception).
        ''' </summary>
        Public Shared Event ExceptionErrorMsgShowEvent As ExceptionEventHandler

        ''' <summary>
        ''' Used by GetControlPosition.
        ''' </summary>
        Public Enum ControlPos
            OnScreen
            OnForm
        End Enum

        ''' <summary>
        ''' Set default messagebox form to be used in ExceptionErrorMsg, QuestionMsg, InformationMsg, and WarningMsg. Object must implement IDialogBox.
        ''' </summary>
        ''' <value></value>
        ''' <remarks></remarks>
        Public Shared WriteOnly Property MessageBoxForm As Interfaces.IDialogBox
            Set(value As Interfaces.IDialogBox)
                _MessageBoxForm = Nothing

                If value IsNot Nothing AndAlso GetType(Interfaces.IDialogBox).IsAssignableFrom(value.GetType) Then
                    _MessageBoxForm = value
                Else
                    Throw New ApplicationException("value must implement IDialogBox interface.")
                End If
            End Set
        End Property
        Private Shared _MessageBoxForm As Interfaces.IDialogBox

        ''' <summary>
        ''' Set messagebox's owner allow the messagebox to block the owner if messagebox was executed in different thread.
        ''' </summary>
        Public Shared WriteOnly Property MessageBoxOwner As Form
            Set(value As Form)
                _MessageBoxOwner = value
            End Set
        End Property
        Private Shared _MessageBoxOwner As Form

        ''' <summary>
        ''' Automatically find owner form when using messagebox methods from this class when MessagBoxOwner is set nothing.
        ''' By default this option is true.
        ''' </summary>
        Public Shared Property MessageBoxSmartOwner As Boolean
            Get
                Return _MessageBoxSmartOwner
            End Get
            Set(value As Boolean)
                _MessageBoxSmartOwner = value
            End Set
        End Property
        Private Shared _MessageBoxSmartOwner As Boolean = True

        ''' <summary>
        ''' Set an event handler before showing any message from this class.
        ''' </summary>
        Shared WriteOnly Property BeforeShowMessageEventHandler As EventHandler
            Set(value As EventHandler)
                _BeforeShowMessageEventHandler = value
            End Set
        End Property
        Private Shared _BeforeShowMessageEventHandler As EventHandler

        ''' <summary>
        ''' Set an event handler after showing any message from this class.
        ''' </summary>
        Shared WriteOnly Property AfterShowMessageEventHandler As EventHandler
            Set(value As EventHandler)
                _AfterShowMessageEventHandler = value
            End Set
        End Property
        Private Shared _AfterShowMessageEventHandler As EventHandler

        ''' <summary>
        ''' Standardized MessageBox header.
        ''' Set value to "-1" to remove default header.
        ''' Set value to empty to use default header.
        ''' Set other value to use customized header.
        ''' </summary>
        Shared WriteOnly Property MessageBoxStandardHeader As String
            Set(value As String)
                _MessageBoxStandardHeader = value
            End Set
        End Property
        Private Shared _MessageBoxStandardHeader As String

        ''' <summary>
        ''' Write some text on an image.
        ''' </summary>
        ''' <param name="myImage"></param>
        ''' <param name="txt"></param>
        ''' <param name="width"></param>
        ''' <param name="height"></param>
        ''' <remarks></remarks>
        Public Shared Sub TextOnImage(ByRef myImage As Image, ByVal txt As String, Optional ByVal width As Integer = 200, Optional ByVal height As Integer = 70)
            If myImage Is Nothing Then myImage = New Bitmap(width, height)
            myImage = New Bitmap(myImage.Width, myImage.Height)
            Dim tmpGrphc As Graphics = Graphics.FromImage(myImage)
            Dim brsh As New SolidBrush(Color.Black)
            Dim fnt As Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            tmpGrphc.DrawString(txt, fnt, brsh, 0, 0)
        End Sub

        ''' <summary>
        ''' Convert painted on screen control to a bitmap image.
        ''' </summary>
        ''' <param name="ctl"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <Obsolete("Use ScreenCapture class instead.")>
        Public Shared Function GetControlImage(ByVal ctl As Control, Optional transparent As Boolean = True) As Bitmap
            Dim bm As New Bitmap(ctl.Width, ctl.Height)
            ctl.DrawToBitmap(bm, New Rectangle(0, 0, ctl.Width, ctl.Height))
            If transparent Then bm.MakeTransparent()
            Return bm
        End Function

        ''' <summary>
        ''' Draw an image anywhere on the screen, when the screen refreshes, it will be flushed out.
        ''' </summary>
        ''' <param name="img"></param>
        ''' <param name="loc"></param>
        ''' <remarks></remarks>
        Public Shared Sub DrawImageAnywhere(ByVal img As Image, ByVal loc As Point)
            Dim hdc As IntPtr = GetDC(IntPtr.Zero)
            Dim gr As Graphics = Graphics.FromHdc(hdc)
            gr.DrawImage(img, loc)
            ReleaseDC(IntPtr.Zero, hdc)
        End Sub

        ''' <summary>
        ''' Mix 2 colours just like overlapping them.
        ''' </summary>
        ''' <param name="Color1"></param>
        ''' <param name="Color2"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetMixedColour(ByVal Color1 As Color, ByVal Color2 As Color) As Color
            Return Color.FromArgb( _
                            Decimal.Round(((CInt(Color1.A) + CInt(Color2.A)) / 2)), _
                            Decimal.Round(((CInt(Color1.R) + CInt(Color2.R)) / 2)), _
                            Decimal.Round(((CInt(Color1.G) + CInt(Color2.G)) / 2)), _
                            Decimal.Round(((CInt(Color1.B) + CInt(Color2.B)) / 2)) _
                            )
        End Function

        ''' <summary>
        ''' Use on the form load event.
        ''' Will update the MinimumSize property of every control in the form as it looks like in the designer.
        ''' </summary>
        ''' <param name="frm"></param>
        ''' <remarks></remarks>
        Public Shared Sub SetDesignMinimumSize(ByRef frm As Form)
            For Each c As Control In frm.Controls
                If TypeOf c Is GroupBox Then
                    For Each d As Control In c.Controls
                        d.MinimumSize = d.Size
                    Next
                End If
                c.MinimumSize = c.Size
            Next
        End Sub

        ''' <summary>
        ''' Get the PC's motherboard serial number.
        ''' </summary>
        Public Shared Function GetMotherboardSN() As String
            'Ari updated the method, not using old one as it's causing a weird glitch when executing on Immediate Window.
            'This new method comes from http://www.vb-helper.com/howto_net_get_cpu_serial_number_id.html

            ' Get the Windows Management Instrumentation object.
            Dim wmi As Object = GetObject("WinMgmts:")

            ' Get the "base boards" (mother boards).
            Dim serial_numbers As String = ""
            Dim mother_boards As Object = wmi.InstancesOf("Win32_BaseBoard")
            For Each board As Object In mother_boards
                serial_numbers &= ", " & board.SerialNumber
            Next board
            If serial_numbers.Length > 0 Then serial_numbers = serial_numbers.Substring(2)

            Return serial_numbers.Trim
        End Function

        ''' <summary>
        ''' Handle exception error messages.
        ''' </summary>
        ''' <param name="strExMsg">Main error message to be displayed</param>
        ''' <param name="strExString">Optional, if param is not set, additional message will be shown and asked to be allow copy to clipboard</param>
        ''' <param name="intStack">Get last call method. Value set as 1 for the last method by default.</param>
        ''' <remarks></remarks>
        Public Shared Sub ExceptionErrorMsg(ByVal strExMsg As String, Optional ByVal strExString As String = "", Optional ByVal intStack As Integer = 1, Optional ByVal strHeader As String = "", Optional ByVal useSystemMessageBox As Boolean = False)
            CheckThrowExceptionMessage(strExMsg & Environment.NewLine & strExString)

            Dim frame As System.Diagnostics.StackFrame = (New System.Diagnostics.StackTrace(App.Debug.IsDebug)).GetFrame(intStack)
            Dim framestring As String = If(App.Debug.IsDebug, String.Format("Class:{1}{0}Method:{2}{0}File:{3}, {4}", vbCrLf, frame.GetMethod.DeclaringType.ToString, frame.GetMethod.Name, IO.Path.GetFileName(frame.GetFileName), frame.GetFileLineNumber, ""), "")

            'Write it to error.log file
            Dim sb As New System.Text.StringBuilder
            sb.AppendLine("ExceptionErrorMsg: " & strExMsg)
            If Not String.IsNullOrEmpty(framestring) Then
                sb.AppendLine(framestring)
            End If
            If Not String.IsNullOrEmpty(strExString) Then
                sb.AppendLine(strExString)
            End If
            Ape.EL.App.ErrorLog.WriteMessage(sb.ToString())

            Trace(intStack + 2, strExMsg)

            'Show the message
            Try
                StandardizeMsgHdr(strHeader, Localization.StringId.MsgHdr_Warning)

                If Not String.IsNullOrEmpty(strExString) Then
                    If ShowMessageBox(Nothing, String.Format("{2}{1}{1}{1}Do you want to show detailed message?", "", vbCrLf, strExMsg), strHeader, MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2, useSystemMessageBox) = DialogResult.Yes Then
                        If ShowMessageBox(Nothing, String.Format("{2}{0}{1}{1}Copy error message?", strExString, vbCrLf, If(App.Debug.IsDebug, framestring & vbCrLf & vbCrLf, "")), strHeader, MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2, useSystemMessageBox) = DialogResult.Yes Then
                            Clipboard.SetText(strExString)
                        End If
                    End If
                Else
                    If ShowMessageBox(Nothing, String.Format("{0}{1}{1}Copy error message?", strExMsg, vbCrLf), strHeader, MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2, useSystemMessageBox) = DialogResult.Yes Then
                        Clipboard.SetText(strExMsg)
                    End If
                End If
            Catch ex As Exception
                Try
                    If ShowMessageBox(Nothing, String.Format("{0}{1}{1}Copy error message?", strExMsg, vbCrLf), strHeader, MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2, useSystemMessageBox) = DialogResult.Yes Then
                        Clipboard.SetText(strExMsg)
                    End If
                Catch ex2 As Exception
                    App.ErrorLog.Info("ExceptionErrorMsg :" & Environment.NewLine & strExMsg)
                    Throw New Exception("Failed to show error message.", ex)
                End Try
            End Try
        End Sub

        ''' <summary>
        ''' Handle exception error messages.
        ''' </summary>
        ''' <param name="_ex">Throw an Exception to show default exception error message</param>
        ''' <remarks></remarks>
        Public Shared Sub ExceptionErrorMsg(ByVal _ex As Exception,
                                            Optional ByVal useSystemMessageBox As Boolean = False)
            CheckThrowExceptionMessage(_ex.Message & Environment.NewLine & _ex.ToString)

            Try
                RaiseEvent ExceptionErrorMsgShowEvent(_ex)
                If _ex IsNot Nothing AndAlso GetType(ApplicationException).Name = _ex.GetType.Name OrElse GetType(ApplicationMessageException).Name = _ex.GetType.Name Then
                    'just show simple warning message if it is application exception
                    If Not _ex.Message.IsEmpty Then WarningMsg(_ex.Message, , , useSystemMessageBox)
                Else
                    ExceptionErrorMsg(_ex.Message, _ex.ToString, 3, , useSystemMessageBox)
                End If
            Catch ex As Exception
                App.ErrorLog.Info("ExceptionErrorMsg :" & Environment.NewLine & _ex.ToString)
                Throw New Exception("Failed to show error message.", ex)
            End Try
        End Sub

        ''' <summary>
        ''' Handles Warning messages.
        ''' </summary>
        ''' <param name="strMsg">Message shown.</param>
        ''' <param name="strHeader">Set to "-1" to remove header.</param>
        ''' <remarks></remarks>
        Public Shared Sub WarningMsg(ByVal strMsg As String,
                                     Optional ByVal strHeader As String = "",
                                     Optional ByVal defaultButton As MessageBoxDefaultButton = MessageBoxDefaultButton.Button1,
                                     Optional ByVal useSystemMessageBox As Boolean = False)
            WarningMsg(Nothing, strMsg, strHeader, defaultButton, useSystemMessageBox)
        End Sub

        ''' <summary>
        ''' Handles Warning messages.
        ''' </summary>
        ''' <param name="strMsg">Message shown.</param>
        ''' <param name="strHeader">Set to "-1" to remove header.</param>
        ''' <remarks></remarks>
        Public Shared Sub WarningMsg(ByVal owner As IWin32Window,
                                     ByVal strMsg As String,
                                     Optional ByVal strHeader As String = "",
                                     Optional ByVal defaultButton As MessageBoxDefaultButton = MessageBoxDefaultButton.Button1,
                                     Optional ByVal useSystemMessageBox As Boolean = False)
            CheckThrowMessage(strMsg)

            Try
                StandardizeMsgHdr(strHeader, Localization.StringId.MsgHdr_Warning)

                If Not String.IsNullOrEmpty(strMsg) Then
                    ShowMessageBox(owner, strMsg, strHeader, MessageBoxButtons.OK, MessageBoxIcon.Warning, defaultButton, useSystemMessageBox)
                End If
            Catch ex As Exception
                App.ErrorLog.Info("WarningMsg :" & Environment.NewLine & strMsg)
                ExceptionErrorMsg("Failed to show warning message.", ex.ToString)
            End Try
        End Sub

        ''' <summary>
        ''' Handles information messages.
        ''' </summary>
        ''' <param name="strMsg">Message shown.</param>
        ''' <param name="strHeader">Set to "-1" to remove header.</param>
        ''' <remarks></remarks>
        Public Shared Sub InformationMsg(ByVal strMsg As String,
                                         Optional ByVal strHeader As String = "",
                                         Optional ByVal msgboxIcon As MessageBoxIcon = MessageBoxIcon.Information,
                                         Optional ByVal defaultButton As MessageBoxDefaultButton = MessageBoxDefaultButton.Button1,
                                         Optional ByVal useSystemMessageBox As Boolean = False)
            InformationMsg(Nothing, strMsg, strHeader, msgboxIcon, defaultButton, useSystemMessageBox)
        End Sub

        ''' <summary>
        ''' Handles information messages.
        ''' </summary>
        ''' <param name="strMsg">Message shown.</param>
        ''' <param name="strHeader">Set to "-1" to remove header.</param>
        ''' <remarks></remarks>
        Public Shared Sub InformationMsg(ByVal owner As IWin32Window,
                                         ByVal strMsg As String,
                                         Optional ByVal strHeader As String = "",
                                         Optional ByVal msgboxIcon As MessageBoxIcon = MessageBoxIcon.Information,
                                         Optional ByVal defaultButton As MessageBoxDefaultButton = MessageBoxDefaultButton.Button1,
                                         Optional ByVal useSystemMessageBox As Boolean = False)
            CheckThrowMessage(strMsg)

            Try
                StandardizeMsgHdr(strHeader, Localization.StringId.MsgHdr_Information)

                If Not String.IsNullOrEmpty(strMsg) Then
                    ShowMessageBox(Nothing, strMsg, strHeader, MessageBoxButtons.OK, msgboxIcon, defaultButton, useSystemMessageBox)
                End If
            Catch ex As Exception
                App.ErrorLog.Info("InformationMsg :" & Environment.NewLine & strMsg)
                ExceptionErrorMsg("Failed to show information message.", ex.ToString)
            End Try
        End Sub

        ''' <summary>
        ''' Handles question messages.
        ''' </summary>
        ''' <param name="strMsg">Message shown.</param>
        ''' <param name="strHeader">Set to "-1" to remove header.</param>
        ''' <param name="msgBtn">Shown buttons.</param>
        ''' <param name="msgBoxIcon">Message box icon.</param>
        ''' <param name="defaultButton">Default selected button.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function QuestionMsg(ByVal strMsg As String,
                                           Optional ByVal strHeader As String = "",
                                           Optional ByVal msgBtn As MessageBoxButtons = MessageBoxButtons.YesNo,
                                           Optional ByVal msgBoxIcon As MessageBoxIcon = MessageBoxIcon.Question,
                                           Optional ByVal defaultButton As MessageBoxDefaultButton = MessageBoxDefaultButton.Button1,
                                           Optional ByVal useSystemMessageBox As Boolean = False) As DialogResult
            Return QuestionMsg(Nothing, strMsg, strHeader, msgBtn, msgBoxIcon, defaultButton, useSystemMessageBox)
        End Function

        ''' <summary>
        ''' Handles question messages.
        ''' </summary>
        ''' <param name="strMsg">Message shown.</param>
        ''' <param name="strHeader">Set to "-1" to remove header.</param>
        ''' <param name="msgBtn">Shown buttons.</param>
        ''' <param name="msgBoxIcon">Message box icon.</param>
        ''' <param name="defaultButton">Default selected button.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function QuestionMsg(ByVal owner As IWin32Window,
                                           ByVal strMsg As String,
                                           Optional ByVal strHeader As String = "",
                                           Optional ByVal msgBtn As MessageBoxButtons = MessageBoxButtons.YesNo,
                                           Optional ByVal msgBoxIcon As MessageBoxIcon = MessageBoxIcon.Question,
                                           Optional ByVal defaultButton As MessageBoxDefaultButton = MessageBoxDefaultButton.Button1,
                                           Optional ByVal useSystemMessageBox As Boolean = False) As DialogResult
            CheckThrowMessage(strMsg)

            Try
                StandardizeMsgHdr(strHeader, Localization.StringId.MsgHdr_Question)

                If Not String.IsNullOrEmpty(strMsg) Then
                    Return ShowMessageBox(Nothing, strMsg, strHeader, msgBtn, msgBoxIcon, defaultButton, useSystemMessageBox)
                End If
            Catch ex As Exception
                App.ErrorLog.Info("QuestionMsg :" & Environment.NewLine & strMsg)
                ExceptionErrorMsg("Failed to show question message.", ex.ToString)
            End Try
            Return DialogResult.None
        End Function

        ''' <summary>
        ''' Handles error message with mshta javascript which is not managed by calling program (detached as separate running program).
        ''' Message cannot be suppressed. Exception will not be thrown.
        ''' On AspNet, message will be logged to ErrorLog.
        ''' </summary>
        Public Shared Sub ErrorScriptMsg(ByVal strMsg As String, Optional ByVal strHeader As String = "")
            Try
                If Utility.General.IsAspNet Then
                    Ape.EL.App.ErrorLog.WriteMessage(strMsg)
                Else
                    StandardizeMsgHdr(strHeader)

                    If Not String.IsNullOrEmpty(strMsg) Then
                        strMsg = strMsg.Replace("""", "\x22").Replace("'", "\'")
                        strHeader = strHeader.Replace("""", "\x22").Replace("'", "\'")

                        Dim c As String = String.Format("mshta ""javascript:var sh=new ActiveXObject( 'WScript.Shell' ); sh.Popup( '{0}', 0, '{1}', 16 );close()""", strMsg, strHeader)
                        Ape.EL.App.Cmd.Run(c)

                    End If
                End If
            Catch ex As Exception
            End Try
        End Sub

        ''' <summary>
        ''' Header = DefaultCaption - DefaultHeader.
        ''' </summary>
        ''' <param name="Header">Referenced header value. Set to "-1" to remove header.</param>
        ''' <param name="DefaultCaption">A default caption.</param>
        ''' <remarks>DefaultHeader is GetFullVersion, you can override the value through setting MessageBoxStandardHeader property.</remarks>
        Private Shared Sub StandardizeMsgHdr(ByRef Header As String, Optional ByVal DefaultCaption As Localization.StringId = Nothing)
            If Header.Trim.IsEmpty And DefaultCaption <> Nothing Then
                Header = Localization.Localizer.GetString(DefaultCaption)
            ElseIf Header = "-1" Then
                Header = ""
            End If

            Dim defaultHeader As String = Ape.EL.Utility.General.GetFullVersion

            If _MessageBoxStandardHeader = "-1" Then
                defaultHeader = ""
            ElseIf Not _MessageBoxStandardHeader.IsEmpty Then
                defaultHeader = _MessageBoxStandardHeader
            End If

            If Header.Trim.IsEmpty Then
                Header = defaultHeader
            ElseIf Not defaultHeader.IsEmpty Then
                Header = String.Format("{0} - {1}", Header, defaultHeader)
            End If
        End Sub

        ''' <summary>
        ''' Internal usage for showing MessageBox. If _MessageBoxForm is set, then try to use Show function from that object.
        ''' </summary>
        ''' <param name="owner"> Obsolete. Use MessageBoxOwner property instead. </param>
        Private Overloads Shared Function ShowMessageBox(ByVal owner As IWin32Window, _
                                               ByVal text As String, _
                                               ByVal caption As String, _
                                               ByVal buttons As MessageBoxButtons, _
                                               ByVal icon As MessageBoxIcon, _
                                               ByVal defaultButton As MessageBoxDefaultButton,
                                               ByVal useSystemMessageBox As Boolean) As DialogResult
            Dim result As DialogResult = DialogResult.None

            If _BeforeShowMessageEventHandler IsNot Nothing Then
                _BeforeShowMessageEventHandler.Invoke("Ape.EL", New EventArgs)
            End If

            Dim fOwner As Form = Nothing
            If owner Is Nothing Then
                If _MessageBoxOwner IsNot Nothing AndAlso Not _MessageBoxOwner.IsDisposed AndAlso _MessageBoxOwner.Visible Then
                    owner = _MessageBoxOwner
                End If
            End If
            If owner Is Nothing AndAlso MessageBoxSmartOwner Then
                owner = GetCallingForm()
            End If
            If owner Is Nothing Then
                fOwner = New Form() With {.TopMost = True, .StartPosition = FormStartPosition.CenterScreen}
                owner = fOwner
            End If

            CType(owner, Form).InvokeIfRequired(Sub(x)
                                                    If _MessageBoxForm IsNot Nothing And Not useSystemMessageBox Then
                                                        Try
                                                            result = _MessageBoxForm.Show(x, text, caption, buttons, icon, defaultButton)
                                                        Catch ex As Exception
                                                            MessageBoxForm = Nothing
                                                            ExceptionErrorMsg(New ApplicationException(ex.Message & Environment.NewLine & "MessageBoxForm value has been set to null.", ex))
                                                        End Try
                                                    Else
                                                        result = MessageBox.Show(x, text, caption, buttons, icon, defaultButton)
                                                    End If
                                                End Sub)

            If fOwner IsNot Nothing Then fOwner.Dispose()

            If _AfterShowMessageEventHandler IsNot Nothing Then
                _AfterShowMessageEventHandler.Invoke("Ape.EL", New EventArgs)
            End If

            Return result
        End Function

        ''' <summary>
        ''' Put this in the first like of function.
        ''' </summary>
        Private Shared Sub CheckThrowExceptionMessage(ByVal strMsg As String)
            If InternalGlobal.SuppressExceptionMessage OrElse Utility.General.IsAspNet Then
                Ape.EL.App.ErrorLog.WriteMessage(strMsg)
                Throw New ApplicationException(strMsg)
                'should exit this sub afterwards
            End If
        End Sub

        ''' <summary>
        ''' Put this in the first like of function.
        ''' </summary>
        Private Shared Sub CheckThrowMessage(ByVal strMsg As String)
            If InternalGlobal.SuppressMessage OrElse Utility.General.IsAspNet Then
                Throw New ApplicationMessageException(strMsg)
                'should exit this sub afterwards
            End If
        End Sub

        ''' <summary>
        ''' Write log to specified location.
        ''' </summary>
        ''' <param name="strMessage"></param>
        ''' <remarks></remarks>
        <Obsolete("Use Ape.EL.App.ErrorLog.WriteMessage(string)")>
        Public Shared Sub WriteToLogFile(ByVal strMessage As String, Optional ByVal bTimeStamp As Boolean = False, Optional ByVal bUseDatedFolder As Boolean = True, Optional ByVal strPath As String = "", Optional ByVal strFileName As String = "")
            Try
                Dim strDatedFolder As String = ""
                Dim strFilePath As String = ""

                '***Path
                If strPath = "" Then 'set default folder path
                    strPath = String.Format("{0}\{1}\Log", Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), GetAsmNamespace())
                End If
                While strPath.StartsWith("\") 'remove all backslash from starts of strPath, to be processed later on
                    strPath = strPath.Substring(1, strPath.Length - 1)
                End While
                While strPath.EndsWith("\") 'remove all backslash from ends of strPath, to be processed later on
                    strPath = strPath.Substring(0, strPath.Length - 1)
                End While

                '***Dated Folder
                If bUseDatedFolder Then
                    strDatedFolder = "\" & CStr(Now().ToString("yyyyMMdd"))
                End If

                '***File Name
                If strFileName = "" Then 'set default file name
                    strFileName = CStr(Now().ToString("yyyyMMdd-HH")) & ".txt"
                Else
                    If Not strFileName.EndsWith(".txt") Then 'add .txt extension if filename does not have it
                        strFileName = strFileName & ".txt"
                    End If
                End If
                While strFileName.StartsWith("\") 'remove all backslash from starts of strFileName, to be processed later on
                    strFileName = strFileName.Substring(1, strFileName.Length - 1)
                End While
                strFileName = "\" & strFileName 'add backslash at start of file name

                '***Combine to file full path
                strFilePath = strPath & strDatedFolder & strFileName

                '***Create path if not exist, append strMessage to file
                Utility.General.MakePath(System.IO.Path.GetDirectoryName(strFilePath))
                Using oFile As New System.IO.StreamWriter(strFilePath, True)

                    If bTimeStamp Then 'add time stamp
                        strMessage = String.Format("{0:HH:mm:ss} {1}", Now(), strMessage)
                    End If

                    oFile.WriteLine(strMessage)
                    oFile.Close()
                End Using
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        ''' <summary>
        ''' Auto resize font size to fit the text on the control.
        ''' </summary>
        ''' <param name="ctl">The specified control.</param>
        ''' <param name="bMaxOriginal">If set true, then the maximum size will reuse the original font size.</param>
        Public Shared Sub FontAutoSize(ByVal ctl As Control, Optional ByVal bMaxOriginal As Boolean = False)
            FontAutoSize(ctl, "", bMaxOriginal)
        End Sub

        ''' <summary>
        ''' Auto resize font size to fit the text on the control.
        ''' </summary>
        ''' <param name="ctl">The specified control.</param>
        ''' <param name="altText">Alternate text to replace ctl's Text.</param>
        ''' <param name="bMaxOriginal">If set true, then the maximum size will reuse the original font size.</param>
        Public Shared Sub FontAutoSize(ByVal ctl As Control, ByVal altText As String, Optional ByVal bMaxOriginal As Boolean = False)
            Dim fntOriginal As Font = ctl.Font
            Dim txt As String = ctl.Text

            If Not altText.IsEmpty Then
                'use alternative text to auto size
                txt = altText
            End If

            ' Only bother if there's text.
            If txt.Length > 0 Then
                Dim best_size As Decimal = 100

                ' See how much room we have, allowing a bit
                ' for the Label's internal margin.
                Dim wid As Integer = ctl.Bounds.Width
                Dim hgt As Integer = ctl.Bounds.Height
                Dim verBound As Integer = ctl.Padding.Bottom + ctl.Padding.Top + 5
                Dim horBound As Integer = ctl.Padding.Left + ctl.Padding.Right + 5

                ' Make a Graphics object to measure the text.
                Using gr As Graphics = ctl.CreateGraphics()
                    Dim i As Decimal = 2
                    While True
                        Using test_font As New Font(ctl.Font.FontFamily, i)
                            Dim text_size As SizeF = gr.MeasureString(txt, test_font, wid)
                            If text_size.Width + horBound >= wid Or text_size.Height + verBound >= hgt Then
                                best_size = i - 1
                                Exit While
                            Else
                                i += 0.05
                            End If
                        End Using
                    End While
                End Using

                ' Use that font size.
                ctl.Font = New Font(ctl.Font.FontFamily, best_size)

                'Use original font if too big
                If bMaxOriginal Then
                    If fntOriginal.Size < ctl.Font.Size Then
                        ctl.Font = fntOriginal
                    End If
                End If
            End If
        End Sub

        ''' <summary>
        ''' Determine if text is too large for the control.
        ''' </summary>
        ''' <param name="ctl">The specified control.</param>
        ''' <param name="altText">Alternate text to replace ctl's Text.</param>
        ''' <param name="bNoFloor">Disable Math.Floor on the graphic size.</param>
        Public Shared Function IsTextTooLarge(ByVal ctl As Control, Optional altText As String = "", Optional ByVal bNoFloor As Boolean = False) As Boolean
            If ctl IsNot Nothing AndAlso Not ctl.IsDisposed Then
                Dim fnt As Font = ctl.Font
                Dim txt As String = ctl.Text

                If txt.Length > 0 Then
                    Using gr As Graphics = ctl.CreateGraphics()
                        Dim text_size As SizeF = gr.MeasureString(txt, fnt, ctl.Bounds.Width)
                        If Math.Floor(text_size.Height) > ctl.Height OrElse Math.Floor(text_size.Width) > ctl.Width Then
                            Return True
                        End If
                    End Using
                End If
            End If
            Return False
        End Function

        ''' <summary>
        ''' Get form's center location.
        ''' </summary>
        Public Shared Function GetCenterPosition(ByVal myForm As Form, Optional ByVal parentForm As Form = Nothing) As Point
            Dim ptNewLocation As Point

            If parentForm IsNot Nothing Then
                ptNewLocation.X = parentForm.Location.X + ((parentForm.Width - myForm.Width) / 2)
                ptNewLocation.Y = parentForm.Location.Y + ((parentForm.Height - myForm.Height) / 2)
            Else
                ptNewLocation.X = (Screen.PrimaryScreen.Bounds.Width - myForm.Width) / 2
                ptNewLocation.Y = (Screen.PrimaryScreen.Bounds.Height - myForm.Height) / 2
            End If

            Return ptNewLocation
        End Function

        ''' <summary>
        ''' Get control's center position.
        ''' </summary>
        Public Shared Function GetCenterPosition(ByVal myCtl As Control, ByVal parentCtl As Control) As Point
            Dim ptNewLocation As Point
            ptNewLocation.X = parentCtl.Location.X + ((parentCtl.Width - myCtl.Width) / 2)
            ptNewLocation.Y = parentCtl.Location.Y + ((parentCtl.Height - myCtl.Height) / 2)

            Return ptNewLocation
        End Function

        Public Shared Function GetCenterPosition(s As Size, Optional ctls As Size = Nothing) As Point
            Dim p As New Point(0, 0)

            If ctls = Nothing Then
                ctls = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Size
            End If

            If s.Width < ctls.Width And s.Height < ctls.Height Then
                p.X = (ctls.Width - s.Width) \ 2
                p.Y = (ctls.Height - s.Height) \ 2
            End If

            Return p
        End Function

        ''' <summary>
        ''' Get control's position relative to form or to desktop screen.
        ''' </summary>
        Public Shared Function GetPreciseControlPosition(sender As Control, ByVal relativePos As ControlPos) As Point
            Dim p As New Point
            Dim pf As Form = sender.FindForm

            If sender IsNot Nothing AndAlso pf IsNot Nothing Then
                If relativePos = ControlPos.OnScreen Then
                    p = sender.PointToScreen(p)
                ElseIf relativePos = ControlPos.OnForm Then
                    'http://stackoverflow.com/a/1478105/2502282
                    p = pf.PointToClient(sender.Parent.PointToScreen(sender.Location))
                Else
                    p = sender.Location
                End If
            End If

            Return p
        End Function

        ''' <summary>
        ''' Centralize controls in the specified control containers to be positioned centrally inside the provided control. eg. For buttons to be positioned centrally like dialog message.
        ''' </summary>
        ''' <param name="ctl"></param>
        ''' <remarks></remarks>
        Public Shared Sub CentralizeControls(ctl As Control)
            Dim arySizeList As New ArrayList
            Dim totalWidth As Integer = 0
            Dim rememberHeight As Integer = -1
            Dim rememberWidth As Integer = -1
            Dim count As Integer = 0
            Dim visiblecount As Integer = 0
            For Each c As Control In ctl.Controls
                If c.Visible Then
                    visiblecount += 1
                End If
            Next
            For Each c As Control In ctl.Controls
                If Not c.Visible Then
                    Continue For
                End If
                If rememberHeight = -1 Then
                    rememberHeight = c.Height
                End If
                If rememberWidth = -1 Then
                    rememberWidth = c.Width
                End If
                arySizeList.Add(c.Size) 'save each control's ori size
                totalWidth += c.Width 'compute all control width
                c.Height = rememberHeight 'set height
                c.Top = (ctl.Height - rememberHeight) / 2 'set top position

                Dim remain As Integer = ctl.Width - ((rememberWidth) * visiblecount)
                Dim extrapadding As Integer = remain / (visiblecount + 1)
                c.Left = (count * (rememberWidth + extrapadding)) + (extrapadding)

                count += 1
            Next
        End Sub

        ''' <summary>
        ''' Internal use, tracing method callers.
        ''' </summary>
        ''' <param name="methodframe">Trace the method. Default value is 2.</param>
        ''' <param name="msg">Message to be shown.</param>
        ''' <remarks></remarks>
        Public Shared Sub Trace(Optional ByVal methodFrame As Integer = 2, Optional ByVal msg As String = "")
            If Not Debugger.IsAttached Then Exit Sub
            If methodFrame <= 1 Then methodFrame = 2

            'create the stack trace and get the methods
            Dim st As New System.Diagnostics.StackTrace()
            Dim methodX As Reflection.MethodBase = st.GetFrame(methodFrame).GetMethod
            Dim methodY As Reflection.MethodBase = st.GetFrame(methodFrame - 1).GetMethod

            'begin write
            Debug.WriteLineIf(Debugger.IsAttached, String.Format("Begin trace on {0:HH:mm:ss}, frame {1}.", Now, methodFrame))
            Debug.WriteLineIf(Debugger.IsAttached, String.Format("Method : {0}.{1} > {2}.{3}", methodX.DeclaringType.FullName, methodX.Name, methodY.DeclaringType.FullName, methodY.Name))
            Debug.WriteLineIf(Debugger.IsAttached And Not msg.IsEmpty, String.Format("Message: {0}", msg))

            'clean up the mess
            methodX = Nothing
            methodY = Nothing
            st = Nothing
        End Sub

        ''' <summary>
        ''' Internal use, tracing method callers.
        ''' </summary>
        ''' <param name="msg">Message to be shown.</param>
        Public Shared Sub Trace(ByVal msg As String)
            Trace(3, msg)
        End Sub

        ''' <summary>
        ''' Return nothing if there is no form or PauseFormMonitor is true. Experimental function, use with caution.
        ''' </summary>
        Public Shared Function GetCallingForm() As Form
            Dim result As Form = Nothing

            'create the stack trace and get the methods
            Dim st As New System.Diagnostics.StackTrace()

            Dim methodX As Reflection.MethodBase = Nothing
            If st.FrameCount > 0 Then
                Dim curActiveForm = Application.OpenForms

                For methodFrame As Integer = 0 To st.FrameCount - 1
                    methodX = st.GetFrame(methodFrame).GetMethod
                    If GetType(Form).IsAssignableFrom(methodX.DeclaringType) Then
                        result = (From x As Form In curActiveForm
                                 Where methodX.DeclaringType Is x.GetType AndAlso
                                 Not x.IsDisposed AndAlso
                                 Threading.Thread.CurrentThread.ManagedThreadId = GetControlThreadId(x)
                                 Select x).LastOrDefault
                        If result IsNot Nothing Then Exit For
                    End If
                Next
            End If

            'clean up the mess
            methodX = Nothing
            st = Nothing

            Return result
        End Function

        ''' <summary>
        ''' Get stack trace in string list. Experimental function, use with caution.
        ''' </summary>
        Public Shared Function GetStackTrace(ByVal withDebug As Boolean) As List(Of String)
            Dim res As New List(Of String)

            'create the stack trace and get the methods
            Dim st As New System.Diagnostics.StackTrace(withDebug)

            If st.FrameCount > 0 Then
                For methodFrame As Integer = 1 To st.FrameCount - 1

                    Dim frame As System.Diagnostics.StackFrame = st.GetFrame(methodFrame)
                    Dim mp = frame.GetMethod.GetParameters
                    Dim paramString As New System.Text.StringBuilder
                    For Each m In mp
                        If Not paramString.ToString.IsEmpty Then paramString.Append(", ")
                        paramString.AppendFormat("{0} {1}", m.ParameterType.Name, m.Name)
                    Next

                    Dim fileString As String = ""
                    If Not frame.GetFileName.IsEmpty Then
                        fileString = String.Format(" in {0}:line {1}", _
                                                   IO.Path.GetFileName(frame.GetFileName), _
                                                   frame.GetFileLineNumber)
                    End If


                    Dim frmstr As String

                    frmstr = String.Format("   at {0}.{1}({2}){3}", _
                                           frame.GetMethod.DeclaringType.ToString, _
                                           frame.GetMethod.Name, _
                                           paramString.ToString, _
                                           fileString)

                    res.Add(frmstr)
                Next
            End If

            'clean up the mess
            st = Nothing

            Return res
        End Function

        ''' <summary>
        ''' Get current thread of a control.
        ''' </summary>
        Public Shared Function GetControlThread(control As Control) As Threading.Thread
            Dim thread As Threading.Thread = Nothing
            control.InvokeIfRequired(Sub()
                                         thread = Threading.Thread.CurrentThread
                                     End Sub)
            Return thread
        End Function

        ''' <summary>
        ''' Get current ManagedThreadID of a control. If thread is not found, return -1.
        ''' </summary>
        Public Shared Function GetControlThreadId(control As Control) As Integer
            Dim th As Threading.Thread = GetControlThread(control)
            If th IsNot Nothing Then Return th.ManagedThreadId
            Return -1
        End Function

        ''' <summary>
        ''' Check if form is on screen boundary.
        ''' </summary>
        ''' <param name="form"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function IsOnScreen(ByVal form As Form) As Boolean
            Dim screens() As Screen = Screen.AllScreens

            For Each scrn As Screen In screens
                Dim formRectangle As Rectangle = New Rectangle(form.Left, form.Top, form.Width, form.Height)

                If scrn.WorkingArea.Contains(formRectangle) Then
                    Return True
                End If
            Next

            Return False
        End Function

        ''' <summary>
        ''' Find all controls and their children within a container.
        ''' </summary>
        Public Shared Function FindAllControls(container As Control) As List(Of Control)
            Dim controlList As New List(Of Control)()
            FindAllControls(container, controlList)
            Return controlList
        End Function

        Private Shared Sub FindAllControls(container As Control, ctrlList As IList(Of Control))
            ctrlList.Add(container)
            For Each ctrl As Control In container.Controls
                If ctrl.Controls.Count = 0 Then
                    ctrlList.Add(ctrl)
                Else
                    FindAllControls(ctrl, ctrlList)
                End If
            Next
        End Sub

        ''' <summary>
        ''' Generic usage of FolderBrowserDialog.
        ''' </summary>
        Public Shared Function ShowFolderBrowserDialog(ByVal DefaultLoc As String) As String
            Dim fbdDestination As New FolderBrowserDialog
            Dim res As String = DefaultLoc

            fbdDestination.ShowNewFolderButton = True
            If System.IO.Directory.Exists(DefaultLoc) Then
                fbdDestination.SelectedPath = DefaultLoc
            End If

            Ape.EL.App.General.RunMethodOnNewThread(Sub() SendKeys.SendWait("{TAB}{TAB}{RIGHT}")) 'run in another thread to make sure sendkeys is processed when fbdDestination is active.

            If fbdDestination.ShowDialog = Windows.Forms.DialogResult.OK Then
                If fbdDestination.SelectedPath.Substring(fbdDestination.SelectedPath.Length - 1, 1) = "\" Then
                    res = fbdDestination.SelectedPath
                Else
                    res = fbdDestination.SelectedPath & "\"
                End If
            Else
                res = ""
            End If

            Return res
        End Function

        ''' <summary>
        ''' Generic usage of OpenFileDialog.
        ''' </summary>
        ''' <param name="InitialDirectory">Example "C:\"</param>
        ''' <param name="Filter">Example "All files (*.*)|*.*"</param>
        Public Shared Function ShowOpenFileDialog(ByVal InitialDirectory As String, ByVal Filter As String) As String
            Dim fd As New OpenFileDialog
            If Not String.IsNullOrEmpty(InitialDirectory) Then
                fd.InitialDirectory = InitialDirectory
            End If
            If String.IsNullOrEmpty(Filter) Then
                fd.Filter = "All files (*.*)|*.*"
            Else
                fd.Filter = Filter
            End If

            fd.FilterIndex = 0
            fd.RestoreDirectory = True

            If fd.ShowDialog = Windows.Forms.DialogResult.OK Then
                Return fd.FileName
            End If

            Return String.Empty
        End Function

        ''' <summary>
        ''' Generic usage of SaveFileDialog.
        ''' </summary>
        ''' <param name="InitialDirectory">Example "C:\"</param>
        ''' <param name="Filter">Example "All files (*.*)|*.*"</param>
        Public Shared Function ShowSaveFileDialog(ByVal InitialDirectory As String, ByVal Filter As String) As String
            Dim fd As New SaveFileDialog
            If Not String.IsNullOrEmpty(InitialDirectory) Then
                fd.InitialDirectory = InitialDirectory
                fd.FileName = System.IO.Path.GetFileName(InitialDirectory)
            End If
            If String.IsNullOrEmpty(Filter) Then
                fd.Filter = "All files (*.*)|*.*"
            Else
                fd.Filter = Filter
            End If

            fd.FilterIndex = 0
            fd.RestoreDirectory = True

            If fd.ShowDialog = Windows.Forms.DialogResult.OK Then
                Return fd.FileName
            End If

            Return String.Empty
        End Function

        ''' <summary>
        ''' Get a unique filename in a folder with incremental integer suffix.
        ''' </summary>
        Public Shared Function GetUniqueFileName(ByVal FilePath As String, ByVal FileName As String) As String
            Dim FullFilePath As String = FilePath & System.IO.Path.DirectorySeparatorChar & FileName
            Dim iCount As Integer = 0
            While True
                If System.IO.File.Exists(FullFilePath) Then
                    iCount += 1
                    FullFilePath = FilePath & System.IO.Path.DirectorySeparatorChar & System.IO.Path.GetFileNameWithoutExtension(FileName) & IIf(iCount > 0, iCount, "") & System.IO.Path.GetExtension(FileName)
                Else
                    Exit While
                End If
            End While

            Return System.IO.Path.GetFullPath(FullFilePath)
        End Function

        ''' <summary>
        ''' Get a unique name within a list of string with incremental integer suffix.
        ''' </summary>
        Public Shared Function GetUniqueName(ByVal ExistingNames As List(Of String), ByVal Name As String) As String
            If ExistingNames Is Nothing Then ExistingNames = New List(Of String)
            Dim NewName As String = Name
            Dim iCount As Integer = 0
            For i As Integer = 0 To ExistingNames.Count - 1
                ExistingNames(i) = ExistingNames(i).ToLower
            Next
            While True
                If ExistingNames.Contains(NewName.ToLower) Then
                    iCount += 1
                    NewName = System.IO.Path.GetFileNameWithoutExtension(Name) & IIf(iCount > 0, iCount, "") & System.IO.Path.GetExtension(Name)
                Else
                    Exit While
                End If
            End While

            Return NewName
        End Function

        ''' <summary>
        ''' Check whether system contains the provided truetype font.
        ''' </summary>
        Public Shared Function IsFontTrueType(ByVal fontName As String) As Boolean
            Dim listFontFamily As List(Of String) = FontFamily.Families.Select(Function(x) UCase(x.Name)).ToList
            Return listFontFamily.Contains(UCase(fontName))
        End Function

        ''' <summary>
        ''' Generic usage of FontDialog.
        ''' </summary>
        Public Shared Function ShowFontDialog() As Font
            Dim myFontDialog As New FontDialog

            myFontDialog.ShowHelp = False
            myFontDialog.ShowApply = False
            myFontDialog.ShowColor = False
            If myFontDialog.ShowDialog() = Windows.Forms.DialogResult.OK Then
                Return myFontDialog.Font
            End If

            Return Nothing
        End Function

        ''' <summary>
        ''' Force the focus to running application.
        ''' </summary>
        Public Shared Sub FocusOnApp()
            Try
                Dim appFileName As String = Ape.EL.Utility.General.GetStartUpName

                Dim arrProcesses As Process() = Process.GetProcessesByName(appFileName)
                If arrProcesses.Length > 0 Then

                    Dim ipHwnd As IntPtr = arrProcesses(0).MainWindowHandle
                    System.Threading.Thread.Sleep(100)

                    SetForegroundWindow(ipHwnd)
                End If
            Catch ex As Exception
            End Try
        End Sub
    End Class
End Namespace
