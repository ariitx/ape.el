﻿Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Reflection
Imports System.Resources
Imports System.Text
Imports System.Threading

Namespace Valuation
    Public Class Valuator
        Private Shared myTable As System.Collections.Hashtable = New System.Collections.Hashtable()

        Private Sub New()
        End Sub

        Public Shared Function GetString(id As System.[Enum], ParamArray args As Object()) As String
            If id Is Nothing Then
                Throw New System.ArgumentNullException("id")
            End If
            Dim type As System.Type = id.[GetType]()
            Dim valuatorAttribute As ValuatorAttribute = TryCast(System.Attribute.GetCustomAttribute(type, GetType(ValuatorAttribute)), ValuatorAttribute)
            If valuatorAttribute Is Nothing Then
                Throw New System.ArgumentException("ValuatorAttribute was not defined.", "id")
            End If
            Dim text As String = Nothing
            If text Is Nothing Then
                Dim member As System.Reflection.MemberInfo() = type.GetMember(id.ToString())
                Dim defaultStringAttribute As ValueAttribute = TryCast(System.Attribute.GetCustomAttribute(member(0), GetType(ValueAttribute)), ValueAttribute)
                If defaultStringAttribute IsNot Nothing Then
                    text = defaultStringAttribute.Text
                Else
                    text = String.Empty
                End If
            End If
            Dim stringBuilder As System.Text.StringBuilder = New System.Text.StringBuilder()
            Dim i As Integer = 0
            While i < text.Length
                If text(i) = "\" AndAlso i < text.Length - 1 AndAlso text(i + 1) = "\" Then
                    stringBuilder.Append("\")
                    i += 2
                Else
                    If text(i) = "\" AndAlso i < text.Length - 1 AndAlso text(i + 1) = "n" Then
                        stringBuilder.AppendLine()
                        i += 2
                    Else
                        If text(i) = "\" AndAlso i < text.Length - 1 AndAlso text(i + 1) = "t" Then
                            stringBuilder.Append(vbTab)
                            i += 2
                        Else
                            Dim arg_172_0 As System.Text.StringBuilder = stringBuilder
                            Dim arg_16D_0 As String = text
                            Dim expr_168 As Integer = i
                            i = expr_168 + 1
                            arg_172_0.Append(arg_16D_0(expr_168))
                        End If
                    End If
                End If
            End While
            text = stringBuilder.ToString()
            If args IsNot Nothing Then
                Return String.Format(text, args)
            End If
            Return text
        End Function

        Public Shared Function GetEnumStringsWithExclude(type As System.Type, ParamArray idToExclude As System.[Enum]()) As String()
            If Not type.IsEnum Then
                Throw New System.ArgumentException("type is not enum.", "type")
            End If
            Dim list As System.Collections.Generic.List(Of String) = New System.Collections.Generic.List(Of String)()
            Dim valuatorAttribute As ValuatorAttribute = TryCast(System.Attribute.GetCustomAttribute(type, GetType(ValuatorAttribute)), ValuatorAttribute)
            If valuatorAttribute Is Nothing Then
                Throw New System.ArgumentException("ValuatorAttribute was not defined.", "type")
            End If
            Dim fields As System.Reflection.FieldInfo() = type.GetFields()
            Dim i As Integer = 0
            While i < fields.Length
                Dim fieldInfo As System.Reflection.FieldInfo = fields(i)
                If idToExclude Is Nothing OrElse idToExclude.Length <= 0 Then
                    GoTo SkipWhile_97
                End If
                Dim flag As Boolean = False
                For j As Integer = 0 To idToExclude.Length - 1
                    If fieldInfo.Name.Equals(idToExclude(j).ToString()) Then
                        flag = True
                        Exit For
                    End If
                Next
                If Not flag Then
                    GoTo SkipWhile_97
                End If
Reset_105:
                i += 1
                Continue While
SkipWhile_97:
                Dim array As ValueAttribute() = TryCast(fieldInfo.GetCustomAttributes(GetType(ValueAttribute), False), ValueAttribute())
                If array.Length <= 0 Then
                    GoTo Reset_105
                End If
                Dim text As String = Nothing
                If text IsNot Nothing Then
                    list.Add(text)
                    GoTo Reset_105
                End If
                list.Add(array(0).Text)
                GoTo Reset_105
            End While
            Return list.ToArray()
        End Function

        Public Shared Function GetEnumStrings(type As System.Type) As String()
            Return Valuator.GetEnumStringsWithExclude(type, New System.[Enum](0 - 1) {})
        End Function

        ''' <summary>
        ''' Map enum which has LocalizableString to table which can be used by LookupEditBuilder.
        ''' </summary>
        Public Shared Function GetEnumTable(id As Type) As DataTable
            Dim dt As New DataTable
            dt.Columns.Add("NAME", GetType(String))
            dt.Columns.Add("ID", GetType(String))

            If id Is Nothing Then
                Return dt
            End If

            Try
                Dim items As Array = System.Enum.GetValues(id)
                For Each item As Object In items
                    Dim drv As DataRowView = Ape.EL.Misc.DataHelper.ConvertToDataView(dt).AddNew
                    drv.BeginEdit()
                    drv("NAME") = Valuator.GetString(item)
                    drv("ID") = item.ToString
                    drv.EndEdit()
                Next
            Catch ex As Exception
                Debug.Assert(False, ex.ToString)
            End Try

            Return dt
        End Function
    End Class
End Namespace
