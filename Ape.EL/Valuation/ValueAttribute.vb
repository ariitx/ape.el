﻿Namespace Valuation
    <System.AttributeUsage(System.AttributeTargets.Field)>
    Public Class ValueAttribute
        Inherits System.Attribute

        Private myText As String

        Public ReadOnly Property Text() As String
            Get
                Return Me.myText
            End Get
        End Property

        Private Sub Initialize(text As String)
            If text Is Nothing Then
                Throw New System.ArgumentNullException("text")
            End If
            Me.myText = text
        End Sub

        Public Sub New(text As String)
            Initialize(text)
        End Sub

        Public Sub New(obTtext As Object)
            'Dim objType As Type = obTtext.GetType.BaseType
            If obTtext IsNot Nothing Then
                Initialize(obTtext.ToString)
            Else
                Initialize(Nothing)
            End If
        End Sub
    End Class
End Namespace