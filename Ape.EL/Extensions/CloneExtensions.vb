﻿'Source code is released under the MIT license.
'The MIT License (MIT)
'Copyright (c) 2014 Burtsev Alexey
'Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
'The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
'THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

'I wrote deep object copy extension method, based on recursive "MemberwiseClone", it is fast (3 times faster then BinaryFormatter), it works with any object, you don't need default constructor or serializable attributes.
'https://github.com/Burtsev-Alexey/net-object-deep-copy

Imports System.Collections.Generic
Imports System.Reflection
Imports System.Runtime.CompilerServices
Imports System.Runtime.Serialization
Imports System.Runtime.Serialization.Formatters.Binary
Imports System.IO
Imports System.Web.Script.Serialization

Namespace Extensions
    <System.Diagnostics.DebuggerStepThrough()>
    <HideModuleName()>
    Public Module CloneExtensions
#Region "DeepCopy Extension"
        'Private ReadOnly CloneMethod As MethodInfo = GetType([Object]).GetMethod("MemberwiseClone", BindingFlags.NonPublic Or BindingFlags.Instance)

        'Private Function IsPrimitive(type As Type) As Boolean
        '    If type = GetType([String]) Then
        '        Return True
        '    End If
        '    Return (type.IsValueType And type.IsPrimitive)
        'End Function

        'Private Function Copy(originalObject As [Object]) As [Object]
        '    Return InternalCopy(originalObject, New Dictionary(Of [Object], [Object])(New ReferenceEqualityComparer()))
        'End Function

        'Private Function InternalCopy(originalObject As [Object], visited As IDictionary(Of [Object], [Object])) As [Object]
        '    If originalObject Is Nothing Then
        '        Return Nothing
        '    End If
        '    Dim typeToReflect = originalObject.[GetType]()
        '    If IsPrimitive(typeToReflect) Then
        '        Return originalObject
        '    End If
        '    If visited.ContainsKey(originalObject) Then
        '        Return visited(originalObject)
        '    End If
        '    If GetType([Delegate]).IsAssignableFrom(typeToReflect) Then
        '        Return Nothing
        '    End If
        '    'If GetType(Pointer) = typeToReflect Then
        '    '    Return Nothing
        '    'End If
        '    Dim cloneObject = CloneMethod.Invoke(originalObject, Nothing)
        '    If typeToReflect.IsArray Then
        '        Dim arrayType = typeToReflect.GetElementType()
        '        If IsPrimitive(arrayType) = False Then
        '            Dim clonedArray As Array = DirectCast(cloneObject, Array)
        '            clonedArray.ForEach(Sub(array, indices) array.SetValue(InternalCopy(clonedArray.GetValue(indices), visited), indices))
        '        End If
        '    End If
        '    visited.Add(originalObject, cloneObject)
        '    CopyFields(originalObject, visited, cloneObject, typeToReflect)
        '    RecursiveCopyBaseTypePrivateFields(originalObject, visited, cloneObject, typeToReflect)
        '    Return cloneObject
        'End Function

        'Private Sub RecursiveCopyBaseTypePrivateFields(originalObject As Object, visited As IDictionary(Of Object, Object), cloneObject As Object, typeToReflect As Type)
        '    If typeToReflect.BaseType IsNot Nothing Then
        '        RecursiveCopyBaseTypePrivateFields(originalObject, visited, cloneObject, typeToReflect.BaseType)
        '        CopyFields(originalObject, visited, cloneObject, typeToReflect.BaseType, BindingFlags.Instance Or BindingFlags.NonPublic, Function(info) info.IsPrivate)
        '    End If
        'End Sub

        'Private Sub CopyFields(originalObject As Object, visited As IDictionary(Of Object, Object), cloneObject As Object, typeToReflect As Type, Optional bindingFlags__1 As BindingFlags = BindingFlags.Instance Or BindingFlags.NonPublic Or BindingFlags.[Public] Or BindingFlags.FlattenHierarchy, Optional filter As Func(Of FieldInfo, Boolean) = Nothing)
        '    For Each fieldInfo As FieldInfo In typeToReflect.GetFields(bindingFlags__1)
        '        If filter IsNot Nothing AndAlso filter(fieldInfo) = False Then
        '            Continue For
        '        End If
        '        If IsPrimitive(fieldInfo.FieldType) Then
        '            Continue For
        '        End If
        '        Dim originalFieldValue = fieldInfo.GetValue(originalObject)
        '        Dim clonedFieldValue = InternalCopy(originalFieldValue, visited)
        '        fieldInfo.SetValue(cloneObject, clonedFieldValue)
        '    Next
        'End Sub

        ' ''' <summary>
        ' ''' Deep copy an object without needs of serializable attribute //Burtsev-Alexey
        ' ''' </summary>
        '<Extension> <Obsolete("Use clone method.", True)>
        'Public Function DeepCopy(Of T)(original As T) As T
        '    Return DirectCast(Copy(DirectCast(original, [Object])), T)
        'End Function

        '<Extension>
        'Private Sub ForEach(array As Array, action As Action(Of Array, Integer()))
        '    If array.LongLength = 0 Then
        '        Return
        '    End If
        '    Dim walker As New ArrayTraverse(array)
        '    Do
        '        action(array, walker.Position)
        '    Loop While walker.[Step]()
        'End Sub

        'Private Class ArrayTraverse
        '    Public Position As Integer()
        '    Private maxLengths As Integer()

        '    Public Sub New(array As Array)
        '        maxLengths = New Integer(array.Rank - 1) {}
        '        For i As Integer = 0 To array.Rank - 1
        '            maxLengths(i) = array.GetLength(i) - 1
        '        Next
        '        Position = New Integer(array.Rank - 1) {}
        '    End Sub

        '    Public Function [Step]() As Boolean
        '        For i As Integer = 0 To Position.Length - 1
        '            If Position(i) < maxLengths(i) Then
        '                Position(i) += 1
        '                For j As Integer = 0 To i - 1
        '                    Position(j) = 0
        '                Next
        '                Return True
        '            End If
        '        Next
        '        Return False
        '    End Function
        'End Class

        'Private Class ReferenceEqualityComparer
        '    Inherits EqualityComparer(Of [Object])
        '    Public Overrides Function Equals(x As Object, y As Object) As Boolean
        '        Return ReferenceEquals(x, y)
        '    End Function
        '    Public Overrides Function GetHashCode(obj As Object) As Integer
        '        If obj Is Nothing Then
        '            Return 0
        '        End If
        '        Return obj.GetHashCode()
        '    End Function
        'End Class
#End Region

#Region "Clone Extension"
        ''' <summary>
        ''' Same as DeepCopy.
        ''' Use rClone for better output.
        ''' </summary>
        <Extension>
        Public Function Clone(Of T)(source As T) As T
            Return source.DeepCopy
        End Function

        ''' <summary>
        ''' Deep clone method using JSON serialization, thus removing the needs of Serializable type on the object.
        ''' This method can only be applied on non complex classes. Classes such as dataview, form or any class with complex object will throw exception.
        ''' Use rClone for better output.
        ''' </summary>
        <Extension>
        Public Function DeepCopy(Of T)(objSource As T) As T
            ' Don't serialize a null object, simply return the default for that object
            If [Object].ReferenceEquals(objSource, Nothing) Then
                Return Nothing
            End If

            'Serialize the object as stream
            Dim stream1 As New MemoryStream()
            Dim ser As New Json.DataContractJsonSerializer(GetType(T))
            ser.WriteObject(stream1, objSource)
            stream1.Position = 0
            Dim sr As New StreamReader(stream1)

            'Deserialize the stream as object
            stream1.Position = 0
            Dim p2 As T = DirectCast(ser.ReadObject(stream1), T)
            Return p2
        End Function

        ''' <summary>
        ''' Create a DeepCopy and overwrite the result to self object.
        '''  Use rCopyFrom or rRefCopyFrom for better output.
        ''' </summary>
        <Extension>
        Public Sub CopyFrom(Of T)(ByRef selfObj As T, source As T)
            Dim cloneOfSource As T = source.DeepCopy
            selfObj = cloneOfSource
        End Sub

        ''' <summary>
        ''' Clone using ReflectionCloner to a new object.
        ''' </summary>
        <Extension>
        Public Function rClone(Of T)(source As T) As T
            Return DeepClone.ReflectionCloner.DeepFieldClone(source)
        End Function

        ''' <summary>
        ''' Copy all fields from source to provided self object by reference. Will create another object if self object is null.
        ''' Clone using ReflectionClonerByRef.
        ''' </summary>
        <Extension>
        Public Sub rCopyFrom(Of T)(ByRef selfObj As T, source As T)
            DeepClone.ReflectionClonerByRef.DeepFieldClone(Of T)(selfObj, source)
        End Sub
#End Region
    End Module
End Namespace
