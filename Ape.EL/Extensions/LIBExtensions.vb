﻿Imports Ape.EL.WinForm.General
Imports Ape.EL.Data
Imports System.Runtime.CompilerServices
Imports System.Data.SqlClient
Imports System.Text.RegularExpressions
Imports System.IO
Imports System.Web.Script.Serialization

Namespace Extensions
    ''' <summary>
    ''' This is an extensions module.
    ''' </summary>
    <HideModuleName()> _
    Public Module LIBExtensions
#Region "SqlConnection Extensions"
        <Extension>
        Public Function GetReferencedObjects(ByVal con As System.Data.Common.DbConnection) As List(Of Object)
            Return Data.DbUtility.GetReferencedObjects(con)
        End Function

        ''' <summary>
        ''' Check whether your connection is open or not.
        ''' </summary>
        <Extension>
        Public Function DBConnected(sender As SqlConnection) As Boolean
            Return sender IsNot Nothing AndAlso sender.State = ConnectionState.Open
        End Function

        '-- GetRecords
        ''' <summary>
        ''' Set Transaction parameters to get records based on transaction. Please use SqlString.Format to encapsulate single quote.
        ''' </summary>
        <Extension>
        Function GetRecords(sender As SqlConnection, ByVal strSQL As String, Optional ByVal Transaction As SqlClient.SqlTransaction = Nothing) As SqlClient.SqlDataReader
            Return sender.GetRecords(strSQL, Nothing, Transaction)
        End Function

        <Extension>
        Function GetRecords(sender As SqlConnection, ByVal strSQL As String, ByVal Transaction As SqlClient.SqlTransaction, ByVal ParamArray param() As SqlParameter) As SqlClient.SqlDataReader
            Return sender.GetRecords(strSQL, param.ToList(), Transaction)
        End Function

        <Extension>
        Function GetRecords(sender As SqlConnection, ByVal strSQL As String, ByVal ParamArray param() As SqlParameter) As SqlClient.SqlDataReader
            Return sender.GetRecords(strSQL, param.ToList(), Nothing)
        End Function

        <Extension>
        Function GetRecords(sender As SqlConnection, ByVal strSQL As String, ByVal param As List(Of SqlParameter), Optional ByVal Transaction As SqlClient.SqlTransaction = Nothing) As SqlClient.SqlDataReader
            Try
                If DBConnected(sender) Then
                    ResetConnection(sender)
                    Dim commSQL As SqlClient.SqlCommand = New SqlClient.SqlCommand
                    Dim datTable As SqlClient.SqlDataReader

                    commSQL.Connection = sender
                    commSQL.CommandText = DbUtility.Sql.EscapeInsertSingleQuote(strSQL)
                    commSQL.Transaction = Transaction 'if myTran is not nothing, then this GetRecords will be transaction based.
                    If param IsNot Nothing Then commSQL.Parameters.AddRange(param.ToArray)
                    datTable = commSQL.ExecuteReader
                    commSQL.Dispose()
                    Return datTable
                Else
                    Throw New ApplicationException("No connection to database.")
                    Return Nothing
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        '-- GetAdapter
        ''' <summary>
        ''' Please use SqlString.Format to encapsulate single quote.
        ''' </summary>
        <Extension>
        Function GetAdapter(sender As SqlConnection, ByVal strSQL As String, Optional ByRef Transaction As SqlTransaction = Nothing) As SqlClient.SqlDataAdapter
            Try
                If DBConnected(sender) Then
                    ResetConnection(sender)
                    Dim adaptSQL As New SqlClient.SqlDataAdapter(DbUtility.Sql.EscapeInsertSingleQuote(strSQL), sender)
                    adaptSQL.SelectCommand.Transaction = Transaction
                    Return adaptSQL
                Else
                    Throw New ApplicationException("No connection to database.")
                    Return Nothing
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        '-- GetDataTable
        ''' <summary>
        ''' Please use SqlString.Format to encapsulate single quote.
        ''' </summary>
        <Extension>
        Function GetDataTable(sender As SqlConnection, ByVal strSQL As String, Optional ByRef Transaction As SqlTransaction = Nothing) As DataTable
            Return sender.GetDataTable(strSQL, Nothing, Transaction)
        End Function

        <Extension>
        Function GetDataTable(sender As SqlConnection, ByVal strSQL As String, ByRef Transaction As SqlTransaction, ByVal ParamArray param() As SqlParameter) As DataTable
            Return sender.GetDataTable(strSQL, param.ToList(), Transaction)
        End Function

        <Extension>
        Function GetDataTable(sender As SqlConnection, ByVal strSQL As String, ByVal ParamArray param() As SqlParameter) As DataTable
            Return sender.GetDataTable(strSQL, param.ToList(), Nothing)
        End Function

        <Extension>
        Function GetDataTable(sender As SqlConnection, ByVal strSQL As String, ByVal param As List(Of SqlParameter), Optional ByRef Transaction As SqlTransaction = Nothing) As DataTable
            Return Ape.EL.Misc.DataHelper.ConvertToDataTable(sender.GetDataView(strSQL, param, Transaction))
        End Function

        '-- GetDataView
        ''' <summary>
        ''' Please use SqlString.Format to encapsulate single quote.
        ''' </summary>
        <Extension>
        Function GetDataView(sender As SqlConnection, ByVal strSQL As String, Optional ByRef Transaction As SqlTransaction = Nothing) As DataView
            Return sender.GetDataView(strSQL, Nothing, Transaction)
        End Function

        <Extension>
        Function GetDataView(sender As SqlConnection, ByVal strSQL As String, ByRef Transaction As SqlTransaction, ByVal ParamArray param() As SqlParameter) As DataView
            Return sender.GetDataView(strSQL, param.ToList(), Transaction)
        End Function

        <Extension>
        Function GetDataView(sender As SqlConnection, ByVal strSQL As String, ByVal ParamArray param() As SqlParameter) As DataView
            Return sender.GetDataView(strSQL, param.ToList(), Nothing)
        End Function

        <Extension>
        Function GetDataView(sender As SqlConnection, ByVal strSQL As String, ByVal param As List(Of SqlParameter), Optional ByRef Transaction As SqlTransaction = Nothing) As DataView
            Try
                If DBConnected(sender) Then
                    Dim ds As DataSet = New DataSet()

                    Using oleSQLDataAdapter As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(DbUtility.Sql.EscapeInsertSingleQuote(strSQL), sender)
                        oleSQLDataAdapter.SelectCommand.Transaction = Transaction
                        If param IsNot Nothing Then oleSQLDataAdapter.SelectCommand.Parameters.AddRange(param.ToArray)
                        oleSQLDataAdapter.SelectCommand.CommandTimeout = InternalGlobal.DefaultQueryTimeout ' Default is only 30 Second
                        Try
                            oleSQLDataAdapter.Fill(ds, "temp")
                        Catch ex As Exception
                            If IsSqlTransportException(ex) Then oleSQLDataAdapter.Fill(ds, "temp")
                        End Try
                        oleSQLDataAdapter.Dispose()
                    End Using

                    Dim dvManager As DataViewManager = New DataViewManager(ds)
                    Dim dv As DataView = dvManager.CreateDataView(ds.Tables("temp"))

                    Return dv
                Else
                    Throw New ApplicationException("No connection to database.")
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        <Extension>
        Function GetDataSet(sender As SqlConnection, ByVal strSQL As String, Optional ByRef Transaction As SqlTransaction = Nothing) As DataSet
            Return sender.GetDataSet(strSQL, Nothing, Transaction)
        End Function

        <Extension>
        Function GetDataSet(sender As SqlConnection, ByVal strSQL As String, ByRef Transaction As SqlTransaction, ByVal ParamArray param() As SqlParameter) As DataSet
            Return sender.GetDataSet(strSQL, param.ToList(), Transaction)
        End Function

        <Extension>
        Function GetDataSet(sender As SqlConnection, ByVal strSQL As String, ByVal ParamArray param() As SqlParameter) As DataSet
            Return sender.GetDataSet(strSQL, param.ToList(), Nothing)
        End Function

        '-- GetDataSet
        <Extension>
        Function GetDataSet(sender As SqlConnection, ByVal strSQL As String, ByVal param As List(Of SqlParameter), Optional ByRef Transaction As SqlTransaction = Nothing) As DataSet
            Try
                If DBConnected(sender) Then
                    Dim ds As DataSet = New DataSet()

                    Using oleSQLDataAdapter As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(DbUtility.Sql.EscapeInsertSingleQuote(strSQL), sender)
                        oleSQLDataAdapter.SelectCommand.Transaction = Transaction
                        If param IsNot Nothing Then oleSQLDataAdapter.SelectCommand.Parameters.AddRange(param.ToArray)
                        oleSQLDataAdapter.SelectCommand.CommandTimeout = InternalGlobal.DefaultQueryTimeout ' Default is only 30 Second
                        Try
                            oleSQLDataAdapter.Fill(ds)
                        Catch ex As Exception
                            If IsSqlTransportException(ex) Then oleSQLDataAdapter.Fill(ds)
                        End Try
                        oleSQLDataAdapter.Dispose()
                    End Using

                    Return ds
                Else
                    Throw New ApplicationException("No connection to database.")
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        '-- GetDataRowView
        ''' <summary>
        ''' Please use SqlString.Format to encapsulate single quote.
        ''' </summary>
        <Extension>
        Function GetDataRowView(sender As SqlConnection, ByVal strSQL As String, Optional ByVal Row As Integer = 0, Optional ByRef Transaction As SqlTransaction = Nothing) As DataRowView
            Return sender.GetDataRowView(strSQL, Nothing, Row, Transaction)
        End Function

        <Extension>
        Function GetDataRowView(sender As SqlConnection, ByVal strSQL As String, ByVal Row As Integer, ByRef Transaction As SqlTransaction, ByVal ParamArray param() As SqlParameter) As DataRowView
            Return sender.GetDataRowView(strSQL, param.ToList(), Row, Transaction)
        End Function

        <Extension>
        Function GetDataRowView(sender As SqlConnection, ByVal strSQL As String, ByVal ParamArray param() As SqlParameter) As DataRowView
            Return sender.GetDataRowView(strSQL, param.ToList(), 0, Nothing)
        End Function

        <Extension>
        Function GetDataRowView(sender As SqlConnection, ByVal strSQL As String, ByVal param As List(Of SqlParameter), Optional ByVal Row As Integer = 0, Optional ByRef Transaction As SqlTransaction = Nothing) As DataRowView
            Try
                Dim dv As DataView = sender.GetDataView(strSQL, param, Transaction)

                If dv IsNot Nothing AndAlso dv.Count > 0 Then
                    Return dv(Row)
                Else
                    Return Nothing
                End If
            Catch ex As Exception
                If ex.InnerException IsNot Nothing Then Throw ex.InnerException Else Throw ex
            End Try
        End Function

        '-- ExecuteScalar
        ''' <summary>
        ''' Get first column result from query. Please use SqlString.Format to encapsulate single quote.
        ''' </summary>
        <Extension>
        Function ExecuteScalar(sender As SqlConnection, ByVal strSQL As String, Optional ByVal Row As Integer = 0, Optional ByRef Transaction As SqlTransaction = Nothing) As String
            Return sender.ExecuteScalar(strSQL, Nothing, Row, Transaction)
        End Function

        <Extension>
        Function ExecuteScalar(sender As SqlConnection, ByVal strSQL As String, ByVal Row As Integer, ByRef Transaction As SqlTransaction, ByVal ParamArray param() As SqlParameter) As String
            Return sender.ExecuteScalar(strSQL, param.ToList(), Row, Transaction)
        End Function

        <Extension>
        Function ExecuteScalar(sender As SqlConnection, ByVal strSQL As String, ByVal ParamArray param() As SqlParameter) As String
            Return sender.ExecuteScalar(strSQL, param.ToList(), 0, Nothing)
        End Function

        <Extension>
        Function ExecuteScalar(sender As SqlConnection, ByVal strSQL As String, ByVal param As List(Of SqlParameter), Optional ByVal Row As Integer = 0, Optional ByRef Transaction As SqlTransaction = Nothing) As String
            Try
                Dim drv As DataRowView = sender.GetDataRowView(strSQL, param, Row, Transaction)
                If drv IsNot Nothing Then
                    Return drv(0).ToString
                Else
                    Return String.Empty
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        '-- ExecuteNonScalar
        ''' <summary>
        ''' Please use SqlString.Format to encapsulate single quote.
        ''' </summary>
        <Extension>
        Function ExecuteNonQuery(sender As SqlConnection, ByVal strSQL As String, Optional ByVal Transaction As SqlClient.SqlTransaction = Nothing) As Boolean
            Return sender.ExecuteNonQuery(strSQL, Nothing, Transaction)
        End Function

        <Extension>
        Function ExecuteNonQuery(sender As SqlConnection, ByVal strSQL As String, ByVal Transaction As SqlClient.SqlTransaction, ByVal ParamArray param() As SqlParameter) As Boolean
            Return sender.ExecuteNonQuery(strSQL, param.ToList(), Transaction)
        End Function

        <Extension>
        Function ExecuteNonQuery(sender As SqlConnection, ByVal strSQL As String, ByVal ParamArray param() As SqlParameter) As Boolean
            Return sender.ExecuteNonQuery(strSQL, param.ToList(), Nothing)
        End Function

        <Extension>
        Function ExecuteNonQuery(sender As SqlConnection, ByVal strSQL As String, ByVal param As List(Of SqlParameter), Optional ByVal Transaction As SqlClient.SqlTransaction = Nothing) As Boolean
            Dim result As Boolean
            Try
                ResetConnection(sender)
                If DBConnected(sender) Then
                    Dim commSQL As SqlClient.SqlCommand = New SqlClient.SqlCommand
                    commSQL.Connection = sender
                    commSQL.CommandText = DbUtility.Sql.EscapeInsertSingleQuote(strSQL)
                    If param IsNot Nothing Then commSQL.Parameters.AddRange(param.ToArray)
                    commSQL.Transaction = Transaction 'if myTran is not nothing, then this GetRecords will be transaction based.
                    commSQL.ExecuteNonQuery()
                    commSQL.Dispose()
                    result = True
                Else
                    Throw New ApplicationException("No connection to database.")
                End If
            Catch ex As Exception
                Throw ex
            End Try
            Return result
        End Function

        '-- Create SQLCommand
        <Extension>
        Function GetSqlCommand(sender As SqlConnection, ByVal strSQL As String) As SqlCommand
            Dim myCommand As New SqlClient.SqlCommand(strSQL, sender)
            myCommand.CommandTimeout = InternalGlobal.DefaultQueryTimeout
            Return myCommand
        End Function

        '-- Check database exists
        <Extension>
        Function ContainsDb(sender As SqlConnection, ByVal dbname As String) As Boolean
            Dim val As Integer = sender.ExecuteScalar(Data.DbUtility.SqlString.Format("SELECT Count(1) AS HasDb FROM master.dbo.sysdatabases WHERE ('[' + name + ']' = '{0}' OR name = '{0}')", dbname)).ToInt32()
            Return val > 0
        End Function

#Region "SQL Version Info"
        <Extension>
        Function GetSQLServerFullVersion(sender As SqlConnection) As String
            Return sender.ExecuteScalar("SELECT @@Version")
        End Function

        <Extension>
        Function GetSQLServerEdition(sender As SqlConnection) As String
            Return sender.ExecuteScalar("SELECT SERVERPROPERTY('Edition')")
        End Function

        <Extension>
        Function GetSQLServerProductVersion(sender As SqlConnection) As String
            Return sender.ExecuteScalar("SELECT SERVERPROPERTY('productversion')")
        End Function

        <Extension>
        Function GetSQLServerProductLevel(sender As SqlConnection) As String
            Return sender.ExecuteScalar("SELECT SERVERPROPERTY('productlevel')")
        End Function
#End Region

#Region "Reconnect"
        ''' <summary>
        ''' Try to restart connection if the state is closed.
        ''' </summary>
        Private Sub ResetConnection(ByRef obj As SqlClient.SqlConnection)
            If obj IsNot Nothing AndAlso obj.State = ConnectionState.Closed Then
                obj.Close()
                obj.Open()
            End If
        End Sub

        ''' <summary>
        ''' Determine if exception is caused by sql transport forcibly closed.
        ''' </summary>
        Private Function IsSqlTransportException(ex As Exception) As Boolean
            Const _SqlTransportExceptionMsg As String = "A transport-level error has occurred when sending the request to the server. (provider: TCP Provider, error: 0 - An existing connection was forcibly closed by the remote host.)"
            Return ex IsNot Nothing AndAlso ex.GetType Is GetType(SqlException) AndAlso ex.Message = _SqlTransportExceptionMsg
        End Function
#End Region
#End Region

#Region "SqlTransaction Extensions"
        <Extension>
        Function GetRecords(sender As SqlTransaction, ByVal strSQL As String, ByVal param As List(Of SqlParameter)) As SqlClient.SqlDataReader
            Return sender.Connection.GetRecords(strSQL, param, sender)
        End Function

        <Extension>
        Function GetRecords(sender As SqlTransaction, ByVal strSQL As String, ByVal ParamArray param() As SqlParameter) As SqlClient.SqlDataReader
            Return sender.Connection.GetRecords(strSQL, param.ToList(), sender)
        End Function

        <Extension>
        Function GetAdapter(sender As SqlTransaction, ByVal strSQL As String) As SqlClient.SqlDataAdapter
            Return sender.Connection.GetAdapter(strSQL, sender)
        End Function

        <Extension>
        Function GetDataTable(sender As SqlTransaction, ByVal strSQL As String, ByVal param As List(Of SqlParameter)) As DataTable
            Return sender.Connection.GetDataTable(strSQL, param, sender)
        End Function

        <Extension>
        Function GetDataTable(sender As SqlTransaction, ByVal strSQL As String, ByVal ParamArray param() As SqlParameter) As DataTable
            Return sender.Connection.GetDataTable(strSQL, param.ToList(), sender)
        End Function

        <Extension>
        Function GetDataView(sender As SqlTransaction, ByVal strSQL As String, ByVal param As List(Of SqlParameter)) As DataView
            Return sender.Connection.GetDataView(strSQL, param, sender)
        End Function

        <Extension>
        Function GetDataView(sender As SqlTransaction, ByVal strSQL As String, ByVal ParamArray param() As SqlParameter) As DataView
            Return sender.Connection.GetDataView(strSQL, param.ToList(), sender)
        End Function

        <Extension>
        Function GetDataSet(sender As SqlTransaction, ByVal strSQL As String, ByVal param As List(Of SqlParameter)) As DataSet
            Return sender.Connection.GetDataSet(strSQL, param, sender)
        End Function

        <Extension>
        Function GetDataSet(sender As SqlTransaction, ByVal strSQL As String, ByVal ParamArray param() As SqlParameter) As DataSet
            Return sender.Connection.GetDataSet(strSQL, param.ToList(), sender)
        End Function

        <Extension>
        Function GetDataRowView(sender As SqlTransaction, ByVal strSQL As String, ByVal param As List(Of SqlParameter), Optional ByVal Row As Integer = 0) As DataRowView
            Return sender.Connection.GetDataRowView(strSQL, param, Row, sender)
        End Function

        <Extension>
        Function GetDataRowView(sender As SqlTransaction, ByVal strSQL As String, ByVal Row As Integer, ByVal ParamArray param() As SqlParameter) As DataRowView
            Return sender.Connection.GetDataRowView(strSQL, param.ToList(), Row, sender)
        End Function

        <Extension>
        Function GetDataRowView(sender As SqlTransaction, ByVal strSQL As String, ByVal ParamArray param() As SqlParameter) As DataRowView
            Return sender.Connection.GetDataRowView(strSQL, param.ToList(), 0, sender)
        End Function

        <Extension>
        Function ExecuteScalar(sender As SqlTransaction, ByVal strSQL As String, ByVal param As List(Of SqlParameter), Optional ByVal Row As Integer = 0) As String
            Return sender.Connection.ExecuteScalar(strSQL, param, Row, sender)
        End Function

        <Extension>
        Function ExecuteScalar(sender As SqlTransaction, ByVal strSQL As String, ByVal Row As Integer, ByVal ParamArray param() As SqlParameter) As String
            Return sender.Connection.ExecuteScalar(strSQL, param.ToList(), Row, sender)
        End Function

        <Extension>
        Function ExecuteScalar(sender As SqlTransaction, ByVal strSQL As String, ByVal ParamArray param() As SqlParameter) As String
            Return sender.Connection.ExecuteScalar(strSQL, param.ToList(), 0, sender)
        End Function

        <Extension>
        Function ExecuteNonQuery(sender As SqlTransaction, ByVal strSQL As String, ByVal param As List(Of SqlParameter)) As Boolean
            Return sender.Connection.ExecuteNonQuery(strSQL, param, sender)
        End Function

        <Extension>
        Function ExecuteNonQuery(sender As SqlTransaction, ByVal strSQL As String, ByVal ParamArray param() As SqlParameter) As Boolean
            Return sender.Connection.ExecuteNonQuery(strSQL, param.ToList(), sender)
        End Function

        '-- Create SQLCommand
        <Extension>
        Function GetSqlCommand(sender As SqlTransaction, ByVal strSQL As String) As SqlCommand
            Dim myCommand As New SqlClient.SqlCommand(strSQL, sender.Connection, sender)
            myCommand.CommandTimeout = InternalGlobal.DefaultQueryTimeout
            Return myCommand
        End Function
#End Region

#Region "SqlCommand Extensions"
        ''' <summary>
        ''' Adds a value to the end of the System.Data.SqlClient.SqlParameterCollection. This will convert nothing value to DbNull.Value.
        ''' </summary>
        ''' <param name="parameterName">The name of the parameter.</param>
        ''' <param name="value">The value to be added.</param>
        ''' <returns>A System.Data.SqlClient.SqlParameter object.</returns>
        <Extension> <Obsolete("Use SqlCommand.AddwithValueNull instead", True)>
        Function AddWithValueNull(sender As SqlParameterCollection, parameterName As String, value As Object) As System.Data.SqlClient.SqlParameter
            If value Is Nothing Then
                value = DBNull.Value
            End If

            Return sender.AddWithValue(parameterName, value)
        End Function

        ''' <summary>
        ''' Adds a value to the end of the System.Data.SqlClient.SqlParameterCollection. This will convert nothing value to DbNull.Value.
        ''' This function will also update the SqlCommand.CommandText to use default value when param value is null (using ISNULL) for update query.
        ''' </summary>
        ''' <param name="parameterName">The name of the parameter.</param>
        ''' <param name="value">The value to be added.</param>
        ''' <param name="useNullQuery">Enable or disable updating the commandText to use ori value when its value is null (only on update query, not for insert)</param>
        ''' <param name="setNullValue">Value to replace in ISNULL(@param,nullValue)</param>
        ''' <param name="valueType">You must explicitly declare the value's type in case the value is nothing. Byte() and Drawing.Image must declare.</param>
        ''' <returns>A System.Data.SqlClient.SqlParameter object.</returns>
        <Extension>
        Function AddWithValueNull(sender As SqlCommand, parameterName As String, value As Object, Optional ByVal useNullQuery As Boolean = True, Optional ByVal setNullValue As String = "", Optional valueType As Type = Nothing) As System.Data.SqlClient.SqlParameter
            If Not String.IsNullOrEmpty(sender.CommandText.Trim) AndAlso useNullQuery AndAlso Not String.IsNullOrEmpty(parameterName) AndAlso parameterName.StartsWith("@") Then
                If String.IsNullOrEmpty(setNullValue) Then setNullValue = parameterName.Replace("@", "") 'if the setNullValue is not set, use ori param value to update
                Dim input As String = sender.CommandText
                Dim replacement As String = String.Format("= ISNULL({0},{1})", parameterName, setNullValue)
                Dim pattern As String = String.Format("=\ *?{0}(?=\ |,|\n)", parameterName) 'find pattern starts with "=", followed by any number of " ", then @paramName. @paramName must be followed by " " or "," or new line.
                Dim options As RegexOptions = RegexOptions.IgnoreCase Or RegexOptions.IgnorePatternWhitespace Or RegexOptions.Multiline
                Dim result As String = New Regex(pattern, options).Replace(input, replacement)
                If Not String.IsNullOrEmpty(result) Then
                    sender.CommandText = result
                End If
            End If

            If value Is Nothing Then
                value = DBNull.Value
                Dim sqlParam As SqlParameter = Nothing
                If valueType IsNot Nothing Then
                    If valueType Is GetType(Byte()) Then
                        sqlParam = sender.Parameters.Add(parameterName, SqlDbType.VarBinary)
                    ElseIf TypeOf value Is Drawing.Image Then
                        sqlParam = sender.Parameters.Add(parameterName, SqlDbType.Image)
                    End If
                    sqlParam.Value = value
                Else
                    sqlParam = sender.Parameters.AddWithValue(parameterName, value)
                End If
                Return sqlParam
            End If

            Return sender.Parameters.AddWithValue(parameterName, value)
        End Function

        ''' <summary>
        ''' Print Sql Parameters from collection to console.
        ''' </summary>
        <Extension>
        Sub PrintValues(sender As SqlParameterCollection)
            For i As Integer = 0 To sender.Count - 1
                Console.WriteLine(String.Format("{0}:{1}", sender(i).ParameterName, sender(i).Value))
            Next
        End Sub
#End Region

#Region "String Extensions"
        ''' <summary>
        ''' Format string to dd/MMM/yyyy HH:mm:ss.
        ''' </summary>
        <Extension>
        Function FDateTime(sender As String) As String
            If IsDate(sender.ToStr.Trim) Then
                Return sender.ToStr.Trim.ToDateTime.FDateTime
            End If
            Return sender
        End Function

        ''' <summary>
        ''' Format string to dd/MMM/yyyy.
        ''' </summary>
        <Extension>
        Function FDate(sender As String) As String
            If IsDate(sender.ToStr.Trim) Then
                Return sender.ToStr.Trim.ToDateTime.FDate
            End If
            Return sender
        End Function

        ''' <summary>
        ''' Format string to HH:mm:ss.
        ''' </summary>
        <Extension>
        Function FTime(sender As String) As String
            Dim val As String = sender.ToStr.Trim
            If IsDate(val) Then
                Return val.ToDateTime.FTime
            End If
            Return sender
        End Function

        ''' <summary>
        ''' Convert string to DateTime type.
        ''' </summary>
        <Extension>
        Function ToDateTime(sender As String) As DateTime
            Dim val As String = sender.ToStr.Trim
            If IsDate(val) Then
                Return Ape.EL.Data.Convert.ToDateTime(val)
            End If
            Return DateTime.MinValue
        End Function

        <Extension>
        Function ToInt32(sender As String) As Int32
            Return Ape.EL.Data.Convert.ToInt32(sender.ToStr.Trim)
        End Function

        <Extension>
        Function ToDecimal(sender As String) As Decimal
            Return Ape.EL.Data.Convert.ToDecimal(sender.ToStr.Trim)
        End Function

        <Extension>
        Function ToBoolean(sender As String) As Boolean
            Return Ape.EL.Data.Convert.ToBoolean(sender.ToStr.Trim)
        End Function

        ''' <summary>
        ''' Check for IsNullOrEmpty for the string.
        ''' </summary>
        <Extension>
        Function IsEmpty(sender As String) As Boolean
            Return String.IsNullOrEmpty(sender)
        End Function

        ''' <summary>
        ''' Neutralize all null string to String.Empty.
        ''' </summary>
        <Extension()> _
        Function ToEmpty(ByVal sender As String) As String
            Return If(sender.IsEmpty, String.Empty, sender)
        End Function

        ''' <summary>
        ''' Append string using StringBuilder.
        ''' </summary>
        <Extension>
        Sub Append(ByRef sender As String, appendable As String)
            Dim sb As New System.Text.StringBuilder
            sb.Append(sender)
            sb.Append(appendable)
            sender = sb.ToString
        End Sub

        ''' <summary>
        ''' Append string using StringBuilder.
        ''' </summary>
        <Extension>
        Sub AppendLn(ByRef sender As String, appendable As String)
            sender.Append(appendable)
            sender.Append(Environment.NewLine)
        End Sub

        ''' <summary>
        ''' Convert string (from point.ToString) to point.
        ''' </summary>
        <Extension>
        Function ToPoint(sender As String) As System.Drawing.Point
            Dim g As String() = Regex.Replace(sender, "[\{\}a-zA-Z=]", "").Split(","c)

            If g.Count = 2 AndAlso IsNumeric(g(0)) AndAlso IsNumeric(g(1)) Then
                Return New System.Drawing.Point(Integer.Parse(g(0)), Integer.Parse(g(1)))
            End If

            Return New System.Drawing.Point
        End Function

        ''' <summary>
        ''' Convert string (from size.ToString) to size.
        ''' </summary>
        <Extension>
        Function ToSize(sender As String) As System.Drawing.Size
            Dim g As String() = Regex.Replace(sender, "[\{\}a-zA-Z=]", "").Split(","c)

            If g.Count = 2 AndAlso IsNumeric(g(0)) AndAlso IsNumeric(g(1)) Then
                Return New System.Drawing.Size(Integer.Parse(g(0)), Integer.Parse(g(1)))
            End If

            Return New System.Drawing.Size
        End Function

        ''' <summary>
        ''' Remove all characters in the param if match exactly in the string.
        ''' The removal is first come first remove. 
        ''' Means if some params extends the previous ones, it might not be removed as the characters have been removed previously.
        ''' </summary>
        <Extension>
        Function Remove(sender As String, ParamArray param() As String) As String
            If param IsNot Nothing AndAlso param.Count > 0 Then
                For Each p As String In param
                    sender = sender.Replace(p, "")
                Next
            End If
            Return sender
        End Function

        ''' <summary>
        ''' Check if any of the param value contained in the sender. Case ignored.
        ''' </summary>
        <Extension>
        Function Contains(sender As String, ParamArray param() As String) As Boolean
            If param IsNot Nothing AndAlso param.Count > 0 Then
                For Each p As String In param
                    If String.Compare(sender, p, True) = 0 Then
                        Return True
                    End If
                Next
            End If
            Return False
        End Function

        ''' <summary>
        ''' Return first n characters from string.
        ''' </summary>
        <Extension>
        Function First(sender As String, ByVal len As Integer) As String
            If sender.Length >= len Then
                Return sender.Substring(0, len)
            Else
                Return sender
            End If
        End Function

        ''' <summary>
        ''' Return last n characters from string.
        ''' </summary>
        <Extension>
        Function Last(sender As String, ByVal len As Integer) As String
            If sender.Length >= len Then
                Return sender.Substring(sender.Length - len, len)
            Else
                Return sender
            End If
        End Function

        ''' <summary>
        ''' Return true if input value contain unicode character.
        ''' </summary>
        <Extension>
        Public Function ContainsUnicodeCharacter(ByVal input As String) As Boolean
            Const MaxAnsiCode As Integer = 256
            Return input.Any(Function(c) System.Convert.ToInt32(c) > MaxAnsiCode)
        End Function

        <Extension()>
        Public Function LeftEx(ByVal str As String, ByVal length As Integer) As String
            Return str.Substring(0, Math.Min(length, str.Length))
        End Function

        <Extension()>
        Public Function RightEx(ByVal str As String, ByVal length As Integer) As String
            Return str.Substring(str.Length - Math.Min(length, str.Length))
        End Function
#End Region

#Region "String List Extensions"
        ''' <summary>
        ''' Remove all empty elements from string list.
        ''' </summary>
        <Extension>
        Sub RemoveEmpty(ByRef sender As List(Of String))
            While sender.Contains(String.Empty)
                sender.Remove(String.Empty)
            End While
        End Sub

        ''' <summary>
        ''' Remove first element in the list.
        ''' </summary>
        <Extension>
        Sub RemoveFirst(ByRef sender As List(Of String))
            If sender.Count > 0 Then sender.RemoveAt(0)
        End Sub

        ''' <summary>
        ''' Remove last element in the list.
        ''' </summary>
        <Extension>
        Sub RemoveLast(ByRef sender As List(Of String))
            If sender.Count > 0 Then sender.RemoveAt(sender.Count - 1)
        End Sub
#End Region

#Region "String array Extensions"
        ''' <summary>
        ''' Remove all empty elements from string array.
        ''' </summary>
        <Extension>
        Sub RemoveEmpty(ByRef sender As String())
            Dim k As List(Of String) = sender.ToList
            While k.Contains(String.Empty)
                k.Remove(String.Empty)
            End While
            sender = k.ToArray
        End Sub

        ''' <summary>
        ''' Remove element at specific position in the array.
        ''' </summary>
        <Extension>
        Sub RemoveAt(ByRef sender As String(), ByVal pos As Integer)
            Dim k As List(Of String) = sender.ToList
            If pos <= k.Count - 1 Then
                k.RemoveAt(pos)
            End If
            sender = k.ToArray
        End Sub

        ''' <summary>
        ''' Remove first element in the array.
        ''' </summary>
        <Extension>
        Sub RemoveFirst(ByRef sender As String())
            sender.RemoveAt(0)
        End Sub

        ''' <summary>
        ''' Remove last element in the array.
        ''' </summary>
        <Extension>
        Sub RemoveLast(ByRef sender As String())
            sender.RemoveAt(sender.Count - 1)
        End Sub

        ''' <summary>
        ''' Split string by new lines. It will remove all empty elements from string array.
        ''' </summary>
        <Extension>
        Function SplitNewLine(ByRef sender As String) As String()
            Dim s As String()
            s = Misc.StringHelper.ToMultiLineString(sender)
            's = sender.Split(Chr(10), Chr(13), vbNewLine)
            s.RemoveEmpty()
            Return s
        End Function
#End Region

#Region "DateTime Extensions"
        ''' <summary>
        ''' Convert DateTime and format to dd/MMM/yyyy HH:mm:ss.
        ''' </summary>
        <Extension>
        Function FDateTime(sender As DateTime) As String
            Return sender.ToString("dd/MMM/yyyy HH:mm:ss")
        End Function

        ''' <summary>
        ''' Convert DateTime and format to dd/MMM/yyyy.
        ''' </summary>
        <Extension>
        Function FDate(sender As DateTime) As String
            Return sender.ToString("dd/MMM/yyyy")
        End Function

        ''' <summary>
        ''' Convert DateTime and format to HH:mm:ss.
        ''' </summary>
        <Extension>
        Function FTime(sender As DateTime) As String
            Return sender.ToString("HH:mm:ss")
        End Function
#End Region

#Region "Drawing Extensions"
        <Extension>
        Function GetControlImage(sender As Windows.Forms.Control) As Drawing.Image
            Return Ape.EL.WinForm.ScreenCapture.CaptureWindow(sender.Handle)
        End Function

        <Extension>
        Function GetControlBitmap(sender As Windows.Forms.Control) As Drawing.Bitmap
            Return GetControlImage(sender).ToBitmap
        End Function

        <Extension>
        Function ToBitmap(sender As Drawing.Image) As Drawing.Bitmap
            Return Data.Convert.ImageToBitmap(sender)
        End Function

        <Extension>
        Function ToImage(sender As Drawing.Bitmap) As Drawing.Image
            Return Data.Convert.BitmapToImage(sender)
        End Function
#End Region

#Region "Control Extensions"
        ''' <summary>
        ''' Usage: 
        ''' ControlName.InvokeIfRequired(Sub(x As ControlType) x.AppendText(str))
        ''' </summary>
        <Extension> <System.Diagnostics.DebuggerStepThrough()>
        Public Sub InvokeIfRequired(c As Windows.Forms.Control, action As Action(Of Windows.Forms.Control))
            If c.InvokeRequired Then
                c.Invoke(New Action(Sub() action(c)))
            Else
                action(c)
            End If
        End Sub

        ''' <summary>
        ''' Usage: 
        ''' ControlName.BeginInvokeIfRequired(Sub(x As ControlType) x.AppendText(str))
        ''' </summary>
        <Extension> <System.Diagnostics.DebuggerStepThrough()>
        Public Function BeginInvokeIfRequired(c As Windows.Forms.Control, action As Action(Of Windows.Forms.Control)) As System.IAsyncResult
            If c.InvokeRequired Then
                Return c.BeginInvoke(New Action(Sub() action(c)))
            Else
                action(c)
                Return Nothing
            End If
        End Function

        ''' <summary>
        ''' Find current thread of a control.
        ''' </summary>
        <Extension>
        Public Function GetThread(c As Windows.Forms.Control) As Threading.Thread
            Return WinForm.General.GetControlThread(c)
        End Function

        ''' <summary>
        ''' Find current thread id of a control.
        ''' </summary>
        <Extension>
        Public Function GetThreadId(c As Windows.Forms.Control) As Integer
            Return WinForm.General.GetControlThreadId(c)
        End Function
#End Region

#Region "DataHelper Extensions"
        <Extension>
        Public Function ConvertToDataView(dt As DataTable) As DataView
            Return Misc.DataHelper.ConvertToDataView(dt)
        End Function

        <Extension>
        Public Function ConvertToDataTable(dv As DataView) As DataTable
            Return Misc.DataHelper.ConvertToDataTable(dv)
        End Function

        ''' <summary>
        ''' Delete all records in provided dataview.
        ''' </summary>
        <Extension>
        Public Sub Clear(ByRef dv As DataView)
            If dv IsNot Nothing Then
                Do While dv.Count > 0
                    dv(0).Delete()
                Loop
            End If
        End Sub

        ''' <summary>
        ''' Delete all records in provided datatable.
        ''' </summary>
        <Extension>
        Public Sub Clear(ByRef dv As DataTable)
            If dv IsNot Nothing Then
                Do While dv.Rows.Count > 0
                    dv(0).Delete()
                Loop
            End If
        End Sub

        ''' <summary>
        ''' Copies both the structure and data for this Dataview using System.Data.DataTable's Copy method.
        ''' </summary>
        <Extension>
        Public Function Copy(dv As DataView) As DataView
            Return dv.ToTable.Copy.ConvertToDataView
        End Function

        ''' <summary>
        ''' Clone a datarow value.
        ''' </summary>
        <Extension>
        Public Function Copy(dr As DataRow) As DataRow
            Dim list As List(Of String) = (From x As DataColumn In dr.Table.Columns Select x.ColumnName).ToList
            Dim sbList As New System.Text.StringBuilder
            For Each s As String In list
                If Not sbList.ToString.IsEmpty Then sbList.Append(";")
                sbList.Append(s)
            Next
            Dim drNew As DataRow = dr.Table.NewRow
            Misc.DataRowHelper.CopyDataRow(dr, drNew, sbList.ToString)

            Return drNew
        End Function
#End Region

#Region "Assembly Extensions"
        <Extension>
        Public Function GetPublicKeyToken(asm As Reflection.Assembly) As String
            Return Misc.AssemblyHelper.GetPublicKeyTokenFromAssembly(asm)
        End Function

        ''' <summary>
        ''' Get assembly's Codebase without file:///.
        ''' </summary>
        <Extension>
        Public Function CodeBaseEx(asm As Reflection.Assembly) As String
            Return asm.CodeBase.Remove(0, 8)
        End Function

        ''' <summary>
        ''' Get assembly's binary path. Able to return path in local and network environment.
        ''' </summary>
        <Extension>
        Public Function BinaryPath(asm As Reflection.Assembly) As String
            If asm.Location.StartsWith("\\") Then
                Return asm.Location
            Else
                Return asm.CodeBase.Remove(0, 8).Replace("/", "\")
            End If
        End Function

        <Extension>
        Public Function IsAnon(asm As Reflection.Assembly) As Boolean
            Return asm.GetName.Name = "Anonymously Hosted DynamicMethods Assembly"
        End Function

        <Extension>
        Public Function BuildTime(asm As Reflection.Assembly) As DateTime
            If asm.IsAnon Then Return Nothing
            Return Utility.General.RetrieveAssemblyBuildTime(asm.BinaryPath)
        End Function

        <Extension>
        Public Function BuildFileVersion(asm As Reflection.Assembly) As String
            If asm.IsAnon Then Return String.Empty
            Dim fvi As FileVersionInfo = Ape.EL.Utility.General.GetFileVersionInfo(asm)
            Return fvi.FileVersion.ToString
        End Function

        <Extension>
        Public Function BuildVersion(asm As Reflection.Assembly) As String
            If asm.IsAnon Then Return String.Empty
            Dim fvi As FileVersionInfo = Ape.EL.Utility.General.GetFileVersionInfo(asm)
            Return asm.GetName.Version.ToString
        End Function

        <Extension>
        Public Function GetNamespace(asm As Reflection.Assembly) As String
            If asm.IsAnon Then Return String.Empty
            Return Utility.General.GetAsmNamespace(asm, False)
        End Function

        <Extension>
        Public Function GetRootNamespace(asm As Reflection.Assembly) As String
            If asm.IsAnon Then Return String.Empty
            Return Utility.General.GetAsmNamespace(asm, True)
        End Function

        <Extension>
        Public Function GetDescription(asm As Reflection.Assembly) As String
            If asm.IsAnon Then Return String.Empty
            Return FileVersionInfo.GetVersionInfo(asm.BinaryPath).FileDescription
        End Function

        <Extension>
        Public Function GetIcon(asm As Reflection.Assembly, largeIcon As Boolean) As System.Drawing.Icon
            If asm.IsAnon Then Return Nothing
            Return Utility.General.GetAssemblyIcon(asm.BinaryPath, largeIcon)
        End Function
#End Region

#Region "Stream Extensions"
        ''' <summary>
        ''' Reads the bytes from the current stream and writes them to another stream.
        ''' </summary>
        ''' <param name="output">The stream to which the contents of the current stream will be copied.</param>
        ''' <remarks>This function is to adapt same method name implementation in .net 4.0.</remarks>
        <Extension>
        Public Sub CopyTo(input As Stream, output As Stream)
            Dim buffer As Byte() = New Byte(16 * 1024 - 1) {}   'Fairly arbitrary size
            Dim bytesRead As Integer

            While ((bytesRead = input.Read(buffer, 0, buffer.Length)) > 0)
                output.Write(buffer, 0, bytesRead)
            End While
        End Sub
#End Region

#Region "DbGeneral Helper"
        ''' <summary> Create new oGeneral optionally. </summary>
        ''' <param name="T">Set T if oGeneral's type is derived from DbGeneral.</param>
        <Extension>
        Public Sub OptNew(ByRef oGeneral As DbGeneral, Optional ConnectionString As String = "", Optional ByVal T As Type = Nothing)
            Try
                'Get connection string from previous connection
                Dim cns As String = ConnectionString
                If oGeneral IsNot Nothing And String.IsNullOrEmpty(cns) Then
                    cns = oGeneral.ConnectionString
                End If

                'Check for connection
                Dim bInstantiate As Boolean = False
                Dim bNotOpen As Boolean = False 'Determine connection exists, but is not open.
                If oGeneral Is Nothing Then
                    bInstantiate = True
                Else
                    If oGeneral.ConnectionSQL.State <> ConnectionState.Open Then
                        bInstantiate = True
                        bNotOpen = True
                    End If
                End If

                If bInstantiate Then
                    If T IsNot Nothing AndAlso GetType(DbGeneral).IsAssignableFrom(T) Then
                        oGeneral = T.GetInstance(Of String)(cns)
                    Else
                        oGeneral = New DbGeneral(cns)
                    End If
                    If bNotOpen Then oGeneral.WithGeneral = True
                Else
                    oGeneral.WithGeneral = True
                End If
            Catch ex As Exception
                Ape.EL.App.ErrorLog.WriteMessage(String.Format("OptNew: {0}", ex.Message))
            End Try
        End Sub

        ''' <summary> Dispose oGeneral optionally. </summary>
        <Extension>
        Public Sub OptDis(ByRef oGeneral As DbGeneral)
            Try
                If oGeneral IsNot Nothing AndAlso oGeneral.WithGeneral = False Then
                    oGeneral.Dispose()
                    oGeneral = Nothing
                End If
            Catch ex As Exception
                Ape.EL.App.ErrorLog.WriteMessage(String.Format("OptDis: {0}", ex.Message))
            End Try
        End Sub

        ''' <summary> Begin a simple new transaction optionally. Commit or rollback after usage. </summary>
        <Extension>
        Public Sub OptBeginTran(ByRef oGeneral As DbGeneral)
            If oGeneral Is Nothing Then
                Throw New NullReferenceException("oGeneral")
            End If

            Try
                Dim bInstantiate As Boolean = False
                If oGeneral.Transaction Is Nothing And oGeneral.WithTransaction = False Then
                    bInstantiate = True
                End If

                If bInstantiate Then
                    oGeneral.Transaction = oGeneral.ConnectionSQL.BeginTransaction()
                Else
                    oGeneral.WithTransaction = True
                End If
            Catch ex As Exception
                Ape.EL.App.ErrorLog.WriteMessage(String.Format("OptBeginTran: {0}", ex.Message))
            End Try
        End Sub

        ''' <summary> Commit optionally created transaction and dispose. </summary>
        <Extension>
        Public Sub OptCommit(ByRef oGeneral As DbGeneral)
            If oGeneral Is Nothing Then
                Throw New NullReferenceException("oGeneral")
            End If

            Try
                If oGeneral.Transaction IsNot Nothing AndAlso oGeneral.WithTransaction = False Then
                    oGeneral.Transaction.Commit()
                    oGeneral.Transaction.Dispose()
                    oGeneral.Transaction = Nothing
                End If
            Catch ex As Exception
                Ape.EL.App.ErrorLog.WriteMessage(String.Format("OptCommit: {0}", ex.Message))
            End Try
        End Sub

        ''' <summary> Rollback optionally created transaction and dispose. </summary>
        <Extension>
        Public Sub OptRollback(ByRef oGeneral As DbGeneral)
            If oGeneral Is Nothing Then
                Throw New NullReferenceException("oGeneral")
            End If

            Try
                If oGeneral.Transaction IsNot Nothing AndAlso oGeneral.WithTransaction = False Then
                    oGeneral.Transaction.Rollback()
                    oGeneral.Transaction.Dispose()
                    oGeneral.Transaction = Nothing
                End If
            Catch ex As Exception
                Ape.EL.App.ErrorLog.WriteMessage(String.Format("OptRollback: {0}", ex.Message))
            End Try
        End Sub
#End Region

#Region "Generic"
        ''' <summary>
        ''' Dispose the collection items when clearing.
        ''' </summary>
        <Extension>
        Public Sub ClearList(Of T)(collection As List(Of T), dispose As Boolean)
            If collection Is Nothing Then Exit Sub
            If collection.Count > 0 Then
                Dim bIsDisposable As Boolean = GetType(T).IsAssignableFrom(GetType(IDisposable))

                If Not bIsDisposable And dispose Then
                    'try the hard mode, see whether we have dispose function by checking the method list.
                    Dim methodList As List(Of String) = Reflex.ObjectHelper.GetMethodNameList(GetType(T))
                    bIsDisposable = methodList.Select(Of String)(Function(x) x.ToLower).Contains("dispose")
                End If

                For i As Integer = collection.Count - 1 To 0 Step -1
                    If dispose Then
                        If bIsDisposable Then
                            Try
                                CType(collection(i), IDisposable).Dispose()
                            Catch ex As Exception
                                'a safety measurement in case the Dispose function cannot be called properly.
                                Trace(ex.Message)
                            End Try
                        End If
                        collection(i) = Nothing
                        collection.RemoveAt(i)
                    Else
                        collection.RemoveAt(i)
                    End If
                Next
            End If
        End Sub

        ''' <summary>
        ''' Create a dataview from list of structure with values.
        ''' </summary>
        <Extension>
        Public Function CreateDataView(Of T)(collection As List(Of T)) As DataView
            If collection Is Nothing Then Return Nothing
            Dim dv As DataView = Ape.EL.Data.Convert.CreateDvFromStructure(Of T)()
            If collection.Count > 0 Then
                For i As Integer = 0 To collection.Count - 1
                    Ape.EL.Data.Convert.AddStructureToDv(Of T)(collection(i), dv)
                Next
            End If
            Return dv
        End Function

        ''' <summary>
        ''' Create a datatable from list of structure with values.
        ''' </summary>
        <Extension>
        Public Function CreateDataTable(Of T)(collection As List(Of T)) As DataTable
            Return CreateDataView(collection).ConvertToDataTable()
        End Function

        ''' <summary>
        ''' Create a dataview from structure with values.
        ''' </summary>
        <Extension>
        Public Function CreateDataView(Of T)(obj As T) As DataView
            Dim dv As DataView = Ape.EL.Data.Convert.CreateDvFromStructure(Of T)()

            If obj IsNot Nothing Then
                Ape.EL.Data.Convert.AddStructureToDv(Of T)(obj, dv)
            End If

            Return dv
        End Function

        ''' <summary>
        ''' Create a datatable from structure with values.
        ''' </summary>
        <Extension>
        Public Function CreateDataTable(Of T)(obj As T) As DataTable
            Return CreateDataView(obj).ConvertToDataTable()
        End Function

        ''' <summary>
        ''' Convert whole data of dataview with specific structure into list of structures.
        ''' </summary>
        <Extension>
        Public Function ToStructureList(Of T)(dv As DataView) As List(Of T)
            Return Ape.EL.Data.Convert.DvToStructureList(Of T)(dv)
        End Function

        ''' <summary>
        ''' Convert whole data of datatable with specific structure into list of structures.
        ''' </summary>
        <Extension>
        Public Function ToStructureList(Of T)(dt As DataTable) As List(Of T)
            Return ToStructureList(Of T)(dt.ConvertToDataView())
        End Function

        ''' <summary>
        ''' Copy datarowview values (with columns same as provided structure properties) to the class properties.
        ''' </summary>
        <Extension>
        Public Function ToStructure(Of T)(drv As DataRowView) As T
            Return Ape.EL.Data.Convert.DrvToStructure(Of T)(drv)
        End Function

        ''' <summary>
        ''' Copy datarowview values (with columns same as provided structure properties) to the class properties.
        ''' </summary>
        <Extension>
        Public Function ToStructure(Of T)(dr As DataRow) As T
            Return ToStructure(Of T)(dr.ToDataRowView())
        End Function

        ''' <summary>
        ''' Get datarow from datarowview.
        ''' </summary>
        <Extension>
        Public Function ToDataRow(drv As DataRowView) As DataRow
            Return drv.Row
        End Function

        ''' <summary>
        ''' Get datarowview from datarow.
        ''' </summary>
        <Extension>
        Public Function ToDataRowView(dr As DataRow) As DataRowView
            Return dr.Table.DefaultView.Cast(Of DataRowView).Where(Function(a) a.Row Is dr).FirstOrDefault()
        End Function

        ''' <summary>
        ''' Get empty string when obj is nothing. This is to avoid null reference exception.
        ''' </summary>
        <Extension>
        Public Function ToStr(Of T)(obj As T) As String
            Return Data.Convert.GetStr(obj)
        End Function

        ''' <summary>
        ''' Serialize JSON using JavaScriptSerializer method.
        ''' </summary>
        <Extension>
        Public Function JSONSerialize(Of T)(obj As T) As String
            Dim json As New JavaScriptSerializer
            json.MaxJsonLength = Int32.MaxValue
            Return json.Serialize(obj)
        End Function

        ''' <summary>
        ''' Deserialize JSON using JavaScriptSerializer method.
        ''' </summary>
        <Extension>
        Public Function JSONDeserialize(Of T)(value As String) As T
            Dim json As New JavaScriptSerializer
            json.MaxJsonLength = Int32.MaxValue
            Return json.Deserialize(Of T)(value)
        End Function

        ''' <summary>
        ''' Serialize JSON using Newtonsoft method with default settings.
        ''' </summary>
        <Extension>
        Public Function NJSONSerialize(Of T)(obj As T) As String
            Return Newtonsoft.Json.JsonConvert.SerializeObject(obj, Newtonsoft.Json.Formatting.None, InternalGlobal.NJsonSerializerSetting)
        End Function

        ''' <summary>
        ''' Serialize JSON using Newtonsoft method.
        ''' </summary>
        <Extension>
        Public Function NJSONSerialize(Of T)(obj As T, IgnoreNullValue As Boolean, IgnoreDefaultValue As Boolean) As String
            Return Newtonsoft.Json.JsonConvert.SerializeObject(obj, Newtonsoft.Json.Formatting.None, InternalGlobal.GetNJsonSerializerSetting(IgnoreNullValue, IgnoreDefaultValue))
        End Function

        ''' <summary>
        ''' Serialize JSON using Newtonsoft method with default settings. Formated / indented.
        ''' </summary>
        <Extension>
        Public Function NJSONSerializeF(Of T)(obj As T) As String
            Return Newtonsoft.Json.JsonConvert.SerializeObject(obj, Newtonsoft.Json.Formatting.Indented, InternalGlobal.NJsonSerializerSetting)
        End Function

        ''' <summary>
        ''' Serialize JSON using Newtonsoft method. Formated / indented.
        ''' </summary>
        <Extension>
        Public Function NJSONSerializeF(Of T)(obj As T, IgnoreNullValue As Boolean, IgnoreDefaultValue As Boolean) As String
            Return Newtonsoft.Json.JsonConvert.SerializeObject(obj, Newtonsoft.Json.Formatting.Indented, InternalGlobal.GetNJsonSerializerSetting(IgnoreNullValue, IgnoreDefaultValue))
        End Function

        ''' <summary>
        ''' Deserialize JSON using Newtonsoft method.
        ''' </summary>
        <Extension>
        Public Function NJSONDeserialize(Of T)(value As String) As T
            Return Newtonsoft.Json.JsonConvert.DeserializeObject(Of T)(value)
        End Function

        ''' <summary>
        ''' Swap position between 2 indexes in a list.
        ''' </summary>
        <Extension>
        Public Sub Swap(Of T)(list As List(Of T), index1 As Integer, index2 As Integer)
            If index1 > list.Count - 1 Or index2 > list.Count - 1 Then
                Throw New ApplicationException("Index out of bound.")
            End If
            Dim temp As T = list(index1)
            list(index1) = list(index2)
            list(index2) = temp
        End Sub

        <Extension()>
        Public Sub Add(Of T)(ByVal source As List(Of T), ParamArray item As T())
            source.AddRange(item)
        End Sub
#End Region

#Region "Custom Property Extensions"
        ''' <summary>
        ''' Required by GetExProp and SetExProp to indicate a key in the dictionary.
        ''' </summary>
        Private Class PropKey
            Property Obj As Object
            Property Name As String
            Public Sub New(obj As Object, name As String)
                Me.Obj = obj
                Me.Name = name
            End Sub
        End Class

        ''' <summary>
        ''' Required by GetExProp and SetExProp to store values in dictionary.
        ''' </summary>
        Private PropDictionary As New Dictionary(Of PropKey, Object)

        Private PropDictionaryAccessCount As Integer

        Private Sub DeletePropDictionaryObjects()
            If PropDictionaryAccessCount >= 100 Then
                Using lockObj As New Helper.LockHolder(Of Dictionary(Of PropKey, Object))(PropDictionary, 1000)
                    Try

                        Dim delList As New List(Of PropKey)
                        For Each kvp As KeyValuePair(Of PropKey, Object) In PropDictionary
                            Dim v1 As PropKey = kvp.Key
                            Dim v2 As Object = kvp.Value
                            Dim dispose As Boolean
                            If v1.Obj Is Nothing Then
                                dispose = True
                            ElseIf v2 Is Nothing OrElse IsDBNull(v2) OrElse v2.ToString = "" Then
                                dispose = True
                            End If

                            If dispose Then
                                delList.Add(v1)
                            End If
                        Next
                        For Each prop As PropKey In delList
                            PropDictionary.Remove(prop)
                        Next
                        delList.Clear()
                        PropDictionaryAccessCount = 0
                    Catch ex As Exception
                        Ape.EL.App.ErrorLog.Debug(ex.ToString)
                        PropDictionaryAccessCount = 0
                    End Try
                End Using
            End If

            PropDictionaryAccessCount += 1
        End Sub

        ''' <summary>
        ''' Get a custom property by name. This is not type safe. Value will be kept lifetime even the object is already disposed.
        ''' </summary>
        ''' <param name="propName">The custom property name.</param>
        ''' <returns>Return value as string.</returns>
        <Extension>
        Public Function GetExPropertyStr(Of T)(obj As T, ByVal propName As String) As String
            DeletePropDictionaryObjects()
            If obj Is Nothing Then Return String.Empty

            Using lockObj As New Helper.LockHolder(Of Dictionary(Of PropKey, Object))(PropDictionary, 1000)
                Dim key As PropKey = (From x As PropKey In PropDictionary.Keys Where obj IsNot Nothing AndAlso x.Obj.Equals(obj) And x.Name = propName Select x).FirstOrDefault

                If key IsNot Nothing AndAlso PropDictionary.ContainsKey(key) Then
                    Return Data.Convert.GetStr(PropDictionary(key))
                End If
            End Using

            Return String.Empty
        End Function

        ''' <summary>
        ''' Get a custom property by name. This is not type safe. Value will be kept lifetime even the object is already disposed.
        ''' </summary>
        ''' <param name="propName">The custom property name.</param>
        ''' <returns>Return a value previously set using SetExProperty. Return DBNull.Value as default.</returns>
        <Extension>
        Public Function GetExProperty(Of T)(obj As T, ByVal propName As String) As Object
            DeletePropDictionaryObjects()
            If obj Is Nothing Then Return DBNull.Value

            Using lockObj As New Helper.LockHolder(Of Dictionary(Of PropKey, Object))(PropDictionary, 1000)
                Dim key As PropKey = (From x As PropKey In PropDictionary.Keys Where obj IsNot Nothing AndAlso x.Obj.Equals(obj) And x.Name = propName Select x).FirstOrDefault

                If key IsNot Nothing AndAlso PropDictionary.ContainsKey(key) Then
                    Return PropDictionary(key)
                End If
            End Using

            Return DBNull.Value
        End Function

        ''' <summary>
        ''' Set a custom property by name. This is not type safe. Value will be kept lifetime even the object is already disposed.
        ''' </summary>
        ''' <param name="propName">The custom property name.</param>
        ''' <param name="value">The value of the custom property.</param>
        <Extension>
        Public Sub SetExProperty(Of T)(obj As T, propName As String, value As Object)
            DeletePropDictionaryObjects()
            If obj Is Nothing Then Exit Sub

            Using lockObj As New Helper.LockHolder(Of Dictionary(Of PropKey, Object))(PropDictionary, 1000)
                Dim key As PropKey = (From x As PropKey In PropDictionary.Keys Where obj IsNot Nothing AndAlso x.Obj.Equals(obj) And x.Name = propName Select x).FirstOrDefault

                If key Is Nothing OrElse Not PropDictionary.ContainsKey(key) Then
                    key = New PropKey(obj, propName)
                    PropDictionary.Add(key, Nothing)
                Else
                    If value Is Nothing Then
                        PropDictionary.Remove(key)
                    End If
                End If
                If value IsNot Nothing Then
                    PropDictionary(key) = value
                End If
            End Using
        End Sub
#End Region

#Region "Type Extension"
        ''' <summary>
        ''' Determine if following object type contains public property of propertyName.
        ''' </summary>
        <Extension>
        Public Function HasProperty(obj As Type, propertyName As String) As Boolean
            Return obj.GetProperty(propertyName) IsNot Nothing
        End Function

        ''' <summary>
        ''' Determine if following object type is windows form control or component type with Name property.
        ''' </summary>
        <Extension>
        Public Function IsControl(obj As Type) As Boolean
            Return (GetType(System.ComponentModel.Component).IsAssignableFrom(obj) OrElse GetType(Windows.Forms.Control).IsAssignableFrom(obj)) AndAlso HasProperty(obj, "Name")
        End Function
#End Region

#Region "Form Extension"
        ''' <summary>
        ''' Determine whether form is fullscreen by checking its FormBorderStyle (None) and WindowState (Maximized).
        ''' </summary>
        <Extension>
        Public Function IsFullScreen(obj As System.Windows.Forms.Form) As Boolean
            Return obj IsNot Nothing AndAlso obj.FormBorderStyle = Windows.Forms.FormBorderStyle.None AndAlso obj.WindowState = Windows.Forms.FormWindowState.Maximized
        End Function
#End Region

#Region "Enum Localization"
        ''' <summary>
        ''' Extends Localizer.GetString function to enums. Return empty string when exception is caught.
        ''' Must have LocalizableString attribute on the enum.
        ''' </summary>
        <Extension>
        Public Function GetString(id As System.Enum, ParamArray args() As Object) As String
            Try
                'Check if LocalizableStringAttribute exist for this enum. If not then we return empty.
                Dim type As System.Type = id.[GetType]()
                Dim localizableStringAttribute As Ape.EL.Localization.LocalizableStringAttribute = TryCast(System.Attribute.GetCustomAttribute(type, GetType(Ape.EL.Localization.LocalizableStringAttribute)), Ape.EL.Localization.LocalizableStringAttribute)
                If localizableStringAttribute IsNot Nothing Then
                    Return Ape.EL.Localization.Localizer.GetString(id, args)
                End If
            Catch ex As Exception
            End Try
            Return String.Empty
        End Function

        ''' <summary>
        ''' Check if contain attribute.
        ''' </summary>
        <Extension>
        Public Function HasAttrib(Of T As System.Attribute)(value As System.Enum) As Boolean
            Try
                Dim enumType = value.[GetType]()
                Dim name = [Enum].GetName(enumType, value)
                Dim val = enumType.GetField(name).GetCustomAttributes(False).OfType(Of T)().SingleOrDefault()

                Return val IsNot Nothing
            Catch ex As Exception
            End Try
            Return False
        End Function
#End Region

#Region "Enum Valuation"
        ''' <summary>
        ''' Extends Valuator.GetString function to enums. Return empty string when exception is caught.
        ''' Must have Valuator attribute on the enum.
        ''' </summary>
        <Extension>
        Public Function GetValuatorString(id As System.Enum, ParamArray args() As Object) As String
            Try
                'Check if ValuatorAttribute exist for this enum. If not then we return empty.
                Dim type As System.Type = id.[GetType]()
                Dim localizableStringAttribute As Ape.EL.Valuation.ValuatorAttribute = TryCast(System.Attribute.GetCustomAttribute(type, GetType(Ape.EL.Valuation.ValuatorAttribute)), Ape.EL.Valuation.ValuatorAttribute)
                If localizableStringAttribute IsNot Nothing Then
                    Return Ape.EL.Valuation.Valuator.GetString(id, args)
                End If
            Catch ex As Exception
            End Try
            Return String.Empty
        End Function
#End Region

#Region "Enum Extensions"
        ''' <summary>
        ''' A FX 3.5 way to mimic the FX4 "HasFlag" method.
        ''' </summary>
        ''' <param name="variable">The tested enum.</param>
        ''' <param name="value">The value to test.</param>
        ''' <returns>True if the flag is set. Otherwise false.</returns>
        <Extension>
        Public Function HasFlag(variable As [Enum], value As [Enum]) As Boolean
            ' check if from the same type.
            If variable.GetType IsNot value.GetType Then
                Throw New ArgumentException("The checked flag is not from the same type as the checked variable.")
            End If

            Dim num As ULong = Convert.ToUInt64(value)
            Dim num2 As ULong = Convert.ToUInt64(variable)

            Return (num2 And num) = num
        End Function
#End Region

#Region "CollectionBase"
        ''' <summary>
        ''' Determine member type of a collection. Collection must have at least 1 member.
        ''' </summary>
        <Extension>
        Public Function GetMemberType(collection As CollectionBase) As Type
            If collection.Count > 0 AndAlso collection(0) IsNot Nothing Then
                Return collection(0).GetType
            End If
            Return GetType(Object)
        End Function
#End Region

#Region "Threading.Thread"
        ''' <summary>
        ''' Rename a thread that has been named before.
        ''' </summary>
        <Extension>
        Public Sub SetName(t1 As Threading.Thread, ByVal name As String)
            Try
                SyncLock t1
                    Reflex.PropertyHelper.SetPrivateFieldValue(Of String)(t1, "m_Name", Nothing)
                    't1.[GetType]().GetField("m_Name", BindingFlags.Instance Or BindingFlags.NonPublic).SetValue(t1, Nothing)
                    t1.Name = name
                End SyncLock
            Catch ex As Exception
                'consume exception
            End Try
        End Sub
#End Region

#Region "ServiceProcess.ServiceController"
        ''' <summary>
        ''' Continue paused service; Start stopped service.
        ''' </summary>
        <Extension>
        Public Sub SoftStart(svc As ServiceProcess.ServiceController, Optional ByVal args() As String = Nothing)
            If svc IsNot Nothing Then
                Try
                    If args Is Nothing Then args = New String() {}
                    Select Case svc.Status
                        Case ServiceProcess.ServiceControllerStatus.Stopped
                            svc.Start(args)
                        Case ServiceProcess.ServiceControllerStatus.Paused
                            svc.Continue()
                    End Select
                Catch ex As Exception
                    Ape.EL.App.ErrorLog.Error(ex.ToString)
                End Try
            End If
        End Sub

        ''' <summary>
        ''' Continue paused service; Start stopped service.
        ''' </summary>
        <Extension>
        Public Sub Restart(svc As ServiceProcess.ServiceController)
            If svc IsNot Nothing Then
                Try
                    Dim name As String = svc.ServiceName
                    If svc.Status <> ServiceProcess.ServiceControllerStatus.Running Then
                        If svc.CanStop Then
                            svc.Stop()
                        End If
                    End If
                    Dim ps() As Process = System.Diagnostics.Process.GetProcessesByName(name)
                    If ps Is Nothing Then ps = New Process() {}
                    For Each p As Process In ps
                        p.Kill()
                        ' possibly with a timeout
                        p.WaitForExit()
                        ' process was terminating or can't be terminated
                    Next

                    svc.SoftStart()
                Catch ex As Exception
                    Ape.EL.App.ErrorLog.Error(ex)
                End Try
            End If
        End Sub
#End Region

#Region "IEnumerable"
        'https://stackoverflow.com/a/34180417
        <Extension()>
        Public Function DistinctBy(Of T, TKey)(ByVal items As IEnumerable(Of T), ByVal [property] As Func(Of T, TKey)) As IEnumerable(Of T)
            Dim comparer As GeneralPropertyComparer(Of T, TKey) = New GeneralPropertyComparer(Of T, TKey)([property])
            Return items.Distinct(comparer)
        End Function

        Private Class GeneralPropertyComparer(Of T, TKey)
            Implements IEqualityComparer(Of T)

            Private Property expr As Func(Of T, TKey)

            Public Sub New(ByVal expr As Func(Of T, TKey))
                Me.expr = expr
            End Sub

            Public Shadows Function Equals(ByVal left As T, ByVal right As T) As Boolean Implements IEqualityComparer(Of T).Equals
                Dim leftProp = expr.Invoke(left)
                Dim rightProp = expr.Invoke(right)

                If leftProp Is Nothing AndAlso rightProp Is Nothing Then
                    Return True
                ElseIf leftProp Is Nothing Xor rightProp Is Nothing Then
                    Return False
                Else
                    Return leftProp.Equals(rightProp)
                End If
            End Function

            Public Shadows Function GetHashCode(ByVal obj As T) As Integer Implements IEqualityComparer(Of T).GetHashCode
                Dim prop = expr.Invoke(obj)
                Return If((prop Is Nothing), 0, prop.GetHashCode())
            End Function
        End Class

        <Extension()>
        Public Function GetDefaults(Of TSource)(ByVal source As IEnumerable(Of TSource)) As TSource
            Try
                Dim val As TSource = Nothing
                If val Is Nothing Then val = Activator.CreateInstance(Of TSource)()
                Return val
            Catch
                Return Nothing
            End Try
        End Function

        <Extension()>
        Public Function GetDefault(Of TSource)(ByVal source As TSource) As TSource
            Try
                Dim val As TSource = Nothing
                If val Is Nothing Then val = Activator.CreateInstance(Of TSource)()
                Return val
            Catch
                Return Nothing
            End Try
        End Function

        <Extension()>
        Public Function FirstOrInstance(Of TSource)(ByVal source As IEnumerable(Of TSource)) As TSource
            Dim val = source.FirstOrDefault()
            If val Is Nothing Then val = val.GetDefault()
            Return val
        End Function

        <Extension()>
        Public Function FirstOrInstance(Of TSource)(ByVal source As IEnumerable(Of TSource), predicate As System.Func(Of TSource, Boolean)) As TSource
            Dim val = source.FirstOrDefault(predicate)
            If val Is Nothing Then val = val.GetDefault()
            Return val
        End Function

        <Extension()>
        Public Function LastOrInstance(Of TSource)(ByVal source As IEnumerable(Of TSource)) As TSource
            Dim val = source.LastOrDefault()
            If val Is Nothing Then val = val.GetDefault()
            Return val
        End Function

        <Extension()>
        Public Function LastOrInstance(Of TSource)(ByVal source As IEnumerable(Of TSource), predicate As System.Func(Of TSource, Boolean)) As TSource
            Dim val = source.LastOrDefault(predicate)
            If val Is Nothing Then val = val.GetDefault()
            Return val
        End Function

        <Extension()>
        Public Function ElementAtOrInstance(Of TSource)(source As System.Collections.Generic.IEnumerable(Of TSource), index As Integer) As TSource
            Dim val = source.ElementAtOrDefault(index)
            If val Is Nothing Then val = val.GetDefault()
            Return val
        End Function

        <Extension()>
        Public Function SingleOrInstance(Of TSource)(source As System.Collections.Generic.IEnumerable(Of TSource)) As TSource
            Dim val = source.SingleOrDefault()
            If val Is Nothing Then val = val.GetDefault()
            Return val
        End Function

        <Extension()>
        Public Function SingleOrInstance(Of TSource)(source As System.Collections.Generic.IEnumerable(Of TSource), predicate As System.Func(Of TSource, Boolean)) As TSource
            Dim val = source.SingleOrDefault(predicate)
            If val Is Nothing Then val = val.GetDefault()
            Return val
        End Function

        'https://www.codeproject.com/Tips/375967/Flatten-a-Hierarchical-Collection-of-Objects-with
        ''' <summary>
        ''' Usage: 
        ''' myObjects
        ''' .Where(myObject => myObject.Id == 1)
        ''' .Flatten(myObject => myObject.Children)
        ''' .ToList(); 
        ''' 
        ''' myObjects
        ''' .Where(myObject => myObject.Id == 1)
        ''' .Flatten((myObject, objectsBeingFlattened) =>
        '''           myObject.Children.Except(objectsBeingFlattened))
        ''' .ToList();
        ''' </summary>
        <Extension()>
        Public Function Flatten(Of T)(ByVal source As IEnumerable(Of T), ByVal childPropertySelector As Func(Of T, IEnumerable(Of T))) As IEnumerable(Of T)
            Return source.Flatten(Function(itemBeingFlattened, objectsBeingFlattened) childPropertySelector(itemBeingFlattened))
        End Function

        <Extension()>
        Public Function Flatten(Of T)(ByVal source As IEnumerable(Of T), ByVal childPropertySelector As Func(Of T, IEnumerable(Of T), IEnumerable(Of T))) As IEnumerable(Of T)
            Return source.Concat(source.Where(Function(item) childPropertySelector(item, source) IsNot Nothing).SelectMany(Function(itemBeingFlattened) childPropertySelector(itemBeingFlattened, source).Flatten(childPropertySelector)))
        End Function
#End Region

#Region "Dictionary"
        <Extension()>
        Public Function GetValue(Of TKey, TValue)(ByVal dictionary As Dictionary(Of TKey, TValue), ByVal key As TKey) As TValue
            Return dictionary.Where(Function(x) x.Key.Equals(key)).FirstOrDefault().Value
        End Function
#End Region

#Region "DataTable"
        <Extension()>
        Public Function CopyToDataTableEx(Of T As DataRow)(ByVal source As IEnumerable(Of T)) As DataTable
            If source IsNot Nothing AndAlso source.Count() > 0 Then
                Return source.CopyToDataTable()
            End If

            Return Nothing
        End Function

        <Extension()>
        Public Function SelectDt(ByVal dt As DataTable, Optional ByVal filterExpression As String = Nothing, Optional ByVal sort As String = Nothing) As DataTable
            If dt IsNot Nothing Then
                Dim dt2 As DataTable = dt.Clone()
                dt = dt.[Select](filterExpression, sort).CopyToDataTableEx()

                If dt IsNot Nothing Then
                    Return dt
                Else
                    Return dt2
                End If
            End If

            Return Nothing
        End Function

        <Extension>
        Public Function RemoveColumnsByPrefix(ByVal dt As DataTable, ByVal prefix As String) As DataTable
            If dt IsNot Nothing Then
                Dim colToRemoveItemMaster = (From x As DataColumn In dt.Columns Where x.ColumnName.StartsWith(prefix) Select x.ColumnName).ToList()

                While colToRemoveItemMaster.Any()
                    dt.Columns.Remove(colToRemoveItemMaster(0))
                    colToRemoveItemMaster.RemoveAt(0)
                End While
            End If
            Return dt
        End Function

        <Extension>
        Public Function SetDateMinValueAsDbNull(ByVal dt As DataTable, ByVal colname As String) As DataTable
            If dt IsNot Nothing Then
                Dim colToRemoveItemMaster = (From x As DataColumn In dt.Columns Where x.ColumnName = colname Select x.ColumnName).ToList()

                For Each col In colToRemoveItemMaster
                    For Each row In dt.Rows
                        Dim cell As DateTime
                        If DateTime.TryParse(row(col), cell) Then
                            If cell = DateTime.MinValue Then row(col) = DBNull.Value
                        Else
                            Exit For
                        End If
                    Next
                Next
            End If
            Return dt
        End Function
#End Region

#Region "LINQ"
        <Extension()>
        Public Function LinqUnNullBool(ByVal val As Boolean?, Optional ByVal defaultValue As Boolean = False) As Boolean
            If val.HasValue Then Return val.Value
            Return defaultValue
        End Function

        <Extension()>
        Public Function LinqUnNullNumber(ByVal val As Decimal?) As Decimal
            If val.HasValue Then Return val.Value
            Return 0
        End Function

        <Extension()>
        Public Function LinqUnNullInt(ByVal val As Integer?) As Integer
            If val.HasValue Then Return val.Value
            Return 0
        End Function

        <Extension()>
        Public Function LinqUnNullDateTime(ByVal val As DateTime?) As DateTime
            If val.HasValue Then Return val.Value
            Return DateTime.MinValue
        End Function
#End Region
    End Module
End Namespace