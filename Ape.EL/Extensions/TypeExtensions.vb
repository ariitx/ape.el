﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Linq.Expressions
Imports System.Reflection

Namespace Extensions
    ''' <summary>
    ''' This is an extensions module.
    ''' </summary>
    <HideModuleName()> _
    Public Module TypeExtensions
        ''' <summary>
        ''' Returns an instance of the <paramref name="type"/> on which the method is invoked.
        ''' </summary>
        ''' <param name="type">The type on which the method was invoked.</param>
        ''' <returns>An instance of the <paramref name="type"/>.</returns>
        <System.Runtime.CompilerServices.Extension> _
        Public Function GetInstance(type As Type) As Object
            Return GetInstance(Of TypeToIgnore)(type, Nothing)
        End Function

        ''' <summary>
        ''' Returns an instance of the <paramref name="type"/> on which the method is invoked.
        ''' </summary>
        ''' <typeparam name="TArg">The type of the argument to pass to the constructor.</typeparam>
        ''' <param name="type">The type on which the method was invoked.</param>
        ''' <param name="argument">The argument to pass to the constructor.</param>
        ''' <returns>An instance of the given <paramref name="type"/>.</returns>
        <System.Runtime.CompilerServices.Extension> _
        Public Function GetInstance(Of TArg)(type As Type, argument As TArg) As Object
            Return GetInstance(Of TArg, TypeToIgnore)(type, argument, Nothing)
        End Function

        ''' <summary>
        ''' Returns an instance of the <paramref name="type"/> on which the method is invoked.
        ''' </summary>
        ''' <typeparam name="TArg1">The type of the first argument to pass to the constructor.</typeparam>
        ''' <typeparam name="TArg2">The type of the second argument to pass to the constructor.</typeparam>
        ''' <param name="type">The type on which the method was invoked.</param>
        ''' <param name="argument1">The first argument to pass to the constructor.</param>
        ''' <param name="argument2">The second argument to pass to the constructor.</param>
        ''' <returns>An instance of the given <paramref name="type"/>.</returns>
        <System.Runtime.CompilerServices.Extension> _
        Public Function GetInstance(Of TArg1, TArg2)(type As Type, argument1 As TArg1, argument2 As TArg2) As Object
            Return GetInstance(Of TArg1, TArg2, TypeToIgnore)(type, argument1, argument2, Nothing)
        End Function

        ''' <summary>
        ''' Returns an instance of the <paramref name="type"/> on which the method is invoked.
        ''' </summary>
        ''' <typeparam name="TArg1">The type of the first argument to pass to the constructor.</typeparam>
        ''' <typeparam name="TArg2">The type of the second argument to pass to the constructor.</typeparam>
        ''' <typeparam name="TArg3">The type of the third argument to pass to the constructor.</typeparam>
        ''' <param name="type">The type on which the method was invoked.</param>
        ''' <param name="argument1">The first argument to pass to the constructor.</param>
        ''' <param name="argument2">The second argument to pass to the constructor.</param>
        ''' <param name="argument3">The third argument to pass to the constructor.</param>
        ''' <returns>An instance of the given <paramref name="type"/>.</returns>
        <System.Runtime.CompilerServices.Extension> _
        Public Function GetInstance(Of TArg1, TArg2, TArg3)(type As Type, argument1 As TArg1, argument2 As TArg2, argument3 As TArg3) As Object
            Return InstanceCreationFactory(Of TArg1, TArg2, TArg3).CreateInstanceOf(type, argument1, argument2, argument3)
        End Function

        ' To allow for overloads with differing numbers of arguments, we flag arguments which should be 
        ' ignored by using this Type:
        Private Class TypeToIgnore
        End Class

        Private NotInheritable Class InstanceCreationFactory(Of TArg1, TArg2, TArg3)
            ' This dictionary will hold a cache of object-creation functions, keyed by the Type to create:
            Private Shared ReadOnly _instanceCreationMethods As New Dictionary(Of Type, Func(Of TArg1, TArg2, TArg3, Object))()

            Public Shared Function CreateInstanceOf(type As Type, arg1 As TArg1, arg2 As TArg2, arg3 As TArg3) As Object
                Try
                    CacheInstanceCreationMethodIfRequired(type)

                    Return _instanceCreationMethods(type).Invoke(arg1, arg2, arg3)
                Catch ex As Exception
                    Dim msg As String = ""
                    msg.AppendLn(String.Format("InstanceCreationFactory.CreateInstanceOf failed to construct '{0}' object type.", type.FullName))
                    msg.AppendLn(ex.ToString)
                    App.ErrorLog.Fatal(msg)
                    Throw ex
                End Try
            End Function

            Private Shared Sub CacheInstanceCreationMethodIfRequired(type As Type)
                ' Bail out if we've already cached the instance creation method:
                If _instanceCreationMethods.ContainsKey(type) Then
                    Return
                End If

                Dim argumentTypes As Type() = New Type() {GetType(TArg1), GetType(TArg2), GetType(TArg3)}

                ' Get a collection of the constructor argument Types we've been given; ignore any 
                ' arguments which are of the 'ignore this' Type:
                Dim constructorArgumentTypes As Type() = argumentTypes.Where(Function(t) t IsNot GetType(TypeToIgnore)).ToArray()

                ' Get the Constructor which matches the given argument Types:
                Dim constructor As System.Reflection.ConstructorInfo = type.GetConstructor(BindingFlags.Instance Or BindingFlags.[Public], Nothing, CallingConventions.HasThis, constructorArgumentTypes, New ParameterModifier(-1) {})

                ' Get a set of Expressions representing the parameters which will be passed to the Func:
                Dim lamdaParameterExpressions As ParameterExpression() = New ParameterExpression() {Expression.Parameter(GetType(TArg1), "param1"), Expression.Parameter(GetType(TArg2), "param2"), Expression.Parameter(GetType(TArg3), "param3")}

                ' Get a set of Expressions representing the parameters which will be passed to the constructor:
                Dim constructorParameterExpressions As System.Collections.Generic.IEnumerable(Of Expression) = lamdaParameterExpressions.Take(constructorArgumentTypes.Length).ToArray()

                ' Get an Expression representing the constructor call, passing in the constructor parameters:
                Dim constructorCallExpression As System.Linq.Expressions.NewExpression = Expression.[New](constructor, constructorParameterExpressions)

                ' Compile the Expression into a Func which takes three arguments and returns the constructed object:
                Dim constructorCallingLambda As System.Func(Of TArg1, TArg2, TArg3, Object) = Expression.Lambda(Of Func(Of TArg1, TArg2, TArg3, Object))(constructorCallExpression, lamdaParameterExpressions).Compile()

                _instanceCreationMethods(type) = constructorCallingLambda
            End Sub
        End Class
    End Module
End Namespace
